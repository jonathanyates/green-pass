import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {IAlertType} from "../../../../../../shared/interfaces/alert.interface";
import {IReportType} from "../../../../../../shared/interfaces/report.interface";

export class AlertTypeInputMapper {

  static mapFrom(form:FormGroup, reportTypes: IReportType[]): IAlertType {

    let reportTypeId = form.controls['reportType'].value;
    let reportType = reportTypes.find(rt => rt._id === reportTypeId);

    return <IAlertType>{
      priority: form.controls['priority'].value,
      reportType: reportType
    };
  }

  static mapTo(alertType:IAlertType, reportTypes: IReportType[]): FormInputGroup[] {

    let priorities = [
      { key: 'High', value: 'High', selected: alertType.priority === 'High' },
      { key: 'Medium', value: 'Medium', selected: alertType.priority === 'Medium' },
      { key: 'Low', value: 'Low', selected: alertType.priority === 'Low' },

    ];

    let reportTypeOptions = reportTypes.map(reportType => {
      return {
        key: reportType._id,
        value: reportType.name,
        selected: alertType.reportType && alertType.reportType._id === reportType._id
      }
    });

    return [
      {
        label: 'AlertType',
        inputs: [
          { id: 'reportType', label: 'Report', type:'dropdown', required: true,
            value: alertType.reportType ? alertType.reportType._id : null, gridClass: 'col-md-6', options: reportTypeOptions },
          { id: 'priority', label: 'Priority', type:'radio', required: true,
            value: alertType.priority, options: priorities },
        ]
      }
    ];
  }



}
