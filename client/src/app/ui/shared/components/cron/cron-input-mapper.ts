import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {Cron} from "./cron.model";
import {FormInputGroup} from "../dynamic-form/dynamic-form.model";

/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */

/*
Month #####  Day of Month ##### Day of Week ##### (at) Hour #### Minutes #####
      Every               Every             Every           Every        Every
      Jan                  1                 Sun             0            0
      Feb                  2                 Mon             1            1
      Mar                  3                 Tue             2            2
      etc                 etc(to 12)         etc            etc(to 23)   etc(to 59)
*/

@Injectable()
export class CronInputMapper {

  static mapFrom(form:FormGroup): Cron {
    let cron = new Cron();
    cron.minute = form.controls['minute'].value;
    cron.hour = form.controls['hour'].value;
    cron.dayOfWeek = form.controls['dayOfWeek'].value;
    cron.dayOfMonth = form.controls['dayOfMonth'].value;
    cron.month = form.controls['month'].value;
    return cron;
  }

  static mapTo(cron: Cron): FormInputGroup {

    let minutes = Array.from(new Array(60), (val, index) => {
        let minute = index.toString();
        return { key: minute, value: minute, selected: cron.minute === minute };
      });

    let hours = [{ key: '*', value: 'Any', selected: cron.hour === '*' }]
      .concat(Array.from(new Array(24), (val, index) => {
        let hour = index.toString();
        return { key: hour, value: hour, selected: cron.hour === hour };
      }));

    let daysOfMonth = [{ key: '*', value: 'Any', selected: cron.dayOfMonth === '*' }]
      .concat(Array.from(new Array(31), (val, index) => {
        let day = (index+1).toString();
        return { key: day, value: day, selected: cron.dayOfMonth === day };
      }));

    let months = [
      { value: 'Any', key: '*', selected: cron.month === '*' },
      { value: 'January', key: '1', selected: cron.month === '1' },
      { value: 'February', key: '2', selected: cron.month === '2' },
      { value: 'March', key: '3', selected: cron.month === '3' },
      { value: 'April', key: '4', selected: cron.month === '4' },
      { value: 'May', key: '5', selected: cron.month === '5' },
      { value: 'June', key: '6', selected: cron.month === '6' },
      { value: 'July', key: '7', selected: cron.month === '7' },
      { value: 'August', key: '8', selected: cron.month === '8' },
      { value: 'September', key: '9', selected: cron.month === '9' },
      { value: 'October', key: '10', selected: cron.month === '10' },
      { value: 'November', key: '11', selected: cron.month === '11' },
      { value: 'December', key: '12', selected: cron.month === '12' },
    ];

    let daysOfWeek = [
      { value: 'Any', key: '*', selected: cron.dayOfWeek === '*' },
      { value: 'Sunday', key: '0', selected: cron.dayOfWeek === '0' },
      { value: 'Monday', key: '1', selected: cron.dayOfWeek === '1' },
      { value: 'Tuesday', key: '2', selected: cron.dayOfWeek === '2' },
      { value: 'Wednesday', key: '3', selected: cron.dayOfWeek === '3' },
      { value: 'Thursday', key: '4', selected: cron.dayOfWeek === '4' },
      { value: 'Friday', key: '5', selected: cron.dayOfWeek === '5' },
      { value: 'Saturday', key: '6', selected: cron.dayOfWeek === '6' },
    ];

    return  {
      label: 'cron',
      inputs: [
        { id: 'month', label: 'Month', type:'dropdown', required: true, options: months, value: cron.month },
        { id: 'dayOfMonth', label: 'Day Of Month', type:'dropdown', required: true, options: daysOfMonth, value: cron.dayOfMonth },
        { id: 'dayOfWeek', label: 'Day Of Week', type:'dropdown', required: true, options: daysOfWeek, value: cron.dayOfWeek },
        { id: 'hour', label: 'Hour', type:'dropdown', required: true, options: hours, value: cron.hour },
        { id: 'minute', label: 'Minute', type:'dropdown', required: true, options: minutes, value: cron.minute },
      ]
    };
  }

}
