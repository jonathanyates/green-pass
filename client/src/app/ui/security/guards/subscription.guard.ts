import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {AuthenticationState} from "../../../store/authentication/authentication.reducer";
import {
  SelectAuthenticationState,
  SelectIsGreenPassAdmin
} from "../../../store/authentication/authentication.selectors";
import {Paths} from "../../../shared/constants";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Observable} from "rxjs/Observable";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {SubscriptionHelper} from "../../../../../../shared/models/subscription.model";
import {Roles} from "../../../../../../shared/constants";

const subscriptionResourceMap = {
  assessments: 'onLineAssessments',
  roadMarque: 'onLineAssessments'
};

@Injectable()
export class SubscriptionGuard implements CanActivate, CanActivateChild {

  private authState: AuthenticationState;

  constructor(private store: Store<AppState>,
              private router: Router) {
    this.store.let(SelectAuthenticationState())
      .subscribe(authState => this.authState = authState);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    let user = this.authState.user;
    if (!user) {
      return Observable.of(true);
    }

    let companyId = user._companyId;

    if (companyId) {
      this.store.dispatch(new SelectCompanyAction(companyId));
    }

    if (user.role === Roles.Admin) {
      return Observable.of(true);
    }

    return this.store.let(SelectCompany()).take(1)
      .map(company => {
        let key = Object.keys(subscriptionResourceMap).find(key =>  state.url.includes(key));
        if (key) {
          let resource = subscriptionResourceMap[key];
          let subscription = SubscriptionHelper.getSubscription(company);
          let allow:boolean = subscription.resources[resource];

          if (!allow) {
            this.router.navigate([Paths.Unauthorized]);
            return false;
          }

          return allow;
        }
      });

  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

}
