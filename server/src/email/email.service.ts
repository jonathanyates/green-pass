import logger = require('winston');
import nodemailer = require('nodemailer');
import { injectable } from 'inversify';

export interface IEmail {
  recipients: string[];
  from?: string;
  subject: string;
  content: string;
  attachments?: any[];
}

export interface IEmailService {
  send(recipients: string[], subject: string, content: string, from?: string, attachments?: any[]): Promise<boolean>;
}

@injectable()
export class EmailService implements IEmailService {
  send(recipients: string[], subject: string, content: string, from?: string, attachments?: any[]): Promise<boolean> {
    if (from == null) {
      from = 'admin@greenpasscompliance.co.uk';
    }

    const transporter = nodemailer.createTransport({
      host: 'smtp.zoho.eu',
      port: 465,
      secure: true,
      auth: {
        user: from,
        pass: 'Everest99!',
      },
    });

    const mailOptions = {
      from: '"Green Pass Compliance" <' + from + '>',
      to: recipients.join(', '),
      subject: subject,
      html: content,
    };

    return new Promise((resolve, reject) => {
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          logger.error(`Failed to send email to ${mailOptions.to}`);
          logger.error(error.message);
          return reject(error);
        }

        logger.info(`Email '${subject}' sent to ${mailOptions.to}`);
        return resolve(true);
      });
    });
  }
}
