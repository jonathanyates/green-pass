import {AbstractControl} from "@angular/forms";
import {Observable} from "rxjs";

export interface FormInput {
  id: string;
  label:string;
  type: string;
  value?: any;
  required?: boolean;
  regex?: RegExp;
  autoFocus?: boolean;
  icon?: string;
  gridClass?: string;
  options?: {key: string, value: string, selected: boolean}[];
  control?: AbstractControl;
  readOnly?: boolean;
  uppercase?: boolean;
  maxLength?: number;
  disabled?: boolean;
  hidden?: boolean;

  action?: (param:any) => Observable<any>;
  select?: (item:any) => void;
  file?: File;
  fileType?: string;
}

export interface FormInputGroup {
  label: string;
  inputs: Array<FormInput|FormInputGroup>;
  hidden?: boolean;
}
