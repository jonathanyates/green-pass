import * as actions from "./template.actions";
import {TemplatesState} from "./templates.state";
import {ActionTypes} from "../authentication/authentication.actions";
import {ITemplate} from "../../../../../shared/interfaces/document.interface";

export const initialState: TemplatesState = {
  list: null,
  selected: null
};

export function TemplatesReducer(state: TemplatesState = initialState, action: actions.Actions): TemplatesState {
  switch (action.type) {

    case actions.ActionTypes.LOAD_SUCCESS:
    case actions.ActionTypes.SELECT_LIST_SUCCESS:
      return {
        list: <ITemplate[]>action.payload,
        selected: state.selected
      };

    case actions.ActionTypes.SELECT_TEMPLATE_SUCCESS:
    case actions.ActionTypes.SELECT_COMPANY_TEMPLATE_SUCCESS:
      return {
        list: state.list,
        selected: <ITemplate>action.payload
      };

    case ActionTypes.LOGOUT:
      return initialState;

    default:
      return state;
  }
}

