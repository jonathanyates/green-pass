import {AuthenticationReducer} from "./authentication/authentication.reducer";
import {CompaniesReducer} from "./companies/companies.reducer";
import {ErrorReducer} from "./error/error.reducer";
import {CompanyReducer} from "./company/company.reducer";
import {NavigationReducer} from "./navigation/navigation.reducer";
import {VehiclesReducer} from "./vehicles/vehicle.reducer";
import {UsersReducer} from "./users/user.reducer";
import {DriversReducer} from "./drivers/driver.reducer";
import {TemplatesReducer} from "./templates/template.reducer";
import {DocumentsReducer} from "./documents/document.reducer";
import {PageReducer} from "./page/page.reducer";
import {ProgressReducer} from "./progress/progress.reducer";
import {AddressReducer} from "./address/address.reducer";
import {ComplianceReducer} from "./compliance/compliance.reducer";
import {AlertsReducer} from "./alerts/alert.reducer";
import {ReportsReducer} from "./reports/report.reducer";

export const AppReducers = {
  authentication: AuthenticationReducer,
  navigation: NavigationReducer,
  companies: CompaniesReducer,
  company: CompanyReducer,
  users: UsersReducer,
  vehicles: VehiclesReducer,
  drivers: DriversReducer,
  templates: TemplatesReducer,
  documents: DocumentsReducer,
  address: AddressReducer,
  page: PageReducer,
  error: ErrorReducer,
  progress: ProgressReducer,
  compliance: ComplianceReducer,
  alerts: AlertsReducer,
  reports: ReportsReducer
};
