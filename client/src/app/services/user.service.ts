import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {IUser} from "../../../../shared/interfaces/user.interface";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class UserService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getUsers(id: string): Observable<IUser[]> {

    this.authorize();

    return this.httpClient.get<IUser[]>(this.companiesUrl + id + '/users', this.getOptions())
      .map(res => this.handleResult<IUser[]>(res))
      .catch(this.handleError);
  }

  getUser(companyId: string, userId: string): Observable<IUser> {

    this.authorize();

    return this.httpClient.get<IUser>(this.companiesUrl + companyId + '/users/' + userId, this.getOptions())
      .map(res => this.handleResult<IUser>(res))
      .catch(this.handleError);
  }

  saveUser(user: IUser): Observable<IUser> {

    this.authorize();

    if (user._id) {
      return this.httpClient.put<IUser>(this.companiesUrl + user._companyId + '/users/' + user._id, user, this.getOptions())
        .map(res => this.handleResult<IUser>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IUser>(this.companiesUrl + user._companyId + '/users', user, this.getOptions())
        .map(res => this.handleResult<IUser>(res))
        .catch(this.handleError);
    }
  }

  removeUser(companyId: string,userId: string): Observable<IUser> {
    this.authorize();

    return this.httpClient.delete<IUser>(`${this.companiesUrl}${companyId}/users/${userId}`, this.getOptions())
      .map(response =>  this.handleResult(response))
      .catch(this.handleError);
  }
}
