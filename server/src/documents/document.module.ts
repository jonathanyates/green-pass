import { ContainerModule, interfaces } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { DocumentRouter } from './router/document.router';
import { DocumentController } from './controller/document.controller';
import { DocumentService } from './service/document.service';
import { IDocumentService } from './service/document.service.interface';
import { IDocumentSignatureService } from './interfaces/signature.service.interface';
import { IDocumentController } from './controller/document.controller.interface';
import { LegalesignService } from './legalesign/legalesign.service';

export const documentModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.DocumentRouter).to(DocumentRouter).inSingletonScope();
  bind<IDocumentController>(TYPES.controllers.DocumentController).to(DocumentController).inSingletonScope();
  bind<IDocumentService>(TYPES.services.DocumentService).to(DocumentService).inSingletonScope();
  bind<IDocumentSignatureService>(TYPES.services.DocumentSignatureService).to(LegalesignService).inSingletonScope();
});
