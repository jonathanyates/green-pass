import '../../../shared/extensions/date.extensions';
import { IReportQueryResult, ReportCategory } from '../../../shared/interfaces/report.interface';
import { inject, injectable } from 'inversify';
import { Table } from './report';
import ReactDOMServer = require('react-dom/server');
import React from 'react';
import { TYPES } from '../container/container.types';
import { IReportTransformService } from './report-transform.service';
import { IDynamicList } from '../../../shared/models/dynamic-list.model';
import { FormatDate } from '../../../shared/utils/helpers';

export interface IReportGeneratorService {
  generate(queryResult: IReportQueryResult): Promise<any>;
}

@injectable()
export class ReportGeneratorService implements IReportGeneratorService {
  constructor(@inject(TYPES.services.ReportTransformService) private reportTransformService: IReportTransformService) {}

  generate(reportQueryResult: IReportQueryResult): Promise<any> {
    if (reportQueryResult.result && reportQueryResult.result.length === 0) {
      const items =
        reportQueryResult.reportType.category === ReportCategory.System
          ? 'items'
          : reportQueryResult.reportType.category + 's';

      const date = FormatDate(new Date(), 'EEEE, Do MMMM yyyy');

      return Promise.resolve(`
        <h1>${reportQueryResult.reportType.name} Report</h1>
        <p>Date: ${date}</p>
        <p>There are no ${items} to report.</p>
      `);
    }

    return this.reportTransformService.transform(reportQueryResult).then((dynamicList: IDynamicList) => {
      const content = React.createFactory(Table);
      const html = ReactDOMServer.renderToStaticMarkup(
        content({ name: reportQueryResult.reportType.name, list: dynamicList })
      );
      return Promise.resolve(html);
    });
  }
}
