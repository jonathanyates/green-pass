import {Injectable} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {NavigationEndAction} from "../store/navigation/navigation.actions";

@Injectable()
export class BreadcrumbService {

  private routesFriendlyNames: Map<string, string> = new Map<string, string>();
  private routesFriendlyNamesRegex: Map<string, string> = new Map<string, string>();
  private routesWithCallback: Map<string, (string) => string> = new Map<string, (string) => string>();
  private routesWithCallbackRegex: Map<string, (string) => string> = new Map<string, (string) => string>();
  private hideRoutes: Array<string> = [];
  private hideRoutesRegex: Array<string> = [];

  constructor(
    private router: Router,
    private store: Store<AppState>) {
  }

  listenForNavigation() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map((navigationEnd:NavigationEnd) => navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url)
      .subscribe(url => {
        let breadcrumb = this.generateBreadcrumbTrail([], url);
        this.store.dispatch(new NavigationEndAction(breadcrumb));
        document.body.scrollTop = 0; // ensure pages appear at the top
      });
  }

  private generateBreadcrumbTrail(urls: string[], url: string): string[] {
    if (!this.isRouteHidden(url)) {
      //Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
      urls.unshift(url);
    }

    if (url.lastIndexOf('/') > 0) {
      return this.generateBreadcrumbTrail(urls, url.substr(0, url.lastIndexOf('/'))); //Find last '/' and add everything before it as a parent route
    }

    return urls;
  }

  /**
   * Specify a friendly name for the corresponding route.
   *
   * @param route
   * @param name
   */
  addFriendlyNameForRoute(route: string, name: string): void {
    this.routesFriendlyNames.set(route, name);
  }

  /**
   * Specify a friendly name for the corresponding route matching a regular expression.
   *
   * @param route
   * @param name
   */
  addFriendlyNameForRouteRegex(routeRegex: string, name: string): void {
    this.routesFriendlyNamesRegex.set(routeRegex, name);
  }

  /**
   * Specify a callback for the corresponding route.
   * When a mathing url is navigatedd to, the callback function is invoked to get the name to be displayed in the breadcrumb.
   */
  addCallbackForRoute(route: string, callback: (x:string) => string): void {
    this.routesWithCallback.set(route, callback);
  }

  /**
   * Specify a callback for the corresponding route matching a regular expression.
   * When a mathing url is navigatedd to, the callback function is invoked to get the name to be displayed in the breadcrumb.
   */
  addCallbackForRouteRegex(routeRegex: string, callback: (x:string) => string): void {
    this.routesWithCallbackRegex.set(routeRegex, callback);
  }

  /**
   * Show the friendly name for a given route (url). If no match is found the url (without the leading '/') is shown.
   *
   * @param route
   * @returns {*}
   */
  getFriendlyNameForRoute(route: string): string {
    let name;
    let routeEnd = route.substr(route.lastIndexOf('/')+1, route.length);

    this.routesFriendlyNames.forEach((value, key, map) => {
      if (key === route) {
        name = value;
      }
    });

    this.routesFriendlyNamesRegex.forEach((value, key, map) => {
      if (new RegExp(key).exec(route)) {
        name = value;
      }
    });

    this.routesWithCallback.forEach((value, key, map) => {
      if (key === route) {
        name = value(routeEnd);
      }
    });

    this.routesWithCallbackRegex.forEach((value, key, map) => {
      if (new RegExp(key).exec(route)) {
        name = value(routeEnd);
      }
    });

    return name ? name : routeEnd;
  }

  /**
   * Specify a route (url) that should not be shown in the breadcrumb.
   */
  hideRoute(route: string): void {
    if (!(this.hideRoutes.indexOf(route) > -1)) {
      this.hideRoutes.push(route);
    }
  }

  /**
   * Specify a route (url) regular expression that should not be shown in the breadcrumb.
   */
  hideRouteRegex(routeRegex: string): void {
    if (!(this.hideRoutesRegex.indexOf(routeRegex) > -1)) {
      this.hideRoutesRegex.push(routeRegex);
    }
  }

  /**
   * Returns true if a route should be hidden.
   */
  isRouteHidden(route: string): boolean {
    let hide = this.hideRoutes.indexOf(route) > -1;

    this.hideRoutesRegex.forEach((value) => {
      if (new RegExp(value).exec(route)) {
        hide = true;
      }
    });

    return hide;
  }
}
