import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IVehicle } from '../../../../../../shared/interfaces/vehicle.interface';
import { FormatDate } from '../../../../../../shared/utils/helpers';
import { IListGroupItem } from '../../shared/components/dynamic-list-group/dynamic-list-group';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BusyAction } from '../../../store/progress/progress.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.states';
import { VehicleService } from 'app/services/vehicle.service';
import { SelectCompany } from '../../../store/company/company.selectors';
import { ICompany } from '../../../../../../shared/interfaces/company.interface';

@Component({
  selector: 'gp-vehicle-dvla-details',
  templateUrl: './vehicle-dvla-details.component.html',
})
export class VehicleDvlaDetailsComponent implements OnInit {
  details: IListGroupItem[];
  vehicle: IVehicle;
  company: ICompany;
  error: string;
  private subscription: Subscription;
  private companySubscription: Subscription;

  @Input() set searchTerm$(searchTerm$: Observable<string>) {
    if (searchTerm$) {
      this.subscribe(searchTerm$);
    }
  }
  @Output() vehicleChanged: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();

  constructor(private store: Store<AppState>, private vehicleService: VehicleService) {}

  ngOnInit() {
    this.companySubscription = this.store.let(SelectCompany()).subscribe((company) => {
      this.company = company;
    });
  }

  private subscribe(searchTerm$: Observable<string>) {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = searchTerm$
      .do((term) => {
        if (term == '') {
          this.error = null;
        }
      })
      .filter((term) => term != null && term.trim() != '')
      .distinctUntilChanged()
      .do((term) => {
        this.vehicle = null;
        this.error = null;
        this.store.dispatch(new BusyAction());
      })
      .switchMap((searchTerm) => {
        return this.store.let(SelectCompany()).switchMap((company) => {
          return this.vehicleService.dvlaVehicleCheck(company._id, searchTerm).catch((error) => {
            console.log(error);
            this.store.dispatch(new BusyAction(false));
            this.error = error.message;
            //this.error = 'Unable to complete Vehicle Check. Please try again.';
            return Observable.empty();
          });
        });
      })
      .subscribe(
        (vehicle: IVehicle) => {
          this.store.dispatch(new BusyAction(false));
          if (vehicle) {
            this.vehicle = vehicle;
            this.setDetails(vehicle);
            this.vehicleChanged.emit(vehicle);
          }
        },
        (error) => {
          console.log(error);
          this.store.dispatch(new BusyAction(false));
          this.error = error.message;
          //this.error = 'Unable to complete Vehicle Check. Please try again.';
        },
        () => {
          console.log('Completed');
        }
      );
  }

  setDetails(vehicle: IVehicle) {
    this.details = [
      { title: 'Registration Number', value: vehicle.registrationNumber },
      { title: 'Make', value: vehicle.make },
      {
        title: 'Date of first registration',
        value: vehicle ? FormatDate(vehicle.dateOfFirstRegistration, 'dd MMMM yyyy') : null,
      },
      { title: 'Year of manufacture', value: vehicle ? vehicle.yearOfManufacture : null },
      { title: 'Cylinder capacity (cc)', value: vehicle ? vehicle.cylinderCapacityCc : null },
      { title: 'CO₂ Emissions', value: vehicle ? vehicle.co2Emissions : null },
      { title: 'Fuel type', value: vehicle ? vehicle.fuelType : null },
      { title: 'Export marker', value: vehicle ? vehicle.make : null },
      { title: 'Vehicle status', value: vehicle ? vehicle.vehicleStatus : null },
      { title: 'Colour', value: vehicle ? vehicle.vehicleColour : null },
      { title: 'Vehicle type approval', value: vehicle ? vehicle.vehicleTypeApproval : null },
      { title: 'Wheelplan', value: vehicle ? vehicle.wheelplan : null },
      { title: 'Revenue weight', value: vehicle ? vehicle.revenueWeight : null },
    ];
  }
}
