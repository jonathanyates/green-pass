import {IDriver} from "../../../shared/interfaces/driver.interface";
import {inject, injectable} from "inversify";
import {TYPES} from "../container/container.types";
import {IRoadMarqueService} from "./roadmarque.service";
import {IRoadMarqueDriver} from "../../../shared/interfaces/roadmarque-driver.interface";
import {assessmentMapper} from "./assessment.mapper";
import {AssessmentSummary} from "../../../shared/models/assessment-summary.model";
import {IAssessment} from "../../../shared/interfaces/assessment.interface";
import {ServerError} from "../shared/errors";
import codes from "../shared/error.codes";
import logger = require('winston');
import {IDriverRepository} from "../drivers/driver.repository";
import serverConfig from "../server/server.config";

export interface IAssessmentService {
  updateAssessments(driver: IDriver): Promise<IDriver>;
}

@injectable()
export class AssessmentService implements IAssessmentService {

  constructor(
    @inject(TYPES.services.RoadMarqueService) private roadMarqueService: IRoadMarqueService,
    @inject(TYPES.repositories.DriverRepository) private driverRepository: IDriverRepository) {
  }

  updateAssessments = (driver: IDriver): Promise<IDriver> => {

    logger.debug(`Updating driver assessments for driver ${driver._user.username}...`);
    logger.debug(`Getting driver from RoadMarque service...`);

    return this.roadMarqueService.getDrivers({RefID: driver._id.toString()})
      .then((roadMarqueDriver: IRoadMarqueDriver) => {

        const group = serverConfig.roadMarque.driverGroup;

        if (roadMarqueDriver) {

          logger.debug(`Road Marque Driver found.`);
          logger.debug(`Updating Road Marque Driver...`);

          return this.roadMarqueService.updateDriver(roadMarqueDriver.basicDetails.userId, driver, group)
            .then(result => {
              if (result.resultMessage.success == 'false') {
                logger.error('updateAssessments:roadMarqueService.updateDriver: Failed to update driver. Driver RefId' + driver._id.toString());
              }

              const assessment: IAssessment = assessmentMapper(roadMarqueDriver, driver);

              // if assessments are complete and the next assessment date is <= the completed date
              // then set the next assessment date to 1 year from the invite date.
              if (driver.nextAssessmentDate != null && assessment.complete &&
                assessment.completedDate.getDayDiff(driver.nextAssessmentDate) <= 0) {

                driver.nextAssessmentDate = assessment.completedDate.toDate().addMonths(12);
                logger.debug(`Set Next Assessment Date to ${driver.nextAssessmentDate.toString()}`);
              }

              // if we haven't set the next assessment date or we have reached the next assessment date
              // then do an assessment invite
              if (driver.nextAssessmentDate == null ||
                (assessment.complete && Date.prototype.getToday().getDayDiff(driver.nextAssessmentDate) <= 0)) {
                return this.assessmentInvite(driver);
              }

              return this.updateDriverAssessment(driver, assessment);
            })
            .catch(error => {
              logger.error('Error updating assessments: ' + error.message);
              return Promise.reject(ServerError(error.message, codes.INTERNAL_SERVER_ERROR));
            })
        }

        logger.debug(`Adding new Driver to Road Marque...`);

        return this.roadMarqueService.addDriver(driver, group)
          .then(result => {
            if (result.resultMessage.success == 'false') {
              throw ServerError('Failed to add driver', codes.INTERNAL_SERVER_ERROR);
            }

            logger.debug(`Successfully added Road Marque Driver`);
            // Always do an assessment invite for a new driver
            return this.assessmentInvite(driver);
          })
      })
      .then(driver => this.driverRepository.update(driver))
      .catch(error => {
        console.log(error);
        return Promise.reject(ServerError(error.message, codes.INTERNAL_SERVER_ERROR));
      })
  };

  private assessmentInvite = (driver: IDriver): Promise<IDriver> => {
    logger.debug(`Inviting driver to next assessment...`);
    return this.roadMarqueService.AssessmentInvite(driver._id.toString())
      .then(invite => {
        if (invite.resultMessage.success == 'false') {
          logger.error('assessmentInvite:Failed to invite driver to assessment. Driver RefId ' + driver._id.toString());
        }
        logger.debug(`Successfully invited driver to assessment`);
        return this.roadMarqueService.getDrivers({RefID: driver._id.toString()})
      })
      .then((roadMarqueDriver: IRoadMarqueDriver) => {
        // if the next assessment date has never been set then set it to the invite date
        if (driver.nextAssessmentDate == null) {
          driver.nextAssessmentDate = roadMarqueDriver.aptitudeAssessments.assessmentInvite.assessmentInviteDate;
          logger.debug(`Next Assessment Date set to ${driver.nextAssessmentDate.toString()}`);
        }
        return roadMarqueDriver;
      })
      .then((roadMarqueDriver: IRoadMarqueDriver) => {
        const assessment: IAssessment = assessmentMapper(roadMarqueDriver, driver);
        return this.updateDriverAssessment(driver, assessment);
      })
  };

  // check if the latest roadmarque assessment start date is later than the last driver assessment start date
  // if they are the same then update the driver assessment with the latest roadmarque assessment
  // if the start date is later then add the assessment to the driver assessments.
  private updateDriverAssessment = (driver: IDriver, assessment: IAssessment): IDriver => {
    if (assessment) {
      assessment.lastUpdatedDate = new Date();
      driver.lastAssessmentUpdatedDate = assessment.lastUpdatedDate;
      logger.debug(`Updating Driver Assessments. Last Updated ${assessment.lastUpdatedDate.toString()}`);

      if (driver.assessments && driver.assessments.length > 0) {
        const lastAssessment = driver.assessments.slice(-1)[0];
        const rmAssessmentSummary = new AssessmentSummary(assessment);
        const assessmentSummary = new AssessmentSummary(lastAssessment);

        if (rmAssessmentSummary.inviteDate > assessmentSummary.startDate) {
          driver.assessments.push(assessment);
          logger.debug(`Added new Driver Assessment.`);
        } else {
          // ensure we include on road assessments in the update if there are any
          driver.assessments[driver.assessments.length - 1] = assessment;
          logger.debug(`Updated Driver Assessment.`);
        }
      } else {
        if (!driver.assessments) {
          driver.assessments = [];
        }
        driver.assessments.push(assessment);
        logger.debug(`Added new Driver Assessment.`);
      }

      logger.debug(JSON.stringify(assessment, null, 2));
    }

    return driver;
  }
}