import {injectable, inject} from "inversify";
import {IDriver, IDriverVehicleMap} from "../../../shared/interfaces/driver.interface";
import {IDbContext} from "../db/db.context.interface";
import {TYPES} from "../container/container.types";
import {IVehicle} from "../../../shared/interfaces/vehicle.interface";

export interface IDriverVehicleMapRepository {
  get(driverId: string, vehicleId: string): Promise<IDriverVehicleMap>;
  add(driverId: string, vehicleId: string): Promise<IDriverVehicleMap>;
  getDriverVehicles(driverId: string): Promise<IVehicle[]>;
  getVehicleDrivers(vehicleId: string): Promise<IDriver[]>;
  remove(driverId: string, vehicleId: string): Promise<void>;
}

@injectable()
export class DriverVehicleMapRepository implements IDriverVehicleMapRepository {

  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {
  }

  get(driverId: string, vehicleId: string): Promise<IDriverVehicleMap> {
    return <Promise<IDriverVehicleMap>>this.dbContext.driverVehicleMap
      .findOne({ driverId: driverId, vehicleId: vehicleId })
      .lean()
      .exec();
  }

  add(driverId: string, vehicleId: string): Promise<IDriverVehicleMap> {
    const model = new this.dbContext.driverVehicleMap({ driverId: driverId, vehicleId: vehicleId });
    return model.save();
  }

  remove(driverId: string, vehicleId: string): Promise<void> {
    return this.dbContext.driverVehicleMap.remove({ driverId: driverId, vehicleId: vehicleId })
      .exec();
  }

  getDriverVehicles(driverId: string): Promise<IVehicle[]> {
    return this.dbContext.driverVehicleMap
      .find({driverId: driverId})
      .populate('vehicleId')
      .lean()
      .exec()
      .then((driverVehicles: IDriverVehicleMap[]) => <IVehicle[]>driverVehicles.map(driverVehicle => driverVehicle.vehicleId))
  }

  getVehicleDrivers(vehicleId: string): Promise<IDriver[]> {
    return this.dbContext.driverVehicleMap
      .find({vehicleId: vehicleId})
      .populate({
        path: 'driverId',
        populate: {path: '_user'}
      })
      .lean()
      .exec()
      .then((driverVehicles: IDriverVehicleMap[]) => <IDriver[]>driverVehicles.map(driverVehicle => driverVehicle.driverId))
      .then(drivers => {
        return Promise.all(drivers.map(driver => {
          return this.getDriverVehicles(driver._id)
            .then(vehicles => {
              driver.vehicles = vehicles;
              return driver;
            })
        }))
      })
  }

}