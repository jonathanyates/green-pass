import { Paths } from '../../../shared/constants';
import { AppState } from '../../../store/app.states';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {LoadReportTypesAction} from "../../../store/reports/report.actions";
import {SelectReportTypes} from "../../../store/reports/report.selectors";
import {IReportType} from "../../../../../../shared/interfaces/report.interface";

@Component({
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class ReportTypeListComponent implements OnInit, OnDestroy {

  list: IDynamicList;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new LoadReportTypesAction());

    this.list = {
      columns: [
        { header: 'Name', field: 'name' },
        { header: 'Category', field: 'category' },
        { header: 'Query Name', field: 'query'},
      ]
    };

    this.subscription = this.store.let(SelectReportTypes())
      .filter(reportTypes => reportTypes != null)
      .subscribe((reportTypes:IReportType[]) => {
        this.list.items = reportTypes;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  select(reportType:IReportType) {
    this.router.navigate(['./', reportType._id], { relativeTo: this.route });
  }

  add() {
    this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
