import 'reflect-metadata';
import { IRoadMarqueService, RoadMarqueService } from '../src/assessments/roadmarque.service';
import { vehicleModule } from '../src/vehicles/vehicle.module';
import { DbContext } from '../src/db/db.context';
import { TYPES } from '../src/container/container.types';
import { IDbContext } from '../src/db/db.context.interface';
import { Container } from 'inversify';
import { companyModule } from '../src/companies/company.module';
import { driverModule } from '../src/drivers/driver.module';
import { userModule } from '../src/users/user.module';
import { dvlaModule } from '../src/dvla/dvla.module';
import { documentModule } from '../src/documents/document.module';
import { assessmentModule } from '../src/assessments/assessment.module';
import { toCamel } from '../src/shared/object.extensions';

const container = new Container();
container.bind<IDbContext>(TYPES.db.DbContext).to(DbContext).inSingletonScope();
container.load(companyModule, driverModule, userModule, dvlaModule, documentModule, vehicleModule, assessmentModule);

describe.skip('RoadMarque Service', function () {
  describe.skip('getDrivers', function () {
    it('gets drivers', () => {
      const service = container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);

      return service.getDrivers().then((result) => {
        expect(result).toBeDefined();
      });
    });
  });

  describe.skip('getLastRetrieveUserDetailsDate', function () {
    it('get Last Retrieve User Details Date', () => {
      const service = container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);

      return service.getLastRetrieveUserDetailsDate().then((result) => {
        expect(result).toBeDefined();
        expect(result.success).toBeTruthy();
        expect(result.lastRun).toBeDefined();
      });
    });
  });

  describe.skip('getUserDetails', function () {
    it('get User Details', () => {
      const service = container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);
      const userId = 0;

      return service.getUserDetails(userId).then((result) => {
        expect(result).toBeDefined();
        expect(result.userID).toEqual(userId);
        expect(result.refID).toBeDefined();
        expect(result.password).toBeDefined();
      });
    });
  });

  describe.skip('doLogin', function () {
    it('do Login', () => {
      const service = container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);
      const refId = 'YATES607198JP9LK';

      return service.doLogin(refId).then((result) => {
        expect(result).toBeDefined();
        expect(result.success).toEqual('true');
        expect(result.loginUrl).toBeDefined();
      });
    });
  });

  describe.skip('getCustomers', function () {
    it('get Customers', () => {
      const service: RoadMarqueService = <RoadMarqueService>(
        container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService)
      );

      return service.getCustomers().then((result) => {
        return expect(result).toBeDefined();
      });
    });
  });

  describe.skip('toCamel', function () {
    it('to Camelcase properties', () => {
      const date = new Date();

      const obj = {
        Foo: 'bar',
        Prop: {
          Prop_1: date,
          Prop_2: [
            {
              Item1Prop_1: 'item1prop1value',
              Item1Prop_2: 'item1prop2value',
            },
            {
              Item2Prop_1: 'item2prop1value',
              Item2Prop_2: 'item2prop2value',
            },
          ],
        },
      };

      let result = toCamel(obj);

      const expected = JSON.stringify({
        foo: 'bar',
        prop: {
          prop1: date,
          prop2: [
            {
              item1Prop1: 'item1prop1value',
              item1Prop2: 'item1prop2value',
            },
            {
              item2Prop1: 'item2prop1value',
              item2Prop2: 'item2prop2value',
            },
          ],
        },
      });

      result = JSON.stringify(result);

      console.log(result);
      console.log(expected);

      expect(result).toEqual(expected);
    });
  });
});
