import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {IFleetInsurance, IInsurance} from "../../../../../../../shared/interfaces/common.interface";
import {SelectCompanyState} from "../../../../store/company/company.selectors";
import {ICompany} from "../../../../../../../shared/interfaces/company.interface";
import {SaveCompanyInsuranceAction} from "../../../../store/company/company-insurance.actions";

@Component({
  selector: 'gp-company-insurance-input',
  template: `
    <gp-fileRecord-input [fileRecord]="insurance"
                        [heading]="'Insurance details'"
                        [fileHeading]="'Insurance Certificate'" 
                        (submit)="save($event)" (cancel)="cancel()">
    </gp-fileRecord-input>
  `
})
export class CompanyInsuranceInputComponent implements OnInit, OnDestroy {

  insurance: IFleetInsurance;
  private company: ICompany;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectCompanyState())
      .subscribe(state => {
        this.company = state.company;
        this.insurance = state.selectedInsurance
          ? state.selectedInsurance
          : <IFleetInsurance>{ validFrom: null, validTo: null };
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(insurance: IInsurance) {
    this.store.dispatch(new SaveCompanyInsuranceAction(this.company, insurance));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
