import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./company.actions";
import {CompaniesService} from "../../services/companies.service";
import {Router} from "@angular/router";
import {HandleErrorAction} from "../error/error.actions";
import {AppState} from "../app.states";
import {Store} from "@ngrx/store";
import {Paths} from "../../shared/constants";
import {SelectCompany, SelectCompanyState} from "./company.selectors";
import {BusyAction} from "../progress/progress.actions";
import {messenger} from "../../services/messenger";
import {Messages} from "../app.messages";
import * as CompanyInsuranceActions from './company-insurance.actions';
import {FileService} from "../../services/file.service";

@Injectable()
export class CompanyEffects {

  constructor(
    private companiesService:CompaniesService,
    private fileService: FileService,
    private actions$:Actions,
    private router: Router,
    private store: Store<AppState>) {
  }
  // Company Effects

  @Effect() selectCompany$ = this.actions$
    .ofType(actions.ActionTypes.SELECT)
    .map((action: actions.SelectCompanyAction) => action.payload)
    .switchMap(payload => this.store.let(SelectCompany()).take(1)
      .map(company => company && company._id === payload
        ? new actions.SelectCompanySuccessAction(company)
        : new actions.LoadCompanyAction(payload))
    );

  @Effect() loadCompany$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .map((action: actions.LoadCompanyAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.companiesService.getCompany(payload)
      .map(company => new actions.LoadCompanySuccessAction(company))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveCompany$ = this.actions$
    .ofType(actions.ActionTypes.SAVE)
    .map((action: actions.SaveCompanyAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(company => this.companiesService.saveCompany(company)
      .do(company => this.router.navigate([Paths.Company, company._id, Paths.Details]))
      .map(company => new actions.SaveCompanySuccessAction(company))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        messenger.publish(Messages.SaveError, error);
        return Observable.empty();
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // @Effect() saveCompanySuccess$ = this.actions$
  //   .ofType(actions.ActionTypes.SAVE_SUCCESS)
  //   .map((action: actions.SaveCompanySuccessAction) => action.payload)
  //   .map(company => new LoadCompaniesAction());

  // Depot

  @Effect() saveDepot$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_DEPOT)
    .map((action: actions.SaveDepotAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(depot => this.store.let(SelectCompany()).take(1))
    .switchMap(company => this.companiesService.saveCompany(company)
      .do(company => this.router.navigate([Paths.Company,  company._id, 'depots']))
      .map(company => new actions.SaveCompanySuccessAction(company))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // Supplier

  @Effect() saveSupplier$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_SUPPLIER)
    .map((action : actions.SaveSupplierAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(supplier => this.store.let(SelectCompanyState())
      .map(state => state.company).take(1))
    .switchMap(company => this.companiesService.saveCompany(company)
      .do(company => this.router.navigate([Paths.Company,  company._id, 'suppliers']))
      .map(company => new actions.SaveCompanySuccessAction(company))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // Alert Types

  @Effect() saveAlertTypes$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_ALERT_TYPES)
    .map((action: actions.SaveCompanyAlertTypesAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(supplier => this.store.let(SelectCompanyState())
      .map(state => state.company).take(1))
    .switchMap(company => this.companiesService.saveCompany(company)
      .map(company => new actions.SaveCompanySuccessAction(company))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // Company Insurance

  @Effect() selectCompanyInsurance$ = this.actions$
    .ofType(CompanyInsuranceActions.ActionTypes.SELECT_COMPANY_INSURANCE)
    .map((action: CompanyInsuranceActions.SelectCompanyInsuranceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectCompany())
      .filter(company => company && company._id === payload.companyId).take(1)
      .switchMap(company => {
        let insurance = company.fleetInsuranceHistory.find(insurance => insurance._id === payload.insuranceId);
        if (insurance == null || insurance.file || insurance.filename == null) {
          return Observable.of(new CompanyInsuranceActions.SelectCompanyInsuranceSuccessAction(insurance))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(company._id, insurance.filename)
          .map(file => Object.assign({}, insurance, { file: file }))
          .map(insurance => new CompanyInsuranceActions.SelectCompanyInsuranceSuccessAction(insurance))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveCompanyInsurance$ = this.actions$
    .ofType(CompanyInsuranceActions.ActionTypes.SAVE_COMPANY_INSURANCE)
    .map((action: CompanyInsuranceActions.SaveCompanyInsuranceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.companiesService.saveCompanyInsurance(payload.company, payload.insurance)
      .do(insurance => this.router.navigate([Paths.Company, payload.company._id, Paths.Insurance, insurance._id]))
      .map(insurance => new CompanyInsuranceActions.SaveCompanyInsuranceSuccessAction(payload.company, insurance))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

}
