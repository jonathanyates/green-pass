import ConnectRoles = require('connect-roles');
import {IRoles} from "./authorization.middleware";

export interface IAuthorizationMiddleware {
  user: ConnectRoles;
  roles: IRoles;
}