import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AuthorizationService} from "../../../services/authorization.service";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {ServerError} from "app/shared/errors/server.error";
import HTTP_STATUS_CODES from "../../../shared/errors/status-codes.enum";
import {AppState} from "../../../store/app.states";

@Injectable()
export class ResetPasswordResolve {

  constructor(
    private authService: AuthorizationService,
    private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    let token = route.params['token'];

    if (!token) {
      return Observable.of(false);
    }

    return this.authService.verifyResetToken(token)
      .map(valid => {
          if (!valid) {
            this.store.dispatch(new HandleErrorAction(this.getError()));
            return false;
          }
          return true;
        })
      .catch(error => {
          this.store.dispatch(new HandleErrorAction(error));
          return Observable.of(false)
        }).take(1);
  }

  getError() {
    return new ServerError('Unable to reset password. The password reset token is invalid or has expired',
      HTTP_STATUS_CODES.BAD_REQUEST);
  }
}
