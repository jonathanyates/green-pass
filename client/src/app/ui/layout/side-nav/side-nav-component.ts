import {Component, Input} from '@angular/core';
import {NavigationLink} from "../../../store/navigation/navigation.model";
import {Router} from "@angular/router";
import {IUser} from "../../../../../../shared/interfaces/user.interface";

declare let $:any;

@Component({
  selector: 'gp-side-nav',
  templateUrl: 'side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent {

  @Input() loggedIn: boolean;
  @Input() user: IUser;
  @Input() links: NavigationLink[];

  constructor(private router: Router) {
  }

  navigateTo(url: string): void {
    this.router.navigateByUrl(url);
  }

}
