import {ICompanySettings} from "./company-settings.interface";

export const DEFAULT_DRIVER_REMINDER_CRON = '0 9 * * 1';

export const defaultSettingsFactory = (): ICompanySettings => {
  return {
    vehicles: {
      grey: {
        exclusions: {
          service: true,
          inspection: true,
          checks: true
        }
      }
    },
    drivers: {
      allowInput: true,
      references: false,
      reminders: {
        sendEmails: false,
        cron: DEFAULT_DRIVER_REMINDER_CRON
      },
      licenceChecks: {
        checkPeriods: [
          {
            noOfPoints: 9,
            months: 3
          },
          {
            noOfPoints: 6,
            months: 6
          }
        ]
      },
      includeFaw: false,
      includeDbs: false
    },
  }
};