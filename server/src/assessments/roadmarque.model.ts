export interface IRMLastUse {
  lastRun: string,
  success: boolean
}

export interface IRMUserRecord {
  refID: string,
  userID: number,
  success: boolean,
  firstname: string,
  surname: string,
  password: string,
  emailAddress: string
}

export interface IRMDeepLogin {
  loginUrl: string,
  success: boolean
}