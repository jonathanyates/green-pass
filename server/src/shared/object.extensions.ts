import _ = require('lodash');

// Modified from here with addition of supporting arrays
// https://github.com/odynvolk/map-keys-deep-lodash/blob/master/index.js
export const mapKeysDeep = (obj, cb) => {

  if (_.isUndefined(obj)) {
    throw new Error(`toCamel expects an object but got ${typeof obj}`);
  }

  if (Array.isArray(obj)) {
    return obj.map(item => toCamel(item))
  }

  obj = _.mapKeys(obj, cb);
  const res = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const val = obj[key];

      if (Array.isArray(val)){
        for(let i = 0; i < val.length; i++) {
          res[key] = val.map(item => toCamel(item))
        }
      } else if (_.isObject(val) && !_.isDate(val)) {
        res[key] = toCamel(val);
      } else {
        res[key] = val;
      }
    }
  }

  return res;
};

export const toCamel = (obj) => {
  return mapKeysDeep(obj, (value:any, key:string) => _.camelCase(key))
};