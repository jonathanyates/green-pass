import {FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {FormGroup} from '@angular/forms';
import {IVehicle} from '../../../../../../shared/interfaces/vehicle.interface';
import {RegExpPatterns} from '../../../../../../shared/constants';
import {IDriver} from '../../../../../../shared/interfaces/driver.interface';

export class VehicleInputMapper {

  static mapFrom(form: FormGroup, driver?: IDriver): IVehicle {

    const vehicle: any = {};
    vehicle.category = form.controls['category'].value;

    if (!driver || !driver.ownedVehicle) {
      vehicle.type = form.controls['type'].value;
    }

    const mileage = form.controls['mileage'].value;
    vehicle.annualMileage = mileage ? mileage : 12000;

    if (form.controls.hasOwnProperty('model')) {
      vehicle.model = form.controls['model'].value;
    }

    if (form.controls.hasOwnProperty('make')) {
      vehicle.make = form.controls['make'].value;
    }

    return vehicle;
  }

  static mapTo(vehicle: IVehicle, driver?: IDriver): FormInputGroup[] {

    const categories = [
      { key: 'A1', value: 'A1 - Light motorcycles', selected: false },
      { key: 'A', value: 'A - Motorcycles', selected: false },
      { key: 'B1', value: 'B1 - Motor tricycles', selected: false },
      { key: 'B', value: 'B - Cars', selected: false },
      { key: 'B+E', value: 'B+E - Cars with trailers', selected: false },
      { key: 'C1', value: 'C1 - Medium Sized Vehicles', selected: false },
      { key: 'C1+E', value: 'C1+E - Medium Sized vehicles with trailers', selected: false },
      { key: 'C', value: 'C - Large Vehicles', selected: false },
      { key: 'C+E', value: 'C+E - Large Vehicles with trailers', selected: false },
      { key: 'D1', value: 'D1 - Minibuses', selected: false },
      { key: 'D1+E', value: 'D1+E - Minibuses with trailers', selected: false },
      { key: 'D', value: 'D - Buses', selected: false },
      { key: 'D+E', value: 'D+E - Buses with trailers', selected: false }
    ];

    if (vehicle.category) {
      const category = categories.find(cat => cat.key === vehicle.category);
      if (category) {
        category.selected = true;
      }
    } else {
      const category = categories.find(cat => cat.key === 'B');
      category.selected = true;
    }

    const types = [
      { key: 'Grey', value: 'Grey', selected: vehicle.type === 'Grey' },
      { key: 'Fleet', value: 'Fleet', selected: vehicle.type === 'Fleet' }
    ];

    if (vehicle.type) {
      const type = types.find(vehicleType => vehicleType.key === vehicle.type);
      if (type) {
        type.selected = true;
      }
    } else {
      types[0].selected = true;
      vehicle.type = types[0].value;
    }

    const group: FormInputGroup[] = [
      {
        label: vehicle._id ? vehicle.registrationNumber : 'Please complete vehicle details',
        inputs: [
          { id: 'model', label: 'Model', type: 'text', required: true, autoFocus: false, value: vehicle.model },
          { id: 'mileage', label: 'Annual mileage', type: 'text', required: false, autoFocus: false, value: vehicle.annualMileage,
            regex: RegExpPatterns.Number },
          { id: 'category', label: 'Category', type: 'dropdown', required: true, autoFocus: false, value: vehicle.category,
            gridClass: 'col-md-6', options: categories },
          { id: 'type', label: 'Type', type: 'radio', required: true, autoFocus: false,
            hidden: driver && driver.ownedVehicle, options: types, value: vehicle.type, }
        ]
      }
    ];

    if (!vehicle.make || vehicle.make === 'Unknown') {
      group[0].inputs.unshift(
        { id: 'make', label: 'Make', type: 'text', required: true, autoFocus: false, value: null });
    }

    return group;
  }



}
