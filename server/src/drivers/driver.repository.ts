import logger = require('winston');
import { injectable, inject } from 'inversify';
import { IRepository } from '../db/db.repository';
import { IDriver } from '../../../shared/interfaces/driver.interface';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { IDriverModel } from './driver.schema';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IVehicle } from '../../../shared/interfaces/vehicle.interface';
import { DriverCompliance } from '../../../shared/models/driver-compliance.model';
import { IDriverVehicleMapRepository } from './driver-vehicle-map.repository';
import { VehicleCompliance } from '../../../shared/models/vehicle-compliance.model';
import { ICompanyRepository } from '../companies/company.repository';
import { LeanDocument } from 'mongoose';

export interface IDriverRepository extends IRepository<IDriver> {
  count(criteria: any);
  populateVehicles(driverModel: LeanDocument<IDriver>): Promise<LeanDocument<IDriver>>;
}

@injectable()
export class DriverRepository implements IDriverRepository {
  private getAggregations(criteria: any): any[] {
    return [
      {
        $lookup: {
          from: 'users',
          localField: '_user',
          foreignField: '_id',
          as: '_user',
        },
      },
      { $unwind: '$_user' },
      { $addFields: { name: { $concat: ['$_user.forename', ' ', '$_user.surname'] } } },
      { $match: criteria },
    ];
  }

  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.repositories.DriverVehicleMapRepository)
    private driverVehicleMapRepository: IDriverVehicleMapRepository,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  getAll(): Promise<IDriver[]> {
    return <Promise<IDriver[]>>this.dbContext.driver
      .find({ removed: false })
      .populate({
        path: '_user',
        select: 'title forename surname email username',
      })
      .lean()
      .exec()
      .then((drivers) => Promise.all(drivers.map((driver) => this.populateVehicles(driver))))
      .then((drivers: IDriver[]) => this.addCompliances(drivers));
  }

  get(id: string): Promise<IDriver> {
    return this.dbContext.driver
      .findById(id)
      .populate('_user')
      .lean()
      .exec()
      .then((driver) => this.populateVehicles(driver))
      .then((driver: IDriver) => this.addCompliance(driver));
  }

  findOne(criteria: any): Promise<IDriver> {
    criteria.removed = false;
    return <Promise<IDriver>>this.dbContext.driver
      .findOne(criteria)
      .populate({
        path: '_user',
        select: 'title forename surname email username',
      })
      .lean()
      .exec()
      .then((driver) => this.populateVehicles(driver))
      .then((driver: IDriver) => this.addCompliance(driver));
  }

  count(criteria: any) {
    criteria.removed = false;

    return this.dbContext.driver.aggregate(this.getAggregations(criteria).concat([{ $count: 'count' }])).exec();
  }

  find(criteria: any, page?: number, pageSize = 10): Promise<IDriver[]> {
    criteria.removed = false;

    if (page) {
      const skip = (page - 1) * pageSize;

      return this.dbContext.driver
        .aggregate(this.getAggregations(criteria).concat([{ $skip: skip }, { $limit: pageSize }]))
        .exec()
        .then((drivers: IDriver[]) => Promise.all(drivers.map((driver) => this.populateVehicles(driver))))
        .then((drivers: IDriver[]) => this.addCompliances(drivers));
    }

    return <Promise<IDriver[]>>this.dbContext.driver
      .find(criteria)
      .populate({
        path: '_user',
        select: 'title forename surname email username',
      })
      .lean()
      .exec()
      .then((drivers) => Promise.all(drivers.map((driver) => this.populateVehicles(driver))))
      .then((drivers: IDriver[]) => this.addCompliances(drivers));
  }

  populateVehicles(driverModel: LeanDocument<IDriver>): Promise<LeanDocument<IDriver>> {
    if (!driverModel || !driverModel._id) {
      return Promise.resolve(driverModel);
    }

    return Promise.all([
      this.driverVehicleMapRepository.getDriverVehicles(driverModel._id),
      this.companyRepository.get(driverModel._companyId),
    ]).then((results) => {
      const vehicles: IVehicle[] = results[0];
      const company = results[1];
      vehicles.forEach((vehicle) => (vehicle.compliance = new VehicleCompliance(vehicle, company.settings)));
      driverModel.vehicles = vehicles;
      return driverModel;
    });
  }

  private addCompliance(driver: IDriver): Promise<IDriver> {
    if (driver) {
      return this.companyRepository.get(driver._companyId).then((company) => {
        driver.compliance = new DriverCompliance(driver, company);
        return driver;
      });
    }
    return Promise.resolve(null);
  }

  private addCompliances(drivers: IDriver[]): Promise<IDriver[]> {
    if (drivers) {
      return Promise.all(drivers.map((driver) => this.addCompliance(driver)));
    }

    return Promise.resolve([]);
  }

  add<T extends IDriver>(driver: IDriver): Promise<IDriver> {
    const model = new this.dbContext.driver(driver);

    return model
      .save()
      .then((driver) => this.addCompliance(driver))
      .then((driver) => {
        logger.info(`Driver ${driver._id} added.`);
        return this.get(driver._id).then((driver) =>
          this.messageService
            .send(queueNames.companyComplianceUpdate, driver._companyId)
            .then((sent) => driver)
            .catch((err) => driver)
        );
      });
  }

  update<T extends IDriver | IDriverModel>(driver: IDriver | IDriverModel): Promise<IDriver> {
    // if it's already a document model then just save and return
    if (driver.constructor.name === 'model') {
      return (<IDriverModel>driver)
        .save()
        .then((driver) => this.addCompliance(driver))
        .then((driver) => {
          logger.info(`Driver ${driver._id} updated.`);
          return this.messageService
            .send(queueNames.companyComplianceUpdate, driver._companyId)
            .then((sent) => driver)
            .catch((err) => driver);
        });
    }

    return this.dbContext.user
      .findByIdAndUpdate(
        driver._user._id,
        {
          title: driver._user.title,
          forename: driver._user.forename,
          surname: driver._user.surname,
          email: driver._user.email,
        },
        { new: true }
      )
      .then((user) => {
        return this.dbContext.driver
          .findByIdAndUpdate(driver._id, driver, { new: true })
          .populate('_user')
          .lean()
          .exec()
          .then((driver) => this.populateVehicles(driver))
          .then((driver: IDriver) => this.addCompliance(driver))
          .then((driver: IDriver) => {
            logger.info(`Driver ${driver._id} updated.`);
            return this.messageService
              .send(queueNames.companyComplianceUpdate, driver._companyId)
              .then((sent) => driver)
              .catch((err) => driver);
          });
      });
  }

  remove<T extends IDriver>(driverId: string): Promise<IDriverModel> {
    return this.dbContext.driver
      .findByIdAndUpdate(driverId, { removed: true }, { new: true })
      .populate('_user')
      .exec()
      .then((driver) => {
        logger.info(`Driver ${driverId} removed.`);
        this.messageService.send(queueNames.companyComplianceUpdate, driver._companyId);
        return this.messageService
          .send(queueNames.companyComplianceUpdate, driver._companyId)
          .then((sent) => driver)
          .catch((err) => driver);
      });
  }
}
