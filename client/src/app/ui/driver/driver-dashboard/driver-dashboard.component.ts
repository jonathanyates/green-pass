import '../../../../../../shared/extensions/date.extensions';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Observable, Subscription} from "rxjs";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {AssessmentSummary} from "../../../../../../shared/models/assessment-summary.model";
import {ModuleSummary} from "../../../../../../shared/models/module-summary.model";
import {IAssessment, IOnRoadAssessment} from "../../../../../../shared/interfaces/assessment.interface";
import {AssessmentUpdateAction} from "../../../store/drivers/driver.actions";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {SelectCompany} from "../../../store/company/company.selectors";
import {ISubscription} from "../../../../../../shared/interfaces/subscription.interface";
import {SubscriptionHelper} from "../../../../../../shared/models/subscription.model";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {VehicleTypes} from "../../../../../../shared/interfaces/constants";

@Component({
  selector: 'app-driver-dashboard',
  templateUrl: './driver-dashboard.component.html',
  styleUrls: ['./driver-dashboard.component.css']
})
export class DriverDashboardComponent implements OnInit, OnDestroy {

  driver: IDriver;
  company: ICompany;
  subscription: ISubscription;
  showLicenceCheck: boolean = false;
  showVehicles: boolean = false;
  showInsurance: boolean = false;
  showFaw: boolean = false;
  assessment: IAssessment;
  assessmentSummary: AssessmentSummary;
  moduleSummary: ModuleSummary;
  onRoadAssessment: IOnRoadAssessment;

  private subscriptions: Subscription[] = [];

  constructor(private store: Store<AppState>) {}

  ngOnInit() {

    this.subscriptions.push(Observable.combineLatest(
      this.store.let(SelectCompany()).filter(company => company != null),
      this.store.let(SelectDriver()).filter(driver => driver !== null),
      (company, driver) => ({ company, driver }))
      .subscribe(results => {
        this.company = results.company;
        this.subscription = SubscriptionHelper.getSubscription(this.company);
        this.driver = results.driver;
        if (this.driver) {
          this.setLicenceCheckStatus(this.driver);
          this.setAssessmentStatus(this.driver);
          this.showVehicles = this.driver.ownedVehicle;
          this.showInsurance = this.driver.ownedVehicle ||
            this.driver.vehicles && this.driver.vehicles.some((vehicle:IVehicle) => vehicle.type === VehicleTypes.grey);
        }

        if (this.company && this.driver) {
          this.showFaw = this.company.settings.drivers.includeFaw === true &&
            this.driver.fawRequired === true;
        }
      }));

    this.subscriptions.push(this.store.let(SelectDriver())
      .filter(driver => driver !== null)
      .subscribe((driver:IDriver) => {
        let lastAssessment = driver.assessments && driver.assessments.length > 0
          ? driver.assessments.slice(-1)[0]
          : null;

        if (lastAssessment && lastAssessment.lastLoginDate > lastAssessment.lastUpdatedDate) {
          this.store.dispatch(new AssessmentUpdateAction(driver));
        }
      }));
  }

  private setLicenceCheckStatus(driver: IDriver) {
    if (!driver.licence || !driver.licence.checks || driver.licence.checks.length === 0) {
      this.showLicenceCheck = true;
    } else {
      let now = new Date();
      let diff = now.getDayDiff(driver.licence.nextCheckDate);
      if (driver.licence.nextCheckDate && diff <= 0) {
        this.showLicenceCheck = true;
      }
    }
  }

  private setAssessmentStatus(driver: IDriver) {
    if (driver.assessments && driver.assessments.length > 0) {
      this.assessment = driver.assessments.slice(-1)[0];
      this.assessmentSummary = new AssessmentSummary(this.assessment);
      this.moduleSummary = new ModuleSummary(this.assessment);
      if (this.assessment.onRoad) {
        this.onRoadAssessment = this.assessment.onRoad;
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
