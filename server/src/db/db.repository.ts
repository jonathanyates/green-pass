import { Query, Document } from 'mongoose';

export interface IRepository<T> {
  getAll(): Promise<T[]>;

  get(id: string): Promise<T>;

  find(criteria: any, page?: number, pageSize?: number, populate?: boolean): Promise<T[]>;

  findOne(criteria: any): Promise<T>;

  add(entity: T): Promise<T>;

  update(entity: T): Promise<T>;

  remove(entityId: string): Promise<T>;
}

export interface IQueryableRepository<T> extends IRepository<T> {
  queryAll(): Query<T[], Document>;

  query(id: string): Query<T, Document>;
}
