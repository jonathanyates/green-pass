import "reflect-metadata";
import {TYPES} from "../../src/container/container.types";
import {IDocumentService} from "../../src/documents/service/document.service.interface";
import container from "../../src/container/container.config";

console.log('Processing Documents...');

const documentService = container.get<IDocumentService>(TYPES.services.DocumentService);

documentService.sendDocumentsForAllCompanies()
    .then(companies => {
      console.log('Processing Documents completed successfully');
      process.exit();
    })
    .catch(error =>{
      console.log(error);
      process.exit(1);
    });

