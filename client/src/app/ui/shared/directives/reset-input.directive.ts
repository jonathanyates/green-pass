import {Directive, ElementRef, Renderer, Input} from '@angular/core';
declare let $: any;

// This is to resolve an mdbootstrap bug where the the label gets set as a placeholder when there
// is text in the input field, therefore the placeholder appears in the same position.
//
@Directive({
  selector: '[gp-reset-input]'
})
export class ResetInputDirective  {

  constructor(
    private element: ElementRef,
    public renderer: Renderer) {
  }

  @Input('gp-reset-input') set resetInput(value: string) {
    if (value && this.element && this.renderer) {
      this.renderer.setElementClass(this.element.nativeElement, 'active', true);
    }
  }

}
