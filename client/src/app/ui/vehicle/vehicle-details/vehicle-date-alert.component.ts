import { Component, Input } from '@angular/core';
import { IsDateExpired } from '../../../../../../shared/utils/helpers';
import { isDate } from 'moment';

@Component({
  selector: 'gp-vehicle-date-alert',
  template: `
    <div class="card text-center white-text justify-content-center" [ngClass]="dateStyle">
      <div class="d-flex justify-content-center p-3">
        <div>
          <i *ngIf="date" class="d-inline fa fa-2x" [ngClass]="{ 'fa-check': !expired, 'fa-times': expired }"></i>
          <h3 class="d-inline">{{ !expired ? title : expiredTitle }}</h3>
          <h5>{{ dateHeading }}: {{ date | date : 'dd MMMM yyyy' }}</h5>
        </div>
      </div>
    </div>
  `,
})
export class VehicleDateAlertComponent {
  private _date: Date;
  @Input() set date(date: Date) {
    this._date = date;
    this.dateStyle = this.getDateStyle(date);
    this.expired = date && IsDateExpired(date);
  }
  get date() {
    return this._date;
  }

  @Input() title: string;
  @Input() expiredTitle: string;
  @Input() dateHeading: string;
  dateStyle: string;
  expired: boolean = false;

  getDateStyle(date: Date) {
    if (!isDate(date)) {
      return 'info-color-dark';
    }
    if (date.getTime() <= Date.now() || date.isToday()) {
      return 'danger-color-dark';
    }
    if (new Date(date).addMonths(-1).getTime() <= Date.now()) {
      return 'warning-color';
    }

    return 'success-color';
  }
}
