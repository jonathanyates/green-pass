import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {User} from "../../user/user.model";
import {FormatDate, GetDateStyle} from "../../../../../../shared/utils/helpers";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {ICompanySettings} from '../../../../../../shared/interfaces/company-settings.interface';

@Component({
  selector: 'gp-driver-details',
  template: `<gp-dynamic-list-group [items]="items" [allowEdit]="allowEdit" (edit)="onEdit()"></gp-dynamic-list-group>`
})
export class DriverDetailsComponent {

  items: IListGroupItem[];
  private _driver: IDriver;

  @Input() companySettings: ICompanySettings;

  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    this.setItems(driver);
  }
  get driver() { return this._driver; }

  @Input() isDriver: boolean;
  @Input() allowEdit: boolean;

  @Output() edit: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    protected router: Router,
    private route: ActivatedRoute) {
  }

  setItems = (driver:IDriver) => {
    this.items = [
      {title: "Name", value: driver ? User.getFullName(driver._user) : null},
      {title: "Address", value: driver ? driver.address : null},
      {title: "Licence Number", value: driver ? driver.licenceNumber : null},
      {title: "NI Number", value: driver ? driver.niNumber : null},
      {title: "Date of birth", value: driver ? FormatDate(driver.dateOfBirth) : null},
      {title: "Gender", value: driver ? driver.gender : null},
      {title: "Phone", value: driver ? driver.phone : null},
      {title: "Mobile", value: driver ? driver.mobile : null},
      {title: "Email", value: driver ? driver._user.email : null},
      {title: "Username", value: driver ? driver._user.username : null},
      {
        title: "Owned Vehicle", value: driver
          ? driver.ownedVehicle === true
            ? 'Yes' : 'No'
          : null
      }
    ];

    if (this.companySettings && this.companySettings.drivers.includeFaw) {
      this.items.push(
        { title: 'Requires First Aid Certificate?',
          value: driver.fawRequired ? 'Yes' : 'No'
        }
      )
    }

    if (this.companySettings && this.companySettings.drivers.includeDbs) {
      this.items.push(
        { title: 'DBS Certificate Expiry Date',
          value: driver.dbsExpiry ? FormatDate(driver.dbsExpiry) : 'Not Set',
          style: DriverDetailsComponent.getDateStyle(driver.dbsExpiry)
        }
      )
    }

    if (this.companySettings && this.companySettings.drivers.includePNumber) {
      this.items.splice(4, 0,
        { title: 'PNumber',
          value: driver.pNumber ? driver.pNumber : 'Not Set'
        }
      )
    }
  };

  onEdit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  static getDateStyle(date: Date) {
    let style = GetDateStyle(date);
    return 'badge ' + (style ? style : 'danger-color');
  }
}
