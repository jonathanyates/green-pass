export interface IPaginationResults<T> {
  total: number,
  page: number,
  items: T[]
}

export interface IFileRecord {
  _id?: string,
  validFrom: Date,
  validTo:Date,
  filename?: string,
  fileType?: string,
  fileId?: string,
  file?: any
}

export interface IInsurance extends IFileRecord {}

export interface IFleetInsurance extends IInsurance {}

export interface IFawCertificate extends IFileRecord {}

export interface IDbsCertifcate extends IFileRecord {}
