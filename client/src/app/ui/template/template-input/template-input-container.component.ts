import {Observable} from "rxjs/Rx";
import {Component, OnInit, ViewChild} from '@angular/core';
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {SelectTemplate, SelectTemplates, SelectTemplatesState} from "../../../store/templates/template.selectors";
import {ActivatedRoute, Router} from "@angular/router";
import {TemplateInputComponent} from "./template-input.component";
import {TemplateService} from "../../../services/template.service";
import {BusyAction} from "../../../store/progress/progress.actions";
import {HandleErrorAction} from "../../../store/error/error.actions";

@Component({
  selector: 'gp-template-container',
  template: `
    <gp-template-input [folders]="folders" [template]="template" [nameExistsFunc]="nameExists"
                       (save)="save($event)" (back)="back()"></gp-template-input>
  `
})

export class TemplateInputContainerComponent implements OnInit {

  allTemplateNames: string[];
  template: ITemplate;
  folders: string[];
  @ViewChild(TemplateInputComponent) templateInputComponent: TemplateInputComponent;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private templateService: TemplateService) {
  }

  ngOnInit() {
    this.store.let(SelectTemplatesState())
      .filter(state => state.list != null && state.selected != null)
      .subscribe(state => {
        this.allTemplateNames = state.list.map(template => template.name);
        this.folders = Array.from(new Set(state.list.map(template => template.folderName)));
        this.template = Object.assign({}, state.selected);
      });
  }

  save(template: ITemplate) {
    this.store.dispatch(new BusyAction());
    this.templateService.saveTemplate(template)
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
      .subscribe(template => this.router.navigate(['../'], { relativeTo: this.route }));
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  nameExists = (name: string) => {
    return this.allTemplateNames && this.allTemplateNames.indexOf(name) > -1
  }
}
