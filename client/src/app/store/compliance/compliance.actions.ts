import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {ICompliance} from "../../../../../shared/interfaces/compliance.interface";

export const ActionTypes = {
  LOAD:                 type('[Compliance] Load'),
  LOAD_SUCCESS:         type('[Compliance] Load Success'),
  SELECT:                 type('[Compliance] Select'),
  SELECT_SUCCESS:         type('[Compliance] Select Success')
};

/**
 * Load Compliance Actions
 */
export class LoadComplianceAction implements Action {
  type = ActionTypes.LOAD;
  constructor() { }
}

export class LoadComplianceSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: ICompliance[]) { }
}

/**
 * Load Compliance Actions
 */
export class SelectComplianceAction implements Action {
  type = ActionTypes.SELECT;
  constructor(public payload: string) { } // company id
}

export class SelectComplianceSuccessAction implements Action {
  type = ActionTypes.SELECT_SUCCESS;
  constructor(public payload: ICompliance) { }
}

export type Actions
  = LoadComplianceSuccessAction
  | SelectComplianceSuccessAction
  | LogoutAction;
