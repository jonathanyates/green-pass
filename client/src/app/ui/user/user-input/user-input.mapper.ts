import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {User} from "../user.model";
import {RegExpPatterns} from "../../../../../../shared/constants";
import {IUser} from "../../../../../../shared/interfaces/user.interface";

export class UserInputMapper {

  static mapFrom(form:FormGroup): IUser {

    let user:any = {};
    user.title = form.controls['title'].value;
    user.forename = form.controls['forename'].value;
    user.surname = form.controls['surname'].value;
    user.email = form.controls['email'].value;
    return user;
  }

  static mapTo(user:IUser): FormInputGroup[] {

    let titles = [
      { key: 'Mr', value: 'Mr', selected: user && user.title === 'Mr' },
      { key: 'Mrs', value: 'Mrs', selected: user && user.title === 'Mrs' },
      { key: 'Ms', value: 'Ms', selected: user && user.title === 'Ms' },
      { key: 'Dr', value: 'Dr', selected: user && user.title === 'Dr' },
    ];

    return [
      {
        label: 'User',
        inputs: [
          { id: 'title', label: 'Title', type:'dropdown', required: true, autoFocus: true,
            options: titles, value: user && user.title ? user.title : null },
          { id: 'forename', label: 'First Name', type:'text', required: true, value: user.forename },
          { id: 'surname', label: 'Surname', type:'text', required: true, value: user.surname },
          { id: 'email', label: 'Email', type:'text', required: true,
            regex: RegExpPatterns.Email, value: user.email },
        ]
      }
    ];
  }

}
