import { Pipe, PipeTransform } from '@angular/core';
import {IAddress} from "../../../../../../shared/interfaces/address.interface";

@Pipe({ name: 'fullAddress' })
export class FullAddressPipe implements PipeTransform {

  transform(value: IAddress, args: any[]): any {
    return value
      ? value.line1 + ', '
        .concat(this.format(value.line2))
        .concat(this.format(value.line3))
        .concat(this.format(value.line4))
        .concat(this.format(value.locality))
        .concat(value.townCity + ', ')
        .concat(value.county + ', ')
        .concat(value.postcode + ', ')
        .concat(value.country + ', ')
      :null;
  };

  format(value: string): string {
    return value && value.replace(/\s/g,'').length > 0 ? value + ', ' : ''
  }
}
