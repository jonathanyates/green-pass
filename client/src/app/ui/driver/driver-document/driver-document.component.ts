import * as Cookies from 'js-cookie';
import { environment } from '../../../../environments/environment';
import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentService } from '../../../services/document.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.states';
import { Subscription, Observable } from 'rxjs';
import { SelectDriver } from '../../../store/drivers/driver.selectors';
import { Paths } from '../../../shared/constants';
import { SelectDocument } from '../../../store/documents/document.selectors';
import { Driver } from '../driver.model';
import { IDocument } from '../../../../../../shared/interfaces/document.interface';
import { BusyAction } from '../../../store/progress/progress.actions';
import { SelectLoggedInUser } from '../../../store/authentication/authentication.selectors';
import { Roles } from '../../../../../../shared/constants';

declare let $: any;

@Component({
  selector: 'gp-driver-document',
  template: `
    <div *ngIf="document">
      <div *ngIf="embeddedUrl" class="fixframe">
        <iframe
          id="frame"
          width="100%"
          style="border: none"
          #frame
          [src]="embeddedUrl | safe"
          (load)="onFrameLoad(frame)"
          [ngClass]="{ 'hidden-xs-up': busy }"
        ></iframe>
      </div>

      <div *ngIf="pdf" class="offset-lg-1 col-lg-10 offset-xl-2 col-xl-8">
        <gp-pdf-preview [file]="pdf"></gp-pdf-preview>
      </div>

      <gp-error-alert [error]="error"></gp-error-alert>
    </div>
  `,
  styleUrls: ['driver-document.component.css'],
})
export class DriverDocumentComponent implements OnInit, OnDestroy {
  document: IDocument;
  driver: Driver;
  embeddedUrl: string;
  pdf: any;
  returnUrl: string;
  busy: boolean = true;
  error: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private documentService: DocumentService,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.store.dispatch(new BusyAction());
    this.returnUrl = `${window.location.origin}/driver/documents`;

    this.renderer.listen('window', 'message', this.listenForMessages.bind(this));

    let documentSubscription = Observable.combineLatest(
      this.store.let(SelectDriver()).filter((driver) => driver != null),
      this.store.let(SelectDocument()),
      this.store.let(SelectLoggedInUser()).filter((user) => user != null),
      (driver, document, user) => ({ driver: driver, document: document, user: user })
    ).subscribe(
      (results) => {
        this.driver = results.driver;
        this.document = results.document;

        if (results.user.role !== Roles.Driver || this.document.signed) {
          this.getPdf();
        } else {
          this.safariFix();
          this.setEmbeddedUrl();
        }

        this.store.dispatch(new BusyAction(false));
      },
      (error) => {
        this.store.dispatch(new BusyAction(false));
        console.log(error);
      }
    );

    this.subscriptions.push(documentSubscription);
  }

  private getPdf() {
    this.subscriptions.push(
      this.documentService.getDocumentPdf(this.driver, this.document.documentId).subscribe(
        (file) => {
          this.pdf = file;
          this.busy = false;
          this.store.dispatch(new BusyAction(false));
        },
        (error) => {
          this.error = error.message;
          this.busy = false;
          this.store.dispatch(new BusyAction(false));
        }
      )
    );
  }

  private setEmbeddedUrl() {
    let url = this.document ? `${this.document.embeddingUri}` : null;
    if (url) {
      this.embeddedUrl = `${url}?hidetext=1&hidenav=1`;
    }
  }

  private safariFix() {
    if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
      console.log('Found safari as browser');

      let cookie = Cookies.get('fixed');

      if (!cookie) {
        console.log('Setting fixed Cookie');

        Cookies.set('fixed', 'fixed');

        let url =
          'https://app1.legalesign.com/auth/_safari_fix/' +
          `?user=${environment.legalesign.username}` +
          `&group=${environment.legalesign.group}` +
          `&path=${encodeURIComponent('/driver/documents/' + this.document.documentId)}`;

        console.log('Setting window.location with ' + url);

        window.location.replace(url);

        console.log('Set window.location');
      }
    }
  }

  private listenForMessages(message: MessageEvent) {
    // Only trust messages from the below origin.
    if (message.origin !== 'https://app1.legalesign.com') return;

    if (message.data == 'signed') {
      return this.router.navigate([Paths.Driver, Paths.Documents]);
    }

    if (message.data == 'expired' && this.embeddedUrl) {
      this.busy = false;
      console.log('Received expired message.');

      let parts = this.embeddedUrl.split('/');
      let index = parts.lastIndexOf('esign') + 1;
      let signerId = parts[index];

      this.embeddedUrl = null;

      console.log('Getting new embedded Url using signerId ' + signerId);

      this.store.dispatch(new BusyAction());

      this.documentService.getNewEmbeddedUrl(this.driver, this.document.documentId, signerId).subscribe(
        (url) => {
          this.store.dispatch(new BusyAction(false));
          this.busy = false;

          if (url) {
            console.log('Setting new embeddedUrl to ' + url);
            this.embeddedUrl = url;
          } else {
            console.log('Unable to get new embedded Url.');
          }
        },
        (error) => {
          this.store.dispatch(new BusyAction(false));
          this.busy = false;

          console.log('Error getting new embedded Url.');
          console.log(error);
        }
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  onFrameLoad(frame) {
    try {
      if (this.embeddedUrl) {
        Observable.timer(1000).subscribe((_) => {
          this.busy = false;
          $('#frame').addClass('animated fadeIn');

          // force scroll to top
          Observable.timer(100).subscribe((_) => window.scrollTo(0, 0));
          this.store.dispatch(new BusyAction(false));
        });
      }
    } catch (e) {
      // swallow 'blocked a frame with origin error'
      // frame is loading from origin, so set busy to false after small timeout
    }
  }
}
