import {ContainerModule, interfaces} from "inversify";
import {ICompanyService} from "./company.service.interface";
import {TYPES} from "../container/container.types";
import {CompanyRouter} from "./company.router";
import {CompanyController, ICompanyController} from "./company.controller";
import {CompanyService} from "./company.service";
import {CompanyRepository, ICompanyRepository} from "./company.repository";
import {IApiRouter} from "../interfaces/api-router.interface";

export const companyModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.CompanyRouter).to(CompanyRouter).inSingletonScope();
  bind<ICompanyController>(TYPES.controllers.CompanyController).to(CompanyController).inSingletonScope();
  bind<ICompanyService>(TYPES.services.CompanyService).to(CompanyService).inSingletonScope();
  bind<ICompanyRepository>(TYPES.repositories.CompanyRepository).to(CompanyRepository).inSingletonScope();
});
