import 'reflect-metadata';
import container from '../src/container/container.config';
import { TYPES } from '../src/container/container.types';
import { IAlertQueryService } from '../src/alerts/alert-query.service';
import { IAlertService } from '../src/alerts/alert.service';

describe.skip('AlertQueryService', () => {
  describe.skip('updateAllAlerts', () => {
    it('updates all alerts for a given company', async () => {
      const alertQueryService = container.get<IAlertQueryService>(TYPES.services.AlertQueryService);
      const result = await alertQueryService.updateAllAlerts('012345678901234567891000');
      expect(result).not.toBeNull;
    });
  });

  describe.skip('updateAlert', function () {
    it('updates alerts for a given alert type', async () => {
      const alertQueryService = container.get<IAlertQueryService>(TYPES.services.AlertQueryService);
      const alertService = container.get<IAlertService>(TYPES.services.AlertService);

      const alertType = await alertService.getAlertType('63b9e22b74af340045a68feb');
      const result = await alertQueryService.updateAlert('63b9e262b2346e0016d7853d', alertType);
      expect(result).not.toBeNull;
    });
  });
});
