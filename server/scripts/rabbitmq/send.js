import { connect } from 'amqplib/callback_api';

connect('amqp://localhost', function (err, conn) {
  conn.createChannel(function (err, ch) {
    let queue = 'hello';
    let message = {
      cron: '* * * * *',
      name: 'Some Report',
    };

    ch.assertQueue(queue, { durable: false });

    // Note: on Node 6 Buffer.from(msg) should be used
    ch.sendToQueue(queue, new Buffer(JSON.stringify(message)));

    console.log(' [x] Sent %s', message);
  });
  setTimeout(function () {
    conn.close();
    process.exit(0);
  }, 500);
});
