import logger = require('winston');
import mongoose from 'mongoose';
import { IQueryableRepository } from '../db/db.repository';
import { ICompany } from '../../../shared/interfaces/company.interface';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { Query } from 'mongoose';
import { ICompanyModel } from './company.schema';
import { queueNames } from '../messaging/messages.constants';
import { IMessageService } from '../messaging/message.service';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

export interface ICompanyRepository extends IQueryableRepository<ICompany | ICompanyModel> {
  updateTemplates(companyId: string, templates: string[]): Promise<ICompanyModel>;
}

@injectable()
export class CompanyRepository implements ICompanyRepository {
  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  private populate(model) {
    return model.populate({
      path: 'alertTypes.alertType',
      populate: { path: 'reportType' },
    });
    // .populate({
    //   path: 'alertTypes.emailRecipients',
    //   select: 'forename surname email',
    // });
  }

  updateTemplates(companyId: string, templates: string[]): Promise<ICompanyModel> {
    return this.populate(this.dbContext.company.findByIdAndUpdate(companyId, { templates: templates }, { new: true }))
      .lean()
      .exec();
  }

  getAll(): Promise<ICompany[]> {
    return this.populate(this.dbContext.company.find({ removed: false }))
      .lean()
      .exec();
  }

  get(id: string): Promise<ICompany> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.populate(this.dbContext.company.findById(id)).lean().exec();
  }

  queryAll(): Query<ICompanyModel[], mongoose.Document> {
    const result = this.dbContext.company.find({ removed: false });
    return result;
  }

  query(id: string): Query<ICompanyModel, mongoose.Document> {
    return this.populate(this.dbContext.company.findById(id));
  }

  find(criteria: any): Promise<ICompany[]> {
    criteria.removed = false;
    return <Promise<ICompany[]>>this.dbContext.company.find(criteria).lean().exec();
  }

  findOne(criteria: any): Promise<ICompany> {
    criteria.removed = false;
    return this.populate(this.dbContext.company.findOne(criteria)).lean().exec();
  }

  add<T extends ICompany>(company: ICompanyModel): Promise<ICompanyModel> {
    const companyModel = new this.dbContext.company(company);

    return companyModel
      .save()
      .then((company: ICompanyModel) => {
        this.updateChildIds(company);
        return company.save().then((company) => {
          logger.info(`Company ${company._id} added.`);
          return company;
        });
      })
      .catch((err) => {
        if (err.name === 'MongoError' && err.code === 11000) {
          return Promise.reject(
            ServerError(
              `
          A company with the name '${company.name}' already exists.<br>
          This company may have been removed but still exists on the system.<br>
          You can not add companies with the same name even if they have been removed.`,
              codes.INTERNAL_SERVER_ERROR
            )
          );
        }

        return Promise.reject(ServerError(err.message, codes.INTERNAL_SERVER_ERROR));
      });
  }

  update<T extends ICompany>(company: ICompany): Promise<ICompanyModel> {
    this.updateChildIds(company);
    return this.populate(this.dbContext.company.findByIdAndUpdate(company._id, company, { new: true }))
      .exec()
      .then((company) => {
        logger.info(`Company ${company._id} updated.`);
        return this.messageService
          .send(queueNames.companyComplianceUpdate, company._id)
          .then((sent) => company)
          .catch((err) => company);
      });
  }

  remove<T extends ICompany>(companyId: string): Promise<ICompanyModel> {
    return this.dbContext.company
      .findByIdAndUpdate(companyId, { removed: true }, { new: true })
      .exec()
      .then((company) => {
        logger.info(`Company ${companyId} removed.`);
        return company;
      });
  }

  // This method ensures that the depot and supplier company ids are always set
  // and set to the correct company id
  private updateChildIds = (company) => {
    if (company.depots) {
      company.depots
        .filter((depot) => !depot._companyId || depot._companyId !== company._id)
        .forEach((depot) => {
          depot._companyId = company._id;
        });
    }

    if (company.suppliers) {
      company.suppliers
        .filter((supplier) => !supplier._companyId || supplier._companyId !== company._id)
        .forEach((supplier) => {
          supplier._companyId = company._id;
        });
    }
  };
}
