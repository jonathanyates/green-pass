import "reflect-metadata";
import {TYPES} from "../src/container/container.types";
import container from "../src/container/container.config";
import {subscriptions} from "../../shared/interfaces/subscription.interface";
import {ISubscriptionService} from "../src/subscriptions/subscription.service";

const subscriptionService:ISubscriptionService = container.get<ISubscriptionService>(TYPES.services.SubscriptionService);

Promise.all(subscriptions.map(subscription => {
  return subscriptionService.add(subscription)
}))
  .then(subscriptions => {
    console.log((subscriptions ? subscriptions.length : 0) + ' subscriptions created.');
    process.exit();
  })
  .catch(error =>{
    console.log(error);
    process.exit(1);
  });