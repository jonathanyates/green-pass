import {ContainerModule, interfaces} from "inversify";
import {TYPES} from "../container/container.types";
import {IDvlaService} from "./service/dvla.service.interface";
import {DvlaService} from "./service/dvla.service";

export const dvlaModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IDvlaService>(TYPES.services.DvlaService).to(DvlaService).inSingletonScope();
});