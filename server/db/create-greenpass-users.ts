import 'reflect-metadata';
import { TYPES } from '../src/container/container.types';
import { IRepository } from '../src/db/db.repository';
import { IUser } from '../../shared/interfaces/user.interface';
import container from '../src/container/container.config';

const userRepository = container.get<IRepository<IUser>>(TYPES.repositories.UserRepository);

const users = [
  {
    username: 'lee.dickinson@greenpasscompliance.co.uk',
    password: 'greenpass',
    forename: 'Lee',
    surname: 'Dickinson',
    email: 'lee.dickinson@greenpasscompliance.co.uk',
    role: 'admin',
    changePassword: true,
  },
  {
    username: 'jonathan.yates@greenpasscompliance.co.uk',
    password: 'greenpass',
    forename: 'Jonathan',
    surname: 'Yates',
    email: 'jonathan.yates@greenpasscompliance.co.uk',
    role: 'admin',
    changePassword: true,
  },
];

Promise.all(
  users.map((user) => {
    return userRepository.add(user);
  })
)
  .then((users) => {
    console.log('Users created successfully.');
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
