import {ICompliance} from "../../../../../shared/interfaces/compliance.interface";

export interface ComplianceState {
  list: ICompliance[];
  selected?: ICompliance;
}
