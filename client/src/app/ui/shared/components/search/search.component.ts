import {Component, OnInit} from "@angular/core";
import {Subject} from "rxjs/Subject";
import {Messenger} from "../../../../services/messenger";
import {Messages} from "../../../../shared/constants";

@Component({
  selector: 'gp-search',
  template: `
    <form class="form-inline waves-effect waves-light ml-auto">
      <input class="form-control" type="text" placeholder="Search"
             (keyup)="onSearchChanged($event.target.value)">
    </form>
  `,
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private messenger: Messenger) {}

  searchTerm$ = new Subject<string>();

  ngOnInit(): void {
    this.searchTerm$
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(term => this.messenger.publish(Messages.SearchTerm, term));
  }

  onSearchChanged(term: string) {
    this.searchTerm$.next(term);
  }
}
