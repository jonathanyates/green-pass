import {Injectable} from '@angular/core';
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {apiPath} from "../shared/constants";
import {Observable} from "rxjs";
import {
  IAlertSearchResults, IAlertSummary, IAlertType,
  IAlertTypeSummary
} from "../../../../shared/interfaces/alert.interface";
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable()
export class AlertService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  search(companyId: string, page: number, pageSize: number, searchParameters?: any)
    : Observable<IAlertSearchResults> {

    this.authorize();

    let params;

    if (page != null && pageSize != null) {
      params = new HttpParams()
        .set('page', page ? page.toString() : '1')
        .set('pageSize', pageSize ? pageSize.toString() : '10');
    }

    if (searchParameters) {
      if (!params) {
        params = new HttpParams();
      }
      for(let prop in searchParameters) {
        if (searchParameters.hasOwnProperty(prop)) {
          params = params.set(prop, searchParameters[prop]);
        }
      }
    }

    let url = this.companiesUrl + companyId + '/alerts';

    return this.httpClient.get<IAlertSearchResults>(url, this.getOptions(params))
      .map(res => this.handleResult<IAlertSearchResults>(res))
      .catch(this.handleError);
  }

  getAlertSummaries(): Observable<IAlertSummary[]> {

    this.authorize();

    let url = apiPath + '/alerts/summary';

    return this.httpClient.get<IAlertSummary[]>(url, this.getOptions())
      .map(res => this.handleResult<IAlertSummary[]>(res))
      .catch(this.handleError);
  }

  getAlertSummary(companyId?: string): Observable<IAlertSummary> {

    this.authorize();

    let url = this.companiesUrl + companyId + '/alerts/summary';

    return this.httpClient.get<IAlertSummary>(url, this.getOptions())
      .map(res => this.handleResult<IAlertSummary>(res))
      .catch(this.handleError);
  }

  getAlertTypes(): Observable<IAlertType[]> {

    this.authorize();

    let url = apiPath + '/alerttypes';

    return this.httpClient.get<IAlertType[]>(url, this.getOptions())
      .map(res => this.handleResult<IAlertType[]>(res))
      .catch(this.handleError);
  }

  saveAlertType(alertType: IAlertType): Observable<IAlertType[]> {

    this.authorize();

    let url = apiPath + '/alerttypes/';

    if (alertType._id) {
      return this.httpClient.put<IAlertType>(url + alertType._id, alertType, this.getOptions())
        .map(res => this.handleResult<IAlertType>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IAlertType>(url, alertType, this.getOptions())
        .map(res => this.handleResult<IAlertType>(res))
        .catch(this.handleError);
    }
  }

  getTypeSummary(companyId: string): Observable<IAlertTypeSummary[]> {

    this.authorize();

    let url = this.companiesUrl + companyId + '/alerts/alertTypeSummary';

    return this.httpClient.get<IAlertTypeSummary[]>(url, this.getOptions())
      .map(res => this.handleResult<IAlertTypeSummary[]>(res))
      .catch(this.handleError);
  }

}
