import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import { SelectDriversState} from "../../../../store/drivers/driver.selectors";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {IFawCertificate} from "../../../../../../../shared/interfaces/common.interface";
import {SaveDriverFawAction} from "../../../../store/drivers/driver-faw.actions";

@Component({
  selector: 'gp-driver-faw-input',
  template: `
    <gp-fileRecord-input [fileRecord]="fawCertificate"
                         [heading]="'First Aid At Work'"
                         [fileHeading]="'First Aid At Work Certificate'"
                         (submit)="save($event)" (cancel)="cancel()">
    </gp-fileRecord-input>  `
})
export class DriverFawInputComponent implements OnInit, OnDestroy {

  fawCertificate: IFawCertificate;
  private driver: IDriver;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectDriversState())
      .subscribe(state => {
        this.driver = state.selected;
        this.fawCertificate = state.selectedFaw
          ? state.selectedFaw
          : <IFawCertificate>{ validFrom: null, validTo: null };
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(fawCertificate: IFawCertificate) {
    this.store.dispatch(new SaveDriverFawAction(this.driver, fawCertificate));
  }

  cancel() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
