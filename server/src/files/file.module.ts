import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {FileRouter} from "./file.router";
import {IFileService, FileService} from "./file.service";
import {FileController, IFileController} from "./file.controller";

export const fileModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.FileRouter).to(FileRouter).inSingletonScope();
  bind<IFileController>(TYPES.controllers.FileController).to(FileController).inSingletonScope();
  bind<IFileService>(TYPES.services.FileService).to(FileService).inSingletonScope();
});