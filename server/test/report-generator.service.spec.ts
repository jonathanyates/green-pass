import 'reflect-metadata';
import { IReportGeneratorService, ReportGeneratorService } from '../src/reports/report-generator.service';
import container from '../src/container/container.config';
import { IReportQueryService } from '../src/reports/report-query.service';
import { TYPES } from '../src/container/container.types';
import { IReportTypeService } from '../src/reports/report-type.service';
import { IEmailService } from '../src/email/email.service';

describe.skip('ReportGeneratorService', function () {
  describe.skip('generate', function () {
    it('generates a Report', () => {
      const generatorService = container.get<IReportGeneratorService>(TYPES.services.ReportGeneratorService);
      const reportTypeService = container.get<IReportTypeService>(TYPES.services.ReportTypeService);
      const reportQueryService = container.get<IReportQueryService>(TYPES.services.ReportQueryService);
      const emailService = container.get<IEmailService>(TYPES.services.EmailService);

      return reportTypeService.findOne({ query: 'getNoneCompliantVehicles' }).then((reportType) => {
        return reportQueryService.queries[reportType.query]('012345678901234567891000', reportType)
          .then((queryResult) => generatorService.generate(queryResult))
          .then((result) => {
            expect(result).toBeDefined();
            return emailService.send(
              ['jonathan.yates@hotmail.com'],
              'Report Test',
              result,
              'admin@greenpasscompliance.co.uk'
            );
          })
          .then((result) => {
            expect(result).toBeTruthy();
          });
      });
    });
  });
});
