import {
  ICompanyCompliance,
  ICompanyDriverCompliance, ICompanyVehicleCompliance, ICompliance,
  ITotalCompliance
} from "../interfaces/compliance.interface";

const ComplianceWeightings = {
  driver: 45,
  vehicle: 45,
  insurance: 10
};

export class CompanyCompliance implements ICompliance {

  compliant: number;

  constructor(public _companyId: string,
              public company: ICompanyCompliance,
              public vehicle: ICompanyVehicleCompliance,
              public driver: ICompanyDriverCompliance) {

    let compliant = (vehicle.compliant + driver.compliant) / 2;

    // if company compliance includes fleet insurance then need to factor this in using ComplianceWeightings
    if (company != null && company.insurance != null) {
      let insurance = company.insurance === true ? 100 : 0;
      compliant = (
        (vehicle.compliant * ComplianceWeightings.vehicle) +
        (driver.compliant * ComplianceWeightings.vehicle) +
        (insurance * ComplianceWeightings.insurance)
      ) / 100;
    }

    this.compliant = compliant;
  }

  static create(compliance: ICompliance) {
    return new CompanyCompliance(compliance._companyId, compliance.company,
      compliance.vehicle, compliance.driver);
  }

  static getTotalCompliance(items: ICompliance[]): ITotalCompliance {

    // add up the compliance values
    let compliance = items.reduce((acc, company, i) => {
      return {
        compliant: (acc.compliant + (company.compliant ? company.compliant : 0)),
        driver: (acc.driver + (company.driver.compliant ? company.driver.compliant : 0)),
        vehicle: (acc.vehicle + (company.vehicle.compliant ? company.vehicle.compliant : 0)),

      }
    }, {compliant: 0, driver: 0, vehicle: 0});

    // get totals with compliance values so we can divide the above by a sensible value
    // i.e. ignore any which have no compliance values yet
    let totals = items.reduce((acc, company, i) => {
      return {
        compliant: (acc.compliant + 1),
        driver: (acc.driver + 1),
        vehicle: (acc.vehicle + 1)
      }
    }, {compliant: 0, driver: 0, vehicle: 0});

    // return the average for all companies
    let result = {
      compliant: totals.compliant > 0 ? compliance.compliant / totals.compliant : 0,
      driver: totals.driver > 0 ? compliance.driver / totals.driver : 0,
      vehicle: totals.vehicle > 0 ? compliance.vehicle / totals.vehicle : 0,
    };

    result.compliant = (result.driver + result.vehicle) / 2;

    return result;
  }
}