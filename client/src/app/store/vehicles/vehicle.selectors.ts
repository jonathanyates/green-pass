import {Store} from "@ngrx/store";
import {AppState, AppStates} from "../app.states";
import {VehiclesState} from "./vehicles.state";

export const SelectVehiclesState = () => {
  return (store:Store<AppState>) => {
    return store.select<VehiclesState>(state => state.vehicles);
  }
};

export const SelectVehicles = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectVehiclesState())
      .map(state => state.list);
  }
};

export const SelectVehiclesPagination = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectVehiclesState())
      .map(state => ({
        total: state.total,
        page: state.page,
        vehicles: state.list,
      }));
  }
};

export const SelectVehicle = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectVehiclesState())
      .map(state => state.selected)
      .distinctUntilChanged();
  }
};

export const SelectSelectedVehicleDetailsTab = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectVehiclesState())
      .map(state => state.selectedTab);
  }
};
