import {ContainerModule, interfaces} from "inversify";
import {TYPES} from "../container/container.types";
import {IRoadMarqueService, RoadMarqueService} from "./roadmarque.service";
import {AssessmentService, IAssessmentService} from "./assessment.service";

export const assessmentModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IRoadMarqueService>(TYPES.services.RoadMarqueService).to(RoadMarqueService).inSingletonScope();
  bind<IAssessmentService>(TYPES.services.AssessmentService).to(AssessmentService).inSingletonScope();
});