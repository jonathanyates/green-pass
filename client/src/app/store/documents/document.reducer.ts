import * as actions from "./document.actions";
import {DocumentsState} from "./document.state";
import {ActionTypes} from "../authentication/authentication.actions";
import {IDocument} from "../../../../../shared/interfaces/document.interface";

export const initialState: DocumentsState = {
  list: [],
  selected: null
};

export function DocumentsReducer(state: DocumentsState = initialState, action: actions.Actions): DocumentsState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_SUCCESS:
      case actions.ActionTypes.SELECT_LIST_SUCCESS:
        return {
          list: <IDocument[]>action.payload,
          selected: null
        };

      case actions.ActionTypes.SELECT_DOCUMENT_SUCCESS:
        return {
          list: <IDocument[]>state.list,
          selected: <IDocument>action.payload
        };

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }
  };
