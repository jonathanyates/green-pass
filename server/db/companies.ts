import '../../shared/extensions/date.extensions';
import mongoose from 'mongoose';
import { ICompany } from '../../shared/interfaces/company.interface';
import { subscriptionNames, subscriptions } from '../../shared/interfaces/subscription.interface';

export const companies: ICompany[] = [
  {
    _id: new mongoose.Types.ObjectId('012345678901234567891000'),
    name: 'Green Pass Compliance Ltd',
    coreBusiness: 'Driver Compliance',
    address: {
      line1: '21, Topiary Gardens',
      locality: 'Bowgreave',
      townCity: 'Garstang',
      county: 'Lancashire',
      country: 'United Kingdom',
      postcode: 'PR3 1YF',
    },
    depots: [
      {
        name: 'Primary Dept',
        address: {
          line1: '21, Topiary Gardens',
          locality: 'Bowgreave',
          townCity: 'Garstang',
          county: 'Lancashire',
          country: 'United Kingdom',
          postcode: 'PR3 1YF',
        },
        phone: '01995 605124',
      },
    ],
    suppliers: [
      {
        _id: new mongoose.Types.ObjectId('012345678901234567892000'),
        name: 'ICA',
        address: {
          postcode: 'PR3 0RD',
          line1: 'Unit 1',
          line2: 'North Forge',
          line3: 'Off Garstang Road',
          locality: 'Bilsborrow',
          townCity: 'Preston',
          county: 'Lancashire',
          country: 'United Kingdom',
        },
        phone: '01995 640958',
        mobile: '07872 824248',
      },
    ],
    noOfEmployees: 2,
    phone: '01995 605124',
    mobile: '07500862225',
    email: 'jonathan.yates@hotmail.com',
    controllingMind: {
      name: 'Jonathan Yates',
      position: 'Director',
    },
    templates: [],
    subscription: Object.assign(
      {},
      subscriptions.find((x) => x.name == subscriptionNames.bronze),
      {
        startDate: new Date(),
        renewalDate: new Date().addMonths(12),
        alerts: 5,
        active: true,
      }
    ),
  },
];
