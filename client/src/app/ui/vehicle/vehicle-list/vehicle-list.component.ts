import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Messages, pagination, Paths} from "../../../shared/constants";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {VehicleListBuilder} from "../../../../../../shared/models/vehicle-list-builder";
import {Messenger} from "../../../services/messenger";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import {DynamicListComponent} from "../../shared/components/dynamic-list/dynamic-list.component";
import {SelectCompany} from "../../../store/company/company.selectors";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {BaseComponent} from "../../shared/components/base/base.component";

@Component({
  selector: 'gp-vehicle-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="onAdd()" [serverMode]="serverMode" 
                     [lightHeader]="lightHeader" [allowAdd]="allowAdd"
                     [total]="total" [page]="page" [itemsPerPage]="itemsPerPage" (pageChange)="onPageChange($event)"
                     (itemSelected)="select($event)"></gp-dynamic-list>
  `
})
export class VehicleListComponent extends BaseComponent implements OnInit {

  allowAdd: boolean;

  @Input() total: number;
  @Input() page: number = 1;
  @Input() itemsPerPage: number = pagination.pageSize;
  @Input() serverMode: boolean;
  @Input() lightHeader: boolean = false;
  @Input() showSearch: boolean = true;

  @Output() back: EventEmitter<any> = new EventEmitter();
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  @Output() add = new EventEmitter();
  @Output() remove: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();
  @Output() itemSelected: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();

  @Input() set vehicles(vehicles: IVehicle[]) {
    if (vehicles) {
      this.setList(vehicles);
    }
  }

  list: IDynamicList;

  @ViewChild(DynamicListComponent) listComponent: DynamicListComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private messenger: Messenger,
    private store: Store<AppState>) {
    super();
  }

  ngOnInit() {
    this.vehicles = null;

    if (this.showSearch) {
      this.messenger.publish(Messages.ShowSearch, true);
    }

    this.subscriptions.push(this.messenger.toObservable(Messages.SearchTerm)
      .subscribe(term => this.search.emit(term)));

    if (this.back.observers.length > 0) {
      this.listComponent.back.subscribe(e => this.onBack());
    }
  }

  setList(vehicles: IVehicle[]) {
    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .subscribe((company: ICompany) => {
        this.allowAdd = company != null && company.settings != null && company.settings.drivers != null
          ? company.settings.drivers.allowInput
          : false;
        this.list = VehicleListBuilder.getList(vehicles, company.settings);
        this.addActions();
      }));
  }

  addActions() {
    if (this.remove.observers.length > 0) {
      this.list.columns.push({header: '', field: 'remove', columnType: ColumnType.Icon});
      this.list.actions = this.list.items.map(item => ({
        remove: {
          action: args => { if (args) this.onRemove(args) },
          icon: 'fa fa-trash-o green-text'
        }
      }));
    }
  }

  onRemove(args) {
    if (!args && !args.item) return;
    this.remove.emit(args.item);
  }

  onAdd() {
    this.add.emit();
  }

  select(vehicle: IVehicle) {
    if (vehicle) {
      if (this.itemSelected.observers.length > 0) {
        this.itemSelected.emit(vehicle);
        return;
      }
      return this.router.navigate(['./', vehicle._id], { relativeTo: this.route });
    }
  }

  onBack() {
    this.messenger.publish(Messages.ShowSearch, false);
    this.back.emit();
  }

  onPageChange(page: number) {
    this.pageChange.emit(page);
  }
}
