import {Component, OnInit} from '@angular/core';
import {IAssessment} from "../../../../../../shared/interfaces/assessment.interface";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectDriver, SelectDriverUserInfo} from "../../../store/drivers/driver.selectors";
import {BaseComponent} from "../../shared/components/base/base.component";
import {AssessmentUpdateAction} from "../../../store/drivers/driver.actions";
import {Roles} from "../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-assessments-container',
  template: `
    <gp-card [header]="'Assessments'">
      <gp-driver-assessments [driver]="driver" [user]="user" [lightHeader]="true" 
                             [assessments]="assessments" [showAction]="showAction"></gp-driver-assessments>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `,
})
export class DriverAssessmentsContainerComponent extends BaseComponent implements OnInit {

  driver: IDriver;
  user: IUser;
  showAction: boolean;
  assessments: IAssessment[];

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        this.driver = info.driver;
        this.user = info.user;
        this.showAction = this.user && this.user.role == Roles.Driver;
        if (this.driver) {
          this.assessments = this.driver.assessments;
        }
      }));

    this.subscriptions.push(this.store.let(SelectDriver())
      .filter(driver => driver !== null).take(1)
      .subscribe((driver:IDriver) => {
        let lastAssessment = driver.assessments && driver.assessments.length > 0
          ? driver.assessments.slice(-1)[0]
          : null;

        if (lastAssessment && lastAssessment.lastLoginDate > lastAssessment.lastUpdatedDate) {
          this.store.dispatch(new AssessmentUpdateAction(driver));
        }
      }));
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
