import mongoose = require('mongoose');
import _ = require('lodash');
import {injectable, inject} from "inversify";
import {
  IAlert, IAlertSearchResults, IAlertSummary, IAlertType,
  IAlertTypeSummary
} from "../../../shared/interfaces/alert.interface";
import {IApiService} from "../interfaces/api-service.interface";
import {TYPES} from "../container/container.types";
import {IRepository} from "../db/db.repository";
import {ServerError} from "../shared/errors";
import codes from "../shared/error.codes";
import {ICompany} from "../../../shared/interfaces/company.interface";
import {ReportCategory} from "../../../shared/interfaces/report.interface";
import {IDbContext} from "../db/db.context.interface";
import {IAlertRepository} from "./alert.repository";
import {ICompanyRepository} from "../companies/company.repository";

export interface IAlertService extends IApiService<IAlert> {

  getAlertSummaries(): Promise<IAlertSummary[]>;

  getAlertSummary(companyId: string): Promise<IAlertSummary>;

  getAlertTypes(): Promise<IAlertType[]>;

  getAlertType(id: string): Promise<IAlertType>;

  findAlertType(criteria: any): Promise<IAlertType[]>;

  addAlertType(entity: IAlertType): Promise<IAlertType>;

  updateAlertType(entity: IAlertType): Promise<IAlertType>;

  getAlertTypeSummary(companyId: string): Promise<IAlertTypeSummary[]>;

  search(criteria: any, page: number, pageSize: number)
    : Promise<IAlertSearchResults>;
}

@injectable()
export class AlertService implements IAlertService {

  constructor(@inject(TYPES.repositories.AlertTypeRepository) private alertTypeRepository: IRepository<IAlertType>,
              @inject(TYPES.repositories.AlertRepository) private alertRepository: IAlertRepository,
              @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
              @inject(TYPES.db.DbContext) private dbContext: IDbContext) {
  }

  findOne(criteria: any): Promise<IAlert> {
    return this.alertRepository.findOne(criteria);
  }

  getAlertTypes(): Promise<IAlertType[]> {
    return this.alertTypeRepository.getAll();
  }

  getAlertType = (id: string): Promise<IAlertType> => {
    return this.alertTypeRepository.get(id);
  };

  findAlertType(criteria: any): Promise<IAlertType[]> {
    return this.alertTypeRepository.find(criteria);
  }

  addAlertType(alertType: IAlertType): Promise<IAlertType> {
    return this.alertTypeRepository.add(alertType)
  }

  updateAlertType(alertType: IAlertType): Promise<IAlertType> {
    return this.alertTypeRepository.update(alertType);
  }

  getAlertSummaries = (): Promise<IAlertSummary[]> => {
    return Promise.all([
      this.companyRepository.getAll(),
      this.alertRepository.find({status: 'Active'}, null, null, false)])
      .then(results => {
        const companies:ICompany[] = results[0];
        const alerts:IAlert[] = results[1];

        return companies.map(company => {
          const companyAlerts = alerts.filter(alert => alert._companyId.equals(company._id));
          return {
            companyId: company._id,
            driverAlerts: companyAlerts.filter(alert => alert.alertType.reportType.category === ReportCategory.Driver).length,
            vehicleAlerts: companyAlerts.filter(alert => alert.alertType.reportType.category === ReportCategory.Vehicle).length,
          }
        })
      });
  };

  getAlertSummary = (companyId: string): Promise<IAlertSummary> => {
    return Promise.all([
      this.alertRepository.find({ _companyId: new mongoose.Types.ObjectId(companyId), status: 'Active'}),
      this.companyRepository.get(companyId)
    ])
      .then(results => {
        let alerts = results[0];
        const company = results[1];

        // ensure we only include allowed alert Types for the company
        alerts = alerts.filter(alert => {
          return company.alertTypes
            .some(alertType => alertType.alertType.reportType.name === alert.alertType.reportType.name);
        });

        return {
          companyId: companyId,
          driverAlerts: alerts.filter(alert => alert.alertType.reportType.category === ReportCategory.Driver).length,
          vehicleAlerts: alerts.filter(alert => alert.alertType.reportType.category === ReportCategory.Vehicle).length,
        }
      });
  };

  getAlertTypeSummary = (companyId: string): Promise<IAlertTypeSummary[]> => {
    return Promise.all([
      this.getAll(companyId),
      this.companyRepository.get(companyId)
    ])
      .then(results => {
        let alerts: IAlert[] = results[0];
        const company = results[1];

        if (!alerts) return;

        // ensure we only include allowed alert Types for the company
        alerts = alerts.filter(alert => {
          return company.alertTypes
            .some(alertType => alertType.alertType.reportType.name === alert.alertType.reportType.name);
        });

        const groups = _.groupBy(alerts.map(alert => (<any>{ name: alert.alertType.reportType.name, alert: alert })), 'name');

        const summaries = Object.keys(groups).map(key => {
          const alert:IAlert = groups[key].length > 0
            ? groups[key][0].alert
            : null;

          if (!alert) return null;

          return <IAlertTypeSummary>{
            key: key,
            category: alert.alertType.reportType.category,
            priority: alert.alertType.priority,
            alertType: alert.alertType,
            count: groups[key].length,
            alerts: groups[key]
          }
        });

        return summaries;
      })
  };

  getAll = (companyId: string): Promise<IAlert[]> => {
    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return ServerError('Invalid company id.', codes.BAD_REQUEST);
    }

    return this.alertRepository.find({_companyId: new mongoose.Types.ObjectId(companyId), status: 'Active'});
  };

  get = (id: string): Promise<IAlert> => {
    return this.alertRepository.get(id);
  };

  find = (criteria: any): Promise<IAlert[]> => {
    return this.alertRepository.find(criteria);
  };

  search(criteria: any, page: number, pageSize: number)
    : Promise<IAlertSearchResults> {
    return this.alertRepository.find(criteria, page, pageSize)
      .then(alerts => {
        return this.alertRepository.count(criteria)
          .then(total => total != null && total.length > 0 ? total[0].count : 0)
          .then(count => {
            return {
              total: count,
              page: page,
              items: alerts
            }
          })
      })
  }

  add = (companyId: string, alert: IAlert): Promise<IAlert> => {

    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.companyRepository.get(companyId)
      .then(company => {
        if (!company) {
          throw ServerError(`Can not add alert. Company ${companyId} not found.`, codes.NOT_FOUND);
        }

        alert._companyId = companyId;

        return this.alertRepository.add(alert);
      });
  };

  update = (alert: IAlert): Promise<IAlert> => {
    return this.alertRepository.update(alert);
  };
}