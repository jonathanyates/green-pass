import {Component, OnInit} from '@angular/core';
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ICompanyDriverCompliance, ICompliance} from "../../../../../../shared/interfaces/compliance.interface";
import {BaseComponent} from "../../shared/components/base/base.component";
import {SelectSelectedCompliance} from "../../../store/compliance/compliance.selectors";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {IComplianceCardModel} from "../../shared/components/compliance-card/compliance-card.component";
import {Observable} from "rxjs/Observable";
import {SelectCompany} from "../../../store/company/company.selectors";

@Component({
  selector: 'gp-drivers-compliance',
  template: `
    <gp-compliance-cards [model]="model" (back)="back()"></gp-compliance-cards>
  `
})

export class DriversComplianceComponent extends BaseComponent implements OnInit {

  model: IComplianceCardModel[];

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private router: Router) {
    super();
  }

  ngOnInit() {
    Observable.combineLatest(
      this.store.let(SelectSelectedCompliance())
        .filter(x => x != null)
        .map(compliance => compliance.driver),
      this.store.let(SelectCompany()))
      .subscribe(result => {
        let compliance = result[0];
        let company = result[1];

        this.model = [
          {id: 'compliant', header: 'Drivers compliant', chartText: 'Compliant', percentage: compliance.compliant},
          {id: 'details', header: 'Driver details', chartText: 'Complete', percentage: compliance.details},
          {id: 'nok', header: 'Driver Next of Kin', chartText: 'Complete', percentage: compliance.nextOfKin},
          {id: 'licence', header: 'Driver licence checks', chartText: 'Complete', percentage: compliance.licence},
          {id: 'documents', header: 'Driver documents', chartText: 'Complete', percentage: compliance.documents},
        ];

        if (company.settings.drivers.references) {
          this.model.push({id: 'references', header: 'Driver References', chartText: 'Complete', percentage: compliance.references});
        }

        if (company.subscription.resources.onLineAssessments) {
          this.model.push({id: 'training', header: 'Driver training', chartText: 'Complete', percentage: compliance.assessments});
        }

        if (compliance.vehicles) {
          this.model.push({id: 'vehicles', header: 'Driver vehicles', chartText: 'Complete', percentage: compliance.vehicles});
        }

        if (compliance.insurance) {
          this.model.push({id: 'insurance', header: 'Driver insurance', chartText: 'Complete', percentage: compliance.insurance});
        }
      })
  }

  back() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }
}
