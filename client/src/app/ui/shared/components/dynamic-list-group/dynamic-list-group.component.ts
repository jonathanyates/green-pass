import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IListGroupItem} from "app/ui/shared/components/dynamic-list-group/dynamic-list-group";
import {isAddress} from "../../../../../../../shared/interfaces/type.guards";
import {Store} from "@ngrx/store";
import {AppState} from "app/store/app.states";
import {SelectIsAdmin} from "../../../../store/authentication/authentication.selectors";

@Component({
  selector: 'gp-dynamic-list-group',
  templateUrl: 'dynamic-list-group.component.html',
  styleUrls: ['dynamic-list-group.component.css']
})
export class DynamicListGroupComponent implements OnInit {

  private canEdit: boolean;

  @Input() items: IListGroupItem[];
  @Input() allowEdit: boolean = false;

  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() add: EventEmitter<any> = new EventEmitter<any>();

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.let(SelectIsAdmin())
      .subscribe(isAdmin => {
        this.canEdit = (this.allowEdit || isAdmin === true) && this.edit.observers.length > 0
      })
  }

  onEdit() {
    this.edit.emit();
  }

  onAdd() {
    this.add.emit();
  }

  isAddressType(value: any) {
    if (value) {
      return isAddress(value);
    }
    return false;
  }
}
