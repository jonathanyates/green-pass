import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import codes from '../shared/error.codes';
import { IReportTypeService } from './report-type.service';

export interface IReportTypeController {
  getAll(req: Request, res: Response, next: NextFunction);
  get(req: Request, res: Response, next: NextFunction);
  find(req: Request, res: Response, next: NextFunction);
  add(req: Request, res: Response, next: NextFunction);
  update(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class ReportTypeController implements IReportTypeController {
  constructor(@inject(TYPES.services.ReportTypeService) private reportTypeService: IReportTypeService) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {
    this.reportTypeService
      .getAll()
      .then((reports) => {
        return res.status(codes.OK).json(reports);
      })
      .catch((err) => {
        next(err);
      });
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.reportTypeId;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid report type id.' });

    this.reportTypeService
      .get(id)
      .then((report) => {
        if (!report) {
          return res.status(codes.NOT_FOUND).send({ message: 'Report not found.' });
        }

        return res.status(codes.OK).json(report);
      })
      .catch((err) => {
        next(err);
      });
  };

  find(req: Request, res: Response, next: NextFunction) {
    const criteria = {};

    if (req.query) {
      for (const param in req.query) {
        if (req.query.hasOwnProperty(param)) {
          criteria[param] = req.query[param];
        }
      }
    }

    this.reportTypeService
      .find(criteria)
      .then((reports) => res.status(codes.OK).json(reports))
      .catch((err) => next(err));
  }

  add = (req: Request, res: Response, next: NextFunction) => {
    const report = req.body;

    if (!report) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No report specified.' });
    }

    this.reportTypeService
      .add(report)
      .then((report) => {
        return res.status(codes.CREATED).json(report);
      })
      .catch((err) => {
        next(err);
      });
  };

  update = (req: Request, res: Response, next: NextFunction) => {
    const report = req.body;

    if (!report) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No report specified.' });
    }

    this.reportTypeService
      .update(report)
      .then((report) => {
        return res.status(codes.ACCEPTED).json(report);
      })
      .catch((err) => {
        next(err);
      });
  };
}
