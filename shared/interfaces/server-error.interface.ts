export interface IServerError extends Error {
  statusCode: number
}