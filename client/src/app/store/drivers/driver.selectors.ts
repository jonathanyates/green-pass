import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {DriversState} from "./drivers.state";
import {Observable} from "rxjs/Observable";
import {SelectCompany} from "../company/company.selectors";
import {SelectLoggedInUser} from "../authentication/authentication.selectors";
import {Roles} from "../../../../../shared/constants";
import {IDriver} from "../../../../../shared/interfaces/driver.interface";
import {ICompany} from "../../../../../shared/interfaces/company.interface";
import {IUser} from "../../../../../shared/interfaces/user.interface";

export const SelectDriversState = () => {
  return (store:Store<AppState>) => {
    return store.select<DriversState>(state => state.drivers);
  }
};

export const SelectDrivers = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => state.list);
  }
};

export const SelectDriversPagination = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => ({
        total: state.total,
        page: state.page,
        drivers: state.list,
      }));
  }
};

export const SelectDriver = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => state.selected);
  }
};

export const SelectDriverLicence = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriver())
      .filter(driver => driver !== null && driver.licence !== null)
      .map(driver => driver.licence.checks)
      .filter(checks => checks && checks.length > 0)
      .map(checks => checks.sort((a, b) => b.checkDate.getTime() - a.checkDate.getTime())[0])
  }
};

export const SelectDriverReference = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => state.selectedReference);
  }
};

export const SelectSelectedDriverDetailsTab = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => state.selectedTab);
  }
};

export const SelectAssessmentsLoggedIn = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectDriversState())
      .map(state => state.assessmentLoggedIn);
  }
};

export interface IDriverUserInfo {
  driver: IDriver,
  company: ICompany,
  user: IUser,
  allowInput: boolean
}

export const SelectDriverUserInfo = () => {
  return (store:Store<AppState>): Observable<IDriverUserInfo> => {
    return Observable.combineLatest(
      store.let(SelectDriver()).filter(driver => driver != null),
      store.let(SelectCompany()).filter(company => company != null),
      store.let(SelectLoggedInUser())
    )
    .map(results => {
      let driver = results[0];
      let company = results[1];
      let user = results[2];
      let isDriver = this.user && this.user.role == Roles.Driver;
      let allowInput = isDriver === false || company.settings.drivers.allowInput;

      return {
        driver: driver,
        company: company,
        user: user,
        allowInput: allowInput
      };
    })
  }
};
