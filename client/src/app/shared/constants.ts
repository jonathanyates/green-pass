import { environment } from '../../environments/environment';

export const apiPath = environment.apiRoot + '/api';

export const Paths = {
  Root: '/',
  Admin: '/admin',
  Companies: '/admin/companies',
  Company: '/company',
  Compliance: 'compliance',
  Users: 'users',
  Depots: 'depots',
  Suppliers: 'suppliers',
  Drivers: 'drivers',
  Driver: '/driver',
  Documents: 'documents',
  Document: 'document',
  Details: 'details',
  Licence: 'licence',
  LicenceCheck: 'licenceCheck',
  NextOfKin: 'nextofkin',
  Assessments: 'assessments',
  RoadMarque: 'roadMarque',
  Vehicles: 'vehicles',
  References: 'references',
  Services: 'services',
  Inspections: 'inspections',
  Checks: 'checks',
  Insurance: 'insurance',
  Faw: 'faw',
  Vehicle: 'vehicle',
  Alerts: 'alerts',
  AlertTypes: 'alertTypes',
  Reports: 'reports',
  Schedule: 'schedule',
  ReportTypes: 'reportTypes',
  Templates: 'templates',
  Subscription: 'subscription',
  Subscriptions: 'subscriptions',
  Settings: 'settings',
  New: 'new',
  Edit: 'edit',
  Add: 'add',
  Manage: 'manage',
  Login: '/login',
  ForgotPassword: 'forgotPassword',
  ChangePassword: 'changePassword',
  Unauthorized: '/unauthorized',
  Error: '/error',
  Import: 'import',
};

export const pagination = {
  pageSize: 10,
};

export const Messages = {
  ShowSearch: 'ShowSearch',
  SearchTerm: 'SearchTerm',
  VehicleSearchError: 'VehicleSearchError',
};

export const FileTypes = {
  image: 'image',
  pdf: 'pdf',
};
