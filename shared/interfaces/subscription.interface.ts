export interface ISubscription {
  _id?: any;
  name: string;
  resources: {
    mandates: boolean;
    licenceCheck: boolean;
    onLineAssessments: boolean;
    onRoadTraining: boolean;
    alerts: number;
    presentations: boolean;
    workshops: boolean;
  };
}

export interface ITrialSubscription {
  endDate: Date;
  subscription: ISubscription;
}

export interface ICompanySubscription extends ISubscription {
  startDate: Date;
  renewalDate: Date;
  active: boolean;
  trial?: ITrialSubscription;
}

export const subscriptionNames = {
  bronze: 'Bronze',
  silver: 'Silver',
  gold: 'Gold',
};

export const subscriptions: ISubscription[] = [
  {
    name: subscriptionNames.bronze,
    resources: {
      mandates: true,
      licenceCheck: true,
      onLineAssessments: false,
      onRoadTraining: false,
      alerts: 2,
      presentations: false,
      workshops: false,
    },
  },
  {
    name: subscriptionNames.silver,
    resources: {
      mandates: true,
      licenceCheck: true,
      onLineAssessments: true,
      onRoadTraining: false,
      alerts: 5,
      presentations: false,
      workshops: false,
    },
  },
  {
    name: subscriptionNames.gold,
    resources: {
      mandates: true,
      licenceCheck: true,
      onLineAssessments: true,
      onRoadTraining: true,
      alerts: -1, // unlimited
      presentations: true,
      workshops: true,
    },
  },
];
