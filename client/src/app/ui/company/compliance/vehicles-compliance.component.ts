import {Component, OnInit} from '@angular/core';
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ICompanyVehicleCompliance} from "../../../../../../shared/interfaces/compliance.interface";
import {BaseComponent} from "../../shared/components/base/base.component";
import {SelectSelectedCompliance} from "../../../store/compliance/compliance.selectors";
import {ActivatedRoute, Router} from "@angular/router";
import {IComplianceCardModel} from "../../shared/components/compliance-card/compliance-card.component";
import {Observable} from "rxjs/Observable";
import {SelectCompany} from "../../../store/company/company.selectors";

@Component({
  selector: 'gp-vehicles-compliance',
  template: `
    <gp-compliance-cards [model]="model" (back)="back()"></gp-compliance-cards>
  `
})

export class VehiclesComplianceComponent extends BaseComponent implements OnInit {

  model: IComplianceCardModel[];

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private router: Router) {
    super();
  }

  ngOnInit() {
    Observable.combineLatest(
      this.store.let(SelectSelectedCompliance())
        .filter(x => x != null)
        .map(compliance => compliance.vehicle),
      this.store.let(SelectCompany()))
      .subscribe(results => {
        let compliance = results[0];
        let company = results[1];

        this.model = [
          {id: 'compliant', header: 'Vehicles compliant', chartText: 'Compliant', percentage: compliance.compliant},
          {id: 'tax', header: 'Vehicle Tax', chartText: 'Valid', percentage: compliance.tax},
          {id: 'mot', header: 'Vehicle Mot', chartText: 'Valid', percentage: compliance.mot}
        ];

        if (!company.settings.vehicles.grey.exclusions.service) {
          this.model.push({id: 'service', header: 'Vehicle services', chartText: 'Valid', percentage: compliance.service});
        }

        if (!company.settings.vehicles.grey.exclusions.inspection) {
          this.model.push({id: 'inspection', header: 'Vehicle inspections', chartText: 'Complete', percentage: compliance.inspection});
        }

        if (!company.settings.vehicles.grey.exclusions.checks) {
          this.model.push({id: 'checks', header: 'Vehicle daily checks', chartText: 'Complete', percentage: compliance.checks});
        }
      })
  }

  back() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }
}
