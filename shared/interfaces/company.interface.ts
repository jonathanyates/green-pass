import '../extensions/date.extensions';
import {IAddress} from "./address.interface";
import {ICompanyAlertType} from "./alert.interface";
import {IFleetInsurance} from "./common.interface";
import {ICompanySettings} from "./company-settings.interface";
import {ICompanySubscription} from "./subscription.interface";

export interface IDepot {
  _id?: any;
  _companyId?: string,
  name: string,
  address: IAddress,
  phone: string,
  mobile?: string,
  email?: string
}

export interface ISupplier {
  _id?: any;
  _companyId?: string,
  name: string,
  address: IAddress,
  phone: string,
  mobile?: string,
  email?: string
}

export interface ICompany {
  _id?: any
  name: string,
  coreBusiness: string,
  address: IAddress,
  noOfEmployees?: number,
  phone: string,
  mobile?: string,
  email: string,
  controllingMind?: {
    name: string,
    position: string
  },
  depots?: IDepot[],
  suppliers?: ISupplier[],
  users?: number,
  vehicles?: number,
  drivers?: number,
  fleetInsuranceHistory?: IFleetInsurance[],
  templates?: string[],
  alertTypes?: ICompanyAlertType[],
  settings?: ICompanySettings,
  subscription: ICompanySubscription,
  removed?: boolean
}



