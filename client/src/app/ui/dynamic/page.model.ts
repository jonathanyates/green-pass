export interface IPage {
  _id: string;
  pageId:  string;
  content: string;
}

export class Page implements  IPage{
  _id: string;
  pageId:  string;
  content: string;
}
