import 'reflect-metadata';
import { DvlaService } from '../src/dvla/service/dvla.service';

describe('Dvla Service', () => {
  describe.skip('vehicleEnquiry', () => {
    it('gets vehicle Enquiry', async () => {
      const registrationNumber = 'E18EBY';
      const dvlaService = new DvlaService();
      const result = await dvlaService.vehicleEnquiry(registrationNumber);
      expect(result).not.toBeNull();
    });
  });

  describe.skip('drivingLicenceEnquiry', () => {
    it('gets driving Licence Enquiry', async () => {
      const drivingLicenceNumber = 'YATES607198JP9LK',
        nationalInsuranceNumber = 'NR071159A',
        postCode = 'PR31YF';

      const dvlaService = new DvlaService();
      const result = await dvlaService.drivingLicenceEnquiry(drivingLicenceNumber, nationalInsuranceNumber, postCode);
      expect(result).not.toBeNull();
    });
  });

  describe('motEnquiry', () => {
    it('gets motEnquiry', async () => {
      const registrationNumber = 'E18EBY';

      const dvlaService = new DvlaService();
      const result = await dvlaService.motEnquiry(registrationNumber);
      expect(result).not.toBeNull();
    });
  });
});
