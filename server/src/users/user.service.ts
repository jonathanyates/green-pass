import logger = require('winston');
import generator = require('generate-password');
import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { IUser } from '../../../shared/interfaces/user.interface';
import { IRepository } from '../db/db.repository';
import { TYPES } from '../container/container.types';
import { IApiService } from '../interfaces/api-service.interface';
import { ICompanyModel } from '../companies/company.schema';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';
import { roles } from '../security/authorization/authorization.middleware';
import { ICompany } from '../../../shared/interfaces/company.interface';
import serverConfig from '../server/server.config';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IEmail } from '../email/email.service';
import { getUserLoginEmail } from './user.emails';
import { ICompanyRepository } from '../companies/company.repository';
import { IDbContext } from '../db/db.context.interface';
import { RegExpPatterns } from '../../../shared/utils/regex';

const DefaultPassword = 'greenpass';

export interface IUserService extends IApiService<IUser> {
  remove(userId: string): Promise<IUser>;
  createUserAccount(user: IUser, company: ICompany): Promise<IUser>;
}

@injectable()
export class UserService implements IUserService {
  constructor(
    @inject(TYPES.repositories.UserRepository) private userRepository: IRepository<IUser>,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.MessageService) private messageService: IMessageService,
    @inject(TYPES.db.DbContext) private dbContext: IDbContext
  ) {}

  getAll = (companyId: string): Promise<IUser[]> => {
    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.userRepository.find({ _companyId: companyId, role: 'company' });
  };

  get = (id: string): Promise<IUser> => {
    return this.userRepository.get(id);
  };

  find(criteria: any): Promise<IUser[]> {
    return this.userRepository.find(criteria);
  }

  findOne(criteria: any): Promise<IUser> {
    return this.userRepository.findOne(criteria);
  }

  createUserAccount = async (user: IUser, company: ICompany): Promise<IUser> => {
    if (user.email != null && user.email.match(RegExpPatterns.Email)) {
      // ensure no white space and lowercase email address
      user.email = user.email.toLowerCase().trim();

      const exiting = await this.userRepository.findOne({ username: user.email });

      let username;

      if (exiting) {
        username = await this.createUsername(user);
      } else {
        username = user.email;
      }

      user.username = username;
      const password = this.generatePassword();
      user.password = password;
      user.changePassword = true;
      user = await this.userRepository.add(user);

      if (serverConfig.email.sendEmails) {
        try {
          const sent = await this.emailUserLoginDetails(user, password, company);

          if (sent) {
            logger.info('Successfully sent account details email to ' + user.username);
          } else {
            logger.error('Failed to send account details email to ' + user.username);
          }
          return user;
        } catch (ex) {
          logger.error(`Failed to send account details email to ${user.username}. Error: ${ex}`);
          return user;
        }
      }
    } else {
      // Alternative account creation if user does not have an email address.
      const username = await this.createUsername(user);
      user.username = username;
      user.password = DefaultPassword; // This must be changed at fist login
      user.changePassword = true;
      const newUser = await this.userRepository.add(user);
      return newUser;
    }
  };

  add = async (companyId: string, user: IUser): Promise<IUser> => {
    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return ServerError('Invalid company id.', codes.BAD_REQUEST);
    }

    const company = await this.companyRepository.get(companyId);

    if (!company) {
      throw ServerError('Company not found.', codes.NOT_FOUND);
    }

    user._companyId = companyId;
    user.role = roles.company;

    user = await this.createUserAccount(user, company);
    return user;
  };

  update = (user: IUser): Promise<IUser> => {
    return this.userRepository.update(user);
  };

  remove = (userId: string): Promise<IUser> => {
    return this.userRepository.remove(userId);
  };

  private createUsername(user: IUser, count = 0): Promise<string> {
    let username = `${user.forename}.${user.surname}`
      .replace(/\s+/g, '') // remove spaces
      .toLowerCase();

    if (count > 0) {
      username += count;
    }
    return this.dbContext.user.findOne({ username: username }).then((existing) => {
      if (existing) {
        return this.createUsername(user, ++count);
      }
      return username;
    });
  }

  private generatePassword() {
    if (process.env.NODE_ENV === 'production') {
      return generator.generate({
        length: 10,
        numbers: true,
      });
    }

    return DefaultPassword;
  }

  private emailUserLoginDetails(user: IUser, password: string, company: ICompany) {
    const text = getUserLoginEmail(user, password, company);

    return this.messageService.send(queueNames.sendEmail, <IEmail>{
      recipients: [user.email],
      from: 'admin@greenpasscompliance.co.uk',
      subject: `Your ${company.name} Green Pass account details`,
      content: text,
    });
  }
}
