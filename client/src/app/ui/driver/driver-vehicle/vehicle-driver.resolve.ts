import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {LoadDriverAction, LoadDriversAction} from "../../../store/drivers/driver.actions";
import {pagination} from "../../../shared/constants";
import {LoadVehicleAction} from "../../../store/vehicles/vehicle.actions";

@Injectable()
export class VehicleDriverResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    let vehicleId = route.params['vehicleId'];

    if (!companyId || !vehicleId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new LoadVehicleAction(companyId, vehicleId));

    let driverId = route.params['driverId'];

    if (driverId) {
      this.store.dispatch(new LoadDriverAction(companyId, driverId));
    } else {
      this.store.dispatch(new LoadDriversAction(companyId, 1, pagination.pageSize));
    }

    return true;
  }

}
