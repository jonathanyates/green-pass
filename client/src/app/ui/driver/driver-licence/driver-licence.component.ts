import {Component, Input} from '@angular/core';
import {IDrivingLicence} from "../../../../../../shared/interfaces/driver.interface";
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {FormatDate} from "../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-driver-licence',
  template: `<gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>`

})
export class DriverLicenceComponent {

  items: IListGroupItem[];
  private _licence: IDrivingLicence;

  @Input() nextCheckDate: Date;

  @Input() set licence(licence: IDrivingLicence) {
    this._licence = licence;
    this.items = [
      {title: "Licence number", value: licence ? licence.number : null},
      {title: "Issue number", value: licence ? licence.issueNumber : null},
      {title: "Status", value: licence ? licence.status : null},
      {title: "Valid from", value: licence ? FormatDate(licence.validFrom) : null},
      {title: "Valid to", value: licence ? FormatDate(licence.validTo) : null},
      {title: "Licence Check Date", value: licence ? FormatDate(licence.checkDate) : null},
      {title: "Next Licence Check Date", value: this.nextCheckDate ? FormatDate(this.nextCheckDate) : null},

    ];
  }
  get licence() { return this._licence; }
}
