import { add, transports, query } from 'winston';

add(transports.File, { filename: 'D:\\Playground\\green-pass\\server\\build\\server\\log\\2017-05-17.log' });

let options = {
  from: new Date() - 24 * 60 * 60 * 1000,
  until: new Date(),
  limit: 10,
  start: 0,
  order: 'desc',
  fields: ['message'],
};

//
// Find items logged between today and yesterday.
//
query(options, function (err, results) {
  if (err) {
    throw err;
  }
  console.log(results);
});
