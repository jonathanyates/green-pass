export interface IAddress {
  line1?: string,
  line2?: string,
  line3?: string,
  line4?: string,
  locality?: string,
  townCity?: string,
  county?: string,
  postcode?: string,
  country?: string
}

export interface IAddressList {
  postcode: string,
  addresses: IAddress[]
}