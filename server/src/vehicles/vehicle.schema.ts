import { Schema, Document } from 'mongoose';
import { IVehicle } from '../../../shared/interfaces/vehicle.interface';
import { VehicleTypes } from '../../../shared/interfaces/constants';

// http://www.licencecheck.co.uk/driving-licence-category-codes
const vehicleCategories = ['A1', 'A', 'B1', 'B', 'B+E', 'C1', 'C1+E', 'C', 'C+E', 'D1', 'D1+E', 'D', 'D+E', ''];
const vehicleType = [VehicleTypes.fleet, VehicleTypes.grey, ''];

export const VehicleSchema = new Schema(
  {
    _companyId: { type: Schema.Types.ObjectId, ref: 'Company', required: true },
    make: { type: String, required: true },
    model: { type: String, required: true },
    registrationNumber: { type: String, required: true },
    category: { type: String, enum: vehicleCategories, default: '', required: false },
    type: { type: String, enum: vehicleType, default: 'Fleet', required: false },

    dvlaCheckDate: { type: Date, required: false },
    taxDueDate: { type: Date, required: false },
    motDueDate: { type: Date, required: false },
    motDueText: { type: String, required: false },

    serviceDueDate: { type: Date, required: false },
    inspectionDueDate: { type: Date, required: false },
    vehicleChecksDueDate: { type: Date, required: false },

    dateOfFirstRegistration: { type: Date, required: false },
    manufactureDate: { type: Date, required: false },
    yearOfManufacture: { type: Number, required: false },
    firstUsedDate: { type: Date, required: false },
    cylinderCapacityCc: { type: String, required: false },
    co2Emissions: { type: String, required: false },
    fuelType: { type: String, required: false },
    exportMarker: { type: String, required: false },
    vehicleStatus: { type: String, required: false },
    vehicleColour: { type: String, required: false },
    vehicleTypeApproval: { type: String, required: false },
    wheelplan: { type: String, required: false },
    revenueWeight: { type: String, required: false },

    euroStatus: { type: String, required: false },
    realDrivingEmissions: { type: String, required: false },
    dateOfLastv5cIssued: { type: Date, required: false },

    motHistory: [
      {
        testDate: { type: Date, required: false },
        expiryDate: { type: Date, required: false },
        testResult: { type: String, required: false },
        odometerReading: { type: String, required: false },
        odometerUnit: { type: String, required: false },
        motTestNumber: { type: String, required: false },
        reasonsForFailure: [{ type: String, required: false }],
        advisoryNotices: [{ type: String, required: false }],
        reasonsForRejection: [
          {
            text: { type: String, required: true },
            type: { type: String, required: true },
            dangerous: { type: Boolean, required: true },
            advice: { type: String, required: false },
          },
        ],

        // added for HGV/PSV Annual Tests
        testType: { type: String, required: false },
        testCertificateNumber: { type: String, required: false },
        location: { type: String, required: false },
        numberOfDefectsAtTest: { type: String, required: false },
        numberOfAdvisoryDefectsAtTest: { type: String, required: false },
      },
    ],
    serviceHistory: [
      {
        serviceDate: { type: Date, required: true },
        supplierId: { type: Schema.Types.ObjectId, required: true },
        filename: { type: String, required: false },
        fileType: { type: String, required: false },
      },
    ],
    inspectionHistory: [
      {
        inspectionDate: { type: Date, required: true },
        supplierId: { type: Schema.Types.ObjectId, required: true },
        filename: { type: String, required: false },
        fileType: { type: String, required: false },
      },
    ],
    checkHistory: [
      {
        checkDate: { type: Date, required: true },
        checkedBy: { type: String, required: true },
        filename: { type: String, required: false },
        fileType: { type: String, required: false },
        odometerReading: { type: Number, required: false },
      },
    ],
    nextService: { type: Date, required: false },
    nextInspection: { type: Date, required: false },
    nextVehicleCheck: { type: Date, required: false },

    annualMileage: { type: Number, required: false },
    inspectionIntervalType: { type: String, required: false },

    removed: { type: Boolean, default: false, required: false },

    // added fields for HGV/PSV Tests
    vehicleType: { type: String, required: false },
    class: { type: String, required: false },
    annualTestHistory: [
      {
        testDate: { type: Date, required: false },
        testType: { type: String, required: false },
        testResult: { type: String, required: false },
        testCertificateNumber: { type: String, required: false },
        expiryDate: { type: Date, required: false },
        numberOfDefectsAtTest: { type: String, required: false },
        numberOfAdvisoryDefectsAtTest: { type: String, required: false },
        location: { type: String, required: false },
        defects: [
          {
            failureItemNo: { type: String, required: false },
            failureReason: { type: String, required: false },
            severityCode: { type: String, required: false },
            severityDescription: { type: String, required: false },
          },
        ],
      },
    ],
  },
  { timestamps: true }
);

export interface IVehicleModel extends Document, IVehicle {
  _id: any;
  model: any;
}
