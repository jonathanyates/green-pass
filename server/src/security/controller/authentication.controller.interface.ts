import {NextFunction, Request, Response} from "express-serve-static-core";

export interface IAuthenticationController {

  login(req: Request, res: Response, next: NextFunction);

  authenticate(req: Request, res: Response, next: NextFunction);

  forgot(req: Request, res: Response, next: NextFunction);

  verifyResetToken(req: Request, res: Response, next: NextFunction);

  reset(req: Request, res: Response, next: NextFunction);

  change(req: Request, res: Response, next: NextFunction);
}