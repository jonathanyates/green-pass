import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {AppState} from "../../../store/app.states";
import {AuthenticationState} from "../../../store/authentication/authentication.reducer";
import * as actions from "../../../store/authentication/authentication.actions";
import {SelectAuthenticationState} from "../../../store/authentication/authentication.selectors";
import {Paths} from "../../../shared/constants";

@Component({
  selector: 'gp-login-status',
  templateUrl: 'login-status.component.html',
  styleUrls: ['login-status.component.css']
})
export class LoginStatusComponent implements OnInit {

  public state: AuthenticationState;

  constructor(
    private store: Store<AppState>,
    public router: Router) {
  }

  ngOnInit() {
    this.store.let(SelectAuthenticationState())
      .distinctUntilChanged()
      .subscribe((state:AuthenticationState) => {
        this.state = state;
    });
  }

  login() {
    this.router.navigate([Paths.Login]);
  }

  logout() {
    this.store.dispatch(new actions.LogoutAction(this.state));
  }
}
