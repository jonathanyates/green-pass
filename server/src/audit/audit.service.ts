import { IAudit } from './audit.interface';
import { IQueryableRepository } from '../db/db.repository';
import { TYPES } from '../container/container.types';
import { inject, injectable } from 'inversify';
import { IUser } from '../../../shared/interfaces/user.interface';

export const AuditActions = {
  Login: 'Login',
  ChangePassword: 'ChangePassword',
  Get: 'Get',
  GetAll: 'GetAll',
  Add: 'Add',
  Import: 'Import',
  Update: 'Update',
  Remove: 'Remove',
  Completed: 'Completed',
};

export const AuditTargets = {
  Company: 'Company',
  Driver: 'Driver',
  Drivers: 'Drivers',
  Vehicle: 'Vehicle',
  Service: 'Service',
  Inspection: 'Inspection',
  VehicleCheck: 'VehicleCheck',
  VehicleInsurance: 'VehicleInsurance',
  DriverInsurance: 'DriverInsurance',
  DriverVehicle: 'DriverVehicle',
  CompanyInsurance: 'CompanyInsurance',
  User: 'User',
  AlertType: 'AlertType',
  Assessment: 'Assessment',
  CheckCode: 'CheckCode',
  LicenceCheck: 'LicenceCheck',
  DrivingLicence: 'DrivingLicence',
  Template: 'Template',
  FawCertificate: 'FawCertificate',
};

export const AuditResults = {
  Success: 'Success',
  Failure: 'Failure',
};

export interface IAuditService {
  getAll(companyId: string): Promise<IAudit[]>;
  get(id: string): Promise<IAudit>;
  find(criteria: any): Promise<IAudit[]>;
  add(audit: IAudit): Promise<IAudit>;
  audit(user: Partial<IUser>, action: string, target: string, result: any);
  auditError(user: Partial<IUser>, action: string, target: string, error: any);
  systemAudit(action: string, target?: string, result?: any, error?: any);
}

@injectable()
export class AuditService implements IAuditService {
  constructor(@inject(TYPES.repositories.AuditRepository) private repository: IQueryableRepository<IAudit>) {}

  getAll(companyId: string): Promise<IAudit[]> {
    return this.repository.find({ _companyId: companyId });
  }

  get(id: string): Promise<IAudit> {
    return this.repository.get(id);
  }

  find(criteria: any): Promise<IAudit[]> {
    return this.repository.find(criteria);
  }

  add(audit: IAudit): Promise<IAudit> {
    return this.repository.add(audit);
  }

  audit(user: Partial<IUser>, action: string, target: string, result: any) {
    const audit = <IAudit>{
      userId: user._id,
      _companyId: user._companyId,
      role: user.role,
      action: action,
      target: target,
      result: result,
      dateTime: new Date(),
    };

    return this.repository.add(audit);
  }

  auditError(user: Partial<IUser>, action: string, target: string, error: any) {
    const audit = <IAudit>{
      userId: user._id,
      _companyId: user._companyId,
      role: user.role,
      action: action,
      target: target,
      error: error,
      dateTime: new Date(),
    };

    return this.repository.add(audit);
  }

  systemAudit(action: string, target?: string, result?: any, error?: any) {
    const audit = <IAudit>{
      role: 'System',
      action: action,
      target: target,
      result: result,
      error: error,
      dateTime: new Date(),
    };

    return this.repository.add(audit);
  }
}
