import mongoose from 'mongoose';
import { IAlertType } from '../../../shared/interfaces/alert.interface';

export const AlertTypeSchema = new mongoose.Schema({
  _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
  reportType: { type: mongoose.Schema.Types.ObjectId, ref: 'ReportType', required: false },
  priority: { type: String, enum: ['Critical', 'High', 'Medium', 'Low'], default: 'Low', required: true },
});

export interface IAlertTypeModel extends mongoose.Document, IAlertType {
  _id: any;
}
