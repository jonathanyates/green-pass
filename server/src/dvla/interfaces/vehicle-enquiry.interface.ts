export interface IVehicleEnquiry {
  registrationNumber: string;

  taxDueDate: Date;

  motDueDate: Date;
  motDueText: string;

  vehicleMake: string;
  vehicleModel?: string;
  dateOfFirstRegistration?: Date;
  yearOfManufacture: number;
  cylinderCapacityCc: string;
  co2Emissions: string;
  fuelType: string;
  exportMarker: string;
  vehicleStatus: string;
  vehicleColour: string;
  vehicleTypeApproval: string;
  wheelplan: string;
  revenueWeight: string;

  euroStatus: string;
  realDrivingEmissions: string;
  dateOfLastv5cIssued: Date;
}

export interface IDvlaVehicleEnquiryResponse {
  registrationNumber: string;
  artEndDate: string;
  co2Emissions: number;
  colour: string;
  engineCapacity: number;
  fuelType: string;
  make: string;
  markedForExport: boolean;
  monthOfFirstDvlaRegistration: string;
  monthOfFirstRegistration: string;
  motStatus: string;
  motExpiryDate: string;
  revenueWeight: number;
  taxDueDate: string;
  taxStatus: string;
  typeApproval: string;
  wheelplan: string;
  yearOfManufacture: number;
  euroStatus: string;
  realDrivingEmissions: string;
  dateOfLastV5CIssued: string;
}
