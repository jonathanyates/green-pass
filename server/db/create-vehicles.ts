import 'reflect-metadata';
import '../../shared/extensions/date.extensions';
import mongoose from 'mongoose';
import { TYPES } from '../src/container/container.types';
import { vehicles } from './vehicles';
import { IVehicleApiService } from '../src/vehicles/vehicle.service';
import { IVehicle, IVehicleCheck } from '../../shared/interfaces/vehicle.interface';
import container from '../src/container/container.config';
import { SequencePromise } from '../../shared/extensions/promise.extensions';
import { VehicleTypes } from '../../shared/interfaces/constants';
import { ICompanyRepository } from '../src/companies/company.repository';

const companyRepository = container.get<ICompanyRepository>(TYPES.repositories.CompanyRepository);
const vehicleService = container.get<IVehicleApiService>(TYPES.services.VehicleService);

const addVehicle = (vehicleInfo, company, vehicleType) => {
  vehicleInfo.reg = vehicleInfo.reg.toUpperCase();
  vehicleInfo.make = vehicleInfo.make && vehicleInfo.make.toUpperCase();

  const vehicle: IVehicle = {
    _companyId: company._id,
    make: vehicleInfo.make,
    model: 'Unknown',
    registrationNumber: vehicleInfo.reg,
    category: 'B',
    type: vehicleType,
    motHistory: [],
    serviceHistory: [],
    inspectionHistory: [],
    checkHistory: [],
    annualMileage: 12000,
    inspectionIntervalType: 'A',
  };

  return vehicleService
    .add(company._id, vehicle)
    .then((vehicle) => {
      return vehicleService
        .addService(company._id, vehicle._id, {
          serviceDate: new Date().addDays(Math.floor(Math.random() * (365 + 7 * 12) + 1) * -1),
          supplierId: new mongoose.Types.ObjectId('012345678901234567892000'),
        })
        .then((service) => {
          return vehicleService.addInspection(company._id, vehicle._id, {
            inspectionDate: new Date().addDays(Math.floor(Math.random() * (7 * 12) + 1) * -1),
            supplierId: new mongoose.Types.ObjectId('012345678901234567892000'),
          });
        })
        .then((inspection) => {
          // work out when the next friday is
          const now = new Date();

          // get the end of last week (sunday)
          const first = now.getDate() - now.getDay();

          // minus 2 days to give the last friday
          const friday = first - 2;

          const multiplier = Math.floor(Math.random() * 2 + 1);
          const checkDate = new Date(now.setDate(friday)).addDays(7 * multiplier);
          const odometerReading = Math.floor(Math.random() * 20 + 10) * 1000;

          return vehicleService.addCheck(company._id, vehicle._id, <IVehicleCheck>{
            checkDate: checkDate,
            checkedBy: 'A Driver',
            odometerReading: odometerReading,
          });
        })
        .then((check) => {
          return vehicle;
        })
        .catch((error) => {
          console.log(error);
          return null;
        });
    })
    .catch((error) => {
      console.log('Unable to add vehicle. Error: ' + error.message);
      return null;
    });
};

companyRepository
  .getAll()
  .then((companies) =>
    Promise.all([
      SequencePromise.sequence(vehicles.slice(0, 20), (vehicle) => {
        // if (vehicles.indexOf(vehicle) < 10) {
        //   let index = Math.floor(Math.random() * 2) + 1;
        //   let vehicleType = index == 1 ? VehicleTypes.grey : VehicleTypes.fleet;
        //   return addVehicle(vehicle, companies[0], vehicleType)
        // }
        return addVehicle(vehicle, companies[0], VehicleTypes.grey).then(function () {
          return new Promise(function (resolve, reject) {
            setTimeout(resolve, 2000);
          });
        });
      }),
    ])
  )
  .then((vehicles) => {
    console.log(`Vehicles created successfully.`);
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
