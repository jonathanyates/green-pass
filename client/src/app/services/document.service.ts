import { Injectable } from '@angular/core';
import {HttpBaseService} from "./http-base.service";
import {apiPath} from "../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {Observable} from "rxjs";
import {IDocumentStatus} from "../../../../shared/interfaces/document.interface";
import {HttpClient, HttpParams} from "@angular/common/http";
import {IDriver} from "../../../../shared/interfaces/driver.interface";
import {ServerError} from "../shared/errors/server.error";

@Injectable()
export class DocumentService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getDocuments(driver: IDriver): Observable<IDocumentStatus[]> {

    this.authorize();

    let url = `${this.companiesUrl}/${driver._companyId}/drivers/${driver._id}/documents`;

    return this.httpClient.get(url, this.getOptions())
      .map(res =>  {
        let result = this.handleResult<any>(res);
        return result;
      })
      .catch(this.handleError);
  }

  getDocumentUrl(driver: IDriver, documentId: string, returnUrl:string): Observable<string> {
    this.authorize();

    let url = `${this.companiesUrl}/${driver._companyId}/drivers/${driver._id}/documents/${documentId}/url`;

    let params = new HttpParams().set('returnUrl', returnUrl);

    return this.httpClient.get(url, this.getOptions(params))
      .map(res =>  {
        let result = this.handleResult<any>(res);
        return result;
      })
      .catch(this.handleError);
  }

  getDocumentPdf(driver: IDriver, documentId: string): Observable<string> {
    this.authorize();

    let options = this.getOptions(null, 'arraybuffer');
    options.headers = options.headers.append('Content-Type', 'application/pdf');

    let url = `${this.companiesUrl}/${driver._companyId}/drivers/${driver._id}/documents/${documentId}/pdf`;

    return this.httpClient.get(url, options)
      .map(result => result)
      .catch(res => {
        return Observable.throw(new ServerError(res.statusText, res.status))
      });
  }

  getNewEmbeddedUrl(driver: IDriver, documentId: string, signerId: string): Observable<string> {
    let url = `${this.companiesUrl}/${driver._companyId}/drivers/${driver._id}/documents/${documentId}/newlink/${signerId}`;

    return this.httpClient.get(url, this.getOptions())
      .map((result: any) =>  {
        if (result) {
          return result.url;
        }
        return null;
      })
      .catch(this.handleError);
  }
}
