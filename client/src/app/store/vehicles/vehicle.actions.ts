import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IVehicle} from "../../../../../shared/interfaces/vehicle.interface";
import {LogoutAction} from "../authentication/authentication.actions";
import {pagination} from "../../shared/constants";

export const ActionTypes = {
  LOAD:                   type('[Vehicles] Load'),
  LOAD_SUCCESS:           type('[Vehicles] Load Success'),
  SELECT_LIST:            type('[Vehicles] Select'),
  SELECT_LIST_SUCCESS:    type('[Vehicles] Select Success'),
  LOAD_VEHICLE:           type('[Vehicle] Load'),
  LOAD_VEHICLE_SUCCESS:   type('[Vehicle] Load Success'),
  SELECT_VEHICLE:         type('[Vehicle] Select'),
  SELECT_VEHICLE_SUCCESS: type('[Vehicle] Select Success'),
  SAVE:                   type('[Vehicle] Save'),
  SAVE_SUCCESS:           type('[Vehicle] Save Success'),
  SAVE_FAILURE:           type('[Vehicle] Save Failure'),
  SELECT_SELECTED_TAB:    type('[Vehicle] Select Selected Tab'),
  CLEAR_LIST:             type('[Vehicle] Clear List'),
  UPDATE_SUCCESS:         type('[Vehicle] Update Success')
};

/**
 * Load Vehicles Actions
 */
export class LoadVehiclesAction implements Action {
  type = ActionTypes.LOAD;
  page: number;
  pageSize: number = pagination.pageSize;
  searchCriteria: any;
  constructor(public payload: string, page?: number, pageSize?: number, searchCriteria?: any) {
    this.page = page;
    this.pageSize = pageSize;
    this.searchCriteria = searchCriteria;
  } //  payload company id
}

export class LoadVehiclesSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  total: number;
  page: number;
  constructor(public payload: IVehicle[], total?: number, page?: number) {
    this.total = total ? total : this.payload.length;
    this.page = page;
  }
}

/**
 * Select Vehicles Actions
 */
export class SelectVehiclesAction implements Action {
  type = ActionTypes.SELECT_LIST;
  total: number;
  constructor(public payload: string) {
  } //  payload company id
}

export class SelectVehiclesSuccessAction implements Action {
  type = ActionTypes.SELECT_LIST_SUCCESS;
  constructor(public payload: IVehicle[]) {
  }
}

/**
 * Load Vehicles Actions
 */
export class LoadVehicleAction implements Action {
  type = ActionTypes.LOAD_VEHICLE;
  payload: { companyId:string, vehicleId:string };

  constructor(companyId:string, vehicleId:string) {
    this.payload = { companyId:companyId, vehicleId:vehicleId }
  }
}

export class LoadVehicleSuccessAction implements Action {
  type = ActionTypes.LOAD_VEHICLE_SUCCESS;
  constructor(public payload: IVehicle) { }
}

/**
 * Select Vehicle Actions
 */
export class SelectVehicleAction implements Action {
  type = ActionTypes.SELECT_VEHICLE;
  payload: { companyId:string, vehicleId:string };

  constructor(companyId:string, vehicleId:string) {
    this.payload = { companyId:companyId, vehicleId:vehicleId }
  }
}

export class SelectVehicleSuccessAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_SUCCESS;
  constructor(public payload: IVehicle) { }
}

/**
 * Save Vehicle Actions
 */
export class SaveVehicleAction implements Action {
  type = ActionTypes.SAVE;
  constructor(public payload: IVehicle) { }
}

export class SaveVehicleSuccessAction implements Action {
  type = ActionTypes.SAVE_SUCCESS;
  constructor(public payload: IVehicle) { }
}

export class SaveVehicleFailureAction implements Action {
  type = ActionTypes.SAVE_FAILURE;
  constructor(public payload: Error) { }
}

export class UpdateVehicleSuccessAction implements Action {
  type = ActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IVehicle) { }
}

/**
 * Selected Tab Actions
 */

export class SelectVehicleDetailsTabAction implements Action {
  type = ActionTypes.SELECT_SELECTED_TAB;
  constructor(public payload: string) {}
}

/**
 * Clear List Action
 */
export class ClearVehicleListAction implements Action {
  type = ActionTypes.CLEAR_LIST;
  payload;
  constructor() { }
}

export type Actions
  = LoadVehiclesSuccessAction
  | SelectVehiclesSuccessAction
  | LoadVehicleSuccessAction
  | SelectVehicleSuccessAction
  | UpdateVehicleSuccessAction
  | SaveVehicleSuccessAction
  | SelectVehicleDetailsTabAction
  | ClearVehicleListAction
  | LogoutAction;
