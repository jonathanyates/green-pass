import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {IPage} from "../page.model";
import {PageService} from "../../../services/page.service";
import {SelectLoggedIn} from "../../../store/authentication/authentication.selectors";
import {LoadPageAction} from "../../../store/page/page.actions";
import {SelectPage} from "../../../store/page/page.selectors";
import {SelectError} from "../../../store/error/error.selectors";
import {ClearErrorAction} from "../../../store/error/error.actions";

@Injectable()
export class DynamicPageResolve implements Resolve<IPage|boolean> {

  loggedIn:boolean;

  constructor(
    private pageService:PageService,
    private store: Store<AppState>,
    private router: Router) {
    this.store.let(SelectLoggedIn()).subscribe(loggedIn => this.loggedIn = loggedIn);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPage|boolean> {

    if (this.loggedIn) return Observable.of(true);

    let pageId = state.url;
    if (!pageId) pageId = '/home';

    this.store.dispatch(new ClearErrorAction());
    this.store.dispatch(new LoadPageAction(pageId));

    return this.store.let(SelectPage())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (page, error) => ({ page:page, error:error }))
      .filter(result => result.error != null || (result.page && result.page.pageId === pageId))
      .map(result => result.error ? false : result.page)
      .take(1);
  }

}
