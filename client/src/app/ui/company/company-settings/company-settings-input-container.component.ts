import {Component, OnInit} from '@angular/core';
import {AppState} from '../../../store/app.states';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {SaveCompanyAction} from '../../../store/company/company.actions';
import {jsonDeserialiser} from '../../../../../../shared/utils/json.deserialiser';
import {SelectCompany} from "../../../store/company/company.selectors";
import {ICompanySettings} from "../../../../../../shared/interfaces/company-settings.interface";
import {BaseComponent} from "../../shared/components/base/base.component";

@Component({
  selector: 'gp-settings-input-container',
  template: `
    <gp-company-settings-input [settings]="settings" (save)="save($event)"></gp-company-settings-input>
  `
})
export class CompanySettingsInputContainerComponent extends BaseComponent implements OnInit {

  inputGroups: FormInputGroup[];
  company: ICompany;
  settings: ICompanySettings;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) {
     super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .subscribe(company => {
        this.company = company;
        this.settings = company.settings;
      }));
  }

  save(settings: ICompanySettings) {
    let company = jsonDeserialiser.parse(JSON.stringify(this.company));
    company.settings = settings;
    this.store.dispatch(new SaveCompanyAction(company));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
