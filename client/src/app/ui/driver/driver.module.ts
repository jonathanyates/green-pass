import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {DriverInputComponent} from "./driver-input/driver-input.component";
import {DriverRoutingModule} from "./driver-routing.module";
import {DriverDocumentsComponent} from "./driver-documents/driver-documents.component";
import {DriverComponent} from "./driver.component";
import {DocumentService} from "../../services/document.service";
import {DriverDocumentComponent} from "./driver-document/driver-document.component";
import { DriverDashboardComponent } from './driver-dashboard/driver-dashboard.component';
import { DriverDetailsComponent } from './driver-details/driver-details.component';
import { DriverLicenceComponent } from './driver-licence/driver-licence.component';
import { DriverEndorsementsComponent } from './driver-endorsements/driver-endorsements.component';
import { AllowedVehiclesComponent } from './allowed-vehicles/allowed-vehicles.component';
import { DriverLicenceContainerComponent } from './driver-licence-container/driver-licence-container.component';
import { DriverAssessmentsComponent } from './driver-assessments/driver-assessments.component';
import { DriverReferenceListComponent } from './driver-reference/driver-reference-list/driver-reference-list.component';
import { DriverDocumentsContainerComponent } from './driver-documents/driver-documents-container.component';
import {DriverInputMapper} from "./driver-input/driver-input.mapper";
import {DriverDetailsTabListComponent} from "./driver-details-tab-list/driver-details-tab-list.component";
import {DriverReferenceDetailComponent } from "./driver-reference/driver-reference-details/driver-reference-details.component";
import {DriverReferenceInputComponent} from "./driver-reference/driver-reference-input/driver-reference-input.component";
import {DriverReferenceInputMapper} from "./driver-reference/driver-reference-input/driver-reference-input.mapper";
import {DriverBreadcrumbRouting} from "./driver-breadcrumb.routing";
import {LicenceCheckComponent} from "./licence-check/licence-check.component";
import {RoadMarqueComponent} from './roadmarque/roadmarque.component';
import {OnRoadAssessmentComponent} from "./driver-assessments/on-road-assessment/on-road-assessment.component";
import {OnRoadAssessmentInputComponent} from "./driver-assessments/on-road-assessment/on-road-assessment-input.component";
import {OnRoadAssessmentInputMapper} from "./driver-assessments/on-road-assessment/on-road-assessment-input.mapper";
import {DriverNextOfKinInputComponent} from "./driver-next-of-kin/driver-next-of-kin-input.component";
import {NextOfKinInputMapper} from "./driver-next-of-kin/driver-next-of-kin-input.mapper";
import {DriverLicenceCheckComponent} from "./driver-licence-check/driver-licence-check.component";
import {DriverListContainerComponent} from "./driver-list/driver-list-container.component";
import {DriverInsuranceComponent} from "./driver-insurance/driver-insurance-detail/driver-insurance.component";
import {DriverInsuranceInputComponent} from "./driver-insurance/driver-insurance-input/driver-insurance-input.component";
import {NgxPaginationModule} from "ngx-pagination";
import {DriverVehicleSelectComponent} from "./driver-vehicle/driver-vehicle-select.component";
import {VehicleModule} from "../vehicle/vehicle.module";
import {AssessmentSummaryComponent} from "./driver-assessments/assessment-summary/assessment-summary.component";
import {ModuleSummaryComponent} from "./driver-assessments/module-summary/module-summary.component";
import {OnRoadAssessmentSummaryComponent} from "./driver-assessments/on-road-assessment-summary/on-road-assessment-summary.component";
import {TachoCpcComponent} from "./driver-tacho-cpc/driver-tacho-cpc";
import {VehicleDriverComponent} from "./driver-vehicle/vehicle-driver.component";
import {DriverDetailsCardComponent} from "./driver-dashboard/cards/driver-details-card.component";
import {DriverDocumentsCardComponent} from "./driver-dashboard/cards/driver-documents-card.component";
import {DriverLicenceCheckCardComponent} from "./driver-dashboard/cards/driver-licence-check-card.component";
import {DriverDetailsContainerComponent} from "./driver-details/driver-details-container.component";
import {DriverInsuranceListComponent} from "./driver-insurance/driver-insurance-list/driver-insurance-list.component";
import {DriverVehicleListComponent} from "./driver-vehicle/driver-vehicle-list.component";
import {DriverLicenceCardComponent} from "./driver-dashboard/cards/driver-licence-card.component";
import {DriverVehiclesCardComponent} from "./driver-dashboard/cards/driver-vehicles-card.component";
import {DriverVehicleListContainerComponent} from "./driver-vehicle/driver-vehicle-list-container.component";
import {DriverInsuranceCardComponent} from "./driver-dashboard/cards/driver-insurance-card.component";
import {DriverInsuranceListContainerComponent} from "./driver-insurance/driver-insurance-list/driver-insurance-list-container.component";
import {DriverAssessmentsContainerComponent} from "./driver-assessments/driver-assessments-container.component";
import {DriverReferenceListContainerComponent} from "./driver-reference/driver-reference-list/driver-reference-list-container.component";
import {DriverReferencesCardComponent} from "./driver-dashboard/cards/driver-references-card.component";
import {DriverNextOfKinCardComponent} from "./driver-dashboard/cards/driver-next-of-kin-card.component";
import {DriverNextOfKinContainerComponent} from "./driver-next-of-kin/driver-next-of-kin.container.component";
import {DriverNextOfKinComponent} from "./driver-next-of-kin/driver-next-of-kin.component";
import {DriverImportComponent} from "./driver-import/driver-import.component";
import {DriverFawComponent} from './driver-faw/driver-faw-detail/driver-faw.component';
import {DriverFawInputComponent} from './driver-faw/driver-faw-input/driver-faw-input.component';
import {DriverFawListComponent} from './driver-faw/driver-faw-list/driver-faw-list.component';
import {DriverFawListContainerComponent} from './driver-faw/driver-faw-list/driver-faw-list-container.component';
import {DriverFawCardComponent} from './driver-dashboard/cards/driver-faw-card.component';

@NgModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      CommonModule,
      SharedModule,
      VehicleModule,
      DriverRoutingModule,
      NgxPaginationModule
    ],
    declarations: [
      DriverComponent,
      DriverListContainerComponent,
      DriverInputComponent,
      DriverDocumentsComponent,
      DriverDocumentComponent,
      DriverDashboardComponent,

      DriverLicenceCheckCardComponent,
      DriverLicenceCardComponent,
      DriverDetailsCardComponent,
      DriverNextOfKinCardComponent,
      DriverReferencesCardComponent,
      DriverDocumentsCardComponent,
      DriverVehiclesCardComponent,
      DriverInsuranceCardComponent,
      DriverFawCardComponent,

      DriverDetailsComponent,
      DriverNextOfKinComponent,
      DriverNextOfKinContainerComponent,
      DriverLicenceComponent,
      DriverEndorsementsComponent,
      AllowedVehiclesComponent,

      DriverLicenceContainerComponent,
      DriverLicenceCheckComponent,
      LicenceCheckComponent,

      DriverDetailsContainerComponent,
      DriverDetailsTabListComponent,

      DriverAssessmentsComponent,
      DriverAssessmentsContainerComponent,
      DriverReferenceListComponent,
      DriverReferenceListContainerComponent,
      DriverDocumentsContainerComponent,
      DriverReferenceDetailComponent,
      DriverReferenceInputComponent,
      DriverNextOfKinInputComponent,
      DriverImportComponent,

      OnRoadAssessmentComponent,
      OnRoadAssessmentInputComponent,
      RoadMarqueComponent,
      AssessmentSummaryComponent,
      ModuleSummaryComponent,
      OnRoadAssessmentSummaryComponent,

      DriverInsuranceListComponent,
      DriverInsuranceListContainerComponent,
      DriverInsuranceComponent,
      DriverInsuranceInputComponent,

      DriverFawListComponent,
      DriverFawListContainerComponent,
      DriverFawComponent,
      DriverFawInputComponent,

      DriverVehicleListComponent,
      DriverVehicleListContainerComponent,
      DriverVehicleSelectComponent,
      VehicleDriverComponent,
      TachoCpcComponent
    ],
    providers: [
      DocumentService,
      DriverInputMapper,
      NextOfKinInputMapper,
      DriverReferenceInputMapper,
      OnRoadAssessmentInputMapper,
      DriverBreadcrumbRouting
    ],
})
export class DriverModule { }
