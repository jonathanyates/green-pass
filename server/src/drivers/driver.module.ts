import {ContainerModule, interfaces} from "inversify";
import {TYPES} from "../container/container.types";
import {DriverRouter} from "./driver.router";
import {DriverController, IDriverController} from "./driver.controller";
import {DriverService, IDriverService} from "./driver.service";
import {DriverRepository, IDriverRepository} from "./driver.repository";
import {IApiRouter} from "../interfaces/api-router.interface";
import {DriverVehicleMapRepository, IDriverVehicleMapRepository} from "./driver-vehicle-map.repository";
import {DriverReminderScheduler, IDriverReminderScheduler} from "./driver-reminder.scheduler";

export const driverModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.DriverRouter).to(DriverRouter).inSingletonScope();
  bind<IDriverController>(TYPES.controllers.DriverController).to(DriverController).inSingletonScope();
  bind<IDriverService>(TYPES.services.DriverService).to(DriverService).inSingletonScope();
  bind<IDriverRepository>(TYPES.repositories.DriverRepository).to(DriverRepository).inSingletonScope();
  bind<IDriverVehicleMapRepository>(TYPES.repositories.DriverVehicleMapRepository).to(DriverVehicleMapRepository).inSingletonScope();

  bind<IDriverReminderScheduler>(TYPES.services.DriverReminderScheduler).to(DriverReminderScheduler).inSingletonScope();
});