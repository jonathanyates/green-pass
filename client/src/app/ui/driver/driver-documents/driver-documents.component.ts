import {Component, Input, Output, EventEmitter} from '@angular/core';
import {IDocument} from "../../../../../../shared/interfaces/document.interface";

@Component({
  selector: 'gp-driver-documents',
  templateUrl: 'driver-documents.component.html',
  styleUrls: ['driver-documents.component.css']
})
export class DriverDocumentsComponent {

  @Input() documents: IDocument[];
  @Input() lightHeader: boolean = true;
  @Output() select: EventEmitter<IDocument> = new EventEmitter<IDocument>();

  onSelect(document: IDocument) {
    this.select.emit(document);
  }

}
