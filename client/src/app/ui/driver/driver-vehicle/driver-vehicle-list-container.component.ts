import {Component, OnInit, ViewChild} from '@angular/core';
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {SelectDriverUserInfo} from "../../../store/drivers/driver.selectors";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {BaseComponent} from "../../shared/components/base/base.component";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {BusyAction} from "../../../store/progress/progress.actions";
import {SaveDriverSuccessAction} from "../../../store/drivers/driver.actions";
import {DriverService} from "../../../services/driver.service";
import {ModalComponent} from "../../shared/components/modal/model.component";

@Component({
  selector: 'driver-vehicle-list-component',
  template: `
    <gp-card [header]="'Vehicles'">
      <gp-driver-vehicle-list [company]="company" [driver]="driver" [lightHeader]="true"
                              [user]="user" [allowInput]="allowInput" (remove)="removeVehicle($event)"
                              (add)="add()" (select)="select($event)"></gp-driver-vehicle-list>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>

    <gp-model [title]="'Remove Vehicle'" (confirmed)="onRemoveConfirmed($event)">
      <div *ngIf="selectedVehicle; else noVehicle">
        Are you sure you want to remove vehicle '{{selectedVehicle.registrationNumber}}' ?
      </div>
      <ng-template #noVehicle>No Vehicle selected</ng-template>
    </gp-model>    
  `
})

export class DriverVehicleListContainerComponent extends BaseComponent implements OnInit {

  driver: IDriver;
  company: ICompany;
  user : IUser;
  selectedVehicle: IVehicle;
  allowInput: boolean = false;

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private driverService: DriverService) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        this.driver = info.driver;
        this.company = info.company;
        this.user = info.user;
        this.allowInput = info.allowInput;
      }));
  }

  add() {
    return this.router.navigate(['./', Paths.Add], { relativeTo: this.route });
  }

  select(vehicle: IVehicle) {
    return this.router.navigate(['./', vehicle._id], { relativeTo: this.route });
  }

  removeVehicle(vehicle: IVehicle) {
    if (!vehicle) return;
    this.modal.open();
    this.selectedVehicle = vehicle;
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedVehicle) {

      this.store.dispatch(new BusyAction());

      this.driverService.removeVehicle(this.driver, this.selectedVehicle._id)
        .subscribe(driver => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new SaveDriverSuccessAction(driver));
        }, error => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new HandleErrorAction(error));
        })
    }
    this.selectedVehicle = null;
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
