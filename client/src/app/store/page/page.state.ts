import {IPage} from "../../ui/dynamic/page.model";

export interface PageState {
  page: IPage;
}
