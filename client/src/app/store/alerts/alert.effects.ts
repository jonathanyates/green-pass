import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./alert.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {BusyAction} from "../progress/progress.actions";
import {AlertService} from "../../services/alert.service";
import {SelectAlertTypes} from "./alert.selectors";
import {LoadSelectedAlertSummaryAction, SelectAlertTypesSuccessAction} from "./alert.actions";
import {Messages} from "../app.messages";
import {messenger} from "../../services/messenger";
import {Router} from "@angular/router";
import {Paths} from "../../shared/constants";

@Injectable()
export class AlertsEffects {

  constructor(
    private alertService:AlertService,
    private actions$:Actions,
    private store: Store<AppState>,
    private router: Router) {
  }
  // Alert Types
  @Effect() selectAlertTypes$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_ALERT_TYPES)
    .switchMap(action => this.store.let(SelectAlertTypes()).take(1)
      .map(alertTypes => alertTypes
        ? new SelectAlertTypesSuccessAction(alertTypes)
        : new actions.LoadAlertTypesAction())
    );

  @Effect() loadAlertTypes$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_ALERT_TYPES)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap(action => this.alertService.getAlertTypes()
      .map(alertTypes =>  new actions.LoadAlertTypesSuccessAction(alertTypes))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveAlertType$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_ALERT_TYPE)
    .map((action: actions.SaveAlertTypeAction) => action.payload)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap(alertType => this.alertService.saveAlertType(alertType)
      .map(alertType =>  new actions.SaveAlertTypeSuccessAction(alertType))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        messenger.publish(Messages.SaveError, error);
        return Observable.empty();
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({dispatch:false}) saveAlertTypeSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_ALERT_TYPE_SUCCESS)
    .map((action: actions.SaveAlertTypeSuccessAction) => action.payload)
    .do(alertType => this.router.navigate([Paths.Admin, Paths.AlertTypes]));

  @Effect() loadAlertSummary$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_SUMMARY)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.alertService.getAlertSummaries()
      .map(alertSummary =>  new actions.LoadAlertSummarySuccessAction(alertSummary))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );


  @Effect() loadSelectedAlertSummary$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_SELECTED_ALERT_SUMMARY)
    .map((action: LoadSelectedAlertSummaryAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.alertService.getAlertSummary(payload)
      .map(alertSummary =>  new actions.LoadSelectedAlertSummarySuccessAction(alertSummary))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

}
