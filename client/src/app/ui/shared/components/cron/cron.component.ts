import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormInput, FormInputGroup} from "../dynamic-form/dynamic-form.model";
import {CronInputMapper} from "./cron-input-mapper";
import {Cron} from "./cron.model";
import {FormGroup} from "@angular/forms";
import {DynamicFormBuilder} from "../dynamic-form/dynamic-form.builder";
import {Observable} from "rxjs/Observable";

declare let $: any;

@Component({
 selector: 'gp-cron',
 templateUrl: 'cron.component.html'
})

export class CronComponent implements OnInit, AfterViewInit, OnDestroy {

  private _cron: Cron;
  @Input() set cron(cron: Cron) {
    this._cron = cron;
    this.setInputs();
  }
  get cron() { return this._cron; }

  @Output() cronChange = new EventEmitter<Cron>();

  inputGroup: FormInputGroup;
  form: FormGroup;
  subscriptions = [];

 constructor() { }

 ngOnInit() { }

 setInputs() {
  this.inputGroup = CronInputMapper.mapTo(this.cron);
  this.form = DynamicFormBuilder.buildFromGroups([this.inputGroup]);

   this.inputGroup.inputs.forEach((input: FormInput) => {
     this.subscriptions.push(input.control.valueChanges
       .subscribe(changes => {
         let cron = CronInputMapper.mapFrom(this.form);
         if (cron.toString() == this.cron.toString()) return;
         this._cron = cron;
         this.cronChange.emit(cron);
       }))
   });

 }

  ngAfterViewInit(): void {
    // Material Select Initialization
    // $(document).ready(function() {
    //   setTimeout(() => {
    //     $('.mdb-select').material_select();
    //   }, 50);
    // });
  }

  ngOnDestroy(): void {
   this.subscriptions.forEach(x => x.unsubscribe());
  }

}

/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */

/*
Month #####  Day of Month ##### Day of Week ##### (at) Hour #### Minutes #####
      Every               Every             Every           Every        Every
      Jan                  1                 Sun             0            0
      Feb                  2                 Mon             1            1
      Mar                  3                 Tue             2            2
      etc                 etc(to 12)         etc            etc(to 23)   etc(to 59)
*/
