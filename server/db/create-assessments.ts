import container from "../src/container/container.config";
import {TYPES} from "../src/container/container.types";
import {IDriverService} from "../src/drivers/driver.service";
import {IDriverModel} from "../src/drivers/driver.schema";
import {assessments} from "./assessment";

const driverService = container.get<IDriverService>(TYPES.services.DriverService);

driverService.findOne({ licenceNumber: 'YATES607198JP9LK' })
  .then(driver => {
    driver.assessments = assessments;
    return driverService.update(driver);
  })
  .then(driver => {
    console.log('Assessments created')
  })
  .catch(error => {
    console.log(error);
  });