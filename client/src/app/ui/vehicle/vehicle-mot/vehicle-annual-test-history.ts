import { Component, Input } from '@angular/core';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { IAnnualTest } from '../../../../../../shared/interfaces/vehicle.interface';
import { FormatDate } from '../../../../../../shared/utils/helpers';
import { ColumnType } from '../../../../../../shared/models/column-type.model';

const testResultMap = {
  Pass: 'PASSED',
  Fail: 'FAILED',
};

@Component({
  selector: 'gp-vehicle-annual-test-history',
  template: ` <gp-dynamic-list [list]="list" [lightHeader]="true" [allowAdd]="allowAdd"></gp-dynamic-list> `,
})
export class VehicleAnnualTestHistoryComponent {
  list: IDynamicList;

  @Input() allowAdd: boolean = false;

  @Input()
  set annualTestHistory(annualTestHistory: IAnnualTest[]) {
    this.createItems(annualTestHistory);
  }

  createItems(annualTestHistory: IAnnualTest[]) {
    this.list = {
      columns: [
        { header: '', field: 'expander', columnType: ColumnType.Expander },
        { header: 'Test Date', field: 'testDate' },
        { header: 'Expires', field: 'expiryDate' },
        { header: 'Result', field: 'testResult' },
        { header: 'Certificate Number', field: 'testCertificateNumber' },
      ],
    };

    console.log(annualTestHistory);

    if (annualTestHistory) {
      this.list.items = annualTestHistory.map((test) => {
        let hasFailures = Number(test.numberOfDefectsAtTest) > 0;
        let hasNotices = Number(test.numberOfAdvisoryDefectsAtTest) > 0;
        const hasDefects = test.defects && test.defects.length > 0;

        let item: any = Object.assign({}, test, {
          expander: hasFailures || hasNotices,
          testDate: FormatDate(test.testDate),
          expiryDate: FormatDate(test.expiryDate),
          testCertificateNumber: test.testCertificateNumber,
          testResult: testResultMap[test.testResult] || test.testResult.toUpperCase(),
        });

        if (hasDefects) {
          const { defects } = test;
          const failureNotices = defects.filter((defect) => !defect.severityDescription.includes('Advisory'));
          const advisoryNotices = defects.filter((defect) => defect.severityDescription === 'Advisory');
          item.details = [];

          if (failureNotices.length > 0) {
            const failureItems = {
              header: 'Reason(s) for failure',
              detail: failureNotices.map((defect) => defect.failureReason),
              style: 'text-danger',
            };
            item.details.push(failureItems);
          }

          if (advisoryNotices.length > 0) {
            const advisoryItems = {
              header: 'Advisory notice item(s)',
              detail: advisoryNotices.map((defect) => defect.failureReason),
              style: 'text-secondary',
            };
            item.details.push(advisoryItems);
          }
        }

        return item;
      });

      this.list.styles = annualTestHistory.map((mot) => ({
        testResult: this.getResultStyle(mot.testResult),
      }));
    }
  }

  getResultStyle(result: string) {
    if (result === 'Pass') {
      return 'success-color badge';
    }
    if (result === 'Pass after rectification') {
      return 'warning-color badge';
    }
    if (result === 'Fail') {
      return 'danger-color badge';
    }
    return 'success-color badge';
  }
}
