import { AppState } from '../../../store/app.states';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Store } from '@ngrx/store';
import {IAlertType, ICompanyAlertType} from "../../../../../../shared/interfaces/alert.interface";
import {SelectCompany} from "../../../store/company/company.selectors";
import {SelectAlertTypes} from "../../../store/alerts/alert.selectors";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import {SaveCompanyAlertTypesAction} from "../../../store/company/company.actions";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";

@Component({
  template: `
    <gp-dynamic-list [list]="list" (edit)="edit()" (back)="back()" (itemSelected)="select($event)"></gp-dynamic-list>
    <button *ngIf="editing" class="btn btn-primary waves-effect" (click)="cancel()">Cancel</button>
    <button *ngIf="editing" class="btn btn-primary waves-effect" (click)="save()">Save</button>
`
})
export class CompanyAlertTypeListComponent implements OnInit, OnDestroy {

  alertTypes: IAlertType[];
  company: ICompany;
  list: IDynamicList;
  private subscriptions = [];
  editing:boolean;

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .combineLatest(this.store.let(SelectAlertTypes()),
        (company, alertTypes) => ({alertTypes: alertTypes, company: company }))
      .filter(result => result.alertTypes != null && result.company != null)
      .subscribe(result => {
        this.alertTypes = result.alertTypes;
        this.company = result.company;
        this.setList(this.company);
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  setList(company: ICompany) {
    this.list = {
      columns: [
        { header: 'Name', field: 'name' },
        { header: 'Category', field: 'category' },
        { header: 'Priority', field: 'priority'},
        { header: 'Email To', field: 'recipients'}
      ]
    };

    if (company.alertTypes != null) {
      this.list.items = company.alertTypes.map(companyAlertType => {
        return {
          name: companyAlertType.alertType.reportType.name,
          category: companyAlertType.alertType.reportType.category,
          priority: companyAlertType.alertType.priority,
          recipients:
            companyAlertType &&
            companyAlertType.schedule &&
            companyAlertType.schedule.recipients
              ? companyAlertType.schedule.recipients.length
              : 'None'
        }
      });
    }
  }

  edit() {
    if (!this.editing && this.company) {
      this.editing = true;
      this.list = {
        columns: [
          { header: 'Selected', field: 'selected', columnType: ColumnType.CheckBox },
          { header: 'Name', field: 'name' },
          { header: 'Category', field: 'category' },
          { header: 'Priority', field: 'priority'},
          { header: 'Email To', field: 'recipients'}
        ]
      };

      let companyAlertTypes = this.company.alertTypes;

      this.list.items = this.alertTypes.map(alertType => {
        let companyAlertType = companyAlertTypes.find(x => x.alertType._id === alertType._id);

        return {
          selected: companyAlertType != null,
          name: alertType.reportType.name,
          category: alertType.reportType.category,
          priority: alertType.priority,
          recipients:
            companyAlertType &&
            companyAlertType.schedule &&
            companyAlertType.schedule.recipients
              ? companyAlertType.schedule.recipients.length
              : 'None',
          alertType: alertType,
          previewSelected: (item:any):boolean => {
            if (this.company.subscription.resources.alerts === -1) {
              return true;
            }
            let selectedItems = this.list.items.filter(item => item.selected === true).length;
            return selectedItems < this.company.subscription.resources.alerts;
          }
        }
      })
    }
  }

  save() {
    this.editing = false;

    let alertTypes = this.list.items
      .filter(item => item.selected)
      .map(item => {
        let existing = this.company.alertTypes.find(x => x.alertType._id === item._id);

        return existing
          ? existing
          : <ICompanyAlertType>{
            alertType: item.alertType,
            emailRecipients: null,
            schedule: null
          }
      });

    this.store.dispatch(new SaveCompanyAlertTypesAction(alertTypes));
  }

  cancel() {
    this.editing = false;
    this.setList(this.company);
  }

  select(alertType:IAlertType) {
    if (!this.editing) {
      this.router.navigate(['./', alertType._id], { relativeTo: this.route });
    }
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
