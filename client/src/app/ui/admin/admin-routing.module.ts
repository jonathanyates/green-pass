import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from "./admin.component";
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {AdminDashboardComponent} from "./dashboard/admin-dashboard.component";
import {CompanyListComponent} from "../company/company-list/company-list.component";
import {CompanyInputComponent} from "../company/company-input/company-input.component";
import {CanDeactivateGuard} from "../shared/guards/can-deactivate.guard";
import {TemplateListComponent} from "../template/template-list/template-list.component";
import {CompanyListResolve} from "../company/company-list/company-list.resolve";
import {CompanyResolve} from "../company/company.resolve";
import {TemplateListResolve} from "../template/template-list/template-list.resolve";
import {Roles} from "../../../../../shared/constants";
import {TemplateResolve} from "../template/template/template.resolve";
import {TemplateContainerComponent} from "../template/template/template-container.component";
import {TemplateInputContainerComponent} from "../template/template-input/template-input-container.component";

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthorizationGuard],
    data: { roles: [Roles.Admin] },
    children: [
      { path: '', component: AdminDashboardComponent },
      { path: 'companies', component: CompanyListComponent, resolve: { companies: CompanyListResolve } },
      { path: 'companies/new', component: CompanyInputComponent, resolve: { company: CompanyResolve }, canDeactivate: [CanDeactivateGuard] },
      { path: 'templates', component: TemplateListComponent, resolve: { templates: TemplateListResolve } },
      { path: 'templates/add', component: TemplateInputContainerComponent, resolve: { templates: TemplateResolve } },
      { path: 'templates/:templateId', component: TemplateContainerComponent, resolve: { templates: TemplateResolve } },
      { path: 'templates/:templateId/edit', component: TemplateInputContainerComponent, resolve: { templates: TemplateResolve } },    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    CompanyListResolve,
    CompanyResolve,
    TemplateListResolve,
    TemplateResolve
  ]
})
export class AdminRoutingModule { }
