import {Component, Input} from '@angular/core';

@Component({
  selector: 'gp-pdf-preview',
  template: `
    <div *ngIf="url">
      <pdf-viewer [src]="url"
                  [show-all]="true"
                  [original-size]="false"
                  style="display: block;">        
      </pdf-viewer>
    </div>
    <div *ngIf="message">{{message}}</div>
  `
})
export class PdfPreviewComponent {

  url: any;
  message: string;

  @Input() set file(file) {
    this.readFile(file);
  }

  readFile = file => {
    if (file == null) {
      this.url = null;
      return;
    }

    if (file instanceof ArrayBuffer) {
      this.url = file;
      return;
    }

    try {
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      };

      reader.onerror = (err:any) => {
        console.log(`Error occured trying to read file ${file.name}. Error: ${err.message}`);
        this.message = 'Invalid file. Please select a valid file.'
      };

      reader.readAsArrayBuffer(file);
    } catch(err) {
      console.log(`Error occured trying to read file ${file.name}. Error: ${err.message}`);
      this.message = 'Invalid file. Please select a valid file.'
    }
  };
}


