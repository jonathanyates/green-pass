export interface IListGroupItem {
  title?: string,
  value?: any,
  type?: string,
  style?: string
}
