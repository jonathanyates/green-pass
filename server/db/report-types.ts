import {IReportType, ReportCategory} from "../../shared/interfaces/report.interface";
import {AlertPriority, IAlertType} from "../../shared/interfaces/alert.interface";

export const reportInfo: { reportType: IReportType, alertType:IAlertType }[] = [
  {
    reportType: <IReportType>{
      name: 'None Compliant Drivers',
      query: 'getNoneCompliantDrivers',
      category: ReportCategory.Driver,
    },
    alertType: <IAlertType>{
      priority: AlertPriority.High
    }
  },
  {
    reportType: <IReportType>{
    name: 'None Compliant Vehicles',
    query: 'getNoneCompliantVehicles',
    category: ReportCategory.Vehicle
  },
    alertType: <IAlertType>{
      priority: AlertPriority.High
    }
  },
  {
    reportType: <IReportType>{
      name: 'Licence Checks',
      query: 'getLicenceCheckRequired',
      category: ReportCategory.Driver
    },
    alertType: <IAlertType>{
      priority: AlertPriority.High
    }
  },
  {
    reportType: <IReportType>{
      name: 'Outstanding Documents',
      query: 'getUnsignedMandates',
      category: ReportCategory.Driver
    },
    alertType: <IAlertType>{
      priority: AlertPriority.Medium
    }
  },
  {
    reportType: <IReportType>{
      name: 'On Road Assessments',
      query: 'getOnRoadAssessments',
      category: ReportCategory.Driver
    },
    alertType: <IAlertType>{
      priority: AlertPriority.Medium
    }
  },
];