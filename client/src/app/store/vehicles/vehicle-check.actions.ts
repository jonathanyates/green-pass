import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IVehicle, IVehicleCheck} from "../../../../../shared/interfaces/vehicle.interface";

export const ActionTypes = {
  SELECT_VEHICLE_CHECK: type('[Vehicle Check] Select'),
  SELECT_VEHICLE_CHECK_SUCCESS : type('[Vehicle Check] Select Success'),
  SAVE_VEHICLE_CHECK:   type('[Vehicle Check] Save'),
  SAVE_VEHICLE_CHECK_SUCCESS :   type('[Vehicle Check] Save Success'),
  CLEAR_VEHICLE_CHECK:  type('[Vehicle Check] Clear')
};

/**
 * Vehicle Check Actions
 */

export class SelectVehicleCheckAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_CHECK;
  payload: { vehicleId: string, checkId: string };
  constructor(vehicleId: string, checkId: string) {
    this.payload = { vehicleId: vehicleId, checkId: checkId }
  }
}

export class SelectVehicleCheckSuccessAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_CHECK_SUCCESS;
  constructor(public payload: IVehicleCheck) { } // check id
}

export class SaveVehicleCheckAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_CHECK;
  payload: { vehicle: IVehicle, check: IVehicleCheck };
  constructor(vehicle: IVehicle, check: IVehicleCheck) {
    this.payload = { vehicle: vehicle, check:check }
  }
}

export class SaveVehicleCheckSuccessAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_CHECK_SUCCESS;
  payload: { vehicle: IVehicle, check: IVehicleCheck };
  constructor(vehicle: IVehicle, check: IVehicleCheck) {
    this.payload = { vehicle: vehicle, check:check }
  }
}

export class ClearVehicleCheckAction implements Action {
  type = ActionTypes.CLEAR_VEHICLE_CHECK;
  payload = null;
  constructor() { } // check id
}

export type Actions
  = SelectVehicleCheckSuccessAction
  | SaveVehicleCheckSuccessAction
  | ClearVehicleCheckAction;
