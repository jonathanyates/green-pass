import {Component, OnInit, Input} from '@angular/core';
import {FormInputGroup} from './dynamic-form.model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'gp-dynamic-input-group',
  template: `
    <h3 class="card-header primary-color white-text"
        [ngClass]="{
          'primary-color white-text': !isSubGroup,
          'sub-card-header': isSubGroup
        }">{{group.label}}</h3>
    <div class="card-block" [ngClass]="{
        'sub-card': isSubGroup
    }">

      <div class="mt-2" *ngFor="let input of group.inputs">
        <gp-input *ngIf="input.hasOwnProperty('type')" [input]="input" [form]="form"></gp-input>

        <div *ngIf="input.hasOwnProperty('inputs')">
            <gp-dynamic-input-group *ngIf="!input.hidden" [group]="input" [form]="form" [isSubGroup]="true"></gp-dynamic-input-group>
        </div>

      </div>
    </div>
`,
  styles: [
    '.sub-card-header { padding-left: 0; background-color: white; color: #757575; }',
    '.sub-card { padding: 0 }',

  ]
})
export class DynamicInputGroupComponent implements OnInit {

  @Input() group: FormInputGroup;
  @Input() form: FormGroup;
  @Input() isSubGroup = false;

  constructor() {
  }

  ngOnInit() {
  }

}
