import 'reflect-metadata';
import { IVehicleApiService, VehicleService } from '../src/vehicles/vehicle.service';
import { TYPES } from '../src/container/container.types';
import { IVehicle } from '../../shared/interfaces/vehicle.interface';
import container from '../src/container/container.config';
import { is3YearsOld } from '../src/vehicles/vehicle.utils';

jest.setTimeout(100000000);

describe('Vehicle Service', function () {
  describe.skip('is3YearsOld', function () {
    it('vehicle is 3 Years Old', () => {
      const now = new Date(Date.now());
      const dateOfFirstRegistration = new Date(now.getFullYear() - 3, now.getMonth(), now.getDate());
      const result = is3YearsOld(dateOfFirstRegistration);
      expect(result).toBeTruthy();
    });

    it('vehicle is not yet 3 Years Old', () => {
      const now = new Date(Date.now());
      const dateOfFirstRegistration = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
      const result = is3YearsOld(dateOfFirstRegistration);
      expect(result).toBeFalsy();
    });
  });

  describe.skip('getNextServiceDate', () => {
    it('get Next Service Date', () => {
      const vehicle = <IVehicle>{
        serviceHistory: [{ serviceDate: new Date() }],
        annualMileage: 25000,
      };

      const vehicleService = <VehicleService>container.get<IVehicleApiService>(TYPES.services.VehicleService);
      const result: Date = vehicleService.getNextServiceDate(vehicle);

      const expected = new Date().addDays(25 * 7);
      expect(result.getFullYear()).toEqual(expected.getFullYear());
      expect(result.getMonth()).toEqual(expected.getMonth());
      expect(result.getDate()).toEqual(expected.getDate());
    });
  });

  describe.skip('getNextInspectionDate', () => {
    it('get Next Inspection Date', () => {
      const vehicle = <IVehicle>{
        inspectionHistory: [{ inspectionDate: new Date() }],
        annualMileage: 25000,
      };

      const vehicleService = <VehicleService>container.get<IVehicleApiService>(TYPES.services.VehicleService);

      const result = vehicleService.getNextInspectionDate(vehicle);

      const expected = new Date().addDays(9 * 7);
      expect(result.getFullYear()).toEqual(expected.getFullYear());
      expect(result.getMonth()).toEqual(expected.getMonth());
      expect(result.getDate()).toEqual(expected.getDate());
    });
  });

  describe('vehicleChecks', () => {
    it('performs vehicle checks', () => {
      const vehicleService = <IVehicleApiService>container.get<IVehicleApiService>(TYPES.services.VehicleService);

      return vehicleService
        .vehicleChecks()
        .then((vehicles) => {
          expect(vehicles).toBeDefined();
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('performs vehicle check then MOT Check', async () => {
      const vehicleService = <IVehicleApiService>container.get<IVehicleApiService>(TYPES.services.VehicleService);
      const vehicle: IVehicle = await vehicleService.vehicleCheck('012345678901234567891000', 'E18EBY', null, false);

      expect(vehicle).toBeDefined();
      expect(vehicle.dateOfFirstRegistration).toBeDefined();
      expect(vehicle.model).toBeDefined();
      expect(vehicle.taxDueDate).toBeDefined();
      expect(vehicle.motDueDate).toBeDefined();
      expect(vehicle.motHistory).toBeDefined();
    });

    it('performs vehicle check then HGV Annual Test Check', async () => {
      const vehicleService = <IVehicleApiService>container.get<IVehicleApiService>(TYPES.services.VehicleService);
      const vehicle: IVehicle = await vehicleService.vehicleCheck('012345678901234567891000', 'N6EPL', null, false);

      expect(vehicle).toBeDefined();
      expect(vehicle.dateOfFirstRegistration).toBeDefined();
      expect(vehicle.model).toBeDefined();
      expect(vehicle.taxDueDate).toBeDefined();
      expect(vehicle.motDueDate).toBeDefined();
      expect(vehicle.annualTestHistory).toBeDefined();
    });
  });
});
