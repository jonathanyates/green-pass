import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {UnauthorizedComponent} from "./status-errors/unauthorized.component";
import {ErrorComponent} from "./status-errors/error.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {ResetPasswordResolve} from "./change-password/reset-password.resolve";
import {ResetPasswordComponent} from "./change-password/reset-password.component";
import {ChangePasswordContainerComponent} from "./change-password/change-password-container.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'resetPassword/:token', component: ResetPasswordComponent, resolve: { valid: ResetPasswordResolve } },
  { path: 'changePassword', component: ChangePasswordContainerComponent },
  { path: 'unauthorized', component: UnauthorizedComponent, pathMatch: 'full' },
  { path: 'error', component: ErrorComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
  providers: [
    ResetPasswordResolve
  ]
})
export class SecurityRoutingModule { }

