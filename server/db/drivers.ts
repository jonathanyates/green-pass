require('../../shared/extensions/date.extensions');

function pad(number, size) {
  let s = String(number);
  while (s.length < (size || 2)) {
    s = '0' + s;
  }
  return s;
}

// let drivers = [];
//
// for (let i = 1; i <= 100; i++) {
//   let num = pad(i, 3);
//   let driver =  {
//     forename: 'Jonathan' + i,
//     surname: 'Yates' + i,
//     ni: `NR071${num}A`,
//     licenceNo: `YATES607${num}JP9LK`,
//     postcode: 'PR3 1YF',
//     gender: 'Male'
//   };
//
//   drivers.push(driver);
// }
//
// export const driverInfo = drivers;

//   // phone: 07824618247
//   // line1:"Toll Bar",
//   // line2:"Lancaster Road",
//   // locality:"Cabus",
//   // townCity:"Preston",
//   // county:"Lancashire",
//   // country:"United Kingdom",
//   // postcode:"PR3 1JE",

export const driverInfo = [
  // {
  // title: 'Mr',
  // forename: 'Lee',
  // surname: 'Dickinson',
  // ni: 'JE779662C',
  // licenceNo: 'DICKI706226LH9HK',
  // postcode: 'PR3 1JE',
  // gender: 'Male',
  // email: 'lee@greenpasstraining.co.uk',
  // },
  {
    title: 'Mr',
    forename: 'Jonathan',
    surname: 'Yates',
    ni: 'NR071159A',
    licenceNo: 'YATES607198JP9LK',
    postcode: 'PR3 1YF',
    gender: 'Male',
    email: 'jonathan.yates@hotmail.com',
  },
];
