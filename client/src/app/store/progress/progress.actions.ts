import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";

export const ActionTypes = {
  SET_PROGRESS: type('[Progress] Set Progress'),
};

export class BusyAction implements Action {
  type = ActionTypes.SET_PROGRESS;
  constructor(public payload: boolean = true) { }
}

export type Actions
  = BusyAction
  | LogoutAction;
