import {AppState} from "../app.states";
import {TemplatesState} from "./templates.state";
import {Store} from "@ngrx/store";

export const SelectTemplatesState = () => {
  return (store:Store<AppState>) => {
    return store.select<TemplatesState>(state => state.templates);
  }
};

export const SelectTemplates = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectTemplatesState())
      .map(state => state.list);
  }
};

export const SelectTemplate = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectTemplatesState())
      .map(state => state.selected);
  }
};
