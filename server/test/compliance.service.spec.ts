import 'reflect-metadata';
import { TYPES } from '../src/container/container.types';
import { IComplianceService } from '../src/compliance/compliance.service';
import container from '../src/container/container.config';

describe.skip('Compliance Service', function () {
  describe.skip('getCompanyVehicleCompliance', () => {
    it('gets Vehicle Compliance', () => {
      const service = container.get<IComplianceService>(TYPES.services.ComplianceService);

      return service.getCompanyVehicleCompliance('012345678901234567891000').then((result) => {
        expect(result).toBeGreaterThan(-1);
      });
    });
  });

  describe.skip('getCompanyDriverCompliance', () => {
    it('gets Driver Compliance', () => {
      const service = container.get<IComplianceService>(TYPES.services.ComplianceService);

      return service.getCompanyDriverCompliance('5a57d6485c3fbcf00cb2c7e4').then((result) => {
        expect(result).toBeGreaterThan(-1);
      });
    });
  });

  describe.skip('updateCompanyCompliance', () => {
    it('update Company Compliance', () => {
      const service = container.get<IComplianceService>(TYPES.services.ComplianceService);

      return service.updateCompanyCompliance('5a57d6485c3fbcf00cb2c7e4').then((result) => {
        expect(result).not.toBeNull();
      });
    });
  });
});
