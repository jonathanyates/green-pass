import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {ICompany} from "../../../../../../../shared/interfaces/company.interface";
import {SelectCompanyState} from "../../../../store/company/company.selectors";
import {IInsurance} from "../../../../../../../shared/interfaces/common.interface";

@Component({
  selector: 'gp-company-insurance',
  template: `
    <gp-fileRecord [fileRecord]="insurance" 
                   [header]="'Fleet Insurance'"
                   [fileHeading]="'Insurance Certificate'"
                   (edit)="edit($event)" (back)="back()" [allowEdit]="true"></gp-fileRecord>
  `
})
export class CompanyInsuranceComponent implements OnInit, OnDestroy {

  insurance: IInsurance;
  private company: ICompany;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectCompanyState())
      .filter(state => state.selectedInsurance != null)
      .subscribe(state => {
        this.company = state.company;
        this.insurance = state.selectedInsurance;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit(insurance: IInsurance) {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
