import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {Paths} from "../../../shared/constants";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ICompany, IDepot} from "../../../../../../shared/interfaces/company.interface";

@Component({
  selector: 'gp-depot-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class DepotListComponent implements OnInit, OnDestroy {

  depots: IDepot[];
  company: ICompany;
  list: IDynamicList;
  private sub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.sub = this.route.data
      .map((data: { company: ICompany }) => data.company)
      .merge(this.store.let(SelectCompany()))
      .distinctUntilChanged()
      .subscribe(company => {
        this.company = company;
        this.depots = company ? company.depots : null;
        this.list = {
          columns: [
            { header: 'Name', field: 'name' },
            { header: 'Address', field: 'address', style: 'hidden-xs-down' },
            { header: 'Phone', field: 'phone', style: 'hidden-sm-down' },
            { header: 'Mobile', field: 'mobile', style: 'hidden-sm-down' },
            { header: 'Email', field: 'email', style: 'hidden-sm-down' },
          ],
          items: this.depots
        }
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  add() {
    this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  select(depot: IDepot) {
    this.router.navigate(['./', depot._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
