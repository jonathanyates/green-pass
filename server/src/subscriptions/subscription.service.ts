import {IRepository} from "../db/db.repository";
import {TYPES} from "../container/container.types";
import {inject, injectable} from "inversify";
import {ISubscription} from "../../../shared/interfaces/subscription.interface";

export interface ISubscriptionService {

  getAll(): Promise<ISubscription[]>;

  get(id: string): Promise<ISubscription>;

  find(criteria: any): Promise<ISubscription[]>;

  findOne(criteria: any): Promise<ISubscription>;

  add(subscription: ISubscription): Promise<ISubscription>;

  update(subscription: ISubscription): Promise<ISubscription>;
}

@injectable()
export class SubscriptionService implements ISubscriptionService {

  constructor(@inject(TYPES.repositories.SubscriptionRepository) private repository: IRepository<ISubscription>) {
  }

  getAll(): Promise<ISubscription[]> {
    return this.repository.getAll();
  }

  get(id: string): Promise<ISubscription> {
    return this.repository.get(id);
  }

  find(criteria: any): Promise<ISubscription[]> {
    return this.repository.find(criteria);
  }

  findOne(criteria: any): Promise<ISubscription> {
    return this.repository.findOne(criteria);
  }

  add(subscription: ISubscription): Promise<ISubscription> {
    return this.repository.add(subscription);
  }

  update(subscription: ISubscription): Promise<ISubscription> {
    return this.repository.update(subscription);
  }
}