import '../../../shared/extensions/date.extensions';
import {inject, injectable} from "inversify";
import {IDbContext} from "../db/db.context.interface";
import {TYPES} from "../container/container.types";
import {IDriverService} from "../drivers/driver.service";
import {DriverCompliance} from "../../../shared/models/driver-compliance.model";
import {
  ICompanyCompliance,
  ICompanyDriverCompliance, ICompanyVehicleCompliance,
  ICompliance
} from "../../../shared/interfaces/compliance.interface";
import {IVehicleApiService} from "../vehicles/vehicle.service";
import {VehicleCompliance} from "../../../shared/models/vehicle-compliance.model";
import {IComplianceModel} from "./compliance.schema";
import {CompanyCompliance} from "../../../shared/models/company-compliance.model";
import {ICompanyService} from "../companies/company.service.interface";
import {VehicleTypes} from "../../../shared/interfaces/constants";
import {IDriver} from "../../../shared/interfaces/driver.interface";
import {ICompany} from "../../../shared/interfaces/company.interface";

export interface IComplianceService {
  getAll(): Promise<ICompliance[]>;
  get(companyId): Promise<ICompliance>;
  update(): Promise<ICompliance[]>;
  updateCompanyCompliance(companyId: string): Promise<ICompliance>;
  getCompanyVehicleCompliance(companyId: any): Promise<ICompanyVehicleCompliance>;
  getCompanyDriverCompliance(companyId: any): Promise<ICompanyDriverCompliance>;
  addDriverCompliance(driver: IDriver);
}

@injectable()
export class ComplianceService implements IComplianceService {

  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext,
              @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
              @inject(TYPES.services.DriverService) private driverService: IDriverService,
              @inject(TYPES.services.VehicleService) private vehicleService: IVehicleApiService) {
  }

  getAll(): Promise<ICompliance[]> {
    return this.dbContext.compliance.find().exec();
  }

  get(companyId): Promise<ICompliance> {
    return this.dbContext.compliance.findOne({_companyId: companyId}).exec();
  }

  update(): Promise<ICompliance[]> {
    return this.companyService.getAll()
      .then(companies => Promise.all<ICompliance>(
        companies.map(company => this.updateCompanyCompliance(company._id))));
  }

  updateCompanyCompliance(companyId: string): Promise<IComplianceModel> {
    return Promise.all([
      this.getCompanyCompliance(companyId),
      this.getCompanyVehicleCompliance(companyId),
      this.getCompanyDriverCompliance(companyId)
    ])
      .then(result => ({company: result[0], vehicle: result[1], driver: result[2]}))
      .then(compliance => {
        const companyCompliance = new CompanyCompliance(companyId, compliance.company,
          compliance.vehicle, compliance.driver);

        return this.dbContext.compliance.findOneAndUpdate({_companyId: companyId}, companyCompliance,
          { upsert: true, 'new': true }).exec()
      });
  }

  getCompanyCompliance(companyId: any): Promise<ICompanyCompliance> {
    return this.vehicleService.find({ _companyId: companyId, type: VehicleTypes.fleet })
      .then(fleetVehicles => {
        const requiresFleetInsurance = fleetVehicles && fleetVehicles.length > 0;

        if (!requiresFleetInsurance) {
          return null
        }

        return this.companyService.get(companyId)
          .then(company => {
            const fleetInsurance = company.fleetInsuranceHistory != null && company.fleetInsuranceHistory.length > 0
              ? company.fleetInsuranceHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0]
              : null;

            const today = Date.prototype.getToday();
            const insuranceCompliance = fleetInsurance != null && fleetInsurance.validTo > today && fleetInsurance.filename != null;

            return <ICompanyCompliance>{
              insurance: insuranceCompliance
            }
          });
      })
  }

  getCompanyVehicleCompliance(companyId: any): Promise<ICompanyVehicleCompliance> {

    return this.companyService.get(companyId)
      .then(company => {
        return this.vehicleService.getAll(companyId)
          .then(vehicles => {
            if (!vehicles || vehicles.length === 0) {
              return {
                compliant: 0,
                tax: 0,
                mot: 0,
                service: 0,
                inspection: 0,
                checks: 0,
                insurance: 0
              };
            }

            const vehicleCompliance = vehicles.map(vehicle => new VehicleCompliance(vehicle, company.settings));

            return{
              compliant: (vehicleCompliance.filter(x => x.compliant === true).length / vehicles.length) * 100,
              tax: (vehicleCompliance.filter(x => x.tax === true).length / vehicles.length) * 100,
              mot: (vehicleCompliance.filter(x => x.mot === true).length / vehicles.length) * 100,
              service: (vehicleCompliance.filter(x => x.service === true).length / vehicles.length) * 100,
              inspection: (vehicleCompliance.filter(x => x.inspection === true).length / vehicles.length) * 100,
              checks: (vehicleCompliance.filter(x => x.vehicleCheck === true).length / vehicles.length) * 100
            };
          });
      })
  }

  getCompanyDriverCompliance(companyId: any): Promise<ICompanyDriverCompliance> {
    return Promise.all([
      this.driverService.getAll(companyId),
      this.companyService.get(companyId),
      ])
      .then(result => {
        const drivers: IDriver[] = result[0];
        const company: ICompany = result[1];

        if (!drivers || drivers.length === 0) {
          return <ICompanyDriverCompliance>{
            compliant: 0,
            details: 0,
            nextOfKin: 0,
            references: 0,
            documents: 0,
            licence: 0,
            assessments: 0,
            insurance: 0,
            faw: 0,
            dbs: 0
          };
        }

        const driverCompliance = drivers.map(driver => new DriverCompliance(driver, company));

        const driversRequiringVehicles = driverCompliance.filter(compliance => compliance.vehicles != null);
        const driversWithVehicles = driverCompliance.filter(x => x.vehicles === true).length;
        const vehicles = driversRequiringVehicles.length > 0
          ? (driversWithVehicles / driversRequiringVehicles.length) * 100
          : null;

        const driversRequiringInsurance = driverCompliance.filter(compliance => compliance.insurance != null);
        const driversWithInsurance = driverCompliance.filter(x => x.insurance === true).length;
        const insurance = driversRequiringInsurance.length > 0
          ? (driversWithInsurance / driversRequiringInsurance.length) * 100
          : null;

        const compliance = <ICompanyDriverCompliance>{
          compliant: (driverCompliance.filter(x => x.compliant === true).length / drivers.length) * 100,
          details: (driverCompliance.filter(x => x.details === true).length / drivers.length) * 100,
          nextOfKin: (driverCompliance.filter(x => x.nextOfKin === true).length / drivers.length) * 100,
          references: (driverCompliance.filter(x => x.references === true).length / drivers.length) * 100,
          documents: (driverCompliance.filter(x => x.documents.compliant === true).length / drivers.length) * 100,
          licence: (driverCompliance.filter(x => x.drivingLicence.compliant === true).length / drivers.length) * 100,
          assessments: (driverCompliance.filter(x => x.assessments.compliant === true).length / drivers.length) * 100,
          vehicles: vehicles,
          insurance: insurance,
        };
        
        if (company.settings.drivers.includeFaw) {
          compliance.faw = (driverCompliance.filter(x => x.faw === true).length / drivers.length) * 100;
        }

        if (company.settings.drivers.includeDbs) {
          compliance.dbs = (driverCompliance.filter(x => x.dbs === true).length / drivers.length) * 100;
        }
        
        return compliance;
      });
  }

  addDriverCompliance(driver: IDriver) {
    if (driver) {
      return this.companyService.get(driver._companyId)
        .then(company => {
          driver.compliance = new DriverCompliance(driver, company);
          return driver;
        })
    }
    return Promise.resolve(null);
  }

}

