import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IDriver, IDriverInsurance} from "../../../../../shared/interfaces/driver.interface";

export const ActionTypes = {
  SELECT_DRIVER_INSURANCE: type('[Driver Insurance] Select'),
  SELECT_DRIVER_INSURANCE_SUCCESS : type('[Driver Insurance] Select Success'),
  SAVE_DRIVER_INSURANCE:   type('[Driver Insurance] Save'),
  SAVE_DRIVER_INSURANCE_SUCCESS :   type('[Driver Insurance] Save Success'),
  CLEAR_DRIVER_INSURANCE:  type('[Driver Insurance] Clear')
};

/**
 * Driver Insurance Actions
 */

export class SelectDriverInsuranceAction implements Action {
  type = ActionTypes.SELECT_DRIVER_INSURANCE;
  payload: { driverId: string, insuranceId: string };
  constructor(driverId: string, insuranceId: string) {
    this.payload = { driverId: driverId, insuranceId: insuranceId }
  }
}

export class SelectDriverInsuranceSuccessAction implements Action {
  type = ActionTypes.SELECT_DRIVER_INSURANCE_SUCCESS;
  constructor(public payload: IDriverInsurance) { } // insurance id
}

export class SaveDriverInsuranceAction implements Action {
  type = ActionTypes.SAVE_DRIVER_INSURANCE;
  payload: { driver: IDriver, insurance: IDriverInsurance };
  constructor(driver: IDriver, insurance: IDriverInsurance) {
    this.payload = { driver: driver, insurance:insurance }
  }
}

export class SaveDriverInsuranceSuccessAction implements Action {
  type = ActionTypes.SAVE_DRIVER_INSURANCE_SUCCESS;
  payload: { driver: IDriver, insurance: IDriverInsurance };
  constructor(driver: IDriver, insurance: IDriverInsurance) {
    this.payload = { driver: driver, insurance:insurance }
  }
}

export class ClearDriverInsuranceAction implements Action {
  type = ActionTypes.CLEAR_DRIVER_INSURANCE;
  payload = null;
  constructor() { } // insurance id
}

export type Actions
  = SelectDriverInsuranceSuccessAction
  | SaveDriverInsuranceSuccessAction
  | ClearDriverInsuranceAction;
