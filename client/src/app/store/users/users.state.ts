import {IUser} from "../../../../../shared/interfaces/user.interface";

export interface UsersState {
  list: IUser[],
  selected?: IUser
}
