import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {IUser} from "../../../../../shared/interfaces/user.interface";

export const ActionTypes = {
  SELECT_LIST:          type('[Users] Select'),
  SELECT_LIST_SUCCESS:  type('[Users] Select Success'),
  LOAD:                 type('[Users] Load'),
  LOAD_SUCCESS:         type('[Users] Load Success'),
  LOAD_FAIL:            type('[Users] Load Fail'),
  SELECT_USER:          type('[User] Select'),
  SELECT_USER_SUCCESS:  type('[User] Select Success'),
  LOAD_USER:            type('[User] Load'),
  LOAD_USER_SUCCESS:    type('[User] Load Success'),
  SAVE:                 type('[User] Save'),
  SAVE_SUCCESS:         type('[User] Save Success'),
  SAVE_FAIL:            type('[User] Save Fail'),
};

/**
 * Select Users Actions
 */
export class SelectUsersAction implements Action {
  type = ActionTypes.SELECT_LIST;
  constructor(public payload: string) { }
}

export class SelectUsersSuccessAction implements Action {
  type = ActionTypes.SELECT_LIST_SUCCESS;
  constructor(public payload: IUser[]) { }
}

/**
 * Load Users Actions
 */
export class LoadUsersAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload: string) { }
}

export class LoadUsersSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: IUser[]) { }
}

/**
 * Select User Actions
 */
export class SelectUserAction implements Action {
  type = ActionTypes.SELECT_USER;
  payload: { companyId:string, userId:string };

  constructor(companyId:string, userId:string) {
    this.payload = { companyId:companyId, userId:userId }
  }
}

export class SelectUserSuccessAction implements Action {
  type = ActionTypes.SELECT_USER_SUCCESS;
  constructor(public payload: IUser) { }
}

/**
 * Load User Actions
 */
export class LoadUserAction implements Action {
  type = ActionTypes.LOAD_USER;
  payload: { companyId:string, userId:string };

  constructor(companyId:string, userId:string) {
    this.payload = { companyId:companyId, userId:userId }
  }
}

export class LoadUserSuccessAction implements Action {
  type = ActionTypes.LOAD_USER_SUCCESS;
  constructor(public payload: IUser) { }
}

/**
 * Save User Actions
 */
export class SaveUserAction implements Action {
  type = ActionTypes.SAVE;
  constructor(public payload: IUser) { }
}

export class SaveUserSuccessAction implements Action {
  type = ActionTypes.SAVE_SUCCESS;
  constructor(public payload: IUser) { }
}

export class SaveUserFailAction implements Action {
  type = ActionTypes.SAVE_FAIL;
  constructor(public payload: any) {
  }
}

export type Actions
  = LoadUsersAction
  | LoadUsersSuccessAction
  | SelectUsersSuccessAction
  | LoadUserSuccessAction
  | SelectUserSuccessAction
  | SaveUserAction
  | SaveUserSuccessAction
  | SaveUserFailAction
  | LogoutAction;
