import mongoose from 'mongoose';
import { IAssessment } from '../../../shared/interfaces/assessment.interface';

export const OnLineAssessmentSchema = new mongoose.Schema({
  tests: [
    {
      testCode: { type: String, required: false },
      description: { type: String, required: false },
      score: { type: Number, required: false },
      riskLevel: { type: String, required: false },
      startDate: { type: Date, required: false },
      endDate: { type: Date, required: false },
      complete: { type: Boolean, required: true },
    },
  ],
  inviteDate: { type: Date, required: false },
  startDate: { type: Date, required: false },
  overallRiskLevel: { type: String, required: false },
});

export const TrainingModuleSchema = new mongoose.Schema({
  createdDate: { type: Date, required: false },
  trainingProvider: { type: String, required: false },
  trainingRecommendationType: { type: String, required: false },
  subTrainingType: { type: String, required: false },
  arrangedDate: { type: Date, required: false },
  arrangedBy: { type: String, required: false },
  contactDate: { type: Date, required: false },
  startDate: { type: Date, required: false },
  completedDate: { type: Date, required: false },
  details: { type: String, required: false },
  reasonDetails: { type: String, required: false },
  satisfactory: { type: String, required: false },
  testScore: { type: Number, required: false },
  riskLevel: { type: String, required: false },
});

export const OnRoadAssessmentSchema = new mongoose.Schema({
  inviteDate: { type: Date, required: true },
  assessmentDate: { type: Date, required: false },
  assessedBy: { type: String, required: false },
  result: { type: String, required: false },
  complete: { type: Boolean, required: false },
  filename: { type: String, required: false },
  fileType: { type: String, required: false },
});

export const AssessmentSchema = new mongoose.Schema({
  refId: { type: String, required: false },
  onLine: { type: OnLineAssessmentSchema, required: false },
  trainingModules: { type: [TrainingModuleSchema], required: false },
  onRoad: { type: OnRoadAssessmentSchema, required: false },
  completedDate: { type: Date, required: false },
  complete: { type: Boolean, required: false },
  lastLoginDate: { type: Date, required: false },
  lastUpdatedDate: { type: Date, required: false },
});

export interface IAssessmentModel extends mongoose.Document, IAssessment {
  _id: any;
}
