import {IAssessment} from "../interfaces/assessment.interface";

export class AssessmentSummary {

  expander: boolean;
  type: string;
  inviteDate: Date;
  startDate: Date;
  completedDate: Date;
  totalAssessments: number;
  completedAssessments: number;
  percentageComplete: number;
  completed: string;
  complete: boolean;
  result: string;
  details: any;
  action: any;

  constructor(assessment: IAssessment) {
    this.initialise(assessment);
  }

  initialise(assessment: IAssessment) {

    if (!assessment || !assessment.onLine) {
      return;
    }

    this.expander = true;
    this.type = 'On Line Assessment';

    this.inviteDate = assessment.onLine.inviteDate;

    if (assessment.onLine.tests && assessment.onLine.tests.length > 0) {
      this.totalAssessments =  assessment.onLine.tests.length;

      this.completedAssessments = assessment.onLine.tests
        .filter(test => test.endDate != null).length;

      if (assessment.onLine.startDate) {
        this.startDate = assessment.onLine.startDate;
      } else {
        this.startDate = assessment.onLine.tests
          .map(result => result.startDate)
          .filter(date => date != null)
          .sort((a, b) => a.getTime() - b.getTime())[0];
      }

      if (this.completedAssessments > 0) {
        this.completedDate = assessment.onLine.tests
          .map(result => result.endDate)
          .filter(date => date != null)
          .sort((a, b) => b.getTime() - a.getTime())[0];
      }
    }

    if (this.totalAssessments > 0) {
      this.percentageComplete = (this.completedAssessments / this.totalAssessments) * 100;
    }

    this.completed = this.startDate
      ? `${this.completedAssessments} of ${this.totalAssessments} Assessments`
      :  'Not started';

    this.complete = this.completedAssessments === this.totalAssessments;
    this.result = assessment.onLine.overallRiskLevel;
  }
}

