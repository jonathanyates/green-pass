import '../extensions/date.extensions';
import {
  IDriverAssessmentCompliance, IDriverDocumentCompliance, IDriverCompliance, IDriverLicenceCompliance
} from "../interfaces/compliance.interface";
import {IDriver} from "../interfaces/driver.interface";
import {IAssessment} from "../interfaces/assessment.interface";
import {AssessmentSummary} from "./assessment-summary.model";
import {ModuleSummary} from "./module-summary.model";
import {IVehicle} from "../interfaces/vehicle.interface";
import {VehicleTypes} from "../interfaces/constants";
import {ISubscription} from "../interfaces/subscription.interface";
import {ICompany} from "../interfaces/company.interface";
import {ICompanySettings} from "../interfaces/company-settings.interface";

export const RiskLevel = {
  Unknown: 'Unknown',
  Low: 'Low',
  Average: 'Average',
  AboveAverage: 'Above Average',
  High: 'High'
};

function isNotNullOrEmpty(value: string) {
  if (value) {
    return value.length > 0
  }
  return false;
}

export class DriverCompliance implements IDriverCompliance {
  compliant: boolean = false;
  details: boolean = false;
  nextOfKin: boolean = false;
  references: boolean = false;
  documents: IDriverDocumentCompliance;
  drivingLicence: IDriverLicenceCompliance;
  assessments: IDriverAssessmentCompliance;
  vehicles?: boolean;
  insurance?: boolean;
  faw?: boolean;
  dbs?: boolean;

  constructor(driver: IDriver, company: ICompany) {
    this.details = DriverCompliance.getDriverDetailsCompliance(driver);
    this.nextOfKin = DriverCompliance.getDriverNextOfKinCompliance(driver);
    this.references = DriverCompliance.getReferenceCompliance(driver, company.settings);
    this.documents = DriverCompliance.getDriverDocumentCompliance(driver);
    this.drivingLicence = DriverCompliance.getDriverLicenceCompliance(driver);
    this.assessments = DriverCompliance.getDriverAssessmentCompliance(driver, company.subscription);
    this.insurance = DriverCompliance.getDriverInsuranceCompliance(driver);
    this.vehicles = DriverCompliance.getVehicleCompliance(driver);

    this.faw = DriverCompliance.getFawCompliance(driver, company.settings);
    this.dbs = DriverCompliance.getDbsCompliance(driver, company.settings);

    this.compliant =
      this.details &&
      this.nextOfKin &&
      this.references &&
      this.documents.compliant &&
      this.drivingLicence.compliant &&
      this.assessments.compliant &&
      (this.vehicles == null || this.vehicles == true) &&
      (this.insurance == null || this.insurance == true) &&
      (this.faw == null || this.faw == true) &&
      (this.dbs == null || this.dbs == true);
  }

  static getDriverDetailsCompliance(driver: IDriver): boolean {
    return driver != null &&
      isNotNullOrEmpty(driver.licenceNumber) &&
      isNotNullOrEmpty(driver.niNumber) &&
      driver._user != null &&
      isNotNullOrEmpty(driver._user.title) &&
      isNotNullOrEmpty(driver._user.forename) &&
      isNotNullOrEmpty(driver._user.surname) &&
      DriverCompliance.getDriverAddressCompliance(driver) &&
      driver.dateOfBirth != null &&
      isNotNullOrEmpty(driver.gender);
  }

  static getDriverAddressCompliance(driver: IDriver): boolean {
    return driver.address != null &&
      isNotNullOrEmpty(driver.address.postcode) &&
      isNotNullOrEmpty(driver.address.line1) &&
      isNotNullOrEmpty(driver.address.townCity) &&
      isNotNullOrEmpty(driver.address.county) &&
      isNotNullOrEmpty(driver.address.country);
  }

  static getDriverNextOfKinCompliance(driver: IDriver): boolean {
    return driver != null && driver.nextOfKin != null &&
      isNotNullOrEmpty(driver.nextOfKin.forename) &&
      isNotNullOrEmpty(driver.nextOfKin.surname) &&
      isNotNullOrEmpty(driver.nextOfKin.phone) &&
      driver.nextOfKin.address != null &&
      isNotNullOrEmpty(driver.nextOfKin.address.postcode) &&
      isNotNullOrEmpty(driver.nextOfKin.address.line1) &&
      isNotNullOrEmpty(driver.nextOfKin.address.townCity) &&
      isNotNullOrEmpty(driver.nextOfKin.address.county) &&
      isNotNullOrEmpty(driver.nextOfKin.address.country);
  }

  static getReferenceCompliance(driver: IDriver, settings: ICompanySettings): boolean {
    return settings.drivers.references === false
      || (driver.references && driver.references.length > 0);
  }

  static getFawCompliance(driver: IDriver, settings: ICompanySettings): boolean {
    if (!settings.drivers.includeFaw || driver.fawRequired !== true) {
      return null;
    }

    let faw = driver.fawHistory != null && driver.fawHistory.length > 0
      ? driver.fawHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0]
      : null;

    let today = Date.prototype.getToday();
    return faw != null && faw.validTo > today && faw.filename != null;
  }

  static getDbsCompliance(driver: IDriver, settings: ICompanySettings): boolean {
    if (!settings.drivers.includeDbs) {
      return null;
    }

    let today = Date.prototype.getToday();
    return driver.dbsExpiry != null && driver.dbsExpiry > today;
  }

  // driver insurance compliance only applies if driver
  // requires business insurance for own vehicle
  static getDriverInsuranceCompliance(driver: IDriver): boolean {
    if (driver.vehicles && driver.vehicles.length > 0) {
      let greyVehicles = driver.vehicles
        .filter((vehicle: any) => vehicle._id != null)
        .filter((vehicle: IVehicle) => vehicle.type === VehicleTypes.grey);

      if (greyVehicles && greyVehicles.length > 0) {
        let insurance = driver.insuranceHistory != null && driver.insuranceHistory.length > 0
          ? driver.insuranceHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0]
          : null;

        let today = Date.prototype.getToday();
        return insurance != null && insurance.validTo > today && insurance.filename != null;
      }
    }

    if (driver.ownedVehicle) {
      let insurance = driver.insuranceHistory != null && driver.insuranceHistory.length > 0
        ? driver.insuranceHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0]
        : null;

      let today = Date.prototype.getToday();
      return insurance != null && insurance.validTo > today && insurance.filename != null;
    }

    return null;
  }

  static getDriverDocumentCompliance(driver: IDriver): IDriverDocumentCompliance {
    if (!driver.documents || driver.documents.length === 0) {
      return {
        compliant: true,
        completed: 0,
        total: 0,
        percentage: 100
      };
    }

    let total = driver.documents.length;
    let signed = driver.documents.filter(document => document.signed).length;
    let percentage = (signed / total) * 100;
    return {
      compliant: percentage === 100,
      completed: signed,
      total: total,
      percentage: percentage
    }
  }

  static getDriverLicenceCompliance(driver: IDriver): IDriverLicenceCompliance {
    if (!driver.licenceNumber || !driver.licence || !driver.licence.checks ||
      driver.licence.checks.length === 0) {
      return {
        compliant: false,
        checkRequired: true
      }
    }

    let check = driver.licence.checks.slice(-1)[0];
    let checkRequired = new Date().getDayDiff(driver.licence.nextCheckDate) <= 0;
    let valid = new Date().getDayDiff(check.validTo) > 0;

    return {
      compliant: !checkRequired && valid && check.points < 12,
      checkRequired: checkRequired,
      valid: valid,
      points: check.points
    }
  }

  static getDriverAssessmentCompliance(driver: IDriver, subscription: ISubscription): IDriverAssessmentCompliance {

    // if there are no assessment subscriptions then just return compliant as true
    if (!subscription.resources.onLineAssessments && !subscription.resources.onRoadTraining) {
      return {
        compliant: true
      };
    }

    let hasAssessments = driver.assessments && driver.assessments.length > 0;

    // if on-line assessments are required and there aren't any then return not compliant
    if (subscription.resources.onLineAssessments && !hasAssessments) {
      return {
        compliant: false,
        onLine: RiskLevel.Unknown
      };
    }

    // if there is only on-road training subscription (no on-line) then just do compliance for on-road only
    if (subscription.resources.onRoadTraining && !subscription.resources.onLineAssessments) {

      // if there are no on-road assessments assigned then driver is compliant
      if (!hasAssessments) {
        return {
          compliant: true
        };
      }

      // get last assessment
      let assessment = driver.assessments.slice(-1)[0];

      // if the last assessment is complete or there is no on-road training required then the driver is compliant
      if (assessment.complete) {
        return {
          compliant: true
        };
      }

      // if on-road training is required then check on-road compliance
      if (assessment.onRoad) {
        return DriverCompliance.onRoadCompliance(assessment);
      }

      // otherwise driver assessments are compliant
      return {
        compliant: true
      };

    }

    let assessment = driver.assessments.slice(-1)[0];
    let assessmentSummary = new AssessmentSummary(assessment);

    if (!assessmentSummary.complete) {
      return {
        compliant: false,
        onLine: RiskLevel.Unknown
      }
    }

    switch (assessmentSummary.result) {
      case RiskLevel.Low:
        return {
          compliant: true,
          onLine: RiskLevel.Low
        };
      // case RiskLevel.High:
      //   return DriverCompliance.onRoadCompliance(assessment, assessmentSummary);
      default:
        return DriverCompliance.moduleCompliance(assessment, assessmentSummary);
    }
  }

  static moduleCompliance(assessment: IAssessment, assessmentSummary: AssessmentSummary): IDriverAssessmentCompliance {
    let moduleSummary = new ModuleSummary(assessment);
    if (!moduleSummary.complete) {
      return {
        compliant: false,
        onLine: assessmentSummary.result,
        modules: RiskLevel.Unknown
      }
    }

    switch (moduleSummary.result) {
      case RiskLevel.Low:
      case RiskLevel.Average:
        return {
          compliant: true,
          onLine: assessmentSummary.result,
          modules: moduleSummary.result
        };
      default:
        return DriverCompliance.onRoadCompliance(assessment, assessmentSummary, moduleSummary);
    }
  }

  static onRoadCompliance(assessment: IAssessment, assessmentSummary?: AssessmentSummary,
                          moduleSummary?: ModuleSummary): IDriverAssessmentCompliance {
    let onRoadCompleted = assessment.onRoad && assessment.onRoad.complete === true;
    let result: IDriverAssessmentCompliance = {
      compliant: onRoadCompleted,
      onLine: assessmentSummary && assessmentSummary.result,
      modules: moduleSummary && moduleSummary.result,
      onRoad: onRoadCompleted
    };

    return result;
  }

  static getVehicleCompliance(driver) {
    if (driver.ownedVehicle === true) {
      return driver.vehicles && driver.vehicles.length > 0;
    }
    return null;
  }
}