﻿import '../shared/rxjs-operators';

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";

import {RouterModule} from "@angular/router";
import {SharedModule} from "./shared/shared.module";
import {AppComponent} from './app/app.component';
import {AppRoutingModule} from "./app-routing.module";
import {StoreModule} from "@ngrx/store";
import {LayoutModule} from "./layout/layout.module";
import {AdminModule} from "./admin/admin.module";
import {SecurityModule} from "./security/security.module";
import {CompanyModule} from "./company/company.module";
import {DriverModule} from "./driver/driver.module";
import {EffectsModule} from "@ngrx/effects";
import {ErrorStatusRoutingModule} from "./error-status-routing.module";

import {AppReducers} from "../store/app.reducers";
import {AuthenticationEffects} from "../store/authentication/authentication.effects";
import {CompaniesEffects} from "../store/companies/companies.effects";
import {ErrorEffects} from "../store/error/error.effects";
import {CompanyEffects} from "../store/company/company.effects";
import {UsersEffects} from "../store/users/user.effects";
import {VehicleEffects} from "../store/vehicles/vehicle.effects";
import {TemplateEffects} from "../store/templates/template.effects";
import {DocumentEffects} from "../store/documents/document.effects";

import {PageService} from "../services/page.service";
import {CompaniesService} from "../services/companies.service";
import {VehicleService} from "../services/vehicle.service";
import {DynamicModule} from "./dynamic/dynamic.module";
import {DynamicPageComponent} from "./dynamic/dynamic-page/dynamic-page.component";
import {DriverEffects} from "../store/drivers/driver.effects";
import {DriverService} from "../services/driver.service";
import {TemplateService} from "../services/template.service";
import {PageEffects} from "../store/page/page.effects";
import {DepotDetailsComponent} from './depot/depot-details/depot-details.component';
import {SupplierDetailsComponent} from './supplier/supplier-details/supplier-details.component';
import {AddressService} from "../services/address.service";
import {AddressEffects} from "../store/address/address.effects";
import {BreadcrumbService} from "../services/breadcrumb.service";
import {FileService} from "../services/file.service";
import {ComplianceEffects} from "../store/compliance/compliance.effects";
import {ComplianceService} from "../services/compliance.service";
import {Messenger} from "../services/messenger";
import {AssessmentService} from "../services/assessment.service";
import {AlertService} from "../services/alert.service";
import {AlertsEffects} from "../store/alerts/alert.effects";
import {ReportService} from "../services/report.service";
import {ReportsEffects} from "../store/reports/report.effects";
import {AlertModule} from "./alert/alert.module";
import {ReportModule} from "./report/report.module";
import {VehicleModule} from "./vehicle/vehicle.module";
import {UserService} from "../services/user.service";
import {ToastModule} from "ng2-toastr";
import {Ng2ImgMaxModule} from "ng2-img-max";

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    FormsModule,
    HttpClientModule,
    RouterModule,
    Ng2ImgMaxModule,

    SharedModule,
    DynamicModule,
    LayoutModule,
    SecurityModule,
    AdminModule,
    CompanyModule,
    DriverModule,
    VehicleModule,
    AlertModule,
    ReportModule,
    ErrorStatusRoutingModule,
    AppRoutingModule,

    StoreModule.forRoot(AppReducers),

    EffectsModule.forRoot([
      AuthenticationEffects,
      CompaniesEffects,
      CompanyEffects,
      UsersEffects,
      VehicleEffects,
      DriverEffects,
      TemplateEffects,
      DocumentEffects,
      PageEffects,
      ErrorEffects,
      AddressEffects,
      ComplianceEffects,
      AlertsEffects,
      ReportsEffects
    ])
  ],
  declarations: [
    AppComponent,
    DynamicPageComponent,
    DepotDetailsComponent,
    SupplierDetailsComponent,
  ],
  exports: [],
  providers: [
    Messenger,
    CompaniesService,
    VehicleService,
    DriverService,
    TemplateService,
    AddressService,
    PageService,
    FileService,
    ComplianceService,
    AssessmentService,
    AlertService,
    ReportService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.listenForNavigation();
  }
}
