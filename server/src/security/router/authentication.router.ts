import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../../container/container.types';
import { IApiRouter } from '../../interfaces/api-router.interface';
import { IAuthenticationController } from '../controller/authentication.controller.interface';
import { IAuthenticationMiddleware } from '../authentication/authentication.middleware.interface';
import { IAuthorizationMiddleware } from '../authorization/authorization.middleware.interface';

@injectable()
export class AuthenticationRouter implements IApiRouter {
  routes = express.Router();

  constructor(
    @inject(TYPES.controllers.AuthenticationController) private authenticationController: IAuthenticationController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware,
    @inject(TYPES.security.AuthenticationMiddleware) private authenticationMiddleware: IAuthenticationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.post('/login', this.authenticationController.login);
    this.routes.post('/authorize', this.authenticationMiddleware.requireAuth);
    this.routes.post('/forgot', this.authenticationController.forgot);
    this.routes.get('/verify-reset/:token', this.authenticationController.verifyResetToken);
    this.routes.post('/reset/:token', this.authenticationController.reset);

    const changePasswordRoutes = express.Router();
    changePasswordRoutes.use(this.authorizationMiddleware.user.middleware());

    changePasswordRoutes.post(
      '/',
      this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
      this.authenticationController.change
    );

    this.routes.use('/change', this.authenticationMiddleware.requireAuth, changePasswordRoutes);
  }
}
