import { Component, Input } from '@angular/core';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { IVehicleMot, RfrType } from '../../../../../../shared/interfaces/vehicle.interface';
import { FormatDate } from '../../../../../../shared/utils/helpers';
import { ColumnType } from '../../../../../../shared/models/column-type.model';

@Component({
  selector: 'gp-vehicle-mot-history',
  template: ` <gp-dynamic-list [list]="list" [lightHeader]="true" [allowAdd]="allowAdd"></gp-dynamic-list> `,
})
export class VehicleMotHistoryComponent {
  list: IDynamicList;

  @Input() allowAdd: boolean = false;

  @Input()
  set motHistory(motHistory: IVehicleMot[]) {
    this.createItems(motHistory);
  }

  createItems(motHistory: IVehicleMot[]) {
    this.list = {
      columns: [
        { header: '', field: 'expander', columnType: ColumnType.Expander },
        { header: 'Test Date', field: 'testDate' },
        { header: 'Expires', field: 'expiryDate' },
        { header: 'Result', field: 'testResult' },
        { header: 'Odometer Reading', field: 'odometerReading' },
        { header: 'Test Number', field: 'motTestNumber' },
      ],
    };

    console.log(motHistory);

    if (motHistory) {
      this.list.items = motHistory.map((mot) => {
        let hasFailures = mot.reasonsForFailure && mot.reasonsForFailure.length > 0;
        let hasNotices = mot.advisoryNotices && mot.advisoryNotices.length > 0;
        const hasReasonsForRejection = mot.reasonsForRejection && mot.reasonsForRejection.length > 0;

        let item: any = Object.assign({}, mot, {
          expander: hasFailures || hasNotices,
          testDate: FormatDate(mot.testDate),
          expiryDate: FormatDate(mot.expiryDate),
          motTestNumber: mot.motTestNumber,
          odometerReading: Number(mot.odometerReading).toLocaleString() + ' miles',
          testResultStyle: this.getResultStyle(mot.testResult),
        });

        const rfrTypes = [
          RfrType.DANGEROUS,
          RfrType.MAJOR,
          RfrType.MINOR,
          RfrType.PASS,
          RfrType.FAIL,
          RfrType.USER_ENTERED,
          RfrType.ADVISORY,
          RfrType.PRS,
        ];

        const rfrTypesToHeaderMap = {
          [RfrType.DANGEROUS]: '(Dangerous Defects)',
          [RfrType.MAJOR]: '(Major Defects)',
          [RfrType.MINOR]: '(Minor Defects)',
          [RfrType.PASS]: '',
          [RfrType.FAIL]: '',
          [RfrType.USER_ENTERED]: '',
          [RfrType.ADVISORY]: '(Advisories)',
          [RfrType.PRS]: '(Pass After Rectification)',
        };

        const rfrTypesToColorMap = {
          [RfrType.DANGEROUS]: 'text-danger',
          [RfrType.MAJOR]: 'text-danger',
          [RfrType.MINOR]: 'text-info',
          [RfrType.PASS]: 'text-success',
          [RfrType.FAIL]: 'text-danger',
          [RfrType.USER_ENTERED]: 'text-secondary',
          [RfrType.ADVISORY]: 'text-secondary',
          [RfrType.PRS]: 'text-danger',
        };

        if (hasReasonsForRejection) {
          const actualRfrTypes = mot.reasonsForRejection.map((rfr) => rfr.type);
          const orderedRfrTypes = rfrTypes.filter((rfrType) => actualRfrTypes.includes(rfrType));
          item.details = orderedRfrTypes.map((rfrType) => {
            const rfrTypes = mot.reasonsForRejection.filter((rfr) => rfr.type === rfrType);
            const adviceHeader = rfrTypes.length > 0 ? rfrTypes[0].advice : '';
            return {
              header: `${adviceHeader} ${rfrTypesToHeaderMap[rfrType]}`,
              detail: rfrTypes.map((rfr) => rfr.text),
              style: rfrTypesToColorMap[rfrType],
            };
          });
        }

        return item;
      });

      this.list.styles = motHistory.map((mot) => ({
        testResult: this.getResultStyle(mot.testResult),
      }));
    }
  }

  getResultStyle(result: string) {
    if (result === 'PASSED') return 'success-color badge';
    if (result === 'FAILED') return 'danger-color badge';
    return 'warning-color badge';
  }
}
