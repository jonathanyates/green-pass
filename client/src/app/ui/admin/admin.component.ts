import { Component, OnInit } from '@angular/core';

@Component({
  template: `
    <gp-breadcrumb></gp-breadcrumb>
    <router-outlet></router-outlet>
`
})
export class AdminComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}
