import {Company} from "../../ui/company/company.model";

export interface CompaniesState {
  list: Company[];
}
