import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IVehicle, IVehicleService} from "../../../../../shared/interfaces/vehicle.interface";

export const ActionTypes = {
  SELECT_VEHICLE_SERVICE: type('[Vehicle Service] Select'),
  SELECT_VEHICLE_SERVICE_SUCCESS : type('[Vehicle Service] Select Success'),
  SAVE_VEHICLE_SERVICE:   type('[Vehicle Service] Save'),
  SAVE_VEHICLE_SERVICE_SUCCESS :   type('[Vehicle Service] Save Success'),
  CLEAR_VEHICLE_SERVICE:  type('[Vehicle Service] Clear')
};

/**
 * Vehicle Service Actions
 */

export class SelectVehicleServiceAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_SERVICE;
  payload: { vehicleId: string, serviceId: string };
  constructor(vehicleId: string, serviceId: string) {
    this.payload = { vehicleId: vehicleId, serviceId: serviceId }
  }
}

export class SelectVehicleServiceSuccessAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_SERVICE_SUCCESS;
  constructor(public payload: IVehicleService) { } // service id
}

export class SaveVehicleServiceAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_SERVICE;
  payload: { vehicle: IVehicle, service: IVehicleService };
  constructor(vehicle: IVehicle, service: IVehicleService) {
    this.payload = { vehicle: vehicle, service:service }
  }
}

export class SaveVehicleServiceSuccessAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_SERVICE_SUCCESS;
  payload: { vehicle: IVehicle, service: IVehicleService };
  constructor(vehicle: IVehicle, service: IVehicleService) {
    this.payload = { vehicle: vehicle, service:service }
  }
}

export class ClearVehicleServiceAction implements Action {
  type = ActionTypes.CLEAR_VEHICLE_SERVICE;
  payload = null;
  constructor() { } // service id
}

export type Actions
  = SelectVehicleServiceSuccessAction
  | SaveVehicleServiceSuccessAction
  | ClearVehicleServiceAction;
