import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {IPage} from "../../ui/dynamic/page.model";

export const ActionTypes = {
  LOAD: type('[Page] Load Page'),
  LOAD_SUCCESS: type('[Page] Load Success Page'),
  CLEAR: type('[Page] Clear Page')
};

export class LoadPageAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload: string) { } // pageId
}

export class LoadPageActionSuccess implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: IPage) { }
}

export class ClearPageAction implements Action {
  type = ActionTypes.CLEAR;
  payload = null;
  constructor() { }
}

export type Actions
  = LoadPageActionSuccess
  | ClearPageAction
  | LogoutAction;
