import { ICompany } from '../../../shared/interfaces/company.interface';
import { IUser } from '../../../shared/interfaces/user.interface';
import serverConfig from '../server/server.config';
import { Roles } from '../../../shared/constants';

export const getUserLoginEmail = (user: IUser, password: string, company: ICompany): string => {
  let text = `
    <p>Hi ${user.forename},</p>
    <p>You are receiving this email from Green Pass Compliance on behalf of ${
      company.name
    } as confirmation that a user account has been set up for you.</p>

    <p>Here are your account details:</p>
    <ul>
        <li>User Name: ${user.username}</li>
        <li>Password: ${password}</li>
    </ul>
    <p>Login to your account by clicking the following link.</p>
    <p><a href="${serverConfig.client.baseUrl + '/login'}">${serverConfig.client.baseUrl + '/login'}</a></p>
    <p>You will be asked to change this password the first time you login.</p>
  `;

  if (user.role === Roles.Driver) {
    text += getDriverEmailText(company);
  }

  text += `<p>Regards,<br>${company.controllingMind.name},<br>${company.name}</p>`;

  return text;
};

function getDriverEmailText(company: ICompany): string {
  let driverText = `
    <p>Please login and complete the following sections.</p>
    <ul>
      <li>Driving Licence Check</li>`;

  if (company?.settings?.drivers?.allowInput) {
    driverText += `
      <li>Your Personal Details</li>
      <li>Next Of Kin Details</li>
    `;

    if (company?.settings?.drivers?.references) {
      driverText += `<li>References</li>`;
    }

    driverText += `<li>Add any owned vehicles used for business purposes</li>`;
  }

  driverText += `<li>Read and sign (E-Signature) all required Mandates</li>`;

  if (company?.subscription?.resources?.onLineAssessments) {
    driverText += `<li>On-Line Assessment Training</li>`;
  }

  driverText += `
    </ul>
    <p>It is of paramount importance that each user completes their electronic driver file by completing the sections above as this allows the organization to develop a safe system of work and facilitates their duty of care under the current legal framework set out by the Health & Safety Executive.</p>
  `;

  return driverText;
}
