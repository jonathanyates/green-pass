import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { NextFunction, Request, Response } from 'express-serve-static-core';
import codes from '../shared/error.codes';
import { IFileService } from './file.service';
import fs from 'fs';
import serverConfig from '../server/server.config';

export interface IFileController {
  get(req: Request, res: Response, next: NextFunction);
  getPdf(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class FileController implements IFileController {
  constructor(@inject(TYPES.services.FileService) private fileService: IFileService) {}

  get = (req: Request, res: Response, next: NextFunction) => {
    const filename = req.params.filename;

    if (!filename) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No filename specified.' });
    }

    this.fileService
      .getFile(filename)
      .then((fileStream) => {
        fileStream.pipe(res);
      })
      .catch((err) => {
        next(err);
      });
  };

  getPdf = (req: Request, res: Response, next: NextFunction) => {
    const filename = req.params.filename;
    const directory = req.params.directory;

    if (!filename) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No pdf filename specified.' });
    }

    if (!directory) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No pdf directory specified.' });
    }

    const pwd = process.env.PWD;

    const filePath = `${pwd}/${serverConfig.server.path}/assets/${directory}/${filename}`;

    console.log(`Getting PDF file ${filePath}...`);

    console.log('pwd: ' + pwd);
    console.log('server.path: ' + serverConfig.server.path);

    fs.readFile(filePath, (err, data) => {
      if (err) {
        console.log(`Failed to get PDF file ${filePath}. Error: ${err}`);
        next(err);
      }

      console.log(`Successfully retrieved PDF file ${filePath}.`);

      res.contentType('application/pdf');
      res.send(data);
    });
  };
}
