import {AppStates, AppState} from "../store/app.states";
import {AuthenticationState} from "../store/authentication/authentication.reducer";
import {Store} from "@ngrx/store";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ServerError} from "../shared/errors/server.error";
import {jsonDeserialiser} from "../../../../shared/utils/json.deserialiser";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
import HTTP_STATUS_CODES from "../shared/errors/status-codes.enum";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {SelectAuthenticationState} from "../store/authentication/authentication.selectors";
import {HttpEvent} from "@angular/common/http/src/response";

export class HttpClientOptions {
  body?: any;
  headers?: HttpHeaders;
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: any = 'json';
  withCredentials?: boolean;
}

@Injectable()
export class  HttpBaseService {

  protected state: AuthenticationState;

  constructor(protected httpClient: HttpClient, protected store: Store<AppState>) {
    this.store.let(SelectAuthenticationState())
      .subscribe(state => this.state = state);
  }

  protected authorize() {
    if (this.state.token == null) {
      throw new Error('Unauthorized');
    }
  }

  protected getOptions(params?: HttpParams, responseType?: string): HttpClientOptions {

    let options: HttpClientOptions = new HttpClientOptions();
    options.headers = this.getHeaders();

    if (params) {
      options.params = params;
    }

    if (responseType) {
      options.responseType = responseType;
    }

    return options;
  }

  protected getHeaders() {
    return new HttpHeaders().set('Authorization', this.state.token);
  }

  protected handleResult<T>(result: T): T {
    if (!result) {
      return null;
    }
    let obj = jsonDeserialiser.parseDates(result);
    return <T>(obj || {});
  }

  protected handleError(err: HttpErrorResponse) : ErrorObservable {

    if (err.error instanceof Error) {
      return Observable.throw(new ServerError(err.message, HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR));
    }

    let message = err.error && err.error.message
      ? err.error.message
      : err.message;

    console.log('An error occured calling api service.');
    console.log('Error message: ' + err.message);
    if (err.error) {
      console.log(err.error);
    }

    if (err.status === 0) {
      return Observable.throw(new ServerError('Service is not responding.', HTTP_STATUS_CODES.SERVICE_UNAVAILABLE));
    }

    return Observable.throw(new ServerError(message, err.status));
  }

}



