import logger = require('winston');
import schedule = require('node-schedule');
import { TYPES } from '../container/container.types';
import { inject, injectable } from 'inversify';
import { ICompanyService } from '../companies/company.service.interface';
import { Job } from 'node-schedule';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IDriverReminderSetting } from '../../../shared/interfaces/company-settings.interface';
import { IDriverService } from './driver.service';
import { getDriverReminderEmail } from './driver-reminder.emails';
import { IEmail } from '../email/email.service';
import { log } from 'util';

export interface IDriverReminderSchedule {
  companyId: string;
  reminders: IDriverReminderSetting;
}

export interface IDriverReminderScheduler {
  scheduleAll(): Promise<boolean[][]>;
  scheduleForCompany(companyId: string): Promise<boolean[]>;
  schedule(schedule: IDriverReminderSchedule): Promise<boolean[]>;
  listen();
}

@injectable()
export class DriverReminderScheduler implements IDriverReminderScheduler {
  private jobs: Map<string, Job> = new Map<string, Job>();

  constructor(
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
    @inject(TYPES.services.DriverService) private driverService: IDriverService,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  listen = () => {
    // delay to give rabbit chance to start
    setTimeout(() => {
      this.messageService
        .receive(queueNames.driverReminderSchedules)
        .subscribe(this.handleUpdate, (error) => logger.error(`Error receiving driver reminder update:\n ${error}`));
    }, 5000);
  };

  private handleUpdate = (reminderSchedule: IDriverReminderSchedule) => {
    logger.info('Handling driver reminder schedule update.');
    logger.info('reminder schedule: ');
    logger.info(JSON.stringify(reminderSchedule));

    const job = this.jobs.get(reminderSchedule.companyId);

    if (job) {
      logger.info('Cancelled old driver reminder schedule.');
      job.cancel();
    }

    this.schedule(reminderSchedule);
  };

  scheduleAll = (): Promise<boolean[][]> => {
    return this.companyService.getAll().then((companies) => {
      return Promise.all(companies.map((company) => this.scheduleForCompany(company._id)));
    });
  };

  scheduleForCompany = (companyId: string): Promise<boolean[]> => {
    return this.companyService.get(companyId).then((company) => {
      return this.schedule({
        companyId: company._id,
        reminders: company.settings.drivers.reminders,
      });
    });
  };

  schedule = (reminderSchedule: IDriverReminderSchedule): Promise<boolean[]> => {
    // don't schedule if the isn't one or sendEmails is false
    if (!reminderSchedule || !reminderSchedule.reminders || !reminderSchedule.reminders.sendEmails) {
      return Promise.resolve([true]);
    }

    logger.info('Scheduling new driver reminder schedule for company ' + reminderSchedule.companyId);

    return new Promise((resolve, reject) => {
      const job = schedule.scheduleJob(reminderSchedule.reminders.cron, () => {
        this.sendReminders(reminderSchedule)
          .then((sent) => {
            return resolve(sent);
          })
          .catch((err) => {
            return reject(err);
          });
      });

      this.jobs.set(reminderSchedule.companyId, job);
    });
  };

  private sendReminders(reminderSchedule: IDriverReminderSchedule): Promise<boolean[]> {
    // don't schedule if the isn't one or sendEmails is false
    if (!reminderSchedule || !reminderSchedule.reminders || !reminderSchedule.reminders.sendEmails) {
      return Promise.resolve([true]);
    }

    logger.info('Sending driver reminders for company ' + reminderSchedule.companyId);

    return this.companyService.get(reminderSchedule.companyId).then((company) => {
      return this.driverService.find({ _companyId: reminderSchedule.companyId }).then((drivers) => {
        const nonCompliantDrivers = drivers.filter((driver) => !driver.compliance.compliant);
        if (nonCompliantDrivers && nonCompliantDrivers.length > 0) {
          logger.info('Drivers to be send reminders: ' + nonCompliantDrivers.length);

          return Promise.all(
            nonCompliantDrivers.map((driver) => {
              const emailText = getDriverReminderEmail(company, driver);
              return this.messageService.send(queueNames.sendEmail, <IEmail>{
                recipients: [driver._user.email],
                subject: 'Driver Compliance Reminder',
                content: emailText,
              });
            })
          );
        }

        logger.info('No drivers require a reminder sending.');
        return Promise.resolve([]);
      });
    });
  }
}
