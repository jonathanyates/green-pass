import container from "../../src/container/container.config";
import {IAssessmentService} from "../../src/assessments/assessment.service";
import TYPES from "../../src/container/container.types";
import {IDriverService} from "../../src/drivers/driver.service";
import {SequencePromise} from "../../../shared/extensions/promise.extensions";

const assessmentService = container.get<IAssessmentService>(TYPES.services.AssessmentService);
const driverService = container.get<IDriverService>(TYPES.services.DriverService);

driverService.getAll('5a11eb5618665e000fb0ac9e')
  .then(drivers => Promise.all([
      SequencePromise.sequence(drivers, driver => {
        return assessmentService.updateAssessments(driver)
      })
    ])
  )
  .then(drivers => {
    console.log('Updated Driver Assessments');
    process.exit(0);
  })
  .catch(error => {
    console.log(error);
    process.exit(1);
  });
