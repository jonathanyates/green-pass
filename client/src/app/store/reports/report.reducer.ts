import * as actions from "./report.actions";
import {ActionTypes} from "../authentication/authentication.actions";
import {ReportState} from "./report.state";

export const initialState: ReportState = {
  reportTypes: null,
};

export function ReportsReducer(state: ReportState = initialState, action: actions.Actions): ReportState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_REPORT_TYPES_SUCCESS:
        return Object.assign({}, state, {
          reportTypes: action.payload
        });

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }

};
