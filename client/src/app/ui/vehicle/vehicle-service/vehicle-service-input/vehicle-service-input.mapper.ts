import {FormInput, FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {FormatDate, FormatDateForInput, GetDate} from "../../../../../../../shared/utils/helpers";
import {IVehicleService} from "../../../../../../../shared/interfaces/vehicle.interface";
import {ISupplier} from "../../../../../../../shared/interfaces/company.interface";
import {Observable} from "rxjs";

@Injectable()
export class VehicleServiceInputMapper {

  mapFrom(form:FormGroup, inputGroups: FormInputGroup[], vehicleService: IVehicleService): IVehicleService {

    let input = <FormInput>inputGroups[0].inputs.find((input:FormInput) => input.id === 'file');
    let fileChanged = input.file && input.file !== vehicleService.file;

    return {
      serviceDate: GetDate(form.controls['serviceDate'].value),
      supplierId: form.controls['supplier'].value,
      file: fileChanged ? input.file : null,
      fileType: input.fileType
    };
  }

  mapTo(service: IVehicleService, suppliers: ISupplier[]): FormInputGroup[] {

    let supplierList = suppliers.map(supplier => ({ key: supplier._id, value: supplier.name , selected: false }));

    if (service.supplierId) {
      let supplier = supplierList.find(item => item.key === service.supplierId);
      if (supplier) {
        supplier.selected = true;
      }
    }

    let reportFileInput = <FormInput>{ id: 'file', label: 'Service report', type:'file', required: false, value: '' };

    if (service.file) {
      reportFileInput.file = service.file;
      reportFileInput.fileType = service.fileType;
    }

    reportFileInput.action = file => {
      reportFileInput.file = file;
      reportFileInput.fileType = file.fileType;
      return Observable.empty();
    };

    return [
      {
        label: 'Service details',
        inputs: [
          { id: 'serviceDate', label: 'Service Date', type:'date', required: true, autoFocus: true,
            value: FormatDateForInput(service.serviceDate) },
          { id: 'supplier', label: 'Serviced By', type:'dropdown', required: true, value: service.supplierId,
            gridClass: 'col-md-6', options: supplierList },
          reportFileInput
        ]
      }
    ];
  }

}
