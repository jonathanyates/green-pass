import container from '../src/container/container.config';
import { TYPES } from '../src/container/container.types';
import { IDocumentSignatureService } from '../src/documents/interfaces/signature.service.interface';

const legalesignService = container.get<IDocumentSignatureService>(TYPES.services.DocumentSignatureService);

describe.skip('Legalesign Service', () => {
  describe.skip('createDocument', () => {
    it('create Document', () => {
      const template: any = {
        templateId: '1',
        name: 'Policy 1',
        folderName: 'Folder 1',
        html: `<h1>Policy 1</h1><div>The is Policy 1</div>`,
      };

      return legalesignService
        .createDocument(
          template,
          {
            firstName: 'Jonathan',
            lastName: 'Yates',
            email: 'jonathan.yates@hotmail.com',
          },
          'Green Pass Compliance Ltd'
        )
        .then((document) => {
          return expect(document).toBeDefined();
        })
        .catch((error) => {
          console.log(error);
        });
    });
  });
});
