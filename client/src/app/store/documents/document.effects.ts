import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./document.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {SelectDriver} from "../drivers/driver.selectors";

@Injectable()
export class DocumentEffects {

  constructor(
    private actions$:Actions,
    private store: Store<AppState>) { }

  @Effect() selectDocument$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_DOCUMENT)
    .map((action: actions.SelectDocumentAction) => action.payload)
    .switchMap(documentId => this.store.let(SelectDriver())
      .filter(driver => driver !== null)
      .take(1)
      .map(driver => driver.documents.find(document => document.documentId === documentId))
      .map(document =>  new actions.SelectDocumentSuccessAction(document))
      .catch(error => Observable.of(new HandleErrorAction(error)))
    );
}
