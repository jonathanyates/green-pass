import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'gp-items-summary',
  template: `<h4 class="text-faded">{{getItemsCountText()}} {{itemText + getItemTextSuffix()}}</h4>`
})
export class ItemsSummaryComponent implements OnInit {

  @Input() items;
  @Input() itemText: string;

  constructor() { }

  ngOnInit() { }

  getItemsCountText(): string {

    if (this.items && Array.isArray(this.items)) {
      return this.items && this.items.length > 0
        ? this.items.length.toString()
        : 'No';
    }

    return this.items
      ? this.items.toString()
      : 'No';
  }

  getItemTextSuffix(): string {

    if (this.items && Array.isArray(this.items)) {
      return !this.items || this.items.length !== 1 ? 's' : '';
    }

    return !this.items || this.items !== 1 ? 's' : '';
  }

}
