import {FormInput, FormInputGroup} from "./dynamic-form.model";
import {FormControl, Validators, FormGroup, ValidatorFn, AbstractControl} from "@angular/forms";
import {Observable} from "rxjs";
import {FileValidator} from '../../directives/file-value-accessor.directive';

export class DynamicFormBuilder {

  static build(inputs: FormInput[]) {
    let controls: any = {};
    inputs.forEach(input => DynamicFormBuilder.addControl(input, controls));
    return new FormGroup(controls);
  }

  static buildFromGroups(groups: FormInputGroup[]) {
    let controls: any = {};

    groups.forEach(inputGroup => DynamicFormBuilder.addControls(inputGroup, controls));

    return new FormGroup(controls);
  }

  static buildFromGroup(group: FormInputGroup) {
    let controls: any = {};
    this.addControls(group, controls);
    return new FormGroup(controls);
  }

  static addControls(inputGroup: FormInputGroup, controls: { [key: string]: AbstractControl; }) {
    inputGroup.inputs.forEach(item => DynamicFormBuilder.addControl(item, controls));
  };

  static addControl(item: FormInput|FormInputGroup, controls: { [key: string]: AbstractControl; }) {

      if (item.hasOwnProperty('inputs')) {
        let group = <FormInputGroup>item;
        this.addControls(group, controls);
      }

      let input = <FormInput>item;

      let validators = [];
      if (input.required === true) {
        validators.push(Validators.required);
      }
      if (input.regex) {
        validators.push(Validators.pattern(input.regex))
      }

      // Fix for file inputs not working on safari. Known Issue
      // https://github.com/angular/angular/issues/25680
      if (input.type === 'file') {
        validators.push(FileValidator.validate)
      }

      let control:any = new FormControl(input.value || '', validators);

      control.input = input;
      controls[input.id] = control;
      input.control = control;
  };

}
