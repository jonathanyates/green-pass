import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {AddressRouter} from "./address.router";
import {AddressController, IAddressController} from "./address.controller";
import {AddressService, IAddressService} from "./address.service";
import {AddressRepository, IAddressRepository} from "./address.repository";

export const addressModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.AddressRouter).to(AddressRouter).inSingletonScope();
  bind<IAddressController>(TYPES.controllers.AddressController).to(AddressController).inSingletonScope();
  bind<IAddressService>(TYPES.services.AddressService).to(AddressService).inSingletonScope();
  bind<IAddressRepository>(TYPES.repositories.AddressRepository).to(AddressRepository).inSingletonScope();
});