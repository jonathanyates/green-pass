import {Component, Input, AfterViewInit} from '@angular/core';
import {Form, FormGroup} from '@angular/forms';
import {FormInput} from '../dynamic-form/dynamic-form.model';

declare let $: any;

@Component({
  selector: 'gp-input',
  templateUrl: 'dynamic-input.component.html',
  styleUrls: ['dynamic-input.component.css']
})
export class InputComponent implements AfterViewInit  {

  @Input() form: FormGroup;
  @Input() input: FormInput;
  errors = '';

  ngAfterViewInit(): void {

    const control = $('#' + this.input.id);

    if (control && this.input.type !== 'checkbox') {
      control.change(function () {
        const value = control.val();
        this.input.control.setValue(value);
      }.bind(this));
    }

    control.blur(() => {
      this.validate();
    });

    this.input.control.valueChanges
      .subscribe(value => this.validate());
  }

  validate() {
    const control = this.input.control;

    if (this.input.uppercase && control.value && control.value !== control.value.toUpperCase()) {
      control.setValue(control.value.toUpperCase());
    }

    if (control && control.dirty && !control.valid && control.touched) {
      this.errors = '';
      for (const key in control.errors) {
        if (key === 'pattern') {
          this.errors = 'Invalid';
        }
      }
    } else {
      this.errors = '';
    }
  }

  onChange(input: FormInput, value: any) {
    if (input.action) {
      input.action(value);
    }
  }

}
