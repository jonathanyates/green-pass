import {Component, Input, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {ICompanySettings} from "../../../../../../shared/interfaces/company-settings.interface";
import * as cronstrue from "cronstrue";

@Component({
  selector: 'gp-company-settings',
  template: `<gp-dynamic-list-group [items]="items" (edit)="edit()"></gp-dynamic-list-group>`
})
export class CompanySettingsComponent implements OnInit {

  items: IListGroupItem[];
  private _settings: ICompanySettings;

  @Input() set settings(settings: ICompanySettings) {
    this._settings = settings;
    if (settings) {
      let sendDriverReminders = settings.drivers.reminders && settings.drivers.reminders.sendEmails;
      this.items = [
        {
          title: "Exclude Vehicle Services",
          value: settings.vehicles.grey.exclusions.service ? 'Yes' : 'No'
        },
        {
          title: "Exclude Vehicle Inspections",
          value: settings.vehicles.grey.exclusions.inspection ? 'Yes' : 'No'
        },
        {
          title: "Exclude Vehicle Checks",
          value: settings.vehicles.grey.exclusions.checks ? 'Yes' : 'No'
        },
        {
          title: "Allow Driver Input",
          value: settings.drivers.allowInput ? 'Yes' : 'No'
        },
        {
          title: "Driver References",
          value: settings.drivers.references ? 'Yes' : 'No'
        },
        {
          title: "First Aid at Work",
          value: settings.drivers.includeFaw ? 'Yes' : 'No'
        },
        {
          title: "DBS Certificates",
          value: settings.drivers.includeDbs ? 'Yes' : 'No'
        },
        {
          title: 'Include PNumber',
          value: settings.drivers.includePNumber ? 'Yes' : 'No'
        },
        {
          title: "Send Non-Compliance Reminders",
          value: sendDriverReminders ? 'Yes' : 'No'
        }
      ];

      if (settings.drivers.licenceChecks && settings.drivers.licenceChecks.checkPeriods) {
        this.items.push(...settings.drivers.licenceChecks.checkPeriods.map(period => {
          return {
            title: `${period.noOfPoints} Penalty Points Licence Check`,
            value: `${period.months} months`
          }
        }));
      }

      if (sendDriverReminders) {
        this.items.push({
          title: "Reminder Schedule",
          value: cronstrue.toString(settings.drivers.reminders.cron)
        })
      }
    }
  }
  get settings() {
    return this._settings;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
  }

  edit() {
    return this.router.navigate(['../', Paths.Settings, Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
