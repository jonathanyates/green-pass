import { Container, interfaces } from 'inversify';
import inversify = require('inversify');
import ServiceIdentifier = interfaces.ServiceIdentifier;

export class ContainerExt extends Container {
  // combined decorate and bind
  // e.g.
  // inversify.decorate(inversify.inject(TYPES.CompanyService), CompanyController);
  // container.bind<ICompanyController>(TYPES.CompanyController).to(CompanyController).inSingletonScope();
  decorateAndBind = <TFrom, TTo extends object>(
    dependencyIdentifier: ServiceIdentifier<TTo>,
    serviceIdentifier: ServiceIdentifier<TFrom>,
    toType: { new (...args: any[]): any }
  ) => {
    inversify.decorate(inversify.inject(dependencyIdentifier), toType, 0);
    this.bind<TFrom>(serviceIdentifier).to(toType).inSingletonScope();
  };

  // Injecting dependencies into a function
  // https://github.com/inversify/InversifyJS/blob/master/wiki/recipes.md
  bindDependencies = (func, dependencies) => {
    const injections = dependencies.map((dependency) => {
      return this.get(dependency);
    });
    return func.bind(func, ...injections);
  };
}
