import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {ICompany} from "../../../../../shared/interfaces/company.interface";
import {IFleetInsurance} from "../../../../../shared/interfaces/common.interface";

export const ActionTypes = {
  SELECT_COMPANY_INSURANCE: type('[Company Insurance] Select'),
  SELECT_COMPANY_INSURANCE_SUCCESS : type('[Company Insurance] Select Success'),
  SAVE_COMPANY_INSURANCE:   type('[Company Insurance] Save'),
  SAVE_COMPANY_INSURANCE_SUCCESS :   type('[Company Insurance] Save Success'),
  CLEAR_COMPANY_INSURANCE:  type('[Company Insurance] Clear')
};

/**
 * Company Insurance Actions
 */

export class SelectCompanyInsuranceAction implements Action {
  type = ActionTypes.SELECT_COMPANY_INSURANCE;
  payload: { companyId: string, insuranceId: string };
  constructor(companyId: string, insuranceId: string) {
    this.payload = { companyId: companyId, insuranceId: insuranceId }
  }
}

export class SelectCompanyInsuranceSuccessAction implements Action {
  type = ActionTypes.SELECT_COMPANY_INSURANCE_SUCCESS;
  constructor(public payload: IFleetInsurance) { } // insurance id
}

export class SaveCompanyInsuranceAction implements Action {
  type = ActionTypes.SAVE_COMPANY_INSURANCE;
  payload: { company: ICompany, insurance: IFleetInsurance };
  constructor(company: ICompany, insurance: IFleetInsurance) {
    this.payload = { company: company, insurance:insurance }
  }
}

export class SaveCompanyInsuranceSuccessAction implements Action {
  type = ActionTypes.SAVE_COMPANY_INSURANCE_SUCCESS;
  payload: { company: ICompany, insurance: IFleetInsurance };
  constructor(company: ICompany, insurance: IFleetInsurance) {
    this.payload = { company: company, insurance:insurance }
  }
}

export class ClearCompanyInsuranceAction implements Action {
  type = ActionTypes.CLEAR_COMPANY_INSURANCE;
  payload = null;
  constructor() { } // insurance id
}

export type Actions
  = SelectCompanyInsuranceSuccessAction
  | SaveCompanyInsuranceSuccessAction
  | ClearCompanyInsuranceAction;
