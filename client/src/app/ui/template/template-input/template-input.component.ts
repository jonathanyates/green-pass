import {ChangeDetectorRef, Component, EventEmitter, Input, NgZone, Output} from '@angular/core';
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";

@Component({
  selector: 'gp-template-input',
  template: `
    <gp-card [header]="header">
      <div class="md-form">
        <input type="text" id="nameInput" class="form-control" (change)="onNameChanged($event)"
               name="name" [(ngModel)]="template.name" #name="ngModel">
        <label for="nameInput" class="form-control-label" [gp-reset-input]="true">Name</label>
        <gp-error-alert *ngIf="nameExists" [error]="'This template name already exists. Please choose a unique template name.'"></gp-error-alert>
      </div>
      
      <div class="md-form">
        <input type="text" id="folderInput" list="combo-options" class="form-control"
               name="folderName" [(ngModel)]="template.folderName" #folderName="ngModel">
        <label for="folderInput" class="form-control-label" [gp-reset-input]="true">Folder</label>
        <datalist id="combo-options">
          <option *ngFor="let folder of folders" [value]="folder">{{folder}}</option>
        </datalist>
      </div>
      
      <gp-editor [content]="template.html" [elementId]="'templateEditor'"
                 (onEditorContentChange)="onContentChanged($event)"></gp-editor>
    </gp-card>

    <button class="btn btn-primary waves-effect" (click)="onBack()">
      <i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Back
    </button>
    <button class="btn btn-primary waves-effect" (click)="onSave()"
            [disabled]="!template.name || !template.folderName || ! template.html || nameExists">
      <i class="fa fa-save mr-2" aria-hidden="true"></i> Save
    </button>
  `
})

export class TemplateInputComponent {

  header: string;
  nameExists: boolean;
  valid: boolean;

  private _template: ITemplate;
  @Input() set template(template: ITemplate) {
    if (template) {
      this._template = template;
      this.header = template._id ? 'Edit Template' : 'Add Template';
    }
  }
  get template(): ITemplate {
    return this._template;
  }

  @Input() folders: string[];
  @Input() nameExistsFunc: (name:string) => boolean;
  @Output() save: EventEmitter<ITemplate> = new EventEmitter<ITemplate>();
  @Output() back: EventEmitter<any> = new EventEmitter<any>();

  constructor(private ref: ChangeDetectorRef) {
  }

  onContentChanged(content: string) {
    this.template.html = content;
    this.ref.detectChanges();
  }

  onSave() {
    this.save.emit(this.template);
  }

  onBack() {
    this.back.emit();
  }

  onNameChanged(event) {
    let nameExists = this.checkNameExists(this.template.name);
    this.nameExists = nameExists;
  }

  checkNameExists(name: string): boolean {
    if (!this.nameExistsFunc) {
      return false;
    }

    return this.nameExistsFunc(name);
  }

}
