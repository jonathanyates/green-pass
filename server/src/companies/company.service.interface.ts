import {ICompany} from "../../../shared/interfaces/company.interface";
import {IFleetInsurance} from "../../../shared/interfaces/common.interface";

export interface ICompanyService {

  getAll(): Promise<ICompany[]>;

  get(id: string): Promise<ICompany>;

  add(company: ICompany): Promise<ICompany>;

  update(company: ICompany): Promise<ICompany>;

  remove(companyId: string): Promise<ICompany>;

  addInsurance(companyId: string, insurance: IFleetInsurance): Promise<IFleetInsurance>;
  updateInsurance(companyId: string, insuranceId: string, check: IFleetInsurance): Promise<IFleetInsurance>;


}