
export interface IDocumentStatus {
  documentId: string,
  status: string,
  name: string,
  statusChangedDateTime: Date,
  signed?: boolean
}

export interface  ITemplate {
  _id?: string,
  _companyId?: string,
  name: string,
  folderName?: string,
  selected?: boolean,
  html?: string
}

export interface IDocument {
  documentId: string,
  name: string,
  status: string,
  statusDateTime: Date,
  uri: string,
  embeddingUri?: string,
  signed?: boolean,
  templates: [string],
  pdf?: any
}

export interface IRecipient {
  id?: string,
  firstName: string,
  lastName: string,
  email: string
}