import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {
  SearchDriverAction, SelectDriverReferenceAction,
} from "../../store/drivers/driver.actions";
import {SelectLoggedInUser} from "../../store/authentication/authentication.selectors";
import {IUser} from "../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../shared/constants";
import {ClearDriverInsuranceAction, SelectDriverInsuranceAction} from "../../store/drivers/driver-insurance.actions";
import {SelectDriver} from "../../store/drivers/driver.selectors";
import {Observable} from "rxjs/Observable";
import {LoadVehicleAction} from "../../store/vehicles/vehicle.actions";
import {ClearDriverFawAction, SelectDriverFawAction} from '../../store/drivers/driver-faw.actions';

@Injectable()
export class DriverUserResolve {

  private user: IUser;

  constructor(private store: Store<AppState>) {
    this.store.let(SelectLoggedInUser())
      .subscribe(user => this.user = user);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean|Observable<boolean> {

    if (this.user.role !== Roles.Driver) return false;

    let companyId = this.user ? this.user._companyId : null;
    let userId = this.user ? this.user._id : null;
    if (!companyId || !userId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new SearchDriverAction(companyId, { '_user' : userId}));

    let referenceId = route.params['referenceId'];
    if (referenceId) {
      return this.store.let(SelectDriver())
        .filter(driver => driver != null)
        .take(1)
        .map(driver => {
          this.store.dispatch(new SelectDriverReferenceAction(referenceId));
          return true;
        })

    }

    if (route.url.find(segment => segment.path === 'insurance')) {
      let insuranceId = route.params['insuranceId'];
      if (insuranceId) {
        return this.store.let(SelectDriver())
          .filter(driver => driver != null)
          .take(1)
          .map(driver => {
            this.store.dispatch(new SelectDriverInsuranceAction(driver._id, insuranceId));
            return true;
          })

      } else {
        if (route.url[route.url.length - 1].path == 'new') {
          this.store.dispatch(new ClearDriverInsuranceAction());
        }
      }
    }

    if (route.url.find(segment => segment.path === 'faw')) {
      let fawId = route.params['fawId'];
      if (fawId) {
        return this.store.let(SelectDriver())
          .filter(driver => driver != null)
          .take(1)
          .map(driver => {
            this.store.dispatch(new SelectDriverFawAction(driver._id, fawId));
            return true;
          })

      } else {
        if (route.url[route.url.length - 1].path == 'new') {
          this.store.dispatch(new ClearDriverFawAction());
        }
      }
    }

    if (route.url.find(segment => segment.path === 'vehicles')) {
      let vehicleId = route.params['vehicleId'];

      if (vehicleId) {
        return this.store.let(SelectDriver())
          .filter(driver => driver != null)
          .take(1)
          .map(driver => {
            this.store.dispatch(new LoadVehicleAction(companyId, vehicleId));
            return true;
          })
      }
    }

    return true;
  }
}
