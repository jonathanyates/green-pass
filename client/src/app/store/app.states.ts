import {AuthenticationState} from "./authentication/authentication.reducer";
import {CompaniesState} from "./companies/companies.state";
import {CompanyState} from "./company/company.state";
import {NavigationState} from "./navigation/navigation.state";
import {ErrorState} from "./error/error.state";
import {VehiclesState} from "./vehicles/vehicles.state";
import {UsersState} from "./users/users.state";
import {DriversState} from "./drivers/drivers.state";
import {TemplatesState} from "./templates/templates.state";
import {DocumentsState} from "./documents/document.state";
import {PageState} from "./page/page.state";
import {ProgressState} from "./progress/progress.state";
import {AddressState} from "./address/address.state";
import {ComplianceState} from "./compliance/compliance.state";
import {AlertsState} from "./alerts/alert.state";
import {ReportState} from "./reports/report.state";

export const AppStates = {
  Authentication: 'authentication',
  Navigation: 'navigation',
  Companies: 'companies',
  Company: 'company',
  Users: 'users',
  Vehicles: 'vehicles',
  Drivers: 'drivers',
  Templates: 'templates',
  Documents: 'documents',
  Address: 'address',
  Page: 'page',
  Error: 'error',
  Progress: 'progress',
  Compliance: 'compliance',
  Alerts: 'alerts',
  Reports: 'reports'
};

// defines our entire Application State structure
export interface AppState {
  authentication: AuthenticationState;
  navigation: NavigationState,
  companies: CompaniesState;
  company: CompanyState;
  users: UsersState;
  vehicles: VehiclesState;
  drivers: DriversState;
  templates: TemplatesState;
  documents: DocumentsState;
  address: AddressState;
  page: PageState;
  error: ErrorState;
  progress: ProgressState,
  compliance: ComplianceState,
  alerts: AlertsState,
  reports: ReportState
}



