import {injectable, inject} from "inversify";
import {TYPES} from "../container/container.types";
import {NextFunction, Request, Response} from "express-serve-static-core";
import codes from "../shared/error.codes";
import {IComplianceService} from "./compliance.service";

export interface IComplianceController {
  getAll(req:Request, res:Response, next: NextFunction);
  get(req:Request, res:Response, next: NextFunction);
}

@injectable()
export class ComplianceController implements IComplianceController {

  constructor(
    @inject(TYPES.services.ComplianceService) private complianceService: IComplianceService
  ) {}

  getAll = (req:Request, res:Response, next: NextFunction) => {

    this.complianceService.getAll()
      .then(compliance => res.status(codes.OK).json(compliance))
      .catch(err => next(err));
  };

  get = (req:Request, res:Response, next: NextFunction) => {

    const id = req.params.companyId;

    //this.complianceService.get(id)
    this.complianceService.updateCompanyCompliance(id)
      .then(vehicle => {
        if (!vehicle) {
          return res.status(codes.NOT_FOUND).send({message: `Compliance not found with company id ${id}`});
        }

        return res.status(codes.OK).json(vehicle);
      })
      .catch(err => next(err));
  };

}

