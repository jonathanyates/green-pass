import {IAssessment} from "../interfaces/assessment.interface";

const RiskValues = {
  Unknown: 0,
  Low: 1,
  Average: 2,
  'Above Average': 3,
  High: 4
};

const RiskArray = [ 'Unknown', 'Low', 'Average', 'Above Average', 'High' ];

export class ModuleSummary {

  type: string;
  startDate: Date;
  inviteDate: Date;
  completedDate: Date;
  completedModules: number;
  totalModules: number;
  percentageComplete: number;
  completed: string;
  complete: boolean;
  result: string;
  action: any;
  details: any;

  constructor(assessment: IAssessment) {
    this.initialise(assessment);
  }

  static create(assessment: IAssessment) {
    if (!assessment.trainingModules || assessment.trainingModules.length === 0) {
      return null;
    }

    return new ModuleSummary(assessment);
  }

  private initialise(assessment: IAssessment) {
    if (!assessment.trainingModules || assessment.trainingModules.length === 0) {
      return;
    }

    this.totalModules = assessment.trainingModules.length;
    this.completedModules = assessment.trainingModules.filter(module => module.completedDate != null).length;
    let riskValues = assessment.trainingModules.filter(module => module.riskLevel != null)
      .map(module => RiskValues[module.riskLevel]);

    let avgRisk = riskValues.length > 0
      ? Math.round(riskValues.reduce((acc, value) => acc + value, 0) / riskValues.length)
      : 0;

    let result = RiskArray[avgRisk];

    this.type = 'On Line Module';

    this.inviteDate = assessment.trainingModules.map(module => module.createdDate)
      .filter(date => date != null)
      .sort((a, b) => b.getTime() - a.getTime())[0];

    this.startDate = assessment.trainingModules.map(module => module.startDate)
      .filter(date => date != null)
      .sort((a, b) => b.getTime() - a.getTime())[0];

    this.completedDate = assessment.trainingModules.map(module => module.completedDate)
      .filter(date => date != null)
      .sort((a, b) => b.getTime() - a.getTime())[0];

    this.completed = `${this.completedModules} of ${this.totalModules} Modules`;
    this.complete = this.completedModules === this.totalModules;
    this.result = result ? result : 'Unknown';
    this.percentageComplete = this.totalModules > 0
      ? (this.completedModules / this.totalModules) * 100
      : 0;
  }
}
