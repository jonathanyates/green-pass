import {JwtHelperService} from "@auth0/angular-jwt";
import {IUser} from "../../../../../shared/interfaces/user.interface";

export class AuthenticationHelper {

  static isTokenValid = function (token: string): boolean {
    if (token && typeof token === 'string') {
      let jwtHelper: JwtHelperService = new JwtHelperService(token);
      try {
        return !jwtHelper.isTokenExpired(token);
      } catch (e) {
        console.log(e);
        return false;
      }
    }
    return false;
  };

  static getUserFromToken = function (token: string): IUser {
    if (token && typeof token === 'string') {
      let jwtHelper: JwtHelperService = new JwtHelperService(token);
      try {
        return <IUser>jwtHelper.decodeToken(token);
      } catch (e) {
        console.log(e);
        return null;
      }
    }
    return null;
  };

}
