import {FormInput, FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {FormatDate, FormatDateForInput, GetDate} from "../../../../../../../shared/utils/helpers";
import {IVehicleInspection} from "../../../../../../../shared/interfaces/vehicle.interface";
import {ISupplier} from "../../../../../../../shared/interfaces/company.interface";
import {Observable} from "rxjs";

@Injectable()
export class VehicleInspectionInputMapper {

  mapFrom(form:FormGroup, inputGroups: FormInputGroup[], vehicleInspection: IVehicleInspection): IVehicleInspection {

    let input = <FormInput>inputGroups[0].inputs.find((input:FormInput) => input.id === 'file');
    let fileChanged = input.file && input.file !== vehicleInspection.file;

    return {
      inspectionDate: GetDate(form.controls['inspectionDate'].value),
      supplierId: form.controls['supplier'].value,
      file: fileChanged ? input.file : null,
      fileType: input.fileType
    };
  }

  mapTo(inspection: IVehicleInspection, suppliers: ISupplier[]): FormInputGroup[] {

    let supplierList = suppliers.map(supplier => ({ key: supplier._id, value: supplier.name , selected: false }));

    if (inspection.supplierId) {
      let supplier = supplierList.find(item => item.key === inspection.supplierId);
      if (supplier) {
        supplier.selected = true;
      }
    }

    let reportFileInput = <FormInput>{ id: 'file', label: 'Inspection report', type:'file', required: false, value: '' };

    if (inspection.file) {
      reportFileInput.file = inspection.file;
      reportFileInput.fileType = inspection.fileType;
    }

    reportFileInput.action = file => {
      reportFileInput.file = file;
      reportFileInput.fileType = file.fileType;
      return Observable.empty();
    };

    return [
      {
        label: 'Inspection details',
        inputs: [
          { id: 'inspectionDate', label: 'Inspection Date', type:'date', required: true, autoFocus: true,
            value: FormatDateForInput(inspection.inspectionDate) },
          { id: 'supplier', label: 'Inspected By', type:'dropdown', required: true, value: inspection.supplierId,
            gridClass: 'col-md-6', options: supplierList },
          reportFileInput
        ]
      }
    ];
  }

}
