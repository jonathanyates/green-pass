import * as actions from "./vehicle.actions";
import {VehiclesState} from "./vehicles.state";
import {ActionTypes} from "../authentication/authentication.actions";
import {IVehicle} from "../../../../../shared/interfaces/vehicle.interface";
import {SaveVehicleServiceSuccessAction, SelectVehicleServiceSuccessAction} from "./vehicle-service.actions";
import {SaveVehicleInspectionSuccessAction, SelectVehicleInspectionSuccessAction} from "./vehicle-inspection.actions";
import * as VehicleServiceActions from './vehicle-service.actions';
import * as VehicleInspectionActions from './vehicle-inspection.actions';
import * as VehicleCheckActions from './vehicle-check.actions';
import {SaveVehicleCheckSuccessAction, SelectVehicleCheckSuccessAction} from "./vehicle-check.actions";
import {LoadVehiclesSuccessAction} from "./vehicle.actions";

export const initialState: VehiclesState = {
  list: null,
  total: 0,
  page: 1,
  selected: null,
  selectedService: null,
  selectedInspection: null,
  selectedCheck: null,
  selectedTab: null
};

type reducerActions = actions.Actions
  | VehicleServiceActions.Actions
  | VehicleInspectionActions.Actions
  | VehicleCheckActions.Actions;

export function VehiclesReducer(state: VehiclesState = initialState, action: reducerActions): VehiclesState {
  let serviceHistory;
  let inspectionHistory;
  let checkHistory;

  switch (action.type) {

    case actions.ActionTypes.LOAD_SUCCESS:
      const successAction = <LoadVehiclesSuccessAction>action;
      return Object.assign({}, state, {
        list: action.payload,
        total: successAction.total,
        page: successAction.page,
        selected: state.selected,
        selectedService: null,
        selectedInspection: null,
        selectedCheck: null,
        selectedTab: null
      });

    case actions.ActionTypes.SELECT_LIST_SUCCESS:
      return Object.assign({}, state, {
        selectedService: null,
        selectedInspection: null,
        selectedCheck: null,
        selectedTab: null
      });

    case actions.ActionTypes.SELECT_VEHICLE_SUCCESS:
    case actions.ActionTypes.LOAD_VEHICLE_SUCCESS:
    case actions.ActionTypes.SAVE_SUCCESS:
    case actions.ActionTypes.UPDATE_SUCCESS:
      return Object.assign({}, state, {
        list: state.list,
        selected: <IVehicle>action.payload
      });

    case actions.ActionTypes.CLEAR_LIST:
      return Object.assign({}, state, {
        list: null
      });

    // VehicleServiceActions

    case VehicleServiceActions.ActionTypes.SAVE_VEHICLE_SERVICE_SUCCESS:
      const payload = (<SaveVehicleServiceSuccessAction>action).payload;
      const vehicle = payload.vehicle;
      const service = payload.service;

      let exists = vehicle.serviceHistory.some(item => item._id === service._id);

      if (exists) {
        serviceHistory = vehicle.serviceHistory.map(item => {
          return item._id === service._id ? service : item
        });
      } else {
        serviceHistory = [...vehicle.serviceHistory, service];
      }

      return Object.assign({}, state, {
        selected: Object.assign({}, vehicle, {
          serviceHistory: serviceHistory
        }),
        selectedService: service
      });

    case VehicleServiceActions.ActionTypes.SELECT_VEHICLE_SERVICE_SUCCESS:
      let selectedService = (<SelectVehicleServiceSuccessAction>action).payload;
      if (selectedService && selectedService.file) {
        serviceHistory = state.selected.serviceHistory.map(item => {
          return item._id === selectedService._id ? selectedService : item
        });

        return Object.assign({}, state, {
          selected: Object.assign({}, state.selected, {
            serviceHistory: serviceHistory
          }),
          selectedService: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedService: action.payload
        });
      }

    case VehicleServiceActions.ActionTypes.CLEAR_VEHICLE_SERVICE:
      return Object.assign({}, state, {
        selectedService: null
      });

    // VehicleInspectionActions

    case VehicleInspectionActions.ActionTypes.SAVE_VEHICLE_INSPECTION_SUCCESS:
      const inspectionPayload = (<SaveVehicleInspectionSuccessAction>action).payload;
      const inspectionVehicle = inspectionPayload.vehicle;
      const inspection = inspectionPayload.inspection;

      let inspectionExists = inspectionVehicle.inspectionHistory.some(item => item._id === inspection._id);

      if (inspectionExists) {
        inspectionHistory = inspectionVehicle.inspectionHistory.map(item => {
          return item._id === inspection._id ? inspection : item
        });
      } else {
        inspectionHistory = [...inspectionVehicle.inspectionHistory, inspection];
      }

      return Object.assign({}, state, {
        selected: Object.assign({}, inspectionVehicle, {
          inspectionHistory: inspectionHistory
        }),
        selectedInspection: inspection
      });

    case VehicleInspectionActions.ActionTypes.SELECT_VEHICLE_INSPECTION_SUCCESS:
      let selectedInspection = (<SelectVehicleInspectionSuccessAction>action).payload;
      if (selectedInspection && selectedInspection.file) {
        inspectionHistory = state.selected.inspectionHistory.map(item => {
          return item._id === selectedInspection._id ? selectedInspection : item
        });

        return Object.assign({}, state, {
          selected: Object.assign({}, state.selected, {
            inspectionHistory: inspectionHistory
          }),
          selectedInspection: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedInspection: action.payload
        });
      }

    case VehicleInspectionActions.ActionTypes.CLEAR_VEHICLE_INSPECTION:
      return Object.assign({}, state, {
        selectedInspection: null
      });

    //  VehicleCheckActions

    case VehicleCheckActions.ActionTypes.SAVE_VEHICLE_CHECK_SUCCESS:
      const checkPayload = (<SaveVehicleCheckSuccessAction>action).payload;
      const checkVehicle = checkPayload.vehicle;
      const check = checkPayload.check;

      let checkExists = checkVehicle.checkHistory.some(item => item._id === check._id);

      if (checkExists) {
        checkHistory = checkVehicle.checkHistory.map(item => {
          return item._id === check._id ? check : item
        });
      } else {
        checkHistory = [...checkVehicle.checkHistory, check];
      }

      return Object.assign({}, state, {
        selected: Object.assign({}, checkVehicle, {
          checkHistory: checkHistory
        }),
        selectedCheck: check
      });

    case VehicleCheckActions.ActionTypes.SELECT_VEHICLE_CHECK_SUCCESS:
      let selectedCheck = (<SelectVehicleCheckSuccessAction>action).payload;
      if (selectedCheck && selectedCheck.file) {
        checkHistory = state.selected.checkHistory.map(item => {
          return item._id === selectedCheck._id ? selectedCheck : item
        });

        return Object.assign({}, state, {
          selected: Object.assign({}, state.selected, {
            checkHistory: checkHistory
          }),
          selectedCheck: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedCheck: action.payload
        });
      }

    case VehicleCheckActions.ActionTypes.CLEAR_VEHICLE_CHECK:
      return Object.assign({}, state, {
        selectedCheck: null
      });


    case actions.ActionTypes.SELECT_SELECTED_TAB:
      return Object.assign({}, state, {
        selectedTab: action.payload
      });

    case ActionTypes.LOGOUT:
      return initialState;

    default:
      return state;
  }
}
