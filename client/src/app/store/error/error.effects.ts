import { Injectable } from '@angular/core';
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./error.actions";
import {Router} from "@angular/router";
import {Paths} from "../../shared/constants";
import {ServerError} from "../../shared/errors/server.error";
import {BusyAction} from "../progress/progress.actions";
import {AppState} from "../app.states";
import {Store} from "@ngrx/store";

@Injectable()
export class ErrorEffects {

  constructor(
    private actions$:Actions,
    private router: Router,
    private store: Store<AppState>) { }

  @Effect() handleError$ = this.actions$
    .ofType(actions.ActionTypes.HANDLE_ERROR)
    .distinctUntilChanged()
    .map((action: actions.HandleErrorAction) => action.payload)
    .do(error => this.onError(error))
    .map(error => new actions.SetErrorAction(error))
    .finally(() => this.store.dispatch(new BusyAction(false)));

  private onError(error: ServerError) {
    switch (error.code) {
      case 401:
      case 403:
        return this.router.navigate([Paths.Unauthorized]);
      case 404:
      case 500:
      default:
        return this.router.navigate([Paths.Error]);
    }
  }
}
