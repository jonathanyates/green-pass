import {FormInput, FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {FormGroup} from '@angular/forms';
import {AddressInputMapper} from '../../shared/components/address-input/address-input.mapper';
import {Injectable} from '@angular/core';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {
  ICompanySubscription, ISubscription,
  ITrialSubscription
} from '../../../../../../shared/interfaces/subscription.interface';
import {FormatDateForInput, GetDate} from '../../../../../../shared/utils/helpers';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SubscriptionInputMapper {

  constructor(private addressInputMapper: AddressInputMapper) {
  }

  mapFrom(form: FormGroup, subscriptions: ISubscription[]): ICompanySubscription {

    let alerts = form.controls['alerts'].value;
    if (alerts === 'Unlimited') {
      alerts = -1;
    } else if (alerts === '' || alerts == null) {
      alerts = 0;
    }

    const subscription: ICompanySubscription = {
      startDate: GetDate(form.controls['startDate'].value),
      renewalDate: GetDate(form.controls['renewalDate'].value),
      active: true,
      name: form.controls['subscription'].value,
      resources: {
        mandates: true,
        licenceCheck: true,
        alerts: alerts,
        onLineAssessments: this.getBooleanValue('onLineAssessments', form),
        onRoadTraining: this.getBooleanValue('onRoadTraining', form),
        workshops: this.getBooleanValue('workshops', form),
        presentations: this.getBooleanValue('presentations', form)
      }
    };

    const includeTrial = form.controls['trial'].value;

    if (includeTrial === true) {

      let trialAlerts = form.controls['trial-alerts'].value;
      if (trialAlerts === 'Unlimited') {
        trialAlerts = -1;
      } else {
        trialAlerts = +trialAlerts;
      }

      subscription.trial = {
        endDate: GetDate(form.controls['trial-endDate'].value),
        subscription: {
          name: 'Trial',
          resources: {
            mandates: true,
            licenceCheck: true,
            alerts: trialAlerts,
            onLineAssessments: this.getBooleanValue('trial-onLineAssessments', form),
            onRoadTraining: this.getBooleanValue('trial-onRoadTraining', form),
            workshops: this.getBooleanValue('trial-workshops', form),
            presentations: this.getBooleanValue('trial-presentations', form)
          }
        }
      };
    } else {
      subscription.trial = null;
    }

    return subscription;
  }

  mapTo(company: ICompany, subscriptions: ISubscription[]): FormInputGroup[] {

    const hidden = !company.subscription;

    const companySubscription: ICompanySubscription = company.subscription
      ? company.subscription
      : this.createSubscription();

    const subscriptionTypes = subscriptions.map(subscription => {
      return {key: subscription.name, value: subscription.name, selected: companySubscription.name === subscription.name}
    });

    const subscriptionInput: FormInput = {
      id: 'subscription', label: 'Subscription Level', type: 'dropdown', required: true,
      value: companySubscription.name, options: subscriptionTypes
    };

    subscriptionInput.select = (subscriptionName) => {
      const subscription: ISubscription = subscriptions.find(x => x.name === subscriptionName);
      resourceInputs.forEach((input: FormInput) => {
        input.hidden = false;
        const property = input.id;
        let value = subscription.resources[property];
        if (value === -1) {
          value = 'Unlimited';
        }
        input.control.setValue(value);
      });
    };

    const resourceInputs = this.getResourceInputs(companySubscription, hidden);

    const subscriptionGroup = {
        label: 'Subscription',
        inputs: [
          {
            id: 'startDate', label: 'Start Date', type: 'date', required: true, autoFocus: true,
            value: FormatDateForInput(companySubscription.startDate)
          },
          {
            id: 'renewalDate', label: 'Renewal Date', type: 'date', required: true,
            value: FormatDateForInput(companySubscription.renewalDate)
          },
          subscriptionInput,
          ...resourceInputs
        ]
      };

    const trialGroup = this.getTrialGroup(companySubscription, hidden);

    return [
      subscriptionGroup,
      trialGroup
    ];
  }

  private createSubscription() {
    return {
      name: null,
      startDate: new Date(),
      renewalDate: new Date().addMonths(12),
      active: true,
      resources: {
        mandates: true,
        licenceCheck: true,
        onLineAssessments: false,
        onRoadTraining: false,
        presentations: false,
        workshops: false,
        alerts: 0
      }
    };
  }

  private getTrialGroup(companySubscription: ICompanySubscription, hidden: boolean) {
    const includeTrial = companySubscription.trial != null;

    const includeTrialInput: FormInput = {
      id: 'trial', label: 'Include Trial', type: 'checkbox', required: false, value: includeTrial
    };

    const trialSubscription: ITrialSubscription = includeTrial
      ? companySubscription.trial
      : {
        endDate: new Date().addMonths(1),
        subscription: {
          name: 'Trial',
          resources: Object.assign({}, companySubscription.resources)
        }
      };

    const trialResourceInputs = [
      {
        id: 'trial-endDate', label: 'End Date', type: 'date', required: true, hidden: !includeTrial,
        value: FormatDateForInput(trialSubscription.endDate)
      },
      ...this.getResourceInputs(trialSubscription.subscription, !includeTrial, 'trial-')
    ];

    const trialGroup: FormInputGroup = {
      label: 'Trial',
      inputs: [
        includeTrialInput,
        ...trialResourceInputs
      ]
    };

    includeTrialInput.action = checked => {
      trialResourceInputs.forEach(input => input.hidden = checked === false);
      return Observable.empty();
    };

    return trialGroup;
  }

  private getResourceInputs(companySubscription: ISubscription, hidden: boolean, prefix: string = ''): FormInput[] {
    return [
      {
        id: prefix + 'alerts', label: 'No of alerts', type: 'text', required: false, autoFocus: true, hidden: hidden,
        value: companySubscription.resources.alerts === -1 ? 'Unlimited' : companySubscription.resources.alerts
      },
      {
        id: prefix + 'onLineAssessments',
        label: 'Enable On Line Assessments',
        type: 'checkbox',
        required: false,
        hidden: hidden,
        value: companySubscription.resources.onLineAssessments
      },
      {
        id: prefix + 'onRoadTraining',
        label: 'Enable On Road Training',
        type: 'checkbox',
        required: false,
        hidden: hidden,
        value: companySubscription.resources.onRoadTraining
      },
      {
        id: prefix + 'presentations',
        label: 'Enable Presentations',
        type: 'checkbox',
        required: false,
        hidden: hidden,
        value: companySubscription.resources.presentations
      },
      {
        id: prefix + 'workshops',
        label: 'Enable Workshops',
        type: 'checkbox',
        required: false,
        hidden: hidden,
        value: companySubscription.resources.workshops
      },
    ];


  }

  private getBooleanValue(field: string, form: FormGroup): boolean {
    const value = form.controls[field].value;
    return value === true;
  }
}


