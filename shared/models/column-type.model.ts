export enum ColumnType {
  Default,
  CheckBox,
  Expander,
  Button,
  Icon
}