import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {
  LoadTemplatesAction,
  SelectCompanyTemplateAction, SelectTemplateAction,
  SelectTemplateSuccessAction
} from "../../../store/templates/template.actions";
import {SelectCompanyAction} from "../../../store/company/company.actions";

@Injectable()
export class CompanyTemplateResolve {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    let templateId = route.params['templateId'];

    if (!companyId) {
      return false;
    }

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new LoadTemplatesAction());

    if (templateId) {
      this.store.dispatch(new SelectCompanyTemplateAction(companyId, templateId));
    } else {
      this.store.dispatch(new SelectTemplateSuccessAction({
        _companyId: companyId,
        name: '',
        html: '',
        selected: true
      }));
    }

    return true;
  }
}
