import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./report.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {BusyAction} from "../progress/progress.actions";
import {SelectReportTypes} from "./report.selectors";
import {SelectReportTypesSuccessAction} from "./report.actions";
import {Messages} from "../app.messages";
import {messenger} from "../../services/messenger";
import {Router} from "@angular/router";
import {Paths} from "../../shared/constants";
import {ReportService} from "app/services/report.service";

@Injectable()
export class ReportsEffects {

  constructor(
    private reportService:ReportService,
    private actions$:Actions,
    private store: Store<AppState>,
    private router: Router) {
  }

  // Report Types
  @Effect() selectReportTypes$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_REPORT_TYPES)
    .switchMap(action => this.store.let(SelectReportTypes()).take(1)
      .map(reportTypes => reportTypes
        ? new SelectReportTypesSuccessAction(reportTypes)
        : new actions.LoadReportTypesAction())
    );

  @Effect() loadReportTypes$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_REPORT_TYPES)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap(action => this.reportService.getReportTypes()
      .map(reportTypes =>  new actions.LoadReportTypesSuccessAction(reportTypes))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveReportType$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_REPORT_TYPE)
    .map((action: actions.SaveReportTypeAction) => action.payload)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap(reportType => this.reportService.saveReportType(reportType)
      .map(reportType =>  new actions.SaveReportTypeSuccessAction(reportType))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        messenger.publish(Messages.SaveError, error);
        return Observable.empty();
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({dispatch:false}) saveReportTypeSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_REPORT_TYPE_SUCCESS)
    .map((action: actions.SaveReportTypeSuccessAction) => action.payload)
    .do(reportType => this.router.navigate([Paths.Admin, Paths.ReportTypes]));

}
