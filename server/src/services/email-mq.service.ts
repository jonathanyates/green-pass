import logger from 'winston';
import container from '../container/container.config';
import { TYPES } from '../container/container.types';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { configureLogger } from './logger';
import { IEmail, IEmailService } from '../email/email.service';

import serverConfig from '../server/server.config';

configureLogger('email', 'GreenPass-Email-Service');

logger.info('Starting Email MQ Service...');

const messageService = container.get<IMessageService>(TYPES.services.MessageService);
const emailService = container.get<IEmailService>(TYPES.services.EmailService);

messageService.receive<IEmail>(queueNames.sendEmail).subscribe({
  next: (email: IEmail) => {
    if (email && serverConfig.email.sendEmails === true) {
      logger.info(`Sending email...`);
      emailService
        .send(email.recipients, email.subject, email.content, email.from, email.attachments)
        .catch((error) => {
          logger.error(`An error occured attempting to send email. \nError: ${error.message}`);
        });
    }
  },
  error: (error) => {
    logger.error(`Email Service MQ error. \nError: ${error.message}`);
    throw error;
  },
});

logger.info('Email MQ Service is running.');
