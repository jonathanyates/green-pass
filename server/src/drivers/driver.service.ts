import logger = require('winston');
import '../../../shared/extensions/date.extensions';
import { injectable, inject } from 'inversify';
import { IDriver, IDriverImport, IDriverInsurance, IDrivingLicence } from '../../../shared/interfaces/driver.interface';
import { TYPES } from '../container/container.types';
import { IApiService } from '../interfaces/api-service.interface';
import { IDriverModel } from './driver.schema';
import { IDvlaService } from '../dvla/service/dvla.service.interface';
import { IDocumentService } from '../documents/service/document.service.interface';
import { IDocumentSignatureService } from '../documents/interfaces/signature.service.interface';
import { IAssessmentService } from '../assessments/assessment.service';
import { IOnRoadAssessment } from '../../../shared/interfaces/assessment.interface';
import { IFileService } from '../files/file.service';
import codes from '../shared/error.codes';
import { ServerError } from '../shared/errors';
import { IUserService } from '../users/user.service';
import { IDriverRepository } from './driver.repository';
import serverConfig from '../server/server.config';
import { IMessageService } from '../messaging/message.service';
import { IDriverVehicleMapRepository } from './driver-vehicle-map.repository';
import { IDbContext } from '../db/db.context.interface';
import { IVehicleRepository } from '../vehicles/vehicle.repository';
import { ICompanyRepository } from '../companies/company.repository';
import { IDocumentStatus } from '../../../shared/interfaces/document.interface';
import { IPointsCheckPeriod } from '../../../shared/interfaces/company-settings.interface';
import { IFawCertificate } from '../../../shared/interfaces/common.interface';

export interface IDriverService extends IApiService<IDriver> {
  findByPage(criteria: any, page: number, pageSize: number): Promise<{ total: number; items: IDriver[] }>;

  remove(driverId: string): Promise<IDriver>;

  getDrivingLicenceCheckCode(
    drivingLicenceNumber: string,
    nationalInsuranceNumber: string,
    postCode: string
  ): Promise<string>;

  drivingLicenceCheck(driver: IDriver, checkCode: string, drivingLicenceNumber?: string): Promise<IDriver>;

  updateAllDriverDocuments(): Promise<IDriver[][][]>;

  updateCompanyDriverDocuments(companyId: string): Promise<IDriver[][]>;

  updateDriverDocuments(driverId: string): Promise<IDriver>;

  updateOnRoadAssessment(companyId: string, driverId: string, assessmentId: string, assessment: any): Promise<IDriver>;

  addOnRoadAssessment(companyId: string, driverId: string, onRoadAssessment: any): Promise<IDriver>;

  addInsurance(companyId: string, driverId: string, insurance: IDriverInsurance): Promise<IDriverInsurance>;

  updateInsurance(
    companyId: string,
    driverId: string,
    insuranceId: string,
    check: IDriverInsurance
  ): Promise<IDriverInsurance>;

  deleteInsurance(companyId: string, driverId: string, insuranceId: string): Promise<IDriver>;

  addFawCertificate(companyId: string, driverId: string, insurance: IFawCertificate): Promise<IFawCertificate>;

  updateFawCertificate(
    companyId: string,
    driverId: string,
    insuranceId: string,
    check: IFawCertificate
  ): Promise<IFawCertificate>;

  deleteFawCertificate(companyId: string, driverId: string, insuranceId: string): Promise<IDriver>;

  driverAssessmentChecks(): Promise<IDriver[]>;

  addVehicle(driverId: string, vehicleId: string): Promise<IDriver>;

  removeVehicle(driverId: string, vehicleId: string): Promise<IDriver>;

  importDrivers(companyId: string, drivers: IDriverImport[]): Promise<IDriver[]>;
}

@injectable()
export class DriverService implements IDriverService {
  constructor(
    @inject(TYPES.repositories.DriverRepository) private driverRepository: IDriverRepository,
    @inject(TYPES.repositories.VehicleRepository) private vehicleRepository: IVehicleRepository,
    @inject(TYPES.repositories.DriverVehicleMapRepository)
    private driverVehicleMapRepository: IDriverVehicleMapRepository,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.DvlaService) private dvlaService: IDvlaService,
    @inject(TYPES.services.DocumentService) private documentService: IDocumentService,
    @inject(TYPES.services.DocumentSignatureService) private signatureService: IDocumentSignatureService,
    @inject(TYPES.services.AssessmentService) private assessmentService: IAssessmentService,
    @inject(TYPES.services.UserService) private userService: IUserService,
    @inject(TYPES.services.FileService) private fileService: IFileService,
    @inject(TYPES.services.MessageService) private messageService: IMessageService,
    @inject(TYPES.db.DbContext) private dbContext: IDbContext
  ) {}

  driverAssessmentChecks(): Promise<IDriver[]> {
    // update drivers assessments where the lastAssessmentLoginDate > this.lastAssessmentUpdatedDate
    return this.driverRepository
      .find({
        $where: function () {
          return this.lastAssessmentLoginDate > this.lastAssessmentUpdatedDate;
        },
      })
      .then((drivers) => {
        return Promise.all(
          drivers.map((driver) => {
            return this.assessmentService.updateAssessments(driver);
          })
        );
      });
  }

  updateOnRoadAssessment(
    companyId: string,
    driverId: string,
    assessmentId: string,
    assessment: IOnRoadAssessment
  ): Promise<any> {
    return this.driverRepository.get(driverId).then((driver: IDriverModel) => {
      const driverAssessment = driver.assessments.find(
        (item) => item.onRoad != null && item.onRoad._id != null && item.onRoad._id.toString() == assessmentId
      );
      const onRoadAssessment = driverAssessment ? driverAssessment.onRoad : null;

      if (!onRoadAssessment) {
        throw ServerError(`On Road Assessment ${assessmentId} not found for vehicle ${driverId}.`, codes.NOT_FOUND);
      }

      const file = assessment.file;
      const path = file ? file.path : null;
      const filename = onRoadAssessment.filename ? onRoadAssessment.filename : file ? file.filename : null;

      onRoadAssessment.filename = filename;
      onRoadAssessment.assessmentDate = new Date(assessment.assessmentDate);
      onRoadAssessment.assessedBy = assessment.assessedBy;
      onRoadAssessment.result = assessment.result;
      onRoadAssessment.complete = assessment.complete;
      onRoadAssessment.fileType = assessment.fileType;

      if (driverAssessment.complete == false && onRoadAssessment.complete == true) {
        driverAssessment.complete = true;
        driverAssessment.completedDate = onRoadAssessment.assessmentDate;
        driver.nextAssessmentDate = Date.prototype.getToday().addMonths(12);
      }

      if (path) {
        const options = {
          filename: filename,
          metadata: {
            vehicleId: driver._id,
            checkId: assessmentId,
          },
        };

        return this.fileService.updateFile(filename, path, options).then((x) => this.update(driver));
      }

      return this.update(driver);
    });
  }

  addOnRoadAssessment(companyId: string, driverId: string, onRoadAssessment: IOnRoadAssessment): Promise<any> {
    return this.driverRepository.get(driverId).then((driver: IDriverModel) => {
      if (!driver.assessments) {
        driver.assessments = [];
      }
      driver.assessments.push({
        onRoad: onRoadAssessment,
        complete: false,
      });

      return this.update(driver);
    });
  }

  getAll = (companyId: string): Promise<IDriver[]> => {
    return this.driverRepository.find({ _companyId: companyId });
  };

  get = (id: string): Promise<IDriver> => {
    return this.driverRepository.get(id);
  };

  find(criteria: any): Promise<IDriver[]> {
    return this.driverRepository.find(criteria);
  }

  findByPage(criteria: any, page: number, pageSize: number): Promise<{ total: number; items: IDriver[] }> {
    return this.driverRepository.find(criteria, page, pageSize).then((drivers: IDriverModel[]) => {
      return this.driverRepository
        .count(criteria)
        .then((total) => (total != null && total.length > 0 ? total[0].count : 0))
        .then((total) => {
          return {
            total: total,
            page: page,
            items: drivers,
          };
        });
    });
  }

  findOne(criteria: any): Promise<IDriver> {
    return this.driverRepository.findOne(criteria);
  }

  add = async (companyId: string, driver: IDriver): Promise<IDriver> => {
    const company = await this.companyRepository.get(companyId);

    if (!company) {
      throw ServerError(`Can not add driver. Company ${driver._companyId} not found.`, codes.NOT_FOUND);
    }

    driver._companyId = companyId;
    if (driver.licenceNumber) {
      const existing = await this.driverRepository.findOne({ licenceNumber: driver.licenceNumber });

      if (existing) {
        throw ServerError('Driver with same licence number is already registered.', codes.CONFLICT);
      }
    }

    driver._user._companyId = companyId;
    driver._user.role = 'driver';

    const user = await this.userService.createUserAccount(driver._user, company);
    logger.info(`Added user account for driver ${driver._user.username}`);

    driver._user = user._id;
    driver = await this.driverRepository.add(driver);
    logger.info(`Added driver ${driver._user.username}`);

    if (company.subscription.resources.onLineAssessments) {
      if (serverConfig.roadMarque.updateAssessments) {
        driver = await this.assessmentService.updateAssessments(driver);
        driver = await this.driverRepository.update(driver);
      }
    }

    const documents = await this.documentService.sendDocuments(driver._id);

    if (documents && documents.length > 0) {
      logger.info(`Sent ${documents.length} documents for driver ${driver._user.username}`);
    }
    return driver;
  };

  update = (driver: IDriver): Promise<IDriver> => {
    // if the licence number has changed since the last check then a new check needs to be done
    if (
      driver.licence &&
      driver.licence.checks &&
      driver.licence.checks.length > 0 &&
      driver.licence.checks.slice(-1)[0].number !== driver.licenceNumber
    ) {
      driver.licence.nextCheckDate = new Date();
    }

    return this.driverRepository.update(driver);
  };

  remove = (driverId: string): Promise<IDriver> => {
    return (
      this.driverRepository
        .remove(driverId)
        // remove user
        .then((driver) =>
          this.userService
            .get(driver._user._id)
            .then((user) => this.userService.remove(user._id))
            // remove driver vehicle maps
            .then((_) =>
              this.driverVehicleMapRepository
                .getDriverVehicles(driverId)
                .then((vehicles) =>
                  Promise.all(vehicles.map((vehicle) => this.driverVehicleMapRepository.remove(driverId, vehicle._id)))
                )
            )
            .then((_) => driver)
        )
    );
  };

  importDrivers = (companyId: string, drivers: IDriverImport[]): Promise<IDriver[]> => {
    return Promise.all(drivers.map((info) => this.importDriver(companyId, info)));
  };

  importDriver = (companyId: string, driverImport: IDriverImport) => {
    const driver: IDriver = {
      _user: {
        title: null,
        forename: driverImport.forename,
        surname: driverImport.surname,
        email: driverImport.email,
        role: 'driver',
        username: null,
        password: null,
      },
      gender: null,
      licenceNumber: null,
      niNumber: null,
      phone: null,
      mobile: null,
      address: null,
      dateOfBirth: null,
      licence: {
        checks: [],
      },
      assessments: [],
      ownedVehicle: driverImport.ownedVehicle,
    }; // driver

    return this.add(companyId, driver)
      .then((driver) => {
        return driver;
      })
      .catch((error) => {
        logger.error(`Error importing driver ${driverImport.forename} ${driverImport.surname}.\n ${error}`);
        return null;
      });
  };

  // Driving Licence

  getDrivingLicenceCheckCode = (
    drivingLicenceNumber: string,
    nationalInsuranceNumber: string,
    postCode: string
  ): Promise<string> => {
    return this.dvlaService.generateCheckCode(drivingLicenceNumber, nationalInsuranceNumber, postCode);
  };

  drivingLicenceCheck = (driver: IDriver, checkCode: string, drivingLicenceNumber?: string): Promise<IDriver> => {
    const licenceNumber = drivingLicenceNumber ? drivingLicenceNumber : driver.licenceNumber;

    if (!licenceNumber) {
      throw ServerError(
        `Unable to check driving licence. No Driver Licence Number provided for driver ${driver._id}.`,
        codes.BAD_REQUEST
      );
    }

    return this.dvlaService.drivingLicenceCheck(licenceNumber, checkCode).then((enquiry) => {
      const licence: IDrivingLicence = {
        number: licenceNumber,
        status: enquiry.licence.status,
        issueNumber: enquiry.licence.issueNumber,
        validFrom: enquiry.licence.validFrom,
        validTo: enquiry.licence.validTo,
        allowedVehicles: enquiry.allowedVehicles,
        endorsements: enquiry.endorsements,
        tachographCard: enquiry.tachographCard,
        cpc: enquiry.cpc,
        points: enquiry.points,
        checkCode: checkCode,
        checkDate: new Date(),
      };

      driver.licenceNumber = licenceNumber;

      if (!driver.licence) {
        driver.licence = { checks: [] };
      }

      if (!driver.licence.checks) {
        driver.licence.checks = [];
      }
      driver.licence.checks.push(licence);

      return this.companyRepository.get(driver._companyId).then((company) => {
        if (company.settings.drivers.licenceChecks && company.settings.drivers.licenceChecks.checkPeriods) {
          driver.licence.nextCheckDate = this.getNextCheckDate(
            licence.points,
            company.settings.drivers.licenceChecks.checkPeriods
          );
        } else {
          if (licence.points > 6) {
            driver.licence.nextCheckDate = new Date().addMonths(3);
          } else if (licence.points > 3) {
            driver.licence.nextCheckDate = new Date().addMonths(6);
          } else {
            driver.licence.nextCheckDate = new Date().addMonths(12);
          }
        }

        return this.update(driver);
      });
    });
  };

  private getNextCheckDate(points: number, pointsCheckPeriods: IPointsCheckPeriod[]) {
    const licenceCheckPeriods = pointsCheckPeriods.sort((a, b) => b.noOfPoints - a.noOfPoints);

    const period = licenceCheckPeriods.find((period) => points >= period.noOfPoints);

    if (period && period.months) {
      return new Date().addMonths(period.months);
    }

    return new Date().addMonths(12);
  }

  // Driver Documents

  updateAllDriverDocuments = (): Promise<IDriver[][][]> => {
    return this.companyRepository
      .getAll()
      .then((companies) => Promise.all(companies.map((company) => this.updateCompanyDriverDocuments(company._id))));
  };

  updateCompanyDriverDocuments = (companyId: string): Promise<IDriver[][]> => {
    return this.driverRepository.find({ _companyId: companyId }).then((drivers) => {
      const documentIds = drivers.reduce((acc, driver) => {
        return acc.concat(driver.documents.map((document) => document.documentId));
      }, []);

      if (documentIds.length === 0) {
        return Promise.resolve([]);
      }

      return this.signatureService.getDocumentStatus(documentIds).then((documents) => {
        return Promise.all(drivers.map((driver) => this.updateDocuments(driver, documents)));
      });
    });
  };

  updateDriverDocuments = (driverId: string): Promise<IDriver> => {
    return this.get(driverId).then((driver) => {
      if (!driver) {
        return Promise.reject(
          ServerError('updateDriverDocuments:: Driver not found for driver id ' + driverId, codes.NOT_FOUND)
        );
      }

      return this.documentService
        .getDocuments(driver._id)
        .then((documents) => {
          // Only update unsigned document status to safeguard against documents that might have been archived after signing.
          const unsignedDocuments = documents
            .filter((document) => !document.signed)
            .map((document) => document.documentId);

          return this.signatureService.getDocumentStatus(unsignedDocuments);
        })
        .then((documents) =>
          this.updateDocuments(
            driver,
            documents.filter((document) => document != null)
          )
        );
    });
  };

  private updateDocuments = (driver: IDriver, documents: IDocumentStatus[]): Promise<IDriver> => {
    driver.documents.forEach((driverDocument) => {
      const updatedDocument = documents.find((env) => env.documentId == driverDocument.documentId);
      if (updatedDocument) {
        driverDocument.status = updatedDocument.status;
        driverDocument.statusDateTime = updatedDocument.statusChangedDateTime;
        driverDocument.signed = updatedDocument.signed;
      }
    });

    return this.update(driver);
  };

  // insurance

  addInsurance(companyId: string, driverId: string, insurance: IDriverInsurance): Promise<IDriverInsurance> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      const file = insurance.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const driverInsurance = {
        validFrom: new Date(insurance.validFrom),
        validTo: new Date(insurance.validTo),
        filename: filename,
        fileType: insurance.fileType,
      };

      if (!driver.insuranceHistory) {
        driver.insuranceHistory = [];
      }
      driver.insuranceHistory.push(driverInsurance);
      insurance = driver.insuranceHistory.find((insurance) => insurance.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          driverId: driver._id,
          insuranceId: insurance._id,
        },
      };

      if (path) {
        return this.fileService.createFile(filename, path, options).then((x) => {
          this.driverRepository.update(driver).then((driver) => insurance);
        });
      }

      return this.driverRepository.update(driver).then((driver) => insurance);
    });
  }

  updateInsurance(companyId: string, driverId: string, insuranceId: string, insurance: any): Promise<IDriverInsurance> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      let driverInsurance = driver.insuranceHistory.find((item) => item._id.toString() == insuranceId);

      if (!driverInsurance) {
        throw ServerError(`Insurance ${insuranceId} not found for driver ${driverId}.`, codes.NOT_FOUND);
      }

      const file = insurance.file;
      const filename = driverInsurance.filename ? driverInsurance.filename : file ? file.filename : null;

      driverInsurance.filename = filename;
      driverInsurance.validFrom = new Date(insurance.validFrom);
      driverInsurance.validTo = new Date(insurance.validTo);
      driverInsurance.fileType = insurance.fileType;

      return <Promise<IDriverInsurance>>driver.save().then((driver) => {
        driverInsurance = driver.insuranceHistory.find((item) => item._id.toString() == insuranceId);

        if (file) {
          const path = file.path;

          const options = {
            filename: filename,
            metadata: {
              driverId: driver._id,
              insuranceId: insuranceId,
            },
          };

          return this.fileService.updateFile(filename, path, options).then((x) => driverInsurance);
        }

        return driverInsurance;
      });
    });
  }

  deleteInsurance(companyId: string, driverId: string, insuranceId: string): Promise<IDriver> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      const driverInsurance = driver.insuranceHistory.find((item) => item._id.toString() == insuranceId);

      if (!driverInsurance) {
        throw ServerError(`Insurance ${insuranceId} not found for driver ${driverId}.`, codes.NOT_FOUND);
      }

      driver.insuranceHistory = driver.insuranceHistory.filter((item) => item !== driverInsurance);

      return driver.save();
    });
  }

  // faw

  addFawCertificate(companyId: string, driverId: string, faw: IFawCertificate): Promise<IFawCertificate> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      const file = faw.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const driverFawCertificate = {
        validFrom: new Date(faw.validFrom),
        validTo: new Date(faw.validTo),
        filename: filename,
        fileType: faw.fileType,
      };

      if (!driver.fawHistory) {
        driver.fawHistory = [];
      }
      driver.fawHistory.push(driverFawCertificate);
      faw = driver.fawHistory.find((faw) => faw.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          driverId: driver._id,
          fawId: faw._id,
        },
      };

      if (path) {
        return this.fileService
          .createFile(filename, path, options)
          .then((x) => this.driverRepository.update(driver).then((driver) => faw));
      }

      return this.driverRepository.update(driver).then((driver) => faw);
    });
  }

  updateFawCertificate(companyId: string, driverId: string, fawId: string, faw: any): Promise<IFawCertificate> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      let driverFawCertificate = driver.fawHistory.find((item) => item._id.toString() == fawId);

      if (!driverFawCertificate) {
        throw ServerError(`FawCertificate ${fawId} not found for driver ${driverId}.`, codes.NOT_FOUND);
      }

      const file = faw.file;
      const filename = driverFawCertificate.filename ? driverFawCertificate.filename : file ? file.filename : null;

      driverFawCertificate.filename = filename;
      driverFawCertificate.validFrom = new Date(faw.validFrom);
      driverFawCertificate.validTo = new Date(faw.validTo);
      driverFawCertificate.fileType = faw.fileType;

      return <Promise<IFawCertificate>>driver.save().then((driver) => {
        driverFawCertificate = driver.fawHistory.find((item) => item._id.toString() == fawId);

        if (file) {
          const path = file.path;

          const options = {
            filename: filename,
            metadata: {
              driverId: driver._id,
              fawId: fawId,
            },
          };

          return this.fileService.updateFile(filename, path, options).then((x) => driverFawCertificate);
        }

        return driverFawCertificate;
      });
    });
  }

  deleteFawCertificate(companyId: string, driverId: string, fawId: string): Promise<IDriver> {
    return this.dbContext.driver.findById(driverId).then((driver: IDriverModel) => {
      const driverFawCertificate = driver.fawHistory.find((item) => item._id.toString() == fawId);

      if (!driverFawCertificate) {
        throw ServerError(`FawCertificate ${fawId} not found for driver ${driverId}.`, codes.NOT_FOUND);
      }

      driver.fawHistory = driver.fawHistory.filter((item) => item !== driverFawCertificate);

      return driver.save();
    });
  }

  // Vehicle

  addVehicle(driverId: string, vehicleId: string): Promise<IDriver> {
    return this.driverVehicleMapRepository.get(driverId, vehicleId).then((map) => {
      if (map) {
        throw ServerError(
          `Can not add Vehicle ${vehicleId} to Driver ${driverId}. Vehicle already exists.`,
          codes.CONFLICT
        );
      }

      return this.driverVehicleMapRepository.add(driverId, vehicleId).then((map) => this.get(driverId));
    });
  }

  removeVehicle(driverId: string, vehicleId: string): Promise<IDriver> {
    return this.driverVehicleMapRepository.remove(driverId, vehicleId).then((_) => this.get(driverId));
  }
}
