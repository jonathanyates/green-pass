import logger = require('winston');
import container from '../../src/container/container.config';
import { TYPES } from '../../src/container/container.types';
import { SequencePromise } from '../../../shared/extensions/promise.extensions';
import { driversToResend } from './drivers-to-resend-emails';
import generator = require('generate-password');
import { IRepository } from '../../src/db/db.repository';
import { IUserModel } from '../../src/users/user.schema';
import { getUserLoginEmail } from '../../src/users/user.emails';
import { ICompanyService } from '../../src/companies/company.service.interface';
import { IEmail, IEmailService } from '../../src/email/email.service';
import serverConfig from '../../src/server/server.config';
import { configureLogger } from '../../src/services/logger';

const userRepository = container.get<IRepository<IUserModel>>(TYPES.repositories.UserRepository);
const companyService = container.get<ICompanyService>(TYPES.services.CompanyService);
const emailService = container.get<IEmailService>(TYPES.services.EmailService);

configureLogger('email', 'GreenPass-Email-Service');

Promise.all([
  SequencePromise.sequence(driversToResend, (email) => {
    return userRepository.findOne({ username: email }).then((user) => {
      const password = generator.generate({
        length: 10,
        numbers: true,
      });

      user.password = password;
      user.changePassword = true;

      return user.save().then((user) => {
        return companyService.get(user._companyId).then((company) => {
          const text = getUserLoginEmail(user, password, company);

          const email = <IEmail>{
            recipients: [user.email],
            subject: `Your ${company.name} Green Pass account details`,
            content: text,
          };

          logger.info('Sending user account email to ' + user.username);

          return emailService
            .send(email.recipients, email.subject, email.content, email.from, email.attachments)
            .then((sent) => {
              if (sent) {
                logger.info('Successfully sent user account email to ' + user.username);
              } else {
                logger.error('Failed to send user account email to ' + user.username);
              }
            })
            .catch((error) => {
              logger.error(`An error occured attempting to send user account email. \nError: ${error.message}`);
            });
        });
      });
    });
  }).then((finished) => {
    logger.info('Completed re-sending emails to users.');
    process.exit();
  }),
]);
