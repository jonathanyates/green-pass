import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {User} from "../ui/user/user.model";
import {IDriver} from "../../../../shared/interfaces/driver.interface";
import {IRMDeepLogin} from "../../../../server/src/assessments/roadmarque.model";
import {IOnRoadAssessment} from "../../../../shared/interfaces/assessment.interface";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AssessmentService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(
    httpClient: HttpClient,
    store: Store<AppState>) {
    super(httpClient, store);
  }

  roadMarqueLogin(driver: IDriver): Observable<string> {

    this.authorize();

    return this.httpClient.get<IRMDeepLogin>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/assessments/login`, this.getOptions())
      .map(res =>  {
        let login = this.handleResult<IRMDeepLogin>(res);
        if (login.success) {
          return login.loginUrl;
        } else {
          throw new Error('Login Failed.');
        }
      })
      .catch(this.handleError);

  }

  assessmentUpdate(driver: IDriver): Observable<IDriver> {

    this.authorize();

    return this.httpClient.get<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/assessments/update`, this.getOptions())
      .map(response => this.extractDriver(response))
      .catch(this.handleError);
  }

  saveOnRoadAssessment(driver: IDriver, onRoadAssessment: IOnRoadAssessment): Observable<IDriver> {

    this.authorize();

    let formData = new FormData();
    if (onRoadAssessment.inviteDate) {
      formData.append('inviteDate', onRoadAssessment.inviteDate.toString());
    }

    if (onRoadAssessment.assessmentDate) {
      formData.append('assessmentDate', onRoadAssessment.assessmentDate.toString());
    }

    formData.append('assessedBy', onRoadAssessment.assessedBy);
    formData.append('result', onRoadAssessment.result);

    if (onRoadAssessment.complete != null) {
      formData.append('complete', onRoadAssessment.complete.toString());
    }
    
    formData.append('fileType', onRoadAssessment.fileType);

    if (onRoadAssessment.file) {
      formData.append('file', onRoadAssessment.file);
    }

    if (onRoadAssessment._id) {
      return this.httpClient.put<IDriver>(this.companiesUrl + driver._companyId + '/drivers/' +
        driver._id + '/assessments/onroad/' + onRoadAssessment._id, formData, this.getOptions())
        .map(res => this.handleResult<IDriver>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IDriver>(this.companiesUrl + driver._companyId + '/drivers/' +
        driver._id + '/assessments/onroad/', formData, this.getOptions())
        .map(res => this.handleResult<IDriver>(res))
        .catch(this.handleError);
    }
  }

  private extractDriver(result: IDriver):IDriver {
    let driver = this.handleResult<IDriver>(result);
    driver.fullName = User.getFullName(driver._user);
    return driver;
  }

}
