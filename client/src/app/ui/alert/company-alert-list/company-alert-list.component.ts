import { Paths } from '../../../shared/constants';
import { AppState } from '../../../store/app.states';
import { SelectCompanies } from '../../../store/companies/companies.selectors';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {Company} from "../../company/company.model";
import {SelectAlertSummary} from "../../../store/alerts/alert.selectors";
import {IAlertSummary} from "../../../../../../shared/interfaces/alert.interface";
import {LoadAlertSummaryAction} from "../../../store/alerts/alert.actions";

@Component({
  template: `
    <gp-dynamic-list [list]="list" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class CompanyAlertListComponent implements OnInit, OnDestroy {

  companies: Company[];
  list: IDynamicList;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new LoadAlertSummaryAction());

    this.list = {
      columns: [
        { header: 'Name', field: 'name' },
        { header: 'Address', field: 'address' },
        { header: 'Driver Alerts', field: 'drivers'},
        { header: 'Vehicle Alerts', field: 'vehicles'}
      ]
    };

    this.subscription = this.store.let(SelectCompanies()).filter(companies => companies != null)
      .combineLatest(this.store.let(SelectAlertSummary()).filter(alertSummaries => alertSummaries != null),
        (companies, alertSummaries) => ({ companies: companies, alertSummaries: alertSummaries }))
      .subscribe(results => {
        this.list.items = results.companies.map(company => {
          let alertSummary = results.alertSummaries
            .find((summary: IAlertSummary) => summary.companyId === company._id);

          return {
            _id: company._id,
            name: company.name,
            address: company.address,
            drivers: alertSummary ? alertSummary.driverAlerts : 0,
            vehicles: alertSummary ? alertSummary.vehicleAlerts : 0,
          }
        })

      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  select(company) {
    if (company) {
      this.router.navigate([Paths.Company, company._id, Paths.Alerts]);
    }
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
