import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {IPage} from "../page.model";

@Component({
  selector: 'gp-dynamic-page',
  template: `
    <gp-dynamic-content [content]="content"></gp-dynamic-content>`,
  styleUrls: ['dynamic-page.component.css']
})
export class DynamicPageComponent implements OnInit, OnDestroy {

  content: string;
  private sub: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.data
      .subscribe((data: { page: IPage }) => {
        this.content = data.page ? data.page.content : null;
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
