import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {Observable} from "rxjs";
import {User} from "./user.model";
import {SelectSelectedUser} from "../../store/users/user.selectors";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {SelectError} from "../../store/error/error.selectors";
import {LoadUserAction, SelectUserAction} from "../../store/users/user.actions";
import {IUser} from "../../../../../shared/interfaces/user.interface";

@Injectable()
export class UserResolve implements Resolve<IUser|boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUser|boolean> {

    let companyId = route.params['id'];
    let userId = route.params['userId'];

    this.store.dispatch(new SelectCompanyAction(companyId));

    // edit
    if (!userId) {
      return Observable.of(new User(companyId));
    }

    this.store.dispatch(new LoadUserAction(companyId, userId));

    return this.store.let(SelectSelectedUser())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (users, error) => ({users: users, error: error}))
      .filter(result => result.error != null || result.users != null)
      .map(result => result.error ? false : result.users)
      .take(1);
  }
}
