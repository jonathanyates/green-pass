import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {Driver} from "../../ui/driver/driver.model";
import {LogoutAction} from "../authentication/authentication.actions";
import {IDocument} from "../../../../../shared/interfaces/document.interface";

export const ActionTypes = {
  LOAD:                     type('[Documents] Load'),
  LOAD_SUCCESS:             type('[Documents] Load Success'),
  SELECT_LIST:              type('[Documents] Select'),
  SELECT_LIST_SUCCESS:      type('[Documents] Select Success'),
  SELECT_DOCUMENT:          type('[Document] Select'),
  SELECT_DOCUMENT_SUCCESS:  type('[Document] Select Success'),
};

/**
 * Load Documents Actions
 */
export class LoadDocumentsAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload: Driver) { }
}

export class LoadDocumentsSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: IDocument[]) { }
}

/**
 * Select Documents Actions
 */
export class SelectDocumentsAction implements Action {
  type = ActionTypes.SELECT_LIST;
  constructor(public payload: Driver) { }
}

export class SelectDocumentsSuccessAction implements Action {
  type = ActionTypes.SELECT_LIST_SUCCESS;
  constructor(public payload: IDocument[]) { }
}

/**
 * Select Document Actions
 */
export class SelectDocumentAction implements Action {
  type = ActionTypes.SELECT_DOCUMENT;
  constructor(public payload: string) { } // documentId
}

export class SelectDocumentSuccessAction implements Action {
  type = ActionTypes.SELECT_DOCUMENT_SUCCESS;
  constructor(public payload: IDocument) { }
}

export type Actions
  = LoadDocumentsSuccessAction
  | SelectDocumentsSuccessAction
  | SelectDocumentSuccessAction
  | LogoutAction;
