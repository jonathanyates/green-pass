import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {IReportType} from "../../../../../../shared/interfaces/report.interface";
import {FormGroup} from "@angular/forms";
import {ReportTypeInputMapper} from "./report-type-input.mapper";
import {SaveReportTypeAction} from "../../../store/reports/report.actions";
import {SelectReportTypesAction} from "../../../store/reports/report.actions";
import {SelectReportTypes} from "../../../store/reports/report.selectors";

@Component({
  selector: 'gp-reportType-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class ReportTypeInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private reportType: IReportType;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {

    this.store.dispatch(new SelectReportTypesAction());
    this.store.dispatch(new SelectReportTypesAction());

    this.subscription =
      this.route.params.map((params: Params) => params['reportTypeId'])
        .combineLatest(
          this.store.let(SelectReportTypes()).filter(x => x != null)
        )
      .subscribe(results => {
        let reportTypeId = results[0];
        let reportTypes = results[1];

        this.reportType = reportTypeId
         ? reportTypes.find(reportType => reportType._id === reportTypeId)
         : <IReportType>{ name: '', category: '', query: '' };

        this.inputGroups = ReportTypeInputMapper.mapTo(this.reportType);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    if (!form.controls) return;
    let reportType = Object.assign({}, this.reportType,
      ReportTypeInputMapper.mapFrom(form));
    this.store.dispatch(new SaveReportTypeAction(reportType));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
