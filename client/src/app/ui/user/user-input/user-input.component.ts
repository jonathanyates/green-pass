import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {SaveUserAction} from "../../../store/users/user.actions";
import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {UserInputMapper} from "./user-input.mapper";
import {FormGroup} from "@angular/forms";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {User} from "../user.model";

@Component({
  selector: 'gp-user-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class UserInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private user: IUser;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.route.data
      .map((data: { user: IUser }) => data.user)
      .subscribe(user => {
        this.user = user;
        this.inputGroups = UserInputMapper.mapTo(user);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let user = Object.assign(new User(), this.user, UserInputMapper.mapFrom(form));
    this.store.dispatch(new SaveUserAction(user));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
