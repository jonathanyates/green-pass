import {inject, injectable} from "inversify";
import {IReportQueryResult, ReportCategory} from "../../../shared/interfaces/report.interface";
import {IDynamicList} from "../../../shared/models/dynamic-list.model";
import {DriverListBuilder} from "../../../shared/models/driver-list.builder";
import {VehicleListBuilder} from "../../../shared/models/vehicle-list-builder";
import {IVehicle} from "../../../shared/interfaces/vehicle.interface";
import {ICompanyService} from "../companies/company.service.interface";
import TYPES from "../container/container.types";
import {IDriver} from "../../../shared/interfaces/driver.interface";
import {SubscriptionHelper} from "../../../shared/models/subscription.model";
import {ISubscription} from "../../../shared/interfaces/subscription.interface";
import {toProperCase} from "../../../shared/utils/helpers";

export interface IReportTransformService {
  transform(reportQueryResult: IReportQueryResult): Promise<IDynamicList>;
}

@injectable()
export class ReportTransformService implements IReportTransformService {

  constructor(
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService
  ) {}

  transform(reportQueryResult: IReportQueryResult): Promise<IDynamicList> {
    if (reportQueryResult.reportType.category === ReportCategory.Driver) {
      return this.transformDriversReport(reportQueryResult);
    } else if (reportQueryResult.reportType.category === ReportCategory.Vehicle) {
      return this.transformVehiclesReport(reportQueryResult);
    } else {
      return Promise.resolve(ReportBuilder.getList(reportQueryResult.result));
    }
  }

  transformDriversReport(reportQueryResult: IReportQueryResult): Promise<IDynamicList> {
    const drivers: IDriver[] = reportQueryResult.result;
    if (drivers == null || drivers.length === 0)
      return Promise.resolve({
        columns: [],
        items: []
      });
    const companyId = drivers[0]._companyId;

    return this.companyService.get(companyId)
      .then(company => {
        const subscription = SubscriptionHelper.getSubscription(company);
        return DriverListBuilder.getList(reportQueryResult.result, subscription, company.settings);
      });
  }

  transformVehiclesReport(reportQueryResult: IReportQueryResult): Promise<IDynamicList> {
    const vehicles: IVehicle[] = reportQueryResult.result;
    if (vehicles == null || vehicles.length === 0)
      return Promise.resolve({
        columns: [],
        items: []
      });

    const companyId = vehicles[0]._companyId;

    return this.companyService.get(companyId)
      .then(company => {
        return VehicleListBuilder.getList(reportQueryResult.result, company.settings);
      });
  }
}

class ReportBuilder {
  static getList(report: any[]) {

    if (!report || report.length === 0) {
      return {
        columns: [],
        items: []
      };
    }

    return {
      columns: ReportBuilder.getColumns(report),
      items: report
    };
  }

  private static getColumns(report: any[]) {

    if (!report || report.length === 0) {
      return [];
    }

    const fields = Object.getOwnPropertyNames(report[0]);
    if (!fields) {
      return [];
    }

    return fields.map(field => ({
      header: toProperCase(field),
      field: field
    }));
  }
}