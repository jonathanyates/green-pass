import {injectable, inject} from "inversify";
import {TYPES} from "../container/container.types";
import {Request, Response, NextFunction} from "express-serve-static-core";
import {IAddressService} from "./address.service";
import codes from "../shared/error.codes";

export interface IAddressController {
  addressSearch(req:Request, res:Response, next: NextFunction);
}

@injectable()
export class AddressController implements IAddressController {

  constructor(
    @inject(TYPES.services.AddressService) private addressService: IAddressService
  ) {}

  addressSearch = (req: Request, res: Response, next: NextFunction) => {

    const postcode = req.params.postcode;
    const house = req.params.house;

    if (!postcode) {
      return res.status(codes.BAD_REQUEST).send({message: 'No postcode specified.'});
    }

    this.addressService.addressSearch(postcode, house)
      .then(addresses => res.status(codes.OK).json(addresses))
      .catch(err => next(err));
  }
}