import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {IContact, IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {DriverReferenceInputMapper} from "./driver-reference-input.mapper";
import {Contact} from "../../driver.model";
import {SelectDriversState} from "../../../../store/drivers/driver.selectors";
import {SaveDriverReferenceAction} from "../../../../store/drivers/driver.actions";

@Component({
  selector: 'gp-reference-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
  `
})
export class DriverReferenceInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private driver: IDriver;
  private reference: IContact;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private referenceInputMapper: DriverReferenceInputMapper) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectDriversState()).take(1)
      .subscribe(state => {
        this.driver = state.selected;
        this.reference = state.selectedReference ? state.selectedReference : new Contact();
        this.inputGroups = this.referenceInputMapper.mapTo(this.reference);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let reference = Object.assign(new Contact(), this.reference, this.referenceInputMapper.mapFrom(form));
    this.store.dispatch(new SaveDriverReferenceAction(reference));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
