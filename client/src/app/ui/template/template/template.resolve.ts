import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {
  LoadTemplatesAction,
  SelectTemplateAction, SelectTemplateSuccessAction
} from "../../../store/templates/template.actions";

@Injectable()
export class TemplateResolve {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let templateId = route.params['templateId'];

    this.store.dispatch(new LoadTemplatesAction());

    if (templateId) {
      this.store.dispatch(new SelectTemplateAction(templateId));
    } else {
      this.store.dispatch(new SelectTemplateSuccessAction({
        name: '',
        html: '',
        selected: true
      }));
    }

    return true;
  }
}
