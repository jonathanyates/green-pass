import logger = require('winston');
import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { IRepository } from '../db/db.repository';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { ITemplate } from '../../../shared/interfaces/document.interface';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class TemplateRepository implements IRepository<ITemplate> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  async getAll(): Promise<ITemplate[]> {
    const result = await this.dbContext.template.find({}).lean().exec();
    return result;
  }

  get(id: string): Promise<ITemplate> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid template id.', codes.BAD_REQUEST));
    }

    return <Promise<ITemplate>>this.dbContext.template.findById(id).lean().exec();
  }

  find(criteria: any): Promise<ITemplate[]> {
    return this.dbContext.template.find(criteria).exec();
  }

  findOne(criteria: any): Promise<ITemplate> {
    return this.dbContext.template.findOne(criteria).exec();
  }

  add<T extends ITemplate>(template: ITemplate): Promise<ITemplate> {
    const model = new this.dbContext.template(template);
    return model.save().then((template) => {
      logger.info(`Template ${template._id} added.`);
      return template;
    });
  }

  update<T extends ITemplate>(template: ITemplate): Promise<ITemplate> {
    return this.dbContext.template
      .findByIdAndUpdate(template._id, template, { new: true })
      .exec()
      .then((template) => {
        logger.info(`Template ${template._id} updated.`);
        return template;
      });
  }

  remove<T extends ITemplate>(templateId: string): Promise<ITemplate> {
    return this.dbContext.template
      .findByIdAndRemove(templateId)
      .exec()
      .then((template) => {
        logger.info(`Template ${templateId} removed.`);
        return template;
      });
  }
}
