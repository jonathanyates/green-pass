import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {Subscription, Observable} from "rxjs";
import {SelectIsAdmin} from "../../../../store/authentication/authentication.selectors";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";

@Component({
  selector: 'gp-card',
  template: `
    <div class="card">
      <h3 class="card-header white-text"
          [ngClass]="{
            'danger-color-dark': cardColor === 'danger-color-dark', 
            'warning-color-dark': cardColor === 'warning-color-dark',
            'primary-color': cardColor == null || cardColor == 'primary-color'
        }">{{header}}
        <a *ngIf="canEdit" class="pull-right">
          <i class="fa fa-pencil" (click)="onEdit()"></i>
        </a>
        <a *ngIf="canAdd" class="pull-right">
          <i class="fa fa-plus" (click)="onAdd()"></i>
        </a>
      </h3>
      <div class="card-block">
        <ng-content></ng-content>
      </div>

      <div *ngIf="footer" class="card-footer">{{footer}}</div>
      
    </div>
  `
})
export class CardComponent implements OnInit {

  canEdit: boolean;
  canAdd: boolean;
  checkAdmin: boolean = true;

  private _allowEdit: boolean;
  @Input() set allowEdit(allowEdit: boolean) {
    this.checkAdmin = false;
    this._allowEdit = allowEdit;
  }
  get allowEdit() {
    return this._allowEdit;
  }

  @Input() header: string;
  @Input() footer: string;

  _cardColor: string;
  @Input() set cardColor(cardColor: string) {
   this._cardColor = cardColor
  }
  get cardColor(): string {
    return this._cardColor;
  }

  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() add: EventEmitter<any> = new EventEmitter<any>();

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    if (this.checkAdmin) {
      this.store.let(SelectIsAdmin())
        .subscribe(isAdmin => {
          this.canEdit = this.allowEdit || (isAdmin === true && this.edit.observers.length > 0);
          this.canAdd = isAdmin && this.add.observers.length > 0;
        })
    } else {
      this.canEdit = this.allowEdit;
    }
  }

  onEdit() {
    this.edit.emit();
  }

  onAdd() {
    this.add.emit();
  }
}
