import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-licence-check-card',
  templateUrl: 'driver-licence-check-card.component.html',
  host: {'class': 'card'}
})
export class DriverLicenceCheckCardComponent {

  constructor(private router: Router) {}

  licenceCheck() {
    this.router.navigate([Paths.Driver, Paths.LicenceCheck]);
  }

}
