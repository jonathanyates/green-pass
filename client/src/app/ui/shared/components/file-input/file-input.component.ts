import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FileService} from "../../../../services/file.service";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";
import {BusyAction} from "../../../../store/progress/progress.actions";
import {FormGroup} from "@angular/forms";
import {FormInput} from "../dynamic-form/dynamic-form.model";
import {FileTypes} from "../../../../shared/constants";

@Component({
  selector: 'gp-file-input',
  template: `
    <label *ngIf="input.label" style="position: inherit !important;">{{input.label}}</label>

    <div [formGroup]="form">
        <input type='file' accept="image/*,application/pdf" [id]="input.id" [formControlName]="input.id" 
               (change)="onFileSelected($event)">
    </div>
    
    <div *ngIf="message">{{message}}</div>
  `
})
export class FileInputComponent {

  message: string;
  @Input() form: FormGroup;
  @Input() input: FormInput;
  @Output() fileSelected: EventEmitter<File> = new EventEmitter<File>();

  constructor(private store: Store<AppState>, private fileService: FileService) {}

  onFileSelected(event: any) {

    this.message = null;
    let file = event.target.files && event.target.files.length > 0
      ? event.target.files[0]
      : null;

    if (file == null) {
      return;
    }

    let fileType = file.name && file.name.toLowerCase().endsWith('.pdf')
      ? FileTypes.pdf
      : FileTypes.image;

    if (fileType == FileTypes.pdf) {
      file.fileType = fileType;
      this.fileSelected.emit(file);
    } else {
      this.message = 'Resizing and compressing file...';

      this.store.dispatch(new BusyAction());

      this.fileService.compressFile(file)
        .finally(() => this.store.dispatch(new BusyAction(false)))
        .subscribe((file:any) => {
          this.store.dispatch(new BusyAction(false));
          this.message = null;
          file.fileType = fileType;
          this.fileSelected.emit(file);
        }, err => {
          this.store.dispatch(new BusyAction(false));
          this.message = err.message;
        });
    }


  }

}
