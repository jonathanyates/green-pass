import { result } from 'lodash';
import 'reflect-metadata';
import { IVehicle } from '../../../shared/interfaces/vehicle.interface';
import { DvlaService } from '../../src/dvla/service/dvla.service';
import { motTestMapper } from '../../src/dvla/service/motEnquiryMapper';
import { is3YearsOld } from '../../src/vehicles/vehicle.utils';

const registrationNumber = 'E18EBY';
const vehicleMake = 'TESLA';

const dvlaService = new DvlaService();

dvlaService
  .motEnquiry(registrationNumber)
  .then((enquiry) => {
    console.log('MOT Check Results:', enquiry);

    const vehicle: IVehicle = {} as IVehicle;

    vehicle.model = enquiry.model;
    vehicle.dateOfFirstRegistration = enquiry.registrationDate ? new Date(enquiry.registrationDate) : null;
    vehicle.manufactureDate = enquiry.manufactureDate ? new Date(enquiry.manufactureDate) : null;
    vehicle.firstUsedDate = enquiry.firstUsedDate ? new Date(enquiry.firstUsedDate) : null;

    if (enquiry && enquiry.motTests.length > 0) {
      vehicle.motHistory = motTestMapper(enquiry.motTests);
      vehicle.annualMileage = getAnnualMileage(vehicle);

      const latestMotTest = vehicle.motHistory.find((item) => item.hasOwnProperty('expiryDate'));
      vehicle.motDueDate = latestMotTest && latestMotTest.expiryDate;
    }

    // if the vehicle is less than 3 years old then set the mot due date to 3 years from vehicle registration
    if (vehicle.dateOfFirstRegistration) {
      const isVehicle3YearsOld = is3YearsOld(vehicle.dateOfFirstRegistration);
      if (!isVehicle3YearsOld) {
        vehicle.motDueDate = new Date(
          vehicle.dateOfFirstRegistration.getFullYear() + 3,
          vehicle.dateOfFirstRegistration.getMonth(),
          vehicle.dateOfFirstRegistration.getDate()
        );
      }
    }

    return result;
  })
  .catch((err) => {
    console.log(err);
  });

const getAnnualMileage = (vehicle: IVehicle): number => {
  // can only use mot history if we have at least 2 mots
  if (!vehicle.motHistory || vehicle.motHistory.length < 2) {
    return vehicle.annualMileage;
  }

  const mots = vehicle.motHistory
    .filter((mot) => mot.testResult === 'Pass')
    .sort((a, b) => b.testDate.getTime() - a.testDate.getTime());

  if (mots.length < 2) {
    return vehicle.annualMileage;
  }

  const latestMileage = mots[0].odometerReading;
  const previousMileage = mots[1].odometerReading;
  let mileage = latestMileage - previousMileage;

  // ensure the mileage is for 12 months
  const months = mots[1].testDate.getMonthDiff(mots[0].testDate);
  const monthlyMileage = mileage / months;
  mileage = Math.round(monthlyMileage * 12);

  return mileage;
};
