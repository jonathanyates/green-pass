import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IContact, IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-reference-list',
  template: `<gp-dynamic-list [list]="list" (add)="onAdd()" (itemSelected)="onSelect($event)" 
                              [lightHeader]="lightHeader" [allowAdd]="allowAdd"></gp-dynamic-list>`
})
export class DriverReferenceListComponent {

  list: IDynamicList;
  private _references: IContact[];

  @Input() lightHeader: boolean = false;
  @Input() allowAdd: boolean;
  @Input() driver: IDriver;
  @Input() user: IUser;
  @Input()
  set references(references: IContact[]) {
    this._references = references;
    this.list = {
      columns: [
        { header: 'Name', field: 'name' },
        { header: 'Address', field: 'address', style: 'hidden-xs-down' },
        { header: 'Phone', field: 'phone', style: 'hidden-sm-down' },
        { header: 'Mobile', field: 'mobile', style: 'hidden-sm-down' },
        { header: 'Email', field: 'email', style: 'hidden-sm-down' },
      ]
    };

    if (this.references) {
      this.list.items = references.map(reference => Object.assign({}, reference, {
        name: reference.forename + ' ' + reference.surname
      }))
    }
  }
  get references() { return this._references; }

  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() select: EventEmitter<IContact> = new EventEmitter<IContact>();

  onAdd() {
    this.add.emit();
  }

  onSelect(contact: IContact) {
    this.select.emit(contact);
  }
}
