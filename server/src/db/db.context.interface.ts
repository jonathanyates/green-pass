import { Model } from 'mongoose';
import { Grid } from 'gridfs-stream';
import { IVehicleModel } from '../vehicles/vehicle.schema';
import { ICompanyModel } from '../companies/company.schema';
import { IUserModel } from '../users/user.schema';
import { IDriverModel, IDriverVehicleMapModel } from '../drivers/driver.schema';
import { IAddressModel } from '../address/address.schema';
import { IComplianceModel } from '../compliance/compliance.schema';
import { IAssessmentModel } from '../assessments/assessment.schema';
import { IAuditModel } from '../audit/audit.schema';
import { IAlertModel } from '../alerts/alert.schema';
import { IReportTypeModel } from '../reports/report-type.schema';
import { IAlertTypeModel } from '../alerts/alert-type.schema';
import { IReportScheduleModel } from '../reports/report-schedule.schema';
import { ISubscriptionModel } from '../subscriptions/subscription.schema';
import { ITemplateModel } from '../templates/template.schema';
import { GridFSBucket } from 'mongodb';

export interface IDbContext {
  connect: () => void;
  disconnect: () => void;

  gridFs: Grid;
  gridFSBucket: GridFSBucket;

  company: Model<ICompanyModel>;
  user: Model<IUserModel>;
  driver: Model<IDriverModel>;
  vehicle: Model<IVehicleModel>;
  driverVehicleMap: Model<IDriverVehicleMapModel>;
  address: Model<IAddressModel>;
  compliance: Model<IComplianceModel>;
  assessment: Model<IAssessmentModel>;
  audit: Model<IAuditModel>;

  template: Model<ITemplateModel>;

  alertType: Model<IAlertTypeModel>;
  alert: Model<IAlertModel>;

  reportType: Model<IReportTypeModel>;
  reportSchedule: Model<IReportScheduleModel>;

  subscription: Model<ISubscriptionModel>;
}
