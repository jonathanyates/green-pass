export const environment = {
  production: true,
  apiRoot: 'https://app.greenpasscompliance.co.uk',
  wwwRoot: 'https://greenpasscompliance.co.uk',
  legalesign: {
    username: '836e21d5-4cb2-4627-a67d-1254d15735ea',
    group: '9b659a47-8146-11ed-955f-06d903d83b8a',
    path: `https://app.greenpasscompliance.co.uk/driver/documents/`,
  },
};
