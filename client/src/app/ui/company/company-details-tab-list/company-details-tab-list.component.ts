import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {IInsurance} from "../../../../../../shared/interfaces/common.interface";
import {Paths} from "../../../shared/constants";
import {SelectCompany, SelectSelectedCompanyDetailsTab} from "../../../store/company/company.selectors";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {SelectCompanyDetailsTabAction} from "../../../store/company/company.actions";
import {SelectSelectedCompliance} from "../../../store/compliance/compliance.selectors";
import {SelectIsGreenPassAdmin} from "../../../store/authentication/authentication.selectors";

@Component({
  selector: 'gp-company-details-tab-list',
  templateUrl: 'company-details-tab-list.component.html'
})
export class CompanyDetailsTabListComponent implements OnInit, OnDestroy {

  company: ICompany;
  selectedTab: string;
  showInsurance: boolean;
  showSettings: boolean;
  showSubscription: boolean;

  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => {
        this.company = company;
      }));

    this.subscriptions.push(this.store.let(SelectIsGreenPassAdmin())
      .subscribe(isAdmin => {
        this.showSettings = isAdmin;
        this.showSubscription = isAdmin;
      }));

    // show insurance if insurance compliance is not null (i.e. required)
    this.subscriptions.push(this.store.let(SelectSelectedCompliance())
      .subscribe(compliance => {
        this.showInsurance = compliance != null && compliance.company != null && compliance.company.insurance != null;
      }));

    this.subscriptions.push(this.store.let(SelectSelectedCompanyDetailsTab())
      .subscribe(tab => this.selectedTab = tab));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  selectTab(tab: string) {
    this.store.dispatch(new SelectCompanyDetailsTabAction(tab));
  }

  addInsurance() {
    this.router.navigate(['../', Paths.Insurance, Paths.New], { relativeTo: this.route });
  }

  selectInsurance(insurance: IInsurance) {
    this.router.navigate(['../', Paths.Insurance, insurance._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
