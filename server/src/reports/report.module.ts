import {ContainerModule, interfaces} from "inversify";
import {IQueryableRepository,} from "../db/db.repository";
import {IReportTypeModel} from "./report-type.schema";
import {TYPES} from "../container/container.types";
import {ReportTypeRepository} from "./report-type.repository";
import {ReportTypeService, IReportTypeService} from "./report-type.service";
import {ReportRouter} from "./report.router";
import {IApiRouter} from "../interfaces/api-router.interface";
import {IReportTypeController, ReportTypeController} from "./report-type.controller";
import {ReportScheduleService} from "./report-schedule.service";
import {IReportSchedule} from "../../../shared/interfaces/report.interface";
import {IApiService} from "../interfaces/api-service.interface";
import {ReportScheduleRepository} from "./report-schedule.repository";
import {IReportController, ReportController} from "./report.controller";
import {IReportQueryService, ReportQueryService} from "./report-query.service";
import {ReportScheduleController} from "./report-schedule.controller";
import {IApiController} from "../interfaces/api-controller.interface";
import {ReportTypeRouter} from "./report-type.router";
import {ReportScheduleRouter} from "./report-schedule.router";
import {IReportScheduleModel} from "./report-schedule.schema";
import {IReportGeneratorService, ReportGeneratorService} from "./report-generator.service";
import {IReportScheduler, ReportScheduler} from "./report.scheduler";
import {IReportService, ReportService} from "./report.service";
import {IReportTransformService, ReportTransformService} from "./report-transform.service";

export const reportModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.ReportRouter).to(ReportRouter).inSingletonScope();
  bind<IReportController>(TYPES.controllers.ReportController).to(ReportController).inSingletonScope();
  bind<IReportQueryService>(TYPES.services.ReportQueryService).to(ReportQueryService).inSingletonScope();

  bind<IApiRouter>(TYPES.routers.ReportTypeRouter).to(ReportTypeRouter).inSingletonScope();
  bind<IReportTypeController>(TYPES.controllers.ReportTypeController).to(ReportTypeController).inSingletonScope();
  bind<IReportTypeService>(TYPES.services.ReportTypeService).to(ReportTypeService).inSingletonScope();
  bind<IQueryableRepository<IReportTypeModel>>(TYPES.repositories.ReportTypeRepository).to(ReportTypeRepository).inSingletonScope();

  bind<IApiRouter>(TYPES.routers.ReportScheduleRouter).to(ReportScheduleRouter).inSingletonScope();
  bind<IApiController>(TYPES.controllers.ReportScheduleController).to(ReportScheduleController).inSingletonScope();
  bind<IApiService<IReportSchedule>>(TYPES.services.ReportScheduleService).to(ReportScheduleService).inSingletonScope();
  bind<IQueryableRepository<IReportScheduleModel>>(TYPES.repositories.ReportScheduleRepository).to(ReportScheduleRepository).inSingletonScope();

  bind<IReportService>(TYPES.services.ReportService).to(ReportService).inSingletonScope();
  bind<IReportTransformService>(TYPES.services.ReportTransformService).to(ReportTransformService).inSingletonScope();
  bind<IReportGeneratorService>(TYPES.services.ReportGeneratorService).to(ReportGeneratorService).inSingletonScope();
  bind<IReportScheduler>(TYPES.services.ReportScheduler).to(ReportScheduler).inSingletonScope();
});