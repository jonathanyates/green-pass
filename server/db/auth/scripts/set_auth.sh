#!/bin/bash
# Taken from here (This isn't my filth)
# http://blog.bejanalex.com/2017/03/running-mongodb-in-a-docker-container-with-authentication/

# Wait for MongoDB to boot
RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MongoDB service startup..."
    sleep 1
    mongo admin --eval "help" >/dev/null 2>&1
    RET=$?
done

# Create the admin user
echo "=> Creating admin user with a password in MongoDB"

mongo admin --eval "db.createUser(
    {
        user: '$MONGODB_ADMIN_USER',
        pwd: '$MONGODB_ADMIN_PASS',
        roles:[{role:'root',db:'admin'}]
});"

sleep 1

echo "=> Creating an ${MONGODB_APPLICATION_DATABASE} user with a password in MongoDB"

mongo admin -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS << EOF
use $MONGODB_APPLICATION_DATABASE
db.createUser(
    {
        user: '$MONGODB_APPLICATION_USER',
        pwd: '$MONGODB_APPLICATION_PASS',
        roles:[{role:'dbOwner', db:'$MONGODB_APPLICATION_DATABASE'}]
    })
EOF

sleep 1

# If everything went well, add a file as a flag so we know in the future to not re-create the
# users if we're recreating the container (provided we're using some persistent storage)
echo "=> Done!"
touch /data/db/.mongodb_password_set
