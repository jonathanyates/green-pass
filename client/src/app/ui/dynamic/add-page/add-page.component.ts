import {Component, OnInit, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {DialogComponent} from "../../shared/components/dialog/dialog.component";
import {PageService} from "../../../services/page.service";
import {Page} from "../page.model";
import {Router} from "@angular/router";
import {Paths} from "../../../shared/constants";

declare let $: any;

@Component({
  selector: 'gp-add-page',
  templateUrl: 'add-page.component.html'
})
export class AddPageComponent extends DialogComponent implements OnInit, AfterViewInit {

  // so we can expose this as the element to focus on
  @ViewChild('pageId') focusElement: ElementRef;

  pageId:string;
  errorMessage:string;
  self:AddPageComponent;

  constructor(
    private pageService:PageService,
    private router:Router) {
    super();
    this.self = this;
  }

  ngOnInit() {
    this.active = true;
  }

  ngAfterViewInit(): void {
    if (this.focusElement) {
      this.focusElement.nativeElement.focus();
    }
  }

  add() {
    this.errorMessage = null;
    let page = new Page();
    page.pageId = this.pageId;
    page.content = '<h1>' + this.pageId + '</h1>';
    this.pageService.savePage(page)
      .subscribe(page => {
        this.close();
        if (page) {
          this.router.navigate([Paths.Root, page.pageId]);
        }

      }, err => {
        this.errorMessage = err;
      });

  }

  close() {
    $('#modal-addPage').modal('hide');
    this.reset();
  }

}
