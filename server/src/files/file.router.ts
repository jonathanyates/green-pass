import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IFileController } from './file.controller';

@injectable()
export class FileRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.FileController) private fileController: IFileController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes.get(
      '/:filename',
      this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
      this.fileController.get
    );
  }
}
