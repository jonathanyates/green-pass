import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {SelectLoggedIn} from "../../store/authentication/authentication.selectors";
import {Router, NavigationEnd} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit {

  showFooter: boolean;
  loggedIn: boolean;

  constructor(
    private store: Store<AppState>,
    private router: Router) { }

  ngOnInit(): void {

    this.store.let(SelectLoggedIn())
      .combineLatest(this.router.events.filter(event => event instanceof NavigationEnd)
        .map((events: NavigationEnd) => events.url),
      (loggedIn, url) => ({loggedIn: loggedIn, url: url}))
      .subscribe(result => {
        let noFooterUrls = ['/login', '/forgotPassword', '/resetPassword', '/changePassword', '/error', '/unauthorized'];
        this.showFooter = !result.loggedIn && noFooterUrls.find(url => result.url.startsWith(url)) == null;
        this.loggedIn = result.loggedIn;
      })
  }
}
