import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {AuthenticationRouter} from "./router/authentication.router";
import {AuthenticationController} from "./controller/authentication.controller";
import {IAuthenticationController} from "./controller/authentication.controller.interface";
import {AuthenticationMiddleware} from "./authentication/authentication.middleware";
import {IAuthenticationMiddleware} from "./authentication/authentication.middleware.interface";
import {IAuthorizationMiddleware} from "./authorization/authorization.middleware.interface";
import {AuthorizationMiddleware} from "./authorization/authorization.middleware";
import {AuthenticationService, IAuthenticationService} from "./authentication/authentication.service";

export const securityModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IAuthenticationMiddleware>(TYPES.security.AuthenticationMiddleware).to(AuthenticationMiddleware).inSingletonScope();
  bind<IAuthorizationMiddleware>(TYPES.security.AuthorizationMiddleware).to(AuthorizationMiddleware).inSingletonScope();
  bind<IApiRouter>(TYPES.routers.AuthenticationRouter).to(AuthenticationRouter).inSingletonScope();
  bind<IAuthenticationService>(TYPES.services.AuthenticationService).to(AuthenticationService).inSingletonScope();
  bind<IAuthenticationController>(TYPES.controllers.AuthenticationController).to(AuthenticationController).inSingletonScope();
});