import mongoose = require('mongoose');
import 'reflect-metadata';
import '../../shared/extensions/date.extensions';
import { TYPES } from '../src/container/container.types';
import { driverInfo } from './drivers';
import { IDriverService } from '../src/drivers/driver.service';
import { IAddressService } from '../src/address/address.service';
import { IDriver } from '../../shared/interfaces/driver.interface';
import '../../shared/extensions/promise.extensions';
import container from '../src/container/container.config';
import { ICompanyRepository } from '../src/companies/company.repository';
import { ICompany } from '../../shared/interfaces/company.interface';

const companyRepository = container.get<ICompanyRepository>(TYPES.repositories.CompanyRepository);
const driverService = container.get<IDriverService>(TYPES.services.DriverService);
// const addressService = container.get<IAddressService>(TYPES.services.AddressService);

const addDriver = async (info, company: ICompany): Promise<IDriver> => {
  try {
    const driver: IDriver = {
      _id: info._id,
      _user: {
        title: info.title,
        forename: info.forename,
        surname: info.surname,
        email: info.email ? info.email : null,
        role: 'driver',
        username: null,
        password: null,
      },
      gender: null,
      licenceNumber: null,
      niNumber: null,
      phone: null,
      mobile: null,
      address: null,
      dateOfBirth: null,
      licence: {
        checks: [],
      },
      assessments: [],
    };

    const newDriver = await driverService.add(company._id, driver);
    return newDriver;
  } catch (error) {
    const driver = info;
    console.error('Error driver:');
    console.error(driver);
    console.error(error);
    return null;
  }
};

const addDrivers = async () => {
  try {
    const company = await companyRepository.findOne({ name: 'Green Pass Compliance Ltd' });
    const drivers = await Promise.all(driverInfo.map((info) => addDriver(info, company)));
    console.log(`${drivers.length} Drivers created successfully.`);
    process.exit();
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

addDrivers();
