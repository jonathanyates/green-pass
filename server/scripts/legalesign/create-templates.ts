import 'reflect-metadata';
import path = require('path');
import { TYPES } from '../../src/container/container.types';
import { IDocumentSignatureService } from '../../src/documents/interfaces/signature.service.interface';
import container from '../../src/container/container.config';
import fs from 'fs';
import { ITemplate } from '../../../shared/interfaces/document.interface';
import { getFilesFromDir } from '../../src/shared/file.utils';
import { IRepository } from '../../src/db/db.repository';

const filePath = path.normalize(__dirname + '/../../../../../docs/esignature-documents');

console.log(`Creating templates from ${filePath} ...`);

const signatureService = container.get<IDocumentSignatureService>(TYPES.services.DocumentSignatureService);
const templateRepository = container.get<IRepository<ITemplate>>(TYPES.repositories.TemplateRepository);

const createTemplates = (directory: string) => {
  let files = getFilesFromDir(directory, ['.html']);

  return signatureService.getTemplates().then((templates: ITemplate[]) => {
    if (templates) {
      files = files.filter((file) => {
        const parts = file.split('/');
        const filename = parts.pop();
        const name = filename.replace('.html', '');
        const exists = templates.some((template) => template.name === name);
        return !exists;
      });
    }
    return Promise.all(files.map((file) => createTemplate(directory + file)));
  });
};

const createTemplate = (filePath) => {
  try {
    let fileBytes = null;
    try {
      // read file from a local directory
      fileBytes = fs.readFileSync(filePath);
    } catch (ex) {
      // handle error
      console.log('Exception: ' + ex);
    }

    filePath = filePath.replace(/\\/g, '/');
    const parts = filePath.split('/');
    const filename = parts.pop();
    const name = filename.replace('.html', '');
    const folder = parts.pop();
    const template = <ITemplate>{
      name: name,
      html: fileBytes,
      folderName: folder,
    };

    return templateRepository.add(template);
  } catch (ex) {
    console.log('Exception: ' + ex);
    return;
  }
};

createTemplates(filePath)
  .then((templates) => {
    console.log(`Created ${templates.length} templates successfully.`);
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
