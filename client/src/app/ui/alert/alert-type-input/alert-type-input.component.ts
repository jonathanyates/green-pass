import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {IAlertType} from "../../../../../../shared/interfaces/alert.interface";
import {AlertTypeInputMapper} from "./alert-type-input.mapper";
import {SaveAlertTypeAction, SelectAlertTypesAction} from "../../../store/alerts/alert.actions";
import {SelectAlertTypes} from "../../../store/alerts/alert.selectors";
import {SelectReportTypes} from "../../../store/reports/report.selectors";
import {IReportType} from "../../../../../../shared/interfaces/report.interface";
import {SelectReportTypesAction} from "../../../store/reports/report.actions";

@Component({
  selector: 'gp-alertType-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class AlertTypeInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private alertType: IAlertType;
  private reportTypes: IReportType[];
  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new SelectAlertTypesAction());
    this.store.dispatch(new SelectReportTypesAction());

    this.subscription =
      this.route.params.map((params: Params) => params['alertTypeId'])
        .combineLatest(
          this.store.let(SelectAlertTypes()).filter(x => x != null),
          this.store.let(SelectReportTypes()).filter(x => x != null)
        )
      .subscribe(results => {
        let alertTypeId = results[0];
        let alertTypes = results[1];
        this.reportTypes = results[2];

        this.alertType = alertTypeId
         ? alertTypes.find(alertType => alertType._id === alertTypeId)
         : <IAlertType> {
            reportType: null,
            priority: null
          };
        this.inputGroups = AlertTypeInputMapper.mapTo(this.alertType, this.reportTypes);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    if (!form.controls) return;
    let alertType = Object.assign({}, this.alertType,
      AlertTypeInputMapper.mapFrom(form, this.reportTypes));
    this.store.dispatch(new SaveAlertTypeAction(alertType));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
