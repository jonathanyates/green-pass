import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompany} from "../../../store/company/company.selectors";
import {IAlertTypeSummary} from "../../../../../../shared/interfaces/alert.interface";
import {AlertService} from "../../../services/alert.service";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";

@Component({
  selector: 'gp-alert-list',
  template: `
    <gp-dynamic-list [list]="list" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class AlertListComponent implements OnInit, OnDestroy {

  list: IDynamicList;
  alerts: IAlertTypeSummary[];
  private subscriptions = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService

  ) { }

  ngOnInit() {

    this.list = {
      columns: [
        { header: 'Name', field: 'key' },
        { header: 'Category', field: 'category' },
        { header: 'No of alerts', field: 'count' },
        { header: 'Priority', field: 'priority'}
      ]
    };

    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .switchMap(company => this.alertService.getTypeSummary(company._id))
      .subscribe(alerts => {
        this.alerts = alerts;
        this.list.items = alerts;
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  select(alertSummary: IAlertTypeSummary) {
    if (!alertSummary) return;
    let alertPath = alertSummary.category.toLowerCase() + 's';
    return this.router.navigate(['./', alertPath, alertSummary.alertType._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
