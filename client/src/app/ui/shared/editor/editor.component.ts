import {
  Component,
  OnDestroy,
  AfterViewInit,
  EventEmitter,
  Input,
  Output, OnInit
} from '@angular/core';

@Component({
  selector: 'gp-editor',
  template: `<textarea id="{{elementId}}"></textarea>`
})
export class EditorComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() content: string;
  @Input() elementId: String;
  @Output() onEditorContentChange = new EventEmitter<any>();

  editor: any;

  ngOnInit(): void {
  }


  ngAfterViewInit() {
    this.initialiseEditor();
  }

  initialiseEditor() {
    tinymce.init({
      selector: '#' + this.elementId,
      branding: false,
      height: 500,
      plugins: ['link', 'paste', 'table'], // , 'code', 'image', 'print', 'preview'],
      content_css: [
        "assets/font/font-awesome/css/font-awesome.min.css",
        "assets/font/material-design-icons/material-icons.css",
        "assets/mdb/css/bootstrap.min.css",
        "assets/mdb/css/mdb.css",
        "assets/css/styles.css"
      ],
      skin_url: 'assets/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorContentChange.emit(content);
        });
        editor.on('init', e => {
          e.target.setContent(this.content);
        });
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
}

