import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IApiController } from '../interfaces/api-controller.interface';

@injectable()
export class ReportScheduleRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.ReportScheduleController) private reportScheduleController: IApiController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportScheduleController.getAll
      )
      .get(
        '/:reportTypeId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportScheduleController.get
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportScheduleController.add
      )
      .put(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportScheduleController.update
      )
      .delete(
        '/:reportScheduleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportScheduleController.remove
      );
  }
}
