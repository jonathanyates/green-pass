import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {AlertsState} from "./alert.state";

export const SelectAlertTypes = () => {
  return (store:Store<AppState>) => {
    return store.select<AlertsState>(state => state.alerts)
      .map(state => state.alertTypes);
  }
};

export const SelectAlertSummary = () => {
  return (store:Store<AppState>) => {
    return store.select<AlertsState>(state => state.alerts)
      .map(state => state.alertSummary);
  }
};

export const SelectSelectedAlertSummary = () => {
  return (store:Store<AppState>) => {
    return store.select<AlertsState>(state => state.alerts)
      .map(state => state.selected);
  }
};

