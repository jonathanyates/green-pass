import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {LoadCompaniesAction} from "../../../store/companies/companies.actions";

@Injectable()
export class CompanyListResolve {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    this.store.dispatch(new LoadCompaniesAction());
    return true;
  }
}
