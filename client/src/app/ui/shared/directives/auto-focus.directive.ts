import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[autoFocus]'
})
export class AutoFocusDirective {

  constructor(private element: ElementRef) {
  }

  @Input() set autoFocus(focus: boolean) {
    if (focus) {
      this.element.nativeElement.focus();
    }
  }
}
