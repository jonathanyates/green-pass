import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IVehicleController } from './vehicle.controller';
import multer from 'multer';
const upload = multer({ dest: 'uploads/' });

@injectable()
export class VehicleRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.VehicleController) private vehicleController: IVehicleController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.getAll
      )
      .get(
        '/summary',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.getVehicleSummary
      )

      .get(
        '/vehiclecheck/:registration',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.vehicleCheck
      )
      .get(
        '/:vehicleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.get
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.add
      )
      .put(
        '/:vehicleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.vehicleController.update
      )

      .post(
        '/:vehicleId/services/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.addService
      )
      .put(
        '/:vehicleId/services/:serviceId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.updateService
      )

      .post(
        '/:vehicleId/inspections/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.addInspection
      )
      .put(
        '/:vehicleId/inspections/:inspectionId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.updateInspection
      )

      .post(
        '/:vehicleId/checks/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.addCheck
      )
      .put(
        '/:vehicleId/checks/:checkId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.vehicleController.updateCheck
      )

      .post(
        '/:vehicleId/drivers/:driverId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.vehicleController.addDriver
      )
      .delete(
        '/:vehicleId/drivers/:driverId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.vehicleController.removeDriver
      )

      .delete(
        '/:vehicleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.vehicleController.remove
      );
  }
}
