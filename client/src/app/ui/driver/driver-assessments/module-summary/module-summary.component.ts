import {Component, Input} from '@angular/core';
import {Paths} from "../../../../shared/constants";
import {SelectDriverDetailsTabAction} from "../../../../store/drivers/driver.actions";
import {Router} from "@angular/router";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";
import {ModuleSummary} from "../../../../../../../shared/models/module-summary.model";

@Component({
  selector: 'gp-module-summary',
  templateUrl: 'module-summary.component.html',
  host: {'class': 'card'}
})

export class ModuleSummaryComponent {

  @Input() moduleSummary: ModuleSummary;

  constructor(private store: Store<AppState>,
              private router: Router) {
  }

  viewTraining() {
    this.router.navigate([Paths.Driver, Paths.Assessments]);
    this.store.dispatch(new SelectDriverDetailsTabAction('assessments'));
  }
}
