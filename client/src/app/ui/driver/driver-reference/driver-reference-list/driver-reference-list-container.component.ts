import {Component, Input, OnInit} from '@angular/core';
import {IContact, IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {ICompany} from "../../../../../../../shared/interfaces/company.interface";
import {Subscription} from "rxjs/Subscription";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectDriverUserInfo} from "../../../../store/drivers/driver.selectors";
import {BaseComponent} from "../../../shared/components/base/base.component";
import {Roles} from "../../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-reference-list-container',
  template: `
    <gp-card [header]="'References'">
      <gp-driver-reference-list [driver]="driver" [user]="user" [references]="references" 
                                [allowAdd]="allowInput" [lightHeader]="true"
                                (add)="add()" (select)="select($event)"></gp-driver-reference-list>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class DriverReferenceListContainerComponent extends BaseComponent implements OnInit {

  driver: IDriver;
  user : IUser;
  references: IContact[];
  allowInput: boolean = false;

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        this.driver = info.driver;
        this.user = info.user;
        this.allowInput = info.allowInput;
        if (this.driver) {
          this.references = this.driver.references;
        } else {
          this.references = null;
        }
      }));
  }

  add() {
    return this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  select(contact: IContact) {
    return this.router.navigate(['./', contact._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
