import express from 'express';
import { Express } from 'express-serve-static-core';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthenticationMiddleware } from '../security/authentication/authentication.middleware.interface';
import { IFileController } from '../files/file.controller';

@injectable()
export class ApiRouter {
  routes = express.Router();

  constructor(
    @inject(TYPES.routers.CompanyRouter) private companyRouter: IApiRouter,
    @inject(TYPES.routers.TemplateRouter) private templateRouter: IApiRouter,
    @inject(TYPES.routers.AddressRouter) private addressRouter: IApiRouter,
    @inject(TYPES.routers.ComplianceRouter) private complianceRouter: IApiRouter,
    @inject(TYPES.routers.AuthenticationRouter) private authenticationRouter: IApiRouter,
    @inject(TYPES.routers.AlertRouter) private alertRouter: IApiRouter,
    @inject(TYPES.routers.AlertTypeRouter) private alertTypeRouter: IApiRouter,
    @inject(TYPES.routers.ReportTypeRouter) private reportTypeRouter: IApiRouter,
    @inject(TYPES.routers.SubscriptionRouter) private subscriptionRouter: IApiRouter,
    @inject(TYPES.security.AuthenticationMiddleware) private authenticationMiddleware: IAuthenticationMiddleware,
    @inject(TYPES.controllers.FileController) private fileController: IFileController
  ) {}

  configure(app: Express) {
    app.use('/test', function (req, res) {
      res.send('Api Works!');
    });

    this.routes.use('/auth', this.authenticationRouter.routes);
    this.routes.use('/companies', this.authenticationMiddleware.requireAuth, this.companyRouter.routes);
    this.routes.use('/templates', this.authenticationMiddleware.requireAuth, this.templateRouter.routes);
    this.routes.use('/address', this.authenticationMiddleware.requireAuth, this.addressRouter.routes);
    this.routes.use('/compliance', this.authenticationMiddleware.requireAuth, this.complianceRouter.routes);
    this.routes.use('/alerts', this.authenticationMiddleware.requireAuth, this.alertRouter.routes);
    this.routes.use('/alerttypes', this.authenticationMiddleware.requireAuth, this.alertTypeRouter.routes);
    this.routes.use('/reporttypes', this.authenticationMiddleware.requireAuth, this.reportTypeRouter.routes);
    this.routes.use('/subscriptions', this.authenticationMiddleware.requireAuth, this.subscriptionRouter.routes);
    this.routes.get('/pdf/:directory/:filename', this.fileController.getPdf);

    app.use('/api', this.routes);
  }
}
