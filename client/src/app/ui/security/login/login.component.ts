import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import * as actions from "../../../store/authentication/authentication.actions";
import {FormInput} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {DynamicFormBuilder} from "../../shared/components/dynamic-form/dynamic-form.builder";
import {SelectAuthenticationError} from "../../../store/authentication/authentication.selectors";
import {Paths} from "../../../shared/constants";
import {RegExpPatterns} from "../../../../../../shared/constants";

@Component({
  selector: 'gp-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit  {

  inputs:FormInput[];
  form:FormGroup;
  message:string;

  constructor(
    private store: Store<AppState>,
    public router: Router) {
  }

  ngOnInit(): void {

    this.store.let(SelectAuthenticationError())
      .subscribe(message => this.message = message);

    this.inputs = [
      { id: 'username', label: 'Username', type:'text', icon: 'fa-user', required: true, autoFocus: true },
      { id: 'password', label: 'Password', type:'password', icon: 'fa-lock', required: true }
    ];

    this.form = DynamicFormBuilder.build(this.inputs);
  }

  login() {
    let username = this.form.controls['username'].value.toString().trim();

    // set username to lowercase if it is an email address
    if (username.match(RegExpPatterns.Email)) {
      username = username.toLowerCase();
    }

    let password = this.form.controls['password'].value.toString().trim();
    this.store.dispatch(new actions.LoginAction(username, password));
  }

  forgotPassword() {
    this.router.navigate([Paths.ForgotPassword])
  }

  cancel() {
    this.router.navigate([Paths.Root]);
  }

}
