import 'reflect-metadata';
import mongoose from 'mongoose';
import { TYPES } from '../src/container/container.types';
import container from '../src/container/container.config';
import { IDriver } from '../../shared/interfaces/driver.interface';
import { IDriverRepository } from '../src/drivers/driver.repository';

describe.skip('Driver Repository', function () {
  describe.skip('get Driver', function () {
    this.timeout(20000);
    it('get driver with vehicles', () => {
      const driverRepository = <IDriverRepository>container.get<IDriverRepository>(TYPES.repositories.DriverRepository);

      return driverRepository.get('012345678901234567893001').then((driver: IDriver) => {
        expect(driver).not.toBeNull();
        expect(driver.vehicles).not.toBeNull();
        expect(driver.vehicles?.length).toEqual(2);
        expect(driver.vehicles![0].registrationNumber).toEqual('K2 JPY');
        expect(driver.vehicles![1].registrationNumber).toEqual('S2 SBY');
      });
    });
  });

  describe.skip('findOne Driver', () => {
    this.timeout(20000);
    it('find one driver with vehicles', async () => {
      const driverRepository = <IDriverRepository>container.get<IDriverRepository>(TYPES.repositories.DriverRepository);
      const driver = await driverRepository.findOne({ _id: new mongoose.Types.ObjectId('012345678901234567893001') });

      expect(driver).not.toBeNull();
      expect(driver.vehicles).not.toBeNull();
      expect(driver.vehicles?.length).toEqual(2);
      expect(driver.vehicles![0].registrationNumber).toEqual('K2 JPY');
      expect(driver.vehicles![1].registrationNumber).toEqual('S2 SBY');
    });
  });

  describe.skip('find Drivers', function () {
    this.timeout(20000);
    it('find drivers with vehicles', () => {
      const driverRepository = <IDriverRepository>container.get<IDriverRepository>(TYPES.repositories.DriverRepository);

      return driverRepository
        .find({ _id: new mongoose.Types.ObjectId('012345678901234567893001') })
        .then((drivers: IDriver[]) => {
          expect(drivers).not.toBeNull();
          expect(drivers.length).toEqual(1);
          const driver = drivers[0];
          expect(driver).not.toBeNull();
          expect(driver.vehicles).not.toBeNull();
          expect(driver.vehicles?.length).toEqual(2);
          expect(driver.vehicles![0].registrationNumber).toEqual('K2 JPY');
          expect(driver.vehicles![1].registrationNumber).toEqual('S2 SBY');
        });
    });
  });
});
