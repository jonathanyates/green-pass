import { inject, injectable } from 'inversify';
import request = require('request-promise-native');
import { IAddress, IAddressList } from '../../../shared/interfaces/address.interface';
import { IAddressRepository } from './address.repository';
import { TYPES } from '../container/container.types';

export interface IAddressService {
  addressSearch(postcode: string, house: string);
}

@injectable()
export class AddressService implements IAddressService {
  //private proxy = 'http://a83288:Ev3r3st94-@ncproxy1:8080';
  private proxy = '';

  private apiKey = '-sGYLgxHV0OYSaSUolOxNw37640';
  private apiUrl = 'https://api.getAddress.io/find/';

  constructor(@inject(TYPES.repositories.AddressRepository) private addressRepository: IAddressRepository) {}

  addressSearch(postcode: string, house: string = null): Promise<IAddressList> {
    const url = `${this.apiUrl}${postcode}?api-key=${this.apiKey}`;
    const regex = new RegExp('\b(house)\b');

    return this.addressRepository.get(postcode).then((result) => {
      if (result) {
        if (house) {
          result.addresses = result.addresses.filter((address) => address.line1.search(regex) > -1);
        }

        result.addresses = this.sort(result.addresses);
        return result;
      }

      return request
        .get({
          url: url,
          proxy: this.proxy,
        })
        .then((body) => {
          const result = JSON.parse(body);
          const addressList = <IAddressList>{
            postcode: postcode,
            addresses: [],
          };

          if (result.hasOwnProperty('addresses')) {
            const addresses = result.addresses;
            if (addresses) {
              addressList.addresses = addresses.map((address) => {
                const parts = address.split(',');
                return <IAddress>{
                  line1: parts[0].trim(),
                  line2: parts[1].trim(),
                  line3: parts[2].trim(),
                  line4: parts[3].trim(),
                  locality: parts[4].trim(),
                  townCity: parts[5].trim(),
                  county: parts[6].trim(),
                  country: 'United Kingdom',
                  postcode: postcode,
                };
              });

              addressList.addresses = this.sort(addressList.addresses);

              // save the results to be reused again
              if (addressList.addresses.length > 0) {
                return this.addressRepository.add(addressList);
              }
            }
          }

          if (house) {
            addressList.addresses = result.addresses.filter((address) => address.line1.search(regex) > -1);
          }

          return addressList;
        })
        .catch((error) => {
          console.error(error);
          return {
            postcode: postcode,
            addresses: [],
          };
        });
    });
  }

  sort(addresses: IAddress[]): IAddress[] {
    try {
      return addresses.sort((a, b) => this.compare(a, b, 'line1') || this.compare(a, b, 'line2'));
    } catch (e) {
      console.log('Error occured trying to sort addresses: ' + e.message);
      return addresses;
    }
  }

  compare(a: IAddress, b: IAddress, prop: string): number {
    const regex = /(\d+)/;
    const matchA = a[prop].match(regex);
    const numA = matchA && matchA.length > 0 ? parseInt(matchA[0]) : 0;
    const matchB = b[prop].match(regex);
    const numB = matchB && matchB.length > 0 ? parseInt(matchB[0]) : 0;

    const compareStreet = () => {
      const lineA = a[prop].replace(numA.toString(), '');
      const lineB = b[prop].replace(numB.toString(), '');
      return lineA.localeCompare(lineB);
    };

    return compareStreet() || numA - numB;
  }
}
