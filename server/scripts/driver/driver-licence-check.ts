import {TYPES} from '../../src/container/container.types';
import container from '../../src/container/container.config';
import {IDriverService} from '../../src/drivers/driver.service';

const driverService = container.get<IDriverService>(TYPES.services.DriverService);

const driverId = '5b1edb59cea5970018b9b17a';
const drivingLicenceNumber = 'BROUG659103M99NW',
      checkCode = 'jdFgSP59';

driverService.get(driverId)
  .then(driver => {
    return driverService.drivingLicenceCheck(driver, checkCode, drivingLicenceNumber)
      .then(result => {
        console.log(result);
        return result;
      })
      .catch(error => {
        console.log(error);
      });
  })
  .then(result => {
    process.exit();
  });
