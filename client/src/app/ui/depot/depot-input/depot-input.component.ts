import {Component, OnInit, OnDestroy} from '@angular/core';
import {Depot} from "../../company/company.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SaveDepotAction} from "../../../store/company/company.actions";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {DepotInputMapper} from "./depot-input.mapper";
import {FormGroup} from "@angular/forms";
import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";

@Component({
  selector: 'gp-depot-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class DepotInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private depot: Depot;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private depotInputMapper: DepotInputMapper) { }

  ngOnInit() {

    this.subscription = this.route.data
      .map((data: { depot: Depot }) => data.depot)
      .subscribe(depot => {
        this.depot = depot;
        this.inputGroups = this.depotInputMapper.mapTo(depot);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let depot = Object.assign(new Depot(), this.depot, this.depotInputMapper.mapFrom(form));
    this.store.dispatch(new SaveDepotAction(depot));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
