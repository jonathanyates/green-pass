import {Directive, Input, ElementRef, Renderer} from '@angular/core';
import {NgControl} from "@angular/forms";

@Directive({
  selector: '[validationClass]'
})
export class ValidationClassDirective {

  private control: NgControl;

  constructor(
    private element: ElementRef,
    public renderer: Renderer
  ) { }

  @Input() set validationClass(control: NgControl) {

    this.control = control;

    control.valueChanges.subscribe(x => {
      this.setClasses();
    });

    control.statusChanges.subscribe(x => {
      this.setClasses();
    });

    this.setClasses();
  }

  setClasses() {
    if ((this.control.dirty || this.control.touched)) {
      if (this.control.valid) {
        this.renderer.setElementClass(this.element.nativeElement, 'has-success', true);
        this.renderer.setElementClass(this.element.nativeElement, 'has-danger', false);
      } else  {
        this.renderer.setElementClass(this.element.nativeElement, 'has-danger', true);
        this.renderer.setElementClass(this.element.nativeElement, 'has-success', false);
      }
    }
  }

}

