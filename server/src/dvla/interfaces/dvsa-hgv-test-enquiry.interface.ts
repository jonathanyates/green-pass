export type IDvsaHgvTestEnquiryReponse = IDvsaHgvTestEnquiry[];

export interface IDvsaHgvTestEnquiry {
  registration: string;
  make: string;
  model: string;
  manufactureDate: string;
  vehicleType: string;
  vehicleClass: string;
  registrationDate: string;
  annualTestExpiryDate: string;
  annualTests: IDvsaAnnualTest[];
}

export interface IDvsaAnnualTest {
  testDate: string;
  testType: string;
  testResult: string;
  testCertificateNumber: string;
  expiryDate: string;
  numberOfDefectsAtTest: string;
  numberOfAdvisoryDefectsAtTest: string;
  defects?: IDvsaAnnualTestDefect[];
  location: string;
}

export interface IDvsaAnnualTestDefect {
  failureItemNo: string;
  failureReason: string;
  severityCode: string;
  severityDescription: string;
}
