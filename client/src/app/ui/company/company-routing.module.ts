import { NgModule } from '@angular/core';
import {Roles} from "../../../../../shared/constants";
import { Routes, RouterModule } from '@angular/router';
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {CompanyInputComponent} from "./company-input/company-input.component";
import {CanDeactivateGuard} from "../shared/guards/can-deactivate.guard";
import {DepotListComponent} from "../depot/depot-list/depot-list.component";
import {SupplierListComponent} from "../supplier/supplier-list/supplier-list.component";
import {DepotInputComponent} from "../depot/depot-input/depot-input.component";
import {DepotResolve} from "../depot/depot.resolve";
import {SupplierResolve} from "../supplier/supplier.resolve";
import {SupplierInputComponent} from "../supplier/supplier-input/supplier-input.component";
import {CompanyResolve} from "./company.resolve";
import {CompanyComponent} from "./company.component";
import {SupplierDetailsComponent} from "../supplier/supplier-details/supplier-details.component";
import {DepotDetailsComponent} from "../depot/depot-details/depot-details.component";
import {CompanyBreadcrumbRouting} from "./company-breadcrumb.routing";
import {CompanyDashboardComponent} from "./company-dashboard/company-dashboard.component";
import {CompanyInsuranceResolve} from "./company-insurance/company-insurance.resolve";
import {CompanyInsuranceInputComponent} from "./company-insurance/company-insurance-input/company-insurance-input.component";
import {CompanyInsuranceComponent} from "./company-insurance/company-insurance-detail/company-insurance.component";
import {CompanyDetailsTabListComponent} from "./company-details-tab-list/company-details-tab-list.component";
import {SubscriptionInputComponent} from "./subscription-input/subscription-input.component";
import {CompanySettingsInputContainerComponent} from "./company-settings/company-settings-input-container.component";
import {DriversComplianceComponent} from "./compliance/drivers-compliance.component";
import {VehiclesComplianceComponent} from "./compliance/vehicles-compliance.component";
import {CompanyComplianceComponent} from "./compliance/company-compliance.component";

const routes: Routes = [
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: { roles: [Roles.Admin, Roles.Company] },
    children: [
      { path: '', component: CompanyDashboardComponent, resolve: { company: CompanyResolve } },
      { path: ':id', component: CompanyDashboardComponent, resolve: { company: CompanyResolve } },
      { path: ':id/details', component: CompanyDetailsTabListComponent,  resolve: { company: CompanyResolve } },
      { path: ':id/details/edit', component: CompanyInputComponent, resolve: { company: CompanyResolve }, canDeactivate: [CanDeactivateGuard] },

      { path: ':id/depots', component: DepotListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/depots/new', component: DepotInputComponent, resolve: { depot: DepotResolve } },
      { path: ':id/depots/:depotId', component: DepotDetailsComponent, resolve: { depot: DepotResolve } },
      { path: ':id/depots/:depotId/edit', component: DepotInputComponent, resolve: { depot: DepotResolve } },

      { path: ':id/suppliers', component: SupplierListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/suppliers/new', component: SupplierInputComponent, resolve: { supplier: SupplierResolve } },
      { path: ':id/suppliers/:supplierId', component: SupplierDetailsComponent, resolve: { supplier: SupplierResolve } },
      { path: ':id/suppliers/:supplierId/edit', component: SupplierInputComponent, resolve: { supplier: SupplierResolve } },

      { path: ':id/insurance/new', component: CompanyInsuranceInputComponent, resolve: { vehicleReference: CompanyInsuranceResolve } },
      { path: ':id/insurance/:insuranceId', component: CompanyInsuranceComponent, resolve: { vehicleReference: CompanyInsuranceResolve } },
      { path: ':id/insurance/:insuranceId/edit', component: CompanyInsuranceInputComponent, resolve: { vehicleReference: CompanyInsuranceResolve } },
      { path: ':id/insurance', redirectTo: ':id/details' },

      { path: ':id/subscription/edit', component: SubscriptionInputComponent, resolve: { company: CompanyResolve } },
      { path: ':id/subscription', redirectTo: ':id/details' },

      { path: ':id/settings/edit', component: CompanySettingsInputContainerComponent, resolve: { company: CompanyResolve } },
      { path: ':id/settings', redirectTo: ':id/details' },

      { path: ':id/compliance', component: CompanyComplianceComponent, resolve: { company: CompanyResolve } },
      { path: ':id/drivers/compliance', component: DriversComplianceComponent, resolve: { company: CompanyResolve } }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    CompanyResolve,
    DepotResolve,
    SupplierResolve,
    CompanyInsuranceResolve
  ]
})
export class CompanyRoutingModule {
  constructor(private breadcrumbRouting: CompanyBreadcrumbRouting) {
    this.breadcrumbRouting.configure();
  }
}
