import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IAddress, IAddressList} from "../../../../../shared/interfaces/address.interface";
import {LogoutAction} from "../authentication/authentication.actions";

export const ActionTypes = {
  LOAD: type('[Address] Load Address List'),
  LOAD_SUCCESS: type('[Address] Load Address List Success'),
  SELECT: type('[Address] Select'),
  CLEAR: type('[Address] Clear Address List')
};

export class LoadAddressListAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload: string) { } // postcode
}

export class LoadAddressListActionSuccess implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: IAddressList) { }
}

export class SelectAddressAction implements Action {
  type = ActionTypes.SELECT;
  constructor(public payload: IAddress) { }
}

export class ClearAddressAction implements Action {
  type = ActionTypes.CLEAR;
  payload = null;
  constructor() { }
}

export type Actions
  = LoadAddressListActionSuccess
  | SelectAddressAction
  | ClearAddressAction
  | LogoutAction;
