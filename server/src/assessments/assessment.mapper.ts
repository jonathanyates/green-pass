import {IRoadMarqueDriver} from "../../../shared/interfaces/roadmarque-driver.interface";
import {IAssessment, IOnRoadAssessment} from "../../../shared/interfaces/assessment.interface";
import {RiskLevel} from "../../../shared/models/driver-compliance.model";
import {IDriver} from "../../../shared/interfaces/driver.interface";
import {ModuleSummary} from "../../../shared/models/module-summary.model";
import {AssessmentSummary} from "../../../shared/models/assessment-summary.model";

export const assessmentMapper = (roadMarqueDriver: IRoadMarqueDriver, driver: IDriver): IAssessment => {

  if (roadMarqueDriver.aptitudeAssessments == null && roadMarqueDriver.trainingModules == null) {
    return null;
  }

  const assessment: IAssessment = {
    refId: roadMarqueDriver.basicDetails.refId,
    onLine: {
      tests: [],
      overallRiskLevel: RiskLevel.Unknown
    },
    complete: false
  };

  if (roadMarqueDriver.aptitudeAssessments) {

    assessment.onLine.overallRiskLevel = roadMarqueDriver.aptitudeAssessments.overallAssessmentRiskLevel;

    // map results
    if (roadMarqueDriver.aptitudeAssessments.assessmentResult) {
      roadMarqueDriver.aptitudeAssessments.assessmentResult.forEach(result => {
        assessment.onLine.tests.push({
          testCode: result.testCode,
          description: result.testDescription,
          score: result.testScore,
          riskLevel: result.riskLevel,
          startDate: result.testStartDate,
          endDate: result.testEndDate,
          complete: result.testEndDate != null
        });
      })
    }

    // map invites
    if (roadMarqueDriver.aptitudeAssessments.assessmentInvite) {
      roadMarqueDriver.aptitudeAssessments.assessmentInvite
        .testsRemaining.testDescription.forEach(description => {
        assessment.onLine.tests.push({
          description: description.value,
          score: 0,
          riskLevel: RiskLevel.Unknown,
          startDate: roadMarqueDriver.aptitudeAssessments.assessmentInvite.assessmentStartDate,
          complete: false
        })
      });

      assessment.onLine.inviteDate = roadMarqueDriver.aptitudeAssessments.assessmentInvite.assessmentInviteDate;
      assessment.onLine.startDate = roadMarqueDriver.aptitudeAssessments.assessmentInvite.assessmentStartDate;
    }

    // map completed date if tests complete
    if (assessment.onLine.tests.every(test => test.complete === true)) {

      const assessmentCompletedDate = assessment.onLine.tests
        .map(result => result.endDate)
        .filter(date => date != null)
        .sort((a, b) => b.getTime() - a.getTime())[0];

      // map training modules
      // Only take Modules that are created after the assessment completed date
      // cos others maybe ones in the past which we need to ignore
      if (assessment.onLine.overallRiskLevel != RiskLevel.Low && roadMarqueDriver.trainingModules) {
        const modules = roadMarqueDriver.trainingModules.intervention
          .filter(module => {
            const diff = assessmentCompletedDate && module.createdDate
              ? assessmentCompletedDate.getDayDiff(module.createdDate)
              : -1;

            return diff >= 0;
          });

        if (modules && modules.length > 0) {
          assessment.trainingModules = modules;
        }
      }

      // map on road assessments
      mapOnRoadAssessments(assessment, driver);

      // if on-road then use on-road completed and set completed date to on-road assessment date
      if (assessment.onRoad) {
        if (assessment.onRoad.complete && assessment.onRoad.assessmentDate) {
          assessment.completedDate = assessment.onRoad.assessmentDate;
        }
      }
      // else if there are modules then set completedDate to trainingModules if they are complete
      else if (assessment.trainingModules != null && assessment.trainingModules.length > 0) {
        if (assessment.trainingModules.every(module => module.completedDate != null)) {
          assessment.completedDate = assessment.trainingModules
            .map(result => result.completedDate)
            .filter(date => date != null)
            .sort((a, b) => b.getTime() - a.getTime())[0];
        }
      }
      // else the completedDate is the assessmentCompletedDate
      else {
        assessment.completedDate = assessmentCompletedDate;
      }
    }

    assessment.complete = assessment.completedDate != null;
  }

  return assessment;
};

function mapOnRoadAssessments(assessment: IAssessment, driver: IDriver) {

  const lastAssessment = driver.assessments && driver.assessments.length > 0
    ? driver.assessments.slice(-1)[0]
    : null;

  const moduleSummary = ModuleSummary.create(assessment);
  let onRoad: IOnRoadAssessment = lastAssessment ? lastAssessment.onRoad : null;

  // if the modules are complete and the result is Above Average or High and
  // there is no on road assessment arranged then create an on road assessment
  if ((!onRoad || !onRoad.inviteDate) &&
    (moduleSummary != null && moduleSummary.complete &&
      (moduleSummary.result === RiskLevel.AboveAverage || moduleSummary.result === RiskLevel.High))
  ) {
    onRoad = {
      inviteDate: Date.prototype.getToday(),
      complete: false
    };
  }

  assessment.onRoad = onRoad;
}