import {Component, Input} from '@angular/core';
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {IVehicle, IVehicleCheck} from "../../../../../../../shared/interfaces/vehicle.interface";
import {Paths} from "../../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {FormatDate} from "../../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-vehicle-check-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" 
                     [lightHeader]="true" [allowAdd]="allowAdd"></gp-dynamic-list>
`
})
export class VehicleCheckListComponent {

  list: IDynamicList;
  @Input() vehicle: IVehicle;
  @Input() allowAdd: boolean = false;
  @Input() set checkHistory(checkHistory: IVehicleCheck[]) {
    this.setCheckHistory(checkHistory);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute) {}

  setCheckHistory(checkHistory: IVehicleCheck[]): void {
    this.list = {
      columns: [
        {header: 'Check Date', field: 'checkDate'},
        {header: 'Checked By', field: 'checkedBy'},
        {header: "Odometer Reading", field: 'odometerReading' }
      ]
    };

    if (checkHistory) {
      this.list.items = checkHistory
        .sort((a, b) => b.checkDate.getTime() - a.checkDate.getTime())
        .map(check => {
          return Object.assign({}, check , {
            checkDate: FormatDate(check.checkDate)
          })
        })
    }
  }

  add() {
    this.router.navigate(['./', Paths.Checks, Paths.New], { relativeTo: this.route });
  }

  select(check: IVehicleCheck) {
    this.router.navigate(['./', Paths.Checks, check._id], { relativeTo: this.route });
  }
}
