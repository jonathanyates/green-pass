import {Component, OnDestroy, OnInit} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Company} from "../../company/company.model";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {AlertService} from "../../../services/alert.service";
import {pagination, Paths} from "../../../shared/constants";

@Component({
  selector: 'gp-vehicle-alert-list',
  template: `
    <gp-vehicle-list [vehicles]="vehicles" (back)="back()" (itemSelected)="select($event)"
                     [total]="total" [page]="page" [itemsPerPage]="itemsPerPage" [serverMode]="true"
                     (pageChange)="onPageChange($event)" (search)="onSearch($event)"></gp-vehicle-list>
  `
})
export class VehicleAlertListComponent implements OnInit, OnDestroy {

  company: Company;
  alertTypeId: string;
  vehicles: IVehicle[];

  total: number;
  page: number = 1;
  itemsPerPage: number;
  private searchCriteria: any;

  private subscriptions = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.itemsPerPage = pagination.pageSize;

    this.subscriptions.push(
      this.route.params.map((params: Params) => params['alertTypeId']).filter(x => x != null)
        .combineLatest(this.store.let(SelectCompany()).filter(x => x != null))
        .subscribe(results => {
          this.alertTypeId = results[0];
          this.searchCriteria = {alertType: this.alertTypeId};
          this.company = results[1];
          this.getAlerts(this.page)
        }));
  }

  getAlerts(page: number) {
    this.alertService.search(this.company._id, page, this.itemsPerPage, this.searchCriteria)
      .subscribe(results => {
        this.vehicles = results.items.map(alert => alert.vehicle);
        this.total = results.total;
        this.page = results.page;
      })
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  select(vehicle: IVehicle) {
    return this.router.navigate([Paths.Company, this.company._id, Paths.Vehicles, vehicle._id]);
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onPageChange(page: number) {
    this.getAlerts(page);
  }

  onSearch(term: string) {
    if (term) {
      this.searchCriteria = {
        alertType: this.alertTypeId,
        registrationNumber: term
      };
    } else {
      this.searchCriteria = {alertType: this.alertTypeId};
    }

    this.getAlerts(1);
  }
}
