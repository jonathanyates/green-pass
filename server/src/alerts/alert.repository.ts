import mongoose from 'mongoose';
import { IQueryableRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { Query } from 'mongoose';
import { IAlertModel } from './alert.schema';
import { IAlert } from '../../../shared/interfaces/alert.interface';
import { getAggregations } from './alert.aggregations';
import { IMessageService } from '../messaging/message.service';
import { DriverCompliance } from '../../../shared/models/driver-compliance.model';
import { VehicleCompliance } from '../../../shared/models/vehicle-compliance.model';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';
import { IDriverRepository } from '../drivers/driver.repository';

export interface IAlertRepository extends IQueryableRepository<IAlert> {
  count(criteria: any);
}

@injectable()
export class AlertRepository implements IAlertRepository {
  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.services.MessageService) private messageService: IMessageService,
    @inject(TYPES.repositories.DriverRepository) private driverRepository: IDriverRepository
  ) {}

  private populate(model) {
    return model.populate({
      path: 'alertType',
      populate: { path: 'reportType' },
    });
  }

  getAll(): Promise<IAlertModel[]> {
    return this.queryAll().exec();
  }

  get(id: string): Promise<IAlertModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid Alert id.', codes.BAD_REQUEST));
    }

    return this.query(id).exec();
  }

  queryAll(): Query<IAlertModel[], mongoose.Document> {
    return this.populate(this.dbContext.alert.find());
  }

  query(id: string): Query<IAlertModel, mongoose.Document> {
    return this.populate(this.dbContext.alert.findById(id));
  }

  count(criteria: any) {
    return this.dbContext.alert.aggregate(getAggregations(criteria).concat([{ $count: 'count' }])).exec();
  }

  // populate refers to populating the driver and vehicle objects for each alert
  find(criteria: any, page?: number, pageSize?: number): Promise<IAlert[]> {
    const aggregations = getAggregations(criteria);

    if (page) {
      const skip = (page - 1) * pageSize;
      return this.dbContext.alert
        .aggregate(aggregations.concat([{ $skip: skip }, { $limit: pageSize }]))
        .exec()
        .then((alerts: IAlert[]) =>
          Promise.all(
            alerts.map((alert) => {
              return this.driverRepository.populateVehicles(alert.driver).then((driver) => alert);
            })
          )
        )
        .then((alerts: IAlert[]) => this.addCompliance(alerts));
    }

    return this.dbContext.alert
      .aggregate(aggregations)
      .exec()
      .then((alerts: IAlertModel[]) => {
        alerts.forEach((alert) => {
          // bit of hacky work around for the fact the aggregations insist on returning
          // a driver as an empty object even if one does not exist.
          // Therefore if there is no _id then just reset it to null.
          if (alert.driver && !alert.driver._id) {
            alert.driver = null;
          }
        });
        return alerts;
      })
      .then((alerts: IAlert[]) =>
        Promise.all(
          alerts.map((alert) => {
            return this.driverRepository.populateVehicles(alert.driver).then((driver) => alert);
          })
        )
      )
      .then((alerts: IAlert[]) => this.addCompliance(alerts));
  }

  addCompliance(alerts: IAlert[]): Promise<IAlert[]> {
    if (alerts && alerts.length > 0) {
      return this.dbContext.company.findById(alerts[0]._companyId).then((company) => {
        return alerts.map((alert) => {
          if (alert.driver) {
            alert.driver.compliance = new DriverCompliance(alert.driver, company);
          }
          if (alert.vehicle) {
            alert.vehicle.compliance = new VehicleCompliance(alert.vehicle, company.settings);
          }
          return alert;
        });
      });
    }
    return Promise.resolve([]);
  }

  findOne(criteria: any): Promise<IAlertModel> {
    return this.populate(this.dbContext.alert.findOne(criteria)).exec();
  }

  async add<T extends IAlert>(alert: IAlertModel): Promise<IAlertModel> {
    const AlertModel = new this.dbContext.alert(alert);
    const model = await AlertModel.save();
    const populatedAlert = await this.populate(model);
    return populatedAlert;
    // .then(alert => {
    //   this.messageService.send(queueNames.companyComplianceUpdate, alert._companyId);
    //   return alert;
    // });
  }

  update<T extends IAlert>(Alert: IAlert): Promise<IAlertModel> {
    return this.populate(this.dbContext.alert.findByIdAndUpdate(Alert._id, Alert, { new: true })).exec();
    // .then(alert => {
    //   this.messageService.send(queueNames.companyComplianceUpdate, alert._companyId);
    //   return alert;
    // });
  }

  remove<T extends IAlert>(alertId: string): Promise<IAlertModel> {
    return this.dbContext.alert.findByIdAndRemove(alertId).exec();
    // .then(alert => {
    //   this.messageService.send(queueNames.companyComplianceUpdate, alert._companyId);
    //   return alert;
    // });
  }
}
