#!/usr/bin/env bash
mongoimport --host mongodb --db green-pass --collection addresses --file addresses.json
mongoimport --host mongodb --db green-pass --collection companies --file companies.json
mongoimport --host mongodb --db green-pass --collection compliances --file compliances.json
mongoimport --host mongodb --db green-pass --collection drivers --file drivers.json
mongoimport --host mongodb --db green-pass --collection users --file users.json
mongoimport --host mongodb --db green-pass --collection vehicles --file vehicles.json
