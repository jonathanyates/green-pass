import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";

@Component({
  selector: 'gp-driver-vehicle-list',
  template: `
    <gp-vehicle-list *ngIf="driver" [vehicles]="driver.vehicles" 
                     [lightHeader]="lightHeader" [showSearch]="false"
                     (add)="onAdd()" (remove)="removeVehicle($event)"
                     (itemSelected)="onSelect($event)"></gp-vehicle-list>
  `
})

export class DriverVehicleListComponent {

  @Input() driver: IDriver;
  @Input() company;
  @Input() user : IUser;
  @Input() allowInput: boolean = false;
  @Input() lightHeader: boolean = false;

  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() remove: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();
  @Output() select: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();

  onAdd() {
    this.add.emit();
  }

  onSelect(vehicle: IVehicle) {
    this.select.emit(vehicle);
  }

  removeVehicle(vehicle: IVehicle) {
    this.remove.emit(vehicle);
  }

}
