import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {IContact} from "../../../../../../../shared/interfaces/driver.interface";
import {IListGroupItem} from "../dynamic-list-group/dynamic-list-group";
import {DynamicListGroupComponent} from "../dynamic-list-group/dynamic-list-group.component";

@Component({
  selector: 'gp-contact-details',
  template: `
    <gp-dynamic-list-group [items]="items" [allowEdit]="allowEdit"></gp-dynamic-list-group>
  `
})
export class ContactDetailsComponent implements OnInit {

  items: IListGroupItem[];
  private _contact: IContact;

  @Input() allowEdit: boolean = false;

  @Input() set contact(contact: IContact) {
    this._contact = contact;
    this.setList(contact);
  }
  get contact() { return this._contact; }

  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(DynamicListGroupComponent) listGroup: DynamicListGroupComponent;

  constructor() { }

  private setList(contact: IContact) {
    if (contact) {
      this.items = [
        {title: "Name", value: contact ? contact.forename + ' ' + contact.surname : null},
        {title: "Address", value: contact ? contact.address : null},
        {title: "Phone", value: contact ? contact.phone : null},
        {title: "Mobile", value: contact ? contact.mobile : null},
        {title: "Email", value: contact ? contact.email : null}
      ];
    }
  }

  ngOnInit() {
    if (this.listGroup) {
      if (this.edit.observers.length > 0) {
        this.listGroup.edit.subscribe(e => this.onEdit());
      }
      if (this.add.observers.length > 0) {
        this.listGroup.add.subscribe(e => this.onAdd());
      }
    }
  }

  onEdit() {
    this.edit.emit();
  }

  onAdd() {
    this.add.emit();
  }

}
