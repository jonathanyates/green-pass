import {Component, Input, OnInit} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {DriverCompliance} from "../../../../../../../shared/models/driver-compliance.model";

@Component({
  selector: 'gp-driver-next-of-kin-card',
  templateUrl: 'driver-next-of-kin-card.component.html',
  host: {'class': 'card'}
})
export class DriverNextOfKinCardComponent implements OnInit {

  hasNextOfKin: boolean;

  private _driver: IDriver;
  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasNextOfKin = DriverCompliance.getDriverNextOfKinCompliance(driver);
    }
  }
  get driver() {
    return this._driver;
  }

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  navigateToNextOfKin() {
    if (!this.hasNextOfKin) {
      return this.router.navigate([Paths.Driver, Paths.NextOfKin, Paths.Add]);
    }

    return this.router.navigate([Paths.Driver, Paths.NextOfKin]);
  }
}
