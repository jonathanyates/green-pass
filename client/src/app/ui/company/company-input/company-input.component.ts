import {Component, OnInit, OnDestroy} from '@angular/core';
import {AppState} from '../../../store/app.states';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {SaveCompanyAction} from '../../../store/company/company.actions';
import {FormGroup} from '@angular/forms';
import {FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {CompanyInputMapper} from './company-input.mapper';
import {CompaniesService} from '../../../services/companies.service';
import {ISubscription} from '../../../../../../shared/interfaces/subscription.interface';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {jsonDeserialiser} from '../../../../../../shared/utils/json.deserialiser';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'gp-company-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class CompanyInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private sub: Subscription;
  private company: ICompany;
  private subscriptions: ISubscription[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private companiesService: CompaniesService,
    private companyInputMapper: CompanyInputMapper) { }

  ngOnInit() {
    this.sub = this.route.data
      .map((data: { company: ICompany }) => data.company)
      .combineLatest(this.companiesService.getSubscriptions())
      .subscribe(results => {
        this.company = results[0];
        this.subscriptions = results[1];
        this.inputGroups = this.companyInputMapper.mapTo(this.company, this.subscriptions);
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  save(form: FormGroup) {
    let company = jsonDeserialiser.parse(JSON.stringify(this.company));
    company = Object.assign(company, this.companyInputMapper.mapFrom(form, this.subscriptions));
    this.store.dispatch(new SaveCompanyAction(company));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
