import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'gp-heading',
  template: `
    <h4>{{text}}</h4>
    <hr>
`
})
export class HeadingComponent {

  @Input() text:string;
  @Output() back: EventEmitter<any> = new EventEmitter<any>();

  onBack() {
    this.back.emit();
  }
}
