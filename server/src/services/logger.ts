import fs = require('fs');
import winston = require('winston');
import { Environments, default as serverConfig } from '../server/server.config';
require('winston-daily-rotate-file');
require('winston-loggly-bulk');

const logDir = 'log';
const env = process.env.NODE_ENV || Environments.development;
const tsFormat = () => new Date().toLocaleTimeString();
const datePattern = 'yyyy-MM-dd.';
const level = 'debug'; //env !== Environments.production ? 'debug' : 'info';

export function configureLogger(logDirectory: string, logglyTag: string) {
  const dir = `${logDir}/${logDirectory}`;

  // Create the log directory if it does not exist
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  if (serverConfig.loggly.enabled) {
    winston.add(winston.transports.Loggly, {
      token: serverConfig.loggly.token,
      subdomain: serverConfig.loggly.subdomain,
      tags: [logglyTag],
      json: true,
    });
  }

  winston.add(winston.transports.DailyRotateFile, {
    filename: `${dir}/log`,
    timestamp: tsFormat,
    datePattern: datePattern,
    prepend: true,
    level: level,
  });
}
