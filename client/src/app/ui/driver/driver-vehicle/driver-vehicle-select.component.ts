import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.states';
import {pagination} from '../../../shared/constants';
import {IVehicle} from '../../../../../../shared/interfaces/vehicle.interface';
import {SelectCompany} from '../../../store/company/company.selectors';
import {SelectVehicles} from '../../../store/vehicles/vehicle.selectors';
import {ClearVehicleListAction, LoadVehiclesAction} from '../../../store/vehicles/vehicle.actions';
import {ModalComponent} from '../../shared/components/modal/model.component';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {SelectDriver} from '../../../store/drivers/driver.selectors';
import {IDriver} from '../../../../../../shared/interfaces/driver.interface';
import {IDynamicList} from '../../../../../../shared/models/dynamic-list.model';
import {VehicleListBuilder} from '../../../../../../shared/models/vehicle-list-builder';
import {VehicleTypes} from '../../../../../../shared/interfaces/constants';
import {DriverService} from '../../../services/driver.service';
import {BusyAction} from '../../../store/progress/progress.actions';
import {SaveDriverSuccessAction, SelectDriverDetailsTabAction} from '../../../store/drivers/driver.actions';
import {User} from '../../user/user.model';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {VehicleService} from '../../../services/vehicle.service';
import {IUser} from '../../../../../../shared/interfaces/user.interface';
import {SelectLoggedInUser} from '../../../store/authentication/authentication.selectors';

@Component({
  templateUrl: './driver-vehicle-select.component.html'
})
export class DriverVehicleSelectComponent implements OnInit, OnDestroy {

  searchTerm$: ReplaySubject<string> = new ReplaySubject<string>(1);
  driver: IDriver;
  company: ICompany;
  vehicles: IVehicle[];
  list: IDynamicList;
  vehicle: IVehicle;
  user: IUser;

  itemsPerPage: number;
  message: string;
  dvlaCheck: boolean;
  searching: boolean;

  private searchCriteria: any;
  private subscriptions = [];

  @ViewChild(ModalComponent) modal: ModalComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private driverService: DriverService,
              private store: Store<AppState>,
              private vehicleService: VehicleService) {
  }

  ngOnInit() {
    this.vehicles = null;
    this.itemsPerPage = pagination.pageSize;
    this.store.dispatch(new ClearVehicleListAction());

    this.subscriptions.push(this.store.let(SelectVehicles())
      .combineLatest(
        this.store.let(SelectCompany()).filter(company => company != null),
        this.store.let(SelectDriver()).filter(driver => driver != null),
        (vehicles, company, driver) => ({ vehicles: vehicles, company: company, driver: driver }))
      .subscribe(results => {
        this.searching = false;
        this.driver = results.driver;
        this.company = results.company;
        if (this.searchCriteria) {
          this.onSelectVehicles(results);
        }
      }, err => this.searching = false));

    this.subscriptions.push(this.store.let(SelectLoggedInUser())
      .subscribe(user => this.user = user))
  }

  private onSelectVehicles(results) {
    if (!results) {
      return;
    }

    if (!results.vehicles || results.vehicles.length === 0) {
      this.dvlaCheck = true;
      return;
    }

    const vehicle = results.vehicles[0];
    const isDriverVehicle = this.driver.vehicles.some(driverVehicle => driverVehicle._id === vehicle._id);

    if (isDriverVehicle) {
      this.message = `Vehicle is already added to ${User.getFullName(this.driver._user)}`;
      return;
    }

    if (vehicle.type !== VehicleTypes.grey) {
      this.message = `Vehicle is not a Grey Fleet (Owned) vehicle`;
      return;
    }

    this.vehicles = [vehicle];
    this.list = VehicleListBuilder.getList(this.vehicles, this.company.settings);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  checkTerm(term: string) {
    this.message = null;
    if (!term) {
      this.vehicles = null;
      this.dvlaCheck = false;
      return;
    }
  }

  onPageChange(page: number) {
    if (this.searchCriteria) {
      this.store.dispatch(new LoadVehiclesAction(this.company._id, page, this.itemsPerPage, this.searchCriteria));
      return;
    }

    this.store.dispatch(new LoadVehiclesAction(this.company._id, page, this.itemsPerPage));
  }

  search(term: string) {
    this.dvlaCheck = false;
    this.message = null;
    this.vehicles = null;
    this.vehicle = null;

    if (term) {
      term = term.trim();
    }

    if (!term) {
      return;
    }

    this.searchTerm$.next(term);

    // exact match but allow spaces between characters (e.g. 'K2 JPY')
    const regex = `^${term.split('').reduce((previousValue, currentValue) => previousValue + '\\s*' + currentValue)}$`;
    this.searchCriteria = {
      registrationNumber: regex
    };

    this.searching = true;

    this.store.dispatch(new LoadVehiclesAction(this.company._id, 1, this.itemsPerPage, this.searchCriteria))
  }

  select(vehicle: IVehicle) {
    if (!vehicle) {
      return;
    }
    this.modal.open();
    this.vehicle = vehicle;
  }

  addConfirmed(confirmed: boolean) {
    if (confirmed && this.vehicle) {

      this.message = null;
      this.store.dispatch(new BusyAction());

      if (this.dvlaCheck && this.vehicle && this.vehicle._id == null) {
        this.vehicleService.saveVehicle(this.vehicle)
          .subscribe(vehicle => {
            this.vehicle = vehicle;
            this.addVehicleToDriver();
          })
      } else {
        this.addVehicleToDriver();
      }
    }
    this.vehicle = null;
  }

  addVehicleToDriver() {
    this.driverService.addVehicle(this.driver, this.vehicle._id)
      .subscribe(driver => {
        this.store.dispatch(new BusyAction(false));
        this.store.dispatch(new SaveDriverSuccessAction(driver));
        this.store.dispatch(new SelectDriverDetailsTabAction('vehicles'));
        return this.back();
      }, error => {
        this.store.dispatch(new BusyAction(false));
        this.message = `Unable to add Vehicle. ${error.message}`;
      })
  }

  vehicleChanged(vehicle: IVehicle) {
    if (this.driver.ownedVehicle) {
      vehicle.type = VehicleTypes.grey;
    }
    this.vehicle = vehicle;
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
