import {jsonDeserialiser} from "../../../../shared/utils/json.deserialiser";
import {RiskLevel} from "../../../../shared/models/driver-compliance.model";

export const assessmentAverage = jsonDeserialiser.parse(JSON.stringify({
    "basicDetails": {
      "userId": "58199",
    },
    "aptitudeAssessments": {
      "assessmentResult": [{
        "attributes": {"testType": "THEORY_TEST_1_0"},
        "testDescription": "Theory Test Version 1.0",
        "testScore": "0",
        "riskLevel": RiskLevel.Average,
        "testStartDate": "2017-08-09T20:17:01.000Z",
        "testEndDate": "2017-08-09T20:19:01.000Z",
        "testCode": "THEORY_TEST_1_0"
      }], "overallAssessmentRiskLevel": RiskLevel.Average
    },
  "trainingModules": {
    "intervention": [{
      "createdDate": new Date(),
      "trainingProvider": null,
      "trainingRecommendationType": "E-learning",
      "subTrainingType": "Theory module",
      "arrangedDate": null,
      "arrangedBy": null,
      "contactDate": new Date(),
      "startDate": null,
      "completedDate": null,
      "details": null,
      "reasonDetails": "{UseLastCompletedAnyTimeLicCheck=True}{UseLastCompletedAnyTimeAssessments=True}{Age=49}{YearsLicHeld=9}{Accidents3Yrs=0}{Accidents2Yrs=0}{Accidents1Yrs=0}{AccidentsAtFault1Yrs=0}{AccidentsAtFault2Yrs=0}{AccidentsAtFault3Yrs=0}{AccidentsNonFaultGr1Yrs=0}{AccidentsNonFaultGr2Yrs=0}{AccidentsNonFaultGr3Yrs=0}{Mileage=20000}{WorkHours_Risk=2}{WorkHours_ShiftWorker=False}{WorkHours_NightTime=False}{StressRisk=2}{SpeedingIn1Years=False}{SpeedingIn2Years=False}{SpeedingIn3Years=False}{LicencePoints=0}{BannedIn3Years=False}{BannedIn2Years=False}{BannedIn1Year=False}{VehicleChoice=3}{OverallSurveyRisk=34}{TheoryQScore=100}{VideoScoreCountAboveAverage=0}{OverallAptitudeRisk=100}{OverallCombinedRisk=67}{Factor0=NAME:OverallAptitudeRisk|VALUE:|RECOMM:5, 6, 4}",
      "satisfactory": null,
      "testScore": null,
      "riskLevel": null
    }, {
      "createdDate": new Date(),
      "trainingProvider": null,
      "trainingRecommendationType": "E-learning",
      "subTrainingType": "Customer policy module",
      "arrangedDate": null,
      "arrangedBy": null,
      "contactDate": new Date(),
      "startDate": null,
      "completedDate": null,
      "details": null,
      "reasonDetails": "{UseLastCompletedAnyTimeLicCheck=True}{UseLastCompletedAnyTimeAssessments=True}{Age=49}{YearsLicHeld=9}{Accidents3Yrs=0}{Accidents2Yrs=0}{Accidents1Yrs=0}{AccidentsAtFault1Yrs=0}{AccidentsAtFault2Yrs=0}{AccidentsAtFault3Yrs=0}{AccidentsNonFaultGr1Yrs=0}{AccidentsNonFaultGr2Yrs=0}{AccidentsNonFaultGr3Yrs=0}{Mileage=20000}{WorkHours_Risk=2}{WorkHours_ShiftWorker=False}{WorkHours_NightTime=False}{StressRisk=2}{SpeedingIn1Years=False}{SpeedingIn2Years=False}{SpeedingIn3Years=False}{LicencePoints=0}{BannedIn3Years=False}{BannedIn2Years=False}{BannedIn1Year=False}{VehicleChoice=3}{OverallSurveyRisk=34}{TheoryQScore=100}{VideoScoreCountAboveAverage=0}{OverallAptitudeRisk=100}{OverallCombinedRisk=67}{Factor0=NAME:OverallAptitudeRisk|VALUE:|RECOMM:5, 6, 4}",
      "satisfactory": null,
      "testScore": null,
      "riskLevel": null
    }, {
      "createdDate": new Date(),
      "trainingProvider": null,
      "trainingRecommendationType": "E-learning",
      "subTrainingType": "Reducing stress module",
      "arrangedDate": null,
      "arrangedBy": null,
      "contactDate": new Date(),
      "startDate": null,
      "completedDate": null,
      "details": null,
      "reasonDetails": "{UseLastCompletedAnyTimeLicCheck=True}{UseLastCompletedAnyTimeAssessments=True}{Age=49}{YearsLicHeld=9}{Accidents3Yrs=0}{Accidents2Yrs=0}{Accidents1Yrs=0}{AccidentsAtFault1Yrs=0}{AccidentsAtFault2Yrs=0}{AccidentsAtFault3Yrs=0}{AccidentsNonFaultGr1Yrs=0}{AccidentsNonFaultGr2Yrs=0}{AccidentsNonFaultGr3Yrs=0}{Mileage=20000}{WorkHours_Risk=2}{WorkHours_ShiftWorker=False}{WorkHours_NightTime=False}{StressRisk=2}{SpeedingIn1Years=False}{SpeedingIn2Years=False}{SpeedingIn3Years=False}{LicencePoints=0}{BannedIn3Years=False}{BannedIn2Years=False}{BannedIn1Year=False}{VehicleChoice=3}{OverallSurveyRisk=34}{TheoryQScore=100}{VideoScoreCountAboveAverage=0}{OverallAptitudeRisk=100}{OverallCombinedRisk=67}{Factor0=NAME:OverallAptitudeRisk|VALUE:|RECOMM:5, 6, 4}",
      "satisfactory": null,
      "testScore": null,
      "riskLevel": null
    }]
  }
  }
));
