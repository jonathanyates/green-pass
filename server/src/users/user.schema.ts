import mongoose from 'mongoose';
import bcrypt = require('bcrypt-nodejs');
import { IUser } from '../../../shared/interfaces/user.interface';

//================================
// User Schema
//================================
export const UserSchema = new mongoose.Schema(
  {
    _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company' },
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    title: { type: String, required: false },
    forename: { type: String, required: true },
    surname: { type: String, required: true },
    email: { type: String, required: false },
    role: { type: String, enum: ['admin', 'company', 'driver', ''], default: '', required: true },
    resetPasswordToken: { type: String },
    resetPasswordExpires: { type: Date },
    changePassword: { type: Boolean, required: false },
    removed: { type: Boolean, default: false, required: false },
  },
  { timestamps: true }
);

// Pre-save of user to database, hash password if password is modified or new
UserSchema.pre('save', function (next) {
  // eslint-disable-next-line @typescript-eslint/no-this-alias
  const user = this;
  const SALT_FACTOR = 5;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

// Method to compare password for login
UserSchema.methods.comparePassword = function (candidatePassword, callBack) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return callBack(err);
    callBack(null, isMatch);
  });
};

export interface IUserModel extends mongoose.Document, IUser {
  _id: any;
  comparePassword(candidatePassword, callBack);
}
