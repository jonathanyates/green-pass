import { Injectable } from '@angular/core';
import {apiPath} from "../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {Observable} from "rxjs";
import {HttpBaseService} from "./http-base.service";
import {ICompliance} from "../../../../shared/interfaces/compliance.interface";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ComplianceService extends HttpBaseService {

  private apiUrl = apiPath + '/compliance/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getAll(): Observable<ICompliance[]> {

    this.authorize();

    return this.httpClient.get<ICompliance[]>(this.apiUrl, this.getOptions())
      .map(res => this.handleResult<ICompliance[]>(res))
      .catch(error => this.handleError(error));
  }

  get(companyId): Observable<ICompliance> {

    this.authorize();

    return this.httpClient.get<ICompliance> (this.apiUrl + companyId, this.getOptions())
      .map(res => this.handleResult<ICompliance>(res))
      .catch(this.handleError);
  }

}
