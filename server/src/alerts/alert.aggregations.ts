export const getAggregations = function (criteria: any): any[] {

  criteria = criteria != null ? criteria : {};

  return [
    {
      "$lookup": {
        "from": "alertTypes",
        "localField": "alertType",
        "foreignField": "_id",
        "as": "alertType"
      }
    },
    {$unwind: "$alertType"},
    {
      "$lookup": {
        "from": "reportTypes",
        "localField": "alertType.reportType",
        "foreignField": "_id",
        "as": "alertType.reportType"
      }
    },
    {$unwind: "$alertType.reportType"},
    {
      "$lookup": {
        "from": "vehicles",
        "localField": "vehicle",
        "foreignField": "_id",
        "as": "vehicle"
      }
    },
    {
      $unwind: {
        path: "$vehicle",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      "$lookup": {
        "from": "drivers",
        "localField": "driver",
        "foreignField": "_id",
        "as": "driver"
      }
    },
    {
      $unwind: {
        path: "$driver",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      "$lookup": {
        "from": "users",
        "localField": "driver._user",
        "foreignField": "_id",
        "as": "driver._user"
      }
    },
    {
      $unwind: {
        path: "$driver._user",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $addFields: {
        "name": {$concat: ["$driver._user.forename", " ", "$driver._user.surname"]},
        "registrationNumber": "$vehicle.registrationNumber"
      }
    },
    {
      $match: {
        $and: [
          {
            $or: [
              {'vehicle.removed': false},
              {'driver.removed': false}
            ]
          },
          {'status': {$eq: 'Active'} },
          criteria
        ]
      }
    },
  ];
};

