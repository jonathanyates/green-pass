import logger = require('winston');
import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { IUser } from '../../../shared/interfaces/user.interface';
import { IRepository } from '../db/db.repository';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { IUserModel } from './user.schema';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class UserRepository implements IRepository<IUserModel> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  getAll(): Promise<IUserModel[]> {
    return this.dbContext.user.find({ removed: false }).exec();
  }

  get(id: string): Promise<IUserModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid user id.', codes.BAD_REQUEST));
    }

    return this.dbContext.user.findById(id).exec();
  }

  find(criteria: any): Promise<IUserModel[]> {
    criteria.removed = false;
    return this.dbContext.user.find(criteria).exec();
  }

  findOne(criteria: any): Promise<IUserModel> {
    criteria.removed = false;
    return this.dbContext.user.findOne(criteria).exec();
  }

  add<T extends IUser>(user: IUser): Promise<IUserModel> {
    const model = new this.dbContext.user(user);
    return model.save().then((user) => {
      logger.info(`User ${user._id} added.`);
      return user;
    });
  }

  update<T extends IUser>(user: IUser): Promise<IUserModel> {
    return this.dbContext.user
      .findByIdAndUpdate(user._id, user, { new: true })
      .exec()
      .then((user) => {
        logger.info(`User ${user._id} updated.`);
        return user;
      });
  }

  remove<T extends IUser>(userId: string): Promise<IUserModel> {
    return this.dbContext.user
      .findByIdAndUpdate(userId, { removed: true }, { new: true })
      .exec()
      .then((user) => {
        logger.info(`User ${userId} removed.`);
        return user;
      });
  }
}
