import 'reflect-metadata';
import { EmailService } from '../src/email/email.service';

describe.skip('Email Service', () => {
  describe.skip('send', () => {
    it('send a Report', () => {
      const emailService = new EmailService();

      return emailService
        .send(
          ['jonathan.yates@hotmail.com'],
          'None Compliant Drivers report',
          '<h1>None Compliant Drivers</h1>',
          undefined
        )
        .then((result) => {
          expect(result).toBeTruthy();
        });
    });
  });
});
