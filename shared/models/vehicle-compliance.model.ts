import '../../shared/extensions/date.extensions';
import {IVehicleCompliance} from "../interfaces/compliance.interface";
import {IVehicle} from "../interfaces/vehicle.interface";
import {ICompanySettings} from "../interfaces/company-settings.interface";
import {VehicleTypes} from "../interfaces/constants";
import {defaultSettingsFactory} from "../interfaces/company-settings.factory";

export class VehicleCompliance implements IVehicleCompliance {
  compliant: boolean = false;
  tax: boolean = false;
  mot: boolean = false;
  service: boolean = false;
  inspection: boolean = false;
  vehicleCheck: boolean = false;

  constructor(vehicle: IVehicle, settings: ICompanySettings) {
    let date = new Date();
    this.tax = vehicle.taxDueDate > date;
    this.mot = vehicle.motDueDate > date;

    if (!settings || !settings.vehicles) {
      settings = defaultSettingsFactory();
    }

    if (vehicle.type === VehicleTypes.fleet || !settings.vehicles.grey.exclusions.service) {
      this.service = vehicle.nextService > date;
    } else {
      this.service = true;
    }

    if (vehicle.type === VehicleTypes.fleet || !settings.vehicles.grey.exclusions.inspection) {
      this.inspection = vehicle.nextInspection > date;
    } else {
      this.inspection = true;
    }

    if (vehicle.type === VehicleTypes.fleet || !settings.vehicles.grey.exclusions.checks) {
      this.vehicleCheck = vehicle.nextVehicleCheck > date;
    } else {
      this.vehicleCheck = true;
    }

    this.compliant =
      this.tax &&
      this.mot &&
      this.service &&
      this.inspection &&
      this.vehicleCheck;
  }
 }