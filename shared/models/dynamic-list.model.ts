import {ColumnType} from "./column-type.model";

export interface IColumn {
  header: string,
  field: string,
  style?: string,
  columnType?: ColumnType
}

export interface IDynamicList {
  columns: IColumn[],
  items?: any[],
  styles?: any[],
  actions?: any[]
}
