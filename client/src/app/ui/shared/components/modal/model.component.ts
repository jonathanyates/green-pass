import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

declare let $: any;

@Component({
  selector: 'gp-model',
  templateUrl: './model.component.html'
})
export class ModalComponent implements OnInit {

  @Input() title: string;
  @Input() hideCancel: string;
  @Output() confirmed:EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  open() {
    $('#modal-component').modal('show');
  }

  close() {
    $('#modal-component').modal('hide');
  }

  onConfirm(confirmed: boolean) {
    this.confirmed.emit(confirmed);
    this.close();
  }
}
