import {Component, Input} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-insurance-card',
  templateUrl: 'driver-insurance-card.component.html',
  host: {'class': 'card'}
})
export class DriverInsuranceCardComponent {

  hasInsurance: boolean;
  private _driver: IDriver;

  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasInsurance = driver.compliance.insurance;
    }
  }

  constructor(private router: Router) {}

  viewInsurance() {
    // navigate to add vehicle if the driver has no vehicles to view
    if (this._driver && (!this._driver.insuranceHistory || this._driver.insuranceHistory.length === 0)) {
      return this.router.navigate([Paths.Driver, Paths.Insurance, Paths.New]);
    }
    return this.router.navigate([Paths.Driver, Paths.Insurance]);
  }
}
