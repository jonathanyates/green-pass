# Green Pass

## Prerequistites

1.  Node

    - Install from https://nodejs.org/en/

2.  TypeScript

    - https://www.typescriptlang.org/
    - npm install -g typescript

3.  Angular CLI

    - https://cli.angular.io/
    - https://github.com/angular/angular-cli/wiki
    - Install
      - npm install -g @angular/cli

4.  MongoDB

    - https://www.mongodb.com/download-center#community
    - Make a data directory on your root drive for MongoDB
      - mkdir /data/db

5.  git source control repository
    - https://git-scm.com/

## Source Code

1.  Clone the Green Pass Repository

    - git clone https://github.com/JonathanYates/green-pass.git

1.  Change your Branch to develop (this is important)
    - git checkout develop

## Build / Run

MongoDB

1.  Start MongoDB

    To start the community edition on mac follow these instructions.
    https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-os-x/

    i.e

    - brew services start mongodb-community@6.0
    - mongod --config /usr/local/etc/mongod.conf

Server

1.  Change directory to server directory from source root.

    - cd server

2.  Install npm packages

    - npm install

3.  Transpile Typescript

    - tsc
    - or npm run build

4.  Start Server
    - npm start
    - You should see 'info: Green Pass server is running on port 3000'

Client

1.  Change directory to client directory from source root.

    - cd client

2.  Install npm packages

    - npm install

3.  Run Angular CLI to build and start development server

    - npm start

4.  Open your browser at http://localhost:4200

Running Micro Services (Optional)

Micro services communiate via RabbitMQ, therefore you will need to install and run this first.

1.  Install RabbitMQ

    - https://www.rabbitmq.com/download.html

2.  Change directory to server directory from source root.

    - cd server

3.  Start Services
    - npm run start-services
