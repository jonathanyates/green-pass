import {ContainerModule, interfaces} from "inversify";
import {EmailService, IEmailService} from "./email.service";
import {TYPES} from "../container/container.types";

export const emailModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IEmailService>(TYPES.services.EmailService).to(EmailService).inSingletonScope();
});