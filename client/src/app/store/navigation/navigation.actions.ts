import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";

export const ActionTypes = {
  NAVIGATION_END: type('[Navigation] End')
};

/**
 * Navigation Actions
 */
export class NavigationEndAction implements Action {
  type = ActionTypes.NAVIGATION_END;
  constructor(public payload: string[]) { }
}

export type Actions =
  NavigationEndAction
  | LogoutAction;
