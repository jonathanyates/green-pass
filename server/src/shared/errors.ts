export function ServerError(message, statusCode) {
  const error:any = Error(message);
  error.statusCode = statusCode;
  return error;
}
