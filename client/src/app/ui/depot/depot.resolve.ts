import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {Depot} from "../company/company.model";
import {AppState} from "../../store/app.states";
import {Observable} from "rxjs";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {SelectDepot} from "../../store/company/company.selectors";
import {SelectError} from "../../store/error/error.selectors";
import {IDepot} from "../../../../../shared/interfaces/company.interface";

@Injectable()
export class DepotResolve implements Resolve<IDepot|boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDepot|boolean> {

    let id = route.params['id'];
    let depotId = route.params['depotId'];
    if (!id) return Observable.of(false);

    this.store.dispatch(new SelectCompanyAction(id));

    // edit
    if (!depotId) {
      return Observable.of(Object.assign(new Depot(), {_companyId: id}));
    }

    return this.store.let(SelectDepot(depotId))
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (depot, error) => ({ depot:depot, error:error }))
      .filter(result => result.error !== null || (result.depot && result.depot._companyId == id))
      .map(result => result.error ? false : result.depot)
      .take(1);
  }
}
