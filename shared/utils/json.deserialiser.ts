// Taken from here
// http://stackoverflow.com/questions/14488745/javascript-json-date-deserialization
export const jsonDeserialiser = {
  dateFormat: /\d{4}-\d{2}-\d{2}/,

  parse: function (obj) {
    let parsedObj = JSON.parse(obj);
    return this.parseDates(parsedObj);
  },

  parseDates: function (obj) {
    // iterate properties
    for (let prop in obj) {
      // make sure the property is 'truthy'
      if (obj.hasOwnProperty(prop) && obj[prop]) {
        let value = obj[prop];
        // determine if the property is an array
        if (Array.isArray(value)) {
          for (let i = 0; i < value.length; i++) {
            this.parseDates(value[i]);
          }
        }
        // determine if the property is an object
        else if (typeof value == 'object') {
          this.parseDates(value);
        } else if (typeof value == 'string' && this.dateFormat.test(value)) {
          // parse and replace
          obj[prop] = new Date(obj[prop]);
        }
      }
    }

    return obj;
  },
};
