import {Component, OnInit, OnDestroy} from '@angular/core';
import {Supplier, Company} from "../../company/company.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Subscription} from "rxjs";
import {Paths} from "../../../shared/constants";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {SelectCompany} from "../../../store/company/company.selectors";
import {ISupplier} from "../../../../../../shared/interfaces/company.interface";

@Component({
  selector: 'gp-supplier-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class SupplierListComponent implements OnInit, OnDestroy  {

  suppliers: Supplier[];
  company: Company;
  list: IDynamicList;
  private sub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.sub = this.route.data
      .map((data: { company: Company }) => data.company)
      .merge(this.store.let(SelectCompany()))
      .distinctUntilChanged()
      .subscribe(company => {
        this.company = company;
        this.suppliers = company ? company.suppliers : null;
        this.list = {
          columns: [
            { header: 'Name', field: 'name' },
            { header: 'Address', field: 'address', style: 'hidden-xs-down' },
            { header: 'Phone', field: 'phone', style: 'hidden-sm-down' },
            { header: 'Mobile', field: 'mobile', style: 'hidden-sm-down' },
            { header: 'Email', field: 'email', style: 'hidden-sm-down' },
          ],
          items: this.suppliers
        }
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  add() {
    this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  select(supplier: ISupplier) {
    this.router.navigate(['./', supplier._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
