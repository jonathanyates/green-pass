import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {IReportType, ReportCategory} from "../../../../../../shared/interfaces/report.interface";

export class ReportTypeInputMapper {

  static mapFrom(form:FormGroup): IReportType {
    return <IReportType>{
      name: form.controls['name'].value,
      query: form.controls['query'].value,
      category: form.controls['category'].value
    };
  }

  static mapTo(reportType:IReportType): FormInputGroup[] {

    let category = Object.keys(ReportCategory)
      .map(key => {
        let value = ReportCategory[key];
        return { key: value, value: value, selected: reportType.category === value }
      });

    return [
      {
        label: 'ReportType',
        inputs: [
          { id: 'name', label: 'Report Name', type:'text', required: true, autoFocus: true, value: reportType.name },
          { id: 'query', label: 'Query Name', type:'text', required: true, value: reportType.query },
          { id: 'category', label: 'Category', type:'dropdown', required: true,
            value: reportType.category, gridClass: 'col-md-6', options: category }
        ]
      }
    ];
  }



}
