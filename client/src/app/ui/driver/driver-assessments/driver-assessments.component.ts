import {Component, Input} from '@angular/core';
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {FormatDate} from "../../../../../../shared/utils/helpers";
import {
  IAssessment, IOnLineAssessment, ITrainingModule
} from "../../../../../../shared/interfaces/assessment.interface";
import {AssessmentSummary} from "../../../../../../shared/models/assessment-summary.model";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import {ModuleSummary} from "../../../../../../shared/models/module-summary.model";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../shared/constants";
import {ICompany} from '../../../../../../shared/interfaces/company.interface';

declare let $: any;

let RiskLevelStyles = {
  Unknown: '',
  Low: 'badge success-color',
  Average: 'badge warning-color',
  'Above Average': 'badge warning-color',
  High: 'badge danger-color'
};

@Component({
  selector: 'gp-driver-assessments',
  template: `<gp-dynamic-list [list]="list" [lightHeader]="lightHeader"
                              (add)="addNewOnRoadAssessment()"
                              (itemSelected)="select($event)"></gp-dynamic-list>`,
  styleUrls: ['driver-assessments.component.css']
})
export class DriverAssessmentsComponent {

  list: IDynamicList;
  @Input() driver: IDriver;
  @Input() company: ICompany;
  @Input() user: IUser;
  @Input() showAction: boolean;
  @Input() set assessments(assessments: IAssessment[]) {
    this.setAssessments(assessments);
  }
  @Input() lightHeader: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  private setAssessments(assessments: IAssessment[]) {
    this.list = {
      columns: [
        {header: '', field: 'expander', columnType: ColumnType.Expander},
        {header: 'Type', field: 'type'},
        {header: 'Assessor', field: 'assessedBy'},
        {header: 'Invite Date', field: 'inviteDate'},
        {header: 'Started Date', field: 'startDate'},
        {header: 'Completed Date', field: 'completedDate'},
        {header: 'Result', field: 'result'},
        {header: 'Complete', field: 'completed'}      ],
      items: [],
      actions: [],
      styles: []
    };

    if (this.showAction) {
      this.list.columns.push({ header: 'Action', field: 'action', columnType: ColumnType.Button})
    }

    this.list.columns.push({header: '', field: 'isComplete'});

    if (assessments) {

      assessments = assessments
        .sort((a, b) => !b.complete
          ? 1
          : b.completedDate != null && a.completedDate != null
            ? b.completedDate.getTime() - a.completedDate.getTime()
            : 0);

      assessments.forEach((assessment, i) => {
        this.addAssessment(assessment);
        let summary = new AssessmentSummary(assessment);

        if (summary.complete) {
          this.addModules(assessment);
        }

        if (assessment.onRoad) {
          this.addOnRoad(assessment);
        }

        if (i < assessments.length - 1) {
          this.list.items.push({});
          this.list.styles.push({ rowStyle: 'table-row' });
        }
      });
    }

    this.list.items.forEach((item, i) => {
      if (item.details && item.details.visible && !item.complete) {
        $('#collapsibleDetail' + i).collapse('show');
      }
    })
  }

  private addAssessment(assessment: IAssessment) {

    if (!assessment || !assessment.onLine ||
        !assessment.onLine.tests || assessment.onLine.tests.length === 0) {
      return;
    }

    let assessmentSummary = new AssessmentSummary(assessment);
    assessmentSummary = Object.assign(assessmentSummary, {
      inviteDate: this.FormatDate(assessmentSummary.inviteDate, '-'),
      startDate: this.FormatDate(assessmentSummary.startDate, 'Not started'),
      completedDate: this.FormatDate(assessmentSummary.completedDate, 'Not completed'),
    });

    assessmentSummary.action = !assessmentSummary.complete
      ? assessmentSummary.completedAssessments == 0 ? 'Start' : 'Continue'
      : 'Completed';

    let assessmentDetail:IDynamicList = {
      columns: [
        {header: 'Description', field: 'description'},
        {header: 'Test Date', field: 'startDate'},
        {header: 'Score', field: 'score'},
        {header: 'Risk', field: 'riskLevel'},
        {header: 'Complete', field: 'isComplete'},
      ],
      items: assessment.onLine.tests
        ? assessment.onLine.tests.map(test => Object.assign({}, test, {
          startDate: this.FormatDate(test.startDate, 'Not started'),
          score: test.complete ? test.score : '-',
        }))
        : [],
      styles: []
    };

    assessmentDetail.styles = assessmentDetail.items.map((onLineAssessment: IOnLineAssessment) => ({
      rowStyle: onLineAssessment.complete ? 'table-success' : 'table-danger',
      isComplete: this.completedStyle(onLineAssessment.complete),
      riskLevel: onLineAssessment.complete
        ? RiskLevelStyles.hasOwnProperty(onLineAssessment.riskLevel)
          ? RiskLevelStyles[onLineAssessment.riskLevel]
          : null
        : 'badge danger-color'
    }));

    assessmentSummary.details = {
      header: 'Assessments',
      list: assessmentDetail
    };

    this.list.items.push(assessmentSummary);
    let self = this;

    if (this.showAction) {
      let action = !assessmentSummary.complete
        ? { action: { action: () => self.assessmentLogin() } }
        : null;

      this.list.actions.push(action);
    }

    this.list.styles.push({
      rowStyle: assessmentSummary.complete ? 'table-success' : 'table-danger',
      isComplete: this.completedStyle(assessmentSummary.complete),
      result: assessmentSummary.complete
        ? RiskLevelStyles.hasOwnProperty(assessmentSummary.result)
          ? RiskLevelStyles[assessmentSummary.result]
          : ''
        : assessmentSummary.percentageComplete > 80
          ? 'badge warning-color'
          : 'badge danger-color'
    });
  }

  private addOnRoad(assessment: IAssessment) {

    if (!assessment || !assessment.onRoad) {
      return;
    }

    let item:any = Object.assign({}, assessment.onRoad, {
      type: 'On Road Assessment',
      assessor: assessment.onRoad.assessedBy,
      inviteDate: this.FormatDate(assessment.onRoad.inviteDate, '-'),
      startDate: this.FormatDate(assessment.onRoad.assessmentDate, 'Not started'),
      completedDate: this.FormatDate(assessment.onRoad.assessmentDate, 'Not completed'),
      result: assessment.onRoad.result ? assessment.onRoad.result : 'Unknown'
    });

    if (this.user.role === Roles.Driver && !assessment.onRoad.complete) {
      item.details = {
        header: 'On Road Assessment Details',
        detail: [`
        You are required to complete an on road assessment with a qualified trainer. 
        You will be contacted in due course about a training date.`
        ],
        visible: true,
        headerStyle: 'danger-color'
      };
    }

    this.list.items.push(item);

    this.list.styles.push({
      rowStyle: assessment.onRoad.complete ? 'table-success' : 'table-danger',
      completed: this.completedStyle(assessment.onRoad.complete) });
  }

  addNewOnRoadAssessment() {
    if (this.user.role !== Roles.Driver) {
      return this.router.navigate(['./', Paths.Assessments, Paths.New], { relativeTo: this.route });
    }
  }

  private addModules(assessment: IAssessment) {

    if (!assessment || !assessment.trainingModules || assessment.trainingModules.length === 0) {
      return;
    }

    let moduleSummary = new ModuleSummary(assessment);
    moduleSummary = Object.assign(moduleSummary, {
      inviteDate: this.FormatDate(moduleSummary.inviteDate, '-'),
      startDate: this.FormatDate(moduleSummary.startDate, 'Not started'),
      completedDate: this.FormatDate(moduleSummary.completedDate, 'Not completed'),
      result: moduleSummary.complete
        ? moduleSummary.result
        : Math.round(moduleSummary.percentageComplete) + ' % complete'
    });

    moduleSummary.action = !moduleSummary.complete
      ? moduleSummary.completedModules == 0 ? 'Start' : 'Continue'
      : null;

    let moduleDetail:IDynamicList = {
      columns: [
        {header: 'Description', field: 'subTrainingType'},
        {header: 'Start Date', field: 'startDate'},
        {header: 'Completed Date', field: 'completedDate'},
        {header: 'Score', field: 'testScore'},
        {header: 'Risk', field: 'riskLevel'},
        {header: 'Complete', field: 'isComplete'},

      ],
      items: assessment.trainingModules.map((trainingModule: ITrainingModule) => Object.assign({}, trainingModule, {
        startDate: this.FormatDate(trainingModule.startDate, 'Not started'),
        completedDate: this.FormatDate(trainingModule.completedDate, 'Not completed'),
        testScore: trainingModule.testScore ? trainingModule.testScore : '-',
        riskLevel: trainingModule.riskLevel ? trainingModule.riskLevel : 'Unknown'
      })),
      styles: []
    };

    moduleDetail.styles = assessment.trainingModules.map(trainingModule => ({
      rowStyle: trainingModule.completedDate != null ? 'table-success' : 'table-danger',
      isComplete: this.completedStyle(trainingModule.completedDate != null),
      riskLevel: trainingModule.completedDate != null
        ? RiskLevelStyles.hasOwnProperty(trainingModule.riskLevel)
          ? RiskLevelStyles[trainingModule.riskLevel]
          : null
        : 'badge danger-color'
    }));

    moduleSummary.details = {
      header: 'Modules',
      list: moduleDetail
    };

    this.list.items.push(moduleSummary);
    let self = this;

    if (this.showAction) {
      let action = !moduleSummary.complete
        ? { action: { action: () => self.assessmentLogin() } }
        : null;

      this.list.actions.push(action);
    }

    this.list.styles.push({
      rowStyle: moduleSummary.complete ? 'table-success' : 'table-danger',
      isComplete: this.completedStyle(moduleSummary.complete),
      result: moduleSummary.complete
        ? RiskLevelStyles.hasOwnProperty(moduleSummary.result)
          ? RiskLevelStyles[moduleSummary.result]
          : ''
        : moduleSummary.percentageComplete > 80
          ? 'badge warning-color'
          : 'badge danger-color'
    });

  }

  private completedStyle(completed: boolean) {
    return completed
      ? 'fa fa-check green-text'
      : 'fa fa-times red-text'
  }

  private FormatDate(date: Date, nullValue?: string) {
    if (!date && nullValue) {
      return nullValue;
    }

    return FormatDate(date);
  }

  assessmentLogin() {
    return this.router.navigate(['./', Paths.RoadMarque], { relativeTo: this.route });
  }

  select(item: any) {
    if (this.user.role !== Roles.Driver && item.type === 'On Road Assessment') {
      return this.router.navigate(['./', Paths.Assessments, item._id], { relativeTo: this.route });
    }
  }

}
