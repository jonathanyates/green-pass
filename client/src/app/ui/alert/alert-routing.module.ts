import {NgModule} from "@angular/core";
import {CompanyComponent} from "../company/company.component";
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {Roles} from "../../../../../shared/constants";
import {RouterModule, Routes} from "@angular/router";
import {AlertListComponent} from "./alert-list/alert-list.component";
import {CompanyResolve} from "../company/company.resolve";
import {DriverAlertListComponent} from "./driver-alert-list/driver-alert-list.component";
import {VehicleAlertListComponent} from "./vehicle-alert-list/vehicle-alert-list.component";
import {CompanyAlertTypeListComponent} from "./company-alert-type-list/company-alert-type-list.component";
import {AdminComponent} from "../admin/admin.component";
import {AlertTypeInputComponent} from "./alert-type-input/alert-type-input.component";
import {AlertTypeListComponent} from "./alert-type-list/alert-type-list.component";
import {CompanyAlertListComponent} from "./company-alert-list/company-alert-list.component";
import {CompanyListResolve} from "../company/company-list/company-list.resolve";

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthorizationGuard],
    data: { roles: [Roles.Admin] },
    children: [
      { path: 'alerts', component: CompanyAlertListComponent, resolve: { companies: CompanyListResolve } },
      { path: 'alertTypes', component: AlertTypeListComponent, resolve: { companies: CompanyListResolve } },
      { path: 'alertTypes/new', component: AlertTypeInputComponent },
      { path: 'alertTypes/:alertTypeId', component: AlertTypeInputComponent }
    ]
  },
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/alerts', component: AlertListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/alerts/drivers', redirectTo: ':id/alerts' },
      { path: ':id/alerts/vehicles', redirectTo: ':id/alerts' },

      { path: ':id/alerts/drivers/:alertTypeId', component: DriverAlertListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/alerts/vehicles/:alertTypeId', component: VehicleAlertListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/alertTypes', component: CompanyAlertTypeListComponent, resolve: { company: CompanyResolve } },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AlertRoutingModule {
  constructor() {
  }
}
