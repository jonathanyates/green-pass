import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {IFileRecord} from "../../../../../../../shared/interfaces/common.interface";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";

@Component({
  selector: 'gp-driver-faw-list',
  template: `
    <gp-fileRecord-list [fileRecordHistory]="fawHistory" 
                        [fileRecordType]="'Faw'"
                        [allowAdd]="allowInput" 
                        [lightHeader]="lightHeader"
                        (add)="onAdd()" 
                        (delete)="onDelete($event)" 
                        (itemSelected)="onSelect($event)">      
    </gp-fileRecord-list>
  `
})

export class DriverFawListComponent {

  @Input() fawHistory: IFileRecord[];
  @Input() user : IUser;
  @Input() allowInput: boolean = false;
  @Input() lightHeader: boolean = false;
  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<IFileRecord> = new EventEmitter<IFileRecord>();
  @Output() select: EventEmitter<any> = new EventEmitter<any>();

  onAdd() {
    this.add.emit()
  }

  onSelect(faw: IFileRecord) {
    this.select.emit(faw);
  }

  onDelete(faw: IFileRecord) {
    this.delete.emit(faw);
  }

}
