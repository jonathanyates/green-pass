import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RedirectGuard} from "./security/guards/redirect.guard";
import {DynamicPageComponent} from "./dynamic/dynamic-page/dynamic-page.component";
import {DynamicPageResolve} from "./dynamic/dynamic-page/dynamic-page.resolve";
import {LoginComponent} from "./security/login/login.component";

const routes: Routes = [
  {
    path: '',
    canActivate: [RedirectGuard],
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }


  // {
  //   path: '',
  //   redirectTo: '/home',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'home',
  //   component: DynamicPageComponent,
  //   canActivate: [RedirectGuard],
  //   resolve: { page: DynamicPageResolve },
  //   pathMatch: 'full'
  // },
  // {
  //   path: '**',
  //   component: DynamicPageComponent,
  //   resolve: { page: DynamicPageResolve },
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    DynamicPageResolve
  ]
})
export class AppRoutingModule { }


