import mongoose from 'mongoose';
import { IAddressList } from '../../../shared/interfaces/address.interface';

// "Addresses":["Line1,Line2,Line3,Line4,Locality,Town/City,County"]

export const AddressSchema = new mongoose.Schema({
  line1: { type: String, required: false },
  line2: { type: String, required: false },
  line3: { type: String, required: false },
  line4: { type: String, required: false },
  locality: { type: String, required: false },
  townCity: { type: String, required: false },
  county: { type: String, required: false },
  postcode: { type: String, required: false },
  country: { type: String, required: false },
});

export const AddressListSchema = new mongoose.Schema({
  postcode: { type: String, required: true },
  addresses: [AddressSchema],
});

export interface IAddressModel extends mongoose.Document, IAddressList {}
