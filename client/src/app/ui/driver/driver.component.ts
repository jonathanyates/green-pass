import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {SelectDriver} from "../../store/drivers/driver.selectors";
import {Subscription} from "rxjs";
import {LoadDocumentsAction} from "../../store/documents/document.actions";

@Component({
  template: `
    <gp-breadcrumb></gp-breadcrumb>
    <router-outlet></router-outlet>
  `
})
export class DriverComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.store.let(SelectDriver())
        .filter(driver => driver != null)
        .subscribe(driver => {
          this.store.dispatch(new LoadDocumentsAction(driver));
        }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
