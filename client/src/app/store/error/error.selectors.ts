import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {ErrorState} from "./error.state";

export const SelectError = () => {
  return (store:Store<AppState>) => {
    return store.select<ErrorState>(state => state.error);
  }
};

export const SelectErrorMessage = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectError())
      .map(state => state.message)
      .distinctUntilChanged();
  }
};
