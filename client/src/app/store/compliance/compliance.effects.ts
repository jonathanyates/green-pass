import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./compliance.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {BusyAction} from "../progress/progress.actions";
import {ComplianceService} from "../../services/compliance.service";

@Injectable()
export class ComplianceEffects {

  constructor(
    private complianceService:ComplianceService,
    private actions$:Actions,
    private store: Store<AppState>) {
  }

  @Effect() loadCompliance$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.complianceService.getAll()
      .map(compliance => new actions.LoadComplianceSuccessAction(compliance))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectCompliance$ = this.actions$
    .ofType(actions.ActionTypes.SELECT)
    .map((action: actions.SelectComplianceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.complianceService.get(payload)
      .map(compliance =>  new actions.SelectComplianceSuccessAction(compliance))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );
}
