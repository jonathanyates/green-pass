import {Component, OnInit} from "@angular/core";
import {AuthorizationService} from "../../../services/authorization.service";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {BusyAction} from "../../../store/progress/progress.actions";

@Component({
  template: `
    <gp-change-password [title]="'Reset Password'" (submit)="onSubmit($event)" 
                        [message]="message" [error]="error"></gp-change-password>
  `
})
export class ResetPasswordComponent implements OnInit {

  message: string;
  error: string;
  token: string;

  constructor(
    private authService: AuthorizationService,
    private store: Store<AppState>,
    private route: ActivatedRoute,
    public router: Router) {
  }

  ngOnInit() {
    this.route.params.map((params: Params) => params['token'])
      .filter(token => token != null)
      .subscribe(token => this.token = token);
  }

  onSubmit(password: string) {
    this.store.dispatch(new BusyAction());

    this.authService.resetPassword(this.token, password)
      .subscribe(message => {
        this.store.dispatch(new BusyAction(false));
        this.message = message;
      },(err: Error) => {
        this.store.dispatch(new BusyAction(false));
        this.error = err.message;
      })
  }
}
