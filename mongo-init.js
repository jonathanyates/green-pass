print('Creating user greenpass-admin with a password in MongoDB');

db = db.getSiblingDB('green-pass');

db.createUser({
  user: 'greenpass-admin',
  pwd: 'everest99',
  roles: [
    {
      role: 'dbOwner',
      db: 'green-pass',
    },
  ],
});

print('User created');
