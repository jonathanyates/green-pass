import {Component, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {IsBusy} from "../../../../store/progress/progress.selectors";

@Component({
  selector: 'gp-progress-indicator',
  template: `
    <div *ngIf="busy" class="progress-overlay">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-primary-color-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
        </div>
      </div>
    </div>
`,
  styles: [ '.spinner-primary-color-only { border-color: #00a550; }' ]
})
export class ProgressIndicatorComponent implements OnInit {

  busy: boolean = false;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.let(IsBusy()).subscribe(busy => {
      this.busy = busy;
    });
  }
}
