import fs = require('fs');
import { injectable, inject } from 'inversify';
import { GridFSBucketReadStream, GridFSBucketWriteStreamOptions } from 'mongodb';
import { TYPES } from '../container/container.types';
import { IDbContext } from '../db/db.context.interface';

export interface IFileService {
  getFile(filename: string): Promise<GridFSBucketReadStream>;
  updateFile(filename: string, path: string, options: any): Promise<any>;
  createFile(filename: string, path: any, options: GridFSBucketWriteStreamOptions);
}

@injectable()
export class FileService implements IFileService {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  async getFile(filename: string): Promise<GridFSBucketReadStream> {
    const gfs = this.dbContext.gridFs;
    const file = await gfs.files.findOne({ filename });
    const gridfsBucket = this.dbContext.gridFSBucket;
    const readStream = gridfsBucket.openDownloadStream(file._id);
    return readStream;
  }

  async updateFile(filename: string, path: string, options: GridFSBucketWriteStreamOptions): Promise<any> {
    const gfs = this.dbContext.gridFs;
    const gridfsBucket = this.dbContext.gridFSBucket;

    let file = await gfs.files.findOne({ filename });

    if (file) {
      try {
        await gridfsBucket.delete(file._id);
      } catch (error) {
        console.log(`Error trying to remove file ${filename}. Error: ${error.message}`);
      }
    }

    file = await this.createFile(filename, path, options);
    return file;
  }

  createFile(filename: string, path: any, options: GridFSBucketWriteStreamOptions): Promise<any> {
    return new Promise((resolve, reject) => {
      const gridfsBucket = this.dbContext.gridFSBucket;
      const readStream = fs.createReadStream(path).pipe(gridfsBucket.openUploadStream(filename, options));

      readStream.on('error', function (err) {
        if (err) {
          return reject(err);
        }
      });

      readStream.on('close', (file) => {
        //delete file from temp folder
        fs.unlink(path, () => {
          console.log('temp upload file ' + path + ' removed.');
        });

        return resolve(file);
      });
    });
  }
}
