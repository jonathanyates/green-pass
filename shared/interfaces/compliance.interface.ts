//export interface ICompliance {
//  _id?: any,
//  _companyId?: any,
//  vehicle?: number,
//  driver?: number,
//}

export interface ITotalCompliance {
  compliant: number,
  company?: number,
  vehicle?: number,
  driver?: number
}

export interface ICompliance {
  _id?: any,
  _companyId?: any,
  compliant: number,
  company?: ICompanyCompliance
  vehicle?: ICompanyVehicleCompliance,
  driver?: ICompanyDriverCompliance
}

export interface ICompanyCompliance {
  insurance?: boolean
}

// Driver Compliance

export interface ICompanyDriverCompliance {
  compliant: number,
  details: number,
  nextOfKin: number,
  references: number,
  documents: number,
  licence: number,
  assessments: number,
  vehicles?: number,
  insurance?: number,
  faw?: number,
  dbs?: number
}

export interface IDriverDocumentCompliance {
  compliant: boolean,
  completed: number,
  total?: number,
  percentage: number, // 100%
}

export interface IDriverLicenceCompliance {
  compliant: boolean,
  checkRequired: boolean, // true
  valid?: boolean, // true
  points?: number
}

export interface IDriverAssessmentCompliance {
  compliant: boolean,
  onLine?: string, // Low ok, medium refer to modules, high refer to onRoad
  modules?: string, // Low or medium ok, high refer to onRoad
  onRoad?: boolean // must be completed if required
}

export interface IDriverCompliance {
  compliant: boolean,
  details: boolean,
  nextOfKin: boolean,
  references: boolean,
  documents: IDriverDocumentCompliance,
  drivingLicence: IDriverLicenceCompliance,
  assessments: IDriverAssessmentCompliance,
  vehicles?: boolean,
  insurance?: boolean,
  faw?: boolean,
  dbs?: boolean
}

// Vehicle Compliance

export interface ICompanyVehicleCompliance {
  compliant: number,
  tax: number,
  mot: number,
  service: number,
  inspection: number,
  checks: number
}

export interface IVehicleCompliance {
  compliant: boolean,
  tax: boolean,
  mot: boolean,
  service: boolean,
  inspection: boolean,
  vehicleCheck: boolean
}