import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./vehicle.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {VehicleService} from "../../services/vehicle.service";
import {LoadCompanyAction} from "../company/company.actions";
import {SelectVehicles, SelectVehicle} from "./vehicle.selectors";
import {LoadVehiclesAction, SelectVehicleSuccessAction} from "./vehicle.actions";
import {SelectVehiclesSuccessAction} from "./vehicle.actions";
import {LoadVehicleAction} from "./vehicle.actions";
import {LoadVehicleSuccessAction} from "./vehicle.actions";
import {BusyAction} from "../progress/progress.actions";
import {FileService} from "../../services/file.service";
import * as VehicleServiceActions from './vehicle-service.actions';
import * as VehicleInspectionActions from './vehicle-inspection.actions';
import * as VehicleCheckActions from './vehicle-check.actions';
import {messenger} from "../../services/messenger";
import {Messages} from "../app.messages";
import {Location} from "@angular/common";

@Injectable()
export class VehicleEffects {

  constructor(private vehicleService: VehicleService,
              private fileService: FileService,
              private actions$: Actions,
              private location: Location,
              private store: Store<AppState>) {
  }

  @Effect() selectVehicles$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_LIST)
    .map((action: actions.SelectVehiclesAction) => action.payload)
    .switchMap(payload => this.store.let(SelectVehicles()).take(1)
      .map(vehicles => vehicles && vehicles.every(vehicle => vehicle._companyId === payload)
        ? new SelectVehiclesSuccessAction(vehicles)
        : new LoadVehiclesAction(payload))
    );

  @Effect() loadVehicles$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap((action:LoadVehiclesAction) => this.vehicleService.getVehicles(action.payload, action.page, action.pageSize, action.searchCriteria)
      .map(result => new actions.LoadVehiclesSuccessAction(result.items, result.total, result.page))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectVehicle$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_VEHICLE)
    .map((action: actions.SelectVehicleAction) => action.payload)
    .switchMap(payload => this.store.let(SelectVehicle()).take(1)
      .map(vehicle => vehicle && vehicle._id === payload.vehicleId
        ? new SelectVehicleSuccessAction(vehicle)
        : new LoadVehicleAction(payload.companyId, payload.vehicleId))
    );

  @Effect() loadVehicle$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_VEHICLE)
    .map((action: actions.LoadVehicleAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.vehicleService.getVehicle(payload.companyId, payload.vehicleId)
      .map(vehicle => new LoadVehicleSuccessAction(vehicle))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveVehicle$ = this.actions$
    .ofType(actions.ActionTypes.SAVE)
    .map((action: actions.SaveVehicleAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(vehicle => this.vehicleService.saveVehicle(vehicle)
      .do(vehicle => this.location.back())
      .map(vehicle => new actions.SaveVehicleSuccessAction(vehicle))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        messenger.publish(Messages.SaveError, error);
        return Observable.empty();
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveVehicleSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_SUCCESS)
    .map((action: actions.SaveVehicleSuccessAction) => action.payload)
    .do(vehicle => this.store.dispatch(new LoadCompanyAction(vehicle._companyId)))
    .map(vehicle => new LoadVehiclesAction(vehicle._companyId));

  // Vehicle Service

  @Effect() selectVehicleService$ = this.actions$
    .ofType(VehicleServiceActions.ActionTypes.SELECT_VEHICLE_SERVICE)
    .map((action: VehicleServiceActions.SelectVehicleServiceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectVehicle())
      .filter(vehicle => vehicle && vehicle._id === payload.vehicleId).take(1)
      .switchMap(vehicle => {
        let service = vehicle.serviceHistory.find(service => service._id === payload.serviceId);
        if (service == null || service.file || service.filename == null) {
          return Observable.of(new VehicleServiceActions.SelectVehicleServiceSuccessAction(service))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(vehicle._companyId, service.filename)
          .map(file => Object.assign({}, service, { file: file }))
          .map(service => new VehicleServiceActions.SelectVehicleServiceSuccessAction(service))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveVehicleService$ = this.actions$
    .ofType(VehicleServiceActions.ActionTypes.SAVE_VEHICLE_SERVICE)
    .map((action: VehicleServiceActions.SaveVehicleServiceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.vehicleService.saveVehicleService(payload.vehicle, payload.service)
      .do(service => this.location.back())
      .map(service => new VehicleServiceActions.SaveVehicleServiceSuccessAction(payload.vehicle, service))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // Vehicle Inspection

  @Effect() selectVehicleInspection$ = this.actions$
    .ofType(VehicleInspectionActions.ActionTypes.SELECT_VEHICLE_INSPECTION)
    .map((action: VehicleInspectionActions.SelectVehicleInspectionAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectVehicle())
      .filter(vehicle => vehicle && vehicle._id === payload.vehicleId).take(1)
      .switchMap(vehicle => {
        let inspection = vehicle.inspectionHistory.find(inspection => inspection._id === payload.inspectionId);
        if (inspection == null || inspection.file || inspection.filename == null) {
          return Observable.of(new VehicleInspectionActions.SelectVehicleInspectionSuccessAction(inspection))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(vehicle._companyId, inspection.filename)
          .map(file => Object.assign({}, inspection, { file: file }))
          .map(inspection => new VehicleInspectionActions.SelectVehicleInspectionSuccessAction(inspection))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveVehicleInspection$ = this.actions$
    .ofType(VehicleInspectionActions.ActionTypes.SAVE_VEHICLE_INSPECTION)
    .map((action: VehicleInspectionActions.SaveVehicleInspectionAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.vehicleService.saveVehicleInspection(payload.vehicle, payload.inspection)
      .do(inspection =>  this.location.back())
      .map(inspection => new VehicleInspectionActions.SaveVehicleInspectionSuccessAction(payload.vehicle, inspection))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  // Vehicle Check

  @Effect() selectVehicleCheck$ = this.actions$
    .ofType(VehicleCheckActions.ActionTypes.SELECT_VEHICLE_CHECK)
    .map((action: VehicleCheckActions.SelectVehicleCheckAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectVehicle())
      .filter(vehicle => vehicle && vehicle._id === payload.vehicleId).take(1)
      .switchMap(vehicle => {
        let check = vehicle.checkHistory.find(check => check._id === payload.checkId);
        if (check == null || check.file || check.filename == null) {
          return Observable.of(new VehicleCheckActions.SelectVehicleCheckSuccessAction(check))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(vehicle._companyId, check.filename)
          .map(file => Object.assign({}, check, { file: file }))
          .map(check => new VehicleCheckActions.SelectVehicleCheckSuccessAction(check))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveVehicleCheck$ = this.actions$
    .ofType(VehicleCheckActions.ActionTypes.SAVE_VEHICLE_CHECK)
    .map((action: VehicleCheckActions.SaveVehicleCheckAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.vehicleService.saveVehicleCheck(payload.vehicle, payload.check)
      .do(check =>  this.location.back())
      .map(check => new VehicleCheckActions.SaveVehicleCheckSuccessAction(payload.vehicle, check))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

}
