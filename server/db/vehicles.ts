import serverConfig from '../src/server/server.config';

export const vehicles = [
  // Cars
  { reg: 'MW71VYA', make: '' },
  { reg: 'S2 SBY', make: '' },
  { reg: 'E18 EBY', make: '' },

  // HGVs
  // { reg: 'N6EPL', make: '' },
  // { reg: 'N7EPL', make: '' },
  // { reg: 'BF11VMR', make: '' },
  // { reg: 'YS65ETV', make: '' },

  // Test vehicles
  // { reg: 'AA19AAA', make: '' },
  // { reg: 'AA19EEE', make: '' },
  // { reg: 'AA19PPP', make: '' },
  // { reg: 'L2WPS', make: '' },
  // { reg: 'AA19SRN', make: '' },
  // { reg: 'AA19DSL', make: '' },
  // { reg: 'AA19MOT', make: '' },
  // { reg: 'AA19IMP', make: '' },
];
serverConfig;
