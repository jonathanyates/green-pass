import {Handler} from "express-serve-static-core";

export interface IAuthenticationMiddleware {
  requireAuth: Handler
}