import {FormInput, FormInputGroup} from "../../dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {FormatDateForInput, GetDate} from "../../../../../../../../shared/utils/helpers";
import {Observable} from "rxjs";
import {IFileRecord} from "../../../../../../../../shared/interfaces/common.interface";

@Injectable()
export class FileRecordInputMapper {

  mapFrom(form:FormGroup, inputGroups: FormInputGroup[], fileRecord: IFileRecord): IFileRecord {

    let input = <FormInput>inputGroups[0].inputs.find((input:FormInput) => input.id === 'file');
    let fileChanged = input.file && input.file !== fileRecord.file;

    return {
      validFrom: GetDate(form.controls['validFrom'].value),
      validTo: GetDate(form.controls['validTo'].value),
      file: fileChanged ? input.file : null,
      fileType: input.fileType
    };
  }

  mapTo(fileRecord: IFileRecord, heading: string, fileHeading: string): FormInputGroup[] {

    let fileRecordFileInput = <FormInput>{ id: 'file', label: fileHeading, type:'file', required: true, value: '' };

    if (fileRecord.file) {
      fileRecordFileInput.file = fileRecord.file;
      fileRecordFileInput.fileType = fileRecord.fileType;
      fileRecordFileInput.value = fileRecord.filename;
    }

    fileRecordFileInput.action = file => {
      fileRecordFileInput.file = file;
      fileRecordFileInput.fileType = file.fileType;
      return Observable.empty();
    };

    return [
      {
        label: heading,
        inputs: [
          { id: 'validFrom', label: ' Valid From', type:'date', required: true, autoFocus: true,
            value: FormatDateForInput(fileRecord.validFrom) },
          { id: 'validTo', label: 'Valid To', type:'date', required: true, autoFocus: false,
            value: FormatDateForInput(fileRecord.validTo) },
          fileRecordFileInput
        ]
      }
    ];
  }

}
