import "reflect-metadata";
import {AddressService} from "../../src/address/address.service";
import {AddressRepository} from "../../src/address/address.repository";
import {DbContext} from "../../src/db/db.context";

const addressService = new AddressService(new AddressRepository(new DbContext()));

addressService.addressSearch('nn13er')
  .then(result => {
    console.log(result);
    process.exit();
  })
  .catch(error =>{
    console.log(error);
    process.exit(1);
  });