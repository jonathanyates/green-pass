import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Driver} from "../driver.model";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {SaveDriverAction} from "../../../store/drivers/driver.actions";
import { FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {DynamicFormComponent} from "../../shared/components/dynamic-form/dynamic-form.component";
import {NextOfKinInputMapper} from "./driver-next-of-kin-input.mapper";
import {jsonDeserialiser} from "../../../../../../shared/utils/json.deserialiser";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-next-of-kin-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" 
                     (cancel)="cancel()" #dynamicForm></gp-dynamic-form>
  `
})
export class DriverNextOfKinInputComponent implements OnInit, OnDestroy {

  user: IUser;
  inputGroups: FormInputGroup[];
  private driver: Driver;
  private subscriptions = [];
  @ViewChild('dynamicForm') dynamicForm: DynamicFormComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<AppState>,
              private nextOfKinInputMapper: NextOfKinInputMapper) {
  }

  ngOnInit() {

    this.subscriptions.push(this.store.let(SelectDriver())
      .filter(driver => driver != null)
      .subscribe(driver => {
        this.driver = driver;
        this.inputGroups = this.nextOfKinInputMapper.mapTo(this.driver);
      }));

    this.subscriptions.push(this.store.let(SelectLoggedInUser())
      .filter(user => user != null)
      .subscribe(user => this.user = user));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  save(form: FormGroup) {
    if (!form.controls) return;
    let nextOfKin = this.nextOfKinInputMapper.mapFrom(form, this.driver);
    let copy = jsonDeserialiser.parse(JSON.stringify(this.driver));

    let driver = Object.assign(copy, { nextOfKin: nextOfKin });

    this.store.dispatch(new SaveDriverAction(driver, Paths.NextOfKin));
  }

  cancel() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
