import {Component, Input, Output, EventEmitter} from '@angular/core';
import {IVehicle} from '../../../../../../shared/interfaces/vehicle.interface';
import {FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {VehicleInputMapper} from './vehicle-input.mapper';
import {FormGroup} from '@angular/forms';
import {IDriver} from '../../../../../../shared/interfaces/driver.interface';

@Component({
  selector: 'gp-vehicle-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="onSave($event)" (cancel)="onCancel()"></gp-dynamic-form>
  `
})
export class VehicleInputComponent {

  error: string;
  inputGroups: FormInputGroup[];

  private _driver;
  @Input() set driver (driver: IDriver) {
    this._driver = driver;
  }
  get driver() {
    return this._driver;
  }

  private _vehicle: IVehicle;
  @Input() set vehicle (vehicle: IVehicle) {
    this._vehicle = vehicle;
    this.inputGroups = VehicleInputMapper.mapTo(vehicle, this._driver);
  }
  get vehicle() {
    return this._vehicle;
  }

  @Input() company: any;
  @Output() save: EventEmitter<IVehicle> = new EventEmitter<IVehicle>();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  onSave(form: FormGroup) {
    if (!form.controls) {
      return;
    }

    const vehicle = Object.assign({}, this.vehicle, VehicleInputMapper.mapFrom(form, this._driver));
    vehicle._companyId = this.company._id;
    this.save.emit(vehicle);
  }

  onCancel() {
    this.cancel.emit();
  }
}
