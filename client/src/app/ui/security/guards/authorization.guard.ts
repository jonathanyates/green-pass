import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {AuthenticationState} from "../../../store/authentication/authentication.reducer";
import {SelectAuthenticationState} from "../../../store/authentication/authentication.selectors";
import {Roles} from "../../../../../../shared/constants";
import {SetRedirectUrlAction} from "../../../store/authentication/authentication.actions";

@Injectable()
export class AuthorizationGuard implements CanActivate, CanActivateChild {

  private authState:AuthenticationState;

  constructor(
    private store: Store<AppState>,
    private router: Router) {
    this.store.let(SelectAuthenticationState())
      .subscribe(authState=> this.authState = authState);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (!this.authState.loggedIn) {
      console.log(state.url);
      this.store.dispatch(new SetRedirectUrlAction(state.url));
      this.router.navigate(['/login']); // if not logged in then redirect to login page
      return false;
    }

    let user = this.authState.user;
    let roles = route.data['roles'];

    // redirect user to change password if required
    if (user.changePassword) {
      this.store.dispatch(new SetRedirectUrlAction(state.url));
      this.router.navigate(['/changePassword']);
      return false;
    }

    if (!roles || roles.indexOf(user.role) > -1) {
      return true; // we're ok your have access to this path
    } else {
      if (user.role == Roles.Company && user._companyId) {
        this.router.navigate([user.role, user._companyId]);
      } else {
        this.router.navigate([user.role]);
      }
      return false;
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

}
