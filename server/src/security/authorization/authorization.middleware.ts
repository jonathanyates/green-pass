import ConnectRoles = require('connect-roles');
import {inject, injectable} from "inversify";
import {IAuthorizationMiddleware} from "./authorization.middleware.interface";
import codes from "../../shared/error.codes";
import {ICompanyService} from "../../companies/company.service.interface";
import {TYPES} from "../../container/container.types";

export interface IRoles {
  admin: string,
  company: string,
  driver: string,
  companyAdmin: string,
  any: string
}

export const roles: IRoles = {
  admin: 'admin',
  company: 'company',
  driver: 'driver',
  companyAdmin: 'company admin',
  any: 'company admin driver'
};

@injectable()
export class AuthorizationMiddleware implements IAuthorizationMiddleware {

  user: ConnectRoles;
  roles: IRoles = roles;

  constructor(@inject(TYPES.services.CompanyService) private companyService: ICompanyService) {
    this.configure();
  }

  configure() {

    const user = new ConnectRoles({
      failureHandler: function (req, res, action) {
        res.status(codes.FORBIDDEN);
        res.send('Access Denied - You don\'t have permission to: ' + action);
      }
    });

    user.use(roles.driver, function (req) {
      if (req.user.role === roles.driver) {
        return true;
      }
    });

    user.use(roles.company, function (req) {
      if (req.user.role === roles.company) {
        const id = req.params.id;
        const userCompanyId = req.user._companyId;
        if (id && userCompanyId && !userCompanyId.equals(id)) {
          return false;
        }
        return true;
      }
    });

    user.use(roles.companyAdmin, function (req) {
      if (req.user.role === roles.admin) {
        return true;
      } else if (req.user.role === roles.company ) {
        const id = req.params.id;
        const userCompanyId = req.user._companyId;
        if (id && userCompanyId && !userCompanyId.equals(id)) {
          return false;
        }
        return true;
      }
    });

    user.use(roles.any, function (req) {
      if (req.user.role === roles.admin || req.user.role === roles.company || req.user.role === roles.driver) {
        return true;
      }
    });

    user.use(function (req) {
      if (req.user.role === roles.admin) {
        return true;
      }
    });

    this.user = user;
    this.roles = roles;
  }
}
