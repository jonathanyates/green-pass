import logger = require('winston');
import request = require('request-promise-native');
import { inject, injectable } from 'inversify';
import { IDocumentSignatureService, IEmbeddedUrl } from '../interfaces/signature.service.interface';
import { IDocumentStatus, IDocument, IRecipient, ITemplate } from '../../../../shared/interfaces/document.interface';
import { ServerError } from '../../shared/errors';
import codes from '../../shared/error.codes';
import { jsonDeserialiser } from '../../../../shared/utils/json.deserialiser';
import { TYPES } from '../../container/container.types';
import { IRepository } from '../../db/db.repository';
import { getHeaderHtml, getSignatureHtml, getStyles, getStylesHtml } from './document-html';
import { IFileService } from '../../files/file.service';
import serverConfig from '../../server/server.config';

const statusCodes = {
  10: 'Sent',
  20: 'Fields completed',
  30: 'Signed',
  40: 'Removed (before signing)',
  50: 'Rejected',
};

interface ILegalesignDocument {
  group: string;
  name: string;
  text: string;
  signers: [
    {
      firstname: string;
      lastname: string;
      email: string;
      order: number;
    }
  ];
  do_email: boolean;
  return_signer_links: boolean;
  header?: string;
  header_height?: number;
  footer?: string;
  footer_height?: number;
  user?: string;
}

@injectable()
export class LegalesignService implements IDocumentSignatureService {
  private apiKey = serverConfig.legalesign.apiKey;
  private baseUrl = serverConfig.legalesign.baseUrl;
  private group = serverConfig.legalesign.group;

  constructor(
    @inject(TYPES.repositories.TemplateRepository) private templateRepository: IRepository<ITemplate>,
    @inject(TYPES.services.FileService) private fileService: IFileService
  ) {}

  private getAuthHeader = () => {
    return {
      Authorization: this.apiKey,
      'Content-Type': 'application/json',
    };
  };

  getTemplates(): Promise<ITemplate[]> {
    return this.templateRepository.getAll();
  }

  createTemplates(directory: string) {
    return;
  }

  createDocument(template: ITemplate, recipient: IRecipient, company: string): Promise<IDocument> {
    const signer = `${recipient.firstName} ${recipient.lastName}`;
    const html = [
      getStyles(),
      getStylesHtml(),
      getHeaderHtml(company, signer, template.name),
      template.html,
      getSignatureHtml(signer),
    ].join('\n<br><br>');

    const document = <ILegalesignDocument>{
      group: this.group,
      name: template.name,
      text: html,
      signers: [
        {
          firstname: recipient.firstName,
          lastname: recipient.lastName,
          email: recipient.email,
          order: 0,
        },
      ],
      do_email: false,
      auto_archive: false,
      return_signer_links: true,
      user: serverConfig.legalesign.user,
    };

    return this.send(document, [template]);
  }

  createCompositeDocument(
    templates: ITemplate[],
    documentName: string,
    recipient: IRecipient,
    company?: string
  ): Promise<IDocument> {
    if (!templates || templates.length === 0) {
      return Promise.resolve(null);
    }

    const signer = `${recipient.firstName} ${recipient.lastName}`;
    let html = [
      getStyles(),
      getStylesHtml(),
      getHeaderHtml(company, signer, documentName),
      ...templates.sort((a, b) => a.folderName.localeCompare(b.folderName)).map((template) => template.html),
      getSignatureHtml(signer),
    ].join('\n<br><br>');

    html = html.replace(/API_ROOT/g, serverConfig.server.apiRoot);

    const document = <ILegalesignDocument>{
      group: this.group,
      name: documentName,
      text: html,
      signers: [
        {
          firstname: recipient.firstName,
          lastname: recipient.lastName,
          email: recipient.email,
          order: 0,
        },
      ],
      do_email: false,
      auto_archive: false,
      return_signer_links: true,
      header: '',
      header_height: 0,
      footer: '',
      footer_height: 0,
      user: serverConfig.legalesign.user,
    };

    return this.send(document, templates);
  }

  getDocumentStatus(documentIds: string[]): Promise<IDocumentStatus[]> {
    if (!documentIds || documentIds.length === 0) {
      return Promise.resolve([]);
    }

    return Promise.all(
      documentIds.map((documentId) => {
        return request
          .get({
            url: `${this.baseUrl}document/${documentId}/`,
            headers: this.getAuthHeader(),
          })
          .then((result) => {
            const document = jsonDeserialiser.parse(result);
            return <IDocumentStatus>{
              documentId: documentId,
              name: document.name,
              status: statusCodes[document.status],
              statusChangedDateTime: document.modified,
              signed: document.status === 30,
            };
          })
          .catch((error) => {
            logger.error(`Failed to get Document Status.\n${error}`);
            return null;
          });
      })
    );
  }

  deleteDocuments(documentIds: string[]): Promise<IDocument[]> {
    if (!documentIds || documentIds.length === 0) {
      return Promise.resolve([]);
    }

    return Promise.all(
      documentIds.map((documentId) => {
        return request
          .del({
            url: `${this.baseUrl}document/${documentId}/`,
            headers: this.getAuthHeader(),
          })
          .then((result) => documentId)
          .catch((error) => {
            logger.error(`Failed to delete document ${documentId}.\n${error}`);
            return null;
          });
      })
    ).catch((error) => {
      logger.error(`Failed to delete document.\n${error}`);
      return null;
    });
  }

  requestEmbeddedUrl(documentId: string, recipient: IRecipient, returnUrl: string): Promise<string> {
    return null;
  }

  getDocumentPdf(documentId: string): Promise<any> {
    return request
      .get({
        url: `${this.baseUrl}pdf/${documentId}/`,
        headers: {
          Authorization: this.apiKey,
          'Content-Type': 'application/pdf',
        },
        encoding: null,
      })
      .then((data) => {
        return data;
      })
      .catch((error) => {
        logger.error(`Failed to get document Pdf Data.\n${error}`);
        return null;
      });
  }

  getNewEmbeddedUrl(signerId: string): Promise<IEmbeddedUrl> {
    return request
      .get({
        url: `${this.baseUrl}signer/${signerId}/new-link/`,
        headers: this.getAuthHeader(),
        resolveWithFullResponse: true,
      })
      .then((response) => {
        if (response && response.headers && response.headers.location) {
          return {
            url: response.headers.location,
          };
        }
        return null;
      })
      .catch((error) => {
        logger.error(`Failed to get document Pdf Data.\n${error}`);
        return null;
      });
  }

  private send(document: ILegalesignDocument, templates: ITemplate[]): Promise<IDocument> {
    return request
      .post({
        url: `${this.baseUrl}document/`,
        headers: this.getAuthHeader(),
        json: document,
        resolveWithFullResponse: true,
      })
      .then((response) => {
        const url: string = response.headers.location;
        const documentId = url ? url.slice(0, -1).split('/').slice(-1)[0] : null;
        if (!documentId) {
          throw ServerError('Unable to determine id of document sent', codes.INTERNAL_SERVER_ERROR);
        }

        const embeddingUri = response.body ? response.body.signer_0 : null;

        return this.getDocument(documentId, templates, embeddingUri);
      })
      .catch((error) => {
        logger.error(`Failed to send Document.\n${error}`);
        throw ServerError(error.message, error.statusCode);
      });
  }

  private getDocument = (documentId: string, templates: ITemplate[], embeddingUri: string) => {
    return request
      .get({
        url: `${this.baseUrl}document/${documentId}/`,
        headers: this.getAuthHeader(),
      })
      .then((result) => {
        const document = jsonDeserialiser.parse(result);
        return <IDocument>{
          documentId: documentId,
          templates: templates.map((template) => template._id),
          name: document.name,
          status: statusCodes[document.status],
          statusDateTime: document.modified,
          uri: document.resource_uri,
          embeddingUri: embeddingUri,
          signed: document.status === 30,
        };
      })
      .catch((error) => {
        logger.error(`Failed to get Document.\n${error}`);
        throw ServerError(error.message, error.statusCode);
      });
  };
}
