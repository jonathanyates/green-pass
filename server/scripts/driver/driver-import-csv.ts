import { IDriverService } from '../../src/drivers/driver.service';
import container from '../../src/container/container.config';
import { TYPES } from '../../src/container/container.types';
import { IDriver } from '../../../shared/interfaces/driver.interface';
import csv from 'csvtojson';

const csvFilePath = '/Users/jonathanyates/Projects/green-pass/server/scripts/driver/drivers.csv';

const driverService = container.get<IDriverService>(TYPES.services.DriverService);
const companyId = '5a4e01e6546010000f0fcda9';

//
// driverService.getAll(companyId)
//   .then(drivers => {
//     return Promise.all(drivers.map(driver => {
//       driver.ownedVehicle = true;
//       return driverService.update(driver);
//     }));
//   })
//   .then(drivers => {
//     console.log(`updated ${drivers.length} drivers`);
//     process.exit();
//   })
//   .catch(error => {
//     console.log(error);
//   });

csv({ ignoreEmpty: true })
  .fromFile(csvFilePath)
  .on('json', (jsonObj) => {
    console.log(JSON.stringify(jsonObj));
  })
  .on('end_parsed', (jsonArrObj) => {
    return processAll(jsonArrObj);
  })
  .on('error', (err) => {
    console.log(err);
  })
  .on('done', (error) => {
    console.log('end');
  });

function processAll(infos: Array<any>) {
  return Promise.all(infos.slice(0, 10).map((info) => addDriver(info))).then((drivers) => {
    console.log('Completed Adding drivers.');
    process.exit(0);
  });
}

function addDriver(info) {
  const driver: IDriver = {
    _id: info._id,
    _user: {
      title: null,
      forename: info.forename,
      surname: info.surname,
      email: info.email,
      role: 'driver',
      username: null,
      password: null,
    },
    gender: null,
    licenceNumber: null,
    niNumber: null,
    phone: null,
    mobile: null,
    address: null,
    dateOfBirth: null,
    licence: {
      checks: [],
    },
    assessments: [],
    ownedVehicle: true,
  }; // driver

  return driverService.add(companyId, driver).catch((error) => {
    const driver = info;
    console.error('Error driver:');
    console.error(driver);
    console.error(error);
    return null;
  });
}
