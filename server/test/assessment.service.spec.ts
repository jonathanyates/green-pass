import 'reflect-metadata';
import { TYPES } from '../src/container/container.types';
import { DriverService, IDriverService } from '../src/drivers/driver.service';
import container from '../src/container/container.config';
import { AssessmentService, IAssessmentService } from '../src/assessments/assessment.service';
import { assessmentUnknown } from './mocks/assessments/assessment-Unknown';
import { IRoadMarqueService, RoadMarqueService } from '../src/assessments/roadmarque.service';
import { assessmentLow } from './mocks/assessments/assessment-low';
import { RiskLevel } from '../../shared/models/driver-compliance.model';
import { DriverRepository, IDriverRepository } from '../src/drivers/driver.repository';
import { IDriverModel } from '../src/drivers/driver.schema';

describe.skip('Driver Service', () => {
  describe.skip('updateAssessments', () => {
    it('expect Driver Assessments to complete with Low Risk Level', async () => {
      const driverService = <DriverService>container.get<IDriverService>(TYPES.services.DriverService);
      const roadMarqueService = <RoadMarqueService>container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);
      roadMarqueService.getDrivers = (searchFields) => Promise.resolve(<any>assessmentUnknown);
      const successResult = Promise.resolve(<any>{ resultMessage: { success: 'true' } });
      roadMarqueService.updateDriver = (userId) => successResult;
      roadMarqueService.addDriver = (driver) => successResult;
      roadMarqueService.AssessmentInvite = (refId) => successResult;

      const driverRepository = <DriverRepository>container.get<IDriverRepository>(TYPES.repositories.DriverRepository);
      driverRepository.update = (driver: IDriverModel): Promise<IDriverModel> => {
        return Promise.resolve(driver);
      };

      const assessmentService = <AssessmentService>container.get<IAssessmentService>(TYPES.services.AssessmentService);

      let driver = await driverService.findOne({ licenceNumber: 'YATES607198JP9LK' });
      driver = await assessmentService.updateAssessments(driver);

      expect(driver).not.toBeNull;
      expect(driver.assessments).not.toBeNull;
      expect(driver.assessments?.length).toEqual(1);
      expect(driver.assessments![0].complete).toBeFalsy();
      expect(driver.assessments![0].onLine).not.toBeNull();
      expect(driver.assessments![0].onLine?.overallRiskLevel).toEqual(RiskLevel.Unknown);
      expect(driver.assessments![0].onLine?.tests.length).toEqual(1);
      expect(driver.assessments![0].onLine?.tests[0].complete).toBeFalsy();
      expect(driver.assessments![0].trainingModules).toBeUndefined();
      expect(driver.assessments![0].onRoad).toBeUndefined();

      roadMarqueService.getDrivers = (searchFields) => {
        return Promise.resolve(<any>assessmentLow);
      };

      return assessmentService.updateAssessments(driver);
    });

    // it('expect Driver Assessments with Average Risk Level to have Modules', () => {
    //   let driverService = <DriverService>container.get<IDriverService>(TYPES.services.DriverService);
    //   let roadMarqueService = <RoadMarqueService>container.get<IRoadMarqueService>(TYPES.services.RoadMarqueService);
    //   roadMarqueService.getDrivers = (searchFields) => Promise.resolve(<any>assessmentUnknown);
    //   let successResult = Promise.resolve(<any>{ resultMessage: { success: 'true' } });
    //   roadMarqueService.updateDriver = (userId) => successResult;
    //   roadMarqueService.addDriver = (driver) => successResult;
    //   roadMarqueService.AssessmentInvite = (refId) => successResult;

    //   let driverRepository = <DriverRepository>container.get<IDriverRepository>(TYPES.repositories.DriverRepository);
    //   driverRepository.update = (driver: IDriverModel): Promise<IDriverModel> => {
    //     return Promise.resolve(driver);
    //   };

    //   let assessmentService = <AssessmentService>container.get<IAssessmentService>(TYPES.services.AssessmentService);

    //   return driverService
    //     .findOne({ licenceNumber: 'YATES607198JP9LK' })
    //     .then((driver: IDriver) => assessmentService.updateAssessments(driver))
    //     .then((driver) => {
    //       expect(driver).to.not.be.null;
    //       expect(driver.assessments).to.not.be.null;
    //       expect(driver.assessments.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine).to.not.be.null;
    //       expect(driver.assessments[0].onLine.overallRiskLevel).to.equal(RiskLevel.Unknown);
    //       expect(driver.assessments[0].onLine.tests.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine.tests[0].complete).to.be.false;
    //       expect(driver.assessments[0].trainingModules).to.be.undefined;
    //       expect(driver.assessments[0].onRoad).to.be.undefined;

    //       roadMarqueService.getDrivers = (searchFields) => Promise.resolve(<any>assessmentAverage);
    //       return assessmentService.updateAssessments(driver);
    //     })
    //     .then((driver) => {
    //       expect(driver).to.not.be.null;
    //       expect(driver.assessments).to.not.be.null;
    //       expect(driver.assessments.length).to.be.equal(1);
    //       expect(driver.assessments[0].complete).to.be.false;
    //       expect(driver.assessments[0].onLine).to.not.be.null;
    //       expect(driver.assessments[0].onLine.tests.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine.overallRiskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].onLine.tests[0].complete).to.be.true;
    //       expect(driver.assessments[0].onLine.tests[0].riskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].trainingModules).to.not.be.undefined;
    //       expect(driver.assessments[0].trainingModules.length).to.equal(3);
    //       expect(driver.assessments[0].onRoad).to.be.undefined;

    //       assessmentAverage.trainingModules.intervention.forEach((module) => {
    //         module.startDate = new Date();
    //         module.completedDate = new Date();
    //         module.riskLevel = RiskLevel.Average;
    //       });

    //       roadMarqueService.getDrivers = (searchFields) => Promise.resolve(<any>assessmentAverage);
    //       return assessmentService.updateAssessments(driver);
    //     })
    //     .then((driver) => {
    //       expect(driver).to.not.be.null;
    //       expect(driver.assessments).to.not.be.null;
    //       expect(driver.assessments.length).to.be.equal(1);
    //       expect(driver.assessments[0].complete).to.be.true;
    //       expect(driver.assessments[0].onLine).to.not.be.null;
    //       expect(driver.assessments[0].onLine.tests.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine.overallRiskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].onLine.tests[0].complete).to.be.true;
    //       expect(driver.assessments[0].onLine.tests[0].riskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].trainingModules).to.not.be.undefined;
    //       expect(driver.assessments[0].trainingModules.length).to.equal(3);
    //       driver.assessments[0].trainingModules.forEach((module) => {
    //         expect(module.completedDate).to.not.be.null;
    //         expect(module.riskLevel).to.equal(RiskLevel.Average);
    //       });
    //       expect(driver.assessments[0].onRoad).to.be.undefined;

    //       assessmentAverage.trainingModules.intervention.forEach((module) => {
    //         module.startDate = new Date();
    //         module.completedDate = new Date();
    //         module.riskLevel = RiskLevel.High;
    //       });

    //       roadMarqueService.getDrivers = (searchFields) => Promise.resolve(<any>assessmentAverage);
    //       return assessmentService.updateAssessments(driver);
    //     })
    //     .then((driver) => {
    //       expect(driver).to.not.be.null;
    //       expect(driver.assessments).to.not.be.null;
    //       expect(driver.assessments.length).to.be.equal(1);
    //       expect(driver.assessments[0].complete).to.be.false;
    //       expect(driver.assessments[0].onRoad).to.not.be.undefined;
    //       expect(driver.assessments[0].onRoad.complete).to.be.false;
    //       expect(driver.assessments[0].onLine).to.not.be.null;
    //       expect(driver.assessments[0].onLine.tests.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine.overallRiskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].onLine.tests[0].complete).to.be.true;
    //       expect(driver.assessments[0].onLine.tests[0].riskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].trainingModules).to.not.be.undefined;
    //       expect(driver.assessments[0].trainingModules.length).to.equal(3);

    //       driver.assessments[0].onRoad.assessmentDate = new Date();
    //       driver.assessments[0].onRoad.complete = true;

    //       return assessmentService.updateAssessments(driver);
    //     })
    //     .then((driver) => {
    //       expect(driver).to.not.be.null;
    //       expect(driver.assessments).to.not.be.null;
    //       expect(driver.assessments.length).to.be.equal(1);
    //       expect(driver.assessments[0].complete).to.be.true;
    //       expect(driver.assessments[0].onRoad).to.not.be.undefined;
    //       expect(driver.assessments[0].onRoad.complete).to.be.true;
    //       expect(driver.assessments[0].onLine).to.not.be.null;
    //       expect(driver.assessments[0].onLine.tests.length).to.be.equal(1);
    //       expect(driver.assessments[0].onLine.overallRiskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].onLine.tests[0].complete).to.be.true;
    //       expect(driver.assessments[0].onLine.tests[0].riskLevel).to.equal(RiskLevel.Average);
    //       expect(driver.assessments[0].trainingModules).to.not.be.undefined;
    //       expect(driver.assessments[0].trainingModules.length).to.equal(3);
    //     });
    // });
  });
});
