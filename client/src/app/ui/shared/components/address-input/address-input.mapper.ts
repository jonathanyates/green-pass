import {FormGroup} from "@angular/forms";
import {IAddress} from "../../../../../../../shared/interfaces/address.interface";
import {FormInputGroup, FormInput} from "../dynamic-form/dynamic-form.model";
import {AddressService} from "../../../../services/address.service";
import {Injectable} from "@angular/core";
import {AddressDefaults} from "../../../../../../../shared/interfaces/constants";
import {BusyAction} from "../../../../store/progress/progress.actions";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {RegExpPatterns} from "../../../../../../../shared/constants";
import {Address} from "../../../company/company.model";

@Injectable()
export class AddressInputMapper {

  constructor(private addressService: AddressService,
              private store: Store<AppState>) {
  }

  mapFrom(form: FormGroup, prefix: string = ''): IAddress {
    return {
      postcode: form.controls[prefix + 'postcode'].value,
      line1: form.controls[prefix + 'line1'].value,
      line2: form.controls[prefix + 'line2'].value,
      line3: form.controls[prefix + 'line3'].value,
      line4: form.controls[prefix + 'line4'].value,
      locality: form.controls[prefix + 'locality'].value,
      townCity: form.controls[prefix + 'townCity'].value,
      county: form.controls[prefix + 'county'].value,
      country: AddressDefaults.country
    }
  }

  mapTo(address: IAddress, prefix:string = '', required: boolean = true): FormInputGroup {

    if (!address) {
      address = new Address();
    }

    let postcodeInput: FormInput = {
      id: prefix + 'postcode', label: 'Postcode', type: 'search', required: required, value: address.postcode,
      regex: RegExpPatterns.Postcode, uppercase: true
    };

    let formGroup = {
      label: 'Address',
      inputs: [
        postcodeInput,
        {id: prefix + 'line1', label: 'Line1', type: 'text', required: required, value: address.line1},
        {id: prefix + 'line2', label: 'Line2', type: 'text', required: false, value: address.line2},
        {id: prefix + 'line3', label: 'Line3', type: 'text', required: false, value: address.line3},
        {id: prefix + 'line4', label: 'Line4', type: 'text', required: false, value: address.line4},
        {id: prefix + 'locality', label: 'Locality', type: 'text', required: false, value: address.locality},
        {id: prefix + 'townCity', label: 'Town / City', type: 'text', required: required, value: address.townCity},
        {id: prefix + 'county', label: 'County', type: 'text', required: required, value: address.county}
      ]
    };

    postcodeInput.action = postcode => {
      this.store.dispatch(new BusyAction());
      return this.addressService.search(postcode)
        .timeout(5000)
        .map(result => result.addresses)
        .catch(error => {
          console.log(error);
          return Observable.empty();
        })
        .finally(() => this.store.dispatch(new BusyAction(false)));
    };

    postcodeInput.select = address => {
      if (address) {
        formGroup.inputs.forEach((input: FormInput) => {
          // remove prefix from id to get the property name
          let property = input.id.replace(prefix, '');
          input.control.setValue(address[property]);
        });
      }
    };

    return formGroup;
  }
}


