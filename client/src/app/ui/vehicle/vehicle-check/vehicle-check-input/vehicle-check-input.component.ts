import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {SelectVehiclesState} from "../../../../store/vehicles/vehicle.selectors";
import {IVehicle, IVehicleCheck} from "../../../../../../../shared/interfaces/vehicle.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {SaveVehicleCheckAction} from "app/store/vehicles/vehicle-check.actions";
import {VehicleCheckInputMapper} from "./vehicle-check-input.mapper";

@Component({
  selector: 'gp-check-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
  `
})
export class VehicleCheckInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private vehicle: IVehicle;
  private check: IVehicleCheck;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private checkInputMapper: VehicleCheckInputMapper) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectVehiclesState())
      .subscribe(state => {
        this.vehicle = state.selected;
        this.check = state.selectedCheck
          ? state.selectedCheck
          : <IVehicleCheck>{ checkDate: null };

        this.inputGroups = this.checkInputMapper.mapTo(this.check);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let check = Object.assign({}, this.check, this.checkInputMapper.mapFrom(form, this.inputGroups, this.check));
    this.store.dispatch(new SaveVehicleCheckAction(this.vehicle, check));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
