import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {LoadDriverAction} from "../../../store/drivers/driver.actions";
import {pagination} from "../../../shared/constants";
import {LoadVehicleAction, LoadVehiclesAction} from "../../../store/vehicles/vehicle.actions";

@Injectable()
export class DriverVehicleResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    let driverId = route.params['driverId'];

    if (!companyId || !driverId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new LoadDriverAction(companyId, driverId));

    let vehicleId = route.params['vehicleId'];

    if (vehicleId) {
      this.store.dispatch(new LoadVehicleAction(companyId, vehicleId));
    } else {
      this.store.dispatch(new LoadVehiclesAction(companyId, 1, pagination.pageSize));
    }

    return true;
  }

}
