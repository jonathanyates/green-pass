import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {AddressState} from "./address.state";

export const SelectAddressList = () => {
  return (store:Store<AppState>) => {
    return store.select<AddressState>(state => state.address)
      .map(state => state ? state.list : null);
  }
};

export const SelectAddress = () => {
  return (store:Store<AppState>) => {
    return store.select<AddressState>(state => state.address)
      .map(state => state ? state.selected : null);
  }
};
