import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {Supplier} from "../company/company.model";
import {AppState} from "../../store/app.states";
import {Observable} from "rxjs";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {SelectSupplier} from "../../store/company/company.selectors";
import {SelectError} from "../../store/error/error.selectors";

@Injectable()
export class SupplierResolve implements Resolve<Supplier|boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Supplier|boolean> {

    let id = route.params['id'];
    let supplierId = route.params['supplierId'];
    if (!id) return Observable.of(false);

    this.store.dispatch(new SelectCompanyAction(id));

    // edit
    if (!supplierId) {
      return Observable.of(Object.assign(new Supplier(), {_companyId: id}));
    }

    return this.store.let(SelectSupplier(supplierId))
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (supplier, error) => ({ supplier:supplier, error:error }))
      .filter(result => result.error !== null || (result.supplier && result.supplier._companyId == id))
      .map(result => result.error ? false : result.supplier)
      .take(1);
  }
}
