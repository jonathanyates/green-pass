export interface NavigationLink {
  label: string;
  url?: string;
  href?: string;
  icon?: string;
  children?: NavigationLink[];
}

export const NavigationLinks = {
  public: [
    { label: 'Home', href: 'home', icon: 'fa-home' },
    { label: 'Compliance', href: 'driver-vehicle-compliance', icon: 'fa-check-circle' },
    { label: 'Software', href: 'driver-vehicle-compliance-software', icon: 'fa-cog' },
    { label: 'Driver Training', href: 'driver-training', icon: 'fa-road' },
    { label: 'Contact', href: 'contact-greenpasscompliance', icon: 'fa-envelope' },
  ],
  admin: [
    {
      label: 'Home',
      url: 'admin',
    },
    {
      label: 'Companies',
      url: 'admin/companies',
    },
    {
      label: 'Templates',
      url: 'admin/templates',
    },
    {
      label: 'Reports',
      url: 'admin/reportTypes',
    },
    {
      label: 'Alerts',
      url: 'admin/alerts',
    },
  ],
  company: [
    {
      label: 'Home',
      url: '/',
    },
    {
      label: 'Drivers',
      url: 'company/:id/drivers',
    },
    {
      label: 'Vehicles',
      url: 'company/:id/vehicles',
    },
    {
      label: 'Depots',
      url: 'company/:id/depots',
    },
    {
      label: 'Suppliers',
      url: 'company/:id/suppliers',
    },
    {
      label: 'Documents',
      url: 'company/:id/templates',
    },
    {
      label: 'Users',
      url: 'company/:id/users',
    },
  ],
  driver: [
    {
      label: 'Home',
      url: 'driver',
      icon: 'fa-home',
    },
    {
      label: 'My Details',
      icon: 'fa-user',
      children: [
        {
          label: 'Details',
          url: 'driver/details',
          icon: 'fa-user',
        },
        {
          label: 'Next Of Kin',
          url: 'driver/nextofkin',
          icon: 'fa-user-circle-o',
        },
        {
          label: 'References',
          url: 'driver/references',
          icon: 'fa-check',
        },
        {
          label: 'Driving Licence',
          url: 'driver/licence',
          icon: 'fa-id-card-o',
        },
        {
          label: 'Insurance',
          url: 'driver/insurance',
          icon: 'fa-file-text-o',
        },
      ],
    },
    {
      label: 'Vehicles',
      url: 'driver/vehicles',
      icon: 'fa-car',
    },
    {
      label: 'Documents',
      url: 'driver/documents',
      icon: 'fa-book',
    },
    {
      label: 'Assessments',
      url: 'driver/assessments',
      icon: 'fa-leanpub',
    },
  ],
};
