import {Directive, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs/Subscription";

@Directive({
  selector: 'gp-base'
})
export class BaseComponent implements OnDestroy {

  protected subscriptions: Subscription[] = [];

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
