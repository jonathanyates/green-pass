import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../../container/container.types';
import { IApiRouter } from '../../interfaces/api-router.interface';
import { IDocumentController } from '../controller/document.controller.interface';
import { IAuthorizationMiddleware } from '../../security/authorization/authorization.middleware.interface';

@injectable()
export class DocumentRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.DocumentController) private documentController: IDocumentController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.documentController.getDocuments
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.documentController.sendDriverDocuments
      )
      .get(
        '/:documentId/newlink/:signerId/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.documentController.getNewEmbeddedUrl
      )
      .get(
        '/:documentId/url',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.documentController.requestRecipientView
      )
      .get(
        '/:documentId/pdf',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.documentController.getDocumentPdf
      );
  }
}
