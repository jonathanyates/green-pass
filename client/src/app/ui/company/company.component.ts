import {Component} from '@angular/core';

@Component({
  template: `
    <gp-breadcrumb></gp-breadcrumb>
    <router-outlet></router-outlet>
`
})
export class CompanyComponent {
}
