import { NgModule } from '@angular/core';
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminComponent} from "./admin.component";
import {CommonModule} from "@angular/common";
import {AdminDashboardComponent} from "./dashboard/admin-dashboard.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {DriverModule} from "../driver/driver.module";
import {CompanyModule} from "../company/company.module";
import {UserModule} from "../user/user.module";
import {VehicleModule} from "../vehicle/vehicle.module";
import {TemplateModule} from "../template/template.module";

@NgModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      CommonModule,
      AdminRoutingModule,
      SharedModule,
      CompanyModule,
      DriverModule,
      VehicleModule,
      TemplateModule,
      UserModule
    ],
    exports: [ ],
    declarations: [
      AdminComponent,
      AdminDashboardComponent
    ],
    providers: [
    ],
})
export class AdminModule { }
