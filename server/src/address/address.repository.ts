import {injectable, inject} from "inversify";
import {IDbContext} from "../db/db.context.interface";
import {TYPES} from "../container/container.types";
import {IAddressList} from "../../../shared/interfaces/address.interface";

export interface IAddressRepository {
  get(postcode: string): Promise<IAddressList>;
  add(addressList: IAddressList): Promise<IAddressList>;
}

@injectable()
export class AddressRepository implements IAddressRepository {

  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {
  }

  get(postcode: string): Promise<IAddressList> {
    return this.dbContext.address.findOne({ postcode: postcode }).exec();
  }

  add(addressList: IAddressList): Promise<IAddressList> {
    const model = new this.dbContext.address(addressList);
    return model.save();
  }

}