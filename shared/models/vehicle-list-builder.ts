import {IVehicle} from "../interfaces/vehicle.interface";
import {FormatDate, GetCheckDateStyle, GetDateStyle} from "../utils/helpers";
import {IDynamicList} from "./dynamic-list.model";
import {ICompanySettings} from "../interfaces/company-settings.interface";
import {VehicleTypes} from "../interfaces/constants";

export class VehicleListBuilder {

  static getList(vehicles: IVehicle[], settings: ICompanySettings): IDynamicList {

    let exclusions = settings && settings.vehicles
      ? settings.vehicles.grey.exclusions
      : {
        service: false,
        inspection: false,
        checks: false
      };

    let hasFleet = vehicles.some(vehicle => vehicle.type == VehicleTypes.fleet);

    let list = <IDynamicList>{
      columns: (() => {
        let columns = [
          {header: 'Reg', field: 'registrationNumber'},
          {header: 'Make', field: 'make'},
          {header: 'Model', field: 'model'},
          {header: 'Annual miles', field: 'annualMileage'},
          {header: 'Type', field: 'type'},
          {header: 'Tax Due', field: 'taxDueDate'},
          {header: 'MOT Due', field: 'motDueDate'},
        ];

        if (hasFleet || !exclusions.service) {
          columns.push({header: 'Service Due', field: 'nextService'});
        }

        if (hasFleet || !exclusions.inspection) {
          columns.push({header: 'Inspection Due', field: 'nextInspection'});
        }

        if (hasFleet || !exclusions.checks) {
          columns.push({header: 'Vehicle Check Due', field: 'nextVehicleCheck'});
        }

        columns.push({header: 'Compliant', field: 'compliant'});

        return columns;
      })(),
      items: vehicles
        ? vehicles.map(vehicle => {
          let item:any = Object.assign({}, vehicle, {
            taxDueDate: vehicle.taxDueDate ? FormatDate(new Date(vehicle.taxDueDate)) : null,
            motDueDate: vehicle.motDueDate ? FormatDate(new Date(vehicle.motDueDate)) : 'No MOT'
          });


          item.nextService = vehicle.type !== VehicleTypes.grey || !exclusions.service
            ? vehicle.nextService ? FormatDate(vehicle.nextService) : 'No History'
            : 'N/A';


          item.nextInspection = vehicle.type !== VehicleTypes.grey || !exclusions.inspection
            ? vehicle.nextInspection ? FormatDate(vehicle.nextInspection) : 'No History'
            : 'N/A';

          item.nextVehicleCheck = vehicle.type !== VehicleTypes.grey || !exclusions.checks
            ? vehicle.nextVehicleCheck ? FormatDate(vehicle.nextVehicleCheck) : 'No History'
            : 'N/A';

          return item;
        })
        : null,
      styles: vehicles
        ? vehicles.map(vehicle => {
          let item:any = {
            taxDueDate: VehicleListBuilder.getDateStyle(vehicle.taxDueDate),
            motDueDate: VehicleListBuilder.getDateStyle(vehicle.motDueDate),
            compliant: !vehicle.compliance || vehicle.compliance.compliant
              ? 'fa fa-check green-text'
              : 'fa fa-times red-text'
          };

          if (vehicle.type !== VehicleTypes.grey || !exclusions.service) {
            item.nextService = VehicleListBuilder.getDateStyle(vehicle.nextService);
          }

          if (vehicle.type !== VehicleTypes.grey || !exclusions.inspection) {
            item.nextInspection = VehicleListBuilder.getDateStyle(vehicle.nextInspection);
          }

          if (vehicle.type !== VehicleTypes.grey || !exclusions.checks) {
            item.nextVehicleCheck = VehicleListBuilder.getCheckDateStyle(vehicle.nextVehicleCheck);
          }

          return item;
        })
        : null
    };

    return list;
  }

  static getDateStyle(date: Date) {
    let style = GetDateStyle(date);
    return 'badge ' + (style ? style : 'danger-color');
  }

  static getCheckDateStyle(date: Date) {
    let style = GetCheckDateStyle(date);
    return style ? 'badge ' + style : null;
  }

}
