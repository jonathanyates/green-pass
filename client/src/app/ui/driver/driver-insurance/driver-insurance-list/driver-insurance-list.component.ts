import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IInsurance} from "../../../../../../../shared/interfaces/common.interface";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";

@Component({
  selector: 'gp-driver-insurance-list',
  template: `
    <gp-fileRecord-list [fileRecordHistory]="insuranceHistory" 
                        [allowAdd]="allowInput" 
                        [lightHeader]="lightHeader"
                        [fileRecordType]="'Insurance'"
                        (add)="onAdd()" 
                        (delete)="onDelete($event)" 
                        (itemSelected)="onSelect($event)">      
    </gp-fileRecord-list>
  `
})

export class DriverInsuranceListComponent {

  @Input() insuranceHistory: IInsurance[];
  @Input() user : IUser;
  @Input() allowInput: boolean = false;
  @Input() lightHeader: boolean = false;
  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<IInsurance> = new EventEmitter<IInsurance>();
  @Output() select: EventEmitter<any> = new EventEmitter<any>();

  onAdd() {
    this.add.emit()
  }

  onSelect(insurance: IInsurance) {
    this.select.emit(insurance);
  }

  onDelete(insurance: IInsurance) {
    this.delete.emit(insurance);
  }

}
