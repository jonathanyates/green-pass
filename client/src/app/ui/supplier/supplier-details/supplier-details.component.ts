import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {ISupplier} from "../../../../../../shared/interfaces/company.interface";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";

@Component({
  selector: 'gp-supplier-details',
  template: `
    <gp-card [header]="'Supplier details'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class SupplierDetailsComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private supplier: ISupplier;
  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.subscription = this.route.data
      .map((data: { supplier: ISupplier }) => data.supplier)
      .subscribe(supplier => {
        this.supplier = supplier;
        this.items = [
          {title: "Supplier", value: supplier.name},
          {title: "Address", value: supplier.address},
          {title: "Phone", value: supplier.phone},
          {title: "Mobile", value: supplier.mobile},
          {title: "Email", value: supplier.email}
        ]
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit() {
    this.router.navigate(['./', Paths.Edit], { relativeTo: this.route })
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
