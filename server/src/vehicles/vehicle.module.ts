import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {VehicleRouter} from "./vehicle.router";
import {IVehicleController, VehicleController} from "./vehicle.controller";
import {IVehicleRepository, VehicleRepository} from "./vehicle.repository";
import {IVehicleApiService, VehicleService} from "./vehicle.service";

export const vehicleModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.VehicleRouter).to(VehicleRouter).inSingletonScope();
  bind<IVehicleController>(TYPES.controllers.VehicleController).to(VehicleController).inSingletonScope();
  bind<IVehicleApiService>(TYPES.services.VehicleService).to(VehicleService).inSingletonScope();
  bind<IVehicleRepository>(TYPES.repositories.VehicleRepository).to(VehicleRepository).inSingletonScope();
});