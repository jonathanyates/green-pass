import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'percentageColor' })
export class PercentageColorPipe implements PipeTransform {

  transform(value: number, args: any[]): any {
    if (value === 100) {
      return 'primary-color';
    } else if (value > 75) {
      return 'warning-color-dark'
    } else {
      return 'danger-color-dark'
    }
  };


}
