//import {AlertCategory, AlertPriority, IAlertType} from "../../shared/interfaces/alert.interface";

/* Driver Alerts

Unsigned Mandates
Licence Check Required
DVLA Access denied
Licence Expired
Provisional Licence Holder
8 Points or more

OnLine Assessments incomplete
OnLine Modules incomplete
OnRoad Assessment incomplete

 */

// export const driverAlertTypes: IAlertType[] = [
//   {
//     category: AlertCategory.Driver,
//     name: 'None compliant drivers',
//     query: 'getNoneCompliantDrivers',
//     priority: AlertPriority.Critical
//   },
//   {
//     category: AlertCategory.Vehicle,
//     name: 'None compliant vehicles',
//     query: 'getNoneCompliantVehicles',
//     priority: AlertPriority.Critical
//   }
// ];