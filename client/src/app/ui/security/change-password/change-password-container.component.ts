import {Component, OnInit} from "@angular/core";
import {AuthorizationService} from "../../../services/authorization.service";
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {AppState} from "../../../store/app.states";
import {BusyAction} from "../../../store/progress/progress.actions";
import {SelectAuthenticationState} from "../../../store/authentication/authentication.selectors";
import {AuthenticationState} from "../../../store/authentication/authentication.reducer";
import {ChangePasswordSuccessAction, LoginSuccessAction} from "../../../store/authentication/authentication.actions";
import {AuthenticationHelper} from "../../../store/authentication/authentication.helper";

@Component({
  template: `
    <gp-change-password [title]="'Change Password'" (submit)="onSubmit($event)" 
                        [message]="message" [error]="error"></gp-change-password>
  `
})
export class ChangePasswordContainerComponent implements OnInit {

  message: string;
  error: string;
  private authState: AuthenticationState;

  constructor(
    private authService: AuthorizationService,
    private store: Store<AppState>,
    public router: Router) {
  }

  ngOnInit() {
    this.store.let(SelectAuthenticationState())
      .subscribe(authState=> this.authState = authState);
  }

  onSubmit(password: string) {
    this.store.dispatch(new BusyAction());

    this.authService.changePassword(this.authState.user, password)
      .subscribe(token => {
        this.store.dispatch(new BusyAction(false));

        if (AuthenticationHelper.isTokenValid(token)) {
          localStorage.setItem('auth_token', token);
          this.store.dispatch(new LoginSuccessAction(token, true))
        }
        else {
          throw new Error('Change Password failed. Invalid Login Token.');
        }
      },(err: Error) => {
        this.store.dispatch(new BusyAction(false));
        this.error = err.message;
      })
  }
}
