import { Injectable } from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {
  IVehicle, IVehicleCheck, IVehicleInspection,
  IVehicleService, IVehicleSummary
} from "../../../../shared/interfaces/vehicle.interface";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {IPaginationResults} from "../../../../shared/interfaces/common.interface";
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable()
export class VehicleService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getVehicles(companyId: string, page: number, pageSize: number, searchParameters?: any)
    : Observable<IPaginationResults<IVehicle>> {

    this.authorize();

    let params = new HttpParams()
      .set('page', page ? page.toString() : '1')
      .set('pageSize', pageSize ? pageSize.toString() : '10');

    if (searchParameters) {
      for(let prop in searchParameters) {
        if (searchParameters.hasOwnProperty(prop)) {
          params = params.set(prop, searchParameters[prop]);
        }
      }
    }

    return this.httpClient.get<IPaginationResults<IVehicle>>(this.companiesUrl + companyId + '/vehicles', this.getOptions(params))
      .map(res => this.handleResult<IPaginationResults<IVehicle>>(res))
      .catch(this.handleError);
  }

  getVehicle(companyId: string, vehicleId: string): Observable<IVehicle> {

    this.authorize();

    return this.httpClient.get<IVehicle>(this.companiesUrl + companyId + '/vehicles/' + vehicleId, this.getOptions())
      .map(res => this.handleResult<IVehicle>(res))
      .catch(this.handleError);
  }

  getVehicleSummary(companyId: string): Observable<IVehicleSummary> {

    this.authorize();

    return this.httpClient.get<IVehicleSummary>(this.companiesUrl + companyId + '/vehicles/summary', this.getOptions())
      .map(res => this.handleResult<IVehicleSummary>(res))
      .catch(this.handleError);
  }

  saveVehicle(vehicle: IVehicle): Observable<IVehicle> {

    this.authorize();
    vehicle.drivers = null;

    if (vehicle._id) {
      return this.httpClient.put<IVehicle>(this.companiesUrl + vehicle._companyId + '/vehicles/' + vehicle._id, vehicle, this.getOptions())
        .map(res => this.handleResult<IVehicle>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IVehicle>(this.companiesUrl + vehicle._companyId + '/vehicles', vehicle, this.getOptions())
        .map(res => this.handleResult<IVehicle>(res))
        .catch(this.handleError);
    }
  }

  removeVehicle(companyId: string,vehicleId: string): Observable<IVehicle> {
    this.authorize();

    return this.httpClient.delete<IVehicle>(`${this.companiesUrl}${companyId}/vehicles/${vehicleId}`, this.getOptions())
      .map(res =>  this.handleResult<IVehicle>(res))
      .catch(this.handleError);
  }

  dvlaVehicleCheck(companyId: string, registrationNumber: string): Observable<IVehicle> {

    this.authorize();

    return this.httpClient.get<IVehicle>(`${this.companiesUrl}${companyId}/vehicles/vehiclecheck/${registrationNumber}`, this.getOptions())
      .map(res => this.handleResult<IVehicle>(res))
      .catch(this.handleError);
  }

  saveVehicleService(vehicle: IVehicle, vehicleService: IVehicleService): Observable<IVehicleService> {

    this.authorize();

    let formData = new FormData();
    formData.append('serviceDate', vehicleService.serviceDate.toString());
    formData.append('supplierId', vehicleService.supplierId);
    formData.append('fileType', vehicleService.fileType);

    if (vehicleService.file) {
      formData.append('file', vehicleService.file);
    }

    if (vehicleService._id) {
      return this.httpClient.put<IVehicleService>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
          vehicle._id + '/services/' + vehicleService._id, formData, this.getOptions())
        .map(res => this.handleResult<IVehicleService>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IVehicleService>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
          vehicle._id + '/services/', formData, this.getOptions())
        .map(res => this.handleResult<IVehicleService>(res))
        .catch(this.handleError);
    }
  }

  saveVehicleInspection(vehicle: IVehicle, vehicleInspection: IVehicleInspection): Observable<IVehicleInspection> {

    this.authorize();

    let formData = new FormData();
    formData.append('inspectionDate', vehicleInspection.inspectionDate.toString());
    formData.append('supplierId', vehicleInspection.supplierId);
    formData.append('fileType', vehicleInspection.fileType);

    if (vehicleInspection.file) {
      formData.append('file', vehicleInspection.file);
    }

    if (vehicleInspection._id) {
      return this.httpClient.put<IVehicleInspection>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
        vehicle._id + '/inspections/' + vehicleInspection._id, formData, this.getOptions())
        .map(res => this.handleResult<IVehicleInspection>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IVehicleInspection>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
        vehicle._id + '/inspections/', formData, this.getOptions())
        .map(res => this.handleResult<IVehicleInspection>(res))
        .catch(this.handleError);
    }
  }

  saveVehicleCheck(vehicle: IVehicle, vehicleCheck: IVehicleCheck): Observable<IVehicleCheck> {

    this.authorize();

    let formData = new FormData();
    formData.append('checkDate', vehicleCheck.checkDate.toString());
    formData.append('checkedBy', vehicleCheck.checkedBy);
    formData.append('odometerReading', vehicleCheck.odometerReading ? vehicleCheck.odometerReading.toString() : null);
    formData.append('fileType', vehicleCheck.fileType);

    if (vehicleCheck.file) {
      formData.append('file', vehicleCheck.file);
    }

    if (vehicleCheck._id) {
      return this.httpClient.put<IVehicleCheck>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
        vehicle._id + '/checks/' + vehicleCheck._id, formData, this.getOptions())
        .map(res => this.handleResult<IVehicleCheck>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IVehicleCheck>(this.companiesUrl + vehicle._companyId + '/vehicles/' +
        vehicle._id + '/checks/', formData, this.getOptions())
        .map(res => this.handleResult<IVehicleCheck>(res))
        .catch(this.handleError);
    }
  }

  addDriver(vehicle: IVehicle, driverId: string): Observable<IVehicle> {

    this.authorize();
    let map = { driverId: driverId, vehicleId: vehicle._id };

    return this.httpClient.post<IVehicle>(`${this.companiesUrl}${vehicle._companyId}/vehicles/${vehicle._id}/drivers/${driverId}`,
      map, this.getOptions())
      .map(response => this.handleResult<IVehicle>(response))
      .catch(this.handleError);
  }

  removeDriver(vehicle: IVehicle, driverId: string): Observable<IVehicle> {

    this.authorize();

    return this.httpClient.delete<IVehicle>(`${this.companiesUrl}${vehicle._companyId}/vehicles/${vehicle._id}/drivers/${driverId}`, this.getOptions())
      .map(response => this.handleResult<IVehicle>(response))
      .catch(this.handleError);
  }
}
