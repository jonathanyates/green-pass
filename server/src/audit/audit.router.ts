import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IApiController } from '../interfaces/api-controller.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';

@injectable()
export class AuditRouter implements IApiRouter {
  routes = express.Router();

  constructor(
    @inject(TYPES.controllers.AuditController) private auditController: IApiController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.auditController.getAll
      )
      .get(
        '/:auditId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.auditController.get
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.auditController.add
      )
      .put(
        '/:auditId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.auditController.update
      );
  }
}
