import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { IDocumentService } from './document.service.interface';
import { TYPES } from '../../container/container.types';
import { ICompany } from '../../../../shared/interfaces/company.interface';
import { IDocumentSignatureService, IEmbeddedUrl } from '../interfaces/signature.service.interface';
import { IDriver } from '../../../../shared/interfaces/driver.interface';
import { IDocument, IRecipient, ITemplate } from '../../../../shared/interfaces/document.interface';
import serverConfig from '../../server/server.config';
import { IDriverRepository } from '../../drivers/driver.repository';
import { ICompanyRepository } from '../../companies/company.repository';
import { ServerError } from '../../shared/errors';
import codes from '../../shared/error.codes';
import { ITemplateService } from '../../templates/template.service';

@injectable()
export class DocumentService implements IDocumentService {
  constructor(
    @inject(TYPES.services.DocumentSignatureService) private signatureService: IDocumentSignatureService,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.TemplateService) private templateService: ITemplateService,
    @inject(TYPES.repositories.DriverRepository) private driverRepository: IDriverRepository
  ) {}

  getDocuments = (driverId: string): Promise<IDocument[]> => {
    return this.driverRepository.get(driverId).then((driver) => {
      if (!driver) {
        throw ServerError('Driver not found.', codes.NOT_FOUND);
      }

      if (!driver.documents || driver.documents.length === 0) {
        return [];
      }

      return driver.documents;
    });
  };

  sendDocuments = (driverId: string): Promise<IDocument[]> => {
    if (!mongoose.Types.ObjectId.isValid(driverId)) {
      return Promise.reject(ServerError('Invalid driver id.', codes.BAD_REQUEST));
    }

    return this.driverRepository.get(driverId).then((driver) => {
      if (!driver) {
        return Promise.reject(ServerError('Driver not found.', codes.NOT_FOUND));
      }

      return Promise.all([
        this.companyRepository.get(driver._companyId),
        this.templateService.getCompanyTemplates(driver._companyId),
      ]).then((result) => {
        const company = result[0];
        let templates = result[1];

        // filter the templates by company templates
        if (templates) {
          templates = templates.filter((template) => company.templates.indexOf(template._id.toString()) > -1);
        }

        // don't send if there are no templates selected
        if (!templates || templates.length === 0) {
          return null;
        }

        return this.sendDocumentsForDriver(driver, templates, company.name);
      });
    });
  };

  sendDocumentsForCompany = (companyId: string): Promise<IDocument[][]> => {
    return this.companyRepository
      .query(companyId)
      .select('_id name templates')
      .exec()
      .then((company) => this.sendDocumentsForCompanyDrivers(company));
  };

  sendDocumentsForAllCompanies = (): Promise<IDocument[][][]> => {
    return this.companyRepository
      .queryAll()
      .select('_id name templates')
      .exec()
      .then((companies) => {
        console.log('- Sending Documents for all companies');
        return Promise.all(
          companies.map((company) => {
            console.log('- Sending Documents for Company ' + company._id);
            return this.sendDocumentsForCompanyDrivers(company);
          })
        );
      });
  };

  requestEmbeddedUrl = (driverId: string, documentId: string, returnUrl: string) => {
    return this.driverRepository.get(driverId).then((driver) => {
      if (!driver) {
        return Promise.reject(ServerError('Driver not found.', codes.NOT_FOUND));
      }

      const recipient = <IRecipient>{
        id: driver._id.toString(),
        firstName: driver._user.forename,
        lastName: driver._user.surname,
        email: driver._user.email,
      };

      return this.signatureService.requestEmbeddedUrl(documentId, recipient, returnUrl);
    });
  };

  private sendDocumentsForDriver = (
    driver: IDriver,
    templates: ITemplate[],
    companyName: string
  ): Promise<IDocument[]> => {
    console.log(driver.constructor.name);

    const recipient = <IRecipient>{
      id: driver._id.toString(),
      firstName: driver._user.forename,
      lastName: driver._user.surname,
      email: driver._user.email,
    };

    const recipientName = `${recipient.firstName} ${recipient.lastName}`;

    console.log(`Sending documents for driver ${driver._id} (name:${recipientName}) in company ${companyName}`);

    // documents to remove, i.e. unsigned driver documents
    const documentsToRemove = driver.documents
      .filter((document: IDocument) => !document.signed)
      .map((document) => document.documentId);

    if (documentsToRemove) {
      console.log(
        `- Deleting unsigned documents for driver ${driver._id} (name:${recipientName}) in company ${companyName}`
      );
    }

    return this.signatureService
      .deleteDocuments(documentsToRemove)
      .then((documents) => {
        if (documentsToRemove && documentsToRemove.length > 0) {
          driver.documents = driver.documents.filter(
            (document) => documentsToRemove.indexOf(document.documentId) === -1
          );
          return this.driverRepository.update(driver);
        }

        return driver;
      })
      .then((driver) => {
        // remove any templates that have already been signed
        const signedTemplates = driver.documents
          .filter((document: IDocument) => document.signed)
          .map((document: IDocument) => document.templates)
          .reduce((previous, current) => previous.concat(current), []);

        templates = templates.filter((template) => {
          const templateId = template._id.toString();
          return signedTemplates.indexOf(templateId) === -1;
        });

        console.log(`- Creating documents for driver ${driver._id} (name:${recipientName}) in company ${companyName}`);

        const packNo = driver.documents.length + 1;

        return this.signatureService
          .createCompositeDocument(templates, `Driver Mandate Pack #${packNo}`, recipient, companyName)
          .then((document: IDocument) => {
            return document;
          });
      })
      .then((document: IDocument) => {
        if (document) {
          console.log(`- Document created for Driver ${driver._id} for company ${companyName}`);
          driver.documents = [...driver.documents, document];
        }

        console.log(`Driver ${driver._id} document set to:`);
        console.log(JSON.stringify(driver.documents));
        console.log(`Saving Driver ${driver._id}`);

        return this.driverRepository.update(driver).then((driver) => [document]);
      });
  };

  getNewEmbeddedUrl(driverId: string, documentId: string, signerId: string): Promise<IEmbeddedUrl> {
    return this.driverRepository.get(driverId).then((driver) => {
      const document = driver.documents.find((document) => document.documentId === documentId);

      if (!document) {
        throw ServerError('Unable to find document for driver.', codes.BAD_REQUEST);
      }

      return this.signatureService.getNewEmbeddedUrl(signerId).then((embeddedUrl) => {
        if (!embeddedUrl || !embeddedUrl.url) {
          throw ServerError('Unable to get new link for driver document ' + documentId, codes.INTERNAL_SERVER_ERROR);
        }

        document.embeddingUri = embeddedUrl.url;

        return this.driverRepository.update(driver).then((driver) => {
          return embeddedUrl;
        });
      });
    });
  }

  private sendDocumentsForCompanyDrivers = (company: ICompany): Promise<IDocument[][]> => {
    if (!company) {
      return Promise.reject(ServerError('Company not found.', codes.NOT_FOUND));
    }

    return this.signatureService
      .getTemplates()
      .then((templates) => templates.filter((template) => company.templates.indexOf(template._id.toString()) > -1))
      .then((templates) =>
        this.driverRepository
          .find({ _companyId: new mongoose.Types.ObjectId(company._id) })
          .then((drivers) =>
            Promise.all(drivers.map((driver) => this.sendDocumentsForDriver(driver, templates, company.name)))
          )
      );
  };
}
