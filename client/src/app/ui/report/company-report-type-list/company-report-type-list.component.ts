import { Paths } from '../../../shared/constants';
import { AppState } from '../../../store/app.states';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Store } from '@ngrx/store';
import {LoadReportTypesAction} from "../../../store/reports/report.actions";
import {SelectReportTypesForCompany} from "../../../store/reports/report.selectors";
import {IReportSchedule, IReportType} from "../../../../../../shared/interfaces/report.interface";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Company} from "../../company/company.model";
import {ReportService} from "../../../services/report.service";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import * as cronstrue from 'cronstrue';

@Component({
  template: `
    <gp-dynamic-list [list]="list" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class CompanyReportTypeListComponent implements OnInit, OnDestroy {

  company: Company;
  list: IDynamicList;
  private reportTypes: IReportType[];
  private schedules: IReportSchedule[];
  private subscriptions = [];
  private buttonStyle = 'btn btn-primary btn-sm btn-list waves-effect';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private reportService: ReportService) { }

  ngOnInit() {
    this.store.dispatch(new LoadReportTypesAction());

    this.list = {
      columns: [
        { header: 'Report Name', field: 'name' },
        { header: 'Category', field: 'category' },
        { header: 'Schedule', field: 'cron' },
        { header: 'Recipients', field: 'recipients' },
        { header: 'Set Schedule', field: 'action', columnType: ColumnType.Icon }
      ]
    };

    this.subscriptions.push(this.store.let(SelectReportTypesForCompany())
      .combineLatest(
        this.store.let(SelectCompany())
          .filter(company => company != null)
          .do(company => this.company = company)
          .switchMap(company => this.reportService.getReportSchedules(company._id))
      )
      .subscribe(results => {
        this.reportTypes = results[0];
        this.schedules = results[1];
        this.setList();
      }));
  }

  private setList() {
    // map all report types to schedules
    this.list.items = this.reportTypes.map(reportType => {
      let schedule:IReportSchedule = this.schedules.find(schedule => schedule.reportType._id === reportType._id);

      let recipients = schedule && schedule.recipients
        ? this.truncate(schedule.recipients.map(user => user.email).join('; '), 30)
        : '-';

      return Object.assign({}, reportType, {
        cron: schedule ? cronstrue.toString(schedule.cron) : '-',
        recipients: recipients,
        schedule: schedule
      });
    });

    this.list.styles = this.list.items.map(item =>
      ({ action: { action: 'fa fa-clock-o green-text' } })
    );

    this.list.actions = this.list.items.map(item => ({
      action: {
        action: e => this.addOrEditSchedule(e),
        icon: 'fa-lg fa-clock-o green-text'
      }
    }));
  }

  truncate(string: string, length: number, delimiter?: string) {
    delimiter = delimiter || "...";
    return string.length > length ? string.substr(0, length) + delimiter : string;
  };

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  select(reportType:IReportType) {
    this.router.navigate(['./', reportType._id], { relativeTo: this.route });
  }

  add() {
    this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  addOrEditSchedule(args: { event: Event, item: any }) {
    args.event.preventDefault();
    args.event.stopPropagation();
    this.router.navigate(['./', args.item._id, Paths.Schedule], { relativeTo: this.route });
  }

}
