import {IRouter} from "express-serve-static-core";

export interface IApiRouter {
  routes: IRouter;
}