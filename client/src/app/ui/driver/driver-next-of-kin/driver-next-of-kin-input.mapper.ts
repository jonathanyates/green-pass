import {FormInput, FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {FormGroup} from '@angular/forms';
import {Contact} from '../driver.model';
import {AddressInputMapper} from '../../shared/components/address-input/address-input.mapper';
import {Injectable} from '@angular/core';
import {RegExpPatterns} from '../../../../../../shared/constants';
import {IContact, IDriver} from '../../../../../../shared/interfaces/driver.interface';
import {IAddress} from '../../../../../../shared/interfaces/address.interface';
import { Observable } from 'rxjs/Observable';
import {DriverCompliance} from "../../../../../../shared/models/driver-compliance.model";

@Injectable()
export class NextOfKinInputMapper {

  constructor(private addressInputMapper: AddressInputMapper) {}

  mapFrom(form: FormGroup, driver: IDriver): IContact {

    const contact: IContact = {
      title: form.controls['title'].value,
      forename: form.controls['forename'].value,
      surname: form.controls['surname'].value,
      phone: form.controls['phone'].value,
      mobile: form.controls['mobile'].value,
      email: form.controls['email'].value,
      address: null
    };

    let sameAddress = form.controls['sameAddress'];

    if (sameAddress != null && sameAddress.value === true && driver.address) {
      contact.address = {
        line1: driver.address.line1,
        line2: driver.address.line2,
        line3: driver.address.line3,
        line4: driver.address.line4,
        locality: driver.address.locality,
        townCity: driver.address.townCity,
        postcode: driver.address.postcode,
        county: driver.address.county,
        country: driver.address.country
      }
    } else {
      contact.address = this.addressInputMapper.mapFrom(form);
    }

    return contact;
  }

  mapTo(driver: IDriver): FormInputGroup[] {

    const nextOfKin = driver.nextOfKin
      ? driver.nextOfKin
      : new Contact();

    const titles = [
      { key: 'Mr', value: 'Mr', selected: nextOfKin.title === 'Mr' },
      { key: 'Mrs', value: 'Mrs', selected: nextOfKin.title === 'Mrs' },
      { key: 'Ms', value: 'Ms', selected: nextOfKin.title === 'Ms' },
      { key: 'Dr', value: 'Dr', selected: nextOfKin.title === 'Dr' },
    ];

    const address = driver.nextOfKin && driver.nextOfKin.address
      ? driver.nextOfKin.address
      : driver.address;

    const nextOfKinAddressGroup = this.addressInputMapper.mapTo(address, '', false);

    let group = [
      {
        label: 'Next of kin',
        inputs: [
          { id: 'title', label: 'Title', type: 'dropdown', required: true, autoFocus: true,
          options: titles, value: driver._user && driver._user.title ? driver._user.title : null },
          { id: 'forename', label: 'Forename', type: 'text', required: true, autoFocus: false,
            value: nextOfKin.forename },
          { id: 'surname', label: 'Surname', type: 'text', required: true,
            value: nextOfKin.surname },
          { id: 'phone', label: 'Phone', type: 'text', required: true,
            regex: RegExpPatterns.Phone,
            value: nextOfKin.phone },
          { id: 'mobile', label: 'Mobile', type: 'text', required: false,
            regex: RegExpPatterns.Mobile,
            value: nextOfKin.mobile },
          { id: 'email', label: 'Email', type: 'text', required: false,
            regex: RegExpPatterns.Email, value: nextOfKin.email }
        ]
      },
      nextOfKinAddressGroup
    ];

    let isDriverAddressValid = DriverCompliance.getDriverAddressCompliance(driver);

    // only show option to select same address of driver if the driver has a valid address
    if (isDriverAddressValid && nextOfKin.address.postcode == null) {
      nextOfKinAddressGroup.hidden = true;
      group[0].inputs.push(
        { id: 'sameAddress', label: 'Address same as driver', type: 'checkbox', required: false, value: true,
          action: value => {
            // toggle next of kin address when changed
            nextOfKinAddressGroup.hidden = value;
            if (value === true) {
              this.setNextOfKinFields(nextOfKinAddressGroup, driver.address);
            } else {
              this.clearNextOfKinFields(nextOfKinAddressGroup);
            }
            return Observable.empty();
          }
      })
    }

    return group;
  }

  setNextOfKinFields(nextOfKinAddressGroup: FormInputGroup, driverAddress: IAddress) {
    nextOfKinAddressGroup.inputs.forEach((input: FormInput) => {
      const value = driverAddress[input.id];
      input.control.setValue(value);
    })
  }

  clearNextOfKinFields(nextOfKinAddressGroup: FormInputGroup) {
    nextOfKinAddressGroup.inputs.forEach((input: FormInput) => {
      input.control.setValue('');
    })
  }

}
