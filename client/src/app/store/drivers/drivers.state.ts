import {Driver} from "../../ui/driver/driver.model";
import {IContact, IDriverInsurance} from "../../../../../shared/interfaces/driver.interface";
import {IFawCertificate} from '../../../../../shared/interfaces/common.interface';

export interface DriversState {
  list: Driver[];
  total: number;
  page: number;
  selected?: Driver;
  selectedReference?: IContact,
  selectedTab?: string,
  selectedInsurance?: IDriverInsurance,
  selectedFaw?: IFawCertificate,
  assessmentLoggedIn?: boolean
}
