import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {Router, ActivatedRoute} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {Company} from "../../company/company.model";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompany} from "../../../store/company/company.selectors";
import {SelectUsers} from "../../../store/users/user.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {ModalComponent} from "../../shared/components/modal/model.component";
import {BusyAction} from "../../../store/progress/progress.actions";
import {LoadUsersAction} from "../../../store/users/user.actions";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {UserService} from "../../../services/user.service";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {User} from "../user.model";

@Component({
  selector: 'gp-user-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"
                     (remove)="onRemove($event)"></gp-dynamic-list>

    <gp-model [title]="'Remove User'" (confirmed)="onRemoveConfirmed($event)">
      <ng-container *ngIf="selectedUser && loggedInUser && selectedUser._id == loggedInUser._id; else confirm">
        <div>You can not remove yourself from the list.</div>
      </ng-container>      
      
      <ng-template #confirm>
        <div *ngIf="selectedUser; else noUser">
          Are you sure you want to remove user '{{selectedUser.forename}} {{selectedUser.surname}}' ?
        </div>
      </ng-template>

      <ng-template #noUser>No User selected</ng-template>
    </gp-model>
`
})
export class UserListComponent implements OnInit, OnDestroy {

  company: Company;
  users: IUser[];
  list: IDynamicList;
  selectedUser: IUser;
  loggedInUser: IUser;

  private sub: Subscription;
  private companySub: Subscription;

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private userService: UserService) { }

  ngOnInit() {
    this.sub = this.store.let(SelectUsers())
      .combineLatest(this.store.let(SelectLoggedInUser()).filter(x => x != null))
      .subscribe(results => {
        this.users = results[0];
        this.loggedInUser = results[1];
        this.list = {
          columns: [
            { header: 'Name', field: 'fullname' },
            { header: 'Username', field: 'username' },
            { header: 'Email', field: 'email' },
            {header: '', field: 'remove', columnType: ColumnType.Icon}
          ],
          items: this.users
            ? this.users.map(user => Object.assign({}, user, {
              fullname: User.getFullName(user)
            }))
            : null,
          actions: this.users
            ? this.users.map(user => {
                if (user._id != this.loggedInUser._id) {
                  return {
                    remove: {
                      action: args => { if (args) this.onRemove(args.item) },
                      icon: 'fa fa-trash-o green-text'
                    }
                  }
                } else {
                  return null;
                }
              })
            : null
        }
      });

    this.companySub = this.store.let(SelectCompany())
      .subscribe(company => this.company = company);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.companySub.unsubscribe();
  }

  add() {
    if (this.company && this.company._id) {
      this.router.navigate(['./', Paths.New], { relativeTo: this.route });
    }
  }

  select(user: IUser) {
    if (user) {
      this.router.navigate(['./', user._id], { relativeTo: this.route });
    }
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onRemove(user:IUser) {
    if (!user) return;
    this.modal.open();
    this.selectedUser = user;
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedUser && this.loggedInUser && this.selectedUser._id != this.loggedInUser._id) {
      this.store.dispatch(new BusyAction());
      this.userService.removeUser(this.selectedUser._companyId, this.selectedUser._id)
        .subscribe(user => {
          this.store.dispatch(new LoadUsersAction(this.company._id));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
    this.selectedUser = null;
  }

}
