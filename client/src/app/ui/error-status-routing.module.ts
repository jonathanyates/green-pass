import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {UnauthorizedComponent} from "./security/status-errors/unauthorized.component";
import {PageNotFoundComponent} from "./security/status-errors/page-not-found.component";

const errorRoutes = [
  { path: '401', component: UnauthorizedComponent },
  { path: '404', component: PageNotFoundComponent },
];

// Routes for Error Status. e.g 404 (**) redirects to login page.
@NgModule({
  imports: [RouterModule.forChild(errorRoutes)],
  exports: [RouterModule],
  providers: []
})
export class ErrorStatusRoutingModule { }
