import '../extensions/date.extensions';
import { format, isDate } from 'date-fns';

export const DateFormatString = 'dd MMM yyyy';
export const DateInputFormatString = 'dd MMMM, yyyy';

export function FormatDate(date: Date, dateFormat: string = DateFormatString) {
  if (!isDate(date)) return null;
  return format(date, dateFormat);
}

export function FormatDateForInput(date: Date, dateFormat: string = DateInputFormatString) {
  if (!isDate(date)) return null;
  return format(date, dateFormat);
}

export function GetDate(value: any) {
  if (value) {
    return new Date(value);
  }
  return null;
}

export function GetDateStyle(date: Date) {
  if (!isDate(date)) return null;
  if (date.getTime() <= Date.now() || date.isToday()) {
    return 'danger-color';
  }
  if (new Date(date).addMonths(-1).getTime() <= Date.now()) {
    return 'warning-color';
  }

  return 'success-color';
}

export function GetCheckDateStyle(date: Date) {
  if (!isDate(date)) return 'danger-color';
  if (date.getTime() <= Date.now()) {
    // && !date.isToday()
    return 'danger-color';
  }

  return 'success-color';
}

export function IsDateExpired(date: Date) {
  if (!isDate(date)) return false;
  return date.getTime() <= Date.now() || date.isToday();
}

export function toProperCase(str: string) {
  return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}
