import {NgModule} from "@angular/core";
import {CompanyComponent} from "../company/company.component";
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {Roles} from "../../../../../shared/constants";
import {RouterModule, Routes} from "@angular/router";
import {AdminComponent} from "../admin/admin.component";
import {ReportTypeListComponent} from "./report-type-list/report-type-list.component";
import {ReportTypeInputComponent} from "./report-type-input/report-type-input.component";
import {ReportComponent} from "./report/report.component";
import {CompanyResolve} from "../company/company.resolve";
import {CompanyReportTypeListComponent} from "./company-report-type-list/company-report-type-list.component";
import {ReportScheduleInputComponent} from "./report-schedule-input/report-schedule-input.component";

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthorizationGuard],
    data: { roles: [Roles.Admin] },
    children: [
      { path: 'reportTypes', component: ReportTypeListComponent },
      { path: 'reportTypes/new', component: ReportTypeInputComponent },
      { path: 'reportTypes/:reportTypeId', component: ReportTypeInputComponent }
    ]
  },
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/reports', component: CompanyReportTypeListComponent, resolve: { company: CompanyResolve } },
      { path: ':id/reports/:reportTypeId', component: ReportComponent, resolve: { company: CompanyResolve } },
      { path: ':id/reports/:reportTypeId/schedule', component: ReportScheduleInputComponent, resolve: { company: CompanyResolve } },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ReportRoutingModule {
  constructor() {
  }
}
