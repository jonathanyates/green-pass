'use strict';

// ================================================================
// get all the tools we need
// ================================================================
var express = require('express');
var routes = require('./routes/index.js');
var favicon = require('serve-favicon');
var sitemap = require('express-sitemap');
var path    = require("path");

var port = process.env.PORT || 4000;
var app = express();

// ================================================================
// setup our express application
// ================================================================
app.use('/public', express.static(process.cwd() + '/public'));
app.use(favicon(__dirname + '/public/favicon.ico'));

app.set('view engine', 'ejs');

// ================================================================
// setup routes
// ================================================================
routes(app);

/*
 * sitemap
 */
app.get('/sitemap.xml', function(req, res) { // send XML map
  res.sendfile(path.join(__dirname + '/sitemap.xml'));
});


// ================================================================
// start our server
// ================================================================
app.listen(port, function() {
  console.log('Server listening on port', port);
});