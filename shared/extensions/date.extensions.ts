export {}

declare global {
  interface Date {
    addDays(days: number, useThis?: boolean): Date;
    addMonths(months: number): Date;
    isToday(): boolean;
    clone(): Date;
    isAnotherMonth(date: Date): boolean;
    isWeekend(): boolean;
    isSameDate(date: Date): boolean;
    isLeapYear(): boolean;
    getDaysInMonth(): number;
    getDayDiff(otherDate:Date): number;
    getMonthDiff(otherDate:Date): number;
    getToday(): Date;
    toDate():Date;
  }
}

export class UtcDate extends Date {
  static asUTC = (year: number, month: number, date?: number, hours?: number, minutes?: number,
                  seconds?: number, milliseconds?: number): Date => {
    year = year || 1970;
    month = month || 1;
    date = date || 1;
    hours = hours || 0;
    minutes = minutes || 0;
    seconds = seconds || 0;
    milliseconds = milliseconds || 0;

    return new Date(Date.UTC(year, month, date, hours, minutes, seconds, milliseconds));
  };

  static toUTC = (dateString):Date => {
    if (!dateString) {
      return null;
    }

    let date = new Date(dateString);
    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()))
  };
}

Date.prototype.addDays = function (days) {
  if (!days) return this;
  let date = this;
  date.setDate(date.getDate() + days);
  return date;
};

Date.prototype.addMonths = function (months) {
  if (!months) return this;
  let date = this;
  let n = date.getDate();
  date.setDate(1);
  date.setMonth(this.getMonth() + months);
  date.setDate(Math.min(n, date.getDaysInMonth()));
  return date;
};

Date.prototype.isToday = function () {
  let today = new Date();
  return this.isSameDate(today);
};

Date.prototype.clone = function () {
  return new Date(+this);
};

Date.prototype.isAnotherMonth = function (date) {
  return date && this.getMonth() !== date.getMonth();
};

Date.prototype.isWeekend = function () {
  return this.getDay() === 0 || this.getDay() === 6;
};

Date.prototype.isSameDate = function (date) {
  return date && this.getFullYear() === date.getFullYear() && this.getMonth() === date.getMonth() && this.getDate() === date.getDate();
};

let isLeapYear = function (year) {
  return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

let getDaysInMonth = function (year, month) {
  return [31, (isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
  return isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
  return getDaysInMonth(this.getFullYear(), this.getMonth());
};

const MS_PER_DAY = 1000 * 60 * 60 * 24;

Date.prototype.getDayDiff = function (otherDate:Date): number {
  if (!otherDate) {
    return null;
  }
  // Discard the time and time-zone information.
  let utc1 = Date.UTC(this.getFullYear(), this.getMonth(), this.getDate());
  let utc2 = Date.UTC(otherDate.getFullYear(), otherDate.getMonth(), otherDate.getDate());

  return Math.floor((utc2 - utc1) / MS_PER_DAY);
};

Date.prototype.getMonthDiff = function(otherDate:Date): number {
  let noOfMonths;
  noOfMonths= (otherDate.getFullYear() - this.getFullYear()) * 12;
  noOfMonths-= this.getMonth() + 1;
  noOfMonths+= otherDate.getMonth() + 1; // we should add + 1 to get correct month number
  return noOfMonths <= 0 ? 0 : noOfMonths;
};

Date.prototype.getToday = function(): Date {
  return new Date(new Date().setHours(0, 0, 0, 0));
};

Date.prototype.toDate = function(): Date {
  return new Date(this.getFullYear(), this.getMonth(), this.getDate());
};

