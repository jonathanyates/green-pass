import { Injectable } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.states';
import { Company } from './company.model';
import { User } from '../user/user.model';
import { SelectCompany } from '../../store/company/company.selectors';
import { SelectUsersState } from '../../store/users/user.selectors';
import { SelectVehiclesState } from '../../store/vehicles/vehicle.selectors';
import { VehiclesState } from '../../store/vehicles/vehicles.state';
import { UsersState } from '../../store/users/users.state';
import { DriversState } from '../../store/drivers/drivers.state';
import { SelectDriversState } from '../../store/drivers/driver.selectors';
import { FormatDate } from '../../../../../shared/utils/helpers';
import { IAlertType } from '../../../../../shared/interfaces/alert.interface';
import { SelectAlertTypes } from '../../store/alerts/alert.selectors';
import { IReportType } from '../../../../../shared/interfaces/report.interface';
import { SelectReportTypes } from '../../store/reports/report.selectors';
import { SelectDocument } from '../../store/documents/document.selectors';
import { IDocument } from '../../../../../shared/interfaces/document.interface';
import { TemplatesState } from '../../store/templates/templates.state';
import { SelectTemplatesState } from '../../store/templates/template.selectors';

@Injectable()
export class CompanyBreadcrumbRouting {
  private company: Company;
  private usersState: UsersState;
  private vehicleState: VehiclesState;
  private driversState: DriversState;
  private templatesState: TemplatesState;
  private alertTypes: IAlertType[];
  private reportTypes: IReportType[];
  private document: IDocument;

  constructor(private breadcrumbService: BreadcrumbService, private store: Store<AppState>) {}

  excluded = [
    'depots',
    'suppliers',
    'vehicles',
    'users',
    'drivers',
    'documents',
    'references',
    'services',
    'inspections',
    'checks',
    'edit',
    'details',
    'assessments',
    'insurance',
    'faw',
    'vehicle',
    'alerts',
    'alertTypes',
    'reportTypes',
    'reports',
    'schedule',
    'new',
    'add',
    'subscription',
    'settings',
    'import',
    'templates',
    'compliance',
  ];

  configure() {
    this.store.let(SelectCompany()).subscribe((company) => (this.company = company));

    this.store.let(SelectUsersState()).subscribe((state) => (this.usersState = state));

    this.store.let(SelectVehiclesState()).subscribe((state) => (this.vehicleState = state));

    this.store.let(SelectDriversState()).subscribe((state) => (this.driversState = state));

    this.store.let(SelectTemplatesState()).subscribe((state) => (this.templatesState = state));

    this.store.let(SelectAlertTypes()).subscribe((alertTypes) => (this.alertTypes = alertTypes));

    this.store.let(SelectReportTypes()).subscribe((reportTypes) => (this.reportTypes = reportTypes));

    this.store.let(SelectDocument()).subscribe((document) => {
      this.document = document;
    });

    this.breadcrumbService.hideRoute('/company');
    this.breadcrumbService.hideRouteRegex('edit');

    this.breadcrumbService.addFriendlyNameForRoute('/admin', 'Admin Dashboard');
    this.breadcrumbService.addFriendlyNameForRoute('/admin/alertTypes', 'Alert Types');
    this.breadcrumbService.addFriendlyNameForRoute('/admin/reportTypes', 'Report Types');
    this.breadcrumbService.addCallbackForRouteRegex('/admin/alertTypes/.*$', this.getAlertType);
    this.breadcrumbService.addCallbackForRouteRegex('/admin/reportTypes/.*$', this.getReportType);
    this.breadcrumbService.addCallbackForRouteRegex('/admin/templates/.*$', this.getTemplateName);

    this.breadcrumbService.addCallbackForRouteRegex('/company/.*$', this.getCompanyName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/edit/.*$', this.getCompanyName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/depots/.*$', this.getDepotName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/suppliers/.*$', this.getSupplierName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/users/.*$', this.getUserName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/vehicles/.*$', this.getVehicle);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*$', this.getDriver);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/insurance/.*$', this.getCompanyInsurance);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/vehicles/.*$', this.getVehicle);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/references/.*$', this.getDriverReference);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/nextofkin', (x) => 'Next of kin');
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/vehicles/.*/services/.*$', this.getVehicleService);
    this.breadcrumbService.addCallbackForRouteRegex(
      '/company/.*/vehicles/.*/inspections/.*$',
      this.getVehicleInspection
    );
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/vehicles/.*/checks/.*$', this.getVehicleCheck);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/templates$', (x) => 'templates');
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/templates/.*$', this.getTemplateName);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/assessments/.*$', (x) => 'On Road');
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/insurance/.*$', this.getDriverInsurance);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/drivers/.*/documents/.*$', this.getDocument);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/alerts/.*/.*$', this.getAlertType);

    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/alertTypes', (x) => 'Manage Alerts');
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/alertTypes/.*$', this.getAlertType);

    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/reports/.*$', this.getReportType);
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/reports/.*/schedule', (x) => 'Schedule');
    this.breadcrumbService.addCallbackForRouteRegex('/company/.*/reports/.*/schedule/.*$', this.getSchedule);
  }

  getSchedule = (id: string) => {
    return '...';
  };

  getAlertType = (id: string) => {
    if (this.alertTypes) {
      let alert = this.alertTypes.find((alert) => alert._id == id);
      if (alert) {
        return alert.reportType.name;
      }
    }
    return '...';
  };

  getReportType = (id: string) => {
    if (this.reportTypes) {
      let reportType = this.reportTypes.find((reportType) => reportType._id == id);
      if (reportType) {
        return reportType.name;
      }
    }
    return '...';
  };

  getCompanyName = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }
    let company = this.company && this.company._id === id ? this.company : null;
    return company ? company.name : '...';
  };

  getDepotName = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.company) {
      let depot = this.company.depots.find((depot) => depot._id === id);
      return depot ? depot.name : '...';
    }
    return id;
  };

  getSupplierName = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.company) {
      let depot = this.company.suppliers.find((supplier) => supplier._id === id);
      return depot ? depot.name : '...';
    }
    return id;
  };

  getUserName = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    let user = this.usersState
      ? this.usersState.selected && this.usersState.selected._id === id
        ? this.usersState.selected
        : this.usersState.list
        ? this.usersState.list.find((users) => users._id === id)
        : null
      : null;

    return user ? User.getFullName(user) : '...';
  };

  getVehicle = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    let vehicle = this.vehicleState
      ? this.vehicleState.selected && this.vehicleState.selected._id === id
        ? this.vehicleState.selected
        : this.vehicleState.list
        ? this.vehicleState.list.find((vehicle) => vehicle._id === id)
        : null
      : null;

    return vehicle ? vehicle.registrationNumber : '...';
  };

  getDriver = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    let driver = this.driversState
      ? this.driversState.selected && this.driversState.selected._id === id
        ? this.driversState.selected
        : this.driversState.list
        ? this.driversState.list.find((driver) => driver._id === id)
        : null
      : null;

    return driver ? User.getFullName(driver._user) : '...';
  };

  getDriverReference = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.driversState.selected) {
      let reference = this.driversState.selected.references.find((reference) => reference._id === id);
      return reference ? reference.forename + ' ' + reference.surname : '...';
    }
    return id;
  };

  getVehicleService = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleService = this.vehicleState.selected.serviceHistory.find((service) => service._id === id);
      return vehicleService ? FormatDate(vehicleService.serviceDate) : '...';
    }
    return id;
  };

  getVehicleInspection = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleInspection = this.vehicleState.selected.inspectionHistory.find((inspection) => inspection._id === id);
      return vehicleInspection ? FormatDate(vehicleInspection.inspectionDate) : '...';
    }
    return id;
  };

  getVehicleCheck = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleCheck = this.vehicleState.selected.checkHistory.find((check) => check._id === id);
      return vehicleCheck ? FormatDate(vehicleCheck.checkDate) : '...';
    }
    return id;
  };

  getDriverInsurance = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.driversState.selected) {
      let driverInsurance = this.driversState.selected.insuranceHistory.find((insurance) => insurance._id === id);
      return driverInsurance ? FormatDate(driverInsurance.validTo) : '...';
    }
    return id;
  };

  getCompanyInsurance = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.company) {
      let service = this.company.fleetInsuranceHistory.find((insurance) => insurance._id === id);
      return service ? FormatDate(service.validTo) : '...';
    }
    return id;
  };

  getDocument = (id: string) => {
    if (!this.document || this.excluded.indexOf(id) > -1) {
      return id;
    }

    return this.document.name;
  };

  getTemplateName = (id: string): string => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.templatesState.selected) {
      let template = this.templatesState.selected;
      return template ? template.name : '...';
    }
    return id;
  };
}
