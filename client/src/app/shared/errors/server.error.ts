import HTTP_STATUS_CODES from "./status-codes.enum";

export class ServerError extends Error {
  constructor(public message: string, public code: HTTP_STATUS_CODES) {
    super()
  }
}
