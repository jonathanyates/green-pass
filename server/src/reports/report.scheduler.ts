import schedule = require('node-schedule');
import { IReportQueryResult, IReportSchedule, IReportType } from '../../../shared/interfaces/report.interface';
import { IApiService } from '../interfaces/api-service.interface';
import { TYPES } from '../container/container.types';
import { inject, injectable } from 'inversify';
import { IReportQueryService } from './report-query.service';
import { IReportGeneratorService } from './report-generator.service';
import { ICompanyService } from '../companies/company.service.interface';
import { Job } from 'node-schedule';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IEmail } from '../email/email.service';

export interface IReportScheduler {
  scheduleReports(): Promise<boolean[][]>;
  scheduleCompanyReports(companyId: string): Promise<boolean[]>;
  scheduleReport(reportSchedule: IReportSchedule): Promise<boolean>;
  listen();
}

@injectable()
export class ReportScheduler implements IReportScheduler {
  private jobs: Map<string, Job> = new Map<string, Job>();

  constructor(
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
    @inject(TYPES.services.ReportScheduleService) private reportScheduleService: IApiService<IReportSchedule>,
    @inject(TYPES.services.ReportQueryService) private reportQueryService: IReportQueryService,
    @inject(TYPES.services.ReportGeneratorService) private reportGeneratorService: IReportGeneratorService,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  listen = () => {
    // delay to give rabbit chance to start
    setTimeout(() => {
      this.messageService
        .receive(queueNames.reportSchedules)
        .subscribe(this.handleReportScheduleUpdate, (error) =>
          console.error('Error receiving report schedule update: Error %s', error)
        );
    }, 5000);
  };

  private handleReportScheduleUpdate = (reportSchedule: IReportSchedule) => {
    const job = this.jobs.get(reportSchedule._id);

    if (job) {
      job.cancel();
    }

    // check if schedule exists
    this.reportScheduleService.get(reportSchedule._id).then((schedule) => {
      if (schedule) {
        this.scheduleReport(reportSchedule);
      }
    });
  };

  scheduleReports = (): Promise<boolean[][]> => {
    return this.companyService.getAll().then((companies) => {
      return Promise.all(companies.map((company) => this.scheduleCompanyReports(company._id)));
    });
  };

  scheduleCompanyReports = (companyId: string): Promise<boolean[]> => {
    return this.reportScheduleService.getAll(companyId).then((reportSchedules) => {
      return Promise.all(reportSchedules.map((reportSchedule) => this.scheduleReport(reportSchedule)));
    });
  };

  scheduleReport = (reportSchedule: IReportSchedule): Promise<boolean> => {
    return new Promise((resolve, reject) => {
      const job = schedule.scheduleJob(reportSchedule.cron, () => {
        const query: (companyId: string, reportType: IReportType) => Promise<IReportQueryResult> =
          this.reportQueryService.queries[reportSchedule.reportType.query];

        query(reportSchedule._companyId, reportSchedule.reportType)
          .then((queryResult) => {
            return this.reportGeneratorService.generate(queryResult);
          })
          .then((report: any) => {
            return this.messageService.send(queueNames.sendEmail, <IEmail>{
              from: 'admin@greenpasscompliance.co.uk',
              recipients: reportSchedule.recipients.map((user) => user.email),
              subject: reportSchedule.reportType.name,
              content: report,
            });
          })
          .then((sent) => {
            return resolve(sent);
          })
          .catch((err) => {
            return reject(err);
          });
      });

      this.jobs.set(reportSchedule._id, job);
    });
  };
}
