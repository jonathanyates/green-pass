import { injectable, inject } from 'inversify';
import { IRepository } from '../db/db.repository';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { IAssessmentModel } from './assessment.schema';
import { IAssessment } from '../../../shared/interfaces/assessment.interface';

@injectable()
export class AssessmentRepository implements IRepository<IAssessmentModel> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  getAll(): Promise<IAssessmentModel[]> {
    return this.dbContext.assessment.find().exec();
  }

  get(id: string): Promise<IAssessmentModel> {
    return this.dbContext.assessment.findById(id).exec();
  }

  findOne(criteria: any): Promise<IAssessmentModel> {
    return this.dbContext.assessment.findOne(criteria).exec();
  }

  find(criteria: any): Promise<IAssessmentModel[]> {
    return this.dbContext.assessment.find(criteria).exec();
  }

  add<T extends IAssessment>(assessment: IAssessment): Promise<IAssessmentModel> {
    const model = new this.dbContext.assessment(assessment);
    return model.save();
  }

  update<T extends IAssessmentModel>(assessment: IAssessmentModel): Promise<IAssessmentModel> {
    return this.dbContext.assessment.findByIdAndUpdate(assessment._id, assessment, { new: true }).exec();
  }

  remove<T extends IAssessment>(assessmentId: string): Promise<IAssessmentModel> {
    return this.dbContext.assessment.findByIdAndRemove(assessmentId).exec();
  }
}
