import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {
  ClearCompanyInsuranceAction,
  SelectCompanyInsuranceAction
} from "../../../store/company/company-insurance.actions";

@Injectable()
export class CompanyInsuranceResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let id = route.params['id'];
    let insuranceId = route.params['insuranceId'];

    if (!id) return false;

    this.store.dispatch(new SelectCompanyAction(id));

    if (insuranceId) {
      this.store.dispatch(new SelectCompanyInsuranceAction(id, insuranceId));
    } else {
      if (route.url[route.url.length-1].path == 'new') {
        this.store.dispatch(new ClearCompanyInsuranceAction());
      }
    }

    return true;
  }

}
