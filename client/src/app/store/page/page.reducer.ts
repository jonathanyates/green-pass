import * as actions from "./page.actions";
import {ActionTypes} from "../authentication/authentication.actions";
import {PageState} from "./page.state";

const initialState: PageState = {
  page: null
};

export function PageReducer(state: PageState = initialState, action: actions.Actions): PageState {
    switch (action.type) {
      case actions.ActionTypes.LOAD_SUCCESS:
          return {
            page: action.payload
          };

      case actions.ActionTypes.CLEAR:
        return null;

      case ActionTypes.LOGOUT:
        return null;

      default:
        return state;
    }
  };
