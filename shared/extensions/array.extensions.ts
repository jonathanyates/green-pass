//export {}

// declare global {
//   interface Array<T> {
//     groupBy(prop):any;
//   }
// }
//
// Array.prototype.groupBy = function(prop) {
//   return this.reduce(function(groups, item) {
//     let val = item[prop];
//     groups[val] = groups[val] || [];
//     groups[val].push(item);
//     return groups;
//   }, {});
// };