import {IFleetInsurance} from "../../../../../shared/interfaces/common.interface";
import {ICompany} from "../../../../../shared/interfaces/company.interface";

export interface CompanyState {
  company: ICompany,
  selectedInsurance?: IFleetInsurance,
  selectedTab?: string
}
