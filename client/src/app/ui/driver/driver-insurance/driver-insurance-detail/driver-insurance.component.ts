import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectDriversState} from "../../../../store/drivers/driver.selectors";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {IInsurance} from "../../../../../../../shared/interfaces/common.interface";
import {SelectLoggedInUser} from "../../../../store/authentication/authentication.selectors";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-insurance',
  template: `
    <gp-fileRecord [fileRecord]="insurance" 
                   [header]="'Driver Insurance'" 
                   [fileHeading]="'Insurance Certificate'"
                   (edit)="edit($event)" 
                   (back)="back()" 
                   [allowEdit]="allowEdit"></gp-fileRecord>
  `
})
export class DriverInsuranceComponent implements OnInit, OnDestroy {

  insurance: IInsurance;
  user: IUser;
  allowEdit: boolean;
  private driver: IDriver;
  private subscriptions = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriversState())
      .filter(state => state.selectedInsurance != null)
      .subscribe(state => {
        this.driver = state.selected;
        this.insurance = state.selectedInsurance;
      }));

    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .combineLatest(this.store.let(SelectLoggedInUser()))
      .subscribe(results => {
        let company = results[0];
        this.user = results[1];
        let isDriver = this.user && this.user.role == Roles.Driver;
        this.allowEdit = isDriver === false ||
          (company.settings && company.settings.drivers && company.settings.drivers.allowInput);
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  edit(insurance: IInsurance) {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
