locals {

  do = {
    token = "dop_v1_185b6ec70cfa830f5839004ff66a6342e84bc11484fdf2f79ae58b3f7b92cecb"
    env = {
      default = {
        region      = "lon1"
        k8s_version = "1.25.4-do.0"
        size        = "s-1vcpu-2gb"
        node_count  = 2
      }
      dev = {
        region      = "lon1"
        k8s_version = "1.25.4-do.0"
        size        = "s-1vcpu-2gb"
        node_count  = 2
      }
      demo = {
        region      = "lon1"
        k8s_version = "1.25.4-do.0"
        size        = "s-2vcpu-2gb"
        node_count  = 2
      }
      prod = {
        region      = "lon1"
        k8s_version = "1.25.4-do.0"
        size        = "s-2vcpu-2gb"
        node_count  = 2
      }
    }
  }

  clustername = "gp-k8s"

  rabbitmq_username = "admin"
  rabbitmq_password = "Everest99"

  docker_registry_server   = "https://index.docker.io/v1/"
  docker_registry_username = "greenpass"
  docker_registry_password = "Everest99"
  docker_registry_email    = "jonathan.yates@greenpasscompliance.co.uk"

  # env = {
  #   default = {
  #     node_pool_size = "Standard_DS2_v2"
  #     node_count     = 1
  #     location       = "eastus"
  #   }
  #   dev = {
  #     node_pool_size = "Standard_DS2_v2"
  #     node_count     = 1
  #     location       = "uksouth"
  #   }
  #   demo = {
  #     node_pool_size = "Standard_DS2_v2"
  #     node_count     = 1
  #     location       = "ukwest"
  #   }
  #   prod = {
  #     node_pool_size = "Standard_DS2_v2"
  #     node_count     = 2
  #     location       = "westeurope"
  #   }
  # }

  # azure_tenant_id       = "79c7208c-4b9b-4629-ba85-a39f743e7a34"
  # azure_subscription_id = "7aeaa589-6566-429d-992f-48f49ed7a0ad"

  # external_dns_name                   = "gp-external-dns"
  # aks_service_principal_client_id     = "97292855-576f-4d05-b183-c9e19b6c6320"
  # aks_service_principal_client_secret = "uIH8Q~Iu_etxp8gYjbTw.c7EyexVjZ~5WWR-abk~"

  # external_dns_vars = {
  #   owner_id        = azurerm_kubernetes_cluster.gp-k8s-cluster.name
  #   resource_group  = azurerm_resource_group.gp-resource-group.name,
  #   tenant_id       = local.azure_tenant_id,
  #   subscription_id = local.azure_subscription_id,
  #   log_level       = "trace",
  #   domain          = "greenpasscompliance.co.uk"
  # }

  # external_dns_values = templatefile(
  #   "${path.module}/templates/external_dns_values.yaml.tmpl",
  #   local.external_dns_vars
  # )
}
