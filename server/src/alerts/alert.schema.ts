import mongoose from 'mongoose';
import { IAlert } from '../../../shared/interfaces/alert.interface';

export const AlertSchema = new mongoose.Schema({
  _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
  alertType: { type: mongoose.Schema.Types.ObjectId, ref: 'AlertType', required: true },
  created: { type: Date, required: true },
  updated: { type: Date, required: false },
  status: { type: String, required: true },
  comment: { type: String, required: false },
  driver: { type: mongoose.Schema.Types.ObjectId, ref: 'Driver', required: false },
  vehicle: { type: mongoose.Schema.Types.ObjectId, ref: 'Vehicle', required: false },
});

export interface IAlertModel extends mongoose.Document, IAlert {
  _id: any;
}
