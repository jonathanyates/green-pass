import {Component, OnInit} from '@angular/core';
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {SelectTemplate} from "../../../store/templates/template.selectors";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {SelectIsGreenPassAdmin} from "../../../store/authentication/authentication.selectors";

@Component({
  selector: 'gp-template-container',
  template: `
    <gp-card [header]="template ? template.name : null" (edit)="editTemplate()" [allowEdit]="allowEdit">
      <gp-template [template]="template"></gp-template>      
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">
      <i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Back</button>
  `
})

export class TemplateContainerComponent implements OnInit {

  template: ITemplate;
  allowEdit: boolean;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.store.let(SelectTemplate())
      .subscribe(template => this.template = template);

    this.store.let(SelectIsGreenPassAdmin())
      .subscribe(isAdmin => this.allowEdit = isAdmin);
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  editTemplate() {
    this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }
}
