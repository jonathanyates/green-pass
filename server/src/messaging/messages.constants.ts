export const queueNames = {
  reportSchedules: 'ReportSchedules',
  companyComplianceUpdate: 'CompanyComplianceUpdate',
  sendEmail: 'sendEmail',
  driverReminderSchedules: 'driverReminderSchedules'
};