import {Component, EventEmitter, Input, Output} from "@angular/core";
import {ICompanySettings, IPointsCheckPeriod} from "../../../../../../shared/interfaces/company-settings.interface";
import {jsonDeserialiser} from "../../../../../../shared/utils/json.deserialiser";
import {ActivatedRoute, Router} from "@angular/router";
import {Cron} from "../../shared/components/cron/cron.model";
import {
  DEFAULT_DRIVER_REMINDER_CRON,
  defaultSettingsFactory
} from "../../../../../../shared/interfaces/company-settings.factory";

@Component({
  selector: 'gp-company-settings-input ',
  templateUrl: 'company-settings-input.component.html'
})
export class CompanySettingsInputComponent {

  private _settings: ICompanySettings;
  licenceCheckPeriods: IPointsCheckPeriod[];
  driverReminderCron: Cron;

  @Input()
  set settings(settings: ICompanySettings) {
    if (settings != null) {
      this._settings = jsonDeserialiser.parse(JSON.stringify(settings));
      if (!this._settings.drivers.reminders) {
        this._settings.drivers.reminders = {
          sendEmails: false,
          cron: DEFAULT_DRIVER_REMINDER_CRON
        }
      }

      if (this.settings.drivers.licenceChecks && this.settings.drivers.licenceChecks.checkPeriods) {
        this.licenceCheckPeriods = this.settings.drivers.licenceChecks.checkPeriods;
      } else {
        this.licenceCheckPeriods = [];
      }

    } else {
      this._settings = defaultSettingsFactory();
    }

    this.driverReminderCron = Cron.fromString(this._settings.drivers.reminders.cron);
  }
  get settings() {
    return this._settings;
  }

  @Output() save: EventEmitter<ICompanySettings> = new EventEmitter<ICompanySettings>();

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  onSave() {
    this.settings.drivers.reminders.cron = this.driverReminderCron.toString();

    if (!this.settings.drivers.licenceChecks) {
      this.settings.drivers.licenceChecks = { checkPeriods: [] };
    }

    if (this.licenceCheckPeriods !== this.settings.drivers.licenceChecks.checkPeriods) {
      this.settings.drivers.licenceChecks.checkPeriods = this.licenceCheckPeriods;
    }
    this.save.emit(this.settings)
  }

  back() {
    return this.router.navigate(['../'], { relativeTo:  this.route});
  }

  pointsChange(period: IPointsCheckPeriod, value: number) {
    period.noOfPoints = Number(value);
  }

  monthsChange(period: IPointsCheckPeriod, value: number) {
    period.months = Number(value);
  }

  addPointsPeriod() {
    this.licenceCheckPeriods.push({ noOfPoints: 0, months: 12 });
  }

  removePointsPeriod(period: IPointsCheckPeriod) {
    this.licenceCheckPeriods = this.licenceCheckPeriods.filter(p => p !== period);
  }

}
