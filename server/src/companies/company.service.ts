import { ICompany } from '../../../shared/interfaces/company.interface';
import { ICompanyService } from './company.service.interface';
import { injectable, inject } from 'inversify';
import TYPES from '../container/container.types';
import { IFileService } from '../files/file.service';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';
import { IDbContext } from '../db/db.context.interface';
import { IDriverService } from '../drivers/driver.service';
import { IVehicleApiService } from '../vehicles/vehicle.service';
import { Roles } from '../../../shared/constants';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IAlertType } from '../../../shared/interfaces/alert.interface';
import { reportNames } from '../reports/report-query.service';
import { IAlertService } from '../alerts/alert.service';
import { IFleetInsurance } from '../../../shared/interfaces/common.interface';
import { ICompanyRepository } from './company.repository';

@injectable()
export class CompanyService implements ICompanyService {
  constructor(
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.DriverService) private driverService: IDriverService,
    @inject(TYPES.services.VehicleService) private vehicleService: IVehicleApiService,
    @inject(TYPES.services.FileService) private fileService: IFileService,
    @inject(TYPES.services.MessageService) private messageService: IMessageService,
    @inject(TYPES.services.AlertService) private alertService: IAlertService,
    @inject(TYPES.db.DbContext) private dbContext: IDbContext
  ) {}

  getAll = (): Promise<ICompany[]> => {
    return this.companyRepository.getAll();
  };

  get = (id: string): Promise<ICompany> => {
    return Promise.all([
      this.companyRepository.get(id),
      this.dbContext.driver.count({ _companyId: id, removed: false }).exec(),
      this.dbContext.vehicle.count({ _companyId: id, removed: false }).exec(),
      this.dbContext.user.count({ _companyId: id, role: Roles.Company, removed: false }).exec(),
    ]).then((results) => {
      const company = results[0];
      const drivers = results[1];
      const vehicles = results[2];
      const users = results[3];
      if (company) {
        company.drivers = drivers;
        company.vehicles = vehicles;
        company.users = users;
      }
      return company;
    });
  };

  add = (company: ICompany): Promise<ICompany> => {
    return this.updateAlertTypes(company).then((company) => this.companyRepository.add(company));
  };

  update = (company: ICompany): Promise<ICompany> => {
    return this.get(company._id).then((existing) => {
      // if number of subscription alerts has changed then update alert types.
      if (company.subscription.resources.alerts !== existing.subscription.resources.alerts) {
        return this.updateAlertTypes(company).then((company) => this.companyRepository.update(company));
      }

      return this.companyRepository.update(company).then((company) => {
        // if driver reminders setting has changed then send message
        if (company.settings.drivers.reminders != null) {
          if (
            existing.settings.drivers.reminders == null ||
            company.settings.drivers.reminders.sendEmails != existing.settings.drivers.reminders.sendEmails ||
            company.settings.drivers.reminders.cron != existing.settings.drivers.reminders.cron
          ) {
            this.messageService.send(queueNames.driverReminderSchedules, {
              companyId: company._id,
              reminders: company.settings.drivers.reminders,
            });
          }
        }

        return company;
      });
    });
  };

  private updateAlertTypes = (company: ICompany): Promise<ICompany> => {
    return this.alertService.getAlertTypes().then((alertTypes) => {
      if (company.subscription.resources.alerts <= 2) {
        // for bronze (zero alerts) add only getNoneCompliantDrivers & getNoneCompliantVehicles alert types
        company.alertTypes = [
          { alertType: alertTypes.find((x: IAlertType) => x.reportType.name == reportNames.getNoneCompliantDrivers) },
          { alertType: alertTypes.find((x: IAlertType) => x.reportType.name == reportNames.getNoneCompliantVehicles) },
        ];
      } else if (company.subscription.resources.alerts === -1) {
        // unlimited
        company.alertTypes = alertTypes.map((alertType) => ({ alertType: alertType }));
      } else {
        // for all other subscriptions add alertTypes up to the allowed number of alerts
        if (alertTypes.length <= company.subscription.resources.alerts) {
          company.alertTypes = alertTypes.map((alertType) => ({ alertType: alertType }));
        } else {
          // always include none compliance alerts.

          const companyAlertTypes = alertTypes.filter(
            (alertType: IAlertType) =>
              alertType.reportType.name === reportNames.getNoneCompliantDrivers ||
              alertType.reportType.name === reportNames.getNoneCompliantVehicles
          );

          const remaining = alertTypes.length - 2;

          if (remaining > 0) {
            const remainingAlertTypes = alertTypes
              .filter(
                (alertType: IAlertType) =>
                  alertType.reportType.name !== reportNames.getNoneCompliantDrivers &&
                  alertType.reportType.name !== reportNames.getNoneCompliantVehicles
              )
              .slice(0, company.subscription.resources.alerts - 2);

            companyAlertTypes.push(...remainingAlertTypes);
          }

          company.alertTypes = companyAlertTypes.map((alertType) => ({ alertType: alertType }));
        }
      }

      return company;
    });
  };

  remove = (companyId: string): Promise<ICompany> => {
    return this.companyRepository
      .remove(companyId)
      .then((company) =>
        Promise.all([
          this.driverService
            .getAll(company._id)
            .then((drivers) => Promise.all(drivers.map((driver) => this.driverService.remove(driver._id)))),
          this.vehicleService
            .getAll(company._id)
            .then((vehicles) => Promise.all(vehicles.map((vehicle) => this.vehicleService.remove(vehicle._id)))),
        ]).then((drivers) => company)
      );
  };

  addInsurance(companyId: string, insurance: IFleetInsurance): Promise<IFleetInsurance> {
    return this.dbContext.company.findById(companyId).then((company) => {
      const file = insurance.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const vehicleInsurance = {
        validFrom: new Date(insurance.validFrom),
        validTo: insurance.validTo,
        filename: filename,
        fileType: insurance.fileType,
      };

      if (!company.fleetInsuranceHistory) {
        company.fleetInsuranceHistory = [];
      }
      company.fleetInsuranceHistory.push(vehicleInsurance);
      insurance = company.fleetInsuranceHistory.find((insurance) => insurance.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          vehicleId: company._id,
          insuranceId: insurance._id,
        },
      };

      if (path) {
        return this.fileService.createFile(filename, path, options).then((file) =>
          company.save().then((company) => {
            return this.messageService
              .send(queueNames.companyComplianceUpdate, company._id)
              .then((sent) => insurance)
              .catch((err) => insurance);
          })
        );
      }

      return company.save().then((company) => {
        return this.messageService
          .send(queueNames.companyComplianceUpdate, company._id)
          .then((sent) => insurance)
          .catch((err) => insurance);
      });
    });
  }

  updateInsurance(companyId: string, insuranceId: string, insurance: any): Promise<IFleetInsurance> {
    return this.dbContext.company.findById(companyId).then((company) => {
      let vehicleInsurance = company.fleetInsuranceHistory.find((item) => item._id.toString() == insuranceId);

      if (!vehicleInsurance) {
        throw ServerError(`Insurance ${insuranceId} not found for company ${companyId}.`, codes.NOT_FOUND);
      }

      const file = insurance.file;
      const filename = vehicleInsurance.filename ? vehicleInsurance.filename : file ? file.filename : null;

      vehicleInsurance.filename = filename;
      vehicleInsurance.validFrom = new Date(insurance.validFrom);
      vehicleInsurance.validTo = insurance.validTo;
      vehicleInsurance.fileType = insurance.fileType;

      return <Promise<IFleetInsurance>>company
        .save()
        .then((vehicle) => {
          vehicleInsurance = vehicle.fleetInsuranceHistory.find((item) => item._id.toString() == insuranceId);

          if (file) {
            const path = file.path;

            const options = {
              filename: filename,
              metadata: {
                vehicleId: vehicle._id,
                insuranceId: insuranceId,
              },
            };

            return this.fileService.updateFile(filename, path, options).then((x) => vehicleInsurance);
          }

          return vehicleInsurance;
        })
        .then((_) => {
          return this.messageService
            .send(queueNames.companyComplianceUpdate, company._id)
            .then((sent) => vehicleInsurance)
            .catch((err) => vehicleInsurance);
        });
    });
  }
}
