import _ = require('lodash');
import logger = require('winston');
import mongoose from 'mongoose';
import { inject, injectable } from 'inversify';
import { TYPES } from '../container/container.types';
import { AlertStatus, IAlert, IAlertType } from '../../../shared/interfaces/alert.interface';
import { IAlertService } from './alert.service';
import { ICompanyService } from '../companies/company.service.interface';
import { IReportQueryService } from '../reports/report-query.service';
import { IReportQueryResult, IReportType, ReportCategory } from '../../../shared/interfaces/report.interface';
import { ICompany } from '../../../shared/interfaces/company.interface';

export interface IAlertQueryService {
  updateAllCompanies(): Promise<IAlert[][][]>;

  updateAllAlerts(companyId: any): Promise<IAlert[][]>;

  updateAlert(companyId: any, alertType: IAlertType): Promise<IAlert[]>;
}

@injectable()
export class AlertQueryService implements IAlertQueryService {
  constructor(
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
    @inject(TYPES.services.AlertService) private alertService: IAlertService,
    @inject(TYPES.services.ReportQueryService) private reportQueryService: IReportQueryService
  ) {}

  updateAllCompanies = (): Promise<IAlert[][][]> => {
    return this.companyService.getAll().then((companies) => {
      return Promise.all(
        companies.map((company) => {
          const alertTypes = company.alertTypes.filter((item) => item.alertType).map((item) => item.alertType);
          return Promise.all(alertTypes.map((alertType) => this.updateAlert(company._id.valueOf(), alertType)));
        })
      );
    });
  };

  updateAllAlerts = (companyId: any): Promise<IAlert[][]> => {
    return this.companyService.get(companyId).then((company) => {
      const alertTypes = company.alertTypes.map((item) => item.alertType);
      return this.removeAlerts(company).then((removedAlerts) => {
        return Promise.all(alertTypes.map((alertType) => this.updateAlert(companyId, alertType)));
      });
    });
  };

  removeAlerts = (company: ICompany) => {
    return this.alertService
      .find({
        $and: [{ _companyId: { $eq: new mongoose.Types.ObjectId(company._id) } }, { status: { $eq: 'Active' } }],
      })
      .then((alerts: IAlert[]) => {
        const alertsToRemove = alerts.filter(
          (alert) => !company.alertTypes.find((alertType) => alertType.alertType._id.equals(alert.alertType._id))
        );

        return Promise.all([
          alertsToRemove.map((alert) => {
            alert.status = 'Removed';
            console.log(`Set alert '${alert.alertType.reportType.name}' status to 'Removed'`);
            return this.alertService.update(alert);
          }),
        ]);
      });
  };

  updateAlert = (companyId: string, alertType: IAlertType): Promise<IAlert[]> => {
    if (!companyId || !alertType) {
      return Promise.resolve([]);
    }

    const alertTypeId = alertType._id.toString();

    return this.alertService
      .find({
        $and: [
          { _companyId: { $eq: new mongoose.Types.ObjectId(companyId) } },
          { 'alertType._id': { $eq: new mongoose.Types.ObjectId(alertTypeId) } },
          { status: { $eq: 'Active' } },
        ],
      })
      .then((existingAlerts: IAlert[]) => {
        let query: (companyId: string, reportType: IReportType) => Promise<IReportQueryResult> = null;

        try {
          if (alertType && alertType.reportType && alertType.reportType.query) {
            query = this.reportQueryService.queries[alertType.reportType.query];
          } else {
            logger.error('Failed to update alert. Report query not specified on alert type.');
            return null;
          }
        } catch (e) {
          logger.error('Failed to access report query ' + alertType.reportType.query);
        }

        if (!query) return null;

        // get the latest results for the alert type
        return query(companyId, alertType.reportType)
          .then((queryResult: IReportQueryResult) => {
            if (queryResult.result && queryResult.result.length > 0) {
              return queryResult.result.map(
                (item) =>
                  <IAlert>{
                    _companyId: companyId,
                    alertType: alertType,
                    created: new Date(),
                    status: AlertStatus.Active,
                    driver: queryResult.reportType.category === ReportCategory.Driver ? item : null,
                    vehicle: queryResult.reportType.category === ReportCategory.Vehicle ? item : null,
                  }
              );
            }
            return [];
          })
          .then((alerts: IAlert[]) => {
            // get existing alerts that are not alerts anymore so we can set them to resolved
            // i.e. where latest alerts doesn't contain existing alert.
            // to do this we find in alerts where the drivers are the same or the vehicles are the same
            // and only filter on the ones which don't match (are null)
            const resolvedAlerts = existingAlerts.filter((existing) => {
              const resolved = alerts.find(
                (latest) =>
                  (latest.driver && existing.driver && latest.driver._id.equals(existing.driver._id)) ||
                  (latest.vehicle && existing.vehicle && latest.vehicle._id.equals(existing.vehicle._id))
              );
              const result = resolved == null;
              return result;
            });

            // set any resolved alerts status to Resolved
            const resolved = resolvedAlerts.map((resolvedAlert) => {
              resolvedAlert.status = AlertStatus.Resolved;
              return this.alertService.update(resolvedAlert);
            });

            // get existing alerts that are still alerts so we can update the updated date
            // same as above logic but filter on ones that DO match
            const updatedAlerts = existingAlerts
              .filter((existing) => {
                const alert: IAlert = alerts.find(
                  (latest) =>
                    (latest.driver && existing.driver && latest.driver._id.equals(latest.driver._id)) ||
                    (latest.vehicle && existing.vehicle && latest.vehicle._id.equals(latest.vehicle._id))
                );

                if (alert) {
                  existing.updated = alert.created;
                }
                return alert != null;
              })
              .sort((a, b) => b.created.getTime() - a.created.getTime());

            // It is possible that duplicate alerts could be added if this is run at the same time by multiple requests.
            //
            // resolve any duplicate alerts cos there should only be one per driver per alert type
            // let duplicateDriverAlerts = _.groupBy<IAlert, string>(updatedAlerts.filter(x => x.driver), x => x.driver._id);
            // for(let group in duplicateDriverAlerts) {
            //   let driverAlerts = duplicateDriverAlerts[group];
            //   if (driverAlerts.length > 0) {
            //     let dupDrivers = driverAlerts.slice(1, driverAlerts.length);
            //     dupDrivers.forEach(driverAlert => driverAlert.status = AlertStatus.Resolved);
            //   }
            // }

            // resolve any duplicate alerts cos there should only be one per vehicle per alert type
            // let duplicateVehicleAlerts = _.groupBy<IAlert, string>(updatedAlerts.filter(x => x.vehicle), x => x.vehicle._id);
            // for(let group in duplicateVehicleAlerts) {
            //   let vehicleAlerts = duplicateVehicleAlerts[group];
            //   if (vehicleAlerts.length > 0) {
            //     let dupVehicles = vehicleAlerts.slice(1, vehicleAlerts.length);
            //     dupVehicles.forEach((vehicleAlert:IAlert) => vehicleAlert.status = AlertStatus.Resolved);
            //   }
            // }

            const updated = updatedAlerts.map((updatedAlert) => {
              return this.alertService.update(updatedAlert);
            });

            // create new alerts if there isn't one already
            // get alerts where there isn't an existing one
            const newAlerts = alerts.filter(
              (latest) =>
                existingAlerts.find(
                  (existing) =>
                    (existing.driver && latest.driver && existing.driver._id.equals(latest.driver._id)) ||
                    (existing.vehicle && latest.vehicle && existing.vehicle._id.equals(latest.vehicle._id))
                ) == null
            );

            const added = newAlerts.map((newAlert) => this.alertService.add(companyId, newAlert));

            const promises = resolved.concat(updated).concat(added);

            return Promise.all(promises);
          });
      });
  };
}
