import { FormatDate } from '../../../../shared/utils/helpers';

export const getStylesHtml = () => {
  return `
  <style>
      li { margin-bottom: 20px; }
  </style>
  `;
};

export const getSignatureHtml = (name: string) => {
  const date = FormatDate(new Date());

  return `
<style>
    table {
        margin-top: 40px;
        margin-bottom: 20px;
    }
    .signature-box {
        display:block;
        color: #000000;
        background-color: #ffff99;
        border: 1px solid #00A550;
        border-radius: 2px;
        padding: 10px;
        cursor: pointer;
    }    
    .sigPad input {
      width: 230px;
      padding-left: 2px;
      background: #ffff99;
      border: 1px solid #00A550;
      border-radius: 2px;
    }    
    .sigWrapper {
      margin-bottom: 1rem;
      margin-top: 1rem;
    }    
    .btn-default {
      background-color: #00A550;
      border-color: #00A550;
      border-radius: 2px;
      color: white;
    }
    .btn-default:hover, .btn-default:active, .btn-default.active {
      background-color: #00bf5c !important;
      color: white !important;
    }
    .btn-default:focus {
      background-color: #00592b !important;
      color: white !important;
    }
    .typed1 {
     padding: 0 !important;
    }
</style>

<p><strong>CONFIRMATION</strong></p>
<p>I confirm that I have received, read and understood the organizations mandate policy.</p>
<p>I agree to be bound by the rules contained in the mandate policy and I will advise the organization of any changes which affect my legal or medical fitness to drive.</p>
<p>I accept that it is my responsibility to ensure that I am legally and medically fit to drive.</p>

<table>
    <tbody>
    <tr>
        <td>Signature:</td>
        <td style="padding-left: 10px;">
            <span class="field" data-idx="0" data-optional="false" data-name="Please add your signature here"
                  data-type="signature" data-options="" data-signee="1">
                <span class="signature-box">Click here to Sign</span>
            </span>
        </td>
    </tr>
    <tr>
        <td>Name:</td>
        <td style="padding-left: 10px; padding-top: 15px; padding-bottom: 15px;">${name}</td>
    </tr>
    <tr>
        <td>Date:</td>
        <td style="padding-left: 10px; padding-top: 15px; padding-bottom: 15px;">${date}</td>
    </tr>
    </tbody>
</table>
  `;
};

export const getStyles = () => {
  return `
<style>
    * { font-family: 'Roboto', sans-serif; }
    h1 { font-size: 2em; }
    h2 { font-size: 1.5em; }
    h3 { font-size: 1.17em; }
    h4 { font-size: 1.12em; }
    h5 { font-size: .83em; }
    h6 { font-size: .75em; }
    li { margin-bottom: 20px; }
    .center-div  { margin: 0 auto; width: 400px; text-align: center; }
    .header-text { margin-bottom: 20px; }
    .inline-block { display: inline-block; }
</style>
  `;
};

// As a temporary measure until fully hosted the image is being hosted on ibb at
// https://ibb.co/rbpZD3d
// https://i.ibb.co/GsMVz9H/green-pass-80.png
// This should be moved eventually to
// https://greenpasscompliance.co.uk/public/img/green-pass-80.png

export const getHeaderHtml = (company: string, signer: string, title: string) => {
  const date = FormatDate(new Date());

  return `
<div style="margin-top: 0; margin-bottom: 4rem;">
    <div class="center-div">
        <img src="https://demo.greenpasscompliance.co.uk/public/img/green-pass-80.png" width="250px">
        <div class="header-text">
            <h1>${title}</h1>
        </div>
        <div class="header-text">
            <div class="inline-block">The Organization:&nbsp;</div>
            <span>${company}</span>
        </div>
        <div class="header-text">
            <div class="inline-block">The Driver:&nbsp;</div>
            <span>${signer}</span>
        </div>
        <div class="header-text">
            <div class="inline-block">Date:&nbsp;</div>
            <span>${date}</span>
        </div>
    </div>
</div>

<div style="margin-bottom: 4rem;">
    <p>The mandate statements issued within this policy by &lsquo;The Organization&rsquo; underlines the absolute commitment to create and maintain a safe working environment. When adhered to they will so far as reasonably practicable, help to safeguard the interests of &lsquo;The Organisation&rsquo; and health and safety of other road users including employees, associates and Directors who drive a company vehicle including your own vehicle whilst on organisational business.</p>
    <p>There is a legal duty for all employees of &lsquo;The Organisation&rsquo; to follow company guidance; taking reasonable care for themselves and for the safety of others whilst at work and this includes driving during the course of their business.</p>
    <p>It is of vital importance to &lsquo;The Organization&rsquo; they you have fully read, understood and comply with all the policy in order to help &lsquo;The Organization&rsquo; meet their duty of care under the current legal framework.</p>
</div>
`;
};
