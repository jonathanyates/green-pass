import '../../../../../../../shared/extensions/date.extensions';
import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Company} from "../../../company/company.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Messages, pagination} from "../../../../shared/constants";
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {DriverListBuilder} from "../../../../../../../shared/models/driver-list.builder";
import {Messenger} from "../../../../services/messenger";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {ColumnType} from "../../../../../../../shared/models/column-type.model";
import {DynamicListComponent} from "../dynamic-list/dynamic-list.component";
import {SubscriptionHelper} from "../../../../../../../shared/models/subscription.model";

@Component({
  selector: 'gp-drivers',
  template: `
    <gp-dynamic-list [list]="list" (add)="onAdd()" [serverMode]="serverMode" [lightHeader]="lightHeader"
                     [total]="total" [page]="page" [itemsPerPage]="itemsPerPage" (pageChange)="onPageChange($event)"
                     (itemSelected)="select($event)"></gp-dynamic-list>
  `
})
export class DriverListComponent implements OnInit {

  @Output() back: EventEmitter<any> = new EventEmitter();
  @Input() company: Company;
  @Input() total: number;
  @Input() page: number = 1;
  @Input() itemsPerPage: number = pagination.pageSize;
  @Input() serverMode: boolean;
  @Input() lightHeader: boolean = false;
  @Input() showSearch: boolean = true;

  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  @Output() add = new EventEmitter();
  @Output() remove: EventEmitter<IDriver> = new EventEmitter<IDriver>();
  @Output() itemSelected: EventEmitter<IDriver> = new EventEmitter<IDriver>();

  @ViewChild(DynamicListComponent) listComponent: DynamicListComponent;

  @Input() set drivers(drivers: IDriver[]) {
    if (drivers) {
      this.setList(drivers);
    }
  }

  list: IDynamicList;
  private subscriptions = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private messenger: Messenger) {
  }

  ngOnInit() {

    if (this.showSearch) {
      this.messenger.publish(Messages.ShowSearch, true);
    }

    this.subscriptions.push(this.messenger.toObservable(Messages.SearchTerm)
      .subscribe(term => this.search.emit(term)));

    if (this.back.observers.length > 0) {
      this.listComponent.back.subscribe(e => this.onBack());
    }
  }

  setList(drivers: IDriver[]) {
    let subscription = SubscriptionHelper.getSubscription(this.company);
    this.list = DriverListBuilder.getList(drivers, subscription, this.company.settings);
    this.addActions();
  }

  addActions() {
    if (this.remove.observers.length > 0) {
      this.list.columns.push({header: '', field: 'remove', columnType: ColumnType.Icon});
      this.list.actions = this.list.items.map(item => ({
        remove: {
          action: args => { if (args) this.onRemove(args) },
          icon: 'fa fa-trash-o green-text'
        }
      }));
    }
  }

  onRemove(args) {
    if (!args && !args.item) return;
    this.remove.emit(args.item);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onAdd() {
    this.add.emit();
  }

  select(driver: IDriver) {
    if (driver) {
      if (this.itemSelected.observers.length > 0) {
        this.itemSelected.emit(driver);
        return;
      }
      return this.router.navigate(['./', driver._id], { relativeTo: this.route });
    }
  }

  onBack() {
    this.back.emit();
  }

  onPageChange(page: number) {
    this.pageChange.emit(page);
  }

}
