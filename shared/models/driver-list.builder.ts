import '../../shared/extensions/date.extensions';
import {IDriver} from "../interfaces/driver.interface";
import {AssessmentSummary} from "./assessment-summary.model";
import {FormatDate, GetDateStyle} from "../utils/helpers";
import {User} from "../../client/src/app/ui/user/user.model";
import {ModuleSummary} from "./module-summary.model";
import {ISubscription} from "../interfaces/subscription.interface";
import {ICompany} from "../interfaces/company.interface";
import {ICompanySettings} from "../interfaces/company-settings.interface";

export class DriverListBuilder {

  static getList(drivers: IDriver[], subscription: ISubscription, settings: ICompanySettings) {
    return {
      columns: DriverListBuilder.getColumns(drivers, subscription, settings),
      items: drivers.map((driver) => DriverListBuilder.mapDriver(driver, settings)),
      styles: drivers.map((driver) => DriverListBuilder.mapDriverStyle(driver, settings)),
      actions: []
    };
  }

  static getColumns(drivers: IDriver[], subscription: ISubscription, settings: ICompanySettings) {
    let columns = [
      {header: 'Name', field: 'fullname'},
      {header: 'Details', field: 'driverDetails'},
      {header: 'Next Of Kin', field: 'nextOfKin'},
      {header: 'Documents', field: 'documents'},
      {header: 'Licence', field: 'licence'}
    ];

    // if (settings.drivers.includePNumber) {
    //   columns.splice(2, 0, {header: 'PNumber', field: 'pNumber'});
    // }

    if (settings.drivers.references) {
      columns.splice(2, 0, {header: 'References', field: 'references'});
    }

    if (subscription && subscription.resources) {
      if (subscription.resources.onLineAssessments || subscription.resources.onRoadTraining) {
        columns.push({header: 'Training', field: 'assessmentResult'});
        // columns.push({header: 'Assessment Due', field: 'nextAssessmentDate'})
      }
    }

    if (drivers.some(driver => driver.ownedVehicle == true)) {
      columns.push({header: 'Vehicle', field: 'vehicle'});
    }

    columns.push({header: 'Insurance', field: 'insurance'});

    if (settings.drivers.includeFaw) {
      columns.splice(8, 0, {header: 'FAW', field: 'faw'});
    }

    if (settings.drivers.includeDbs) {
      columns.splice(9, 0, {header: 'DBS', field: 'dbs'});
    }

    columns.push({header: 'Compliant', field: 'compliant'});

    return columns;
  }

  static mapDriver(driver: IDriver, settings: ICompanySettings) {
    if (!driver) return;

    let documentsCompleted = driver.documents && driver.documents.length > 0
      ? driver.documents.filter(document => document.signed).length
      : null;

    let item: any = Object.assign({}, driver, {
      fullname: User.getFullName(driver._user),
      driverDetails: driver.compliance != null && driver.compliance.details === true
        ? 'Complete'
        : 'Incomplete',
      nextOfKin: driver.compliance != null && driver.compliance.nextOfKin === true
        ? 'Complete'
        : 'Incomplete',
      documents: documentsCompleted != null
        ? `${driver.documents.filter(document => document.signed).length} of ${driver.documents.length} signed`
        : `No Documents`
    });

    item.vehicle = driver.ownedVehicle == true
      ? driver.vehicles == null || driver.vehicles.length === 0
        ? 'Required' : 'OK'
      : 'n/a';

    item.insurance = driver.compliance != null && driver.compliance.insurance != null
      ? driver.insuranceHistory && driver.insuranceHistory.length > 0
        ? FormatDate(driver.insuranceHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0].validTo)
        : 'Required'
      : 'n/a';

    item.licence = driver.licence && driver.licence.checks && driver.licence.checks.length > 0
      ? new Date().getDayDiff(driver.licence.nextCheckDate) > 0
        ? FormatDate(driver.licence.nextCheckDate)
        : FormatDate(driver.licence.nextCheckDate)
      : 'Required';

    if (settings.drivers.references) {
      item.references = driver.compliance != null && driver.compliance.references
        ? 'Complete'
        : 'Incomplete'
    }

    if (settings.drivers.includeFaw) {
      item.faw = driver.fawRequired === true
        ? driver.compliance.faw
           ? FormatDate(driver.fawHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0].validTo)
          : 'Required'
        : 'N/A';
    }

    if (settings.drivers.includeDbs) {
      item.dbs = driver.compliance.dbs && driver.dbsExpiry
        ? FormatDate(driver.dbsExpiry)
        : 'Required'
    }

    if (driver.assessments && driver.assessments.length > 0) {
      let assessment = driver.assessments.slice(-1)[0];

      if (assessment.complete) {
        item.assessmentResult = 'Complete'
      } else {
        if (assessment.onRoad != null && !assessment.onRoad.complete) {
          item.assessmentResult = 'On Road'
        } else {
          if (assessment.trainingModules && assessment.trainingModules.length > 0) {
            let moduleSummary = new ModuleSummary(assessment);
            item.assessmentResult = `${moduleSummary.completed}`;
          } else {
            let assessmentSummary = new AssessmentSummary(assessment);
            item.assessmentResult = `${assessmentSummary.completed}`;
          }
        }
      }

      item.nextAssessmentDate = driver.nextAssessmentDate
        ? FormatDate(driver.nextAssessmentDate)
        : 'Not set';
    } else {
      item.assessmentResult = 'None';
      item.nextAssessmentDate = 'Not set';
    }

    return item;
  }

  static mapDriverStyle(driver: IDriver, settings: ICompanySettings) {

    if (!driver) return;

    let completed = driver.documents.filter(document => document.signed).length;
    let percentage = driver.documents.length > 0
      ? (completed / driver.documents.length) * 100
      : 0;

    let styles = <any>{
      driverDetails: driver.compliance != null && driver.compliance.details == true
        ? 'badge success-color'
        : 'badge danger-color',
      nextOfKin: driver.compliance != null && driver.compliance.nextOfKin == true
        ? 'badge success-color'
        : 'badge danger-color',
      documents: driver.documents && driver.documents.length > 0
        ? percentage === 100
          ? 'badge success-color'
          : percentage >= 80
            ? 'badge warning-color'
            : 'badge danger-color'
        : '',
      vehicle: driver.ownedVehicle == true
        ? driver.vehicles == null || driver.vehicles.length === 0
          ? 'badge danger-color'
          : 'badge success-color'
        : '',
      insurance: driver.compliance != null && driver.compliance.insurance != null
        ? driver.compliance.insurance
          ? DriverListBuilder.getDateStyle(driver.insuranceHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0].validTo)
          : 'badge danger-color'
        : '',
      compliant: driver.compliance != null && driver.compliance.compliant
        ? 'fa fa-check green-text'
        : 'fa fa-times red-text'
    };

    styles.licence = driver.licence && driver.licence.checks && driver.licence.checks.length > 0
      ? DriverListBuilder.getDateStyle(driver.licence.nextCheckDate)
      : 'badge danger-color';

    if (settings.drivers.references) {
      styles.references = driver.compliance != null && driver.compliance.references
        ? 'badge success-color'
        : 'badge danger-color'
    }

    if (settings.drivers.includeFaw && driver.fawRequired === true) {
      let faw = driver.fawHistory != null && driver.fawHistory.length > 0
        ? driver.fawHistory.sort((a, b) => b.validTo.getTime() - a.validTo.getTime())[0]
        : null;

      styles.faw = faw
        ? DriverListBuilder.getDateStyle(faw.validTo)
        : 'badge danger-color'
    }

    if (settings.drivers.includeDbs) {
      styles.dbs = DriverListBuilder.getDateStyle(driver.dbsExpiry);
    }

    if (driver.assessments && driver.assessments.length > 0) {

      let assessment = driver.assessments.slice(-1)[0];

      styles.assessmentResult = assessment.complete
        ? 'badge success-color'
        : 'badge danger-color';

      let today = Date.prototype.getToday();

      styles.nextAssessmentDate = today.getDayDiff(driver.nextAssessmentDate) <= 0
        ? 'badge danger-color'
        : today.getDayDiff(driver.nextAssessmentDate) <= 7
          ? 'badge warning-color'
          : 'badge success-color';
    }

    return styles;
  }

  static getDateStyle(date: Date) {
    let style = GetDateStyle(date);
    return 'badge ' + (style ? style : 'danger-color');
  }
}


