import {Company} from '../company.model';
import {FormInputGroup} from '../../shared/components/dynamic-form/dynamic-form.model';
import {FormGroup} from '@angular/forms';
import {AddressInputMapper} from '../../shared/components/address-input/address-input.mapper';
import {Injectable} from '@angular/core';
import {RegExpPatterns} from '../../../../../../shared/constants';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {ISubscription} from '../../../../../../shared/interfaces/subscription.interface';
import {SubscriptionInputMapper} from '../subscription-input/subscription-input.mapper';

@Injectable()
export class CompanyInputMapper {

  constructor(
    private addressInputMapper: AddressInputMapper,
    private subscriptionInputMapper: SubscriptionInputMapper
  ) {}

  mapFrom(form: FormGroup, subscriptions: ISubscription[]): any {

    const company: any = {
      name: form.controls['name'].value,
      coreBusiness: form.controls['coreBusiness'].value,
      phone: form.controls['phone'].value,
      mobile: form.controls['mobile'].value,
      email: form.controls['email'].value,
      controllingMind: {
        name: form.controls['controllingMindName'].value,
        position: form.controls['controllingMindPosition'].value
      },
      noOfEmployees: form.controls['noOfEmployees'].value,
      address: this.addressInputMapper.mapFrom(form)
    };

    if (form.contains('subscription')) {
      company.subscription = this.subscriptionInputMapper.mapFrom(form, subscriptions);
    }

    return company;
  }

  mapTo(company: ICompany, subscriptions: ISubscription[]): FormInputGroup[] {

    const groups = [
      {
        label: 'Company',
        inputs: [
          { id: 'name', label: 'Company name', type: 'text', required: true, autoFocus: true, value: company.name },
          { id: 'coreBusiness', label: 'Core business', type: 'text', required: true, value: company.coreBusiness },
          { id: 'noOfEmployees', label: 'Number of employees', type: 'text', required: true, regex: RegExpPatterns.Number,
            value: company.noOfEmployees }
        ]
      },
      {
        label: 'Controlling mind',
        inputs: [
          { id: 'controllingMindName', label: 'Name', type: 'text', required: true, value: company.controllingMind.name },
          { id: 'controllingMindPosition', label: 'Position', type: 'text', required: true, value: company.controllingMind.position }
        ]
      },
      {
        label: 'Contact',
        inputs: [
          { id: 'email', label: 'Email address', type: 'text', required: true, regex: RegExpPatterns.Email,
            value: company.email },
          { id: 'phone', label: 'Phone number', type: 'text',
            regex: RegExpPatterns.Phone, required: true, value: company.phone },
          { id: 'mobile', label: 'Mobile number',
            regex: RegExpPatterns.Mobile, type: 'text', required: false, value: company.mobile }
        ]
      },
      this.addressInputMapper.mapTo(company.address)
    ];

    if (!company._id) {
      groups.push(...this.subscriptionInputMapper.mapTo(company, subscriptions))
    }

    return groups;
  }

}


