import express from 'express';
import { Logger, transports as _transports } from 'winston';
import winstonLogsDisplay from 'winston-logs-display';

const app = express.app();

var logger = new Logger({
  transports: [
    new _transports.File({
      filename: 'D:\\Playground\\green-pass\\server\\build\\server\\log\\2017-05-17.log',
    }),
  ],
});

winstonLogsDisplay(app, logger);

app.listen(3000, function () {
  console.log('server started on port 3000');
});
