import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import {Paths} from "../../../../shared/constants";
import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {SelectVehiclesState} from "../../../../store/vehicles/vehicle.selectors";
import {IVehicle, IVehicleInspection} from "../../../../../../../shared/interfaces/vehicle.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {VehicleInspectionInputMapper} from "./vehicle-inspection-input.mapper";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {SaveVehicleInspectionAction} from "app/store/vehicles/vehicle-inspection.actions";

@Component({
  selector: 'gp-inspection-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
  `
})
export class VehicleInspectionInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private vehicle: IVehicle;
  private inspection: IVehicleInspection;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private inspectionInputMapper: VehicleInspectionInputMapper) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectVehiclesState())
      .combineLatest(this.store.let(SelectCompany()),
        (vehicleState, company) => ({vehicleState: vehicleState, company: company}))
      .subscribe(result => {
        this.vehicle = result.vehicleState.selected;
        this.inspection = result.vehicleState.selectedInspection
          ? result.vehicleState.selectedInspection
          : <IVehicleInspection>{ inspectionDate: null };

        this.inputGroups = this.inspectionInputMapper.mapTo(this.inspection, result.company.suppliers);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let inspection = Object.assign({}, this.inspection, this.inspectionInputMapper.mapFrom(form, this.inputGroups, this.inspection));
    this.store.dispatch(new SaveVehicleInspectionAction(this.vehicle, inspection));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
