import 'reflect-metadata';
import '../../shared/extensions/date.extensions';
import { TYPES } from '../src/container/container.types';
import container from '../src/container/container.config';
import { IDriverService } from '../src/drivers/driver.service';
import { Driver } from '../../client/src/app/ui/driver/driver.model';
import { IDvlaService } from '../src/dvla/service/dvla.service.interface';
import { ICompanyRepository } from '../src/companies/company.repository';

describe.skip('Driver Service', function () {
  describe.skip('drivingLicenceCheck', function () {
    const driverService = <IDriverService>container.get<IDriverService>(TYPES.services.DriverService);
    const dvlaService = <IDvlaService>container.get<IDvlaService>(TYPES.services.DvlaService);
    const companyRepository = <ICompanyRepository>container.get<ICompanyRepository>(TYPES.repositories.CompanyRepository);
    const driver = new Driver();

    function getLicence(licenceNumber: string, points: number) {
      return Promise.resolve(<any>{
        licence: {
          status: 'OK',
          validFrom: new Date('2000/01/01'),
          validTo: new Date('2020/01/01'),
          licenceNumber: licenceNumber,
          issueNumber: '1',
        },
        points: points,
      });
    }

    beforeEach(() => {
      driverService.update = (driver) => Promise.resolve(driver);

      companyRepository.get = (id) => {
        return Promise.resolve(<any>{
          settings: {
            drivers: {
              licenceChecks: {
                checkPeriods: [
                  { noOfPoints: 6, months: 6 },
                  { noOfPoints: 9, months: 3 },
                  { noOfPoints: 3, months: 8 },
                ],
              },
            },
          },
        });
      };

      driver._id = '1234';
      driver._companyId = '1234';
    });

    it('should set next check date to 3 months with 9 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 9);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(3).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 3 months with 10 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 10);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(3).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 6 months with 6 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 6);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(6).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 6 months with 8 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 8);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(6).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 8 months with 3 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 3);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(8).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 8 months with 5 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 5);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(8).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 12 months with 0 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 0);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(12).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });

    it('should set next check date to 12 months with 2 points', () => {
      dvlaService.drivingLicenceCheck = (licenceNumber, checkcode) => getLicence(licenceNumber, 2);

      return driverService
        .drivingLicenceCheck(driver, 'qwer1212', 'YATES607198JP9LK')
        .then((driver) => {
          const actual = driver.licence.nextCheckDate?.toDateString();
          const expected = new Date().addMonths(12).toDateString();
          expect(actual).toEqual(expected);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  });
});
