import { IDocumentStatus, IDocument, IRecipient, ITemplate } from '../../../../shared/interfaces/document.interface';

export interface IEmbeddedUrl {
  url: string;
}

export interface IDocumentSignatureService {
  // Templates

  getTemplates(): Promise<ITemplate[]>;

  createTemplates(directory: string);

  // Documents

  createDocument(template: ITemplate, recipient: IRecipient, company: string): Promise<IDocument>;

  createCompositeDocument(
    templates: ITemplate[],
    name: string,
    recipient: IRecipient,
    company?: string
  ): Promise<IDocument>;

  getDocumentStatus(documentIds: string[]): Promise<IDocumentStatus[]>;

  deleteDocuments(documentIds: string[]): Promise<IDocument[]>;

  requestEmbeddedUrl(documentId: string, recipient: IRecipient, returnUrl: string): Promise<string>;

  getDocumentPdf(documentId: string): Promise<any>;

  getNewEmbeddedUrl(signerId: string): Promise<IEmbeddedUrl>;
}
