import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import codes from '../shared/error.codes';
import { IReportTypeService } from './report-type.service';
import { IReportQueryService } from './report-query.service';
import { IReportQueryResult, IReportType } from '../../../shared/interfaces/report.interface';
import { IReportService } from './report.service';

export interface IReportController {
  get(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class ReportController implements IReportController {
  constructor(@inject(TYPES.services.ReportService) private reportService: IReportService) {}

  get = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const reportTypeId = req.params.reportTypeId;

    if (!mongoose.Types.ObjectId.isValid(reportTypeId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid report type id.' });

    this.reportService
      .get(companyId, reportTypeId)
      .then((result) => {
        return res.status(codes.OK).json(result);
      })
      .catch((err) => {
        next(err);
      });
  };
}
