import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IDriverController } from './driver.controller';
import multer = require('multer');
const upload = multer({ dest: 'uploads/' });

@injectable()
export class DriverRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.DriverController) private driverController: IDriverController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware,
    @inject(TYPES.routers.DocumentRouter) private documentRouter: IApiRouter
  ) {
    this.configure();
  }

  configure() {
    this.routes.use('/:driverId/documents', this.documentRouter.routes);

    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.getAll
      )
      .post(
        '/import',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.driverController.importDrivers
      )
      .get(
        '/:driverId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.get
      )
      .get(
        '/:driverId/status',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.updateDriverDocuments
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.driverController.add
      )
      .put(
        '/:driverId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.update
      )
      .get(
        '/:driverId/licence/checkcode',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.getDrivingLicenceCheckCode
      )
      .get(
        '/:driverId/licence/check/:checkCode',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.drivingLicenceCheck
      )

      .get(
        '/:driverId/assessments/login',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.assessmentLogin
      )
      .get(
        '/:driverId/assessments/update',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.driverController.updateAssessments
      )

      .post(
        '/:driverId/assessments/onroad/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.driverController.addOnRoadAssessment
      )
      .put(
        '/:driverId/assessments/onroad/:assessmentId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.driverController.updateOnRoadAssessment
      )

      .post(
        '/:driverId/insurance/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.addInsurance
      )
      .put(
        '/:driverId/insurance/:insuranceId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.updateInsurance
      )
      .delete(
        '/:driverId/insurance/:insuranceId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.deleteInsurance
      )

      .post(
        '/:driverId/faw/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.addFawCertificate
      )
      .put(
        '/:driverId/faw/:fawId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.updateFawCertificate
      )
      .delete(
        '/:driverId/faw/:fawId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.deleteFawCertificate
      )

      .post(
        '/:driverId/vehicles/:vehicleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.addVehicle
      )
      .delete(
        '/:driverId/vehicles/:vehicleId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        upload.single('file'),
        this.driverController.removeVehicle
      )

      .delete(
        '/:driverId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.driverController.remove
      );
  }
}
