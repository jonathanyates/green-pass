import {Component, Input} from '@angular/core';
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {IVehicle, IVehicleInspection} from "../../../../../../../shared/interfaces/vehicle.interface";
import {Paths} from "../../../../shared/constants";
import {ISupplier} from "../../../../../../../shared/interfaces/company.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {FormatDate} from "../../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-vehicle-inspection-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" 
                     [lightHeader]="true" [allowAdd]="allowAdd"></gp-dynamic-list>
`
})
export class VehicleInspectionListComponent {

  list: IDynamicList;
  @Input() vehicle: IVehicle;
  @Input() suppliers: ISupplier[];
  @Input() allowAdd: boolean = false;
  @Input() set inspectionHistory(inspectionHistory: IVehicleInspection[]) {
    this.setInspectionHistory(inspectionHistory);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  setInspectionHistory(inspectionHistory: IVehicleInspection[]): void {
    this.list = {
      columns: [
        {header: 'Inspection Date', field: 'inspectionDate'},
        {header: 'Inspected By', field: 'supplier'},
      ]
    };

    if (inspectionHistory) {
      this.list.items = inspectionHistory
        .sort((a, b) => b.inspectionDate.getTime() - a.inspectionDate.getTime())
        .map(inspection => {
          let supplier = this.suppliers
            ? this.suppliers.find(supplier => supplier._id === inspection.supplierId)
            : null;

          return Object.assign({}, inspection , {
            inspectionDate: FormatDate(inspection.inspectionDate),
            supplier: supplier != null ? supplier.name : null
          })
        })
    }
  }

  add() {
    this.router.navigate(['./', Paths.Inspections, Paths.New], { relativeTo: this.route });
  }

  select(inspection: IVehicleInspection) {
    this.router.navigate(['./', Paths.Inspections, inspection._id], { relativeTo: this.route });
  }
}
