import "reflect-metadata";
import {TYPES} from "../../src/container/container.types";
import {IDriverService} from "../../src/drivers/driver.service";
import container from "../../src/container/container.config";

console.log('Processing Documents...');

const driverService = container.get<IDriverService>(TYPES.services.DriverService);

driverService.updateAllDriverDocuments()
  .then(companies => {
    console.log('Driver Documents updated completed successfully');
    let length = 0;
    companies.forEach(company => company.forEach(documents => length += documents.length));
    console.log(`${length} Documents updated.`);
    process.exit();
  })
  .catch(error =>{
    console.log(error);
    process.exit(1);
  });

