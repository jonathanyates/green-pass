'use strict';
const path = require('path');

module.exports = function(app) {
  app.get('/robots.txt', function (req, res) {
    res.status(404).send('Not found');
  });

  app.get('/', function(req, res) {
    res.render('pages/index');
  });

  // routing for all other ejs paths e.g. /contact
  // match paths without dot extensions
  app.get(/^([^.]+)$/, function(req, res, next) {

    // double check for extension and ignore if there is an extension
    const ext = path.extname(req.url);
    if (ext) {
      return next();
    }

    res.render('pages' + req.url, {}, function(err, html) {
      if(err) {
        res.redirect('/');
      } else {
        res.send(html);
      }
    });
  });
};