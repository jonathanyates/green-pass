import {Component, Input} from '@angular/core';
import {Paths} from "../../../../shared/constants";
import {SelectDriverDetailsTabAction} from "../../../../store/drivers/driver.actions";
import {Router} from "@angular/router";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";
import {IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";

@Component({
  selector: 'gp-on-road-assessment-summary',
  templateUrl: 'on-road-assessment-summary.component.html',
  host: {'class': 'card'}
})

export class OnRoadAssessmentSummaryComponent {

  @Input() onRoadAssessment: IOnRoadAssessment;

  constructor(private store: Store<AppState>,
              private router: Router) {
  }

  viewTraining() {
    this.router.navigate([Paths.Driver, Paths.Assessments]);
    this.store.dispatch(new SelectDriverDetailsTabAction('assessments'));
  }
}
