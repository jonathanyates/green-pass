import {Component, Input, OnInit} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {DriverCompliance} from "../../../../../../../shared/models/driver-compliance.model";

@Component({
  selector: 'gp-driver-details-card',
  templateUrl: 'driver-details-card.component.html',
  host: {'class': 'card'}
})
export class DriverDetailsCardComponent implements OnInit {

  hasDetails: boolean;

  private _driver: IDriver;
  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasDetails = DriverCompliance.getDriverDetailsCompliance(driver);
    }
  }

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  viewDetails() {
    if (!this.hasDetails) {
      return this.router.navigate([Paths.Driver, Paths.Details, Paths.Edit]);
    }

    return this.router.navigate([Paths.Driver, Paths.Details]);
  }
}
