import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {IDepot} from "../../../../../../shared/interfaces/company.interface";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";

@Component({
  selector: 'gp-depot-details',
  template: `
    <gp-card [header]="'Depot details'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class DepotDetailsComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private depot: IDepot;
  private subscription: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.subscription = this.route.data
      .map((data: { depot: IDepot }) => data.depot)
      .subscribe(depot => {
        this.depot = depot;
        this.items = [
          {title: "Depot", value: depot.name},
          {title: "Address", value: depot.address},
          {title: "Phone", value: depot.phone},
          {title: "Mobile", value: depot.mobile},
          {title: "Email", value: depot.email}
        ]
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit() {
    this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}

