import mongoose from 'mongoose';
import { IReportSchedule } from '../../../shared/interfaces/report.interface';

export const ReportScheduleSchema = new mongoose.Schema({
  _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
  reportType: { type: mongoose.Schema.Types.ObjectId, ref: 'ReportType', required: false },
  cron: { type: String, required: true },
  recipients: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false }],
});

export interface IReportScheduleModel extends mongoose.Document, IReportSchedule {
  _id: any;
}
