import * as actions from "./driver.actions";
import {DriversState} from "./drivers.state";
import {ActionTypes} from "../authentication/authentication.actions";
import {Contact, Driver} from "../../ui/driver/driver.model";
import * as DriverInsuranceActions from './driver-insurance.actions';
import * as DriverFawActions from './driver-faw.actions';
import {SaveDriverInsuranceSuccessAction, SelectDriverInsuranceSuccessAction} from "./driver-insurance.actions";
import {LoadDriversSuccessAction} from "./driver.actions";
import {IDriver} from "../../../../../shared/interfaces/driver.interface";

export const initialState: DriversState = {
  list: null,
  total: 0,
  page: 1,
  selected: null,
  selectedReference: null,
  selectedInsurance: null,
  selectedFaw: null,
  selectedTab: null
};

type reducerActions = actions.Actions
  | DriverInsuranceActions.Actions

export function DriversReducer(state: DriversState = initialState, action: reducerActions): DriversState {

  let insuranceHistory;
  let fawHistory;

  switch (action.type) {

    case actions.ActionTypes.LOAD_SUCCESS:
      let successAction = <LoadDriversSuccessAction>action;
      return Object.assign({}, state, {
        list: action.payload,
        total: successAction.total,
        page: successAction.page,
        selected: null,
        selectedReference: null,
        selectedInsurance: null,
        selectedTab: null
      });

    case actions.ActionTypes.SELECT_LIST_SUCCESS:
      return Object.assign({}, state, {
        selected: null,
        selectedReference: null,
        selectedInsurance: null,
        selectedTab: null
      });

    case actions.ActionTypes.SAVE_SUCCESS:
    case actions.ActionTypes.SELECT_DRIVER_SUCCESS:
    case actions.ActionTypes.LOAD_DRIVER_SUCCESS:
    case actions.ActionTypes.LICENCE_CHECK_SUCCESS:
    case actions.ActionTypes.SAVE_ON_ROAD_ASSESSMENT_SUCCESS:
      return Object.assign({}, state, {
        selected: action.payload,
        selectedReference: null
      });

    case actions.ActionTypes.CLEAR_LIST:
      return Object.assign({}, state, {
        list: null
      });

    case actions.ActionTypes.ASSESSMENT_UPDATE_SUCCESS:
      return Object.assign({}, state, {
        selected: Object.assign({}, state.selected, {
          assessments: (<IDriver>action.payload).assessments,
          compliance: (<IDriver>action.payload).compliance
        })
      });

    case actions.ActionTypes.UPDATE_DRIVER_STATUS_SUCCESS:
      return Object.assign({}, state, {
        selected: <IDriver>action.payload
      });

    case actions.ActionTypes.SAVE_DRIVER_REFERENCE:
      let referenceList;
      const references = state.selected.references;
      const reference = <Contact>action.payload;

      if (reference._id) {
        referenceList = references.map(item => {
          return item._id === reference._id ? reference : item
        });
      } else {
        referenceList = [...references, action.payload];
      }

      return Object.assign({}, state, {
        selected: Object.assign(new Driver(), state.selected, {
          references: referenceList
        })
      });

    case actions.ActionTypes.SAVE_DRIVER_REFERENCE_SUCCESS:
      return Object.assign({}, state, {
        selected: action.payload
      });

    case actions.ActionTypes.SELECT_DRIVER_REFERENCE:
      let ref = state.selected
        ? state.selected.references.find(reference => reference._id === action.payload)
        : null;
      return Object.assign({}, state, {
        selectedReference: ref
      });

    case actions.ActionTypes.SELECT_SELECTED_TAB:
      return Object.assign({}, state, {
        selectedTab: action.payload
      });

    case actions.ActionTypes.ASSESSMENT_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        assessmentLoggedIn: action.payload
      });

    //  DriverInsuranceActions

    case DriverInsuranceActions.ActionTypes.SAVE_DRIVER_INSURANCE_SUCCESS:
      const insurancePayload = (<SaveDriverInsuranceSuccessAction>action).payload;
      const insuranceDriver = insurancePayload.driver;
      const insurance = insurancePayload.insurance;

      insuranceDriver.insuranceHistory = insuranceDriver.insuranceHistory || [];
      let insuranceExists = insuranceDriver.insuranceHistory.some(item => item._id === insurance._id);

      if (insuranceExists) {
        insuranceHistory = insuranceDriver.insuranceHistory.map(item => {
          return item._id === insurance._id ? insurance : item
        });
      } else {
        insuranceHistory = [...insuranceDriver.insuranceHistory, insurance];
      }

      return Object.assign({}, state, {
        selected: Object.assign({}, insuranceDriver, {
          insuranceHistory: insuranceHistory
        }),
        selectedInsurance: insurance
      });

    case DriverInsuranceActions.ActionTypes.SELECT_DRIVER_INSURANCE_SUCCESS:
      let selectedInsurance = (<SelectDriverInsuranceSuccessAction>action).payload;
      if (selectedInsurance && selectedInsurance.file) {
        insuranceHistory = state.selected.insuranceHistory &&
          state.selected.insuranceHistory.map(item => {
            return item._id === selectedInsurance._id ? selectedInsurance : item
          });

        return Object.assign({}, state, {
          selected: Object.assign({}, state.selected, {
            insuranceHistory: insuranceHistory
          }),
          selectedInsurance: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedInsurance: action.payload
        });
      }

    case DriverInsuranceActions.ActionTypes.CLEAR_DRIVER_INSURANCE:
      return Object.assign({}, state, {
        selectedInsurance: null
      });


    //  DriverFawActions

    case DriverFawActions.ActionTypes.SAVE_DRIVER_FAW_SUCCESS:
      const fawPayload = (<DriverFawActions.SaveDriverFawSuccessAction>action).payload;
      const fawDriver = fawPayload.driver;
      const faw = fawPayload.faw;

      fawDriver.fawHistory  = fawDriver.fawHistory || [];
      let fawExists = fawDriver.fawHistory.some(item => item._id === faw._id);

      if (fawExists) {
        fawHistory = fawDriver.fawHistory.map(item => {
          return item._id === faw._id ? faw : item
        });
      } else {
        fawHistory = [...fawDriver.fawHistory, faw];
      }

      return Object.assign({}, state, {
        selected: Object.assign({}, fawDriver, {
          fawHistory: fawHistory
        }),
        selectedFaw: faw
      });

    case DriverFawActions.ActionTypes.SELECT_DRIVER_FAW_SUCCESS:
      let selectedFaw = (<DriverFawActions.SelectDriverFawSuccessAction>action).payload;
      if (selectedFaw && selectedFaw.file) {
        fawHistory = state.selected.fawHistory && state.selected.fawHistory.map(item => {
          return item._id === selectedFaw._id ? selectedFaw : item
        });

        return Object.assign({}, state, {
          selected: Object.assign({}, state.selected, {
            fawHistory: fawHistory
          }),
          selectedFaw: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedFaw: action.payload
        });
      }

    case DriverFawActions.ActionTypes.CLEAR_DRIVER_FAW:
      return Object.assign({}, state, {
        selectedFaw: null
      });


    case ActionTypes.LOGOUT:
      return initialState;

    default:
      return state;
  }
}
