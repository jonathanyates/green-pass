import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {AddressInputMapper} from "../../shared/components/address-input/address-input.mapper";
import {Depot} from "../../company/company.model";
import {Injectable} from "@angular/core";
import {RegExpPatterns} from "../../../../../../shared/constants";
import {IDepot} from "../../../../../../shared/interfaces/company.interface";

@Injectable()
export class DepotInputMapper {

  constructor(private addressInputMapper: AddressInputMapper) {}

  mapFrom(form:FormGroup): IDepot {

    let depot:any = {};
    depot.name = form.controls['name'].value;
    depot.phone = form.controls['phone'].value;
    depot.mobile = form.controls['mobile'].value;
    depot.email = form.controls['email'].value;
    depot.address = this.addressInputMapper.mapFrom(form);
    return depot;
  }

  mapTo(depot:Depot): FormInputGroup[] {

    return [
      {
        label: 'Depot',
        inputs: [
          { id: 'name', label: 'Name', type:'text', required: true, autoFocus: true, value: depot.name },
          { id: 'phone', label: 'Phone number', type:'text', required: true,
            regex: RegExpPatterns.Phone, value: depot.phone },
          { id: 'mobile', label: 'Mobile number', type:'text', required: false,
            regex: RegExpPatterns.Mobile, value: depot.mobile },
          { id: 'email', label: 'Email', type:'text', required: false,
            regex: RegExpPatterns.Email, value: depot.email },
        ]
      },
      this.addressInputMapper.mapTo(depot.address)

    ];
  }

}
