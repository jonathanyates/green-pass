import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {ComplianceRouter} from "./compliance.router";
import {ComplianceController, IComplianceController} from "./compliance.controller";
import {ComplianceService, IComplianceService} from "./compliance.service";

export const complianceModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.ComplianceRouter).to(ComplianceRouter).inSingletonScope();
  bind<IComplianceController>(TYPES.controllers.ComplianceController).to(ComplianceController).inSingletonScope();
  bind<IComplianceService>(TYPES.services.ComplianceService).to(ComplianceService).inSingletonScope();
});