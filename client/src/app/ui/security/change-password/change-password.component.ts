import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {FormInput} from "../../shared/components/dynamic-form/dynamic-form.model";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {IUser} from "../../../../../../shared/interfaces/user.interface";

@Component({
  selector: 'gp-change-password',
  templateUrl: 'change-password.component.html'
})
export class ChangePasswordComponent implements OnInit  {

  user: IUser;
  inputs: FormInput[];
  formGroup: FormGroup;
  password: AbstractControl;
  confirm: AbstractControl;

  @Input() title: string;
  @Input() message: string;
  @Input() error: string;
  @Output() submit: EventEmitter<string> = new EventEmitter<string>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public router: Router) {
  }

  ngOnInit(): void {

    this.inputs = [
      { id: 'password', label: 'New Password', type:'password', icon: 'fa-lock', required: true, autoFocus: true },
      { id: 'confirm', label: 'Confirm Password', type:'password', icon: 'fa-lock', required: true, autoFocus: false }
    ];

    this.formGroup = this.build(this.inputs);
    this.password = this.formGroup.controls.password;
    this.confirm = this.formGroup.controls.confirm;
  }

  build(inputs: FormInput[]) {
    let group: any = {};

    inputs.forEach(input => {
      let control = new FormControl('', [Validators.required, Validators.minLength(8)]);
      group[input.id] = control;
      input.control = control;

    });
    return new FormGroup(group, this.matchPassword);
  }

  matchPassword(group: FormGroup): any {
    let password = group.controls.password;
    let confirm = group.controls.confirm;

    // Don't kick in until user touches both fields
    if (password.pristine || confirm.pristine) {
      return null;
    }

    // Mark group as touched so we can add invalid class easily
    group.markAsTouched();

    if (password.value === confirm.value) {
      return null;
    }

    return {matchPassword: false};
  }

  onSubmit() {
    if (!this.formGroup.valid) return;
    let password = this.formGroup.controls['password'].value.toString();
    this.submit.emit(password)
  }

}
