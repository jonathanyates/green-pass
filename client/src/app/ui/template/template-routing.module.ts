import {NgModule} from '@angular/core';
import {Roles} from "../../../../../shared/constants";
import {Routes, RouterModule} from '@angular/router';
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {CompanyComponent} from "../company/company.component";
import {TemplateListResolve} from "./template-list/template-list.resolve";
import {TemplateListComponent} from "./template-list/template-list.component";
import {CompanyTemplateResolve} from "./template/company-template.resolve";
import {TemplateContainerComponent} from "./template/template-container.component";
import {TemplateInputContainerComponent} from "./template-input/template-input-container.component";

const routes: Routes = [
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/templates', component: TemplateListComponent, resolve: { templates: TemplateListResolve } },
      { path: ':id/templates/add', component: TemplateInputContainerComponent, resolve: { templates: CompanyTemplateResolve } },
      { path: ':id/templates/:templateId', component: TemplateContainerComponent, resolve: { templates: CompanyTemplateResolve } },
      { path: ':id/templates/:templateId/edit', component: TemplateInputContainerComponent, resolve: { templates: CompanyTemplateResolve } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    TemplateListResolve,
    CompanyTemplateResolve
  ]
})
export class TemplateRoutingModule {
}
