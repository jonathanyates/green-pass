import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {LoadDriversAction} from "../../../store/drivers/driver.actions";
import {pagination} from "../../../shared/constants";

@Injectable()
export class DriverListResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    if (!companyId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new LoadDriversAction(companyId, 1, pagination.pageSize));

    return true;
  }
}
