import {Component, OnInit, Input} from '@angular/core';
import {IDynamicList} from "../../../../../../../shared/models/dynamic-list.model";
import {IVehicle, IVehicleService} from "../../../../../../../shared/interfaces/vehicle.interface";
import {Paths} from "../../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {FormatDate} from "../../../../../../../shared/utils/helpers";
import {ISupplier} from "../../../../../../../shared/interfaces/company.interface";

@Component({
  selector: 'gp-vehicle-service-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" 
                     [lightHeader]="true" [allowAdd]="allowAdd"></gp-dynamic-list>
`
})
export class VehicleServiceListComponent {

  list: IDynamicList;

  @Input() vehicle: IVehicle;
  @Input() suppliers: ISupplier[];
  @Input() allowAdd: boolean = false;

  @Input() set serviceHistory(serviceHistory: IVehicleService[]) {
    this.setServiceHistory(serviceHistory);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  setServiceHistory(serviceHistory: IVehicleService[]): void {
    this.list = {
      columns: [
        {header: 'Service Date', field: 'serviceDate'},
        {header: 'Serviced By', field: 'supplier'},
      ]
    };

    if (serviceHistory) {
      this.list.items = serviceHistory
        .sort((a, b) => b.serviceDate.getTime() - a.serviceDate.getTime())
        .map(service => {
        let supplier = this.suppliers
          ? this.suppliers.find(supplier => supplier._id === service.supplierId)
          : null;

        return Object.assign({}, service , {
          serviceDate: FormatDate(service.serviceDate),
          supplier: supplier != null ? supplier.name : null
        })
      })
    }
  }

  add() {
    this.router.navigate(['./', Paths.Services, Paths.New], { relativeTo: this.route });
  }

  select(service: IVehicleService) {
    this.router.navigate(['./', Paths.Services, service._id], { relativeTo: this.route });
  }
}
