import {IDriver} from "../../../shared/interfaces/driver.interface";
import serverConfig from "../server/server.config";
import {ICompany} from "../../../shared/interfaces/company.interface";

export const getDriverReminderEmail = (company: ICompany, driver: IDriver): string => {
  const user = driver._user;

  let text = `
    <p>Hi ${user.forename},</p>
    <p>You are receiving this email from Green Pass Compliance on behalf of ${company.name} as a reminder that you need to complete your driver compliance.</p>

    ${getComplianceText(driver)}

    <p>Please login to your account to complete your compliance.</p>
    <p>
        Your username is: ${user.username}<br>
        Your password has already been emailed to you.<br>
    </p>
    
    <p>
        If you have forgotten your password then click the Forgot Password button on the login screen to reset your password.
    </p>
    
    <p>Login to your account by clicking the following link:</p>
    <p><a href="${serverConfig.client.baseUrl + '/login'}">${serverConfig.client.baseUrl + '/login'}</a></p>
    <p>If this is the first time you have logged in you will be asked to change your password.</p>
  `;

  text += `<p>Regards,<br>${company.controllingMind.name},<br>${company.name}</p>`;

  return text;
};

function getComplianceText(driver: IDriver): string {
  const compliance = driver.compliance;
  let text = `
    <p>The following sections must be completed:</p>
    <ul>
`;

  if (!compliance.details) {
    text += `<li>Driver details</li>`
  }

  if (!compliance.nextOfKin) {
    text += `<li>Next of kin</li>`
  }

  if (!compliance.drivingLicence.compliant) {
    text += `<li>Driving licence check</li>`
  }

  if (!compliance.documents.compliant) {
    text += `<li>Sign Mandate Documents</li>`
  }

  if (!compliance.assessments.compliant) {
    text += `<li>Driver E-learning Assessments</li>`
  }

  if (compliance.vehicles != null && compliance.vehicles === false) {
    text += `<li>Vehicle details</li>`
  }

  if (compliance.insurance != null && compliance.insurance === false) {
    text += `<li>Business insurance</li>`
  }

  if (compliance.references != null && compliance.references === false) {
    text += `<li>References</li>`
  }

  text += `</ul>`;

  return text;
}
