import { IVehicleCompliance } from './compliance.interface';
import { IInsurance } from './common.interface';
import { IDriver } from './driver.interface';

export interface IVehicleMot {
  testDate: Date;
  expiryDate: Date;
  testResult: string;
  odometerReading: number;
  odometerUnit: string;
  motTestNumber: string;
  reasonsForRejection: Array<IReasonforRejection>;

  /**
   * @deprecated The field has now been replaced by reasonsForRejection
   */
  reasonsForFailure?: Array<string>;

  /**
   * @deprecated The field has now been replaced by reasonsForRejection
   */
  advisoryNotices?: Array<string>;
}

export enum RfrType {
  PASS = 'PASS',
  ADVISORY = 'ADVISORY',
  FAIL = 'FAIL',
  MINOR = 'MINOR',
  MAJOR = 'MAJOR',
  DANGEROUS = 'DANGEROUS',
  USER_ENTERED = 'USER ENTERED',
  PRS = 'PRS',
}

export interface IReasonforRejection {
  text: string;
  type: RfrType;
  dangerous: boolean;
  advice: string;
}

export interface IVehicleMotEnquiry {
  details: {
    vehicleMake?: string;
    vehicleModel?: string;
    dateFirstUsed?: Date;
    fuelType?: string;
    colour?: string;
    dateRegistered?: Date;
    validUntil?: Date;
  };
  history: IVehicleMot[];
}

export interface IAnnualTest {
  testDate: Date;
  testType: string;
  testResult: string;
  testCertificateNumber: string;
  expiryDate: Date;
  numberOfDefectsAtTest: string;
  numberOfAdvisoryDefectsAtTest: string;
  defects?: IAnnualTestDefect[];
  location: string;
}

export interface IAnnualTestDefect {
  failureItemNo: string;
  failureReason: string;
  severityCode: string;
  severityDescription: string;
}

export interface IVehicleService {
  _id?: string;
  serviceDate: Date;
  odometerReading?: number;
  supplierId: any;
  filename?: string;
  fileType?: string;
  fileId?: string;
  file?: any;
}

export interface IVehicleInspection {
  _id?: string;
  inspectionDate: Date;
  odometerReading?: number;
  supplierId: any;
  filename?: string;
  fileType?: string;
  fileId?: string;
  file?: any;
}

export interface IVehicleCheck {
  _id?: string;
  checkDate: Date;
  odometerReading?: number;
  checkedBy: string;
  filename?: string;
  fileType?: string;
  fileId?: string;
  file?: any;
}

export interface IVehicleInsurance extends IInsurance {}

export interface IVehicle {
  _id?: any;
  _companyId: string;
  make: string;
  model: string;
  registrationNumber: string;
  category: string;
  type: string;
  dateOfFirstRegistration?: Date;
  manufactureDate?: Date;
  yearOfManufacture?: number;
  firstUsedDate?: Date;
  cylinderCapacityCc?: string;
  co2Emissions?: string;
  fuelType?: string;
  exportMarker?: string;
  vehicleStatus?: string;
  vehicleColour?: string;
  vehicleTypeApproval?: string;
  wheelplan?: string;
  revenueWeight?: string;

  dvlaCheckDate?: Date;
  taxDueDate?: Date;
  motDueDate?: Date;

  /**
   * @deprecated The field is no longer used as it has been replaced by DVSA MOT check results
   */
  motDueText?: string;

  euroStatus?: string;
  realDrivingEmissions?: string;
  dateOfLastv5cIssued?: Date;

  motHistory: IVehicleMot[];
  serviceHistory: IVehicleService[];
  inspectionHistory: IVehicleInspection[];
  checkHistory: IVehicleCheck[];

  nextService?: Date;
  nextInspection?: Date;
  nextVehicleCheck?: Date;

  annualMileage?: number;
  inspectionIntervalType?: string;

  compliance?: IVehicleCompliance;

  drivers?: Array<IDriver>;

  removed?: boolean;

  // added fields for HGV/PSV Tests
  vehicleType?: string;
  class?: string;
  annualTestHistory?: IAnnualTest[];
}

export interface IVehicleSummary {
  _companyId: string;
  fleet: number;
  grey: number;
}

export interface IInspectionIntervalCurve {
  description: string;
  points: Array<{ x: number; y: number }>;
}
