import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {Response} from "@angular/http";
import {ServerError} from "../../shared/errors/server.error";
import {LogoutAction} from "../authentication/authentication.actions";

export const ActionTypes = {
  HANDLE_ERROR: type('[Error] Handle Error'),
  SET_ERROR: type('[Error] Set Error'),
  CLEAR_ERROR: type('[Error] Clear Error'),
};

export class HandleErrorAction implements Action {
  type = ActionTypes.HANDLE_ERROR;
  constructor(public payload: ServerError) { }
}

export class SetErrorAction implements Action {
  type = ActionTypes.SET_ERROR;
  constructor(public payload: ServerError) { }
}

export class ClearErrorAction implements Action {
  type = ActionTypes.CLEAR_ERROR;
  payload: null;
  constructor() { }
}

export type Actions
  = SetErrorAction
  | HandleErrorAction
  | ClearErrorAction
  | LogoutAction;
