import {FormInput, FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {FormatDate, FormatDateForInput, GetDate} from "../../../../../../../shared/utils/helpers";
import {IVehicleCheck} from "../../../../../../../shared/interfaces/vehicle.interface";
import {Observable} from "rxjs";

@Injectable()
export class VehicleCheckInputMapper {

  mapFrom(form:FormGroup, inputGroups: FormInputGroup[], vehicleCheck: IVehicleCheck): IVehicleCheck {

    let input = <FormInput>inputGroups[0].inputs.find((input:FormInput) => input.id === 'file');
    let fileChanged = input.file && input.file !== vehicleCheck.file;

    return {
      checkDate: GetDate(form.controls['checkDate'].value),
      checkedBy: form.controls['checkedBy'].value,
      odometerReading: form.controls['odometerReading'].value,
      file: fileChanged ? input.file : null,
      fileType: input.fileType
    };
  }

  mapTo(check: IVehicleCheck): FormInputGroup[] {

    let reportFileInput = <FormInput>{ id: 'file', label: 'Vehicle Check report', type:'file', required: false, value: '' };

    if (check.file) {
      reportFileInput.file = check.file;
      reportFileInput.fileType = check.fileType;
    }

    reportFileInput.action = file => {
      reportFileInput.file = file;
      reportFileInput.fileType = file.fileType;
      return Observable.empty();
    };

    return [
      {
        label: 'Check details',
        inputs: [
          { id: 'checkDate', label: 'Check Date', type:'date', required: true, autoFocus: false,
            value: FormatDateForInput(check.checkDate) },
          { id: 'checkedBy', label: 'Checked By', type:'text', required: true, autoFocus: false, value: check.checkedBy },
          { id: 'odometerReading', label: 'Odometer Reading', type:'text', required: true, autoFocus: false, value: check.odometerReading },
          reportFileInput
        ]
      }
    ];
  }

}
