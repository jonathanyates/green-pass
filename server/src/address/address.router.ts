import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IAddressController } from './address.controller';

@injectable()
export class AddressRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.AddressController) private addressController: IAddressController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/:postcode',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.addressController.addressSearch
      )
      .get(
        '/:postcode/:house',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.addressController.addressSearch
      );
  }
}
