﻿import {IAddress} from "../../../../../shared/interfaces/address.interface";
import {ICompanyAlertType} from "../../../../../shared/interfaces/alert.interface";
import {
  ICompany, IDepot, ISupplier
} from "../../../../../shared/interfaces/company.interface";
import {ICompanySettings} from "../../../../../shared/interfaces/company-settings.interface";
import {ICompanySubscription} from "../../../../../shared/interfaces/subscription.interface";
import {IFleetInsurance} from "../../../../../shared/interfaces/common.interface";

export class Address implements IAddress {
  line1: string;
  line2?: string;
  line3?: string;
  line4?: string;
  locality: string;
  townCity: string;
  county: string;
  country: string;
  postcode: string;

  static clone(address: IAddress) {
    return (<any>Object).assign(new Address(), address);
  }
}

export class Depot implements IDepot {
  _id?: any;
  _companyId: string;
  name: string;
  address: IAddress = new Address();
  phone: string;
  mobile?: string;
  email?: string;

  static clone(depot: Depot) {
    return (<any>Object).assign(new Depot(), depot, {
      address: Address.clone(depot.address)
    });
  }
}

export class Supplier implements ISupplier {
  _id?: any;
  _companyId?: string;
  name: string;
  address: IAddress = new Address();
  phone: string;
  mobile?: string;
  email?: string;

  static clone(supplier: Supplier) {
    return (<any>Object).assign(new Supplier(), supplier, {
      address: Address.clone(supplier.address)
    });
  }
}

export class Contact {
  name: string;
  position: string;

  static clone(contact: Contact) {
    return (<any>Object).assign(new Address(), contact);
  }
}

export class Company implements ICompany {
  _id?: any;
  name: string;
  coreBusiness: string;
  address: IAddress = new Address();
  noOfEmployees?: number;
  phone: string;
  mobile?: string;
  email: string;
  controllingMind?: Contact = new Contact();
  depots?: IDepot[] = [];
  suppliers?: ISupplier[] = [];
  users?: number;
  vehicles?: number;
  drivers?: number;
  fleetInsuranceHistory?: IFleetInsurance[];
  templates?: string[] = [];
  alertTypes?: ICompanyAlertType[];
  settings?: ICompanySettings;
  subscription: ICompanySubscription;
  removed?: boolean;
}

