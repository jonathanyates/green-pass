import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'gp-vehicle-search',
  template: `
    <div class="md-form d-flex mt-2 mb-0" style="max-width: 350px">
      <input id="search" class="form-control" type="text"
             [autoFocus]="true" #search (keyup)="onCheckValue($event.target.value)"
             (keyup.enter)="onSearchChanged($event.target.value)" [disabled]="disabled">
      <label for="search">Enter Vehicle Registration</label>

      <div>
        <button class="btn btn-primary btn-lg waves-effect pl-3 pr-3 mr-0"
                (click)="onSearchChanged(search.value)" [disabled]="disabled">Search
        </button>
      </div>
    </div>
  `
})
export class VehicleSearchComponent implements OnInit {

  @Input() disabled: boolean;
  @Output() check: EventEmitter<string> = new EventEmitter<string>();
  @Output() searchTermChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onCheckValue(value: string) {
    this.check.emit(value);
  }

  onSearchChanged(term: string) {
    term != null ? term.trim() : '';
    this.searchTermChanged.emit(term);
  }

}
