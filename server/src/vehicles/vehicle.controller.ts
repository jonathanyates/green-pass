import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiController } from '../interfaces/api-controller.interface';
import { NextFunction, Request, Response } from 'express-serve-static-core';
import codes from '../shared/error.codes';
import { IVehicleApiService } from './vehicle.service';
import { AuditActions, AuditTargets, IAuditService } from '../audit/audit.service';
import { jsonDeserialiser } from '../../../shared/utils/json.deserialiser';

export interface IVehicleController extends IApiController {
  addService(req, res: Response, next: NextFunction);
  updateService(req: Request, res: Response, next: NextFunction);

  addInspection(req, res: Response, next: NextFunction);
  updateInspection(req: Request, res: Response, next: NextFunction);

  addCheck(req, res: Response, next: NextFunction);
  updateCheck(req: Request, res: Response, next: NextFunction);

  vehicleCheck(req: Request, res: Response, next: NextFunction);
  getVehicleSummary(req: Request, res: Response, next: NextFunction);

  addDriver(req: Request, res: Response, next: NextFunction);
  removeDriver(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class VehicleController implements IVehicleController {
  constructor(
    @inject(TYPES.services.VehicleService) private vehicleService: IVehicleApiService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  addService = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!req.body.serviceDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No service date specified.' });
    }

    if (!req.body.supplierId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No supplierId specified.' });
    }

    const service = {
      serviceDate: new Date(req.body.serviceDate),
      supplierId: req.body.supplierId,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .addService(companyId, vehicleId, service)
      .then((service) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Service, service);
        res.status(codes.CREATED).json(service);
      })
      .catch((err) => next(err));
  };

  updateService = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;
    const serviceId = req.params.serviceId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!serviceId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No service Id specified.' });
    }

    if (!req.body.serviceDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No service date specified.' });
    }

    if (!req.body.supplierId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No supplierId specified.' });
    }

    const service = {
      serviceDate: new Date(req.body.serviceDate),
      supplierId: req.body.supplierId,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .updateService(companyId, vehicleId, serviceId, service)
      .then((service) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Service, service);
        res.status(codes.CREATED).json(service);
      })
      .catch((err) => next(err));
  };

  addInspection = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!req.body.inspectionDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No inspection date specified.' });
    }

    if (!req.body.supplierId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No supplierId specified.' });
    }

    const inspection = {
      inspectionDate: new Date(req.body.inspectionDate),
      supplierId: req.body.supplierId,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .addInspection(companyId, vehicleId, inspection)
      .then((inspection) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Inspection, inspection);
        res.status(codes.CREATED).json(inspection);
      })
      .catch((err) => next(err));
  };

  updateInspection = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;
    const inspectionId = req.params.inspectionId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!inspectionId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No inspection Id specified.' });
    }

    if (!req.body.inspectionDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No inspection date specified.' });
    }

    if (!req.body.supplierId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No supplierId specified.' });
    }

    const inspection = {
      inspectionDate: new Date(req.body.inspectionDate),
      supplierId: req.body.supplierId,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .updateInspection(companyId, vehicleId, inspectionId, inspection)
      .then((inspection) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Inspection, inspection);
        res.status(codes.CREATED).json(inspection);
      })
      .catch((err) => next(err));
  };

  addCheck = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!req.body.checkDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check date specified.' });
    }

    if (!req.body.checkedBy) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No checked By field specified.' });
    }

    const check = {
      checkDate: new Date(req.body.checkDate),
      checkedBy: req.body.checkedBy,
      odometerReading: req.body.odometerReading,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .addCheck(companyId, vehicleId, check)
      .then((check) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.VehicleCheck, check);
        res.status(codes.CREATED).json(check);
      })
      .catch((err) => next(err));
  };

  updateCheck = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicleId = req.params.vehicleId;
    const checkId = req.params.checkId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle Id specified.' });
    }

    if (!checkId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    if (!req.body.checkDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check date specified.' });
    }

    if (!req.body.checkedBy) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No checked By field specified.' });
    }

    const check = {
      checkDate: new Date(req.body.checkDate),
      checkedBy: req.body.checkedBy,
      odometerReading: req.body.odometerReading,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.vehicleService
      .updateCheck(companyId, vehicleId, checkId, check)
      .then((check) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.VehicleCheck, check);
        res.status(codes.CREATED).json(check);
      })
      .catch((err) => next(err));
  };

  getAll = (req: Request, res: Response, next: NextFunction) => {
    const criteria = {
      _companyId: new mongoose.Types.ObjectId(req.params.id),
    };

    let page, pageSize: number;

    if (req.query) {
      page = +req.query.page;
      pageSize = +req.query.pageSize;

      delete req.query.page;
      delete req.query.pageSize;

      for (const param in req.query) {
        if (req.query.hasOwnProperty(param)) {
          const pattern = req.query[param] as string;
          criteria[param] = { $regex: new RegExp(pattern, 'i') };
        }
      }
    }

    this.vehicleService
      .findByPage(criteria, page, pageSize)
      .then((vehicles) => res.status(codes.OK).json(vehicles))
      .catch((err) => next(err));
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.vehicleId;

    this.vehicleService
      .get(id)
      .then((vehicle) => {
        if (!vehicle) {
          return res.status(codes.NOT_FOUND).send({ message: `Vehicle not found with id ${id}` });
        }

        return res.status(codes.OK).json(vehicle);
      })
      .catch((err) => next(err));
  };

  add = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const vehicle = jsonDeserialiser.parseDates(req.body);

    if (!vehicle) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle specified.' });
    }

    this.vehicleService
      .add(companyId, vehicle)
      .then((vehicle) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Vehicle, vehicle);
        res.status(codes.CREATED).json(vehicle);
      })
      .catch((err) => next(err));
  };

  update = (req: Request, res: Response, next: NextFunction) => {
    const vehicle = jsonDeserialiser.parseDates(req.body);

    if (!vehicle) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle specified.' });
    }

    this.vehicleService
      .update(vehicle)
      .then((vehicle) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Vehicle, vehicle);
        res.status(codes.ACCEPTED).json(vehicle);
      })
      .catch((err) => next(err));
  };

  remove = (req: Request, res: Response, next: NextFunction) => {
    const vehicleId = req.params.vehicleId;

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicle id specified.' });
    }

    this.vehicleService
      .remove(vehicleId)
      .then((vehicle) => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.Vehicle, vehicle);
        res.status(codes.ACCEPTED).json(vehicle);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Remove, AuditTargets.Vehicle, err);
        next(err);
      });
  };

  vehicleCheck = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const registration = req.params.registration;

    this.vehicleService
      .vehicleCheck(companyId, registration)
      .then((enquiry) => {
        if (!enquiry) {
          return res.status(codes.NOT_FOUND).send({ message: `Vehicle not found with registration ${registration}` });
        }

        return res.status(codes.OK).json(enquiry);
      })
      .catch((err) => next(err));
  };

  getVehicleSummary = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    this.vehicleService
      .getVehicleSummary(companyId)
      .then((summary) => {
        return res.status(codes.OK).json(summary);
      })
      .catch((err) => next(err));
  };

  addDriver = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;
    const vehicleId = req.params.vehicleId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicleId Id specified.' });
    }

    this.vehicleService
      .addDriver(driverId, vehicleId)
      .then((vehicle) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.DriverVehicle, vehicle);
        res.status(codes.ACCEPTED).json(vehicle);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.DriverVehicle, err);
        next(err);
      });
  };

  removeDriver = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;
    const vehicleId = req.params.vehicleId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicleId Id specified.' });
    }

    this.vehicleService
      .removeDriver(driverId, vehicleId)
      .then((vehicle) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.DriverVehicle, vehicle);
        res.status(codes.ACCEPTED).json(vehicle);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.DriverVehicle, err);
        next(err);
      });
  };
}
