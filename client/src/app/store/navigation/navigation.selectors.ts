import {AppState} from "../app.states";
import {Store} from "@ngrx/store";
import {NavigationState} from "./navigation.state";

export const SelectNavigationState = () => {
  return (store:Store<AppState>) => {
    return store.select<NavigationState>(state => state.navigation);
  }
};

export const SelectNavigationBreadcrumb = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectNavigationState())
      .map(state => state.breadcrumb);
  }
};

export const SelectNavigationLinks = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectNavigationState())
      .map(state => state.navigationLinks)
      .distinctUntilChanged();
  }
};
