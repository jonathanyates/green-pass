import {Store} from "@ngrx/store";
import {CompaniesState} from "./companies.state";
import {AppState} from "../app.states";

export const SelectCompanies = () => {
  return (store:Store<AppState>) => {
    return store.select<CompaniesState>(state => state.companies)
      .map(state => state.list);
  }
};

