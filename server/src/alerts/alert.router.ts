import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IAlertController } from './alert.controller';
import { IRouter } from 'express-serve-static-core';

export interface IAlertRouter extends IApiRouter {
  companyRoutes: IRouter;
}

@injectable()
export class AlertRouter implements IAlertRouter {
  companyRoutes = express.Router({ mergeParams: true });
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.AlertController) private alertController: IAlertController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes.get(
      '/summary',
      this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
      this.alertController.getAlertSummaries
    );

    this.companyRoutes.use(this.authorizationMiddleware.user.middleware());

    this.companyRoutes
      .get(
        '/summary',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.getAlertSummary
      )
      .get(
        '/alertTypeSummary',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.getAlertTypeSummary
      )
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.getAll
      )
      .get(
        '/:alertId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.get
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.alertController.add
      );
  }
}

@injectable()
export class AlertTypeRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.AlertController) private alertController: IAlertController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.getAlertTypes
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.addAlertType
      )
      .put(
        '/:alertTypeId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.alertController.updateAlertType
      );
  }
}
