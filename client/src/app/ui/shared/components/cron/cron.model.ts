/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */

export class Cron {
  minute: string;
  hour: string;
  dayOfMonth: string;
  month: string;
  dayOfWeek: string;
  toString() {
    return `${this.minute} ${this.hour} ${this.dayOfMonth} ${this.month} ${this.dayOfWeek}`
  }
  static fromString(cron: string) {
    let parts = cron.split(' ');
    let instance = new Cron();
    instance.minute = parts[0];
    instance.hour = parts[1];
    instance.dayOfMonth = parts[2];
    instance.month = parts[3];
    instance.dayOfWeek = parts[4];
    return instance;
  }
}
