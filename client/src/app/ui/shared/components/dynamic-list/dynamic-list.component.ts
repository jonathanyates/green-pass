import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { IColumn, IDynamicList } from '../../../../../../../shared/models/dynamic-list.model';
import { AppState } from '../../../../store/app.states';
import { Store } from '@ngrx/store';
import { SelectIsAdmin } from '../../../../store/authentication/authentication.selectors';
import { Observable } from 'rxjs';
import { ColumnType } from '../../../../../../../shared/models/column-type.model';
import { pagination } from 'app/shared/constants';

export function ColumnTypeAware(constructor: Function) {
  constructor.prototype.ColumnType = ColumnType;
}

const uid = () => String(Date.now().toString(32) + Math.random().toString(16)).replace(/\./g, '');

declare let $: any;

@Component({
  selector: 'gp-dynamic-list',
  templateUrl: 'dynamic-list.component.html',
  styleUrls: ['dynamic-list.component.css'],
})
@ColumnTypeAware
export class DynamicListComponent implements OnInit {
  allSelected: boolean;
  _list: IDynamicList;
  isAdmin: Observable<boolean>;
  checkAdmin: boolean = true;
  paginationId: string = uid();
  currentIndex: number;

  @Input() lightHeader: boolean;
  @Input() editing: boolean;

  @Input() itemsPerPage = pagination.pageSize;
  @Input() page: number;

  private _allowAdd: boolean;
  @Input() set allowAdd(allowAdd: boolean) {
    this.checkAdmin = false;
    this._allowAdd = allowAdd;
  }
  get allowAdd() {
    return this._allowAdd;
  }

  _total: number;
  @Input() set total(total: number) {
    this._total = total;
  }
  get total() {
    return this._total;
  }

  @Input() serverMode: boolean;

  @Input()
  set list(list: IDynamicList) {
    this.setActualIndexOnListItems(list);
    this._list = list;
    this.setAllSelected();
  }
  get list(): IDynamicList {
    return this._list;
  }

  setActualIndexOnListItems(list: IDynamicList) {
    list.items.forEach((item, i) => {
      item.index = i;
    });
  }

  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() itemSelected: EventEmitter<any> = new EventEmitter<any>();
  @Output() back: EventEmitter<any> = new EventEmitter<any>();
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  constructor(private store: Store<AppState>) {}

  private setAllSelected() {
    if (this._list && this._list.items && this._list.items.length > 0) {
      this.allSelected = this._list.items.every((item) => item.selected && item.selected === true);
    } else {
      this.allSelected = false;
    }
  }

  // setCurrentIndex(pageIndex: number): number {
  //   const currentPage = this.page ? this.page - 1 : 0;
  //   return this.itemsPerPage * currentPage + pageIndex;
  // }

  ngOnInit() {
    if (this.checkAdmin) {
      this.isAdmin = this.store.let(SelectIsAdmin());
    }
  }

  onAdd() {
    this.add.emit();
  }

  onSelect(event: Event, item, column: IColumn) {
    if (event) {
      event.stopPropagation();

      // workaround for select overriding checkbox click
      // ignore select if srcElement is an HTMLLabelElement
      if (event.srcElement instanceof HTMLLabelElement) {
        return;
      }
    }

    if (item) {
      this.itemSelected.emit(item);
    }

    this.toggleItem(event, item, null);
  }

  toggleAll() {
    this.allSelected = !this.allSelected;

    if (this.list && this.list.items) {
      this.list.items.forEach((item) => (item.selected = this.allSelected));
    }
  }

  toggleItem(event: Event, item, checkbox) {
    event.stopPropagation();

    if (item.previewSelected) {
      if (item.previewSelected(item) === false) {
        item.selected = false;
        if (checkbox) {
          checkbox.checked = false;
        }
        return;
      }
    }

    item.selected = !item.selected;
    this.setAllSelected();
  }

  onBack() {
    this.back.emit();
  }

  onEdit() {
    this.edit.emit();
  }

  onPageChange(page: number) {
    if (!this.serverMode) {
      this.page = page;
    }

    this.pageChange.emit(page);
  }
}
