import {DriverReminderScheduler} from "../drivers/driver-reminder.scheduler";

export const TYPES = {
  app: {
    express: Symbol('express')
  },
  db: {
    DbContext: Symbol('DbContext')
  },
  security: {
    AuthenticationMiddleware: Symbol('AuthenticationMiddleware'),
    AuthorizationMiddleware: Symbol('AuthorizationMiddleware'),
  },
  routers: {
    ApiRouter: Symbol('ApiRouter'),
    CompanyRouter: Symbol('CompanyRouter'),
    UserRouter: Symbol('UserRouter'),
    DriverRouter: Symbol('DriverRouter'),
    VehicleRouter: Symbol('VehicleRouter'),
    DocumentRouter: Symbol('DocumentRouter'),
    TemplateRouter: Symbol('TemplateRouter'),
    AuthenticationRouter: Symbol('AuthenticationRouter'),
    AddressRouter: Symbol('AddressRouter'),
    FileRouter: Symbol('FileRouter'),
    ComplianceRouter: Symbol('ComplianceRouter'),
    AuditRouter: Symbol('AuditRouter'),
    AlertRouter: Symbol('AlertRouter'),
    AlertTypeRouter: Symbol('AlertTypeRouter'),
    ReportRouter: Symbol('ReportRouter'),
    ReportTypeRouter: Symbol('ReportTypeRouter'),
    ReportScheduleRouter: Symbol('ReportScheduleRouter'),
    SubscriptionRouter: Symbol('SubscriptionRouter')
  },
  controllers: {
    CompanyController: Symbol('CompanyController'),
    UserController: Symbol('UserController'),
    DriverController: Symbol('DriverController'),
    VehicleController: Symbol('VehicleController'),
    DocumentController: Symbol('DocumentController'),
    TemplateController: Symbol('TemplateController'),
    AuthenticationController: Symbol('AuthenticationController'),
    AddressController: Symbol('AddressController'),
    FileController: Symbol('FileController'),
    ComplianceController: Symbol('ComplianceController'),
    AuditController: Symbol('AuditController'),
    AlertController: Symbol('AlertController'),
    ReportController: Symbol('ReportController'),
    ReportTypeController: Symbol('ReportTypeController'),
    ReportScheduleController: Symbol('ReportScheduleController'),
    SubscriptionController: Symbol('SubscriptionController')
  },
  services: {
    AuthenticationService: Symbol('AuthenticationService'),
    CompanyService: Symbol('CompanyService'),
    UserService: Symbol('UserService'),
    DriverService: Symbol('DriverService'),
    VehicleService: Symbol('VehicleService'),
    DocumentService: Symbol('DocumentService'),
    TemplateService: Symbol('TemplateService'),
    DocumentSignatureService: Symbol('DocumentSignatureService'),
    LegalesignService: Symbol('LegalesignService'),
    DvlaService: Symbol('DvlaService'),
    AddressService: Symbol('AddressService'),
    FileService: Symbol('FileService'),
    ComplianceService: Symbol('ComplianceService'),
    AssessmentService: Symbol('AssessmentService'),
    RoadMarqueService: Symbol('RoadMarqueService'),
    AuditService: Symbol('AuditService'),
    AlertService: Symbol('AlertService'),
    AlertQueryService: Symbol('AlertQueryService'),
    ReportTypeService: Symbol('ReportTypeService'),
    ReportScheduleService: Symbol('ReportScheduleService'),
    ReportQueryService: Symbol('ReportQueryService'),
    EmailService: Symbol('EmailService'),
    ReportGeneratorService: Symbol('ReportGeneratorService'),
    ReportScheduler: Symbol('ReportScheduler'),
    ReportService: Symbol('ReportService'),
    ReportTransformService: Symbol('ReportTransformService'),
    MessageService: Symbol('MessageService'),
    SubscriptionService: Symbol('SubscriptionService'),
    DriverReminderScheduler: Symbol('DriverReminderScheduler')
  },
  repositories: {
    CompanyRepository: Symbol('CompanyRepository'),
    UserRepository: Symbol('UserRepository'),
    DriverRepository: Symbol('DriverRepository'),
    VehicleRepository: Symbol('VehicleRepository'),
    DriverVehicleMapRepository: Symbol('DriverVehicleMapRepository'),
    DocumentRepository: Symbol('DocumentRepository'),
    AddressRepository: Symbol('AddressRepository'),
    FileRepository: Symbol('FileRepository'),
    ComplianceRepository: Symbol('ComplianceRepository'),
    AuditRepository: Symbol('AuditRepository'),
    AlertTypeRepository: Symbol('AlertTypeRepository'),
    AlertRepository: Symbol('AlertRepository'),
    ReportTypeRepository: Symbol('ReportTypeRepository'),
    ReportScheduleRepository: Symbol('ReportScheduleRepository'),
    SubscriptionRepository: Symbol('SubscriptionRepository'),
    TemplateRepository: Symbol('TemplateRepository')
  }
};

export default TYPES;