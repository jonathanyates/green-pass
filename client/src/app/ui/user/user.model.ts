import {IUser} from "../../../../../shared/interfaces/user.interface";

export class User implements IUser {
  _id?: string;
  _companyId?: string;
  title?: string;
  username: string;
  password: string;
  forename: string;
  surname: string;
  email?: string;
  role: string;
  resetPasswordToken?: string;
  resetPasswordExpires?: Date;
  changePassword?: boolean;
  removed?: boolean;

  constructor(companyId?:string) {
    if (companyId) this._companyId = companyId;
  }

  static getFullName(user:IUser) {
    if (!user) {
      return null;
    }
    return user.title
      ? `${user.title} ${user.forename} ${user.surname}`
      : `${user.forename} ${user.surname}`;
  }

  static clone(user: IUser) {
    return (<any>Object).assign(new User(), user);
  }

}
