import {IAllowedVehicle, ICpc, IEndorsement, ITachographCard} from "../../../../shared/interfaces/driver.interface";

export interface IDrivingLicenceEnquiry {
  driver: {
    name: string,
    dateOfBirth: Date,
    gender: string,
    address: string
  },
  licence: {
    status: string,
    validFrom: Date,
    validTo: Date,
    licenceNumber: string,
    issueNumber: number
  },
  allowedVehicles: {
    vehicles: IAllowedVehicle[],
    provisional: IAllowedVehicle[],
  },
  endorsements: IEndorsement[],
  tachographCard?: ITachographCard,
  cpc?: ICpc

  points?: number
}
