
// Base Component for Modal Dialogs which require this filthy hack
export class DialogComponent {

  active = true;

  reset() {
    // Filthy hack to reset the model form so it is pristine when reopened.
    setTimeout(() => {
      this.active = false;
      setTimeout(() => {
        this.setState();
        this.active = true;
      }, 1);
    },0)
  }

  setState() {
  }
}
