import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectDriver, SelectDriverLicence, SelectSelectedDriverDetailsTab} from "../../../store/drivers/driver.selectors";
import {AppState} from "../../../store/app.states";
import {Paths} from "../../../shared/constants";
import {
  LoadDriverAction,
  SaveDriverSuccessAction,
  SelectDriverDetailsTabAction
} from "../../../store/drivers/driver.actions";
import {IContact, IDriver, IDrivingLicence} from "../../../../../../shared/interfaces/driver.interface";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {ModalComponent} from "../../shared/components/modal/model.component";
import {VehicleTypes} from "../../../../../../shared/interfaces/constants";
import {VehicleService} from "../../../services/vehicle.service";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {SelectCompany} from "../../../store/company/company.selectors";
import {ISubscription} from "../../../../../../shared/interfaces/subscription.interface";
import {SubscriptionHelper} from "../../../../../../shared/models/subscription.model";
import {IFawCertificate, IInsurance} from "../../../../../../shared/interfaces/common.interface";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {BusyAction} from "../../../store/progress/progress.actions";
import {DriverService} from "../../../services/driver.service";
import {Roles} from "../../../../../../shared/constants";
import {IDocument} from "../../../../../../shared/interfaces/document.interface";

@Component({
  selector: 'gp-driver-tab-list-container',
  templateUrl: 'driver-details-tab-list.component.html'
})
export class DriverDetailsTabListComponent implements OnInit {

  driver: IDriver;
  selectedTab: string;
  licence: IDrivingLicence;
  nextCheckDate: Date;
  user: IUser;
  company: ICompany;
  selectedVehicle: IVehicle;
  subscription: ISubscription;
  isDriver: boolean;
  showVehicles: boolean = false;
  showInsurance: boolean = false;
  showAssessments: boolean = false;
  allowInput: boolean = false;
  protected subscriptions: Subscription[] = [];

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private driverService: DriverService) { }

  ngOnInit() {

    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .combineLatest(this.store.let(SelectLoggedInUser()))
      .subscribe(results => {
        this.company = results[0];
        this.user = results[1];
        this.isDriver = this.user && this.user.role == Roles.Driver;
        this.allowInput = this.isDriver === false ||
          (this.company.settings && this.company.settings.drivers && this.company.settings.drivers.allowInput);
        this.subscription = SubscriptionHelper.getSubscription(this.company);
        this.showAssessments = this.subscription &&
          (this.subscription.resources.onLineAssessments || this.subscription.resources.onRoadTraining);
      }));

    this.subscriptions.push(
      this.store.let(SelectDriver())
      .filter(driver => driver !== null)
      .subscribe(driver => {
        this.driver = driver;
        this.nextCheckDate = driver.licence ? driver.licence.nextCheckDate : null;
        this.showInsurance = this.driver.ownedVehicle ||
          driver.vehicles && driver.vehicles.some((vehicle:IVehicle) => vehicle.type === VehicleTypes.grey);

        this.vehicleService.getVehicleSummary(this.driver._companyId)
          .subscribe(summary => {
            this.showVehicles = this.driver.ownedVehicle || summary.grey > 0;
          });
      }));

    this.subscriptions.push(this.store.let(SelectDriverLicence())
        .subscribe(licence => this.licence = licence));

    this.subscriptions.push(this.store.let(SelectSelectedDriverDetailsTab())
      .filter(tab => tab != null)
      .subscribe(tab => this.selectedTab = tab));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  selectTab(tab: string) {
    this.store.dispatch(new SelectDriverDetailsTabAction(tab));
  }

  editNextOfKin() {
    return this.router.navigate(['./', Paths.NextOfKin, Paths.Edit], { relativeTo: this.route });
  }

  editDetails() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  addReference() {
    return this.router.navigate(['./', Paths.References, Paths.New], { relativeTo: this.route });
  }

  selectReference(contact: IContact) {
    return this.router.navigate(['./', Paths.References, contact._id], { relativeTo: this.route });
  }

  addVehicle() {
    return this.router.navigate(['./', Paths.Vehicles, Paths.Add], { relativeTo: this.route });
  }

  removeVehicle(vehicle: IVehicle) {
    if (!vehicle) return;
    this.modal.open();
    this.selectedVehicle = vehicle;
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedVehicle) {

      this.store.dispatch(new BusyAction());

      this.driverService.removeVehicle(this.driver, this.selectedVehicle._id)
        .subscribe(driver => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new SaveDriverSuccessAction(driver));
        }, error => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new HandleErrorAction(error));
        })
    }
    this.selectedVehicle = null;
  }

  selectVehicle(vehicle: IVehicle) {
    return this.router.navigate([Paths.Company, this.driver._companyId, Paths.Vehicles, vehicle._id]);
  }

  addInsurance() {
    return this.router.navigate(['./', Paths.Insurance, Paths.New], { relativeTo: this.route });
  }

  deleteInsurance(insurance: IInsurance) {
    if (insurance) {
      this.store.dispatch(new BusyAction());
      this.driverService.deleteDriverInsurance(this.driver, insurance)
        .subscribe(driver => {
          this.store.dispatch(new LoadDriverAction(this.company._id, this.driver._id));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
  }

  selectInsurance(insurance: IInsurance) {
    return this.router.navigate(['./', Paths.Insurance, insurance._id], { relativeTo: this.route });
  }

  addFaw() {
    return this.router.navigate(['./', Paths.Faw, Paths.New], { relativeTo: this.route });
  }

  deleteFaw(fawCertificate: IFawCertificate) {
    if (fawCertificate) {
      this.store.dispatch(new BusyAction());
      this.driverService.deleteDriverFaw(this.driver, fawCertificate)
        .subscribe(driver => {
          this.store.dispatch(new LoadDriverAction(this.company._id, this.driver._id));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
  }

  selectFaw(fawCertificate: IFawCertificate) {
    return this.router.navigate(['./', Paths.Faw, fawCertificate._id], { relativeTo: this.route });
  }

  selectDocument(document: IDocument) {
    return this.router.navigate(['./', Paths.Documents, document.documentId], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
