import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import codes from '../shared/error.codes';
import { IAlertService } from './alert.service';
import { AuditActions, AuditTargets, IAuditService } from '../audit/audit.service';

export interface IAlertController {
  getAlertSummaries(req: Request, res: Response, next: NextFunction);
  getAlertSummary(req: Request, res: Response, next: NextFunction);
  getAlertTypes(req: Request, res: Response, next: NextFunction);
  getAll(req: Request, res: Response, next: NextFunction);
  get(req: Request, res: Response, next: NextFunction);
  add(req: Request, res: Response, next: NextFunction);

  addAlertType(req: Request, res: Response, next: NextFunction);
  updateAlertType(req: Request, res: Response, next: NextFunction);

  getAlertTypeSummary(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class AlertController implements IAlertController {
  constructor(
    @inject(TYPES.services.AlertService) private alertService: IAlertService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  getAlertSummaries = (req: Request, res: Response, next: NextFunction) => {
    return this.alertService
      .getAlertSummaries()
      .then((alertSummary) => {
        if (!alertSummary) {
          return res.status(codes.NOT_FOUND);
        }
        return res.status(codes.OK).json(alertSummary);
      })
      .catch((err) => {
        next(err);
      });
  };

  getAlertSummary = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    return this.alertService
      .getAlertSummary(companyId)
      .then((alertSummary) => {
        if (!alertSummary) {
          return res.status(codes.NOT_FOUND);
        }
        return res.status(codes.OK).json(alertSummary);
      })
      .catch((err) => {
        next(err);
      });
  };

  getAlertTypes = (req: Request, res: Response, next: NextFunction) => {
    return this.alertService
      .getAlertTypes()
      .then((alertTypes) => {
        if (!alertTypes) {
          return res.status(codes.NOT_FOUND);
        }
        return res.status(codes.OK).json(alertTypes);
      })
      .catch((err) => {
        next(err);
      });
  };

  getAll = (req: Request, res: Response, next: NextFunction) => {
    const criteria = {
      _companyId: new mongoose.Types.ObjectId(req.params.id),
    };

    let page, pageSize: number;

    if (req.query) {
      if (req.query.alertType) {
        const alertType = req.query.alertType as string;
        criteria['alertType._id'] = new mongoose.Types.ObjectId(alertType);
        delete req.query.alertType;
      }

      if (req.query.page && req.query.pageSize) {
        page = +req.query.page;
        pageSize = +req.query.pageSize;
        delete req.query.page;
        delete req.query.pageSize;
      }

      for (const param in req.query) {
        if (req.query.hasOwnProperty(param)) {
          const pattern = req.query[param] as string;
          criteria[param] = { $regex: new RegExp(pattern, 'i') };
        }
      }
    }

    this.alertService
      .search(criteria, page, pageSize)
      .then((alerts) => res.status(codes.OK).json(alerts))
      .catch((err) => next(err));
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.alertId;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid alert id.' });

    this.alertService
      .get(id)
      .then((alert) => {
        if (!alert) {
          return res.status(codes.NOT_FOUND).send({ message: 'Alert not found.' });
        }

        return res.status(codes.OK).json(alert);
      })
      .catch((err) => {
        next(err);
      });
  };

  add = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const alert = req.body;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    if (!alert) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No alert specified.' });
    }

    alert._companyId = companyId;

    this.alertService
      .add(companyId, alert)
      .then((alert) => {
        return res.status(codes.CREATED).json(alert);
      })
      .catch((err) => next(err));
  };

  addAlertType = (req: Request, res: Response, next: NextFunction) => {
    const alertType = req.body;

    if (!alertType) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No user specified.' });
    }

    this.alertService
      .addAlertType(alertType)
      .then((alertType) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.AlertType, alertType);
        res.status(codes.CREATED).json(alertType);
      })
      .catch((err) => next(err));
  };

  updateAlertType = (req: Request, res: Response, next: NextFunction) => {
    const alertType = req.body;

    if (!alertType) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No user specified.' });
    }

    this.alertService
      .updateAlertType(alertType)
      .then((alertType) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.AlertType, alertType);
        res.status(codes.ACCEPTED).json(alertType);
      })
      .catch((err) => next(err));
  };

  getAlertTypeSummary = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    this.alertService
      .getAlertTypeSummary(companyId)
      .then((summaries) => res.status(codes.CREATED).json(summaries))
      .catch((err) => next(err));
  };
}
