import 'reflect-metadata';
import { DvlaService } from './dvla.service';

describe('dvla Service', function () {
  describe('vehicleEnquiry', function () {
    //this.timeout(20000);

    it('gets vehicle enquiry', function () {
      expect(true).toBeTruthy();

      const registrationNumber = 'K2JPY';
      const vehicleMake = 'AUDI';

      const expected = {
        isValidTax: true,
        taxDueDate: '0117-04-01T00:00:00.000Z',
        isValidMot: true,
        motExpiryDate: '0117-08-04T00:00:00.000Z',
        vehicleMake: 'AUDI',
        dateOfFirstRegistration: 'April 2005',
        yearOfManufacture: '2005',
        cylinderCapacityCc: '1986 cc',
        co2Emissions: '170 g/km',
        fuelType: 'DIESEL',
        exportMarker: 'No',
        vehicleStatus: 'Tax not due',
        vehicleColour: 'BLACK',
        vehicleTypeApproval: 'M1',
        wheelplan: '2 AXLE RIGID BODY',
        revenueWeight: 'Not available',
      };

      const dvlaService = new DvlaService();

      return dvlaService
        .vehicleEnquiry(registrationNumber)
        .then((result) => {
          console.log(result);
          return result;
        })
        .catch((error) => {
          console.log(error);
          throw error;
        });
    });
  });

  describe('motEnquiry', function () {
    this.timeout(20000);

    it('gets mot enquiry', function () {
      const registrationNumber = 'Sb66xkh';
      const vehicleMake = 'MERCEDES-BENZ';

      const dvlaService = new DvlaService();

      return dvlaService
        .motEnquiry(registrationNumber)
        .then((result) => {
          console.log(result);
          return result;
        })
        .catch((err) => {
          console.log(err);
        });
    });
  });

  describe('generateCheckCode', function () {
    this.timeout(20000);

    it('generates Check Code', function () {
      const drivingLicenceNumber = 'YATES607198JP9LK',
        nationalInsuranceNumber = 'NR071159A',
        postCode = 'PR31YF';

      const dvlaService = new DvlaService();

      return dvlaService
        .generateCheckCode(drivingLicenceNumber, nationalInsuranceNumber, postCode)
        .then((result) => {
          console.log(result);
          return result;
        })
        .catch((error) => {
          console.log(error);
        });
    });
  });

  describe('drivingLicenceEnquiry', function () {
    this.timeout(20000);

    it('gets driving Licence enquiry', function () {
      const drivingLicenceNumber = 'YATES607198JP9LK',
        nationalInsuranceNumber = 'NR071159A',
        postCode = 'PR31YF';

      const dvlaService = new DvlaService();

      return dvlaService
        .drivingLicenceEnquiry(drivingLicenceNumber, nationalInsuranceNumber, postCode)
        .then((result) => {
          console.log(result);
          return result;
        })
        .catch((error) => {
          console.log(error);
        });
    });
  });

  describe('drivingLicenceCheck', function () {
    this.timeout(20000);

    it('gets driving Licence check', function () {
      const drivingLicenceNumber = 'YATES607198JP9LK',
        nationalInsuranceNumber = 'NR071159A',
        postCode = 'PR31YF';

      const dvlaService = new DvlaService();

      return dvlaService
        .generateCheckCode(drivingLicenceNumber, nationalInsuranceNumber, postCode)
        .then((checkCode) => dvlaService.drivingLicenceCheck(drivingLicenceNumber, checkCode))
        .then((result) => {
          console.log(result);
          return result;
        })
        .catch((error) => {
          console.log(error);
        });
    });
  });
});
