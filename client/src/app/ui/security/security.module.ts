import { NgModule } from '@angular/core';
import {AuthorizationService} from "../../services/authorization.service";
import {AuthorizationGuard} from "./guards/authorization.guard";
import {SecurityRoutingModule} from "./security-routing.module";
import {CommonModule} from "@angular/common";
import {UnauthorizedComponent} from "./status-errors/unauthorized.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ErrorComponent} from "./status-errors/error.component";
import {PageNotFoundComponent} from "./status-errors/page-not-found.component";
import {SharedModule} from "../shared/shared.module";
import {LoginComponent} from "./login/login.component";
import {LoginStatusComponent} from "./login-status/login-status.component";
import {RedirectGuard} from "./guards/redirect.guard";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {ChangePasswordContainerComponent} from "./change-password/change-password-container.component";
import {ResetPasswordComponent} from "./change-password/reset-password.component";
import {SubscriptionGuard} from "./guards/subscription.guard";

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      SharedModule,
      SecurityRoutingModule
    ],
    exports: [
      LoginStatusComponent
    ],
    declarations: [
      LoginComponent,
      ForgotPasswordComponent,
      ChangePasswordComponent,
      ChangePasswordContainerComponent,
      ResetPasswordComponent,
      LoginStatusComponent,
      UnauthorizedComponent,
      ErrorComponent,
      PageNotFoundComponent
    ],
    providers: [
      AuthorizationService,
      AuthorizationGuard,
      RedirectGuard,
      SubscriptionGuard
    ],
})
export class SecurityModule { }
