import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {ReportState} from "./report.state";
import {SelectCompany} from "../company/company.selectors";
import {subscriptionNames} from "../../../../../shared/interfaces/subscription.interface";

export const SelectReportState = () => {
  return (store:Store<AppState>) => {
    return store.select<ReportState>(state => state.reports)
  }
};

export const SelectReportTypes = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectReportState())
      .map(state => state.reportTypes);
  }
};

export const SelectReportTypesForCompany = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectReportState())
      .filter(state => state != null && state.reportTypes != null)
      .combineLatest(store.let(SelectCompany()).take(1))
      .map(results => {
        let reportTypes = results[0].reportTypes;
        let company = results[1];

        // If subscription is not gold then filter reports for the allowed alert report types
        if (company.subscription.name !== subscriptionNames.gold) {
          let allowedTypeIds = company.alertTypes
            .map(alertType => alertType.alertType.reportType._id);

          reportTypes = reportTypes.filter(reportType =>
            allowedTypeIds.some(id => reportType._id === id));
        }

        return reportTypes;
      })

  }
};



