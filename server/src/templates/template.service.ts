import {injectable, inject} from "inversify";
import codes from "../shared/error.codes";
import {ServerError} from "../shared/errors";
import {TYPES} from "../container/container.types";
import {ITemplate} from "../../../shared/interfaces/document.interface";
import {ICompanyRepository} from "../companies/company.repository";
import {ICompany} from "../../../shared/interfaces/company.interface";
import {IRepository} from "../db/db.repository";

export interface ITemplateService {

  getTemplates(): Promise<ITemplate[]>;
  getTemplate(templateId: string): Promise<ITemplate>;
  addTemplate(template: ITemplate): Promise<ITemplate>;
  updateTemplate(template: ITemplate): Promise<ITemplate>;
  deleteTemplate(templateId: string): Promise<ITemplate>;
  getCompanyTemplates(companyId: string): Promise<ITemplate[]>;
  saveCompanyTemplates(companyId: string, templates: string[]): Promise<ICompany>;
}

@injectable()
export class TemplateService implements ITemplateService {

  constructor(
    @inject(TYPES.repositories.TemplateRepository) private templateRepository: IRepository<ITemplate>,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository) {}

  getTemplates(): Promise<ITemplate[]> {
    return this.templateRepository.find({ _companyId: null })
  }

  getTemplate(templateId: string): Promise<ITemplate> {
    return this.templateRepository.get(templateId);
  }

  addTemplate(template: ITemplate): Promise<ITemplate> {
    return this.templateRepository.add(template);
  }

  updateTemplate(template: ITemplate): Promise<ITemplate> {
    return this.templateRepository.update(template);
  }

  deleteTemplate(templateId: string): Promise<ITemplate> {
    return this.templateRepository.remove(templateId);
  }

  getCompanyTemplates = (companyId: string): Promise<ITemplate[]> => {
    return this.companyRepository.get(companyId)
      .then(company => {
        if (!company) {
          throw ServerError('Company not found', codes.NOT_FOUND);
        }

        return this.templateRepository.find({
          $or: [
            {_companyId: companyId},
            {_companyId: {$exists: false}}
          ]
        })
      });
  };

  saveCompanyTemplates = (companyId: string, templates: string[]) => {
    return this.companyRepository.updateTemplates(companyId, templates);
  };

}

