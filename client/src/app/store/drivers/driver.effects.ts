import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./driver.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Action, Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {DriverService} from "../../services/driver.service";
import {Paths} from "../../shared/constants";
import {Router} from "@angular/router";
import {LoadCompanyAction} from "../company/company.actions";
import {SelectDrivers, SelectDriver, SelectDriversState} from "./driver.selectors";
import {DrivingLicenceCheckAction, LoadDriversAction, SelectDriverSuccessAction} from "./driver.actions";
import {SelectDriversSuccessAction} from "./driver.actions";
import {LoadDriverAction} from "./driver.actions";
import {LoadDriverSuccessAction, DrivingLicenceCheckSuccessAction} from "./driver.actions";
import {BusyAction} from "../progress/progress.actions";
import {AssessmentService} from "../../services/assessment.service";
import {messenger} from "../../services/messenger";
import {Messages} from "../app.messages";
import * as DriverInsuranceActions from './driver-insurance.actions';
import * as DriverFawActions from './driver-faw.actions';
import {FileService} from "../../services/file.service";
import {SelectLoggedInUser} from "../authentication/authentication.selectors";
import {IUser} from "../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../shared/constants";

@Injectable()
export class DriverEffects {

  loggedInUser: IUser;

  constructor(private driverService: DriverService,
              private assessmentService: AssessmentService,
              private fileService: FileService,
              private actions$: Actions,
              private router: Router,
              private store: Store<AppState>) {
    this.store.let(SelectLoggedInUser())
      .subscribe(user => this.loggedInUser = user);
  }

  @Effect() selectDrivers$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_LIST)
    .map((action: actions.SelectDriversAction) => action.payload)
    .switchMap(payload => this.store.let(SelectDrivers()).take(1)
      .map(drivers => drivers && drivers.every(driver => driver._companyId === payload)
        ? new SelectDriversSuccessAction(drivers)
        : new actions.LoadDriversAction(payload))
    );

  @Effect() loadDrivers$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap((action:LoadDriversAction) => this.driverService.getDrivers(action.payload, action.page, action.pageSize, action.searchCriteria)
      .map(result => new actions.LoadDriversSuccessAction(result.items, result.total, result.page))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectDriver$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_DRIVER)
    .map((action: actions.SelectDriverAction) => action.payload)
    .switchMap(payload => {
      return this.store.let(SelectDriversState()).take(1)
        .map(state => {
          // Force Load Driver if logged into assessments
          if (!state.assessmentLoggedIn && state.selected && state.selected._id === payload.driverId) {
            return new SelectDriverSuccessAction(state.selected);
          } else {
            return new LoadDriverAction(payload.companyId, payload.driverId);
          }
        })
      }
    );

  @Effect() searchDriver$ = this.actions$
    .ofType(actions.ActionTypes.SEARCH_DRIVER)
    .map((action: actions.SearchDriverAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectDriversState()).take(1)
      .switchMap(state => {
        let driver = state.selected;

        if (!driver || driver._user._id !== payload.searchCriteria._user) {
          return this.driverService.searchDriver(payload.companyId, payload.searchCriteria)
            .map(driver => {
              return new LoadDriverSuccessAction(driver);
            })
        }

        return Observable.of<Action>(new LoadDriverSuccessAction(driver));
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() loadDriver$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_DRIVER)
    .map((action: actions.LoadDriverAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => {
      return this.driverService.getDriver(payload.companyId, payload.driverId)
        .map(driver => new LoadDriverSuccessAction(driver))
        .catch(error => Observable.of(new HandleErrorAction(error)))
        .finally(() => this.store.dispatch(new BusyAction(false)))
      }
    );

  @Effect() saveDriver$ = this.actions$
    .ofType(actions.ActionTypes.SAVE)
    .do(action => this.store.dispatch(new BusyAction()))
    .switchMap((action: actions.SaveDriverAction) => this.driverService.saveDriver(action.payload)
      .do(driver => {
        if (this.loggedInUser.role === Roles.Driver) {
          return this.router.navigate([Paths.Driver, action.redirectPath]);
        }
        this.router.navigate([Paths.Company, driver._companyId, Paths.Drivers, driver._id])
      })
      .map(driver => new actions.SaveDriverSuccessAction(driver))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        messenger.publish(Messages.SaveError, error);
        return Observable.empty();
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveDriverSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_SUCCESS)
    .map((action: actions.SaveDriverSuccessAction) => action.payload)
    .do(driver => {
      if (this.loggedInUser.role != Roles.Driver) {
        this.store.dispatch(new LoadCompanyAction(driver._companyId))
      }
    })
    .map(driver => {
      if (this.loggedInUser.role != Roles.Driver) {
        return new actions.LoadDriversAction(driver._companyId)
      }
      return { type: 'Empty' };
    });

  // Driver Reference

  @Effect() saveReference$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_DRIVER_REFERENCE)
    .map((action: actions.SaveDriverReferenceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(reference => this.store.let(SelectDriver()).take(1))
    .switchMap(dvr => this.driverService.saveDriver(dvr)
      .do(driver => {
        if (this.loggedInUser.role === Roles.Driver) {
          return this.router.navigate([Paths.Driver, Paths.References]);
        }
        this.router.navigate([Paths.Company, driver._companyId, Paths.Drivers, driver._id]);
      })
      .map(driver => new actions.SaveDriverReferenceSuccessAction(driver))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() updateDriverStatus$ = this.actions$
    .ofType(actions.ActionTypes.UPDATE_DRIVER_STATUS)
    .map((action: actions.UpdateDriverStatusAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.driverService.updateDriverStatus(payload)
      .map(driver => new actions.UpdateDriverStatusSuccessAction(driver))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  /**
   * Driver Licence Check Code
   */

  @Effect() driverLicenceCheckCode$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK_CODE)
    .map((action: actions.DrivingLicenceCheckCodeAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.driverService.getDrivingLicenceCheckCode(payload)
      .map(checkCode => new actions.DrivingLicenceCheckCodeSuccessAction(checkCode))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        return Observable.of(new actions.DrivingLicenceCheckCodeFailureAction(error))
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) driverLicenceCheckCodeSuccess$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK_CODE_SUCCESS)
    .map((action: actions.DrivingLicenceCheckCodeSuccessAction) => action.payload)
    .do(checkCode => messenger.publish(actions.ActionTypes.LICENCE_CHECK_CODE_SUCCESS, checkCode));

  @Effect({ dispatch: false }) driverLicenceCheckCodeFailure$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK_CODE_FAILURE)
    .map((action: actions.DrivingLicenceCheckCodeFailureAction) => action.payload)
    .do(error => messenger.publish(actions.ActionTypes.LICENCE_CHECK_FAILURE, error));

  /**
   * Driver Licence Checks
   */

  @Effect() driverLicenceCheck$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap((action: DrivingLicenceCheckAction) =>
        this.driverService.drivingLicenceCheck(action.payload.driver, action.payload.checkCode, action.licenceNumber)
      .map(driver => new DrivingLicenceCheckSuccessAction(driver))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        return Observable.of(new actions.DrivingLicenceCheckFailureAction(error))
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) driverLicenceCheckSuccess$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK_SUCCESS)
    .map((action: actions.DrivingLicenceCheckSuccessAction) => action.payload)
    .do(driver => this.router.navigate([Paths.Driver, Paths.Licence]));

  @Effect({ dispatch: false }) driverLicenceCheckFailure$ = this.actions$
    .ofType(actions.ActionTypes.LICENCE_CHECK_FAILURE)
    .map((action: actions.DrivingLicenceCheckFailureAction) => action.payload)
    .do(error => messenger.publish(actions.ActionTypes.LICENCE_CHECK_FAILURE, error));

  /**
   * Assessment Update
   */
  @Effect() assessmentUpdate$ = this.actions$
    .ofType(actions.ActionTypes.ASSESSMENT_UPDATE)
    .map((action: actions.AssessmentUpdateAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.assessmentService.assessmentUpdate(payload)
      .map(driver => new actions.AssessmentUpdateSuccessAction(driver))
      .catch(error => {
        this.store.dispatch(new BusyAction(false));
        return Observable.of(new actions.AssessmentUpdateFailureAction(error))
      })
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveOnRoadAssessment$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_ON_ROAD_ASSESSMENT)
    .map((action: actions.SaveOnRoadAssessmentAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.assessmentService.saveOnRoadAssessment(payload.driver, payload.assessment)
      .map(driver => new actions.SaveOnRoadAssessmentSuccessAction(driver, payload.assessment))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) saveOnRoadAssessmentSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_ON_ROAD_ASSESSMENT_SUCCESS)
    .do((action: actions.SaveOnRoadAssessmentSuccessAction) => {
      if (action.assessment._id) {
        this.router.navigate([Paths.Company, action.payload._companyId,
          Paths.Drivers, action.payload._id, Paths.Assessments, action.assessment._id]);
      } else {
        this.router.navigate([Paths.Company, action.payload._companyId,
          Paths.Drivers, action.payload._id]);
      }
    })
    .catch(error => Observable.of(new HandleErrorAction(error)))
    .finally(() => this.store.dispatch(new BusyAction(false)));

  @Effect() assessmentLoginSuccess$ = this.actions$
    .ofType(actions.ActionTypes.ASSESSMENT_LOGIN_SUCCESS)
    .switchMap((action: actions.AssessmentLoginSuccessAction) => this.store.let(SelectDriver()).take(1))
    .map(driver => new LoadDriverAction(driver._companyId, driver._id));

  // Driver Insurance

  @Effect() selectDriverInsurance$ = this.actions$
    .ofType(DriverInsuranceActions.ActionTypes.SELECT_DRIVER_INSURANCE)
    .map((action: DriverInsuranceActions.SelectDriverInsuranceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectDriver())
      .filter(driver => driver && driver._id === payload.driverId).take(1)
      .switchMap(driver => {
        let insurance = driver.insuranceHistory.find(insurance => insurance._id === payload.insuranceId);
        if (insurance == null || insurance.file || insurance.filename == null) {
          return Observable.of(new DriverInsuranceActions.SelectDriverInsuranceSuccessAction(insurance))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(driver._companyId, insurance.filename)
          .map(file => Object.assign({}, insurance, { file: file }))
          .map(insurance => new DriverInsuranceActions.SelectDriverInsuranceSuccessAction(insurance))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveDriverInsurance$ = this.actions$
    .ofType(DriverInsuranceActions.ActionTypes.SAVE_DRIVER_INSURANCE)
    .map((action: DriverInsuranceActions.SaveDriverInsuranceAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.driverService.saveDriverInsurance(payload.driver, payload.insurance)
      .do(insurance => {
        if (this.loggedInUser.role === Roles.Driver) {
          return this.router.navigate([Paths.Driver, Paths.Insurance]);
        }
        this.router.navigate([Paths.Company, payload.driver._companyId, Paths.Drivers, payload.driver._id]);
      })
      .map(insurance => new DriverInsuranceActions.SaveDriverInsuranceSuccessAction(payload.driver, insurance))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) saveDriverInsurance$Success$ = this.actions$
    .ofType(DriverInsuranceActions.ActionTypes.SAVE_DRIVER_INSURANCE_SUCCESS)
    .do((action: DriverInsuranceActions.SaveDriverInsuranceSuccessAction) =>
      this.store.dispatch(new LoadDriverAction(action.payload.driver._companyId, action.payload.driver._id)));

  // Driver Faw

  @Effect() selectDriverFaw$ = this.actions$
    .ofType(DriverFawActions.ActionTypes.SELECT_DRIVER_FAW)
    .map((action: DriverFawActions.SelectDriverFawAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.store.let(SelectDriver())
      .filter(driver => driver && driver._id === payload.driverId).take(1)
      .switchMap(driver => {
        let faw = driver.fawHistory.find(faw => faw._id === payload.fawId);
        if (faw == null || faw.file || faw.filename == null) {
          return Observable.of(new DriverFawActions.SelectDriverFawSuccessAction(faw))
            .finally(() => this.store.dispatch(new BusyAction(false)));
        }
        return this.fileService.getFile(driver._companyId, faw.filename)
          .map(file => Object.assign({}, faw, { file: file }))
          .map(faw => new DriverFawActions.SelectDriverFawSuccessAction(faw))
          .finally(() => this.store.dispatch(new BusyAction(false)))
      })
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveDriverFaw$ = this.actions$
    .ofType(DriverFawActions.ActionTypes.SAVE_DRIVER_FAW)
    .map((action: DriverFawActions.SaveDriverFawAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.driverService.saveDriverFaw(payload.driver, payload.faw)
      .do(faw => {
        if (this.loggedInUser.role === Roles.Driver) {
          return this.router.navigate([Paths.Driver, Paths.Faw]);
        }
        this.router.navigate([Paths.Company, payload.driver._companyId, Paths.Drivers, payload.driver._id]);
      })
      .map(faw => new DriverFawActions.SaveDriverFawSuccessAction(payload.driver, faw))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) saveDriverFaw$Success$ = this.actions$
    .ofType(DriverFawActions.ActionTypes.SAVE_DRIVER_FAW_SUCCESS)
    .do((action: DriverFawActions.SaveDriverFawSuccessAction) =>
      this.store.dispatch(new LoadDriverAction(action.payload.driver._companyId, action.payload.driver._id)));


}
