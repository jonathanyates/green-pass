import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {AuthenticationState} from "../../../store/authentication/authentication.reducer";
import {Roles} from "../../../../../../shared/constants";
import {SelectAuthenticationState} from "../../../store/authentication/authentication.selectors";

@Injectable()
export class RedirectGuard implements CanActivate, CanActivateChild {

  private authState:AuthenticationState;

  constructor(
    private store: Store<AppState>,
    private router: Router) {
    this.store.let(SelectAuthenticationState())
      .subscribe((authState:AuthenticationState) => this.authState = authState);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    // if the user is logged in then redirect to the default role url
    if (this.isLoggedIn()) {
      // redirect to default role url
      let user = this.authState.user;
      if (user.role == Roles.Company && user._companyId) {
        this.router.navigate([user.role, user._companyId]);
      } else {
        this.router.navigate([user.role]);
      }
      return false;
    }

    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  private isLoggedIn(): boolean {
    return this.authState != null && this.authState.user != null && this.authState.user.role != null;
  }
}
