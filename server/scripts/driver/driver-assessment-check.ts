import logger = require('winston');
import container from '../../src/container/container.config';
import { IDriverService } from '../../src/drivers/driver.service';
import { TYPES } from '../../src/container/container.types';
import { IDriver } from '../../../shared/interfaces/driver.interface';
logger.level = 'debug';

// script to run driver assessment checks immediately
// perform checks for vehicles where the tax or mot date is due within the next 7 days.

const driverService = container.get<IDriverService>(TYPES.services.DriverService);

logger.info('Performing driver assessment checks...');

driverService
  .driverAssessmentChecks()
  .then((drivers: IDriver[]) => {
    logger.info('Successfully completed Driver Assessment Checks.');
    if (drivers && drivers.length > 0) {
      logger.info(`${drivers.length} Driver assessments where updated.`);
      drivers.forEach((driver) => {
        logger.info(` - ${driver._user.username} assessments updated.`);
      });
    } else {
      logger.info('No driver assessments needed updating.');
    }
    //process.exit();
  })
  .catch((error) => {
    logger.error('Error performing driver assessment checks');
    logger.error(error.message);
    process.exit(1);
  });
