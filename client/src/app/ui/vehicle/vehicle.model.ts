import {
  IAnnualTest,
  IVehicle,
  IVehicleCheck,
  IVehicleInspection,
  IVehicleInsurance,
  IVehicleMot,
  IVehicleService,
} from '../../../../../shared/interfaces/vehicle.interface';
import { IDriver } from '../../../../../shared/interfaces/driver.interface';

export class Vehicle implements IVehicle {
  constructor(companyId?: string) {
    if (companyId) this._companyId = companyId;
  }

  _id?: string;
  _companyId: string;
  make: string;
  model: string;
  registrationNumber: string;
  category: string;
  type: string;
  dateOfFirstRegistration?: Date;
  manufactureDate?: Date;
  yearOfManufacture?: number;
  firstUsedDate?: Date;
  cylinderCapacityCc?: string;
  co2Emissions?: string;
  fuelType?: string;
  exportMarker?: string;
  vehicleStatus?: string;
  vehicleColour?: string;
  vehicleTypeApproval?: string;
  wheelplan?: string;
  revenueWeight?: string;

  dvlaCheckDate?: Date;
  taxDueDate?: Date;
  motDueDate?: Date;
  motDueText?: string;

  euroStatus?: string;
  realDrivingEmissions?: string;
  dateOfLastv5cIssued?: Date;

  motHistory: IVehicleMot[] = [];
  serviceHistory: IVehicleService[] = [];
  inspectionHistory: IVehicleInspection[] = [];
  checkHistory: IVehicleCheck[] = [];
  annualMileage?: number;

  compliant: boolean;

  drivers?: Array<IDriver>;

  removed?: boolean;

  // added fields for HGV/PSV Tests
  vehicleType?: string;
  class?: string;
  annualTestHistory: IAnnualTest[];
}
