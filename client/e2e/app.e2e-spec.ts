import { GreenPassPage } from './app.po';

describe('green-pass App', function() {
  let page: GreenPassPage;

  beforeEach(() => {
    page = new GreenPassPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
