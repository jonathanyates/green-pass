import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import codes from '../shared/error.codes';
import { IApiController } from '../interfaces/api-controller.interface';
import { IApiService } from '../interfaces/api-service.interface';
import { IReportSchedule } from '../../../shared/interfaces/report.interface';
import { IRepository } from '../db/db.repository';

@injectable()
export class ReportScheduleController implements IApiController {
  constructor(
    @inject(TYPES.repositories.ReportScheduleRepository) private reportScheduleRepository: IRepository<IReportSchedule>,
    @inject(TYPES.services.ReportScheduleService) private reportScheduleService: IApiService<IReportSchedule>
  ) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    this.reportScheduleService
      .getAll(companyId)
      .then((reportSchedule) => {
        return res.status(codes.OK).json(reportSchedule);
      })
      .catch((err) => {
        next(err);
      });
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const reportTypeId = req.params.reportTypeId;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    if (!mongoose.Types.ObjectId.isValid(reportTypeId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid report type id.' });

    this.reportScheduleService
      .findOne({ _companyId: companyId, reportType: reportTypeId })
      .then((reportSchedule) => {
        return res.status(codes.OK).json(reportSchedule);
      })
      .catch((err) => {
        next(err);
      });
  };

  add = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const reportSchedule = req.body;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    if (!reportSchedule) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No report schedule specified.' });
    }

    this.reportScheduleService
      .add(companyId, reportSchedule)
      .then((reportSchedule) => {
        return res.status(codes.CREATED).json(reportSchedule);
      })
      .catch((err) => {
        next(err);
      });
  };

  update = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const reportSchedule = req.body;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    if (!reportSchedule) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No report schedule specified.' });
    }

    this.reportScheduleService
      .update(reportSchedule)
      .then((reportSchedule) => {
        return res.status(codes.ACCEPTED).json(reportSchedule);
      })
      .catch((err) => {
        next(err);
      });
  };

  remove = (req: Request, res: Response, next: NextFunction) => {
    const reportScheduleId = req.params.reportScheduleId;

    if (!mongoose.Types.ObjectId.isValid(reportScheduleId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid report Schedule Id.' });

    this.reportScheduleRepository
      .remove(reportScheduleId)
      .then((reportSchedule) => {
        return res.status(codes.OK).json(reportSchedule);
      })
      .catch((err) => {
        next(err);
      });
  };
}
