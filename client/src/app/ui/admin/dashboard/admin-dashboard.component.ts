import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {LoadComplianceAction} from "../../../store/compliance/compliance.actions";
import {SelectTotalCompliance} from "../../../store/compliance/compliance.selectors";
import {SelectAlertSummary} from "../../../store/alerts/alert.selectors";
import {LoadAlertSummaryAction} from "../../../store/alerts/alert.actions";
import {IAlertSummary} from "../../../../../../shared/interfaces/alert.interface";

@Component({
  selector: 'gp-admin-dashboard',
  templateUrl: 'admin-dashboard.component.html'
})
export class AdminDashboardComponent implements OnInit, OnDestroy {

  compliance: any;
  alertSummary: IAlertSummary;
  private subscriptions = [];

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new LoadComplianceAction());
    this.store.dispatch(new LoadAlertSummaryAction());

    this.subscriptions.push(this.store.let(SelectTotalCompliance())
      .subscribe(compliance => {
        this.compliance = compliance;
      }));

    this.subscriptions.push(this.store.let(SelectAlertSummary())
      .subscribe(alertSummary => {
        if (!alertSummary) {
          this.alertSummary = {
            driverAlerts: 0,
            vehicleAlerts: 0
          };
          return;
        }

        this.alertSummary = alertSummary.reduce((acc, summary) => {
          return {
            driverAlerts: acc.driverAlerts + summary.driverAlerts,
            vehicleAlerts: acc.vehicleAlerts + summary.vehicleAlerts
          }
        }, { driverAlerts: 0, vehicleAlerts: 0 });
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}
