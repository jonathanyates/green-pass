import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {AddressInputMapper} from "../../../shared/components/address-input/address-input.mapper";
import {Injectable} from "@angular/core";
import {IContact} from "../../../../../../../shared/interfaces/driver.interface";
import {RegExpPatterns} from "../../../../../../../shared/constants";

@Injectable()
export class DriverReferenceInputMapper {

  constructor(private addressInputMapper: AddressInputMapper) {}

  mapFrom(form:FormGroup): IContact {

    let reference:any = {};
    reference.forename = form.controls['forename'].value;
    reference.surname = form.controls['surname'].value;
    reference.phone = form.controls['phone'].value;
    reference.mobile = form.controls['mobile'].value;
    reference.email = form.controls['email'].value;
    reference.address = this.addressInputMapper.mapFrom(form);
    return reference;
  }

  mapTo(reference: IContact): FormInputGroup[] {

    return [
      {
        label: 'Reference',
        inputs: [
          { id: 'forename', label: 'Forename', type:'text', required: true, autoFocus: true, value: reference.forename },
          { id: 'surname', label: 'Surname', type:'text', required: true, value: reference.surname },
          { id: 'phone', label: 'Phone number', type:'text', required: true, value: reference.phone,
            regex: RegExpPatterns.Phone },
          { id: 'mobile', label: 'Mobile number', type:'text', required: false, value: reference.mobile,
            regex: RegExpPatterns.Mobile },
          { id: 'email', label: 'Email', type:'text', required: false, value: reference.email,
            regex: RegExpPatterns.Email },
        ]
      },
      this.addressInputMapper.mapTo(reference.address)
    ];
  }

}
