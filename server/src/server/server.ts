import 'reflect-metadata';
import express = require('express');
import bodyParser = require('body-parser');
import logger = require('winston');
import cors = require('cors');
import { Express } from 'express-serve-static-core';
import nocache = require('nocache');
import { ApiRouter } from './server.routes';
import { TYPES } from '../container/container.types';
import container from '../container/container.config';
import { ErrorHandler } from './error.handler';
import { configureErrorLoggerMiddleware, configureLoggerMiddleware } from './logger.middleware';

export class Server {
  private app: Express = express();
  private apiRouter: ApiRouter;

  constructor() {
    this.configure();
  }

  private configure() {
    this.configureContainer();
    this.configureMiddleware();
    this.configureRoutes();
    this.configurePostRoutesMiddleware();
  }

  private configureContainer() {
    this.apiRouter = container.get<ApiRouter>(TYPES.routers.ApiRouter);
  }

  private configureMiddleware() {
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());

    configureLoggerMiddleware(this.app);

    this.app.use(cors());
    this.app.use(nocache());
  }

  private configureRoutes() {
    this.app.all('/', function (req, res) {
      res.sendFile('public/index.html', { root: __dirname });
    });

    this.apiRouter.configure(this.app);
  }

  private configurePostRoutesMiddleware() {
    configureErrorLoggerMiddleware(this.app);
    this.app.use(ErrorHandler);
  }

  start() {
    const port = process.env.PORT || 3000;
    const server = this.app.listen(port);
    logger.log('info', 'Green Pass server is running on port ' + port + '.');

    logger.log('info', 'process.env.NODE_ENV = ', process.env.NODE_ENV);
    logger.log('info', 'process.env.BASE_URL = ', process.env.BASE_URL);
    logger.log('info', 'process.env.RABBITMQ_URL = ', process.env.RABBITMQ_URL);
    logger.log('info', 'process.env.RABBITMQ_USER = ', process.env.RABBITMQ_USER);
    logger.log('info', 'process.env.RABBITMQ_PASSWORD = ', process.env.RABBITMQ_PASSWORD);
  }
}
