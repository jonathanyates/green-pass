import { FormInputGroup } from '../../shared/components/dynamic-form/dynamic-form.model';
import { FormGroup } from '@angular/forms';
import { Driver } from '../driver.model';
import { AddressInputMapper } from '../../shared/components/address-input/address-input.mapper';
import { FormatDateForInput, GetDate } from '../../../../../../shared/utils/helpers';
import { Injectable } from '@angular/core';
import { RegExpPatterns } from '../../../../../../shared/constants';
import { IDriver } from '../../../../../../shared/interfaces/driver.interface';
import { ICompanySettings } from '../../../../../../shared/interfaces/company-settings.interface';

@Injectable()
export class DriverInputMapper {
  private driverAddressGroup: FormInputGroup;

  constructor(private addressInputMapper: AddressInputMapper) {}

  mapFrom(form: FormGroup): IDriver {
    const driver = <IDriver>{
      _user: {
        title: form.controls['title'].value,
        forename: form.controls['forename'].value,
        surname: form.controls['surname'].value,
        email: form.controls['email'].value,
      },
      gender: form.controls['gender'].value,
      licenceNumber: form.controls['licenceNumber'].value,
      niNumber: form.controls['niNumber'].value,
      dateOfBirth: GetDate(form.controls['dateOfBirth'].value),

      phone: form.controls['phone'].value,
      mobile: form.controls['mobile'].value,

      address: this.addressInputMapper.mapFrom(form),
    };

    if (form.contains('ownedVehicle')) {
      driver.ownedVehicle = form.controls['ownedVehicle'].value === true;
    }

    if (form.contains('faw')) {
      driver.fawRequired = form.controls['faw'].value === true;
    }

    if (form.contains('dbs')) {
      driver.dbsExpiry = GetDate(form.controls['dbs'].value);
    }

    if (form.contains('pNumber')) {
      driver.pNumber = form.controls['pNumber'].value;
    }

    return driver;
  }

  mapTo(driver: Driver, settings: ICompanySettings, isDriver: boolean = true): FormInputGroup[] {
    this.driverAddressGroup = this.addressInputMapper.mapTo(driver.address, '', isDriver);

    const titles = [
      { key: 'Mr', value: 'Mr', selected: driver._user.title === 'Mr' },
      { key: 'Mrs', value: 'Mrs', selected: driver._user.title === 'Mrs' },
      { key: 'Ms', value: 'Ms', selected: driver._user.title === 'Ms' },
      { key: 'Dr', value: 'Dr', selected: driver._user.title === 'Dr' },
    ];

    const genders = [
      { key: 'male', value: 'Male', selected: driver.gender === 'Male' },
      { key: 'female', value: 'Female', selected: driver.gender === 'Female' },
    ];

    let inputGroup = [
      {
        label: 'Driver',
        inputs: [
          {
            id: 'title',
            label: 'Title',
            type: 'dropdown',
            required: true,
            autoFocus: true,
            options: titles,
            value: driver._user && driver._user.title ? driver._user.title : null,
          },
          {
            id: 'forename',
            label: 'Forename',
            type: 'text',
            required: true,
            autoFocus: false,
            value: driver._user ? driver._user.forename : null,
          },
          {
            id: 'surname',
            label: 'Surname',
            type: 'text',
            required: true,
            value: driver._user ? driver._user.surname : null,
          },
          {
            id: 'licenceNumber',
            label: 'Licence Number `no spaces`',
            type: 'text',
            required: isDriver,
            regex: RegExpPatterns.DrivingLicenceNumber,
            maxLength: 18,
            uppercase: true,
            value: driver.licence ? driver.licenceNumber : null,
          },
          {
            id: 'niNumber',
            label: 'National Insurance Number `no spaces`',
            type: 'text',
            required: isDriver,
            regex: RegExpPatterns.NiNumber,
            maxLength: 9,
            uppercase: true,
            value: driver.licence ? driver.niNumber : null,
          },
          {
            id: 'dateOfBirth',
            label: 'Date of birth',
            type: 'date',
            required: isDriver,
            value: FormatDateForInput(driver.dateOfBirth),
          },
          {
            id: 'gender',
            label: 'Gender',
            type: 'radio',
            required: isDriver,
            options: genders,
            value: driver.gender ? driver.gender : null,
          },
          {
            id: 'ownedVehicle',
            label: 'Owned Vehicle',
            type: 'checkbox',
            required: false,
            value: driver.ownedVehicle,
            hidden: isDriver,
          },
        ],
      },
      {
        label: 'Contact',
        inputs: [
          {
            id: 'email',
            label: 'Email',
            type: 'text',
            required: true,
            regex: RegExpPatterns.Email,
            value: driver._user ? driver._user.email : null,
          },
          {
            id: 'phone',
            label: 'Phone',
            type: 'text',
            required: false,
            regex: RegExpPatterns.Phone,
            value: driver ? driver.phone : null,
          },
          {
            id: 'mobile',
            label: 'Mobile',
            type: 'text',
            required: false,
            regex: RegExpPatterns.Mobile,
            value: driver ? driver.mobile : null,
          },
        ],
      },
      this.driverAddressGroup,
    ];

    if (!isDriver) {
      if (settings.drivers.includePNumber) {
        inputGroup[0].inputs.splice(5, 0, {
          id: 'pNumber',
          label: 'PNumber',
          type: 'text',
          required: false,
          value: driver.pNumber,
        });
      }

      if (settings.drivers.includeFaw) {
        inputGroup.push({
          label: 'First Aid At Work',
          inputs: [
            {
              id: 'faw',
              label: 'Requires First Aid Certificate?',
              type: 'checkbox',
              required: false,
              value: driver.fawRequired,
            },
          ],
        });
      }

      if (settings.drivers.includeDbs) {
        inputGroup.push({
          label: 'Disclosure and Barring Service Certificate',
          inputs: [
            {
              id: 'dbs',
              label: 'Certificate Expiry Date',
              type: 'date',
              required: false,
              value: FormatDateForInput(driver.dbsExpiry),
            },
          ],
        });
      }
    }

    return inputGroup;
  }
}
