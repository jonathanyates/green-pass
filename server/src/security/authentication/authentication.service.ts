import { inject, injectable } from 'inversify';
import { TYPES } from '../../container/container.types';
import { IAuditService } from '../../audit/audit.service';
import { IUser } from '../../../../shared/interfaces/user.interface';
import codes from '../../shared/error.codes';
import { ServerError } from '../../shared/errors';
import crypto = require('crypto');
import logger = require('winston');
import bcrypt = require('bcrypt-nodejs');
import serverConfig from '../../server/server.config';
import { IUserService } from '../../users/user.service';
import { IMessageService } from '../../messaging/message.service';
import { queueNames } from '../../messaging/messages.constants';
import { IEmail } from '../../email/email.service';

export interface IAuthenticationService {
  forgotPassword(email: string, host: string);
  changePassword(user: IUser, password: string);
}

@injectable()
export class AuthenticationService implements IAuthenticationService {
  constructor(
    @inject(TYPES.services.AuditService) private auditService: IAuditService,
    @inject(TYPES.services.UserService) private userService: IUserService,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  forgotPassword(email: string, host: string) {
    return this.userService
      .findOne({ email: email })
      .then((user) => {
        if (!user) {
          throw ServerError(`No account with that email address exists.`, codes.NOT_FOUND);
        }

        return <Promise<IUser>>new Promise((resolve, reject) => {
          crypto.randomBytes(20, (err, buf) => {
            if (err) return reject(err);
            const token = buf.toString('hex');
            const expires = new Date();
            expires.setHours(expires.getHours() + 1);
            user.resetPasswordToken = token;
            user.resetPasswordExpires = expires;
            return resolve(user);
          });
        });
      })
      .then((user) => this.userService.update(user))
      .then((user) => {
        const url = serverConfig.client.baseUrl + '/resetPassword/' + user.resetPasswordToken;

        const text = `
        <p>You are receiving this because you have requested the reset of the password for your account.</p>
        <p>Please click on the following link, or paste this into your browser to complete the process.</p>
        <a href="${url}">${url}</a>  
        <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;

        return this.messageService
          .send(queueNames.sendEmail, <IEmail>{
            recipients: [user.email],
            from: 'admin@greenpasscompliance.co.uk',
            subject: 'Green Pass Password Reset',
            content: text,
          })
          .then((success) => {
            if (!success) {
              throw ServerError('Failed to send email to user.', codes.INTERNAL_SERVER_ERROR);
            }

            return user;
          })
          .catch((error) => {
            logger.error('Failed to send reset password email to user. Error:' + error);
            throw ServerError(`Failed to send reset password email to user.`, codes.INTERNAL_SERVER_ERROR);
          });
      });
  }

  changePassword(user: IUser, password: string) {
    return this.encryptPassword(password)
      .then((hash) => {
        user.password = hash;
        user.resetPasswordToken = null;
        user.resetPasswordExpires = null;
        user.changePassword = false;
        return this.userService.update(user);
      })
      .then((user) => this.sendPasswordChangeEmail(user));
  }

  encryptPassword(password: string): Promise<string> {
    const SALT_FACTOR = 5;

    return new Promise((resolve, reject) => {
      bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
        if (err) {
          return reject(err);
        }

        bcrypt.hash(password, salt, null, function (err, hash) {
          if (err) {
            return reject(err);
          }
          resolve(hash);
        });
      });
    });
  }

  sendPasswordChangeEmail(user: IUser): Promise<IUser> {
    const text = `
<p>Hi ${user.forename},</p>
<p>This is a confirmation that the password for your account '${user.email}' has just been changed.</p>
`;

    return this.messageService
      .send(queueNames.sendEmail, <IEmail>{
        recipients: [user.email],
        from: 'admin@greenpasscompliance.co.uk',
        subject: 'Your Green Pass password has been changed.',
        content: text,
      })
      .then((success) => {
        if (!success) {
          logger.error('Failed to send password change confirmation email to user.');
        }
        return user;
      })
      .catch((error) => {
        logger.error('Failed to send password change confirmation email to user. Error:' + error.message);
        return user;
      });
  }
}
