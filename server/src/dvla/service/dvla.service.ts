import request = require('request-promise-native');
import logger = require('winston');
import codes from '../../shared/error.codes';
import { injectable } from 'inversify';
import { IDvlaService } from './dvla.service.interface';
import { IDvlaVehicleEnquiryResponse } from '../interfaces/vehicle-enquiry.interface';
import { IDrivingLicenceEnquiry } from '../interfaces/driving-licence-enquiry.interface';
import { DvlaResultParser } from './dvla-result.parser';
import serverConfig from '../../server/server.config';
import { RequestPromiseOptions } from 'request-promise-native';
import { IDvsaMotEnquiryReponse, IMotEnquiry } from '../interfaces/dvsa-mot-enquiry.interface';
import { IDvsaHgvTestEnquiryReponse, IDvsaHgvTestEnquiry } from '../interfaces/dvsa-hgv-test-enquiry.interface';

@injectable()
export class DvlaService implements IDvlaService {
  private proxy = '';

  async motEnquiry(registrationNumber: string): Promise<IMotEnquiry> {
    try {
      logger.info(`Contacting DVSA MOT API Service for registrationNumber (${registrationNumber}).`);

      const uri = `${serverConfig.dvsa.mot.uri}?registration=${registrationNumber}`;

      const options: RequestPromiseOptions = {
        method: 'get',
        headers: {
          'x-api-key': serverConfig.dvsa.mot.apiKey,
          Accept: 'application/json+v6',
        },
      };

      const response = await request.get(uri, options);
      const motEnquiryResponse: IDvsaMotEnquiryReponse = JSON.parse(response);
      const motEnquiry = motEnquiryResponse && motEnquiryResponse.length > 0 && motEnquiryResponse[0];

      if (!motEnquiry) {
        logger.info(`DVSA API MOT Enquiry Result contains no details for registration ${registrationNumber} failed.`);
        return null;
      }

      logger.info(`Successfully processed MOT enquiry for registrationNumber ${registrationNumber}.`, {
        results: motEnquiry,
      });
      return motEnquiry;
    } catch (error) {
      this.handleError('DVSA API MOT Enquiry Request failed.', error);
    }
  }

  async hgvTestEnquiry(registrationNumber: string): Promise<IDvsaHgvTestEnquiry> {
    try {
      logger.info(`Contacting DVSA HGV Annual Test API Service for registrationNumber (${registrationNumber}).`);

      const uri = `${serverConfig.dvsa.hgvTest.uri}?registrationsOrVins=${registrationNumber}`;

      const options: RequestPromiseOptions = {
        method: 'get',
        headers: {
          'x-api-key': serverConfig.dvsa.hgvTest.apiKey,
          Accept: 'application/json+v7',
        },
      };

      const response = await request.get(uri, options);
      const hgvEnquiryResponse: IDvsaHgvTestEnquiryReponse = JSON.parse(response);
      const hgvEnquiry = hgvEnquiryResponse && hgvEnquiryResponse.length > 0 && hgvEnquiryResponse[0];

      if (!hgvEnquiry) {
        logger.info(
          `DVSA API HGV Annual Test Enquiry Result contains no details for registration ${registrationNumber} failed.`
        );
        return null;
      }

      logger.info(`Successfully processed HGV Annual Test enquiry for registrationNumber ${registrationNumber}.`, {
        results: hgvEnquiry,
      });
      return hgvEnquiry;
    } catch (error) {
      this.handleError('DVLA API HGV Annual Test Enquiry Request failed.', error);
    }
  }

  handleError(message: string, error) {
    const errorCode = error.httpStatus || error.code || 'Unknown error code';
    const errorMessage = error.errorMessage || error.message || 'An error occurred';
    logger.error(`Error: ${errorCode}. Message: ${errorMessage}`);
  }

  async vehicleEnquiry(registrationNumber: string): Promise<IDvlaVehicleEnquiryResponse> {
    const uri = serverConfig.dvla.uri;
    logger.info(`Contacting the DVLA API Vehicle Enquiry Service at (${uri})`);
    logger.info(`Checking registrationNumber (${registrationNumber})...`);

    const options: RequestPromiseOptions = {
      method: 'post',
      headers: {
        'x-api-key': serverConfig.dvla.apiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ registrationNumber: registrationNumber }),
    };

    return request
      .post(uri, options)
      .catch((err) => {
        let error = err;
        if (error.errors && error.errors.length > 0) {
          error = err.errors[0];
          logger.error('DVLA API Vehicle Enquiry Request failed.', error);
          return;
        }
        if (error.message) {
          logger.error('DVLA API Vehicle Enquiry Request failed.', error.message);
          return;
        }
        logger.error('DVLA API Vehicle Enquiry Request failed.', error);
      })
      .then((response) => {
        const vehicleEnquiry: IDvlaVehicleEnquiryResponse = JSON.parse(response);
        logger.info(`Successfully processed Vehicle enquiry results.`, {
          results: vehicleEnquiry,
        });
        return vehicleEnquiry;
      });
  }

  generateCheckCode = (
    drivingLicenceNumber: string,
    nationalInsuranceNumber: string,
    postCode: string
  ): Promise<string> => {
    const baseUrl = 'https://www.viewdrivingrecord.service.gov.uk';

    // cookie store to pass to redirect. This is needed for redirects
    const jar = request.jar();

    logger.debug(`
Getting CheckCode for
driving licence number (${drivingLicenceNumber}), 
national insurance number (${nationalInsuranceNumber}) 
and postcode (${postCode})...`);

    return new Promise((resolve, reject) => {
      logger.debug(`Contacting DVLA driving licence check service...`);

      request
        .post(
          {
            jar: jar,
            url: `${baseUrl}/driving-record/licence-number`,
            headers: { connection: 'keep-alive' },
            form: {
              applicantPassportNumber: null,
              pesel: '5236945509ed9cf9d7763fb124b7c0de',
              dln: drivingLicenceNumber,
              nino: nationalInsuranceNumber,
              postcode: postCode,
              dwpPermission: 1,
            },
            proxy: this.proxy,
          },
          (err, response) => {
            if (err) {
              if (err.code === 'ENOTFOUND') {
                const error: any = new Error('Dvla Service Unavailable.');
                error.statusCode = codes.NOT_FOUND;
                reject(error);
              }
              reject(err);
            }

            // process redirect
            const url = `${baseUrl}${response.headers.location}`;

            logger.debug(`Successfully contacted dvla.`);
            logger.debug(`Redirecting to viewbydln...`);

            request
              .get({
                url: url,
                jar: jar,
                headers: { connection: 'keep-alive' },
                proxy: this.proxy,
              })
              .catch((err) => {
                if (err.code === 'ENOTFOUND') {
                  const error: any = new Error('Dvla Service Unavailable.');
                  error.statusCode = codes.NOT_FOUND;
                  return reject(error);
                }
                return reject(err);
              })
              .then((body) => {
                logger.debug(`Successfully received driving licence check details`);

                request
                  .post(
                    {
                      url: 'https://www.viewdrivingrecord.service.gov.uk/driving-record/share/code',
                      jar: jar,
                      headers: { connection: 'keep-alive' },
                      proxy: this.proxy,
                    },
                    (err, response) => {
                      if (err) {
                        const error: any = new Error('Unable to generate check code.');
                        error.statusCode = codes.NOT_FOUND;
                        reject(error);
                      }

                      request
                        .get({
                          url: 'https://www.viewdrivingrecord.service.gov.uk/driving-record/share/',
                          jar: jar,
                          headers: { connection: 'keep-alive' },
                          proxy: this.proxy,
                        })
                        .then((body) => {
                          logger.debug(`Getting check code...`);

                          const result = DvlaResultParser.parseDrivingLicenceEnquiryCheckCode(body);

                          logger.debug(`CheckCode = ${result}`);

                          resolve(result);
                        })
                        .catch((error) => {
                          logger.debug(error);
                        });
                    }
                  )
                  .catch((error) => {
                    // Error is fine if it is a 303 redirect status code
                    if (error.statusCode !== codes.SEE_OTHER) {
                      logger.debug(error);
                      reject(error);
                    }
                  });
              });
          }
        )
        .catch((error) => {
          // Error is fine if it is a 303 redirect status code
          if (error.statusCode !== codes.SEE_OTHER) {
            logger.debug(error);
            reject(error);
          }
        });
    });
  };

  drivingLicenceEnquiry = (
    drivingLicenceNumber: string,
    nationalInsuranceNumber: string,
    postCode: string
  ): Promise<IDrivingLicenceEnquiry> => {
    const baseUrl = 'https://www.viewdrivingrecord.service.gov.uk';

    // cookie store to pass to redirect. This is needed for redirects
    const jar = request.jar();

    logger.debug(`
Processing driving licence enquiry for
driving licence number (${drivingLicenceNumber}), 
national insurance number (${nationalInsuranceNumber}) 
and postcode (${postCode})...`);

    return new Promise((resolve, reject) => {
      logger.debug(`Contacting DVLA driving licence check service...`);

      // handle redirects manually
      request
        .post(
          {
            jar: jar,
            url: `${baseUrl}/driving-record/licence-number`,
            headers: { connection: 'keep-alive' },
            form: {
              applicantPassportNumber: null,
              pesel: '5236945509ed9cf9d7763fb124b7c0de', // 43c3fd2b9fefe31b237a8bf9f5d04f9d
              dln: drivingLicenceNumber,
              nino: nationalInsuranceNumber,
              postcode: postCode,
              dwpPermission: 1,
            },
            proxy: this.proxy,
          },
          (err, response) => {
            if (err) {
              if (err.code === 'ENOTFOUND') {
                const error: any = new Error('Dvla Service Unavailable.');
                error.statusCode = codes.NOT_FOUND;
                reject(error);
              }

              reject(err);
            }
            // process redirect
            const url = `${baseUrl}${response.headers.location}`;

            logger.debug(`Successfully contacted dvla.`);
            logger.debug(`Redirecting to viewbydln...`);

            request
              .get({
                url: url,
                jar: jar,
                headers: { connection: 'keep-alive' },
                proxy: this.proxy,
              })
              .catch((err) => {
                if (err.code === 'ENOTFOUND') {
                  const error: any = new Error('Dvla Service Unavailable.');
                  error.statusCode = codes.NOT_FOUND;
                  return reject(error);
                }
                return reject(err);
              })
              .then((body) => {
                if (!body) {
                  return;
                }

                logger.debug(`Successfully received driving licence check details`);
                logger.debug(`Processing details...`);

                const result = DvlaResultParser.parseDrivingLicenceEnquiry(body);

                logger.debug(`Successfully processed Driving licence check results.`, { results: result });

                resolve(result);
              });
          }
        )
        .catch((error) => {
          // Error is fine if it is a 303 redirect status code
          if (error.statusCode !== codes.SEE_OTHER) {
            logger.debug(error);
            reject(error);
          }
        });
    });
  };

  drivingLicenceCheck = (drivingLicenceNumber: string, checkCode: string): Promise<IDrivingLicenceEnquiry> => {
    const baseUrl = 'https://www.viewdrivingrecord.service.gov.uk';

    // cookie store to pass to redirect. This is needed for redirects
    const jar = request.jar();

    logger.debug(`
Processing driving licence check for
driving licence number (${drivingLicenceNumber}), 
check code (${checkCode})...`);

    return new Promise((resolve, reject) => {
      logger.debug(`Contacting DVLA driving licence check service...`);

      const docRef = drivingLicenceNumber.slice(0, 16).slice(-8);
      const accessToken = checkCode.replace(/\s+/g, '');

      // handle redirects manually
      request
        .post(
          {
            jar: jar,
            url: `${baseUrl}/driving-record/validate?submit`,
            headers: { connection: 'keep-alive' },
            form: {
              applicantPassportNumber: null,
              docRef: docRef,
              accessToken: accessToken,
            },
            proxy: this.proxy,
          },
          (err, response) => {
            if (err) {
              if (err.code === 'ENOTFOUND') {
                const error: any = new Error('Dvla Service Unavailable.');
                error.statusCode = codes.NOT_FOUND;
                return reject(error);
              }

              return reject(err);
            }
            // process redirect
            //let url = `${baseUrl}${response.headers.location}`;
            const url = `${baseUrl}/driving-record/validate/summary`;

            logger.debug(`Successfully contacted dvla.`);
            logger.debug(`Getting driving licence details...`);

            request.get(
              {
                url: url,
                jar: jar,
                headers: { connection: 'keep-alive' },
                proxy: this.proxy,
              },
              (err, response, body) => {
                if (err) {
                  if (err.code === 'ENOTFOUND') {
                    const error: any = new Error('Dvla Service Unavailable.');
                    error.statusCode = codes.NOT_FOUND;
                    return reject(error);
                  }
                  return reject(err);
                }

                logger.debug(`Successfully received driving licence check details`);
                logger.debug(`Processing details...`);

                let result = null;
                try {
                  result = DvlaResultParser.parseDrivingLicenceCheck(body);
                } catch (err) {
                  return reject(err);
                }

                logger.debug(`Successfully processed Driving licence check results.`, { results: result });

                return resolve(result);
              }
            );
          }
        )
        .catch((error) => {
          // Error is fine if it is a 303 redirect status code
          if (error.statusCode !== codes.SEE_OTHER) {
            logger.debug(error);
            return reject(error);
          }
        });
    });
  };
}
