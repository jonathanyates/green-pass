import mongoose from 'mongoose';
import {ICompany} from "../../../shared/interfaces/company.interface";
import {AddressSchema} from "../address/address.schema";
import {SubscriptionResourcesSchema, SubscriptionSchema} from "../subscriptions/subscription.schema";
import {defaultSettingsFactory} from "../../../shared/interfaces/company-settings.factory";

export const CompanySettingsSchema = new mongoose.Schema({
  vehicles: {
    type: {
      grey: {
        exclusions: {
          service: {type: Boolean, required: true, default: true},
          inspection: {type: Boolean, required: true, default: true},
          checks: {type: Boolean, required: true, default: true}
        }
      },
    }
  },
  drivers: {
    allowInput: {type: Boolean, required: true, default: true},
    references: {type: Boolean, required: true, default: false},
    reminders: {
      type: {
        sendEmails: {type: Boolean, required: true, default: false},
        cron: {type: String, required: true, default: null}
      },
      required: false
    },
    licenceChecks: {
      checkPeriods: [{
        noOfPoints: {type:Number, required: true},
        months: {type:Number, required: true}
      }]
    },
    includeFaw: {type: Boolean, required: false},
    includeDbs: {type: Boolean, required: false},
    includePNumber: {type: Boolean, required: false},
  }
});

export const CompanyAlertTypeSchema = new mongoose.Schema({
  alertType: {type: mongoose.Schema.Types.ObjectId, ref: 'AlertType'},
  schedule: {type: mongoose.Schema.Types.ObjectId, ref: 'ReportSchedule'},
});

export const CompanySubscriptionSchema = new mongoose.Schema({
  name: {type: String, required: true},
  startDate: {type: Date, required: true},
  renewalDate: {type: Date, required: true},
  active: {type: Boolean, default: true, required: true},
  trial: {
    type: {
      endDate: {type: Date, required: true},
      subscription: {type: SubscriptionSchema, required: true},
    },
    required: false
  },
  resources: {type: SubscriptionResourcesSchema, required: true},
});

//================================
// Company Schema
//================================
export const CompanySchema: mongoose.Schema = new mongoose.Schema({
  name: {type: String, unique: true, required: true},
  coreBusiness: {type: String, required: true},
  address: {type: AddressSchema, required: true},
  noOfEmployees: {type: Number, required: false},
  phone: {type: String, required: true},
  mobile: {type: String, required: false},
  email: {type: String, required: true},
  controllingMind: {
    name: {type: String, required: true},
    position: {type: String, required: true}
  },
  depots: [{
    _companyId: {type: mongoose.Schema.Types.ObjectId, ref: 'Company'},
    name: {type: String, required: true},
    address: {type: AddressSchema, required: true},
    phone: {type: String, required: true},
    mobile: {type: String, required: false},
    email: {type: String, required: false}
  }],
  suppliers: [{
    _companyId: {type: mongoose.Schema.Types.ObjectId, ref: 'Company'},
    name: {type: String, required: true},
    address: {type: AddressSchema, required: true},
    phone: {type: String, required: true},
    mobile: {type: String, required: false},
    email: {type: String, required: false}
  }],
  fleetInsuranceHistory: [{
    validFrom: {type: Date, required: true},
    validTo: {type: Date, required: true},
    filename: {type: String, required: false},
    fileType: {type: String, required: false}
  }],
  templates: [{type: String, required: false}],
  alertTypes: [CompanyAlertTypeSchema],
  settings: {
    type: CompanySettingsSchema,
    required: true,
    default: defaultSettingsFactory()
  },
  subscription: { type: CompanySubscriptionSchema, required: true },
  removed: {type: Boolean, default: false, required: false}
}, {timestamps: true});

export interface ICompanyModel extends mongoose.Document, ICompany {
  _id: any
}


