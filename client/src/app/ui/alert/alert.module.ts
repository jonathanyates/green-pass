import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CompanyAlertTypeListComponent} from "./company-alert-type-list/company-alert-type-list.component";
import {VehicleAlertListComponent} from "./vehicle-alert-list/vehicle-alert-list.component";
import {DriverAlertListComponent} from "./driver-alert-list/driver-alert-list.component";
import {AlertListComponent} from "./alert-list/alert-list.component";
import {AlertTypeInputComponent} from "./alert-type-input/alert-type-input.component";
import {AlertTypeListComponent} from "./alert-type-list/alert-type-list.component";
import {CompanyAlertListComponent} from "./company-alert-list/company-alert-list.component";
import {AlertRoutingModule} from "./alert-routing.module";
import {DriverModule} from "../driver/driver.module";
import {VehicleModule} from "../vehicle/vehicle.module";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    DriverModule,
    VehicleModule,
    AlertRoutingModule
  ],
  exports: [],
  declarations: [
    AlertListComponent,
    DriverAlertListComponent,
    VehicleAlertListComponent,
    CompanyAlertTypeListComponent,
    CompanyAlertListComponent,
    AlertTypeListComponent,
    AlertTypeInputComponent
  ],
  providers: [],
})

export class AlertModule {
}
