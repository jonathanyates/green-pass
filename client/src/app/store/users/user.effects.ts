import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./user.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {LoadUsersAction} from "./user.actions";
import {Paths} from "../../shared/constants";
import {Router} from "@angular/router";
import {LoadCompanyAction} from "../company/company.actions";
import {SelectUsers, SelectSelectedUser} from "./user.selectors";
import {SelectUsersSuccessAction} from "./user.actions";
import {SelectUserSuccessAction} from "./user.actions";
import {LoadUserAction} from "./user.actions";
import {BusyAction} from "../progress/progress.actions";
import {UserService} from "../../services/user.service";

@Injectable()
export class UsersEffects {

  constructor(
    private userService: UserService,
    private actions$:Actions,
    private router: Router,
    private store: Store<AppState>) { }

  @Effect() selectUsers$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_LIST)
    .map((action: actions.SelectUsersAction) => action.payload)
    .switchMap(payload => this.store.let(SelectUsers()).take(1)
      .map(users => users && users.every(user => user._companyId === payload)
        ? new SelectUsersSuccessAction(users)
        : new LoadUsersAction(payload))
    );

  @Effect() loadUsers$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .map((action: actions.LoadUsersAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.userService.getUsers(payload)
      .map(Users =>  new actions.LoadUsersSuccessAction(Users))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectUser$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_USER)
    .map((action: actions.SelectUserAction) => action.payload)
    .switchMap(payload => this.store.let(SelectSelectedUser()).take(1)
      .map(user => user && user._id === payload.userId
        ? new SelectUserSuccessAction(user)
        : new LoadUserAction(payload.companyId, payload.userId))
    );

  @Effect() loadUser$ = this.actions$
    .ofType(actions.ActionTypes.LOAD_USER)
    .map((action: actions.LoadUserAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.userService.getUser(payload.companyId, payload.userId)
      .map(Users =>  new actions.LoadUserSuccessAction(Users))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveUser$ = this.actions$
    .ofType(actions.ActionTypes.SAVE)
    .map((action: actions.SaveUserAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(user => this.userService.saveUser(user)
      .do(user => this.router.navigate([Paths.Company, user._companyId, Paths.Users, user._id]))
      .map(user => new actions.SaveUserSuccessAction(user))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveUserSuccess$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_SUCCESS)
    .map((action: actions.SaveUserSuccessAction) => action.payload)
    .do(user => this.store.dispatch(new LoadCompanyAction(user._companyId)))
    .map(user => new LoadUsersAction(user._companyId));

}
