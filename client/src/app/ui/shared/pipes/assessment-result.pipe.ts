import { Pipe, PipeTransform } from '@angular/core';
import {RiskLevel} from "../../../../../../shared/models/driver-compliance.model";

@Pipe({ name: 'assessmentResult' })
export class AssessmentResultPipe implements PipeTransform {

  transform(value: string, args: any[]): any {
    switch (value) {
      case RiskLevel.High : return 'danger-color';
      case RiskLevel.AboveAverage : return 'warning-color';
      case RiskLevel.Average : return 'warning-color';
      case RiskLevel.Low : return 'success-color';
      case RiskLevel.Unknown : return 'danger-color';
      default : return 'danger-color';
    }
  };


}
