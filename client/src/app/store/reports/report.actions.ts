import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {IReportType} from "../../../../../shared/interfaces/report.interface";

export const ActionTypes = {

  LOAD:                       type('[Report] Load'),
  LOAD_SUCCESS:               type('[Report] Load Success'),

  LOAD_REPORT_TYPES:           type('[Report Types] Load'),
  LOAD_REPORT_TYPES_SUCCESS:   type('[Report Types] Load Success'),
  SELECT_REPORT_TYPES:         type('[Report Types] Select'),
  SELECT_REPORT_TYPES_SUCCESS: type('[Report Types] Select Success'),
  SAVE_REPORT_TYPE:            type('[Report Type] Save'),
  SAVE_REPORT_TYPE_SUCCESS:    type('[Report Type] Save Success')
};

/**
 * ReportType Actions
 */

export class LoadReportAction implements Action {
  type = ActionTypes.LOAD;
  payload: {companyId: string, reportType: IReportType};
  constructor(companyId: string, reportType: IReportType) {
    this.payload = { companyId: companyId, reportType: reportType }
  }
}

export class LoadReportSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: any) { }
}

/**
 * ReportType Actions
 */
export class SelectReportTypesAction implements Action {
  type = ActionTypes.SELECT_REPORT_TYPES;
  constructor() { }
}

export class SelectReportTypesSuccessAction implements Action {
  type = ActionTypes.SELECT_REPORT_TYPES_SUCCESS;
  constructor(public payload: IReportType[]) { }
}

export class LoadReportTypesAction implements Action {
  type = ActionTypes.LOAD_REPORT_TYPES;
  constructor() { }
}

export class LoadReportTypesSuccessAction implements Action {
  type = ActionTypes.LOAD_REPORT_TYPES_SUCCESS;
  constructor(public payload: IReportType[]) { }
}

export class SaveReportTypeAction implements Action {
  type = ActionTypes.SAVE_REPORT_TYPE;
  constructor(public payload: IReportType) { }
}

export class SaveReportTypeSuccessAction implements Action {
  type = ActionTypes.SAVE_REPORT_TYPE_SUCCESS;
  constructor(public payload: IReportType) { }
}

export type Actions
  = LoadReportTypesSuccessAction
  | SelectReportTypesSuccessAction
  | SaveReportTypeSuccessAction
  | LogoutAction;
