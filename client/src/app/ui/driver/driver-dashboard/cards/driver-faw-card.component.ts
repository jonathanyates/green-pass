import {Component, Input} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-faw-card',
  templateUrl: 'driver-faw-card.component.html',
  host: {'class': 'card'}
})
export class DriverFawCardComponent {

  hasFaw: boolean;
  private _driver: IDriver;

  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasFaw = driver.compliance.faw;
    }
  }

  constructor(private router: Router) {}

  viewFaw() {
    // navigate to add vehicle if the driver has no vehicles to view
    if (this._driver && (!this._driver.fawHistory || this._driver.fawHistory.length === 0)) {
      return this.router.navigate([Paths.Driver, Paths.Faw, Paths.New]);
    }
    return this.router.navigate([Paths.Driver, Paths.Faw]);
  }
}
