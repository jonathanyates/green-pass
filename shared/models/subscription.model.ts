import { isDate } from 'date-fns';
import { ICompanySubscription, ISubscription } from '../interfaces/subscription.interface';
import { ICompany } from '../interfaces/company.interface';

export class SubscriptionHelper {
  static isTrial(subscription: ICompanySubscription): boolean {
    if (subscription.trial && isDate(subscription.trial.endDate)) {
      let now = new Date();
      let remaining = now.getDayDiff(subscription.trial.endDate);
      return remaining > 0;
    }

    return false;
  }

  static getSubscription(company: ICompany): ISubscription | undefined | null {
    if (!company || !company.subscription || !company.subscription.active) {
      return null;
    }

    if (SubscriptionHelper.isTrial(company.subscription)) {
      return company && company.subscription && company.subscription.trial && company.subscription.trial.subscription;
    }

    return company.subscription;
  }
}
