export const alerts = [{
  "_companyId": "012345678901234567891000",
  "alertType": {
    "_id": "59413b7e65dd129c312e0445",
    "reportType": {
      "_id": "59413b7465dd129c312e0443",
      "name": "None Compliant Vehicles",
      "query": "getNoneCompliantVehicles",
      "category": "Vehicle"
    },
    "priority": "High"
  },
  "created": new Date("2017-06-14T14:30:55.251Z"),
  "status": "Active",
  "driver": null,
  "vehicle": {
    "_id": "59413b439c5335e84e9a8634",
    "compliance": {
      "compliant": false,
      "tax": true,
      "mot": true,
      "service": true,
      "inspection": true,
      "vehicleCheck": false,
      "_id": "59413b439c5335e84e9a864f"
    },
    "updatedAt": new Date("2017-06-14T13:33:55.234Z"),
    "createdAt": new Date("2017-06-14T13:33:55.141Z"),
    "_companyId": "012345678901234567891000",
    "make": "FORD",
    "model": "Unknown",
    "registrationNumber": "CX65 PUE",
    "annualMileage": 12000,
    "inspectionIntervalType": "A",
    "taxDueDate": new Date("2018-03-01T00:00:00.000Z"),
    "motDueDate": new Date("2018-08-31T23:00:00.000Z"),
    "dateOfFirstRegistration": new Date("2015-08-31T23:00:00.000Z"),
    "yearOfManufacture": 2015,
    "cylinderCapacityCc": "2198 cc",
    "co2Emissions": "212 g/km",
    "fuelType": "DIESEL",
    "exportMarker": "No",
    "vehicleStatus": "Tax not due",
    "vehicleColour": "WHITE",
    "vehicleTypeApproval": "N1",
    "wheelplan": "2 AXLE RIGID BODY",
    "revenueWeight": "3500kg",
    "__v": 3,
    "nextService": new Date("2018-04-07T23:00:00.000Z"),
    "nextInspection": new Date("2017-07-22T23:00:00.000Z"),
    "nextVehicleCheck": new Date("2017-06-08T23:00:00.000Z"),
    "checkHistory": [{
      "checkDate": new Date("2017-06-02T13:33:55.222Z"),
      "checkedBy": "A Driver",
      "filename": null,
      "_id": "59413b439c5335e84e9a864e"
    }],
    "inspectionHistory": [{
      "inspectionDate": new Date("2017-05-14T13:33:55.187Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a8644"
    }],
    "serviceHistory": [{
      "serviceDate": new Date("2017-04-08T13:33:55.154Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a863c"
    }],
    "motHistory": [],
    "type": "Grey",
    "category": "B"
  }
}, {
  "_companyId": "012345678901234567891000",
  "alertType": {
    "_id": "59413b7e65dd129c312e0445",
    "reportType": {
      "_id": "59413b7465dd129c312e0443",
      "name": "None Compliant Vehicles",
      "query": "getNoneCompliantVehicles",
      "category": "Vehicle"
    },
    "priority": "High"
  },
  "created": new Date("2017-06-14T14:30:55.251Z"),
  "status": "Active",
  "driver": null,
  "vehicle": {
    "_id": "59413b439c5335e84e9a865e",
    "compliance": {
      "compliant": false,
      "tax": true,
      "mot": true,
      "service": false,
      "inspection": true,
      "vehicleCheck": true,
      "_id": "59413b439c5335e84e9a8680"
    },
    "updatedAt": new Date("2017-06-14T13:33:55.970Z"),
    "createdAt": new Date("2017-06-14T13:33:55.910Z"),
    "_companyId": "012345678901234567891000",
    "make": "FORD",
    "model": "TRANSIT 125 T280 TREND FWD",
    "registrationNumber": "WV13 UXU",
    "annualMileage": 26367,
    "inspectionIntervalType": "A",
    "taxDueDate": new Date("2017-10-01T00:00:00.000Z"),
    "motDueDate": new Date("2017-10-17T00:00:00.000Z"),
    "dateOfFirstRegistration": new Date("2013-03-21T00:00:00.000Z"),
    "yearOfManufacture": 2013,
    "cylinderCapacityCc": "2198 cc",
    "co2Emissions": "189 g/km",
    "fuelType": "DIESEL",
    "exportMarker": "No",
    "vehicleStatus": "Tax not due",
    "vehicleColour": "SILVER",
    "vehicleTypeApproval": "N1",
    "wheelplan": "2 AXLE RIGID BODY",
    "revenueWeight": "2825kg",
    "__v": 3,
    "nextService": new Date("2016-10-07T23:00:00.000Z"),
    "nextInspection": new Date("2017-08-05T23:00:00.000Z"),
    "nextVehicleCheck": new Date("2017-06-15T23:00:00.000Z"),
    "checkHistory": [{
      "checkDate": new Date("2017-06-09T13:33:55.960Z"),
      "checkedBy": "A Driver",
      "filename": null,
      "_id": "59413b439c5335e84e9a867f"
    }],
    "inspectionHistory": [{
      "inspectionDate": new Date("2017-06-04T13:33:55.936Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a8674"
    }],
    "serviceHistory": [{
      "serviceDate": new Date("2016-04-30T13:33:55.923Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a866b"
    }],
    "motHistory": [{
      "testDate": new Date("2016-10-18T00:00:00.000Z"),
      "expiryDate": new Date("2017-10-17T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": "108726",
      "motTestNumber": "9766 7300 0123",
      "_id": "59413b439c5335e84e9a8662",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2016-10-17T00:00:00.000Z"),
      "testResult": "Fail",
      "odometerReading": "108722",
      "motTestNumber": "5394 4656 1269",
      "_id": "59413b439c5335e84e9a8661",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2016-03-18T00:00:00.000Z"),
      "expiryDate": new Date("2017-03-20T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": "93345",
      "motTestNumber": "7182 8451 8028",
      "_id": "59413b439c5335e84e9a8660",
      "advisoryNotices": ["Offside Front Tyre worn close to the legal limit (4.1.E.1)"]
    }, {
      "testDate": new Date("2016-03-09T00:00:00.000Z"),
      "testResult": "Fail",
      "odometerReading": "93208",
      "motTestNumber": "1575 3406 5538",
      "_id": "59413b439c5335e84e9a865f",
      "advisoryNotices": ["Offside Front Tyre worn close to the legal limit (4.1.E.1)"]
    }],
    "type": "Grey",
    "category": "B"
  }
}];
