import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-licence-card',
  templateUrl: 'driver-licence-card.component.html',
  host: {'class': 'card'}
})
export class DriverLicenceCardComponent {

  constructor(private router: Router) {}

  licence() {
    this.router.navigate([Paths.Driver, Paths.Licence]);
  }

}
