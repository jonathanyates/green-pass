import * as actions from "./compliance.actions";
import {ComplianceState} from "./compliance.state";
import {ActionTypes} from "../authentication/authentication.actions";

export const initialState: ComplianceState = {
  list: [],
  selected: null
};

export function ComplianceReducer(state: ComplianceState = initialState, action: actions.Actions): ComplianceState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_SUCCESS:
        return Object.assign({}, state, {
          list: action.payload,
          selected: null
        });

      case actions.ActionTypes.SELECT_SUCCESS:
        return Object.assign({}, state, {
          list: state.list,
          selected: action.payload
        });

      case actions.ActionTypes.SELECT:
        return {
          ...state,
          selected: null
        };

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }

};
