import {NgModule} from '@angular/core';
import {Roles} from "../../../../../shared/constants";
import {Routes, RouterModule} from '@angular/router';
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {CompanyComponent} from "../company/company.component";
import {VehicleListResolve} from "./vehicle-list/vehicle-list.resolve";
import {VehicleResolve} from "./vehicle.resolve";
import {VehicleDetailsTabListComponent} from "./vehicle-details-tab-list/vehicle-details-tab-list.component";
import {VehicleServiceInputComponent} from "./vehicle-service/vehicle-service-input/vehicle-service-input.component";
import {VehicleServiceResolve} from "./vehicle-service/vehicle-service.resolve";
import {VehicleServiceComponent} from "./vehicle-service/vehicle-service-detail/vehicle-service.component";
import {VehicleInspectionInputComponent} from "./vehicle-inspection/vehicle-inspection-input/vehicle-inspection-input.component";
import {VehicleInspectionComponent} from "./vehicle-inspection/vehicle-inspection-detail/vehicle-service.component";
import {VehicleInspectionResolve} from "./vehicle-inspection/vehicle-inspection.resolve";
import {VehicleCheckResolve} from "./vehicle-check/vehicle-check.resolve";
import {VehicleCheckInputComponent} from "./vehicle-check/vehicle-check-input/vehicle-check-input.component";
import {VehicleCheckComponent} from "./vehicle-check/vehicle-check-detail/vehicle-check.component";
import {VehicleListContainerComponent} from "./vehicle-list/vehicle-list-container.component";
import {VehicleDriverSelectComponent} from "./vehicle-driver/vehicle-driver-select.component";
import {VehicleSearchInputComponent} from "./vehicle-input/vehicle-search-input.component";
import {CompanyResolve} from "../company/company.resolve";
import {VehiclesComplianceComponent} from "../company/compliance/vehicles-compliance.component";

const routes: Routes = [
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/vehicles', component: VehicleListContainerComponent, resolve: { vehicles: VehicleListResolve } },
      { path: ':id/vehicles/new', component: VehicleSearchInputComponent, resolve: { vehicle: VehicleResolve } },
      { path: ':id/vehicles/compliance', component: VehiclesComplianceComponent, resolve: { company: CompanyResolve } },
      { path: ':id/vehicles/:vehicleId', component: VehicleDetailsTabListComponent, resolve: { vehicle: VehicleResolve } },
      { path: ':id/vehicles/:vehicleId/edit', component: VehicleSearchInputComponent, resolve: { vehicle: VehicleResolve } },

      { path: ':id/vehicles/:vehicleId/services/new', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/vehicles/:vehicleId/services/:serviceId', component: VehicleServiceComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/vehicles/:vehicleId/services/:serviceId/edit', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/vehicles/:vehicleId/services', redirectTo: ':id/vehicles/:vehicleId' },

      { path: ':id/vehicles/:vehicleId/inspections/new', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/vehicles/:vehicleId/inspections/:inspectionId', component: VehicleInspectionComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/vehicles/:vehicleId/inspections/:inspectionId/edit', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/vehicles/:vehicleId/inspections', redirectTo: ':id/vehicles/:vehicleId' },

      { path: ':id/vehicles/:vehicleId/checks/new', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/vehicles/:vehicleId/checks/:checkId', component: VehicleCheckComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/vehicles/:vehicleId/checks/:checkId/edit', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/vehicles/:vehicleId/checks', redirectTo: ':id/vehicles/:vehicleId' },

      { path: ':id/vehicles/:vehicleId/drivers/add', component: VehicleDriverSelectComponent, resolve: { vehicle: VehicleResolve } },
      { path: ':id/vehicles/:vehicleId/drivers', redirectTo: ':id/vehicles/:vehicleId' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    VehicleListResolve,
    VehicleResolve,
    VehicleServiceResolve,
    VehicleInspectionResolve,
    VehicleCheckResolve
  ]
})
export class VehicleRoutingModule {
}
