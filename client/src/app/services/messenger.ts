import {Injectable} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";

@Injectable()
export class Messenger {

  private subjects = {};

  public toObservable(key:any):Observable<any> {
    if (!this.subjects[key]) {
      this.subjects[key] = new Subject();
    }
    return this.subjects[key].asObservable();
  }

  public publish(key:any, value:any) {
    if (this.subjects[key]) {
      this.subjects[key].next(value);
    }
  }
}

export const messenger = new Messenger();

/* usage
 var messenger = new Messenger();

 messenger.toObservable("foo").subscribe(x => {
 console.log("subscribed to " + x);
 });

 messenger.publish("foo", { bar : 'bar'});
 */
