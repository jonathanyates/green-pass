import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IRouter } from 'express-serve-static-core';
import { ITemplateController } from './template.controller';

export interface ITemplateRouter extends IApiRouter {
  companyRoutes: IRouter;
}

@injectable()
export class TemplateRouter implements ITemplateRouter {
  companyRoutes = express.Router({ mergeParams: true });
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.TemplateController) private templateController: ITemplateController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.templateController.getTemplates
      )
      .get(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.templateController.getTemplate
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.addTemplate
      )
      .put(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.updateTemplate
      )
      .delete(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.deleteTemplate
      );

    this.companyRoutes.use(this.authorizationMiddleware.user.middleware());

    this.companyRoutes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.templateController.getCompanyTemplates
      )
      .get(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.templateController.getTemplate
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.addCompanyTemplate
      )
      .put(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.updateTemplate
      )
      .delete(
        '/:templateId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.templateController.deleteTemplate
      )
      .put(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.templateController.saveCompanyTemplates
      );
  }
}
