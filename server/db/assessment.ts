import {IAssessment, IOnRoadAssessment, ITrainingModule} from "../../shared/interfaces/assessment.interface";

const onLine = {
    "overallRiskLevel": "Average",
    "inviteDate": new Date("2017-05-08T20:22:45.000Z"),
    "tests": [
      {
        "testCode": "THEORY_TEST_1_0",
        "description": "Theory Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      },
      {
        "description": "Observation Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      },
      {
        "description": "Hazard Perception Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      },
      {
        "description": "Distance Following Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      },
      {
        "description": "Reaction Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      },
      {
        "description": "Psychometric Test Version 1.0",
        "score": 0,
        "riskLevel": "Average",
        "startDate": new Date("2017-05-08T20:33:27.000Z"),
        "endDate": new Date("2017-05-08T20:38:29.000Z"),
        "complete": true,
      }
      ]
  };
const trainingModules = [
  <ITrainingModule>{
    "createdDate": new Date(),
    "trainingProvider": null,
    "trainingRecommendationType": "E-learning",
    "subTrainingType": "Theory module",
    "arrangedDate": null,
    "arrangedBy": null,
    "contactDate": null,
    "startDate": new Date(),
    "completedDate": new Date(),
    "details": null,
    "reasonDetails": "Manual Invite",
    "satisfactory": null,
    "testScore": 80,
    "riskLevel": "Average"
  },
  <ITrainingModule>{
    "createdDate": new Date(),
    "trainingProvider": null,
    "trainingRecommendationType": "E-learning",
    "subTrainingType": "Reducing stress module",
    "arrangedDate": null,
    "arrangedBy": null,
    "contactDate": null,
    "startDate": new Date(),
    "completedDate": new Date(),
    "details": null,
    "reasonDetails": "Manual Invite",
    "satisfactory": null,
    "testScore": 80,
    "riskLevel": "Average"
  }
];
const onRoad = <IOnRoadAssessment>{
  inviteDate: new Date(),
  // assessmentDate: new Date(),
  // assessedBy: 'Assessor',
  // complete: true
};

export const assessments = [
  <IAssessment>{
    refId: "YATES607198JP9LK",
    onLine: onLine,
    trainingModules: trainingModules,
    onRoad: onRoad
  }
];


