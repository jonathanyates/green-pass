import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IContact} from "../../../../../../shared/interfaces/driver.interface";

@Component({
  selector: 'gp-driver-next-of-kin',
  template: `
    <gp-contact-details [contact]="nextOfKin" (edit)="editNextOfKin()"
                        (add)="editNextOfKin()" [allowEdit]="allowInput"></gp-contact-details>
  `
})

export class DriverNextOfKinComponent {

  @Input() allowInput: boolean = false;
  @Input() nextOfKin: IContact;
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();

  editNextOfKin() {
    this.edit.emit();
  }
}
