import cheerio = require('cheerio');
import _ = require('lodash');
import { UtcDate } from '../../../../shared/extensions/date.extensions';
import { IDrivingLicenceEnquiry } from '../interfaces/driving-licence-enquiry.interface';
import { IAllowedVehicle, ICpc, ICpcCategory } from '../../../../shared/interfaces/driver.interface';
import { ServerError } from '../../shared/errors';
import codes from '../../shared/error.codes';

export class DvlaResultParser {
  public static getViewState(body) {
    const $ = cheerio.load(body);
    return $('#viewstate').val();
  }

  public static parseDrivingLicenceEnquiryCheckCode = (body: string): string => {
    const $ = cheerio.load(body);
    const checkCode = $('.grid-wrapper table:first-of-type tr:first-child td:first-child').text().replace(/"/g, '');
    return checkCode;
  };

  public static parseDrivingLicenceEnquiry = (body: string): IDrivingLicenceEnquiry => {
    const $ = cheerio.load(body);

    const dateOfBirth = $('.dob-field').text();

    const result = <IDrivingLicenceEnquiry>{
      driver: {
        name: $('.name').text(),
        dateOfBirth: UtcDate.toUTC(dateOfBirth),
        gender: $('.gender-field').text(),
        address: $('#your-details .address-field').text().trim(),
      },
      licence: {
        status: $('.licence-status-field').text(),
        validFrom: UtcDate.toUTC($('.licence-valid-from-field').text()),
        validTo: UtcDate.toUTC($('.licence-valid-to-field').text()),
        licenceNumber: $('.dln-field').text(),
        issueNumber: +$('.issue-number-field').text(),
      },
      allowedVehicles: {
        vehicles: DvlaResultParser.getAllowedVehicles('#vehicles>#fullEntitlements>li', $),
        provisional: DvlaResultParser.getAllowedVehicles('#vehicles>#provEntitlements>li', $),
      },
      endorsements: [],
    };

    $('#penaltyPoints>li').each((i, el) => {
      const regEx = /[^:]*$/;
      const points = $(el).find('.points').text().match(regEx);
      const description = $(el).find('.offence-details>ul>li:nth-child(1)').text().match(regEx);
      const courtDetails = $(el).find('.offence-court>p').text().split(':');

      const penalty = {
        category: $(el).find('.category').text(),
        points: points.length > 0 ? +points[0] : 0,
        description: description.length > 0 ? description[0] : null,
        courtDetails: {
          code: courtDetails.length > 0 ? courtDetails[0] : null,
          description: courtDetails.length > 1 ? courtDetails[1] : null,
        },
        offenceDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(1)').text()),
        expiryDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(2)').text()),
        removalDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(3)').text()),
      };

      result.endorsements.push(penalty);
    });

    const tachographCard = DvlaResultParser.getTachographCard($);

    if (tachographCard) {
      result.tachographCard = tachographCard;
    }

    const cpc = DvlaResultParser.getCpc($);

    if (cpc) {
      result.cpc = cpc;
    }

    return result;
  };

  private static getTachographCard($: cheerio.Root) {
    const tachographCardElement = $('#tacho');

    if (tachographCardElement) {
      return {
        status: tachographCardElement.find('dl>dd:nth-child(2)').text(),
        validFrom: UtcDate.toUTC(tachographCardElement.find('dl>dd:nth-child(4)').text()),
        validTo: UtcDate.toUTC(tachographCardElement.find('dl>dd:nth-child(6)').text()),
        number: tachographCardElement.find('dl>dd:nth-child(8)').text(),
      };
    }

    return null;
  }

  private static getCpc($: cheerio.Root) {
    const cpcElement = $('#cpcs>li');

    if (cpcElement) {
      const cpc = <ICpc>{
        categories: [],
      };

      cpcElement.each((i, el) => {
        const category = <ICpcCategory>{
          category: $(el).find('.detail strong').text().split(':').slice(-1)[0].trim(),
          endDate: UtcDate.toUTC($(el).find('.valid-dates').text().split(':').slice(-1)[0].trim()),
          description: $(el).find('.accordion-content p').text(),
        };

        cpc.categories.push(category);
      });

      return cpc;
    }

    return null;
  }

  public static parseDrivingLicenceCheck = (body: string): IDrivingLicenceEnquiry => {
    const $ = cheerio.load(body);

    const validationSummary = $('.validation-summary>p').text();

    if (validationSummary) {
      throw ServerError(validationSummary, codes.BAD_REQUEST);
    }

    const result = <IDrivingLicenceEnquiry>{
      driver: {
        name: $('.licence-holders-name').text(),
      },
      licence: {
        status: $('.licence-status').text(),
        validFrom: UtcDate.toUTC($('.licence-info .alert-heading-small:nth-child(6)').text()),
        validTo: UtcDate.toUTC($('.licence-info .alert-heading-small:nth-child(8)').text()),
        issueNumber: +$('.licence-info .alert-heading-small:nth-child(4)').text(),
      },
      allowedVehicles: {
        vehicles: DvlaResultParser.getAllowedVehicles('#vehicles>#fullEntitlements>li', $),
        provisional: DvlaResultParser.getAllowedVehicles('#vehicles>#provEntitlements>li', $),
      },
      endorsements: [],
      points: +$('.color-alerts .grid-1-2-inner .grid-1-2-inner:nth-child(2) .alert-heading-small').text(),
    };

    $('#penaltyPoints>li').each((i, el) => {
      const regEx = /[^:]*$/;
      const points = $(el).find('.points').text().match(regEx);
      const description = $(el).find('.offence-details>ul>li:nth-child(1)').text().match(regEx);
      const courtDetails = $(el).find('.offence-court>p').text().split(':');

      const penalty = {
        category: $(el).find('.category').text(),
        points: points.length > 0 ? +points[0] : 0,
        description: description.length > 0 ? description[0] : null,
        courtDetails: {
          code: courtDetails.length > 0 ? courtDetails[0] : null,
          description: courtDetails.length > 1 ? courtDetails[1] : null,
        },
        offenceDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(1)').text()),
        expiryDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(2)').text()),
        removalDate: UtcDate.toUTC($(el).find('.offence-dates>ul>li:nth-child(3)').text()),
      };

      result.endorsements.push(penalty);
    });

    const tachographCard = DvlaResultParser.getTachographCard($);

    if (tachographCard && tachographCard.number) {
      result.tachographCard = tachographCard;
    }

    const cpc = DvlaResultParser.getCpc($);

    if (cpc && cpc.categories.length > 0) {
      result.cpc = cpc;
    }

    return result;
  };

  private static getAllowedVehicles = (selector: string, $: cheerio.Root): IAllowedVehicle[] => {
    const vehicles = [];

    $(selector).each((i, el) => {
      const vehicle = {
        category: $(el).find('.category').text().replace('Category : ', ''),
        startDate: UtcDate.toUTC($(el).find('.valid-dates:nth-child(2)').text()),
        endDate: UtcDate.toUTC($(el).find('.valid-dates:nth-child(3)').text()),
        description: $(el).find('.description>p').text(),
        restrictions: [],
      };

      $(el)
        .find('.restriction>ol>li')
        .each((i2, el2) => {
          const code = $(el2).children('strong').text();
          const description = $(el2).text().replace(code, '').trim();
          vehicle.restrictions.push({
            code: code,
            description: description,
          });
        });

      vehicles.push(vehicle);
    });

    return vehicles;
  };

  private static isDate = (date) => {
    return date && date.toString() !== 'Invalid Date';
  };
}
