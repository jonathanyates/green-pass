import * as actions from "./address.actions";
import {ActionTypes} from "../authentication/authentication.actions";
import {AddressState} from "./address.state";

const initialState: AddressState = {
  list: {
    postcode: null,
    addresses: []
  },
  selected: null
};

export function AddressReducer(state: AddressState = initialState, action: actions.Actions): AddressState {
    switch (action.type) {
      case actions.ActionTypes.LOAD_SUCCESS:
        return {
          list: action.payload,
          selected: null
        };

      case actions.ActionTypes.SELECT:
        return {
          list: state.list,
          selected: action.payload
        };

      case actions.ActionTypes.CLEAR:
      case ActionTypes.LOGOUT:
        return null;

      default:
        return state;
    }
  };
