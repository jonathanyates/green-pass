import mongoose from 'mongoose';
import { ITemplate } from '../../../shared/interfaces/document.interface';

//================================
// Template Schema
//================================
export const TemplateSchema = new mongoose.Schema(
  {
    _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
    name: { type: String, unique: true, required: true },
    folderName: { type: String, required: true },
    html: { type: String, required: true },
  },
  { timestamps: true }
);

export interface ITemplateModel extends mongoose.Document, ITemplate {
  _id: any;
}
