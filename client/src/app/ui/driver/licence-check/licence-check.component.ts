import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ActionTypes, DrivingLicenceCheckAction} from "../../../store/drivers/driver.actions";
import {Driver} from "../driver.model";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {Subscription} from "rxjs/Subscription";
import {ServerError} from "../../../shared/errors/server.error";
import HTTP_STATUS_CODES from "../../../shared/errors/status-codes.enum";
import {Observable} from "rxjs/Observable";
import {messenger} from "../../../services/messenger";
declare let $: any;

@Component({
  selector: 'gp-licence-check',
  templateUrl: 'licence-check.component.html'
})
export class LicenceCheckComponent implements OnInit {

  url: string;
  driver: Driver;
  checkCode: string;
  licenceNumber: string;
  niNumber: string;
  postcode: string;
  error: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.url = 'https://www.viewdrivingrecord.service.gov.uk/driving-record/licence-number';
    this.subscriptions.push(
      this.store.let(SelectDriver())
      .filter(driver => driver !== null)
      .subscribe(driver => {
        this.driver = driver;
        this.licenceNumber = driver.licenceNumber;
        this.niNumber = driver.niNumber;
        this.postcode = driver.address.postcode;
      }));

    messenger.toObservable(ActionTypes.LICENCE_CHECK_FAILURE)
      .subscribe((error:ServerError) => {
        if (error.code === HTTP_STATUS_CODES.BAD_REQUEST && error.message.startsWith('Authentication was unsuccessful')) {
          this.error = 'Invalid Check Code or Driving Licence Number. Please try enter a valid Check Code.'
        } else {
          this.error = error.message;
        }
      });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onFrameLoad(frame) {
    Observable.timer(100).subscribe(_ => {
      window.scrollTo(0,0)
    });
  }

  copy(ele) {
    this.selectText(ele);
  }

  selectText(element) {
    let range = document.createRange();
    range.selectNodeContents(element);
    let selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    selection.removeAllRanges();
  }

  licenceCheck() {
    this.error = null;
    this.store.dispatch(new DrivingLicenceCheckAction(this.driver, this.checkCode))
  }

}
