import logger = require('winston');
import mongoose from 'mongoose';
import { IRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { IMessageService } from '../messaging/message.service';
import { ISubscription } from '../../../shared/interfaces/subscription.interface';
import { ISubscriptionModel } from './subscription.schema';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class SubscriptionRepository implements IRepository<ISubscription | ISubscriptionModel> {
  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  getAll(): Promise<ISubscriptionModel[]> {
    return <Promise<ISubscriptionModel[]>>this.dbContext.subscription.find().lean().exec();
  }

  get(id: string): Promise<ISubscription> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid subscription id.', codes.BAD_REQUEST));
    }

    return <Promise<ISubscription>>this.dbContext.subscription.findById(id).lean().exec();
  }

  find(criteria: any): Promise<ISubscription[]> {
    criteria.removed = false;
    return <Promise<ISubscription[]>>this.dbContext.subscription.find(criteria).lean().exec();
  }

  findOne(criteria: any): Promise<ISubscription> {
    criteria.removed = false;
    return <Promise<ISubscription>>this.dbContext.subscription.findOne(criteria).lean().exec();
  }

  add<T extends ISubscription>(subscription: ISubscription): Promise<ISubscriptionModel> {
    const subscriptionModel = new this.dbContext.subscription(subscription);
    return subscriptionModel.save().then((subscription) => {
      logger.info(`Subscription ${subscription._id} added.`);
      return subscription;
    });
  }

  update<T extends ISubscription>(subscription: ISubscription): Promise<ISubscriptionModel> {
    return this.dbContext.subscription
      .findByIdAndUpdate(subscription._id, subscription, { new: true })
      .exec()
      .then((subscription) => {
        logger.info(`Subscription ${subscription._id} updated.`);
        return subscription;
      });
  }

  remove<T extends ISubscription>(subscriptionId: string): Promise<ISubscriptionModel> {
    return this.dbContext.subscription
      .findByIdAndUpdate(subscriptionId, { new: true })
      .exec()
      .then((subscription) => {
        logger.info(`Subscription ${subscriptionId} removed.`);
        return subscription;
      });
  }
}
