import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IComplianceCardModel} from "./compliance-card.component";

@Component({
  selector: 'gp-compliance-cards',
  template: `
    <div *ngIf="model" class="row">
      <ng-container *ngFor="let item of model">
        <ng-container *ngIf="item && item.percentage != null">
          <gp-compliance-card [model]="item"></gp-compliance-card>
        </ng-container>
      </ng-container>
    </div>
    <button class="btn btn-primary waves-effect" (click)="onBack()"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Back</button>
  `

})

export class ComplianceCardsComponent implements OnInit {

  @Input() model: IComplianceCardModel[];
  @Output() back: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  onBack() {
    this.back.emit();
  }
}
