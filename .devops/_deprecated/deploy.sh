#!/bin/bash

# kubectl
# curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

#helm
# curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
# chmod 700 get_helm.sh
# ./get_helm.sh

# ingress-nginx
helm upgrade --install ingress-nginx ingress-nginx \
--repo https://kubernetes.github.io/ingress-nginx \
--namespace ingress-nginx --create-namespace

# cert-manager
helm install \
cert-manager jetstack/cert-manager \
--namespace cert-manager \
--create-namespace \
--version v1.11.0 \
--set installCRDs=true

# rabbitmq
helm upgrade --install rabbitmq rabbitmq \
--repo https://charts.bitnami.com/bitnami \
--namespace rabbitmq --create-namespace \
--set auth.username=admin,auth.password=Everest99

# docker hub authentication
kubectl create secret docker-registry regcred \
--docker-server=https://index.docker.io/v1/ \
--docker-username=greenpass \
--docker-password=Everest99 \
--docker-email=jonathan.yates@greenpasscompliance.co.uk

# deploy gp-chart
helm install gp-chart-local gp-chart/ --values gp-chart/values/values.local.yaml
# helm install gp-chart-dev gp-chart/ --values gp-chart/values/values.dev.yaml
# helm install gp-chart-demo gp-chart/ --values gp-chart/values/values.demo.yaml
# helm install gp-chart-prod gp-chart/ --values gp-chart/values/values.prod.yaml
