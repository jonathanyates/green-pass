import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./companies.actions";
import {CompaniesService} from "../../services/companies.service";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {BusyAction} from "../progress/progress.actions";

@Injectable()
export class CompaniesEffects {

  constructor(
    private companiesService:CompaniesService,
    private actions$:Actions,
    private store: Store<AppState>) {
  }

  @Effect() loadCompanies$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.companiesService.getCompanies()
      .map(companies =>  new actions.LoadCompaniesSuccessAction(companies))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );
}
