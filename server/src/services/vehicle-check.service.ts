import logger = require('winston');
import container from '../container/container.config';
import { TYPES } from '../container/container.types';
import schedule = require('node-schedule');
import { configureLogger } from './logger';
import { IVehicleApiService } from '../vehicles/vehicle.service';
import { IVehicle } from '../../../shared/interfaces/vehicle.interface';

// perform checks for vehicles where the tax or mot date is due within the next 7 days.

configureLogger('vehicle_checks', 'GreenPass-Vehicle-Checks-Service');

logger.info('Starting Vehicle checks service...');

const vehicleService = container.get<IVehicleApiService>(TYPES.services.VehicleService);

const cron = process.env.CRON ? process.env.CRON : '0 2 * * *'; // default run at 2am

logger.info('Scheduling Vehicle checks to run at Cron interval ' + cron);

schedule.scheduleJob(cron, () => {
  logger.info('Performing vehicle checks...');

  vehicleService
    .vehicleChecks()
    .then((vehicles: IVehicle[]) => {
      logger.info('Successfully performed Dvla vehicle checks.');
      if (vehicles && vehicles.length > 0) {
        logger.info(`Vehicles checked: ${vehicles.join(', ')}`);
      } else {
        logger.info('No vehicles required a Dvla vehicle check.');
      }
    })
    .catch((error) => {
      logger.error('Error performing vehicle checks.');
      logger.error(error.message);
    });
});

logger.info('Vehicle Checks Service is running.');

/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */
