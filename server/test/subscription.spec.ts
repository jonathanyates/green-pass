import '../../shared/extensions/date.extensions';
import { ICompanySubscription, subscriptions } from '../../shared/interfaces/subscription.interface';
import { SubscriptionHelper } from '../../shared/models/subscription.model';

describe('Subscription', function () {
  describe('isTrial', function () {
    it('should be a trial', function () {
      const companySubscription: ICompanySubscription = Object.assign({}, subscriptions[0], {
        startDate: new Date(),
        renewalDate: new Date().addMonths(12),
        active: true,
        trial: {
          endDate: new Date().addMonths(1),
          subscription: subscriptions[1],
        },
      });

      const isTrial = SubscriptionHelper.isTrial(companySubscription);

      expect(isTrial).toBeTruthy();
    });
  });
});
