import {Component, Input, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {FormatDate} from "../../../../../../shared/utils/helpers";
import {ICpc, ITachographCard} from "../../../../../../shared/interfaces/driver.interface";

@Component({
  selector: 'gp-tacho-cpc',
  templateUrl: './driver-tacho-cpc.html',
  styleUrls: ['./driver-tacho-cpc.css']
})
export class TachoCpcComponent implements OnInit {

  private _tachoGraph: ITachographCard;

  @Input() set tachoGraph(tachoGraph: ITachographCard) {
    this._tachoGraph = tachoGraph;
    if (tachoGraph) {
      this.tachoItems = [
        {title: "Status", value: tachoGraph.status},
        {title: "Valid from", value: FormatDate(tachoGraph.validFrom)},
        {title: "Valid to", value: FormatDate(tachoGraph.validTo)},
        {title: "Card number", value: tachoGraph.number}
      ];
    }

  }
  get tachoGraph() { return this._tachoGraph; }

  @Input() cpc: ICpc;

  tachoItems: IListGroupItem[];

  constructor() { }

  ngOnInit() {
  }

}
