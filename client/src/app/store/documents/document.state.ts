import {IDocument} from "../../../../../shared/interfaces/document.interface";

export interface DocumentsState {
  list: IDocument[];
  selected?: IDocument
}
