import {Component, OnInit} from '@angular/core';
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {DriverService} from "../../../services/driver.service";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {BaseComponent} from "../../shared/components/base/base.component";
import {SelectCompany} from "../../../store/company/company.selectors";
import {BusyAction} from "../../../store/progress/progress.actions";
import {LoadDriversSuccessAction} from "../../../store/drivers/driver.actions";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";

@Component({
  selector: 'gp-driver-import',
  template: `
    <div class="mb-4">
      <div class="mb-2">
        <div class="font-weight-normal mb-1">Select csv file to import:</div>
        <input type="file" accept=".csv" (change)="fileSelected($event)">
      </div>

      <div class="md-form">
        <input type="checkbox" class="filled-in" id="checkbox" [(ngModel)]="ownedVehicle">
        <label for="checkbox">Owned Vehicle</label>
      </div>      
    </div>

    <gp-card [header]="'Drivers'">
      <gp-dynamic-list [list]="list" [itemsPerPage]="100"></gp-dynamic-list>
    </gp-card>

    <button class="btn btn-primary waves-effect" (click)="back()">
      <i class="fa fa-chevron-left mr-2" aria-hidden="true"></i>Back</button>
    <button [disabled]="!list" class="btn btn-primary waves-effect" (click)="importDrivers()">
      <i class="fa fa-download mr-2" aria-hidden="true"></i>Import</button>
    
    <gp-error-alert [error]="error"></gp-error-alert>
  `
})

export class DriverImportComponent extends BaseComponent implements OnInit {

  list: IDynamicList;
  company: ICompany;
  error: string;
  ownedVehicle: boolean = true;

  constructor(
    private store: Store<AppState>,
    private driverService: DriverService,
    private router: Router,
    private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));
  }

  fileSelected = (fileInput: any) => {

    let file = fileInput.target.files[0];
    let reader: FileReader = new FileReader();
    reader.readAsText(file);
    let self = this;

    reader.onload = (e) => {
      self.list = { columns: [] };
      let csv: string = reader.result;
      let lines = csv.split(/\r|\n|\r/);
      let headers = lines[0].split(',');
      let items = [];

      headers.forEach(header => {
        if (header && header.trim().length > 0) {
          self.list.columns.push({
            header: header,
            field: header,
          });
        }
      });

      for (let line = 1; line < lines.length; line++) {
        let data = lines[line].split(',');
        if (data.length === headers.length) {
          let item = {
            ownedVehicle: self.ownedVehicle
          };

          headers.forEach((header, i) => {
            if (header && header.trim().length > 0) {
              item[headers[i]] = data[i];
            }
          });

          items.push(item);
        }
      }

      self.list.items = items;
    }
  };

  importDrivers() {
    this.store.dispatch(new BusyAction(true));
    this.driverService.importDrivers(this.company._id, this.list.items)
      .subscribe(drivers => {
        this.store.dispatch(new BusyAction(false));
        this.store.dispatch(new LoadDriversSuccessAction(drivers, drivers.length));
        return this.router.navigate(['../'], { relativeTo: this.route });

      }, error => {
        this.store.dispatch(new BusyAction(false));
        this.error = error.message;
      });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
