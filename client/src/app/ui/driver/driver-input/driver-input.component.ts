import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {Driver} from "../driver.model";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {Paths} from "../../../shared/constants";
import {SaveDriverAction} from "../../../store/drivers/driver.actions";
import { FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {DriverInputMapper} from "./driver-input.mapper";
import {FormGroup} from "@angular/forms";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {DynamicFormComponent} from "../../shared/components/dynamic-form/dynamic-form.component";
import {SelectIsDriver} from "../../../store/authentication/authentication.selectors";
import {jsonDeserialiser} from "../../../../../../shared/utils/json.deserialiser";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectCompany} from '../../../store/company/company.selectors';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';

@Component({
  selector: 'gp-driver-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups"
                     (submit)="save($event)" (cancel)="cancel()" #dynamicForm></gp-dynamic-form>
  `
})
export class DriverInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private driver: Driver;
  private subscription: Subscription;
  private isDriver: boolean;
  private company: ICompany;

  @ViewChild('dynamicForm') dynamicForm: DynamicFormComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private driverInputMapper: DriverInputMapper) {
  }

  ngOnInit() {
    this.subscription = this.store.let(SelectDriver())
      .filter(driver => driver !== null)
      .combineLatest(
        this.store.let(SelectIsDriver()),
        this.store.let(SelectCompany())
      )
      .subscribe(results => {
        this.driver = results[0];
        this.isDriver = results[1];
        this.company = results[2];

        this.inputGroups = this.driverInputMapper.mapTo(this.driver, this.company.settings, this.isDriver);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    if (!form.controls) return;

    let inputDriver = this.driverInputMapper.mapFrom(form);
    let copy = jsonDeserialiser.parse(JSON.stringify(this.driver));

    let driver = Object.assign(copy, inputDriver, {
      _user: Object.assign(copy._user, inputDriver._user)
    });

    this.store.dispatch(new SaveDriverAction(driver, Paths.Details));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
