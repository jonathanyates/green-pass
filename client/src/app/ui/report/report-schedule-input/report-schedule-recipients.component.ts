import {Component, Input, OnInit} from '@angular/core';
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {User} from "../../user/user.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectUsersAction} from "../../../store/users/user.actions";
import {SelectUsers} from "../../../store/users/user.selectors";
import {Company} from "../../company/company.model";
import {ColumnType} from "../../../../../../shared/models/column-type.model";

@Component({
  moduleId: module.id,
  selector: 'gp-report-schedule-recipients',
  template: '<gp-dynamic-list [list]="list" [lightHeader]="true"></gp-dynamic-list>'
})
export class ReportScheduleRecipientsComponent implements OnInit {

  private _recipients: IUser[];
  @Input()
  set recipients(recipients: IUser[]) {
    this._recipients = recipients;
    this.setList();
  }
  get recipients() {
    return this._recipients;
  }

  @Input() set company(company: Company) {
    if (company) {
      this.store.dispatch(new SelectUsersAction(company._id));
    }
  }

  get selectedUsers() {
    return this.list.items.filter(item => item.selected).map(item => item.user);
  }

  private users: IUser[];
  private subscriptions = [];
  list: IDynamicList;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectUsers())
      .subscribe(users => {
        this.users = users;
        this.setList();
      }));
  }

  private setList() {
    this.list = {
      columns: [
        {header: 'Selected', field: 'selected', columnType: ColumnType.CheckBox},
        { header: 'Name', field: 'name' },
        { header: 'Email address', field: 'email' }
      ]
    };

    if (this.users) {
      this.list.items = this.users.map(user => {
        return {
          selected: this._recipients
            ? this._recipients.find(recipient => recipient._id === user._id) != null
            : false,
          name: User.getFullName(user),
          email: user.email,
          user: user
        }
      })
    }
  }


}
