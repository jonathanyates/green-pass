import {NavigationState} from "./navigation.state";
import * as actions from "./navigation.actions";
import {NavigationLinks} from "./navigation.model";
import {ActionTypes} from "../authentication/authentication.actions";

const initialState: NavigationState = {
  breadcrumb: [],
  navigationLinks: NavigationLinks
};

export function NavigationReducer(state: NavigationState = initialState, action: actions.Actions): NavigationState {
    switch (action.type) {

      case actions.ActionTypes.NAVIGATION_END:
        return Object.assign({}, state, { breadcrumb: action.payload });

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }

  };
