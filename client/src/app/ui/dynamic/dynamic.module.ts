import {NgModule} from '@angular/core';
import {AddPageComponent} from "./add-page/add-page.component";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    imports: [
      CommonModule,
      BrowserModule,
      RouterModule,
      FormsModule,
      HttpClientModule,
      RouterModule,
      SharedModule,
    ],
    exports: [
      AddPageComponent
    ],
    declarations: [
      AddPageComponent
    ],
    providers: [

    ],
})
export class DynamicModule { }
