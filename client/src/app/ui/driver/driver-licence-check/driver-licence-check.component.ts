import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {ActionTypes, DrivingLicenceCheckAction} from "../../../store/drivers/driver.actions";
import {ServerError} from "../../../shared/errors/server.error";
import HTTP_STATUS_CODES from "../../../shared/errors/status-codes.enum";
import {DriverService} from "../../../services/driver.service";
import {FormInput} from "../../shared/components/dynamic-form/dynamic-form.model";
import {BusyAction} from "../../../store/progress/progress.actions";
import {messenger} from "../../../services/messenger";
import {RegExpPatterns} from "../../../../../../shared/constants";
import {DynamicFormBuilder} from "../../shared/components/dynamic-form/dynamic-form.builder";
import {FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'gp-driver-licence-check',
  templateUrl: 'driver-licence-check.component.html',
  styleUrls: ['driver-licence-check.component.css']
})
export class DriverLicenceCheckComponent implements OnInit {

  url: string;
  driver: IDriver;
  checkCode: string;
  licenceNumber: string;
  niNumber: string;
  postcode: string;
  confirm: boolean = false;
  error: string;
  busy: boolean = false;
  dlnInput: FormInput;
  niInput: FormInput;
  postcodeInput: FormInput;
  checkCodeValid: boolean;
  form: FormGroup;
  allowCheckCodeEntry: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute,
    private driverService: DriverService) {
  }

  ngOnInit() {
    this.subscriptions.push(
      this.store.let(SelectDriver())
        .filter(driver => driver != null)
        .subscribe(driver => {
          this.driver = driver;
          this.licenceNumber = driver.licenceNumber;
          this.niNumber = driver.niNumber;
          if (driver && driver.address) {
            this.postcode = driver.address.postcode;
          }
          if (!this.licenceNumber || !this.niNumber || !this.postcode) {
            this.setDriverLicenceInput();
          }
        }));

    messenger.toObservable(ActionTypes.LICENCE_CHECK_FAILURE)
      .subscribe((error: ServerError) => {
        if (error.code === HTTP_STATUS_CODES.BAD_REQUEST && error.message.startsWith('Authentication was unsuccessful')) {
          this.error = 'Invalid Check Code or Driving Licence Number. Please try enter a valid Check Code.'
        } else {
          this.error = error.message;
        }
      });

  }

  private setDriverLicenceInput() {
    let inputs = [];

    if (!this.licenceNumber) {
      this.dlnInput = {
        id: 'dln', label: 'Please enter your Driving Licence Number', type: 'text',
        regex: RegExpPatterns.DrivingLicenceNumber, required: true, autoFocus: true, uppercase: true
      };
      inputs.push(this.dlnInput)
    }

    if (!this.niNumber) {
      this.niInput = {
        id: 'ni', label: 'Please enter your National Insurance Number', type: 'text',
        regex: RegExpPatterns.NiNumber, required: true, uppercase: true
      };
      if (!this.dlnInput) {
        this.niInput.autoFocus = true;
      }
      inputs.push(this.niInput);
    }

    if (!this.postcode) {
      this.postcodeInput = {
        id: 'postCode', label: 'Please enter your Postcode', type: 'text',
        regex: RegExpPatterns.Postcode, required: true, uppercase: true
      };
      if (!this.dlnInput && !this.niInput) {
        this.postcodeInput.autoFocus = true;
      }
      inputs.push(this.postcodeInput);
    }

    this.form = DynamicFormBuilder.build(inputs);

    if (this.dlnInput) {
      this.dlnInput.control.valueChanges
        .subscribe(value => {
          if (this.dlnInput.control.valid) {
            this.licenceNumber = value;
          } else {
            this.licenceNumber = null;
          }
        });
    }

    if (this.niInput) {
      this.niInput.control.valueChanges
        .subscribe(value => {
          if (this.niInput.control.valid) {
            this.niNumber = value;
          } else {
            this.niNumber = null;
          }
        });
    }

    if (this.postcodeInput) {
      this.postcodeInput.control.valueChanges
        .subscribe(value => {
          if (this.postcodeInput.control.valid) {
            this.postcode = value.toString().toUpperCase();
          } else {
            this.postcode = null;
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getCheckCode() {
    this.error = null;
    this.busy = true;
    this.store.dispatch(new BusyAction());

    this.driverService.getDrivingLicenceCheckCode(this.driver, this.licenceNumber, this.niNumber, this.postcode).take(1)
      .subscribe(checkCode => {
          this.store.dispatch(new BusyAction(false));
          this.busy = false;
          this.checkCode = checkCode;
          this.saveDriver();
        },
        error => {
          this.store.dispatch(new BusyAction(false));
          this.busy = false;
          this.error = error;
        }
      );
  }

  enterCheckCode() {
    this.allowCheckCodeEntry = true;
  }

  saveDriver() {
    if (this.driver.licenceNumber != this.licenceNumber ||
      this.driver.niNumber != this.niNumber ||
      this.driver.address == null || this.driver.address.postcode != this.postcode) {
      this.driver.licenceNumber = this.licenceNumber;
      this.driver.niNumber = this.niNumber;
      if (this.driver.address == null) {
        this.driver.address = {};
      }
      this.driver.address.postcode = this.postcode;

      this.busy = true;
      this.store.dispatch(new BusyAction());

      this.driverService.saveDriver(this.driver)
        .subscribe(driver => {
          this.driver = driver;
          this.store.dispatch(new BusyAction(false));
          this.busy = false;
        },
          error => {
            this.store.dispatch(new BusyAction(false));
            this.busy = false;
            this.error = error;
          }
        );
    }
  }

  licenceCheck() {
    this.error = null;
    this.busy = true;

    if (!this.driver.licenceNumber && this.licenceNumber) {
      this.store.dispatch(new DrivingLicenceCheckAction(this.driver, this.checkCode, this.licenceNumber));
    } else if (this.driver.licenceNumber) {
      this.store.dispatch(new DrivingLicenceCheckAction(this.driver, this.checkCode));
    }
  }

  licenceCheckFromEnteredCheckCode(checkCode: string) {
    this.error = null;
    this.busy = true;

    if (checkCode && !this.driver.licenceNumber && this.licenceNumber) {
      this.store.dispatch(new DrivingLicenceCheckAction(this.driver, checkCode, this.licenceNumber));
    } else if (this.driver.licenceNumber) {
      this.store.dispatch(new DrivingLicenceCheckAction(this.driver, checkCode));
    }
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
