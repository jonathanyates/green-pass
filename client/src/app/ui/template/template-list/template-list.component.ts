import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Subscription} from "rxjs";
import {SelectCompany} from "../../../store/company/company.selectors";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {SelectTemplates} from "../../../store/templates/template.selectors";
import {SaveCompanyTemplatesAction} from "../../../store/templates/template.actions";
import {DynamicListComponent} from "../../shared/components/dynamic-list/dynamic-list.component";
import {ColumnType} from "../../../../../../shared/models/column-type.model";
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {Paths} from "../../../shared/constants";
import {SelectIsGreenPassAdmin} from "../../../store/authentication/authentication.selectors";

@Component({
  selector: 'gp-template-list',
  template: `
    <gp-dynamic-list [list]="list" [editing]="editing" (itemSelected)="select($event)" (back)="back()"
        (add)="add()" [allowAdd]="allowAdd"></gp-dynamic-list>
    <button *ngIf="editing" class="btn btn-primary waves-effect" (click)="cancel()">Cancel</button>
    <button *ngIf="editing" class="btn btn-primary waves-effect" (click)="save()">Save</button>
`
})
export class TemplateListComponent implements OnInit {

  company: ICompany;
  templates: ITemplate[];
  allTemplates: ITemplate[];
  list: IDynamicList;
  allowAdd: boolean;
  editing: boolean;
  editSubscription: Subscription;
  @ViewChild(DynamicListComponent) dynamicList: DynamicListComponent;

  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.subscription = this.route.data
      .map((data: { templates: ITemplate[] }) => data.templates)
      .merge(this.store.let(SelectTemplates()))
      .distinctUntilChanged()
      .combineLatest(
        this.store.let(SelectCompany()),
        this.store.let(SelectIsGreenPassAdmin()),
        (templates:ITemplate[], company:ICompany, isAdmin: boolean) => ({
          templates: templates,
          company: company,
          isAdmin: isAdmin
        }))
      .subscribe(result => {
        if (result.templates) {
          this.templates = result.templates.sort((a, b) => a.folderName.localeCompare(b.folderName));
          this.allTemplates = this.templates;

          if (result.company) {
            this.company = result.company;
            this.allTemplates.forEach(template =>
              template.selected = result.company.templates.indexOf(template._id) > -1);
            this.templates = this.templates.filter(template => template.selected);
          }
        }

        this.setList(this.templates);

        this.allowAdd = result.isAdmin;
        if (result.isAdmin && this.company) {
          this.editSubscription = this.dynamicList.edit.subscribe(_ => this.edit());
        }
        else if (this.editSubscription) {
          this.editSubscription.unsubscribe();
        }
      });
  }

  private setList(templates: ITemplate[]) {
    this.list = {
      columns: [
        {header: 'Name', field: 'name'},
        {header: 'Folder', field: 'folderName'},
        {header: 'Template Type', field: 'type'}
      ],
      items: templates.map(template => Object.assign({}, template, {
        type: template._companyId ? 'Company' : 'Standard'
      }))
    };
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.editSubscription) {
      this.editSubscription.unsubscribe();
    }
  }

  select(template: ITemplate) {
    if (template) {
      this.router.navigate(['./', template._id], { relativeTo: this.route });
    }
  }

  edit() {
    if (!this.editing && this.company) {
      this.editing = true;
      this.editing = true;
      this.list = {
        columns: [
          { header: 'Selected', field: 'selected', columnType: ColumnType.CheckBox },
          { header: 'Name', field: 'name' },
          { header: 'Folder', field: 'folderName' }
        ],
        items: this.allTemplates.map(template => Object.assign({}, template))
      };
    }
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  save() {
    if (this.company && this.templates) {
      let templates = this.list.items.filter((template: ITemplate) => template.selected)
        .map((template: ITemplate) => template._id);
      this.store.dispatch(new SaveCompanyTemplatesAction(this.company._id, templates))
    }
  }

  cancel() {
    this.editing = false;
    this.setList(this.templates);
  }

  add() {
    this.router.navigate(['./', Paths.Add], { relativeTo: this.route });
  }

}
