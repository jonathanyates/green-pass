import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {ServerError} from "../shared/errors/server.error";
import {HttpClient} from "@angular/common/http";
import {Ng2ImgMaxService} from "ng2-img-max";

@Injectable()
export class FileService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(
    httpClient: HttpClient,
    store: Store<AppState>,
    private ng2ImgMaxService: Ng2ImgMaxService
  ) {
    super(httpClient, store);
  }

  getFile(companyId: string, filename: string): Observable<any> {

    this.authorize();
    let options = this.getOptions(null, 'blob');
    options.headers = options.headers.append('Content-Type', 'image');

    return this.httpClient.get(`${this.companiesUrl}${companyId}/files/${filename}`, options)
      .map(result => result)
      .catch(res => {
        return Observable.throw(new ServerError(res.statusText, res.status))
      });
  }

  compressFile(file): Observable<File> {

    if (file) {
      try {
        return this.ng2ImgMaxService.resizeImage(file, 1500, 10000, true)
          .switchMap(result => {

            if (result.error) {
              console.log(`Error ${result.error} occured trying to resize file. ${result.reason}`);
              return Observable.of(result.compressedFile);
            }

            return this.ng2ImgMaxService.compressImage(result, 0.5)
              .map(result => {
                if (result.error) {
                  console.log(`Error ${result.error} occured trying to compress file. ${result.reason}`);
                  return result.compressedFile;
                }
                return result;
              })
          });
      } catch(err) {
        console.log(`Error occured trying to compress file ${file.name}. Error: ${err.message}`);
        return Observable.throw(new Error('Unable to read of process file. Please select a valid file.'));
      }
    }
    return Observable.empty<File>();
  }

}
