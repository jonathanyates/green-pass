import {ContainerModule, interfaces} from "inversify";
import {TYPES} from "../container/container.types";
import {IApiRouter} from "../interfaces/api-router.interface";
import {IRepository} from "../db/db.repository";
import {ITemplate} from "../../../shared/interfaces/document.interface";
import {TemplateRepository} from "./template.repository";
import {TemplateRouter} from "./template.router";
import {ITemplateController, TemplateController} from "./template.controller";
import {ITemplateService, TemplateService} from "./template.service";

export const templateModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.TemplateRouter).to(TemplateRouter).inSingletonScope();
  bind<ITemplateController>(TYPES.controllers.TemplateController).to(TemplateController).inSingletonScope();
  bind<ITemplateService>(TYPES.services.TemplateService).to(TemplateService).inSingletonScope();
  bind<IRepository<ITemplate>>(TYPES.repositories.TemplateRepository).to(TemplateRepository).inSingletonScope();
});