import { IDvlaVehicleEnquiryResponse } from '../interfaces/vehicle-enquiry.interface';
import { IDrivingLicenceEnquiry } from '../interfaces/driving-licence-enquiry.interface';
import { IMotEnquiry } from '../interfaces/dvsa-mot-enquiry.interface';
import { IDvsaHgvTestEnquiry } from '../interfaces/dvsa-hgv-test-enquiry.interface';

export interface IDvlaService {
  vehicleEnquiry(registrationNumber: string): Promise<IDvlaVehicleEnquiryResponse>;

  motEnquiry(registrationNumber: string): Promise<IMotEnquiry>;

  hgvTestEnquiry(registrationNumber: string): Promise<IDvsaHgvTestEnquiry>;

  generateCheckCode(drivingLicenceNumber: string, nationalInsuranceNumber: string, postCode: string): Promise<string>;

  drivingLicenceEnquiry(
    drivingLicenceNumber: string,
    nationalInsuranceNumber: string,
    postCode: string
  ): Promise<IDrivingLicenceEnquiry>;

  drivingLicenceCheck(drivingLicenceNumber: string, checkCode: string): Promise<IDrivingLicenceEnquiry>;
}
