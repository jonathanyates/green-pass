import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {Roles} from "../../../../../shared/constants";
import {DriverComponent} from "./driver.component";
import {DriverDocumentComponent} from "./driver-document/driver-document.component";
import {DriverDashboardComponent} from "./driver-dashboard/driver-dashboard.component";
import {DriverDocumentsContainerComponent} from "./driver-documents/driver-documents-container.component";
import {CompanyComponent} from "../company/company.component";
import {DriverReferenceInputComponent} from "./driver-reference/driver-reference-input/driver-reference-input.component";
import {DriverReferenceResolve} from "./driver-reference/driver-reference.resolve";
import {DriverReferenceDetailComponent} from "./driver-reference/driver-reference-details/driver-reference-details.component";
import {DriverListResolve} from "./driver-list/driver-list.resolve";
import {DriverResolve} from "./driver.resolve";
import {DriverInputComponent} from "./driver-input/driver-input.component";
import {DriverDetailsTabListComponent} from "./driver-details-tab-list/driver-details-tab-list.component";
import {DriverUserResolve} from "./driver-user.resolve";
import {DriverDocumentResolve} from "./driver-document/driver-document.resolve";
import {DriverBreadcrumbRouting} from "./driver-breadcrumb.routing";
import {RoadMarqueComponent} from "./roadmarque/roadmarque.component";
import {OnRoadAssessmentComponent} from "./driver-assessments/on-road-assessment/on-road-assessment.component";
import {OnRoadAssessmentInputComponent} from "./driver-assessments/on-road-assessment/on-road-assessment-input.component";
import {OnRoadAssessmentResolve} from "./driver-assessments/on-road-assessment/on-road-assessment.resolve";
import {DriverNextOfKinInputComponent} from "./driver-next-of-kin/driver-next-of-kin-input.component";
import {DriverLicenceCheckComponent} from "./driver-licence-check/driver-licence-check.component";
import {DriverListContainerComponent} from "./driver-list/driver-list-container.component";
import {DriverInsuranceResolve} from "./driver-insurance/driver-insurance.resolve";
import {DriverInsuranceInputComponent} from "./driver-insurance/driver-insurance-input/driver-insurance-input.component";
import {DriverInsuranceComponent} from "./driver-insurance/driver-insurance-detail/driver-insurance.component";
import {DriverVehicleResolve} from "./driver-vehicle/driver-vehicle.resolve";
import {DriverVehicleSelectComponent} from "app/ui/driver/driver-vehicle/driver-vehicle-select.component";
import {VehicleDriverResolve} from "./driver-vehicle/vehicle-driver.resolve";
import {VehicleDriverComponent} from "./driver-vehicle/vehicle-driver.component";
import {SubscriptionGuard} from "../security/guards/subscription.guard";
import {DriverDetailsContainerComponent} from "./driver-details/driver-details-container.component";
import {DriverVehicleListContainerComponent} from "./driver-vehicle/driver-vehicle-list-container.component";
import {DriverInsuranceListContainerComponent} from "./driver-insurance/driver-insurance-list/driver-insurance-list-container.component";
import {DriverAssessmentsContainerComponent} from "./driver-assessments/driver-assessments-container.component";
import {DriverReferenceListContainerComponent} from "./driver-reference/driver-reference-list/driver-reference-list-container.component";
import {DriverNextOfKinContainerComponent} from "./driver-next-of-kin/driver-next-of-kin.container.component";
import {DriverLicenceContainerComponent} from "./driver-licence-container/driver-licence-container.component";
import {VehicleServiceInputComponent} from "../vehicle/vehicle-service/vehicle-service-input/vehicle-service-input.component";
import {VehicleServiceComponent} from "../vehicle/vehicle-service/vehicle-service-detail/vehicle-service.component";
import {VehicleDetailsTabListComponent} from "../vehicle/vehicle-details-tab-list/vehicle-details-tab-list.component";
import {VehicleInspectionInputComponent} from "../vehicle/vehicle-inspection/vehicle-inspection-input/vehicle-inspection-input.component";
import {VehicleInspectionComponent} from "../vehicle/vehicle-inspection/vehicle-inspection-detail/vehicle-service.component";
import {VehicleCheckInputComponent} from "../vehicle/vehicle-check/vehicle-check-input/vehicle-check-input.component";
import {VehicleCheckComponent} from "../vehicle/vehicle-check/vehicle-check-detail/vehicle-check.component";
import {VehicleServiceResolve} from "../vehicle/vehicle-service/vehicle-service.resolve";
import {VehicleInspectionResolve} from "../vehicle/vehicle-inspection/vehicle-inspection.resolve";
import {VehicleCheckResolve} from "../vehicle/vehicle-check/vehicle-check.resolve";
import {VehicleSearchInputComponent} from "../vehicle/vehicle-input/vehicle-search-input.component";
import {CompanyDriverDocumentResolve} from "./driver-document/company-driver-document.resolve";
import {DriverImportComponent} from "./driver-import/driver-import.component";
import {DriverFawListContainerComponent} from './driver-faw/driver-faw-list/driver-faw-list-container.component';
import {DriverFawInputComponent} from './driver-faw/driver-faw-input/driver-faw-input.component';
import {DriverFawComponent} from './driver-faw/driver-faw-detail/driver-faw.component';
import {DriverFawResolve} from './driver-faw/driver-faw.resolve';

const routes: Routes = [
  {
    path: 'driver',
    component: DriverComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Driver]},
    children: [
      {path: '', component: DriverDashboardComponent, resolve: { driver: DriverUserResolve } },
      {path: 'details', component: DriverDetailsContainerComponent, resolve: { driver: DriverUserResolve } },
      {path: 'details/edit', component: DriverInputComponent, resolve: { companyDriver: DriverUserResolve } },

      {path: 'nextofkin', component: DriverNextOfKinContainerComponent, resolve: { companyDriver: DriverUserResolve } },
      {path: 'nextofkin/add', component: DriverNextOfKinInputComponent, resolve: { companyDriver: DriverUserResolve } },
      {path: 'nextofkin/edit', component: DriverNextOfKinInputComponent, resolve: { companyDriver: DriverUserResolve } },

      { path: 'references', component: DriverReferenceListContainerComponent, resolve: { driverReference: DriverUserResolve } },
      { path: 'references/new', component: DriverReferenceInputComponent, resolve: { driverReference: DriverUserResolve } },
      { path: 'references/:referenceId', component: DriverReferenceDetailComponent, resolve: { driverReference: DriverUserResolve } },
      { path: 'references/:referenceId/edit', component: DriverReferenceInputComponent, resolve: { driverReference: DriverUserResolve } },

      {path: 'licenceCheck', component: DriverLicenceCheckComponent, resolve: { driver: DriverUserResolve } },
      {path: 'licence', component: DriverLicenceContainerComponent, resolve: { driver: DriverUserResolve } },

      {path: 'assessments', component: DriverAssessmentsContainerComponent, resolve: { driver: DriverUserResolve } },
      {path: 'assessments/roadMarque', component: RoadMarqueComponent, canActivate: [SubscriptionGuard], resolve: { driver: DriverUserResolve } },

      {path: 'documents', component: DriverDocumentsContainerComponent, resolve: { driver: DriverUserResolve } },
      {path: 'documents/:documentId', component: DriverDocumentComponent, resolve: { driver: DriverDocumentResolve } },

      { path: 'insurance', component: DriverInsuranceListContainerComponent, resolve: { driverInsurance: DriverUserResolve } },
      { path: 'insurance/new', component: DriverInsuranceInputComponent, resolve: { driverInsurance: DriverUserResolve } },
      { path: 'insurance/:insuranceId', component: DriverInsuranceComponent, resolve: { driverInsurance: DriverUserResolve } },
      { path: 'insurance/:insuranceId/edit', component: DriverInsuranceInputComponent, resolve: { driverInsurance: DriverUserResolve } },

      { path: 'faw', component: DriverFawListContainerComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/new', component: DriverFawInputComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/:fawId', component: DriverFawComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/:fawId/edit', component: DriverFawInputComponent, resolve: { driverFaw: DriverUserResolve } },
      
      { path: 'faw', component: DriverFawListContainerComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/new', component: DriverFawInputComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/:fawId', component: DriverFawComponent, resolve: { driverFaw: DriverUserResolve } },
      { path: 'faw/:fawId/edit', component: DriverFawInputComponent, resolve: { driverFaw: DriverUserResolve } },

      { path: 'vehicles', component: DriverVehicleListContainerComponent, resolve: { driverVehicle: DriverUserResolve } },
      { path: 'vehicles/add', component: DriverVehicleSelectComponent, resolve: { driverVehicle: DriverUserResolve } },
      { path: 'vehicles/:vehicleId', component: VehicleDetailsTabListComponent, resolve: { driverVehicle: DriverUserResolve } },
      { path: 'vehicles/:vehicleId/edit', component: VehicleSearchInputComponent, resolve: { vehicle: DriverUserResolve } },

      { path: ':vehicles/:vehicleId/services', redirectTo: ':vehicles/:vehicleId' },
      { path: ':vehicles/:vehicleId/services/new', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':vehicles/:vehicleId/services/:serviceId', component: VehicleServiceComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':vehicles/:vehicleId/services/:serviceId/edit', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },

      { path: ':vehicles/:vehicleId/inspections', redirectTo: ':vehicles/:vehicleId' },
      { path: ':vehicles/:vehicleId/inspections/new', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':vehicles/:vehicleId/inspections/:inspectionId', component: VehicleInspectionComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':vehicles/:vehicleId/inspections/:inspectionId/edit', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },

      { path: ':vehicles/:vehicleId/checks', redirectTo: ':vehicles/:vehicleId' },
      { path: ':vehicles/:vehicleId/checks/new', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':vehicles/:vehicleId/checks/:checkId', component: VehicleCheckComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':vehicles/:vehicleId/checks/:checkId/edit', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } }
    ]
  },
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/drivers', component: DriverListContainerComponent, resolve: { companyDrivers: DriverListResolve } },
      { path: ':id/drivers/new', component: DriverInputComponent, resolve: { companyDriver: DriverResolve } },
      { path: ':id/drivers/import', component: DriverImportComponent, resolve: { companyDriver: DriverResolve } },
      { path: ':id/drivers/:driverId', component: DriverDetailsTabListComponent, resolve: { companyDriver: DriverResolve } },
      { path: ':id/drivers/:driverId/edit', component: DriverInputComponent, resolve: { companyDriver: DriverResolve } },

      { path: ':id/drivers/:driverId/nextofkin/edit', component: DriverNextOfKinInputComponent, resolve: { companyDriver: DriverResolve } },
      { path: ':id/drivers/:driverId/nextofkin', redirectTo: ':id/drivers/:driverId' },

      { path: ':id/drivers/:driverId/references/new', component: DriverReferenceInputComponent, resolve: { driverReference: DriverReferenceResolve } },
      { path: ':id/drivers/:driverId/references/:referenceId', component: DriverReferenceDetailComponent, resolve: { driverReference: DriverReferenceResolve } },
      { path: ':id/drivers/:driverId/references/:referenceId/edit', component: DriverReferenceInputComponent, resolve: { driverReference: DriverReferenceResolve } },
      { path: ':id/drivers/:driverId/references', redirectTo: ':id/drivers/:driverId' },

      { path: ':id/drivers/:driverId/assessments/new', canActivate: [SubscriptionGuard], component: OnRoadAssessmentInputComponent, resolve: { onRoadAssessment: OnRoadAssessmentResolve } },
      { path: ':id/drivers/:driverId/assessments/:assessmentId', canActivate: [SubscriptionGuard], component: OnRoadAssessmentComponent, resolve: { onRoadAssessment: OnRoadAssessmentResolve } },
      { path: ':id/drivers/:driverId/assessments/:assessmentId/edit', canActivate: [SubscriptionGuard], component: OnRoadAssessmentInputComponent, resolve: { onRoadAssessment: OnRoadAssessmentResolve } },
      { path: ':id/drivers/:driverId/assessments', redirectTo: ':id/drivers/:driverId', canActivate: [SubscriptionGuard] },

      { path: ':id/drivers/:driverId/documents', redirectTo: ':id/drivers/:driverId' },
      {path: ':id/drivers/:driverId/documents/:documentId', component: DriverDocumentComponent, resolve: { driver: CompanyDriverDocumentResolve } },

      { path: ':id/drivers/:driverId/insurance/new', component: DriverInsuranceInputComponent, resolve: { driverInsurance: DriverInsuranceResolve } },
      { path: ':id/drivers/:driverId/insurance/:insuranceId', component: DriverInsuranceComponent, resolve: { driverInsurance: DriverInsuranceResolve } },
      { path: ':id/drivers/:driverId/insurance/:insuranceId/edit', component: DriverInsuranceInputComponent, resolve: { driverInsurance: DriverInsuranceResolve } },
      { path: ':id/drivers/:driverId/insurance', redirectTo: ':id/drivers/:driverId' },

      { path: ':id/drivers/:driverId/faw/new', component: DriverFawInputComponent, resolve: { driverFaw: DriverFawResolve } },
      { path: ':id/drivers/:driverId/faw/:fawId', component: DriverFawComponent, resolve: { driverFaw: DriverFawResolve } },
      { path: ':id/drivers/:driverId/faw/:fawId/edit', component: DriverFawInputComponent, resolve: { driverFaw: DriverFawResolve } },
      { path: ':id/drivers/:driverId/faw', redirectTo: ':id/drivers/:driverId' },

      { path: ':id/drivers/:driverId/vehicles', redirectTo: ':id/drivers/:driverId' },
      { path: ':id/drivers/:driverId/vehicles/add', component: DriverVehicleSelectComponent, resolve: { driverVehicle: DriverResolve } },

      { path: ':id/drivers/:driverId/vehicles/:vehicleId', component: VehicleDetailsTabListComponent, resolve: { driverVehicle: DriverVehicleResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/edit', component: VehicleSearchInputComponent, resolve: { vehicle: DriverVehicleResolve } },

      { path: ':id/drivers/:driverId/vehicles/:vehicleId/services/new', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/services/:serviceId', component: VehicleServiceComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/services/:serviceId/edit', component: VehicleServiceInputComponent, resolve: { vehicleReference: VehicleServiceResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/services', redirectTo: ':id/drivers/:driverId/vehicles/:vehicleId' },

      { path: ':id/drivers/:driverId/vehicles/:vehicleId/inspections/new', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/inspections/:inspectionId', component: VehicleInspectionComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/inspections/:inspectionId/edit', component: VehicleInspectionInputComponent, resolve: { vehicleReference: VehicleInspectionResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/inspections', redirectTo: ':id/drivers/:driverId/vehicles/:vehicleId' },

      { path: ':id/drivers/:driverId/vehicles/:vehicleId/checks/new', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/checks/:checkId', component: VehicleCheckComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/checks/:checkId/edit', component: VehicleCheckInputComponent, resolve: { vehicleReference: VehicleCheckResolve } },
      { path: ':id/drivers/:driverId/vehicles/:vehicleId/checks', redirectTo: ':id/drivers/:driverId/vehicles/:vehicleId' },

      { path: ':id/vehicles/:vehicleId/drivers/:driverId', component: VehicleDriverComponent, resolve: { driverVehicle: VehicleDriverResolve } }
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    DriverListResolve,
    DriverResolve,
    DriverUserResolve,
    DriverReferenceResolve,
    DriverDocumentResolve,
    CompanyDriverDocumentResolve,
    OnRoadAssessmentResolve,
    DriverInsuranceResolve,
    DriverFawResolve,
    DriverVehicleResolve,
    VehicleDriverResolve
  ]
})
export class DriverRoutingModule {
  constructor(private breadcrumbRouting: DriverBreadcrumbRouting) {
    this.breadcrumbRouting.configure();
  }
}
