import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.states';
import {IVehicle} from '../../../../../../shared/interfaces/vehicle.interface';
import {SelectSelectedVehicleDetailsTab, SelectVehicle} from '../../../store/vehicles/vehicle.selectors';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectCompany} from '../../../store/company/company.selectors';
import {Company} from '../../company/company.model';
import {SelectVehicleDetailsTabAction, UpdateVehicleSuccessAction} from '../../../store/vehicles/vehicle.actions';
import {Paths} from '../../../shared/constants';
import {VehicleTypes} from '../../../../../../shared/interfaces/constants';
import {ModalComponent} from '../../shared/components/modal/model.component';
import {IDriver} from '../../../../../../shared/interfaces/driver.interface';
import {BusyAction} from '../../../store/progress/progress.actions';
import {VehicleService} from '../../../services/vehicle.service';
import {HandleErrorAction} from '../../../store/error/error.actions';
import { Subscription } from 'rxjs/Subscription';
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../shared/constants";

@Component({
  selector: 'gp-vehicle-details-tab-list',
  templateUrl: 'vehicle-details-tab-list.component.html'
})
export class VehicleDetailsTabListComponent implements OnInit, OnDestroy {

  vehicle: IVehicle;
  company: Company;
  selectedTab: string;
  showDrivers = false;
  selectedDriver: IDriver;
  user: IUser;
  isDriver: boolean;
  allowInput: boolean = false;
  protected subscriptions: Subscription[] = [];

  @ViewChild(ModalComponent) modal: ModalComponent;

  constructor(protected store: Store<AppState>,
              private vehicleService: VehicleService,
              protected router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.vehicle = null;

    this.subscriptions.push(this.store.let(SelectVehicle())
      .filter(vehicle => vehicle !== null)
      .combineLatest(
        this.store.let(SelectLoggedInUser()),
        this.store.let(SelectCompany()).filter(company => company != null))
      .subscribe(results => {
        this.vehicle = results[0];
        this.user = results[1];
        this.company = results[2];
        this.isDriver = this.user && this.user.role === Roles.Driver;
        this.allowInput = this.isDriver === false ||
          (this.company.settings && this.company.settings.drivers && this.company.settings.drivers.allowInput);

        this.showDrivers = !this.isDriver && this.vehicle && this.vehicle.type === VehicleTypes.grey;
      }));

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));

    this.subscriptions.push(this.store.let(SelectSelectedVehicleDetailsTab())
      .subscribe(tab => this.selectedTab = tab));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  selectTab(tab: string) {
    this.store.dispatch(new SelectVehicleDetailsTabAction(tab));
  }

  addDriver() {
    if (this.isDriver) return;
    this.router.navigate(['./', Paths.Drivers, Paths.Add], { relativeTo:  this.route});
  }

  removeDriver(driver: IDriver) {
    if (this.isDriver || !driver) {
      return;
    }
    this.modal.open();
    this.selectedDriver = driver;
  }

  selectDriver(driver: IDriver) {
    if (this.isDriver) return;
    this.router.navigate([Paths.Company, this.vehicle._companyId, Paths.Drivers, driver._id]);
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedDriver) {

      this.store.dispatch(new BusyAction());

      this.vehicleService.removeDriver(this.vehicle, this.selectedDriver._id)
        .subscribe(vehicle => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new UpdateVehicleSuccessAction(vehicle));
        }, error => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new HandleErrorAction(error));
        })
    }
    this.selectedDriver = null;
  }

  back() {
    return this.router.navigate(['../'], { relativeTo:  this.route});
  }
}
