import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {CompanyState} from "./company.state";

export const SelectCompanyState = () => {
  return (store:Store<AppState>) => {
    return store.select<CompanyState>(state => state.company);
  }
};

export const SelectCompany = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectCompanyState())
      .map(state => state.company);
  }
};

export const SelectDepot = depotId => {
  return (store:Store<AppState>) => {
    return store.let(SelectCompanyState())
      .map(state => state.company)
      .filter(company => company !== null && company.depots !== null)
      .map(company => company.depots.find(depot => depot._id == depotId));
  }
};

export const SelectSupplier = supplierId => {
  return (store:Store<AppState>) => {
    return store.let(SelectCompanyState())
      .map(state => state.company)
      .filter(company => company !== null && company.suppliers !== null)
      .map(company => company.suppliers.find(supplier => supplier._id == supplierId));
  }
};

export const SelectSelectedCompanyDetailsTab = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectCompanyState())
      .map(state => state.selectedTab);
  }
};
