import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {SelectVehiclesState} from "../../../../store/vehicles/vehicle.selectors";
import {IVehicle, IVehicleService} from "../../../../../../../shared/interfaces/vehicle.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {VehicleServiceInputMapper} from "./vehicle-service-input.mapper";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {SaveVehicleServiceAction} from "../../../../store/vehicles/vehicle-service.actions";

@Component({
  selector: 'gp-service-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
  `
})
export class VehicleServiceInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private vehicle: IVehicle;
  private service: IVehicleService;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private serviceInputMapper: VehicleServiceInputMapper) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectVehiclesState())
      .combineLatest(this.store.let(SelectCompany()).filter(company => company != null),
        (vehicleState, company) => ({vehicleState: vehicleState, company: company}))
      .subscribe(result => {
        this.vehicle = result.vehicleState.selected;
        this.service = result.vehicleState.selectedService
          ? result.vehicleState.selectedService
          : <IVehicleService>{ serviceDate: null };

        this.inputGroups = this.serviceInputMapper.mapTo(this.service, result.company.suppliers);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let service = Object.assign({}, this.service, this.serviceInputMapper.mapFrom(form, this.inputGroups, this.service));
    this.store.dispatch(new SaveVehicleServiceAction(this.vehicle, service));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
