import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectDriverAction} from "../../../store/drivers/driver.actions";
import {SelectDocumentAction} from "../../../store/documents/document.actions";
import {SelectCompanyAction} from "../../../store/company/company.actions";

@Injectable()
export class CompanyDriverDocumentResolve {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let id = route.params['id'];
    let driverId = route.params['driverId'];
    let documentId = route.params['documentId'];
    if (!id || !driverId) return false;

    this.store.dispatch(new SelectCompanyAction(id));
    this.store.dispatch(new SelectDriverAction(id, driverId));

    if (documentId) {
      this.store.dispatch(new SelectDocumentAction(documentId));
    }

    return true;
  }
}
