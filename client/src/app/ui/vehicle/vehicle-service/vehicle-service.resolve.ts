import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {IContact} from "../../../../../../shared/interfaces/driver.interface";
import {SelectVehicleAction} from "../../../store/vehicles/vehicle.actions";
import {ClearVehicleServiceAction, SelectVehicleServiceAction} from "../../../store/vehicles/vehicle-service.actions";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../shared/constants";

@Injectable()
export class VehicleServiceResolve implements Resolve<boolean> {

  private user: IUser;

  constructor(private store: Store<AppState>) {
    this.store.let(SelectLoggedInUser())
      .subscribe(user => this.user = user);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let id: string = this.user && this.user.role === Roles.Driver
      ? this.user._companyId
      : route.params['id'];

    let vehicleId = route.params['vehicleId'];
    let serviceId = route.params['serviceId'];

    if (!id || !vehicleId) return false;

    this.store.dispatch(new SelectCompanyAction(id));
    this.store.dispatch(new SelectVehicleAction(id, vehicleId));

    if (serviceId) {
      this.store.dispatch(new SelectVehicleServiceAction(vehicleId, serviceId));
    } else {
      if (route.url[route.url.length-1].path == 'new') {
        this.store.dispatch(new ClearVehicleServiceAction());
      }
    }

    return true;
  }

}
