import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IFileRecord} from "../../../../../../../../shared/interfaces/common.interface";
import {IListGroupItem} from "../../dynamic-list-group/dynamic-list-group";
import {FormatDate} from "../../../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-fileRecord',
  template: `
    <gp-card [header]="header" (edit)="onEdit()" [allowEdit]="allowEdit">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="onBack()">Back</button>
  `
})
export class FileRecordComponent {

  items: IListGroupItem[];
  private _fileRecord: IFileRecord;
  @Input() allowEdit: boolean;
  @Input() header: string;
  @Input() fileHeading: string;
  @Input() set fileRecord(fileRecord: IFileRecord) {
    this._fileRecord = fileRecord;
    this.setItems(fileRecord);
  }

  @Output() edit: EventEmitter<IFileRecord> = new EventEmitter<IFileRecord>();
  @Output() back: EventEmitter<any> = new EventEmitter<any>();

  setItems(fileRecord: IFileRecord) {
    if (fileRecord) {
      this.items = [
        {title: "Valid From", value: FormatDate(fileRecord.validFrom) },
        {title: "Valid To", value: FormatDate(fileRecord.validTo) }
      ];

      if (fileRecord.filename) {
        this.items.push({title: this.fileHeading, value: fileRecord.file, type: fileRecord.fileType})
      }
    }
  }

  onEdit() {
    this.edit.emit(this._fileRecord);
  }

  onBack() {
    this.back.emit();
  }

}
