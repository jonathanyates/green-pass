import * as actions from "./progress.actions";
import {ActionTypes} from "../authentication/authentication.actions";
import {ProgressState} from "./progress.state";

const initialState: ProgressState = {
  busy: false
};

export function ProgressReducer(state: ProgressState = initialState, action: actions.Actions): ProgressState {
    switch (action.type) {
      case actions.ActionTypes.SET_PROGRESS:
        return {
          busy: <boolean>action.payload
        };

      case ActionTypes.LOGOUT:
        return {
          busy: false,
        };

      default:
        return state;
    }
  };
