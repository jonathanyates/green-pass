import jwt = require('jsonwebtoken');
import passport = require('passport');
import serverConfig from '../../server/server.config';
import { NextFunction, Request, Response } from 'express-serve-static-core';
import { inject, injectable } from 'inversify';
import { IAuthenticationController } from './authentication.controller.interface';
import codes from '../../shared/error.codes';
import { TYPES } from '../../container/container.types';
import { AuditActions, AuditResults, AuditTargets, IAuditService } from '../../audit/audit.service';
import { IAuthenticationService } from '../authentication/authentication.service';
import { IUserService } from '../../users/user.service';
import { IUser } from '../../../../shared/interfaces/user.interface';

@injectable()
export class AuthenticationController implements IAuthenticationController {
  constructor(
    @inject(TYPES.services.AuditService) private auditService: IAuditService,
    @inject(TYPES.services.AuthenticationService)
    private authenticationService: IAuthenticationService,
    @inject(TYPES.services.UserService) private userService: IUserService
  ) {}

  private generateToken = (user) => {
    return jwt.sign(user, serverConfig.authentication.secret, {
      expiresIn: '2d', // Eg: 60, "2 days", "10h", "7d"
    });
  };

  // Set user info from request
  private setUserInfo = (user) => {
    return {
      _id: user._id,
      _companyId: user._companyId,
      username: user.username,
      forename: user.forename,
      surname: user.surname,
      email: user.email,
      role: user.role,
      changePassword: user.changePassword,
    };
  };

  login = (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.status(codes.UNAUTHORIZED).json({ message: info.error });
      }

      const userInfo = this.setUserInfo(user);
      this.auditService.audit(user, AuditActions.Login, AuditTargets.User, AuditResults.Success);
      res.status(codes.OK).json({ token: 'JWT ' + this.generateToken(userInfo) });
    })(req, res, next);
  };

  authenticate = (req: Request, res: Response, next: NextFunction) => {
    const authenticate = passport.authenticate('jwt', { session: false });
    authenticate(req, res, next);
  };

  forgot = (req: Request, res: Response, next: NextFunction) => {
    const email: string = req.body.email;
    const host: string = req.header('host');

    if (!email) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid email address supplied.' });
    }

    if (!host) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No host could be determined from the request.' });
    }

    this.authenticationService
      .forgotPassword(email, host)
      .then((user) => {
        const message = `
        <p>An e-mail has been sent to your email address '${user.email}'.</p> 
        <p>Please check your email to reset your password.</p>`;
        return res.status(codes.OK).json(message);
      })
      .catch((err) => next(err));
  };

  verifyResetToken = (req: Request, res: Response, next: NextFunction) => {
    const token = req.params.token;

    if (!token) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid request' });
    }

    this.userService
      .findOne({
        resetPasswordToken: token,
        resetPasswordExpires: { $gt: Date.now() },
      })
      .then((user) => {
        if (!user) {
          return res.status(codes.BAD_REQUEST).send({
            message: 'Unable to reset password. The password reset token is invalid or has expired',
          });
        }

        return res.status(codes.OK).send(true);
      })
      .catch((err) => next(err));
  };

  reset = (req: Request, res: Response, next: NextFunction) => {
    const token = req.params.token;
    const password = req.body.password;

    if (!token) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Password reset token is invalid or has expired.' });
    }

    if (!password) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Password not supplied.' });
    }

    this.userService
      .findOne({
        resetPasswordToken: token,
        resetPasswordExpires: { $gt: Date.now() },
      })
      .then((user) => {
        if (!user) {
          return res.status(codes.BAD_REQUEST).send({
            message: 'Password reset token is invalid or has expired.',
          });
        }

        this.authenticationService
          .changePassword(user, password)
          .then((user) => {
            const message = `
        <p>Your password has been changed.</p> 
        <p>Please login with your new password.</p>`;
            return res.status(codes.OK).json(message);
          })
          .catch((err) => next(err));
      })
      .catch((err) => next(err));
  };

  change = (req: Request, res: Response, next: NextFunction) => {
    const password = req.body.password;
    const user = req.user as IUser;

    if (!password) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Password not supplied.' });
    }

    this.authenticationService
      .changePassword(user, password)
      .then((user) => {
        const userInfo = this.setUserInfo(user);
        this.auditService.audit(user, AuditActions.ChangePassword, AuditTargets.User, AuditResults.Success);
        res.status(codes.OK).json({ token: 'JWT ' + this.generateToken(userInfo) });
      })
      .catch((err) => next(err));
  };
}
