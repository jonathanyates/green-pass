import '../../shared/extensions/date.extensions';
import { CompanyCompliance } from '../../shared/models/company-compliance.model';
import {
  ICompanyCompliance,
  ICompanyDriverCompliance,
  ICompanyVehicleCompliance,
  ICompliance,
} from '../../shared/interfaces/compliance.interface';

describe('VehicleCompliance', function () {
  describe('create', function () {
    it('expect company to be 10% compliant with 0% driver and vehicle compliance but with company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 0 },
        driver: <ICompanyDriverCompliance>{ compliant: 0 },
        company: <ICompanyCompliance>{ insurance: true },
      });

      expect(companyCompliance.compliant).toEqual(10);
    });

    it('expect company to be 90% compliant with 100% driver and vehicle compliance but no company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 100 },
        driver: <ICompanyDriverCompliance>{ compliant: 100 },
        company: <ICompanyCompliance>{ insurance: false },
      });

      expect(companyCompliance.compliant).toEqual(90);
    });

    it('expect company to be 45% compliant with 50% driver and vehicle compliance but no company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 50 },
        driver: <ICompanyDriverCompliance>{ compliant: 50 },
        company: <ICompanyCompliance>{ insurance: false },
      });

      expect(companyCompliance.compliant).toEqual(45);
    });

    it('expect company to be 33.75% compliant with 25% driver and 50% vehicle compliance but no company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 25 },
        driver: <ICompanyDriverCompliance>{ compliant: 50 },
        company: <ICompanyCompliance>{ insurance: false },
      });

      expect(companyCompliance.compliant).toEqual(33.75);
    });

    it('expect company to be 100% compliant with 100% driver and vehicle compliance and company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 100 },
        driver: <ICompanyDriverCompliance>{ compliant: 100 },
        company: <ICompanyCompliance>{ insurance: true },
      });

      expect(companyCompliance.compliant).toEqual(100);
    });

    it('expect company to be 50% compliant with 50% driver and vehicle compliance and company insurance', function () {
      const companyCompliance = CompanyCompliance.create(<ICompliance>{
        vehicle: <ICompanyVehicleCompliance>{ compliant: 50 },
        driver: <ICompanyDriverCompliance>{ compliant: 50 },
        company: <ICompanyCompliance>{ insurance: true },
      });

      expect(companyCompliance.compliant).toEqual(55);
    });
  });
});
