import {Request, Response, NextFunction} from "express-serve-static-core";

export interface IApiController {

  getAll(req:Request, res:Response, next: NextFunction);

  get(req:Request, res:Response, next: NextFunction);

  add(req:Request, res:Response, next: NextFunction);

  update(req:Request, res:Response, next: NextFunction);

  remove(req: Request, res: Response, next: NextFunction);
}