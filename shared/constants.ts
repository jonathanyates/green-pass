import * as regex from './utils/regex';

export const Roles = {
  Admin: 'admin',
  Company: 'company',
  Driver: 'driver',
};
export const RegExpPatterns = regex.RegExpPatterns;
