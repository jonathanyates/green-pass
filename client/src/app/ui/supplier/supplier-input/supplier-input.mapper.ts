import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {AddressInputMapper} from "../../shared/components/address-input/address-input.mapper";
import {Supplier} from "../../company/company.model";
import {Injectable} from "@angular/core";
import {RegExpPatterns} from "../../../../../../shared/constants";
import {ISupplier} from "../../../../../../shared/interfaces/company.interface";

@Injectable()
export class SupplierInputMapper {

  constructor(private addressInputMapper: AddressInputMapper) {}

  mapFrom(form:FormGroup): ISupplier {
    return <ISupplier>{
      name: form.controls['name'].value,
      phone: form.controls['phone'].value,
      mobile: form.controls['mobile'].value,
      email: form.controls['email'].value,
      address: this.addressInputMapper.mapFrom(form)
    };
  }

  mapTo(supplier:Supplier): FormInputGroup[] {
    return [
      {
        label: 'Supplier',
        inputs: [
          { id: 'name', label: 'Name', type:'text', required: true, autoFocus: true, value: supplier.name },
          { id: 'phone', label: 'Phone number', type:'text', required: true,
            regex: RegExpPatterns.Phone, value: supplier.phone },
          { id: 'mobile', label: 'Mobile number', type:'text', required: false,
            regex: RegExpPatterns.Mobile, value: supplier.mobile },
          { id: 'email', label: 'Email', type:'text', required: false,
            regex: RegExpPatterns.Email, value: supplier.email },
        ]
      },
      this.addressInputMapper.mapTo(supplier.address)

    ];
  }

}
