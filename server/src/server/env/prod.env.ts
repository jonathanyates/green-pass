import { IServerConfig } from '../server.config';
import { v4 } from 'node-uuid';

// PRODUCTION Config
const prodServerConfig: IServerConfig = {
  db: process.env.MONGODB
    ? process.env.MONGODB
    : 'mongodb+srv://greenpass-admin:qYhpukC2NzeykR8Y@prod-cluster.xcq3ftj.mongodb.net/?retryWrites=true&w=majority',
  authentication: {
    secret: v4(),
  },
  dvla: {
    uri: 'https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles',
    apiKey: 'nvJDu2tY5878ocLnRMP4W44BrfGvbFoq3EQKlSvB',
  },
  dvsa: {
    mot: {
      uri: 'https://beta.check-mot.service.gov.uk/trade/vehicles/mot-tests',
      apiKey: 'uYF4Nky6hq3Rg1EjYvT0s4KusFsc244P4TAUNCiG',
    },
    hgvTest: {
      uri: 'https://beta.check-mot.service.gov.uk/trade/vehicles/annual-tests',
      apiKey: 'uYF4Nky6hq3Rg1EjYvT0s4KusFsc244P4TAUNCiG',
    },
  },
  legalesign: {
    apiKey: 'ApiKey 836e21d5-4cb2-4627-a67d-1254d15735ea:7dbc58a4e9309a6e878aa983eacb8404b66de9a7',
    baseUrl: 'https://eu-api.legalesign.com/api/v1/',
    group: '/api/v1/group/9b659a47-8146-11ed-955f-06d903d83b8a/',
    email: 'jonathan.yates@greenpasscompliance.co.uk',
    user: 'adda5bf1-fd2c-43b0-93e4-2c2980167e0b',
  },
  roadMarque: {
    url: 'https://www.roadmarque.com/roadmarque/external/RMIntegration.asmx?wsdl',
    dllUrl: 'https://www.roadmarque.com/roadmarque/Webservices/RMDLLService.svc',
    username: 'rmjonyates',
    password: 'Everest99',
    customerId: '223',
    resellerId: '58217',
    dllPassword: 'hl!rzGoo*#',
    driverGroup: 'Standard',
    updateAssessments: true,
  },
  client: {
    baseUrl: process.env.BASE_URL ? process.env.BASE_URL : 'https://app.greenpasscompliance.co.uk',
  },
  server: {
    apiRoot: process.env.BASE_URL ? process.env.BASE_URL + '/api' : 'https://app.greenpasscompliance.co.uk/api',
    path: 'server', // server path needs to be server when running from dev/demo/prod servers
  },
  email: {
    sendEmails: true,
  },
  loggly: {
    enabled: true,
    token: 'd6f2e8e9-09ab-4653-b846-1550f8500463',
    subdomain: 'greenpass',
  },
  rabbitmq: {
    url:
      process.env.RABBITMQ_USER && process.env.RABBITMQ_PASSWORD && process.env.RABBITMQ_URL
        ? `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_URL}`
        : 'amqp://rabbitmq',
  },
};

export default prodServerConfig;
