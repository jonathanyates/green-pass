import { NgModule } from '@angular/core';
import { CompanyComponent } from './company.component';
import {CompanyRoutingModule} from "./company-routing.module";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {CompanyListComponent} from "./company-list/company-list.component";
import {CompanyInputComponent} from "./company-input/company-input.component";
import {UserModule} from "../user/user.module";
import {VehicleModule} from "../vehicle/vehicle.module";
import {CompanyInputMapper} from "./company-input/company-input.mapper";
import {CompanyBreadcrumbRouting} from "./company-breadcrumb.routing";
import {CompanyDashboardComponent} from "./company-dashboard/company-dashboard.component";
import {CompanyDetailsComponent} from "./company-details/company-details.component";
import {SupplierInputComponent} from "../supplier/supplier-input/supplier-input.component";
import {SupplierListComponent} from "../supplier/supplier-list/supplier-list.component";
import {DepotInputComponent} from "../depot/depot-input/depot-input.component";
import {DepotListComponent} from "../depot/depot-list/depot-list.component";
import {SupplierInputMapper} from "../supplier/supplier-input/supplier-input.mapper";
import {DepotInputMapper} from "../depot/depot-input/depot-input.mapper";
import {CompanyInsuranceComponent} from "./company-insurance/company-insurance-detail/company-insurance.component";
import {CompanyInsuranceInputComponent} from "./company-insurance/company-insurance-input/company-insurance-input.component";
import {CompanyDetailsTabListComponent} from "./company-details-tab-list/company-details-tab-list.component";
import {CompanySettingsInputComponent} from "./company-settings/company-settings-input.component";
import {SubscriptionDetailsComponent} from "./subscription-details/subscription-details.component";
import {SubscriptionInputMapper} from "./subscription-input/subscription-input.mapper";
import {SubscriptionInputComponent} from "./subscription-input/subscription-input.component";
import {CompanySettingsComponent} from "./company-settings/company-settings.component";
import {CompanySettingsInputContainerComponent} from "./company-settings/company-settings-input-container.component";
import {DriversComplianceComponent} from "./compliance/drivers-compliance.component";
import {VehiclesComplianceComponent} from "./compliance/vehicles-compliance.component";
import {CompanyComplianceComponent} from "./compliance/company-compliance.component";

@NgModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      CommonModule,
      SharedModule,
      UserModule,
      VehicleModule,
      CompanyRoutingModule
    ],
    exports: [],
    declarations: [
      CompanyComponent,
      CompanyDetailsComponent,
      CompanyDetailsTabListComponent,
      CompanyListComponent,
      CompanyDashboardComponent,
      CompanyInputComponent,

      DepotListComponent,
      DepotInputComponent,
      SupplierListComponent,
      SupplierInputComponent,

      CompanyInsuranceComponent,
      CompanyInsuranceInputComponent,

      CompanySettingsComponent,
      CompanySettingsInputComponent,
      CompanySettingsInputContainerComponent,
      SubscriptionDetailsComponent,
      SubscriptionInputComponent,
      DriversComplianceComponent,
      VehiclesComplianceComponent,
      CompanyComplianceComponent
    ],
    providers: [
      CompanyInputMapper,
      CompanyBreadcrumbRouting,
      DepotInputMapper,
      SupplierInputMapper,
      SubscriptionInputMapper
    ],
})
export class CompanyModule { }
