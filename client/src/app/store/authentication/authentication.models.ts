export interface LoginDetails {
  username: string,
  password: string
}

export interface ErrorInfo {
  errorMessage: string;
  errorFields?: string[];
}
