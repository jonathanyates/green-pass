import { NgModule } from '@angular/core';
import {TemplateListComponent} from "./template-list/template-list.component";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {TemplateRoutingModule} from "./template-routing.module";
import {TemplateComponent} from "./template/template.component";
import {TemplateContainerComponent} from "./template/template-container.component";
import {TemplateInputComponent} from "./template-input/template-input.component";
import {TemplateInputContainerComponent} from "./template-input/template-input-container.component";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    TemplateRoutingModule
  ],
  exports: [],
  declarations: [
    TemplateComponent,
    TemplateContainerComponent,
    TemplateInputComponent,
    TemplateInputContainerComponent,
    TemplateListComponent
  ],
  providers: [],
})
export class TemplateModule { }
