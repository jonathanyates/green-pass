import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import codes from '../shared/error.codes';
import { IAuditService } from './audit.service';

export interface IAuditController {
  getAll(req: Request, res: Response, next: NextFunction);
  get(req: Request, res: Response, next: NextFunction);
  find(req: Request, res: Response, next: NextFunction);
  add(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class AuditController implements IAuditController {
  constructor(@inject(TYPES.services.AuditService) private auditService: IAuditService) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    this.auditService
      .getAll(companyId)
      .then((companies) => {
        if (!companies) {
          return res.status(codes.NOT_FOUND);
        }
        return res.status(codes.OK).json(companies);
      })
      .catch((err) => {
        next(err);
      });
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.auditId;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid audit id.' });

    this.auditService
      .get(id)
      .then((audit) => {
        if (!audit) {
          return res.status(codes.NOT_FOUND).send({ message: 'Audit not found.' });
        }

        return res.status(codes.OK).json(audit);
      })
      .catch((err) => {
        next(err);
      });
  };

  find(req: Request, res: Response, next: NextFunction) {
    const criteria = {};

    if (req.query) {
      for (const param in req.query) {
        if (req.query.hasOwnProperty(param)) {
          criteria[param] = req.query[param];
        }
      }
    }

    this.auditService
      .find(criteria)
      .then((audits) => res.status(codes.OK).json(audits))
      .catch((err) => next(err));
  }

  add = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const audit = req.body;

    if (!mongoose.Types.ObjectId.isValid(companyId))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    if (!audit) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No audit specified.' });
    }

    audit._companyId = companyId;

    this.auditService
      .add(audit)
      .then((audit) => {
        return res.status(codes.CREATED).json(audit);
      })
      .catch((err) => {
        next(err);
      });
  };
}
