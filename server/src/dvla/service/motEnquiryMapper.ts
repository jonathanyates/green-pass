import { IReasonforRejection, IVehicle, IVehicleMot, RfrType } from '../../../../shared/interfaces/vehicle.interface';
import { is3YearsOld } from '../../vehicles/vehicle.utils';
import { IMotEnquiry, IMotTest } from '../interfaces/dvsa-mot-enquiry.interface';

// https://www.gov.uk/government/news/mot-changes-20-may-2018
const adviceMap = new Map<RfrType, string>([
  [RfrType.PASS, 'Make sure it continues to meet the standard'],
  [RfrType.PRS, 'Repair immediately'],
  [RfrType.ADVISORY, 'Monitor and repair if necessary'],
  [RfrType.MINOR, 'Repair as soon as possible'],
  [RfrType.MAJOR, 'Repair it immediately'],
  [RfrType.DANGEROUS, 'Do not drive the vehicle until it’s been repaired'],
  [RfrType.USER_ENTERED, 'User Entered Advice Noticies'],
  [RfrType.FAIL, 'Reason(s) for failure'],
]);

export const mapMotEnquiryToVehicle = (motEnquiry: IMotEnquiry, vehicle: IVehicle) => {
  if (!motEnquiry) {
    return vehicle;
  }

  vehicle.model = motEnquiry.model;
  vehicle.dateOfFirstRegistration = motEnquiry.registrationDate ? new Date(motEnquiry.registrationDate) : null;
  vehicle.manufactureDate = motEnquiry.manufactureDate ? new Date(motEnquiry.manufactureDate) : null;
  vehicle.firstUsedDate = motEnquiry.firstUsedDate ? new Date(motEnquiry.firstUsedDate) : null;
  vehicle.motDueDate = motEnquiry.motTestDueDate ? new Date(motEnquiry.motTestDueDate) : null;

  if (motEnquiry.motTests && motEnquiry.motTests.length > 0) {
    vehicle.motHistory = motTestMapper(motEnquiry.motTests);
    vehicle.annualMileage = getAnnualMileage(vehicle);

    const latestMotTest = vehicle.motHistory.find((item) => item.hasOwnProperty('expiryDate'));
    vehicle.motDueDate = latestMotTest && latestMotTest.expiryDate;
  }

  // if the vehicle is less than 3 years old then set the mot due date to 3 years from vehicle registration
  if (!vehicle.motDueDate && vehicle.dateOfFirstRegistration) {
    const isVehicle3YearsOld = is3YearsOld(vehicle.dateOfFirstRegistration);
    if (!isVehicle3YearsOld) {
      vehicle.motDueDate = new Date(
        vehicle.dateOfFirstRegistration.getFullYear() + 3,
        vehicle.dateOfFirstRegistration.getMonth(),
        vehicle.dateOfFirstRegistration.getDate()
      );
    }
  }

  return vehicle;
};

export const motTestMapper = (motTests: IMotTest[]): IVehicleMot[] => {
  const motHistory = motTests.map((motTest) => {
    const reasonsForRejection: Array<IReasonforRejection> = motTest.rfrAndComments.map((rfr) => {
      const rfrType = rfr.type as RfrType;
      const advice = rfr.dangerous ? adviceMap.get(RfrType.DANGEROUS) : adviceMap.get(rfrType);

      return {
        text: rfr.text,
        type: rfrType,
        dangerous: rfr.dangerous,
        advice,
      };
    });

    const vehicleMot: IVehicleMot = {
      testDate: motTest.completedDate ? new Date(motTest.completedDate) : null,
      expiryDate: motTest.expiryDate ? new Date(motTest.expiryDate) : null,
      testResult: motTest.testResult,
      odometerReading: motTest.odometerValue ? Number(motTest.odometerValue) : null,
      odometerUnit: motTest.odometerUnit,
      motTestNumber: motTest.motTestNumber,
      reasonsForFailure: [],
      advisoryNotices: [],
      reasonsForRejection,
    };
    return vehicleMot;
  });

  return motHistory;
};

const getAnnualMileage = (vehicle: IVehicle): number => {
  // can only use mot history if we have at least 2 mots
  if (!vehicle.motHistory || vehicle.motHistory.length < 2) {
    return vehicle.annualMileage;
  }

  const mots = vehicle.motHistory
    .filter((mot) => mot.testResult === 'Pass')
    .sort((a, b) => b.testDate.getTime() - a.testDate.getTime());

  if (mots.length < 2) {
    return vehicle.annualMileage;
  }

  const latestMileage = mots[0].odometerReading;
  const previousMileage = mots[1].odometerReading;
  let mileage = latestMileage - previousMileage;

  // ensure the mileage is for 12 months
  const months = mots[1].testDate.getMonthDiff(mots[0].testDate);
  const monthlyMileage = mileage / months;
  mileage = Math.round(monthlyMileage * 12);

  return mileage;
};
