import mongoose from 'mongoose';
import {injectable, inject} from "inversify";
import {IRepository} from "../db/db.repository";
import {TYPES} from "../container/container.types";
import {IApiService} from "../interfaces/api-service.interface";
import {ICompanyModel} from "../companies/company.schema";
import {ServerError} from "../shared/errors";
import codes from "../shared/error.codes";
import {IReportSchedule} from "../../../shared/interfaces/report.interface";
import {IMessageService} from "../messaging/message.service";
import {queueNames} from "../messaging/messages.constants";
import {ICompanyRepository} from "../companies/company.repository";

@injectable()
export class ReportScheduleService implements IApiService<IReportSchedule> {

  constructor(@inject(TYPES.repositories.ReportScheduleRepository) private reportScheduleRepository: IRepository<IReportSchedule>,
              @inject(TYPES.services.MessageService) private messageService: IMessageService,
              @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository) {
  }

  getAll = (companyId: string): Promise<IReportSchedule[]> => {

    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.reportScheduleRepository.find({_companyId: companyId});
  };

  get = (id: string): Promise<IReportSchedule> => {
    return this.reportScheduleRepository.get(id);
  };

  find(criteria: any): Promise<IReportSchedule[]> {
    return this.reportScheduleRepository.find(criteria);
  }

  findOne(criteria: any): Promise<IReportSchedule> {
    return this.reportScheduleRepository.findOne(criteria);
  }

  add = (companyId: string, reportSchedule: IReportSchedule): Promise<IReportSchedule> => {

    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.companyRepository.get(companyId)
      .then((company: ICompanyModel) => {
        if (!company) {
          throw ServerError('Company not found.', codes.NOT_FOUND);
        }
        return this.reportScheduleRepository.add(reportSchedule)
          .then(schedule => {
            return this.messageService.send(queueNames.reportSchedules, schedule)
              .then(sent => schedule)
              .catch(err => schedule);
          })
      });
  };

  update = (reportSchedule: IReportSchedule): Promise<IReportSchedule> => {
    return this.reportScheduleRepository.update(reportSchedule)
      .then(schedule => {
        return this.messageService.send(queueNames.reportSchedules, schedule)
          .then(sent => schedule)
          .catch(err => schedule);
      })
  };

}