import {Component, Input} from '@angular/core';

@Component({
  selector: 'gp-image-preview',
  template: `
    <div *ngIf="url">
      <img [src]="url | safe" class="w-100">
    </div>
    <div *ngIf="message">{{message}}</div>
  `
})
export class ImagePreviewComponent {

  url: any;
  message: string;

  @Input() set file(file) {
    this.readFile(file);
  }

  readFile = file => {
    if (file == null) {
      this.url = null;
      return;
    }

    try {
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      };

      reader.onerror = (err:any) => {
        console.log(`Error occured trying to read file ${file.name}. Error: ${err.message}`);
        this.message = 'Invalid file. Please select a valid file.'
      };

      reader.readAsDataURL(file);
    } catch(err) {
      console.log(`Error occured trying to read file ${file.name}. Error: ${err.message}`);
      this.message = 'Invalid file. Please select a valid file.'
    }
  }
}
