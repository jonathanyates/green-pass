import mongoose from 'mongoose';
import { IDriverService } from './driver.service';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiController } from '../interfaces/api-controller.interface';
import codes from '../shared/error.codes';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { IRoadMarqueService } from '../assessments/roadmarque.service';
import { IAssessmentService } from '../assessments/assessment.service';
import { AuditActions, AuditResults, AuditTargets, IAuditService } from '../audit/audit.service';
import { jsonDeserialiser } from '../../../shared/utils/json.deserialiser';
import { IDriverImport } from '../../../shared/interfaces/driver.interface';

export interface IDriverController extends IApiController {
  getDrivingLicenceCheckCode(req: Request, res: Response, next: NextFunction);
  drivingLicenceCheck(req: Request, res: Response, next: NextFunction);
  updateDriverDocuments(req: Request, res: Response, next: NextFunction);
  assessmentLogin(req: Request, res: Response, next: NextFunction);
  updateAssessments(req: Request, res: Response, next: NextFunction);
  updateOnRoadAssessment(req: Request, res: Response, next: NextFunction);
  addOnRoadAssessment(req: Request, res: Response, next: NextFunction);

  addInsurance(req, res: Response, next: NextFunction);
  updateInsurance(req: Request, res: Response, next: NextFunction);
  deleteInsurance(req: Request, res: Response, next: NextFunction);

  addFawCertificate(req, res: Response, next: NextFunction);
  updateFawCertificate(req: Request, res: Response, next: NextFunction);
  deleteFawCertificate(req: Request, res: Response, next: NextFunction);

  addVehicle(req: Request, res: Response, next: NextFunction);
  removeVehicle(req: Request, res: Response, next: NextFunction);

  importDrivers(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class DriverController implements IDriverController {
  constructor(
    @inject(TYPES.services.DriverService) private driverService: IDriverService,
    @inject(TYPES.services.RoadMarqueService) private roadMarqueService: IRoadMarqueService,
    @inject(TYPES.services.AssessmentService) private assessmentService: IAssessmentService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {
    const criteria = {
      _companyId: new mongoose.Types.ObjectId(req.params.id),
    };

    let page, pageSize: number;
    let pagination = false;

    if (req.query) {
      pagination = req.query.page != null;

      if (pagination) {
        page = +req.query.page;
        pageSize = +req.query.pageSize;

        delete req.query.page;
        delete req.query.pageSize;

        for (const param in req.query) {
          if (req.query.hasOwnProperty(param)) {
            const pattern = req.query[param] as string;
            criteria[param] = { $regex: new RegExp(pattern, 'i') };
          }
        }

        this.driverService
          .findByPage(criteria, page, pageSize)
          .then((drivers) => res.status(codes.OK).json(drivers))
          .catch((err) => next(err));

        return;
      } else {
        for (const param in req.query) {
          if (req.query.hasOwnProperty(param)) {
            criteria[param] = req.query[param];
          }
        }
      }
    }

    this.driverService
      .find(criteria)
      .then((driver) => res.status(codes.OK).json(driver))
      .catch((err) => next(err));
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    return this.driverService
      .get(driverId)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: 'Driver not found.' });
        }

        return res.status(codes.OK).json(driver);
      })
      .catch((err) => {
        next(err);
      });
  };

  add = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driver = jsonDeserialiser.parseDates(req.body);

    if (!driver) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver specified.' });
    }

    const user = driver._user;

    if (!user) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No user specified for driver.' });
    }

    this.driverService
      .add(companyId, driver)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Driver, driver);
        return res.status(codes.CREATED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Add, AuditTargets.Driver, err);
        next(err);
      });
  };

  importDrivers = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const drivers: IDriverImport[] = req.body;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!drivers) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No drivers specified.' });
    }

    this.driverService
      .importDrivers(companyId, drivers)
      .then((drivers) => {
        this.auditService.audit(req.user, AuditActions.Import, AuditTargets.Drivers, drivers);
        return res.status(codes.CREATED).json(drivers);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Import, AuditTargets.Drivers, err);
        next(err);
      });
  };

  update = (req: Request, res: Response, next: NextFunction) => {
    const driver = jsonDeserialiser.parseDates(req.body);

    if (!driver) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver specified.' });
    }

    this.driverService
      .update(driver)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Driver, driver);
        res.status(codes.ACCEPTED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.Driver, err);
        next(err);
      });
  };

  remove = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver id specified.' });
    }

    this.driverService
      .remove(driverId)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.Driver, driver);
        res.status(codes.ACCEPTED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Remove, AuditTargets.Driver, err);
        next(err);
      });
  };

  getDrivingLicenceCheckCode = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.driverId;

    if (!id) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver specified.' });
    }

    this.driverService
      .get(id)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: 'Driver not found.' });
        }

        const licenceNumber = req.query.licenceNumber as string;
        const niNumber = req.query.niNumber as string;
        const postcode = req.query.postcode as string;

        if (!licenceNumber) {
          return res.status(codes.BAD_REQUEST).send({ message: 'No Drivers Licence Number Supplied.' });
        }

        if (!niNumber) {
          return res.status(codes.BAD_REQUEST).send({ message: 'No Drivers National Insurance Number Supplied.' });
        }

        if (!postcode) {
          return res.status(codes.BAD_REQUEST).send({ message: 'No Drivers Post Code Supplied.' });
        }

        this.driverService
          .getDrivingLicenceCheckCode(licenceNumber, niNumber, postcode)
          .then((checkCode) => {
            this.auditService.audit(req.user, AuditActions.Get, AuditTargets.CheckCode, checkCode);
            res.status(codes.OK).json(checkCode);
          })
          .catch((err) => {
            this.auditService.auditError(req.user, AuditActions.Get, AuditTargets.CheckCode, err);
            next(err);
          });
      })
      .catch((err) => {
        next(err);
      });
  };

  drivingLicenceCheck = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.driverId;
    const checkCode = req.params.checkCode;

    if (!id) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver specified.' });
    }

    if (!checkCode) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check code specified.' });
    }

    this.driverService
      .get(id)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: 'Driver not found.' });
        }

        const queryLicenceNumber = req.query.licenceNumber as string;
        const licenceNumber = queryLicenceNumber ? queryLicenceNumber : driver.licenceNumber;

        if (!licenceNumber) {
          return res.status(codes.BAD_REQUEST).send({ message: 'No Drivers Licence Number Supplied.' });
        }

        this.driverService
          .drivingLicenceCheck(driver, checkCode, licenceNumber)
          .then((driver) => {
            this.auditService.audit(req.user, AuditActions.Get, AuditTargets.DrivingLicence, driver.licence);
            res.status(codes.OK).json(driver);
          })
          .catch((err) => {
            this.auditService.auditError(req.user, AuditActions.Get, AuditTargets.DrivingLicence, err);
            next(err);
          });
      })
      .catch((err) => {
        next(err);
      });
  };

  updateDriverDocuments = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.driverId;

    return this.driverService
      .updateDriverDocuments(id)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: 'Driver not found.' });
        }

        return res.status(codes.OK).json(driver);
      })
      .catch((err) => {
        next(err);
      });
  };

  assessmentLogin = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.driverId;

    if (!id) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver specified.' });
    }

    this.driverService
      .get(id)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: 'Driver not found.' });
        }

        this.roadMarqueService
          .doLogin(driver._id.toString())
          .then((login) => {
            // update last login date
            const lastAssessment =
              driver.assessments && driver.assessments.length > 0 ? driver.assessments.slice(-1)[0] : null;

            const lastAssessmentLoginDate = new Date();

            driver.lastAssessmentLoginDate = lastAssessmentLoginDate;

            if (lastAssessment) {
              lastAssessment.lastLoginDate = lastAssessmentLoginDate;
            }

            return this.driverService.update(driver).then((driver) => login);
          })
          .then((login) => {
            this.auditService.audit(req.user, AuditActions.Login, AuditTargets.Assessment, AuditResults.Success);
            res.status(codes.OK).json(login);
          })
          .catch((err) => {
            this.auditService.auditError(req.user, AuditActions.Login, AuditTargets.Assessment, err);
            next(err);
          });
      })
      .catch((err) => {
        next(err);
      });
  };

  updateAssessments = (req: Request, res: Response, next: NextFunction) => {
    const companyId: string = req.params.id;
    const driverId: string = req.params.driverId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    return this.driverService
      .get(driverId)
      .then((driver) => {
        if (!driver) {
          return res.status(codes.NOT_FOUND).send({ message: `Driver ${driverId} not found.` });
        }

        return this.assessmentService
          .updateAssessments(driver)
          .then((driver) => {
            return res.status(codes.OK).json(driver);
          })
          .catch((err) => {
            next(err);
          });
      })
      .catch((err) => {
        next(err);
      });
  };

  updateOnRoadAssessment = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;
    const assessmentId = req.params.assessmentId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!assessmentId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No assessment Id specified.' });
    }

    if (!req.body.assessmentDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No assessment date specified.' });
    }

    if (!req.body.assessedBy) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No assessed By field specified.' });
    }

    if (!req.body.result) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No result field specified.' });
    }

    const assessment = {
      assessmentDate: req.body.assessmentDate ? new Date(req.body.assessmentDate) : null,
      assessedBy: req.body.assessedBy,
      result: req.body.result,
      complete: req.body.complete == 'true',
      file: req.file,
      fileType: req.body.fileType,
    };

    this.driverService
      .updateOnRoadAssessment(companyId, driverId, assessmentId, assessment)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Assessment, driver.assessments);
        res.status(codes.OK).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.Assessment, err);
        next(err);
      });
  };

  addOnRoadAssessment = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!req.body.inviteDate) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No invite date specified.' });
    }

    const assessment = {
      inviteDate: req.body.inviteDate ? new Date(req.body.inviteDate) : null,
      assessedBy: req.body.assessedBy,
      complete: false,
    };

    this.driverService
      .addOnRoadAssessment(companyId, driverId, assessment)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Assessment, driver.assessments);
        res.status(codes.CREATED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Add, AuditTargets.Assessment, err);
        next(err);
      });
  };

  // Driver Insurance

  addInsurance = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid From specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid To field specified.' });
    }

    const insurance = {
      validFrom: req.body.validFrom ? new Date(req.body.validFrom) : null,
      validTo: req.body.validTo ? new Date(req.body.validTo) : null,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.driverService
      .addInsurance(companyId, driverId, insurance)
      .then((insurance) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.DriverInsurance, insurance);
        res.status(codes.CREATED).json(insurance);
      })
      .catch((err) => {
        next(err);
      });
  };

  updateInsurance = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;
    const insuranceId = req.params.insuranceId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!insuranceId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid from specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid to field specified.' });
    }

    const insurance = {
      validFrom: req.body.validFrom ? new Date(req.body.validFrom) : null,
      validTo: req.body.validTo ? new Date(req.body.validTo) : null,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.driverService
      .updateInsurance(companyId, driverId, insuranceId, insurance)
      .then((insurance) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.DriverInsurance, insurance);
        res.status(codes.CREATED).json(insurance);
      })
      .catch((err) => next(err));
  };

  deleteInsurance = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;
    const insuranceId = req.params.insuranceId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!insuranceId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    this.driverService
      .deleteInsurance(companyId, driverId, insuranceId)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.DriverInsurance, driver.insuranceHistory);
        res.status(codes.CREATED).json(driver);
      })
      .catch((err) => next(err));
  };

  // FAW

  addFawCertificate = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid From specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid To field specified.' });
    }

    const faw = {
      validFrom: req.body.validFrom ? new Date(req.body.validFrom) : null,
      validTo: req.body.validTo ? new Date(req.body.validTo) : null,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.driverService
      .addFawCertificate(companyId, driverId, faw)
      .then((faw) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.FawCertificate, faw);
        res.status(codes.CREATED).json(faw);
      })
      .catch((err) => next(err));
  };

  updateFawCertificate = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;
    const fawId = req.params.fawId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!fawId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid from specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid to field specified.' });
    }

    const faw = {
      validFrom: req.body.validFrom ? new Date(req.body.validFrom) : null,
      validTo: req.body.validTo ? new Date(req.body.validTo) : null,
      file: req.file,
      fileType: req.body.fileType,
    };

    this.driverService
      .updateFawCertificate(companyId, driverId, fawId, faw)
      .then((faw) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.FawCertificate, faw);
        res.status(codes.CREATED).json(faw);
      })
      .catch((err) => next(err));
  };

  deleteFawCertificate = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const driverId = req.params.driverId;
    const fawId = req.params.fawId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!fawId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    this.driverService
      .deleteFawCertificate(companyId, driverId, fawId)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.FawCertificate, driver.fawHistory);
        res.status(codes.CREATED).json(driver);
      })
      .catch((err) => next(err));
  };

  addVehicle = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;
    const vehicleId = req.params.vehicleId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicleId Id specified.' });
    }

    this.driverService
      .addVehicle(driverId, vehicleId)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.DriverVehicle, driver);
        res.status(codes.ACCEPTED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.DriverVehicle, err);
        next(err);
      });
  };

  removeVehicle = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;
    const vehicleId = req.params.vehicleId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No driver Id specified.' });
    }

    if (!vehicleId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No vehicleId Id specified.' });
    }

    this.driverService
      .removeVehicle(driverId, vehicleId)
      .then((driver) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.DriverVehicle, driver);
        res.status(codes.ACCEPTED).json(driver);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.DriverVehicle, err);
        next(err);
      });
  };
}
