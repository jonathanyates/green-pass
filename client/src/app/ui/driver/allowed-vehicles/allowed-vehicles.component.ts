import {Component, Input, OnInit} from '@angular/core';
import {IAllowedVehicle, IAllowedVehicles} from "../../../../../../shared/interfaces/driver.interface";

@Component({
  selector: 'gp-allowed-vehicles',
  templateUrl: './allowed-vehicles.component.html',
  styleUrls: ['./allowed-vehicles.component.css']
})
export class AllowedVehiclesComponent implements OnInit {

  @Input() allowedVehicles: IAllowedVehicles;

  constructor() { }

  ngOnInit() {
  }

  hasRestrictions(allowedVehicle: IAllowedVehicle) {
    return allowedVehicle.restrictions && allowedVehicle.restrictions.length > 0
  }

}
