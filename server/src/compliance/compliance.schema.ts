import mongoose from 'mongoose';
import { ICompliance } from '../../../shared/interfaces/compliance.interface';

export const CompanyComplianceSchema = new mongoose.Schema({
  compliant: { type: Boolean, required: true },
  insurance: { type: Boolean, required: false },
});

export const CompanyDriverComplianceSchema = new mongoose.Schema({
  compliant: { type: Number, required: false },
  details: { type: Number, required: false },
  documents: { type: Number, required: false },
  nextOfKin: { type: Number, required: false },
  references: { type: Number, required: false },
  licence: { type: Number, required: false },
  assessments: { type: Number, required: false },
  insurance: { type: Number, required: false },
});

export const CompanyVehicleComplianceSchema = new mongoose.Schema({
  compliant: { type: Number, required: false },
  tax: { type: Number, required: false },
  mot: { type: Number, required: false },
  service: { type: Number, required: false },
  inspection: { type: Number, required: false },
  checks: { type: Number, required: false },
  insurance: { type: Number, required: false },
});

export const ComplianceSchema = new mongoose.Schema({
  _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true },
  compliant: { type: Number, required: false },
  vehicle: { type: CompanyVehicleComplianceSchema, required: false },
  driver: { type: CompanyDriverComplianceSchema, required: false },
  company: { type: CompanyComplianceSchema, required: false },
});

export interface IComplianceModel extends mongoose.Document, ICompliance {
  _id: any;
}

export const DriverComplianceSchema = new mongoose.Schema({
  compliant: { type: Boolean, required: true },
  details: { type: Boolean, required: true },
  nextOfKin: { type: Boolean, required: true },
  references: { type: Boolean, required: true },
  documents: {
    compliant: { type: Boolean, required: true },
    completed: { type: Number, required: false },
    total: { type: Number, required: false },
    percentage: { type: Number, required: true },
  },
  drivingLicence: {
    compliant: { type: Boolean, required: true },
    checkRequired: { type: Boolean, required: true },
    valid: { type: Boolean, required: false },
    points: { type: Number, required: false },
  },
  assessments: {
    compliant: { type: Boolean, required: true },
    onLine: { type: String, required: true }, // Low ok, medium refer to modules, high refer to onRoad
    modules: { type: String, required: false }, // Low or medium ok, high refer to onRoad
    onRoad: { type: Boolean, required: false }, // must be completed if required
  },
  insurance: { type: Boolean, required: false },
});

export const VehicleComplianceSchema = new mongoose.Schema({
  compliant: { type: Boolean, required: true },
  tax: { type: Boolean, required: true },
  mot: { type: Boolean, required: true },
  service: { type: Boolean, required: true },
  inspection: { type: Boolean, required: true },
  vehicleCheck: { type: Boolean, required: true },
});
