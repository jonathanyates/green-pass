import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { ISubscriptionController } from './subscription.controller';

@injectable()
export class SubscriptionRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.SubscriptionController) private subscriptionController: ISubscriptionController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes.get(
      '/',
      this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
      this.subscriptionController.getAll
    );
  }
}
