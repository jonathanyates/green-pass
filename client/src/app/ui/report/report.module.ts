import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ReportTypeListComponent} from "./report-type-list/report-type-list.component";
import {ReportRoutingModule} from "./report-routing.module";
import {ReportTypeInputComponent} from "./report-type-input/report-type-input.component";
import {ReportComponent} from "./report/report.component";
import {CompanyReportTypeListComponent} from "./company-report-type-list/company-report-type-list.component";
import {DriverModule} from "../driver/driver.module";
import {VehicleModule} from "../vehicle/vehicle.module";
import {ReportScheduleInputComponent} from "./report-schedule-input/report-schedule-input.component";
import {ReportScheduleRecipientsComponent} from "./report-schedule-input/report-schedule-recipients.component";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    DriverModule,
    VehicleModule,
    ReportRoutingModule
  ],
  exports: [
    ReportComponent
  ],
  declarations: [
    ReportTypeListComponent,
    ReportTypeInputComponent,
    CompanyReportTypeListComponent,
    ReportComponent,
    ReportScheduleInputComponent,
    ReportScheduleRecipientsComponent
  ],
  providers: [],
})

export class ReportModule {
}
