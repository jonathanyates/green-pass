// This code is to support multiple sub domains,
// so that we can deploy the same build to multiple sub domain deployments

let domain = window.location.hostname;
let subdomain = 'dev.';
const isLocalhost = domain.startsWith('localhost');

if (domain.indexOf('.') > 0) {
  let part = domain.split('.')[0];
  if (part !== 'greenpasscompliance' && part !== 'localhost') {
    subdomain = `${part}.`;
  }
}

export const environment = isLocalhost
  ? {
      production: false,
      apiRoot: 'http://localhost:3000',
      wwwRoot: 'http://localhost:4200',
      legalesign: {
        username: '836e21d5-4cb2-4627-a67d-1254d15735ea',
        group: '9b659a47-8146-11ed-955f-06d903d83b8a',
        path: 'http://localhost:4200/driver/documents/',
      },
    }
  : {
      production: false,
      apiRoot: `http://${subdomain}app.greenpasscompliance.co.uk`,
      wwwRoot: `http://${subdomain}greenpasscompliance.co.uk`,
      legalesign: {
        username: '836e21d5-4cb2-4627-a67d-1254d15735ea',
        group: '9b659a47-8146-11ed-955f-06d903d83b8a',
        path: `http://${subdomain}app.greenpasscompliance.co.uk/driver/documents/`,
      },
    };
