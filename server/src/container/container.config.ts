import "reflect-metadata";
import TYPES from "./container.types";
import {Container} from "inversify";
import {DbContext} from "../db/db.context";
import {IDbContext} from "../db/db.context.interface";
import {ApiRouter} from "../server/server.routes";
import {companyModule} from "../companies/company.module";
import {driverModule} from "../drivers/driver.module";
import {userModule} from "../users/user.module";
import {vehicleModule} from "../vehicles/vehicle.module";
import {documentModule} from "../documents/document.module";
import {securityModule} from "../security/security.module";
import {dvlaModule} from "../dvla/dvla.module";
import {addressModule} from "../address/address.module";
import {fileModule} from "../files/file.module";
import {complianceModule} from "../compliance/compliance.module";
import {assessmentModule} from "../assessments/assessment.module";
import {auditModule} from "../audit/audit.module";
import {alertModule} from "../alerts/alert.module";
import {reportModule} from "../reports/report.module";
import {emailModule} from "../email/email.module";
import {messagingModule} from "../messaging/messaging.module";
import {subscriptionModule} from "../subscriptions/subscription.module";
import {templateModule} from "../templates/template.module";

const container = new Container();

container.bind<IDbContext>(TYPES.db.DbContext).to(DbContext).inSingletonScope();
container.bind(TYPES.routers.ApiRouter).to(ApiRouter).inSingletonScope();

container.load(
  securityModule,
  messagingModule,
  companyModule,
  driverModule,
  userModule,
  vehicleModule,
  documentModule,
  templateModule,
  addressModule,
  dvlaModule,
  fileModule,
  complianceModule,
  assessmentModule,
  auditModule,
  alertModule,
  reportModule,
  emailModule,
  subscriptionModule
);

export class ContainerProvider {
  static container: Container;
}

ContainerProvider.container = container;

export default container;

