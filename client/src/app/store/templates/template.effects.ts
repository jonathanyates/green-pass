import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./template.actions";
import {HandleErrorAction} from "../error/error.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {TemplateService} from "../../services/template.service";
import {LoadTemplatesAction, SelectCompanyTemplateSuccessAction, SelectTemplateSuccessAction} from "./template.actions";
import {SelectTemplates} from "./template.selectors";
import {SelectTemplatesSuccessAction} from "./template.actions";
import {SaveCompanySuccessAction} from "../company/company.actions";
import {BusyAction} from "../progress/progress.actions";

@Injectable()
export class TemplateEffects {

  constructor(
    private templateService:TemplateService,
    private actions$:Actions,
    private store: Store<AppState>) { }

  @Effect() selectTemplates$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_LIST)
    .map((action: actions.SelectTemplatesAction) => action.payload)
    .switchMap(payload => this.store.let(SelectTemplates()).take(1)
      .map(templates => templates
        ? new SelectTemplatesSuccessAction(templates)
        : new LoadTemplatesAction(payload))
    );

  @Effect() loadTemplates$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .map((action: actions.LoadTemplatesAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.templateService.getTemplates(payload)
      .map(templates =>  new actions.LoadTemplatesSuccessAction(templates))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectTemplate$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_TEMPLATE)
    .map((action: actions.SelectTemplateAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.templateService.getTemplate(payload)
      .map(template => new SelectTemplateSuccessAction(template))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() selectCompanyTemplate$ = this.actions$
    .ofType(actions.ActionTypes.SELECT_COMPANY_TEMPLATE)
    .map((action: actions.SelectCompanyTemplateAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.templateService.getCompanyTemplate(payload.companyId, payload.templateId)
      .map(template => new SelectCompanyTemplateSuccessAction(template))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect() saveCompanyTemplates$ = this.actions$
    .ofType(actions.ActionTypes.SAVE_COMPANY_TEMPLATES)
    .map((action: actions.SaveCompanyTemplatesAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.templateService.saveTemplates(payload.companyId, payload.templates)
      .map(company => new SaveCompanySuccessAction(company))
      .catch(error => Observable.of(new HandleErrorAction(error)))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

}
