import {Component, Input, OnInit} from '@angular/core';

export interface IComplianceCardModel {
  id: string,
  header: string,
  chartText: string,
  percentage: number
}

@Component({
  selector: 'gp-compliance-card',
  template: `    
    <gp-card [header]="model.header" [cardColor]="model.percentage | percentageColor">
      <div class="d-flex justify-content-center">
        <div>
          <gp-percentage-chart [percent]="model.percentage"
                               [id]="model.id"></gp-percentage-chart>
          <h5><span class="badge d-block mt-3 mb-0"
                    [ngClass]="model.percentage | percentageColor">{{model.chartText}}</span></h5>
        </div>
      </div>
    </gp-card>
  `,
  styleUrls: ['compliance-card.component.css'],
  host: {'class': <string>'compliance-card col-3 text-center'}
})

export class ComplianceCardComponent implements OnInit {

  @Input() model: IComplianceCardModel;

  constructor() {
  }

  ngOnInit() {
  }
}
