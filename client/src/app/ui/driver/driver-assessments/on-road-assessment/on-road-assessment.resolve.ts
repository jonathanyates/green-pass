import "../../../../../../../shared/extensions/date.extensions"
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectCompanyAction} from "../../../../store/company/company.actions";
import {SelectDriverAction} from "../../../../store/drivers/driver.actions";
import {SelectDriver} from "../../../../store/drivers/driver.selectors";
import {Observable} from "rxjs/Observable";
import {FileService} from "../../../../services/file.service";
import {IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";
import {Paths} from '../../../../shared/constants';

@Injectable()
export class OnRoadAssessmentResolve implements Resolve<Observable<IOnRoadAssessment>|boolean> {

  constructor(private store: Store<AppState>, private fileService: FileService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOnRoadAssessment>|boolean {

    let companyId = route.params['id'];
    let driverId = route.params['driverId'];
    let assessmentId = route.params['assessmentId'];

    if (!companyId || !driverId) {
      return false;
    }

    // new on-road assessment
    if (!assessmentId && route.url.slice(-1)[0].path === Paths.New) {
      return Observable.of(<IOnRoadAssessment>{
        inviteDate: Date.prototype.getToday()
      });
    }

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new SelectDriverAction(companyId, driverId));

    return this.store.let(SelectDriver())
      .first(driver => driver != null)
      .switchMap(driver => {
        let assessment = driver.assessments.map(assessment => assessment.onRoad)
          .find(onRoad => onRoad && onRoad._id === assessmentId);

        return assessment != null && assessment.filename != null
          ? this.fileService.getFile(companyId, assessment.filename)
            .map(file => Object.assign({}, assessment, {file: file})).take(1)
          : Observable.of(assessment).take(1);
      });
  }

}
