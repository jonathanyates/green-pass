import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {Vehicle} from "./vehicle.model";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {LoadVehicleAction, SelectVehicleAction, SelectVehicleSuccessAction} from "../../store/vehicles/vehicle.actions";
import {Paths} from "../../shared/constants";

@Injectable()
export class VehicleResolve {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    if (!companyId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));

    let vehicleId = route.params['vehicleId'];

    // new vehicle
    if (!vehicleId && route.url.slice(-1)[0].path === Paths.New) {
      this.store.dispatch(new SelectVehicleSuccessAction(new Vehicle(companyId)));
      return true;
    }

    this.store.dispatch(new LoadVehicleAction(companyId, vehicleId));
    return true;
  }
}
