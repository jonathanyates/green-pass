import { NgModule } from '@angular/core';
import {SecurityModule} from "../security/security.module";
import {CommonModule} from "@angular/common";
import {DialogService} from "../../services/dialog.service";
import {HeaderComponent} from "./header/header.component";
import {SideNavComponent} from "./side-nav/side-nav-component";
import {FooterComponent} from "./footer/footer.component";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";

@NgModule({
    imports: [
      CommonModule,
      BrowserModule,
      RouterModule,
      SecurityModule
    ],
    exports: [
      HeaderComponent,
      SideNavComponent,
      FooterComponent
    ],
    declarations: [
      HeaderComponent,
      SideNavComponent,
      FooterComponent
    ],
    providers: [
      DialogService,
    ]
})
export class LayoutModule { }
