///<reference path="../../../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>
import {AfterViewInit, Component, Input} from '@angular/core';

declare let $: any;

@Component({
  selector: 'gp-percentage-chart',
  template: `
    <span class="min-chart mt-0 mb-0" [id]="id" [attr.data-percent]="percent"><span class="percent"></span></span>
  `
})
export class PercentageChartComponent implements AfterViewInit {

  private initialised: boolean = false;
  @Input() id : string;

  _percent: number;
  @Input()
  set percent(percent: number) {
    let changed = this._percent !== percent;
    this._percent = percent;
    if (this.initialised && changed) {
      this.updateChart();
    }
  }
  get percent() {
    return this._percent;
  }

  ngAfterViewInit(): void {
    const dangerColor = '#ff4444';
    const warningColor = '#ffbb33';
    const successColor = '#00C851';

    $('.min-chart#' + this.id)
      .easyPieChart({
        barColor: percent => {
          return percent === 100
            ? successColor
            : percent >= 75
              ? warningColor
              : dangerColor;
        },
        onStep: function (from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });

    this.initialised = true;
  }

  private updateChart() {
    $('.min-chart#' + this.id)
      .data('easyPieChart').update(this.percent);
  }

}
