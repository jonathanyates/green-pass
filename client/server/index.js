var express = require('express');
var bodyParser = require('body-parser');
var app = express();

//app.set('port', (process.env.PORT || 4200));
app.set('port', 4200);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname));

app.all('/*', function(req, res) {
  console.log("Sending to index.html for all SPA routes");
  res.sendFile('index.html', { root: __dirname });
});

// Enable CORS from client-side
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.listen(app.get('port'), function() {
  console.log('Green Pass server is running on port', app.get('port'));
});
