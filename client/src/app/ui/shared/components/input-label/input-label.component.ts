import {Component,  Input} from '@angular/core';
import {FormInput} from "../dynamic-form/dynamic-form.model";

@Component({
  selector: 'gp-input-label',
  template: `
    <label class="form-control-label" [attr.for]="input.id"
           [gp-reset-input]="input.type !== 'dropdown' && input.control && input.control.value || input.autoFocus">
      {{input.label + (input.required || errors ? ' (' : '') +
      (input.required ? 'required' : '') +
      (errors ? (input.required ? ' ' : '') + errors : '') +
      (input.required || errors ? ')' : '') }}</label>
`,
  host: {'class': 'form-control-label'}
})
export class InputLabelComponent {

  @Input() input: FormInput;
  errors: string = '';

  validate() {
    let control = this.input.control;
    if (control && control.dirty && !control.valid && control.touched) {
      this.errors = '';
      for (const key in control.errors) {
        if (key === 'pattern') {
          this.errors = 'Invalid ' + this.input.label;
        }
      }
    } else {
      this.errors = '';
    }
  }
}
