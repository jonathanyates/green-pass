import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'booleanColor' })
export class BooleanColorPipe implements PipeTransform {

  transform(value: number, args: any[]): any {
    return value ? 'primary-color' : 'danger-color-dark'
  };

}
