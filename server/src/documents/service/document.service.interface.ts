import { IDocument } from '../../../../shared/interfaces/document.interface';
import { IEmbeddedUrl } from '../interfaces/signature.service.interface';

export interface IDocumentService {
  getDocuments(driverId: string): Promise<IDocument[]>;

  sendDocuments(driverId: string): Promise<IDocument[]>;

  sendDocumentsForCompany(companyId: string): Promise<IDocument[][]>;

  sendDocumentsForAllCompanies(): Promise<IDocument[][][]>;

  requestEmbeddedUrl(driverId: string, documentId: string, returnUrl: string): Promise<string>;

  getNewEmbeddedUrl(driverId: string, documentId: string, signerId: string): Promise<IEmbeddedUrl>;
}
