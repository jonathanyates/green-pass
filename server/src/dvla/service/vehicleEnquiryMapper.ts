import { IVehicle } from '../../../../shared/interfaces/vehicle.interface';
import { IDvlaVehicleEnquiryResponse } from '../interfaces/vehicle-enquiry.interface';

export const mapVehicleEnquiryToVehicle = (
  vehicleEnquiry: IDvlaVehicleEnquiryResponse,
  vehicle: IVehicle
): IVehicle => {
  vehicle.registrationNumber = vehicleEnquiry.registrationNumber;
  vehicle.make = vehicleEnquiry.make;
  vehicle.taxDueDate = new Date(vehicleEnquiry.taxDueDate);
  vehicle.motDueDate = vehicleEnquiry.motExpiryDate && new Date(vehicleEnquiry.motExpiryDate);
  vehicle.motDueText = vehicleEnquiry.motStatus;
  vehicle.yearOfManufacture = vehicleEnquiry.yearOfManufacture;
  vehicle.cylinderCapacityCc = `${vehicleEnquiry.engineCapacity} cc`;
  vehicle.co2Emissions = `${vehicleEnquiry.co2Emissions} g/km`;
  vehicle.fuelType = vehicleEnquiry.fuelType;
  vehicle.exportMarker = vehicleEnquiry.markedForExport === true ? 'Yes' : 'No';
  vehicle.vehicleStatus = vehicleEnquiry.taxStatus;
  vehicle.vehicleColour = vehicleEnquiry.colour;
  vehicle.vehicleTypeApproval = vehicleEnquiry.typeApproval;
  vehicle.wheelplan = vehicleEnquiry.wheelplan;
  vehicle.revenueWeight = vehicleEnquiry.revenueWeight ? `${vehicleEnquiry.revenueWeight}kg` : 'Not available';
  vehicle.dvlaCheckDate = new Date();
  vehicle.euroStatus = vehicleEnquiry.euroStatus || 'Not available';
  vehicle.realDrivingEmissions = vehicleEnquiry.realDrivingEmissions || 'Not available';
  vehicle.dateOfLastv5cIssued = vehicleEnquiry.dateOfLastV5CIssued && new Date(vehicleEnquiry.dateOfLastV5CIssued);

  return vehicle;
};
