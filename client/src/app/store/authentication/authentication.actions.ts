import {Action} from "@ngrx/store";
import {type} from "../../shared/util";
import {LoginDetails} from "./authentication.models";
import {AuthenticationState} from "./authentication.reducer";
import {AuthenticationHelper} from "./authentication.helper";
import {IUser} from "../../../../../shared/interfaces/user.interface";

export const ActionTypes = {
  LOGIN:          type('[Authentication] Login'),
  LOGIN_SUCCESS:  type('[Authentication] Login Success'),
  LOGIN_FAILED:   type('[Authentication] Login Failed'),
  LOGOUT:         type('[Authentication] Logout'),

  FORGOT_PASSWORD:          type('[Authentication] Forgot Password'),
  FORGOT_PASSWORD_SUCCESS:  type('[Authentication] Forgot Password Success'),

  CHANGE_PASSWORD:          type('[Authentication] Change Password'),
  CHANGE_PASSWORD_SUCCESS:  type('[Authentication] Change Password Success'),

  SET_REDIRECT_URL:   type('[Authentication] Set Redirect')
};

export class LoginAction implements Action {
  type = ActionTypes.LOGIN;
  public payload: LoginDetails;
  constructor(username: string, password: string) {
    this.payload = { username: username, password: password };
  }
}

export class LoginSuccessAction implements Action {
  type = ActionTypes.LOGIN_SUCCESS;
  public payload: AuthenticationState;
  public redirect: boolean;
  constructor(token: string, redirect: boolean) {
    let user = AuthenticationHelper.getUserFromToken(token);
    this.payload = {
      loggedIn: true,
      token: token,
      user: user
    };
    this.redirect = redirect;
  }
}

export class LoginFailedAction implements Action {
  type = ActionTypes.LOGIN_FAILED;
  payload: AuthenticationState;
  constructor(message: string = 'Login Failed') {
    this.payload = {
      loggedIn: false,
      message: message
    };
  }
}

export class ForgotPasswordAction implements Action {
  type = ActionTypes.FORGOT_PASSWORD;
  constructor(public payload: string) { // email
  }
}

export class ForgotPasswordSuccessAction implements Action {
  type = ActionTypes.FORGOT_PASSWORD_SUCCESS;
  constructor(public payload: string) {
  }
}

export class ChangePasswordAction implements Action {
  type = ActionTypes.CHANGE_PASSWORD;
  public payload: { user: IUser, password: string };
  constructor(user: IUser, password: string) {
    this.payload = { user: user, password: password }
  }
}

export class ChangePasswordSuccessAction implements Action {
  type = ActionTypes.CHANGE_PASSWORD_SUCCESS;
  constructor(public payload: IUser) {
  }
}

export class LogoutAction implements Action {
  type = ActionTypes.LOGOUT;
  constructor(public payload: AuthenticationState) { }
}

export class SetRedirectUrlAction implements Action {
  type = ActionTypes.SET_REDIRECT_URL;
  constructor(public payload: string) { }
}

export type Actions
  = LoginSuccessAction
  | LoginFailedAction
  | SetRedirectUrlAction
  | ChangePasswordSuccessAction
  | LogoutAction;



