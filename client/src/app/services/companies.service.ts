import { Injectable } from '@angular/core';
import {apiPath} from "../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {Observable} from "rxjs";
import {HttpBaseService} from "./http-base.service";
import {ICompany} from "../../../../shared/interfaces/company.interface";
import {ISubscription} from "../../../../shared/interfaces/subscription.interface";
import {IFleetInsurance} from "../../../../shared/interfaces/common.interface";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CompaniesService extends HttpBaseService {

  private apiUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getCompanies(): Observable<ICompany[]> {

    this.authorize();

    return this.httpClient.get<ICompany[]>(this.apiUrl, this.getOptions())
      .map(res => this.handleResult<ICompany[]>(res))
      .catch(this.handleError);
  }

  getCompany(id): Observable<ICompany> {

    this.authorize();

    return this.httpClient.get<ICompany>(this.apiUrl + id, this.getOptions())
      .map(res => this.handleResult<ICompany>(res))
      .catch(this.handleError);
  }

  saveCompany(company: ICompany): Observable<ICompany> {

    this.authorize();

    if (company._id) {
      return this.httpClient.put<ICompany>(this.apiUrl + company._id, company, this.getOptions())
        .map(res => this.handleResult<ICompany>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<ICompany>(this.apiUrl + company._id, company, this.getOptions())
        .map(res => this.handleResult<ICompany>(res))
        .catch(this.handleError);
    }
  }

  saveCompanyInsurance(company: ICompany, fleetInsurance: IFleetInsurance): Observable<IFleetInsurance> {

    this.authorize();

    let formData = new FormData();
    formData.append('validFrom', fleetInsurance.validFrom.toString());
    formData.append('validTo', fleetInsurance.validTo.toString());
    formData.append('fileType', fleetInsurance.fileType);

    if (fleetInsurance.file) {
      formData.append('file', fleetInsurance.file);
    }

    if (fleetInsurance._id) {
      return this.httpClient.put<IFleetInsurance>(`${this.apiUrl}${company._id}/insurance/${fleetInsurance._id}`, formData, this.getOptions())
        .map(res => this.handleResult<IFleetInsurance>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IFleetInsurance>(`${this.apiUrl}${company._id}/insurance/`, formData, this.getOptions())
        .map(res => this.handleResult<IFleetInsurance>(res))
        .catch(this.handleError);
    }
  }

  removeCompany(companyId: string): Observable<ICompany> {
    this.authorize();

    return this.httpClient.delete<ICompany>(this.apiUrl + companyId, this.getOptions())
      .map(response => this.handleResult(response))
      .catch(this.handleError);
  }

  getSubscriptions(): Observable<ISubscription[]> {

    this.authorize();

    return this.httpClient.get<ISubscription[]>(`${apiPath}/subscriptions`, this.getOptions())
      .map(res => this.handleResult<ISubscription[]>(res))
      .catch(this.handleError);
  }

}
