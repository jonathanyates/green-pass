import codes from "../shared/error.codes";
import {inject, injectable} from "inversify";
import {NextFunction, Request, Response} from "express-serve-static-core";
import TYPES from "../container/container.types";
import {IDocumentService} from "../documents/service/document.service.interface";
import {ITemplateService} from "./template.service";
import {AuditActions, AuditTargets, IAuditService} from "../audit/audit.service";
import {ICompanyService} from "../companies/company.service.interface";

export interface ITemplateController {
  getTemplates(req:Request, res:Response, next: NextFunction);
  getTemplate(req:Request, res:Response, next: NextFunction);
  addTemplate(req:Request, res:Response, next: NextFunction);
  updateTemplate(req:Request, res:Response, next: NextFunction);
  deleteTemplate(req:Request, res:Response, next: NextFunction);

  addCompanyTemplate(req:Request, res:Response, next: NextFunction);
  getCompanyTemplates(req:Request, res:Response, next: NextFunction);
  saveCompanyTemplates(req:Request, res:Response, next: NextFunction);
}

@injectable()
export class TemplateController implements ITemplateController {

  constructor(
    @inject(TYPES.services.DocumentService) private documentService: IDocumentService,
    @inject(TYPES.services.TemplateService) private templateService: ITemplateService,
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  getTemplates = (req: Request, res: Response, next: NextFunction) => {
    this.templateService.getTemplates()
      .then(templates => {
        return res.status(codes.OK).json(templates);
      })
      .catch(error => {
        next(error);
      });
  };

  getTemplate = (req: Request, res: Response, next: NextFunction) => {
    const templateId = req.params.templateId;

    if (!templateId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No templateId specified.'});
    }

    this.templateService.getTemplate(templateId)
      .then(template => {
        return res.status(codes.OK).json(template);
      })
      .catch(error => {
        next(error);
      });
  };

  addTemplate = (req: Request, res: Response, next: NextFunction) => {
    const template = req.body;

    if (!template) {
      return res.status(codes.BAD_REQUEST).send({message: 'No template specified.'});
    }

    this.templateService.addTemplate(template)
      .then(template => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Template, template);
        return res.status(codes.CREATED).json(template)
      })
      .catch(err => {
        this.auditService.auditError(req.user, AuditActions.Add, AuditTargets.Template, err);
        next(err)
      });
  };

  updateTemplate = (req: Request, res: Response, next: NextFunction) => {
    const template = req.body;

    if (!template) {
      return res.status(codes.BAD_REQUEST).send({message: 'No template specified.'});
    }

    this.templateService.updateTemplate(template)
      .then(template => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.Template, template);
        return res.status(codes.CREATED).json(template)
      })
      .catch(err => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.Template, err);
        next(err)
      });
  };

  deleteTemplate = (req: Request, res: Response, next: NextFunction) => {
    const templateId = req.params.templateId;

    if (!templateId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No templateId specified.'});
    }

    this.templateService.deleteTemplate(templateId)
      .then(template => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.Template, template);
        return res.status(codes.CREATED).json(template)
      })
      .catch(err => {
        this.auditService.auditError(req.user, AuditActions.Remove, AuditTargets.Template, err);
        next(err)
      });
  };

  addCompanyTemplate = (req:Request, res:Response, next: NextFunction) => {
    const companyId = req.params.id;
    const template = req.body;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No companyId specified.'});
    }

    if (!template) {
      return res.status(codes.BAD_REQUEST).send({message: 'No template specified.'});
    }

    this.templateService.addTemplate(template)
      .then(template => {
        this.companyService.get(companyId)
          .then(company => {
            const templates = [...company.templates, template._id];
            this.templateService.saveCompanyTemplates(companyId, templates)
              .then(company => {
                return this.documentService.sendDocumentsForCompany(companyId)
                  .then(documents => {
                    this.auditService.audit(req.user, AuditActions.Add, AuditTargets.Template, template);
                    return res.status(codes.CREATED).json(template)
                  })
                  .catch(error => {
                    next(error);
                  });
              })
              .catch(error => {
                next(error);
              });
          })
      })
      .catch(err => {
        this.auditService.auditError(req.user, AuditActions.Add, AuditTargets.Template, err);
        next(err)
      });
  };

  getCompanyTemplates = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No companyId specified.'});
    }

    this.templateService.getCompanyTemplates(companyId)
      .then(templates => {
        return res.status(codes.OK).json(templates);
      })
      .catch(error => {
        next(error);
      });
  };

  saveCompanyTemplates = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const templates = req.body;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No company id specified.'});
    }

    if (!templates) {
      return res.status(codes.BAD_REQUEST).send({message: 'No templates specified.'});
    }

    this.templateService.saveCompanyTemplates(companyId, templates)
      .then(company => {
        return this.documentService.sendDocumentsForCompany(companyId)
          .then(documents => {
            return res.status(codes.ACCEPTED).json(company);
          })
          .catch(error => {
            next(error);
          });
      })
      .catch(error => {
        next(error);
      });
  };

}

