import {AppState} from "../app.states";
import {DocumentsState} from "./document.state";
import {Store} from "@ngrx/store";

export const SelectDocumentsState = () => {
  return (store: Store<AppState>) => {
    return store.select<DocumentsState>(state => state.documents);
  }
};

export const SelectDocuments = () => {
  return (store: Store<AppState>) => {
    return store.let(SelectDocumentsState())
      .map(state => state.list)
      .filter(documents => documents !== null)
      .distinctUntilChanged()
      .map(documents => documents);
  }
};

export const SelectDocument = () => {
  return (store: Store<AppState>) => {
    return store.let(SelectDocumentsState())
      .map(state => state.selected)
      .filter(document => document !== null);
  }
};
