import logger from 'winston';
import { Observable, Observer } from 'rxjs';
import amqp from 'amqplib/callback_api';
import { injectable } from 'inversify';
import serverConfig from '../server/server.config';

export interface IMessageService {
  send<T>(queueName: string, payload: T): Promise<boolean>;
  receive<T>(queueName: string): Observable<T>;
}

const RABBITMQ_HOST = serverConfig.rabbitmq.url ?? 'amqp://rabbitmq';
logger.info('RABBITMQ_HOST = ', RABBITMQ_HOST);

@injectable()
export class MessageService implements IMessageService {
  send<T>(queueName: string, payload: T): Promise<boolean> {
    logger.info(`Sending message '${payload}' to message queue ${queueName} at ${RABBITMQ_HOST}...`);

    return new Promise((resolve, reject) => {
      amqp.connect(RABBITMQ_HOST, function (error, connection) {
        if (error) {
          logger.error(error.message);
          return reject(error);
        }

        connection.createChannel(function (error, channel) {
          if (error) {
            logger.error(error.message);
            return reject(error);
          }

          const message = JSON.stringify(payload);

          try {
            channel.assertQueue(queueName, { durable: false });
            channel.sendToQueue(queueName, new Buffer(message));
          } catch (error) {
            logger.error(error.message);
            return reject(error);
          }

          logger.info(`Sent '${message}' to queue '${queueName}'`);
          return resolve(true);
        });

        setTimeout(function () {
          connection.close();
        }, 500);
      });
    });
  }

  receive<T>(queueName: string): Observable<T> {
    return Observable.create((observer: Observer<T>) => {
      amqp.connect(RABBITMQ_HOST, function (error, connection) {
        if (error) {
          observer.error(error);
          return;
        }

        connection.createChannel(function (error, channel) {
          if (error) {
            observer.error(error);
            return;
          }

          channel.assertQueue(queueName, { durable: false });

          logger.info('Waiting for messages in %s.', queueName);

          try {
            channel.consume(
              queueName,
              function (message) {
                const data = JSON.parse(message.content as any);
                logger.info('Received %s', JSON.stringify(data));
                observer.next(data);
              },
              { noAck: true }
            );
          } catch (error) {
            observer.error(error);
            return;
          }
        });
      });
    });
  }
}
