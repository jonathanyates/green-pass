import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Observable} from "rxjs";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {SelectVehicles} from "../../../store/vehicles/vehicle.selectors";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {LoadVehiclesAction} from "../../../store/vehicles/vehicle.actions";
import {SelectError} from "../../../store/error/error.selectors";
import {pagination} from "../../../shared/constants";

@Injectable()
export class VehicleListResolve implements Resolve<IVehicle[]|boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IVehicle[]|boolean> {

    let companyId = route.params['id'];
    if (!companyId) return Observable.of(false);

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new LoadVehiclesAction(companyId, 1, pagination.pageSize));

    return this.store.let(SelectVehicles())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (vehicles, error) => ({ vehicles:vehicles, error:error }))
      .filter(result => result.error != null || result.vehicles != null)
      .map(result => result.error ? false : result.vehicles)
      .take(1);
  }
}
