import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AutoFocusDirective} from "./directives/auto-focus.directive";
import {FullAddressPipe} from "./pipes/full-address.pipe";
import {ItemsSummaryComponent} from "./components/items-summary/items-summary.component";
import {IfRequiredDirective} from "./directives/if-required.directive";
import {ValidationClassDirective} from "./directives/validation-class.directive";
import {ResetInputDirective} from "./directives/reset-input.directive";
import {DynamicFormComponent} from "./components/dynamic-form/dynamic-form.component";
import {InputComponent} from "./components/dynamic-input/dynamic-input.component";
import {DuplicateValidatorDirective} from "./validators/unique.validator";
import {HeadingComponent} from "./components/heading/heading.component";
import {DynamicListComponent} from "./components/dynamic-list/dynamic-list.component";
import {CanDeactivateGuard} from "./guards/can-deactivate.guard";
import {DynamicInputGroupComponent} from "./components/dynamic-form/dynamic-input-group.component";
import {SafePipe} from "./pipes/safe.pipe";
import {ProgressIndicatorComponent} from "./components/progress-indicator/progress-indicator.component";
import {ContactCardComponent} from './components/contact-card/contact-card.component';
import {AddressComponent} from "./components/address/address.component";
import {AddressInputComponent} from "./components/address-input/address-input.component";
import {AddressInputMapper} from "./components/address-input/address-input.mapper";
import { SearchInputComponent } from './components/search-input/search-input.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import {BreadcrumbService} from "../../services/breadcrumb.service";
import {BreadcrumbComponent} from "./components/breadcrumb/breadcrumb.component";
import {DynamicListGroupComponent} from "./components/dynamic-list-group/dynamic-list-group.component";
import {CardComponent} from "./components/card/card.component";
import {ImagePreviewComponent} from "./components/image-preview/image-preview-component";
import {FileInputComponent} from "./components/file-input/file-input.component";
import {InputLabelComponent} from "./components/input-label/input-label.component";
import {SelectInputComponent} from "./components/select-input/select-input.component";
import {PercentageChartComponent} from "./charts/percentage-chart.component";
import {FileRecordInputMapper} from "./components/file-record/file-record-input/file-record-input.mapper";
import {FileRecordListComponent} from "./components/file-record/insurance-list/file-record-list.component";
import {FileRecordInputComponent} from "./components/file-record/file-record-input/file-record-input.component";
import {FileRecordComponent} from "./components/file-record/file-record-detail/file-record.component";
import {NgxPaginationModule} from "ngx-pagination";
import {SearchComponent} from "app/ui/shared/components/search/search.component";
import {ModalComponent} from "./components/modal/model.component";
import {ErrorAlertComponent} from "./components/error-alert/error-alert.component";
import {CronComponent} from "./components/cron/cron.component";
import {AssessmentResultPipe} from "./pipes/assessment-result.pipe";
import {PercentageColorPipe} from "./pipes/percentage-color.pipe";
import {PdfPreviewComponent} from "./components/image-preview/pdf-preview-component";
import {DriverListComponent} from "./components/driver-list/driver-list.component";
import {BooleanColorPipe} from "./pipes/boolean-color.pipe";
import {BaseComponent} from "./components/base/base.component";
import {PdfViewerComponent} from "ng2-pdf-viewer/dist/pdf-viewer.component";
import {SafeHtmlPipe} from "./pipes/safeHtml.pipe";
import {DynamicContentComponent} from "./components/dynamic-content/dynamic-content.component";
import {EditorComponent} from "./editor/editor.component";
import {ComplianceCardComponent} from "./components/compliance-card/compliance-card.component";
import {ComplianceCardsComponent} from "./components/compliance-card/compliance-cards.component";
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {FileValidator, FileValueAccessor} from './directives/file-value-accessor.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [
    DuplicateValidatorDirective,
    AutoFocusDirective,
    FullAddressPipe,
    ItemsSummaryComponent,
    IfRequiredDirective,
    ValidationClassDirective,
    ResetInputDirective,
    BaseComponent,
    DynamicContentComponent,
    DynamicFormComponent,
    DynamicListComponent,
    DynamicInputGroupComponent,
    InputComponent,
    HeadingComponent,
    ProgressIndicatorComponent,
    SafePipe,
    SafeHtmlPipe,
    CardComponent,
    ContactCardComponent,
    AddressComponent,
    AddressInputComponent,
    SearchInputComponent,
    ContactDetailsComponent,
    BreadcrumbComponent,
    DynamicListGroupComponent,
    PdfViewerComponent,
    FileInputComponent,
    ImagePreviewComponent,
    PdfPreviewComponent,
    InputLabelComponent,
    SelectInputComponent,
    PercentageChartComponent,
    FileRecordListComponent,
    FileRecordInputComponent,
    FileRecordComponent,
    SearchComponent,
    ModalComponent,
    ErrorAlertComponent,
    CronComponent,
    AssessmentResultPipe,
    PercentageColorPipe,
    BooleanColorPipe,
    DriverListComponent,
    EditorComponent,
    ComplianceCardComponent,
    ComplianceCardsComponent,
    PrivacyPolicyComponent,
    FileValueAccessor,
    FileValidator
  ],
  exports: [
    DuplicateValidatorDirective,
    AutoFocusDirective,
    FullAddressPipe,
    ItemsSummaryComponent,
    IfRequiredDirective,
    ValidationClassDirective,
    ResetInputDirective,
    BaseComponent,
    DynamicContentComponent,
    DynamicFormComponent,
    DynamicListComponent,
    DynamicInputGroupComponent,
    InputComponent,
    HeadingComponent,
    ProgressIndicatorComponent,
    SafePipe,
    CardComponent,
    ContactCardComponent,
    AddressComponent,
    AddressInputComponent,
    ContactDetailsComponent,
    BreadcrumbComponent,
    DynamicListGroupComponent,
    FileInputComponent,
    ImagePreviewComponent,
    PdfPreviewComponent,
    InputLabelComponent,
    SelectInputComponent,
    PercentageChartComponent,
    FileRecordListComponent,
    FileRecordInputComponent,
    FileRecordComponent,
    SearchComponent,
    ModalComponent,
    ErrorAlertComponent,
    CronComponent,
    AssessmentResultPipe,
    PercentageColorPipe,
    BooleanColorPipe,
    DriverListComponent,
    EditorComponent,
    ComplianceCardComponent,
    ComplianceCardsComponent,
    PrivacyPolicyComponent,
    FileValueAccessor,
    FileValidator
  ],
  providers: [
    BreadcrumbService,
    CanDeactivateGuard,
    AddressInputMapper,
    FileRecordInputMapper
  ]
})
export class SharedModule { }
