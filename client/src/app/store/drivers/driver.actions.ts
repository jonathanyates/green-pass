import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {IContact, IDriver} from "../../../../../shared/interfaces/driver.interface";
import {IOnRoadAssessment} from "../../../../../shared/interfaces/assessment.interface";
import {pagination, Paths} from "../../shared/constants";

export const ActionTypes = {
  LOAD:                   type('[Drivers] Load'),
  LOAD_SUCCESS:           type('[Drivers] Load Success'),
  SELECT_LIST:            type('[Drivers] Select'),
  SELECT_LIST_SUCCESS:    type('[Drivers] Select Success'),
  LOAD_DRIVER:            type('[Driver] Load'),
  LOAD_DRIVER_SUCCESS:    type('[Driver] Load Success'),
  SEARCH_DRIVER:          type('[Driver] Search'),
  SEARCH_DRIVER_SUCCESS:  type('[Driver] Search Success'),
  SELECT_DRIVER:          type('[Driver] Select'),
  SELECT_DRIVER_SUCCESS:  type('[Driver] Select Success'),
  SAVE:                   type('[Driver] Save'),
  SAVE_SUCCESS:           type('[Driver] Save Success'),

  CLEAR_LIST:             type('[Driver] Clear List'),

  SELECT_DRIVER_REFERENCE:type('[Driver Reference] Select'),
  SAVE_DRIVER_REFERENCE:  type('[Driver Reference] Save'),
  SAVE_DRIVER_REFERENCE_SUCCESS:  type('[Driver Reference] Save Success'),
  UPDATE_DRIVER_STATUS:           type('[Driver] Update Status'),
  UPDATE_DRIVER_STATUS_SUCCESS:   type('[Driver] Update Status Success'),
  SELECT_SELECTED_TAB:    type('[Driver] Select Selected Tab'),

  LICENCE_CHECK_CODE:             type('[Driver] Licence Check Code'),
  LICENCE_CHECK_CODE_SUCCESS:     type('[Driver] Licence Check Code Success'),
  LICENCE_CHECK_CODE_FAILURE:     type('[Driver] Licence Check Code Failure'),

  LICENCE_CHECK:          type('[Driver] Licence Check'),
  LICENCE_CHECK_SUCCESS:  type('[Driver] Licence Check Success'),
  LICENCE_CHECK_FAILURE:  type('[Driver] Licence Check Failure'),

  ASSESSMENT_LOGIN_SUCCESS:   type('[Driver] Assessment Login Success'),
  ASSESSMENT_UPDATE:          type('[Driver] Assessment Update'),
  ASSESSMENT_UPDATE_SUCCESS:  type('[Driver] Assessment Update Success'),
  ASSESSMENT_UPDATE_FAILURE:  type('[Driver] Assessment Update Failure'),

  SAVE_ON_ROAD_ASSESSMENT:  type('[Driver On Road Assessment] Save'),
  SAVE_ON_ROAD_ASSESSMENT_SUCCESS:  type('[Driver On Road Assessment] Save Success'),
};

/**
 * Load Drivers Actions
 */
export class LoadDriversAction implements Action {
  type = ActionTypes.LOAD;
  page: number;
  pageSize: number = pagination.pageSize;
  searchCriteria: any;
  constructor(public payload: string, page?: number, pageSize?: number, searchCriteria?: any) {
    this.page = page;
    this.pageSize = pageSize;
    this.searchCriteria = searchCriteria;
  } //  payload company id
}

export class LoadDriversSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  total: number;
  page: number;
  payload: IDriver[];
  constructor(drivers:  IDriver[], total?: number, page?: number) {
    this.payload = drivers;
    this.total = total ? total : this.payload.length;
    this.page = page;
  }
}

/**
 * Select Drivers Actions
 */
export class SelectDriversAction implements Action {
  type = ActionTypes.SELECT_LIST;
  constructor(public payload: string) { } //  payload company id
}

export class SelectDriversSuccessAction implements Action {
  type = ActionTypes.SELECT_LIST_SUCCESS;
  total: number;
  constructor(public payload: IDriver[]) { }
}

/**
 * Load Drivers Actions
 */
export class LoadDriverAction implements Action {
  type = ActionTypes.LOAD_DRIVER;
  payload: { companyId:string, driverId:string };

  constructor(companyId:string, driverId:string) {
    this.payload = { companyId:companyId, driverId:driverId }
  }
}

export class LoadDriverSuccessAction implements Action {
  type = ActionTypes.LOAD_DRIVER_SUCCESS;
  constructor(public payload: IDriver) { }
}

/**
 * Select Driver Actions
 */
export class SelectDriverAction implements Action {
  type = ActionTypes.SELECT_DRIVER;
  payload: { companyId:string, driverId:string };

  constructor(companyId:string, driverId:string) {
    this.payload = { companyId:companyId, driverId:driverId }
  }
}

export class SelectDriverSuccessAction implements Action {
  type = ActionTypes.SELECT_DRIVER_SUCCESS;
  constructor(public payload: IDriver) { }
}

/**
 * Search Driver Actions
 */
export class SearchDriverAction implements Action {
  type = ActionTypes.SEARCH_DRIVER;
  payload: { companyId:string, searchCriteria: any };

  constructor(companyId:string, searchCriteria: any) {
    this.payload = { companyId:companyId, searchCriteria: searchCriteria }
  }
}

/**
 * Save Driver Actions
 */
export class SaveDriverAction implements Action {
  type = ActionTypes.SAVE;
  redirectPath: string = Paths.Details;
  constructor(public payload: IDriver, redirectPath: string) {
    this.redirectPath = redirectPath;
  }
}

export class SaveDriverSuccessAction implements Action {
  type = ActionTypes.SAVE_SUCCESS;
  constructor(public payload: IDriver) { }
}

/**
 * Clear List Action
 */
export class ClearDriverListAction implements Action {
  type = ActionTypes.CLEAR_LIST;
  payload;
  constructor() { }
}

/**
 * Driver Reference Actions
 */
export class SelectDriverReferenceAction implements Action {
  type = ActionTypes.SELECT_DRIVER_REFERENCE;
  constructor(public payload: string) { } // reference id
}

export class SaveDriverReferenceAction implements Action {
  type = ActionTypes.SAVE_DRIVER_REFERENCE;
  constructor(public payload: IContact) { }
}

export class SaveDriverReferenceSuccessAction implements Action {
  type = ActionTypes.SAVE_DRIVER_REFERENCE_SUCCESS;
  constructor(public payload: IDriver) { }
}

/**
 * Update Driver Status Actions
 */
export class UpdateDriverStatusAction implements Action {
  type = ActionTypes.UPDATE_DRIVER_STATUS;
  constructor(public payload: IDriver) { }
}

export class UpdateDriverStatusSuccessAction implements Action {
  type = ActionTypes.UPDATE_DRIVER_STATUS_SUCCESS;
  constructor(public payload: IDriver) { }
}

export class SelectDriverDetailsTabAction implements Action {
  type = ActionTypes.SELECT_SELECTED_TAB;
  constructor(public payload: string) {}
}

/**
 * Driver Licence Check Code
 */
export class DrivingLicenceCheckCodeAction implements Action {
  type = ActionTypes.LICENCE_CHECK_CODE;
  constructor(public payload: IDriver) {
  }
}

export class DrivingLicenceCheckCodeSuccessAction implements Action {
  type = ActionTypes.LICENCE_CHECK_CODE_SUCCESS;
  constructor(public payload: string) { } // check code
}

export class DrivingLicenceCheckCodeFailureAction implements Action {
  type = ActionTypes.LICENCE_CHECK_CODE_FAILURE;
  constructor(public payload: Error) { }
}

/**
 * Driver Licence Checks
 */
export class DrivingLicenceCheckAction implements Action {
  type = ActionTypes.LICENCE_CHECK;
  payload: { driver: IDriver, checkCode: string };
  licenceNumber: string;
  constructor(public driver: IDriver, checkCode: string, licenceNumber: string = null) {
    this.payload = { driver: driver, checkCode: checkCode };
    this.licenceNumber = licenceNumber;
  }
}

export class DrivingLicenceCheckSuccessAction implements Action {
  type = ActionTypes.LICENCE_CHECK_SUCCESS;
  constructor(public payload: IDriver) { }
}

export class DrivingLicenceCheckFailureAction implements Action {
  type = ActionTypes.LICENCE_CHECK_FAILURE;
  constructor(public payload: Error) { }
}

/**
 * Assessments
 */
export class AssessmentLoginSuccessAction implements  Action {
  type = ActionTypes.ASSESSMENT_LOGIN_SUCCESS;
  payload: boolean = true;
}

export class AssessmentUpdateAction implements Action {
  type = ActionTypes.ASSESSMENT_UPDATE;
  constructor(public payload: IDriver) { }
}

export class AssessmentUpdateSuccessAction implements Action {
  type = ActionTypes.ASSESSMENT_UPDATE_SUCCESS;
  constructor(public payload: IDriver) { }
}

export class AssessmentUpdateFailureAction implements Action {
  type = ActionTypes.ASSESSMENT_UPDATE_FAILURE;
  constructor(public payload: Error) { }
}

export class SaveOnRoadAssessmentAction implements Action {
  type = ActionTypes.SAVE_ON_ROAD_ASSESSMENT;
  payload: { driver: IDriver, assessment: IOnRoadAssessment };
  constructor(driver: IDriver, assessment: IOnRoadAssessment) {
    this.payload = { driver: driver, assessment: assessment }
  }
}

export class SaveOnRoadAssessmentSuccessAction implements Action {
  type = ActionTypes.SAVE_ON_ROAD_ASSESSMENT_SUCCESS;
  constructor(public payload: IDriver, public assessment: IOnRoadAssessment) {
  }
}

export type Actions
  = LoadDriversSuccessAction
  | SelectDriversSuccessAction
  | LoadDriverSuccessAction
  | SelectDriverSuccessAction
  | SelectDriverReferenceAction
  | SaveDriverSuccessAction
  | SaveDriverReferenceAction
  | SaveDriverReferenceSuccessAction
  | UpdateDriverStatusSuccessAction
  | DrivingLicenceCheckSuccessAction
  | SelectDriverDetailsTabAction
  | AssessmentLoginSuccessAction
  | AssessmentUpdateSuccessAction
  | SaveOnRoadAssessmentSuccessAction
  | ClearDriverListAction
  | LogoutAction;
