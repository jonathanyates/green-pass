import {Component, Input} from '@angular/core';
import {AssessmentSummary} from "../../../../../../../shared/models/assessment-summary.model";
import {Paths} from "../../../../shared/constants";
import {SelectDriverDetailsTabAction} from "../../../../store/drivers/driver.actions";
import {Router} from "@angular/router";
import {AppState} from "../../../../store/app.states";
import {Store} from "@ngrx/store";
import {IAssessment, IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";
import {ModuleSummary} from "../../../../../../../shared/models/module-summary.model";

@Component({
  moduleId: module.id,
  selector: 'gp-assessment-summary',
  templateUrl: 'assessment-summary.component.html',
  host: {'class': 'card'}
})

export class AssessmentSummaryComponent {

  private _assessment: IAssessment;
  @Input() set assessment(assessment: IAssessment) {
    this._assessment = assessment;
    this.setAssessments(assessment);
  }
  get assessment() { return this._assessment; }

  assessmentSummary: AssessmentSummary;
  moduleSummary: ModuleSummary;
  onRoadAssessment: IOnRoadAssessment;

  constructor(private store: Store<AppState>,
              private router: Router) {
  }

  viewAssessments() {
    this.router.navigate([Paths.Driver, Paths.Assessments]);
    this.store.dispatch(new SelectDriverDetailsTabAction('assessments'));
  }

  setAssessments(assessment: IAssessment) {
    this.assessmentSummary = new AssessmentSummary(assessment);
    this.moduleSummary = new ModuleSummary(assessment);
    if (assessment.onRoad) {
      this.onRoadAssessment = assessment.onRoad;
    }
  }
}
