import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IAlertRouter } from '../alerts/alert.router';
import multer from 'multer';
import { ICompanyController } from './company.controller';
import { ITemplateRouter } from '../templates/template.router';
const upload = multer({ dest: 'uploads/' });

@injectable()
export class CompanyRouter implements IApiRouter {
  routes = express.Router();

  constructor(
    @inject(TYPES.controllers.CompanyController) private companyController: ICompanyController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware,
    @inject(TYPES.routers.UserRouter) private userRouter: IApiRouter,
    @inject(TYPES.routers.DriverRouter) private driverRouter: IApiRouter,
    @inject(TYPES.routers.VehicleRouter) private vehicleRouter: IApiRouter,
    @inject(TYPES.routers.FileRouter) private fileRouter: IApiRouter,
    @inject(TYPES.routers.AlertRouter) private alertRouter: IAlertRouter,
    @inject(TYPES.routers.ReportRouter) private reportRouter: IApiRouter,
    @inject(TYPES.routers.ReportScheduleRouter) private reportScheduleRouter: IApiRouter,
    @inject(TYPES.routers.TemplateRouter) private templateRouter: ITemplateRouter
  ) {
    this.configure();
  }

  configure() {
    this.routes.use('/:id/users', this.userRouter.routes);
    this.routes.use('/:id/vehicles', this.vehicleRouter.routes);
    this.routes.use('/:id/drivers', this.driverRouter.routes);
    this.routes.use('/:id/files', this.fileRouter.routes);
    this.routes.use('/:id/alerts', this.alertRouter.companyRoutes);
    this.routes.use('/:id/reports', this.reportRouter.routes);
    this.routes.use('/:id/schedules', this.reportScheduleRouter.routes);
    this.routes.use('/:id/templates', this.templateRouter.companyRoutes);

    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.companyController.getAll
      )
      .get(
        '/:id',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.companyController.get
      )
      .post(
        '/:id',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.admin),
        this.companyController.add
      )
      .put(
        '/:id',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.companyController.update
      )
      .delete(
        '/:id',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.companyController.remove
      )

      .post(
        '/:id/insurance/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.companyController.addInsurance
      )
      .put(
        '/:id/insurance/:insuranceId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        upload.single('file'),
        this.companyController.updateInsurance
      );
  }
}
