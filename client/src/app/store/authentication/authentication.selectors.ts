import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {AuthenticationState} from "./authentication.reducer";
import {Roles} from "../../../../../shared/constants";

export const SelectAuthenticationState = () => {
  return (store:Store<AppState>) => {
    return store.select<AuthenticationState>(state => state.authentication);
  }
};

export const SelectLoggedIn = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.loggedIn)
      .distinctUntilChanged();
  }
};

export const SelectLoggedInUser = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.user)
      .distinctUntilChanged();
  }
};

export const SelectIsAdmin = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.user)
      .map(user => user !== null && user.role !== Roles.Driver);
  }
};

export const SelectIsGreenPassAdmin = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.user)
      .map(user => user !== null && user.role === Roles.Admin);
  }
};

export const SelectIsDriver = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.user)
      .map(user => user !== null && user.role == Roles.Driver);
  }
};

export const SelectAuthenticationError = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectAuthenticationState())
      .map(state => state.message)
      .distinctUntilChanged();
  }
};
