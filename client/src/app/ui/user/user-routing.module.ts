import {NgModule} from '@angular/core';
import {Roles} from "../../../../../shared/constants";
import {Routes, RouterModule} from '@angular/router';
import {AuthorizationGuard} from "../security/guards/authorization.guard";
import {CompanyComponent} from "../company/company.component";
import {UserListComponent} from "./user-list/user-list.component";
import {UserInputComponent} from "./user-input/user-input.component";
import {UserDetailsComponent} from "./user-details/user-details.component";
import {UserListResolve} from "./user-list/user-list.resolve";
import {UserResolve} from "./user.resolve";

const routes: Routes = [
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthorizationGuard],
    data: {roles: [Roles.Admin, Roles.Company]},
    children: [
      { path: ':id/users', component: UserListComponent, resolve: { users: UserListResolve } },
      { path: ':id/users/new', component: UserInputComponent, resolve: { user: UserResolve } },
      { path: ':id/users/:userId', component: UserDetailsComponent, resolve: { user: UserResolve } },
      { path: ':id/users/:userId/edit', component: UserInputComponent, resolve: { user: UserResolve } },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UserListResolve,
    UserResolve,
  ]
})
export class UserRoutingModule {
}
