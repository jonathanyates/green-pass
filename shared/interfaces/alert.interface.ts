import { IDriver } from './driver.interface';
import { IVehicle } from './vehicle.interface';
import { IUser } from './user.interface';
import { IReportSchedule, IReportType } from './report.interface';
import { IPaginationResults } from './common.interface';

export const AlertPriority = {
  Critical: 'Critical',
  High: 'High',
  Medium: 'Medium',
  Low: 'Low',
};

export interface IAlertType {
  _id?: any;
  reportType: IReportType;
  priority: string;
}

export interface ICompanyAlertType {
  _id?: any;
  alertType: IAlertType;
  schedule?: IReportSchedule;
}

export const AlertStatus = {
  Active: 'Active',
  Resolved: 'Resolved',
};

export interface IAlert {
  _id?: any;
  _companyId: any;
  alertType: IAlertType;
  created: Date;
  updated?: Date;
  status: string;
  comment?: string;
  driver?: IDriver;
  vehicle?: IVehicle;
}

export interface IAlertSummary {
  companyId?: string;
  driverAlerts: number;
  vehicleAlerts: number;
}

export interface IAlertTypeSummary {
  key: string;
  category: string;
  priority: string;
  alertType: IAlertType;
  count: number;
  alerts: IAlert[];
}

export interface IAlertSearchResults extends IPaginationResults<IAlert> {}
