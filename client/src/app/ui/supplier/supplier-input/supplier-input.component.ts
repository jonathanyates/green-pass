import {Component, OnInit, OnDestroy} from '@angular/core';
import {Supplier} from "../../company/company.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SaveSupplierAction} from "../../../store/company/company.actions";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {FormInputGroup} from "../../shared/components/dynamic-form/dynamic-form.model";
import {SupplierInputMapper} from "./supplier-input.mapper";
import {FormGroup} from "@angular/forms";
import {ISupplier} from "../../../../../../shared/interfaces/company.interface";

@Component({
  selector: 'gp-supplier-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
`
})
export class SupplierInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private supplier: ISupplier;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private supplierInputMapper: SupplierInputMapper) { }

    ngOnInit() {
    this.subscription = this.route.data
      .map((data: { supplier: ISupplier }) => data.supplier)
      .subscribe(supplier => {
        this.supplier = supplier;
        this.inputGroups = this.supplierInputMapper.mapTo(supplier);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(form: FormGroup) {
    let supplier = Object.assign(new Supplier(), this.supplier, this.supplierInputMapper.mapFrom(form));
    this.store.dispatch(new SaveSupplierAction(supplier));
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
