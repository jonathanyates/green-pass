import {Location} from '@angular/common';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Observable} from "rxjs";
import {NavigationLink} from "../../../store/navigation/navigation.model";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {SelectNavigationLinks} from "../../../store/navigation/navigation.selectors";
import {SelectCompany} from "../../../store/company/company.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Router} from "@angular/router";
import {environment} from "../../../../environments/environment";

declare let $: any;
declare let Ps: any;

@Component({
  selector: 'gp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  links: NavigationLink[];
  loggedIn: boolean;
  user: IUser;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    Observable.combineLatest(
      this.store.let(SelectNavigationLinks()),
      this.store.let(SelectLoggedInUser()),
      this.store.let(SelectCompany()),
      (links, user, company) => ({ links:links, user: user, company:company }))
      .subscribe(result => {
        this.user = result.user;
        let key = result.user ? result.user.role : 'public';

        // refactor this to some link transform class to replace :id with the appropriate id.
        this.links = result.links[key].map(link => {
          if (link.url && link.url.includes('company/:id') && result.company != null) {
            return Object.assign({}, link, {
              url: link.url.replace('company/:id', `company/${result.company._id}`)
            })
          }
          return link;
        });

        this.loggedIn = result.user != null;
      });
  }

  ngAfterViewInit(): void {
    $(".button-collapse").sideNav({
      closeOnClick: true
    });

    let el = document.querySelector('.custom-scrollbar');
    Ps.initialize(el);
  }

  navigate(link: NavigationLink) {
    if (link.url) {
      return this.router.navigate([link.url]);
    }

    if (link.href) {
      let root = environment.wwwRoot;
      window.location.href = `${root}/${link.href}`;
    }
  }

}
