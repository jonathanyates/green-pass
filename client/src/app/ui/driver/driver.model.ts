import { Address } from '../company/company.model';
import { User } from '../user/user.model';
import { IAddress } from '../../../../../shared/interfaces/address.interface';
import { IDocument } from '../../../../../shared/interfaces/document.interface';
import {
  IAllowedVehicles,
  IContact,
  ICpc,
  IDriver,
  IDriverInsurance,
  IDrivingLicence,
  IEndorsement,
  ITachographCard,
} from '../../../../../shared/interfaces/driver.interface';
import { IAssessment } from '../../../../../shared/interfaces/assessment.interface';
import { IDriverCompliance } from '../../../../../shared/interfaces/compliance.interface';
import { IVehicle } from '../../../../../shared/interfaces/vehicle.interface';
import { IFawCertificate } from '../../../../../shared/interfaces/common.interface';

export class Contact implements IContact {
  _id?: any;
  title?: string;
  forename: string;
  surname: string;
  address: IAddress = new Address();
  phone: string;
  mobile?: string;
  email?: string;
}

export class DrivingLicence implements IDrivingLicence {
  number: string;
  issueNumber?: number;
  status?: string;
  validFrom?: Date;
  validTo?: Date;
  endorsements?: IEndorsement[];
  points: number;
  allowedVehicles?: IAllowedVehicles;
  tachographCard?: ITachographCard;
  cpc?: ICpc;
  checkCode: string;
  checkDate: Date;
  scan?: {
    front?: string;
    back?: string;
  };
}

export class Driver implements IDriver {
  constructor(companyId?: string) {
    if (companyId) this._companyId = companyId;
  }

  _id?: any;
  _companyId: string;
  _user: User = new User();
  fullName?: string;
  address: IAddress = new Address();
  dateOfBirth: Date;
  gender: string;
  licenceNumber?: string;
  niNumber: string;
  phone: string;
  mobile?: string;
  licence: {
    checks: IDrivingLicence[];
    nextCheckDate?: Date;
  };
  nextOfKin: IContact;
  references: Array<IContact> = [];

  insuranceHistory?: IDriverInsurance[];

  fawRequired?: boolean;
  fawHistory?: IFawCertificate[];
  dbsExpiry?: Date;

  assessments?: IAssessment[];
  nextAssessmentDate?: Date;
  lastAssessmentUpdatedDate?: Date;
  lastAssessmentLoginDate?: Date;
  documents: IDocument[];
  vehicles?: Array<IVehicle>;
  ownedVehicle?: boolean;
  compliance: IDriverCompliance;
  removed?: boolean;

  // Sea Cadets
  pNumber?: string;
}
