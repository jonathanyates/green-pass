import {Router, NavigationStart} from "@angular/router";
import {BreadcrumbService} from "../../../../services/breadcrumb.service";
import {Store} from "@ngrx/store";
import {AppState, AppStates} from "../../../../store/app.states";
import {NavigationState} from "../../../../store/navigation/navigation.state";
import {OnInit, Component, OnDestroy} from "@angular/core";
import {Observable} from "rxjs";
import {Messenger} from "../../../../services/messenger";
import {Messages} from "../../../../shared/constants";
import {ICompany} from "../../../../../../../shared/interfaces/company.interface";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {SelectLoggedInUser} from "../../../../store/authentication/authentication.selectors";
import {Subscription} from "rxjs/Subscription";
import {SelectNavigationState} from "../../../../store/navigation/navigation.selectors";
import {Roles} from "../../../../../../../shared/constants";

@Component({
  selector: 'gp-breadcrumb',
  templateUrl: 'breadcrumb.component.html',
  styleUrls: ['breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  breadcrumbs: Observable<string[]>;
  showSearch: boolean;
  company: ICompany;
  user: IUser;
  isDriver: boolean;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private breadcrumbService: BreadcrumbService,
    private store: Store<AppState>,
    private messenger: Messenger) {
  }

  ngOnInit(): void {
    this.breadcrumbs = this.store.let(SelectNavigationState())
      .map((state:NavigationState) => state.breadcrumb);

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));

    this.subscriptions.push(this.store.let(SelectLoggedInUser())
      .subscribe(user => {
        this.user = user;
        this.isDriver = user && user.role === Roles.Driver;
      }));

    // reset show search to false to every navigation
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe(event => this.messenger.publish(Messages.ShowSearch, false));

    this.subscriptions.push(this.messenger.toObservable(Messages.ShowSearch)
      .subscribe(showSearch => this.showSearch = showSearch));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  navigateTo(url: string): void {
    this.router.navigateByUrl(url);
  }

  friendlyName(url: string): string {
    return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
  }
}
