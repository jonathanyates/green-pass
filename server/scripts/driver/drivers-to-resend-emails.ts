export const driversToResend = [
  'akim.polakovs@eddiversity.blackpool.sch.uk',
  'andrew.mell@eddiversity.blackpool.sch.uk',
  'angela.heald@eddiversity.blackpool.sch.uk',
  'anthony.riley@eddiversity.blackpool.sch.uk',
  'blake.greaves@eddiversity.blackpool.sch.uk',
  'bobby.moss@eddiversity.blackpool.sch.uk',
  'carolyn.lee@eddiversity.blackpool.sch.uk',
  'catherine.shevlin@eddiversity.blackpool.sch.uk',
  'clare.brown@eddiversity.blackpool.sch.uk',
  'dan.halton@eddiversity.blackpool.sch.uk',
  'danny.dicken@eddiversity.blackpool.sch.uk',
  'evie.thompson@eddiversity.blackpool.sch.uk',
  'fiona.shaw@eddiversity.blackpool.sch.uk',
  'heather.smith@eddiversity.blackpool.sch.uk',
  'helen.ward@eddiversity.blackpool.sch.uk',
  'janette.dickin@eddiversity.blackpool.sch.uk',
  'jill.fletcher@eddiversity.blackpool.sch.uk',
  'julie.catherall@eddiversity.blackpool.sch.uk',
  'karen.turner@eddiversity.blackpool.sch.uk',
  'kelly.hancock@eddiversity.blackpool.sch.uk',
  'kerry.ashton-a@eddiversity.blackpool.sch.uk',
  'lesley.winters@eddiversity.blackpool.sch.uk',
  'lorna.manning@eddiversity.blackpool.sch.uk',
  'michelle.mortimer@eddiversity.blackpool.sch.uk',
  'mike.byrom@eddiversity.blackpool.sch.uk',
  'peter.boldy@eddiversity.blackpool.sch.uk',
  'rachel.muller@eddiversity.blackpool.sch.uk',
  'rebecca.kenyon@eddiversity.blackpool.sch.uk',
  'sam.hancock@eddiversity.blackpool.sch.uk',
  'steve.jenkinson@eddiversity.blackpool.sch.uk',
  'vicky.leah@eddiversity.blackpool.sch.uk',
];