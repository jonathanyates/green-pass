import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IDriver} from "../../../../../shared/interfaces/driver.interface";
import {IFawCertificate} from '../../../../../shared/interfaces/common.interface';

export const ActionTypes = {
  SELECT_DRIVER_FAW: type('[Driver Faw] Select'),
  SELECT_DRIVER_FAW_SUCCESS : type('[Driver Faw] Select Success'),
  SAVE_DRIVER_FAW:   type('[Driver Faw] Save'),
  SAVE_DRIVER_FAW_SUCCESS :   type('[Driver Faw] Save Success'),
  CLEAR_DRIVER_FAW:  type('[Driver Faw] Clear')
};

/**
 * Driver Faw Actions
 */

export class SelectDriverFawAction implements Action {
  type = ActionTypes.SELECT_DRIVER_FAW;
  payload: { driverId: string, fawId: string };
  constructor(driverId: string, fawId: string) {
    this.payload = { driverId: driverId, fawId: fawId }
  }
}

export class SelectDriverFawSuccessAction implements Action {
  type = ActionTypes.SELECT_DRIVER_FAW_SUCCESS;
  constructor(public payload: IFawCertificate) { } // faw id
}

export class SaveDriverFawAction implements Action {
  type = ActionTypes.SAVE_DRIVER_FAW;
  payload: { driver: IDriver, faw: IFawCertificate };
  constructor(driver: IDriver, faw: IFawCertificate) {
    this.payload = { driver: driver, faw:faw }
  }
}

export class SaveDriverFawSuccessAction implements Action {
  type = ActionTypes.SAVE_DRIVER_FAW_SUCCESS;
  payload: { driver: IDriver, faw: IFawCertificate };
  constructor(driver: IDriver, faw: IFawCertificate) {
    this.payload = { driver: driver, faw:faw }
  }
}

export class ClearDriverFawAction implements Action {
  type = ActionTypes.CLEAR_DRIVER_FAW;
  payload = null;
  constructor() { } // faw id
}

export type Actions
  = SelectDriverFawSuccessAction
  | SaveDriverFawSuccessAction
  | ClearDriverFawAction;
