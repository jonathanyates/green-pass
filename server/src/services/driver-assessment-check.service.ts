import logger = require('winston');
import container from '../container/container.config';
import { TYPES } from '../container/container.types';
import schedule = require('node-schedule');
import { configureLogger } from './logger';
import { IDriverService } from '../drivers/driver.service';
import { IDriver } from '../../../shared/interfaces/driver.interface';

// perform checks for vehicles where the tax or mot date is due within the next 7 days.

configureLogger('driver-assessment_checks', 'GreenPass-Driver-Assessment-Check-Service');

logger.info('Starting Driver Assessment Check Service...');

const driverService = container.get<IDriverService>(TYPES.services.DriverService);

const cron = process.env.CRON ? process.env.CRON : '0 3 * * *'; // default run at 3am

logger.info('Scheduling Driver Assessment checks to run at Cron interval ' + cron);

schedule.scheduleJob(cron, () => {
  logger.info('Performing driver assessment checks...');

  driverService
    .driverAssessmentChecks()
    .then((drivers: IDriver[]) => {
      logger.info('Successfully completed Driver Assessment Checks.');
      if (drivers && drivers.length > 0) {
        logger.info(`${drivers.length} Driver assessments where updated.`);
      } else {
        logger.info('No driver assessments needed updating.');
      }
    })
    .catch((error) => {
      logger.error('Error performing driver assessment checks');
      logger.error(error.message);
    });
});

logger.info('Driver Assessment Check Service is running.');

/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */
