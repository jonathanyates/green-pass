import {IAlertSummary, IAlertType} from "../../../../../shared/interfaces/alert.interface";

export interface AlertsState {
  alertTypes: IAlertType[];
  alertSummary: IAlertSummary[];
  selected: IAlertSummary;
}
