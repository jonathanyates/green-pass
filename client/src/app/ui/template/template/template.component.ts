import {Component, Input, OnInit} from '@angular/core';
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'gp-template',
  template: `
  <gp-dynamic-content [content]="template && template.html ? template.html : null"></gp-dynamic-content>`
})

export class TemplateComponent implements OnInit {

  _template: ITemplate;

  @Input()
  set template(value: ITemplate) {
    if (value && value.html) {
      value.html = value.html.replace(/API_ROOT/g, environment.apiRoot + '/api')
    }
    this._template = value;
  }
  get template(): ITemplate {
    return this._template;
  }


  constructor() {
  }

  ngOnInit() {
  }
}
