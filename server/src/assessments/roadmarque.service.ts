import logger = require('winston');
import crypto = require('crypto');
import soap from 'soap';
import xml2js from 'xml2js';
import { injectable } from 'inversify';
import { jsonDeserialiser } from '../../../shared/utils/json.deserialiser';
import { IRoadMarqueDriver } from '../../../shared/interfaces/roadmarque-driver.interface';
import { toCamel } from '../shared/object.extensions';
import { IDriver } from '../../../shared/interfaces/driver.interface';
import serverConfig from '../server/server.config';
import { IRMDeepLogin, IRMLastUse, IRMUserRecord } from './roadmarque.model';
import codes from '../shared/error.codes';
import { ServerError } from '../shared/errors';

const deepLoginLinkMethods = {
  getLastRetrieveUserDetailsDate: 'GetLastRetrieveUserDetailsDate',
  getUserDetails: 'GetUserDetails',
  getUserLastLoginDate: 'GetUserLastLoginDate',
  doLogin: 'DoLogin',
};

export interface IRoadMarqueService {
  // RoadMarque asmx Soap Web Service API Methods
  addDriver(driver: IDriver, groupName?: string): Promise<any>;
  updateDriver(userId: number, driver: IDriver, groupName?: string): Promise<any>;
  getDrivers(searchFields?: any): Promise<IRoadMarqueDriver[] | IRoadMarqueDriver>;
  AssessmentInvite(refId: string): Promise<any>;

  // RoadMarque Deep Login Links API Methods
  getLastRetrieveUserDetailsDate(): Promise<IRMLastUse>;
  getUserDetails(userId: number): Promise<IRMUserRecord>;
  getUserLastLoginDate(userId: number, refId: string, password: string): Promise<IRMLastUse>;
  doLogin(refId: string): Promise<IRMDeepLogin>;
}

@injectable()
export class RoadMarqueService implements IRoadMarqueService {
  private passwordHash: string;
  private userCredentialsHeader: string;
  private soapOptions: any;

  constructor() {
    this.passwordHash = this.getHash(serverConfig.roadMarque.password).toUpperCase();
    this.userCredentialsHeader =
      `<UserCredentials xmlns="http://tempuri.org/">` +
      `<username>${serverConfig.roadMarque.username}</username>` +
      `<password>${serverConfig.roadMarque.password}</password>` +
      `</UserCredentials>`;

    //let PROXY_URL = 'http://a83288:Ev3r3st96-@ncproxy1:8080';

    this.soapOptions = {
      forceSoap12Headers: true,
      connection: 'keep-alive',
      //request: request.defaults({'proxy': PROXY_URL, 'timeout': 5000})
    };
  }

  // RoadMarque asmx Soap Web Service API Methods

  addDriver = (driver: IDriver, groupName?: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      soap.createClient(serverConfig.roadMarque.url, this.soapOptions, (err, client: any) => {
        if (err) {
          return reject(err);
        }

        client.addSoapHeader(this.userCredentialsHeader);

        const args: any = {
          inpDriverCriteria: {
            InputFields: {
              CompanySearchFields: {
                CustomerID: serverConfig.roadMarque.customerId,
              },
              BasicDetails: {
                Firstname: driver._user.forename,
                // Middlename: driver._user.middlename,
                Surname: driver._user.surname,
                RefID: driver._id.toString(),
                BirthDate: driver.dateOfBirth,
              },
            },
          },
        };

        if (groupName) {
          args.inpDriverCriteria.InputFields.BasicDetails.GroupName = groupName;
        }

        client.AddDriver(args, (err, response) => {
          if (err) {
            return reject(err);
          }

          const result = this.deserialise(response.AddDriverInternalResult.AddDriver);

          resolve(result);
        });
      });
    });
  };

  updateDriver = (userId: number, driver: IDriver, groupName?: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      soap.createClient(serverConfig.roadMarque.url, this.soapOptions, (err, client: any) => {
        if (err) {
          return reject(err);
        }

        client.addSoapHeader(this.userCredentialsHeader);

        const args: any = {
          inpDriverCriteria: {
            InputFields: {
              CompanySearchFields: {
                CustomerID: serverConfig.roadMarque.customerId,
              },
              DriverSearchFields: {
                UserID: userId,
              },
              BasicDetails: {
                Firstname: driver._user.forename,
                // Middlename: driver._user.middlename,
                Surname: driver._user.surname,
                RefID: driver._id.toString(),
                BirthDate: driver.dateOfBirth,
              },
            },
          },
        };

        if (groupName) {
          args.inpDriverCriteria.InputFields.BasicDetails.GroupName = groupName;
        }

        client.UpdateDriver(args, (err, response) => {
          if (err) {
            return reject(err);
          }

          const result = this.deserialise(response.UpdateDriverInternalResult.UpdateDriver);

          resolve(result);
        });
      });
    });
  };

  getDrivers = (searchFields?: any): Promise<IRoadMarqueDriver[] | IRoadMarqueDriver> => {
    return new Promise((resolve, reject) => {
      soap.createClient(serverConfig.roadMarque.url, this.soapOptions, (err, client: any) => {
        if (err) {
          return reject(err);
        }

        client.addSoapHeader(this.userCredentialsHeader);

        const args: any = {
          inpDriverCriteria: {
            InputFields: {
              CompanySearchFields: {
                CustomerID: serverConfig.roadMarque.customerId,
              },
            },
          },
        };

        if (searchFields) {
          args.inpDriverCriteria.InputFields.DriverSearchFields = searchFields;
        }

        client.GetDriver(args, (err, response) => {
          if (err) {
            return reject(err);
          }

          const drivers = this.deserialise(response.GetDriverInternalResult.GetDriver.CompanyDetails.DriverRecord);

          this.normalise(drivers);

          resolve(drivers);
        });
      });
    });
  };

  AssessmentInvite = (refId: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      soap.createClient(serverConfig.roadMarque.url, this.soapOptions, (err, client: any) => {
        if (err) {
          return reject(err);
        }

        client.addSoapHeader(this.userCredentialsHeader);

        const args: any = {
          inpInviteCriteria: {
            InputFields: {
              CompanySearchFields: {
                CustomerID: serverConfig.roadMarque.customerId,
              },
              DriverSearchFields: {
                RefID: refId,
              },
            },
          },
        };

        client.AssessmentInvite(args, (err, response) => {
          if (err) {
            return reject(err);
          }

          const result = this.deserialise(response.AssessmentInviteResult.AssessmentInvite);

          resolve(result);
        });
      });
    });
  };

  // RoadMarque Deep Login Links API Methods

  getLastRetrieveUserDetailsDate = (): Promise<IRMLastUse> => {
    const message = {
      inpCustomerID: serverConfig.roadMarque.customerId,
      inpUserID: serverConfig.roadMarque.resellerId,
      inpPassword: this.passwordHash,
    };

    return this.send(deepLoginLinkMethods.getLastRetrieveUserDetailsDate, message)
      .then((result) => {
        return result.envelope.body.getLastRetrieveUserDetailsDateResponse.getLastRetrieveUserDetailsDateResult;
      })
      .catch((error) => {
        const errorMsg = `Failed to call RoadMarque::getLastRetrieveUserDetailsDate with payload ${JSON.stringify(
          message
        )}. \nError: ${error.message}`;
        logger.error(errorMsg);
        throw ServerError(errorMsg, codes.INTERNAL_SERVER_ERROR);
      });
  };

  getUserDetails = (userId: number): Promise<IRMUserRecord> => {
    return this.getLastRetrieveUserDetailsDate().then((lastUse: IRMLastUse) => {
      if (!lastUse.success) {
        throw ServerError('Failed to get last use date for user details.', codes.INTERNAL_SERVER_ERROR);
      }

      // RMCustomerID + MD5HashOf(RMResellerPassword) + RMResellerUserID + LastRunDate + RMDLLPassword
      const hashStr =
        serverConfig.roadMarque.customerId +
        this.passwordHash +
        serverConfig.roadMarque.resellerId +
        lastUse.lastRun +
        serverConfig.roadMarque.dllPassword;
      const hash = this.getHash(hashStr).toUpperCase();

      const message = {
        inpCustomerID: serverConfig.roadMarque.customerId,
        inpUserID: serverConfig.roadMarque.resellerId,
        inpPassword: this.passwordHash,
        hashed: hash,
        returnAllUsers: false,
        specificUserID: userId,
      };

      return <Promise<IRMUserRecord>>this.send(deepLoginLinkMethods.getUserDetails, message).then((result) => {
        const success = result.envelope.body.getUserDetailsResponse.getUserDetailsResult.userRecord[0].success;

        if (!success) {
          throw ServerError('Failed to get user details.', codes.INTERNAL_SERVER_ERROR);
        }

        return result.envelope.body.getUserDetailsResponse.getUserDetailsResult.userRecord[1];
      });
    });
  };

  getUserLastLoginDate = (userId: number, refId: string, password: string): Promise<IRMLastUse> => {
    const message = {
      inpUserID: userId,
      inpRefID: refId,
      inpPassword: password,
    };

    return this.send(deepLoginLinkMethods.getUserLastLoginDate, message).then((result) => {
      return result.envelope.body.getUserLastLoginDateResponse.getUserLastLoginDateResult;
    });
  };

  doLogin = (refId: string): Promise<IRMDeepLogin> => {
    return this.getDrivers({ RefID: refId }).then((driver: IRoadMarqueDriver) => {
      if (!driver) {
        throw ServerError('Driver not found.', codes.NOT_FOUND);
      }

      return this.getUserDetails(driver.basicDetails.userId).then((userRecord: IRMUserRecord) => {
        return this.getUserLastLoginDate(userRecord.userID, userRecord.refID, userRecord.password).then(
          (lastUse: IRMLastUse) => {
            if (!lastUse.success) {
              throw ServerError('Failed to get last use date for user login.', codes.INTERNAL_SERVER_ERROR);
            }

            // RMUserID + RMReferenceID + RMPassword + lastLoginDate + RMDLLPassword
            const hashStr =
              userRecord.userID +
              userRecord.refID +
              userRecord.password +
              lastUse.lastRun +
              serverConfig.roadMarque.dllPassword;
            const hash = this.getHash(hashStr).toUpperCase();

            const message = {
              inpUserID: userRecord.userID,
              inpRefID: userRecord.refID,
              inpPassword: userRecord.password,
              hashed: hash,
            };

            return <Promise<IRMDeepLogin>>this.send(deepLoginLinkMethods.doLogin, message).then((result) => {
              return result.envelope.body.doLoginResponse.doLoginResult;
            });
          }
        );
      });
    });
  };

  getCustomers = (): Promise<any> => {
    return new Promise((resolve, reject) => {
      soap.createClient(serverConfig.roadMarque.url, this.soapOptions, (err, client: any) => {
        if (err) {
          return reject(err);
        }

        client.addSoapHeader(this.userCredentialsHeader);

        client.GetCustomers(null, (err, response) => {
          if (err) {
            return reject(err);
          }

          // let drivers = this.deserialise(response.GetCustomerResult.GetCustomers
          //   .Customer);

          const customers = response;

          //this.normalise(drivers);

          resolve(customers);
        });
      });
    });
  };

  private send(operation: string, message: any): Promise<any> {
    // let binding = new WSHttpBinding()
    //   , proxy = new Proxy(binding, serverConfig.roadMarque.dllUrl)
    //   , msg = this.createEnvelope(operation, message);

    return new Promise((resolve, reject) => {
      // proxy.send(msg, `http://tempuri.org/RMDLLIService/${operation}`, (response, ctx) => {
      //   if (ctx.statusCode != 200) {
      //     let error:any = new Error(`${operation} failed`);
      //     error.statusCode = ctx.statusCode;
      //     return reject(error);
      //   }
      //   parseString(response, {
      //     explicitArray: false,
      //     tagNameProcessors: [ stripPrefix, firstCharLowerCase, parseBooleans, parseNumbers  ]
      //   }, (err, result) => {
      //     if (err) {
      //       return reject(err);
      //     }
      //     return resolve(result);
      //   });
      // });
    });
  }

  private createEnvelope(operation: string, message: any) {
    const builder = new xml2js.Builder({ headless: true, rootName: 'Envelope' });

    const payload = {
      $: { xmlns: 'http://www.w3.org/2003/05/soap-envelope' },
      Header: {},
      Body: {
        [operation]: Object.assign({}, { $: { xmlns: 'http://tempuri.org/' } }, message),
      },
    };

    return builder.buildObject(payload);
  }

  private getHash(input: string) {
    return crypto.createHash('md5').update(input, 'ascii').digest('hex');
  }

  private deserialise(result: any) {
    if (result) {
      const res = toCamel(jsonDeserialiser.parse(JSON.stringify(result)));
      return res;
    }
    return result;
  }

  private normalise(roadMarqueDriver: IRoadMarqueDriver) {
    if (!roadMarqueDriver) return;

    if (roadMarqueDriver.aptitudeAssessments && roadMarqueDriver.aptitudeAssessments.assessmentResult) {
      if (!Array.isArray(roadMarqueDriver.aptitudeAssessments.assessmentResult)) {
        const results = [];
        results.push(roadMarqueDriver.aptitudeAssessments.assessmentResult);
        roadMarqueDriver.aptitudeAssessments.assessmentResult = results;
      }
    }

    if (
      roadMarqueDriver.aptitudeAssessments &&
      roadMarqueDriver.aptitudeAssessments.assessmentInvite &&
      roadMarqueDriver.aptitudeAssessments.assessmentInvite.testsRemaining &&
      roadMarqueDriver.aptitudeAssessments.assessmentInvite.testsRemaining.testDescription
    ) {
      if (!Array.isArray(roadMarqueDriver.aptitudeAssessments.assessmentInvite.testsRemaining.testDescription)) {
        const results = [];
        results.push(roadMarqueDriver.aptitudeAssessments.assessmentInvite.testsRemaining.testDescription);
        roadMarqueDriver.aptitudeAssessments.assessmentInvite.testsRemaining.testDescription = results;
      }
    }

    if (roadMarqueDriver.trainingModules && roadMarqueDriver.trainingModules.intervention) {
      if (!Array.isArray(roadMarqueDriver.trainingModules.intervention)) {
        const results = [];
        results.push(roadMarqueDriver.trainingModules.intervention);
        roadMarqueDriver.trainingModules.intervention = results;
      }
    }
  }
}
