import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {IAlertSummary, IAlertType} from "../../../../../shared/interfaces/alert.interface";

export const ActionTypes = {
  LOAD_SUMMARY:                 type('[Alerts] Load Summary'),
  LOAD_SUMMARY_SUCCESS:         type('[Alerts] Load Summary Success'),
  LOAD_SELECTED_ALERT_SUMMARY:        type('[Alerts] Load Selected Alert Summary'),
  LOAD_SELECTED_SUMMARY_ALERT_SUCCESS: type('[Alerts] Load Selected Alert Summary Success'),
  LOAD_ALERT_TYPES:           type('[Alert Types] Load'),
  LOAD_ALERT_TYPES_SUCCESS:   type('[Alert Types] Load Success'),
  SELECT_ALERT_TYPES:         type('[Alert Types] Select'),
  SELECT_ALERT_TYPES_SUCCESS: type('[Alert Types] Select Success'),
  SAVE_ALERT_TYPE:            type('[Alert Type] Save'),
  SAVE_ALERT_TYPE_SUCCESS:    type('[Alert Type] Save Success')
};

/**
 * Load Alert Summary Actions
 */
export class LoadAlertSummaryAction implements Action {
  type = ActionTypes.LOAD_SUMMARY;
  constructor() { }
}

export class LoadAlertSummarySuccessAction implements Action {
  type = ActionTypes.LOAD_SUMMARY_SUCCESS;
  constructor(public payload: IAlertSummary[]) { }
}

export class LoadSelectedAlertSummaryAction implements Action {
  type = ActionTypes.LOAD_SELECTED_ALERT_SUMMARY;
  constructor(public  payload: string) { } // company Id
}

export class LoadSelectedAlertSummarySuccessAction implements Action {
  type = ActionTypes.LOAD_SELECTED_SUMMARY_ALERT_SUCCESS;
  constructor(public payload: IAlertSummary) { }
}

/**
 * AlertType Actions
 */
export class SelectAlertTypesAction implements Action {
  type = ActionTypes.SELECT_ALERT_TYPES;
  constructor() { }
}

export class SelectAlertTypesSuccessAction implements Action {
  type = ActionTypes.SELECT_ALERT_TYPES_SUCCESS;
  constructor(public payload: IAlertType[]) { }
}

export class LoadAlertTypesAction implements Action {
  type = ActionTypes.LOAD_ALERT_TYPES;
  constructor() { }
}

export class LoadAlertTypesSuccessAction implements Action {
  type = ActionTypes.LOAD_ALERT_TYPES_SUCCESS;
  constructor(public payload: IAlertType[]) { }
}

export class SaveAlertTypeAction implements Action {
  type = ActionTypes.SAVE_ALERT_TYPE;
  constructor(public payload: IAlertType) { }
}

export class SaveAlertTypeSuccessAction implements Action {
  type = ActionTypes.SAVE_ALERT_TYPE_SUCCESS;
  constructor(public payload: IAlertType[]) { }
}

export type Actions
  = LoadAlertSummarySuccessAction
  | LoadAlertTypesSuccessAction
  | LoadSelectedAlertSummaryAction
  | LoadSelectedAlertSummarySuccessAction
  | SelectAlertTypesSuccessAction
  | SaveAlertTypeSuccessAction
  | LogoutAction;
