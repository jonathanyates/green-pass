export type IDvsaMotEnquiryReponse = IMotEnquiry[];

export interface IMotEnquiry {
  registration: string;
  make: string;
  model: string;
  firstUsedDate: string;
  fuelType: string;
  primaryColour: string;
  vehicleId: string;
  registrationDate: string;
  manufactureDate: string;
  manufactureYear: string;
  engineSize: string;
  motTestDueDate: string;
  motTests: IMotTest[];
}

export interface IMotTest {
  completedDate: string;
  testResult: string;
  expiryDate?: string;
  odometerValue: string;
  odometerUnit: string;
  odometerResultType: string;
  motTestNumber: string;
  rfrAndComments: IRfrAndComment[];
}

export interface IRfrAndComment {
  text: string;
  type: string;
  dangerous: boolean;
}
