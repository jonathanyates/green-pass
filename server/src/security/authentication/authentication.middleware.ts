import logger = require('winston');
import passport = require('passport');
import serverConfig from '../../server/server.config';
import { inject, injectable } from 'inversify';
import { Strategy as LocalStrategy } from 'passport-local';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IAuthenticationMiddleware } from './authentication.middleware.interface';
import { TYPES } from '../../container/container.types';
import { Handler } from 'express-serve-static-core';
import { IRepository } from '../../db/db.repository';
import { IUserModel } from '../../users/user.schema';
import { ICompanyService } from '../../companies/company.service.interface';

@injectable()
export class AuthenticationMiddleware implements IAuthenticationMiddleware {
  requireAuth: Handler;

  constructor(
    @inject(TYPES.repositories.UserRepository) private userRepository: IRepository<IUserModel>,
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService
  ) {
    this.configure();
  }

  private configure() {
    const localOptions = { usernameField: 'username' };

    // Setting up local login strategy
    const localLogin = new LocalStrategy(localOptions, this.localVerify);

    const jwtOptions = {
      // Telling Passport to check authorization headers for JWT
      jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
      // Telling Passport where to find the secret
      secretOrKey: serverConfig.authentication.secret,
    };

    // Setting up JWT login strategy
    const jwtLogin = new Strategy(jwtOptions, this.jwtVerify);

    passport.use(jwtLogin);
    passport.use(localLogin);

    this.requireAuth = passport.authenticate('jwt', { session: false });
  }

  private localVerify = (username, password, done) => {
    const loginErrors = {
      username: 'Your username was not found.',
      password: 'Your password is incorrect.',
      subscriptionExpired: 'Unable to login. Subscription has expired.',
    };

    this.userRepository
      .findOne({ username: username })
      .then((user) => {
        if (!user) {
          return done(null, false, { error: loginErrors.username, fields: ['username'] });
        }

        user.comparePassword(password, (err, isMatch) => {
          if (err) {
            return done(err);
          }
          if (!isMatch) {
            return done(null, false, { error: loginErrors.password, fields: ['password'] });
          }

          if (user._companyId) {
            this.companyService.get(user._companyId).then((company) => {
              if (!company.subscription.active) {
                return done(null, false, { error: loginErrors.subscriptionExpired });
              }
              return done(null, user);
            });
          } else {
            return done(null, user);
          }
        });
      })
      .catch((err) => {
        logger.error('An error occured in localVerify.', err);
        done(err);
      });
  };

  private jwtVerify = (payload, done) => {
    this.userRepository
      .get(payload._id)
      .then((user) => {
        if (user) {
          if (user._companyId) {
            this.companyService.get(user._companyId).then((company) => {
              if (!company.subscription.active) {
                return done(null, false);
              }
              return done(null, user);
            });
          } else {
            return done(null, user);
          }
        } else {
          done(null, false);
        }
      })
      .catch((err) => {
        logger.error('An error occured in jwtVerify.', err);
        done(err);
      });
  };
}
