import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.states';
import {pagination} from '../../../shared/constants';
import {IVehicle} from '../../../../../../shared/interfaces/vehicle.interface';
import {SelectCompany} from '../../../store/company/company.selectors';
import {SelectVehicle} from '../../../store/vehicles/vehicle.selectors';
import {UpdateVehicleSuccessAction} from '../../../store/vehicles/vehicle.actions';
import {ModalComponent} from '../../shared/components/modal/model.component';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {SelectDriversPagination} from '../../../store/drivers/driver.selectors';
import {IDriver} from '../../../../../../shared/interfaces/driver.interface';
import {IDynamicList} from '../../../../../../shared/models/dynamic-list.model';
import {Subject} from 'rxjs/Subject';
import {BusyAction} from '../../../store/progress/progress.actions';
import {ClearDriverListAction, LoadDriversAction} from '../../../store/drivers/driver.actions';
import {DriverListBuilder} from '../../../../../../shared/models/driver-list.builder';
import {VehicleService} from '../../../services/vehicle.service';

@Component({
  templateUrl: './vehicle-driver-select.component.html'
})
export class VehicleDriverSelectComponent implements OnInit, OnDestroy {

  vehicle: IVehicle;
  company: ICompany;
  drivers: IDriver[];

  total: number;
  page: number;
  itemsPerPage: number;
  list: IDynamicList;
  selectedDriver: IDriver;
  searchTerm$ = new Subject<string>();
  error: string;
  message: string;

  private searchCriteria: any;
  private subscriptions = [];

  @ViewChild(ModalComponent) modal: ModalComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private vehicleService: VehicleService,
              private store: Store<AppState>) {
  }

  ngOnInit() {
    this.drivers = null;
    this.itemsPerPage = pagination.pageSize;
    this.store.dispatch(new ClearDriverListAction());

    this.subscriptions.push(this.store.let(SelectDriversPagination())
      .filter(vehicles => vehicles !== null)
      .subscribe(results => {
        this.drivers = results.drivers
          ? results.drivers.filter(driver =>
            this.vehicle.drivers == null || !this.vehicle.drivers.some(vehicleDrivers => vehicleDrivers._id === driver._id))
          : [];

        if (this.drivers.length === 0 && this.searchCriteria) {
          this.message = 'No matches found, driver already assigned to vehicle, or does not exist on the system.';
        }

        this.store.let(SelectCompany())
          .filter(company => company != null)
          .subscribe(company => {
            this.company = company;
            this.list = DriverListBuilder.getList(this.drivers, company.subscription, company.settings);
          });

        this.total = results.total;
        this.page = results.page;
      }));

    this.subscriptions.push(this.store.let(SelectVehicle())
      .subscribe(vehicle => this.vehicle = vehicle));

    this.subscriptions.push(this.searchTerm$
      .map(term => term != null ? term.trim() : '')
      .subscribe(term => this.search(term)));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onSearchChanged(term: string) {
    this.searchTerm$.next(term);
  }

  checkTerm(term: string) {
    if (!term) {
      this.drivers = null;
      return;
    }
  }

  onPageChange(page: number) {
    if (this.searchCriteria) {
      this.store.dispatch(new LoadDriversAction(this.company._id, page, this.itemsPerPage, this.searchCriteria));
      return;
    }

    this.store.dispatch(new LoadDriversAction(this.company._id, page, this.itemsPerPage));
  }

  search(term: string) {

    this.message = null;
    this.searchCriteria = {};

    const regex = `${term.split('').reduce((previousValue, currentValue) => previousValue + '.*' + currentValue)}`;
    if (term) {
      this.searchCriteria.name = regex;
    }

    this.store.dispatch(new LoadDriversAction(this.company._id, 1, this.itemsPerPage, this.searchCriteria))
  }

  select(driver: IDriver) {
    if (!driver) {
      return;
    }
    this.modal.open();
    this.selectedDriver = driver;
  }

  addConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedDriver) {

      this.error = null;
      this.store.dispatch(new BusyAction());

      this.vehicleService.addDriver(this.vehicle, this.selectedDriver._id)
        .subscribe(vehicle => {
          this.store.dispatch(new BusyAction(false));
          this.store.dispatch(new UpdateVehicleSuccessAction(vehicle));
          this.back();
        }, error => {
          this.store.dispatch(new BusyAction(false));
          this.error = `Unable to add Driver. ${error.message}`;
        })
    }
    this.selectedDriver = null;
  }

  back() {
    return this.router.navigate(['../'], { relativeTo:  this.route});
  }
}
