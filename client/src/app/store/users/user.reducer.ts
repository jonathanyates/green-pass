import * as actions from "./user.actions";
import {UsersState} from "./users.state";
import {ActionTypes} from "../authentication/authentication.actions";

export const initialState: UsersState = {
  list: null,
  selected: null
};

export function UsersReducer(state: UsersState = initialState, action: actions.Actions): UsersState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_SUCCESS:
        return {
          list: action.payload,
          selected: null
        };

      case actions.ActionTypes.LOAD_USER_SUCCESS:
      case actions.ActionTypes.SELECT_USER_SUCCESS:
        return {
          list: state.list,
          selected: action.payload
        };

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }
  };
