import 'reflect-metadata';
import { TYPES } from '../src/container/container.types';
import { IUser } from '../../shared/interfaces/user.interface';
import container from '../src/container/container.config';
import { IUserService } from '../src/users/user.service';
import { ICompanyRepository } from '../src/companies/company.repository';

const userService = container.get<IUserService>(TYPES.services.UserService);
const companyRepository = container.get<ICompanyRepository>(TYPES.repositories.CompanyRepository);

const createUsers = async () => {
  try {
    const company = await companyRepository.findOne({ name: 'Green Pass Compliance Ltd' });

    const users = <Array<IUser>>[
      {
        forename: 'Jonathan',
        surname: 'Yates',
        email: 'jonathan.yates@hotmail.com',
        role: 'company',
      },
      // {
      //   forename: 'Lee',
      //   surname: 'Dickinson',
      //   email: 'lee.dickinson@greenpasscompliance.co.uk',
      //   role: 'company',
      // },
    ];

    const newUsers = await Promise.all(users.map((user) => userService.add(company._id, user)));

    console.log(`${newUsers.length} Users created successfully.`);
    process.exit();
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

createUsers();
