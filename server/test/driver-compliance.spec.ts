import '../../shared/extensions/date.extensions';
import { Driver } from '../../client/src/app/ui/driver/driver.model';
import { DriverCompliance } from '../../shared/models/driver-compliance.model';
import { IDocument } from '../../shared/interfaces/document.interface';
import { IAssessment, IOnLineAssessment } from '../../shared/interfaces/assessment.interface';
import { IDriverInsurance, IDrivingLicence } from '../../shared/interfaces/driver.interface';
import { Vehicle } from '../../client/src/app/ui/vehicle/vehicle.model';
import { VehicleTypes } from '../../shared/interfaces/constants';
import { subscriptionNames, subscriptions } from '../../shared/interfaces/subscription.interface';
import { defaultSettingsFactory } from '../../shared/interfaces/company-settings.factory';

describe('DriverCompliance', () => {
  describe('construct', () => {
    it('expect driver to be none compliant', () => {
      const driver = new Driver();
      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: subscriptions.find((x) => x.name == subscriptionNames.gold),
      };
      const driverCompliance = new DriverCompliance(driver, company);
      expect(driverCompliance.compliant).toBeFalsy();
    });

    it('expect driver to have compliant driving licence', () => {
      const driver = new Driver();
      driver.licenceNumber = 'YATES607198JP9LK';
      driver.licence = {
        checks: [
          <IDrivingLicence>{
            validTo: new Date().addMonths(12),
            points: 3,
          },
        ],
        nextCheckDate: new Date().addMonths(12),
      };

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: subscriptions.find((x) => x.name == subscriptionNames.gold),
      };

      const driverCompliance = new DriverCompliance(driver, company);
      expect(driverCompliance.compliant).toBeFalsy();
      expect(driverCompliance.drivingLicence.compliant).toBeTruthy();
    });

    it('expect driver to have compliant documents', () => {
      const driver = new Driver();

      driver.documents = [
        <IDocument>{ status: 'completed', signed: true },
        <IDocument>{ status: 'completed', signed: true },
        <IDocument>{ status: 'completed', signed: true },
        <IDocument>{ status: 'completed', signed: true },
      ];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: subscriptions.find((x) => x.name == subscriptionNames.gold),
      };

      const driverCompliance = new DriverCompliance(driver, company);

      expect(driverCompliance.compliant).toBeFalsy();
      expect(driverCompliance.documents.total).toEqual(4);
      expect(driverCompliance.documents.compliant).toBeTruthy();
      expect(driverCompliance.documents.percentage).toEqual(100);
      expect(driverCompliance.documents.completed).toEqual(4);
    });

    it('expect driver to have compliant assessments', () => {
      const driver = new Driver();

      driver.assessments = [
        <IAssessment>{
          refId: 'test',
          onLine: {
            tests: [
              <IOnLineAssessment>{
                complete: true,
                description: 'test',
                endDate: new Date(),
                riskLevel: 'Low',
                startDate: new Date(),
                score: 100,
                _id: 'test',
              },
            ],
            inviteDate: new Date(),
            startDate: new Date(),
            overallRiskLevel: 'Low',
          },
        },
      ];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: subscriptions.find((x) => x.name == subscriptionNames.gold),
      };

      const driverCompliance = new DriverCompliance(driver, company);
      expect(driverCompliance.compliant).toBeFalsy();
      expect(driverCompliance.assessments.compliant).toBeTruthy();
    });

    it('expect driver to be compliant when only on-road training is required and no assessment are assigned', () => {
      const driver = new Driver();

      driver.assessments = [];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: {
          name: subscriptionNames.bronze,
          resources: {
            mandates: true,
            licenceCheck: true,
            onLineAssessments: false,
            onRoadTraining: true,
            alerts: 0,
            presentations: false,
            workshops: false,
          },
        },
      };

      const driverCompliance = new DriverCompliance(driver, company);
      return expect(driverCompliance.assessments.compliant).toBeTruthy();
    });

    it('expect driver to be compliant when only on-road training is required and on-road training assessment is assigned but not completed', () => {
      const driver = new Driver();

      driver.assessments = [
        <IAssessment>{
          onRoad: {
            inviteDate: new Date(),
          },
        },
      ];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: {
          name: subscriptionNames.bronze,
          resources: {
            mandates: true,
            licenceCheck: true,
            onLineAssessments: false,
            onRoadTraining: true,
            alerts: 0,
            presentations: false,
            workshops: false,
          },
        },
      };

      const driverCompliance = new DriverCompliance(driver, company);
      return expect(driverCompliance.assessments.compliant).toBeFalsy();
    });

    it('expect driver to be compliant when only on-road training is required and on-road training assessment is assigned and completed', () => {
      const driver = new Driver();

      driver.assessments = [
        <IAssessment>{
          onRoad: {
            inviteDate: new Date(),
            assessedBy: 'J Yates',
            filename: 'test assessment',
            result: 'Pass',
            complete: true,
          },
        },
      ];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: {
          name: subscriptionNames.bronze,
          resources: {
            mandates: true,
            licenceCheck: true,
            onLineAssessments: false,
            onRoadTraining: true,
            alerts: 0,
            presentations: false,
            workshops: false,
          },
        },
      };

      const driverCompliance = new DriverCompliance(driver, company);
      return expect(driverCompliance.assessments.compliant).toBeTruthy();
    });

    it('expect driver to have compliance insurance', function () {
      const driver = new Driver();
      const vehicle = new Vehicle();
      vehicle._id = 'test vehicle';
      vehicle.type = VehicleTypes.grey;
      driver.vehicles = [vehicle];
      driver.insuranceHistory = [
        <IDriverInsurance>{
          validFrom: new Date().addMonths(-12),
          validTo: new Date().addDays(1),
          file: 'insurance file',
          filename: 'insurance.png',
          fileId: 'insurance',
        },
      ];

      const company: any = {
        settings: defaultSettingsFactory(),
        subscription: subscriptions.find((x) => x.name == subscriptionNames.gold),
      };

      const driverCompliance = new DriverCompliance(driver, company);
      expect(driverCompliance.compliant).toBeFalsy();
      expect(driverCompliance.insurance).not.toBeNull();
      expect(driverCompliance.insurance).toBeTruthy();
    });
  });
});
