import 'reflect-metadata';
import { DvlaService } from '../../src/dvla/service/dvla.service';

const drivingLicenceNumber = 'YATES607198JP9LK',
  nationalInsuranceNumber = 'NR071159A',
  postCode = 'PR31YF';

const dvlaService = new DvlaService();

dvlaService
  .generateCheckCode(drivingLicenceNumber, nationalInsuranceNumber, postCode)
  .then((checkCode) => {
    console.log('CheckCode = ', checkCode);
    return dvlaService.drivingLicenceCheck(drivingLicenceNumber, checkCode);
  })
  .then((result) => {
    console.log('drivingLicenceCheck results: ', result);
    return result;
  })
  .catch((error) => {
    console.log(error);
  });
