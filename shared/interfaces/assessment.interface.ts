
export interface IAssessmentResult {
  testCode: string,
  testDescription: string,
  testScore: number,
  riskLevel: string,
  testStartDate: Date,
  testEndDate: Date,
}

export interface ITrainingModule {
  createdDate: Date,
  trainingProvider: string,
  trainingRecommendationType: string,
  subTrainingType: string,
  arrangedDate: Date,
  arrangedBy: string,
  contactDate: Date,
  startDate: Date,
  completedDate: Date,
  details: string,
  reasonDetails: string,
  satisfactory: string,
  testScore: number,
  riskLevel: string
}

export interface IAssessmentInvite {
  assessmentInviteDate: Date,
  assessmentStartDate: Date,
  testsRemaining: string[]
}

export interface IOnLineAssessment {
  _id?: any,
  testCode?: string,
  description: string,
  score: number,
  riskLevel: string,
  startDate?: Date,
  endDate?: Date,
  complete: boolean
}

export interface IOnRoadAssessment {
  _id?: any,
  inviteDate: Date,
  assessmentDate?: Date,
  assessedBy?: string,
  result?: string,
  complete: boolean,
  filename?: string,
  fileType?: string,
  fileId?: string,
  file?: any
}

export interface IAssessment {
  _id?: any,
  refId?: string,
  onLine?: {
    tests: IOnLineAssessment[],
    inviteDate?: Date,
    startDate?: Date,
    overallRiskLevel: string
  },
  trainingModules?: ITrainingModule[],
  onRoad?: IOnRoadAssessment,
  completedDate?: Date,
  complete: boolean,
  lastLoginDate?: Date,
  lastUpdatedDate?: Date
}