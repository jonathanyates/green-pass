import logger = require('winston');
import mongoose, { LeanDocument } from 'mongoose';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { inject, injectable } from 'inversify';
import { IRepository } from '../db/db.repository';
import { IVehicleModel } from './vehicle.schema';
import { IVehicle } from '../../../shared/interfaces/vehicle.interface';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import { IDriver } from '../../../shared/interfaces/driver.interface';
import { VehicleCompliance } from '../../../shared/models/vehicle-compliance.model';
import { IDriverVehicleMapRepository } from '../drivers/driver-vehicle-map.repository';
import { DriverCompliance } from '../../../shared/models/driver-compliance.model';
import { ICompany } from '../../../shared/interfaces/company.interface';
import { ICompanyRepository } from '../companies/company.repository';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

export interface IVehicleRepository extends IRepository<IVehicle> {
  count(criteria: any);
}

@injectable()
export class VehicleRepository implements IVehicleRepository {
  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.repositories.DriverVehicleMapRepository)
    private driverVehicleMapRepository: IDriverVehicleMapRepository,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  getAll(): Promise<IVehicle[]> {
    return <Promise<IVehicle[]>>this.dbContext.vehicle
      .find({ removed: false })
      .lean()
      .exec()
      .then((vehicles) => Promise.all(vehicles.map((vehicle) => this.populateDrivers(vehicle))))
      .then((vehicles: IVehicle[]) => this.addCompliances(vehicles));
  }

  get(id: string): Promise<IVehicle> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid vehicle id.', codes.BAD_REQUEST));
    }

    return this.dbContext.vehicle
      .findById(id)
      .lean()
      .exec()
      .then((vehicle) => this.populateDrivers(vehicle))
      .then((vehicle: IVehicle) => this.addCompliance(vehicle));
  }

  findOne(criteria: any): Promise<IVehicle> {
    criteria.removed = false;
    return this.dbContext.vehicle
      .findOne(criteria)
      .lean()
      .exec()
      .then((vehicle) => this.populateDrivers(vehicle))
      .then((vehicle: IVehicle) => this.addCompliance(vehicle));
  }

  count(criteria: any) {
    criteria.removed = false;
    return this.dbContext.vehicle.count(criteria).exec();
  }

  find(criteria: any, page?: number, pageSize = 10): Promise<IVehicle[]> {
    criteria.removed = false;

    if (page) {
      const skip = (page - 1) * pageSize;
      return <Promise<IVehicle[]>>this.dbContext.vehicle
        .find(criteria)
        .limit(pageSize)
        .skip(skip)
        .lean()
        .exec()
        .then((vehicles) => Promise.all(vehicles.map((vehicle) => this.populateDrivers(vehicle))))
        .then((vehicles: IVehicle[]) => {
          return this.addCompliances(vehicles);
        });
    }

    return this.dbContext.vehicle
      .find(criteria)
      .lean<IVehicle[]>()
      .exec()
      .then((vehicles) => Promise.all(vehicles.map((vehicle) => this.populateDrivers(vehicle))))
      .then((vehicles: IVehicle[]) => {
        return this.addCompliances(vehicles);
      });
  }

  private populateDrivers(vehicleModel: LeanDocument<IVehicle>): Promise<LeanDocument<IVehicle>> {
    if (!vehicleModel || !vehicleModel._id) {
      return Promise.resolve(vehicleModel);
    }

    return Promise.all([
      this.driverVehicleMapRepository.getVehicleDrivers(vehicleModel._id),
      this.companyRepository.get(vehicleModel._companyId),
    ]).then((results) => {
      const drivers: IDriver[] = results[0];
      const company: ICompany = results[1];
      drivers.forEach((driver) => (driver.compliance = new DriverCompliance(driver, company)));
      vehicleModel.drivers = drivers;
      return vehicleModel;
    });
  }

  private addCompliance(vehicle: IVehicle): Promise<IVehicle> {
    if (vehicle) {
      return this.companyRepository.get(vehicle._companyId).then((company) => {
        vehicle.compliance = new VehicleCompliance(vehicle, company.settings);
        return vehicle;
      });
    }
    return Promise.resolve(vehicle);
  }

  private addCompliances(vehicles: IVehicle[]): Promise<IVehicle[]> {
    if (vehicles && vehicles.length > 0) {
      return this.companyRepository.get(vehicles[0]._companyId).then((company) => {
        return vehicles.map((vehicle) => {
          vehicle.compliance = new VehicleCompliance(vehicle, company.settings);
          return vehicle;
        });
      });
    }
    return Promise.resolve(vehicles);
  }

  add<T extends IVehicle>(vehicle: IVehicle): Promise<IVehicle> {
    const model = new this.dbContext.vehicle(vehicle);

    return model
      .save()
      .then((vehicle) => this.addCompliance(vehicle))
      .then((vehicle) => {
        logger.info(`Vehicle ${vehicle._id} added.`);
        return this.messageService
          .send(queueNames.companyComplianceUpdate, vehicle._companyId)
          .then((sent) => vehicle)
          .catch((err) => vehicle);
      });
  }

  update<T extends IVehicle>(vehicle: IVehicle): Promise<IVehicle> {
    return this.dbContext.vehicle
      .findByIdAndUpdate(vehicle._id, vehicle, { new: true })
      .exec()
      .then((vehicle) => this.populateDrivers(vehicle))
      .then((vehicle) => this.addCompliance(vehicle as IVehicle))
      .then((vehicle) => {
        logger.info(`Vehicle ${vehicle._id} updated.`);
        return this.messageService
          .send(queueNames.companyComplianceUpdate, vehicle._companyId)
          .then((sent) => vehicle)
          .catch((err) => vehicle);
      });
  }

  remove<T extends IVehicle>(vehicleId: string): Promise<IVehicleModel> {
    return this.dbContext.vehicle
      .findByIdAndUpdate(vehicleId, { removed: true }, { new: true })
      .exec()
      .then((vehicle) => {
        logger.info(`Vehicle ${vehicleId} removed.`);
        return this.messageService
          .send(queueNames.companyComplianceUpdate, vehicle._companyId)
          .then((sent) => vehicle)
          .catch((err) => vehicle);
      });
  }
}
