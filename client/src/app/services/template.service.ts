import { Injectable } from '@angular/core';
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpClient} from "@angular/common/http";
import {ITemplate} from "../../../../shared/interfaces/document.interface";
import {ICompany} from "../../../../shared/interfaces/company.interface";
import {IDriver} from "../../../../shared/interfaces/driver.interface";

@Injectable()
export class TemplateService extends HttpBaseService {

  private apiUrl = apiPath + '/templates/';
  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getTemplates(companyId?: string): Observable<ITemplate[]> {

    this.authorize();

    if (companyId) {
      return this.getCompanyTemplates(companyId)
    }

    return this.httpClient.get<ITemplate[]>(this.apiUrl, this.getOptions())
      .map(res => this.handleResult<ITemplate[]>(res))
      .catch(this.handleError);
  }

  private  getCompanyTemplates(companyId: string): Observable<ITemplate[]> {
    return this.httpClient.get<ITemplate[]>(`${this.companiesUrl}${companyId}/templates`, this.getOptions())
      .map(res => this.handleResult<ITemplate[]>(res))
      .catch(this.handleError);
  }

  getTemplate(templateId: string): Observable<ITemplate> {

    this.authorize();

    let url = `${this.apiUrl}${templateId}`;

    return this.httpClient.get<ITemplate>(url, this.getOptions())
      .map(res => this.handleResult<ITemplate>(res))
      .catch(this.handleError);
  }

  getCompanyTemplate(companyId: string, templateId: string): Observable<ITemplate> {

    this.authorize();

    let url = `${this.companiesUrl}${companyId}/templates/${templateId}`;

    return this.httpClient.get<ITemplate>(url, this.getOptions())
      .map(res => this.handleResult<ITemplate>(res))
      .catch(this.handleError);
  }

  saveTemplates(companyId: string, templates: string[]): Observable<ICompany> {

    this.authorize();

    return this.httpClient.put<ICompany>(`${this.companiesUrl}${companyId}/templates`, templates, this.getOptions())
      .map(res => this.handleResult<ICompany>(res))
      .catch(this.handleError);
  }

  saveTemplate(template: ITemplate): Observable<ITemplate> {
    this.authorize();

    if (template._companyId) {
      return this.saveCompanyTemplate(template);
    }

    if (template._id) {
      return this.httpClient.put<ITemplate>(`${this.apiUrl}${template._id}`, template, this.getOptions())
        .map(response => this.handleResult<ITemplate>(response))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<ITemplate>(apiPath + '/templates', template, this.getOptions())
        .map(response => this.handleResult<ITemplate>(response))
        .catch(this.handleError);
    }
  }

  private saveCompanyTemplate(template: ITemplate): Observable<ITemplate> {
    this.authorize();

    if (template._id) {
      return this.httpClient.put<ITemplate>(`${this.companiesUrl}${template._companyId}/templates/${template._id}`, template, this.getOptions())
        .map(response => this.handleResult<ITemplate>(response))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<ITemplate>(`${this.companiesUrl}${template._companyId}/templates`, template, this.getOptions())
        .map(response => this.handleResult<ITemplate>(response))
        .catch(this.handleError);
    }
  }

}
