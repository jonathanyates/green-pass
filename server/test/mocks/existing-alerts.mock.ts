import {ObjectID} from "mongodb";

export const existingAlerts = [{
  "_id": "59413b8e5cbc02844b1c819a",
  "_companyId": "012345678901234567891000",
  "alertType": {
    "_id": "59413b7e65dd129c312e0445",
    "reportType": {
      "_id": "59413b7465dd129c312e0443",
      "name": "None Compliant Vehicles",
      "query": "getNoneCompliantVehicles",
      "category": "Vehicle",
      "__v": 0
    },
    "__v": 0,
    "priority": "High"
  },
  "created": new Date("2017-06-14T13:35:10.462Z"),
  "status": "Active",
  "driver": null,
  "vehicle": {
    "_id": new ObjectID("59413b439c5335e84e9a865e"),
    "compliance": {
      "compliant": false,
      "tax": true,
      "mot": true,
      "service": false,
      "inspection": true,
      "vehicleCheck": true,
      "_id": "59413b439c5335e84e9a8680"
    },
    "updatedAt": new Date("2017-06-14T13:33:55.970Z"),
    "createdAt": new Date("2017-06-14T13:33:55.910Z"),
    "_companyId": "012345678901234567891000",
    "make": "FORD",
    "model": "TRANSIT 125 T280 TREND FWD",
    "registrationNumber": "WV13 UXU",
    "annualMileage": 26367,
    "inspectionIntervalType": "A",
    "taxDueDate": new Date("2017-10-01T00:00:00.000Z"),
    "motDueDate": new Date("2017-10-17T00:00:00.000Z"),
    "dateOfFirstRegistration": new Date("2013-03-21T00:00:00.000Z"),
    "yearOfManufacture": 2013,
    "cylinderCapacityCc": "2198 cc",
    "co2Emissions": "189 g/km",
    "fuelType": "DIESEL",
    "exportMarker": "No",
    "vehicleStatus": "Tax not due",
    "vehicleColour": "SILVER",
    "vehicleTypeApproval": "N1",
    "wheelplan": "2 AXLE RIGID BODY",
    "revenueWeight": "2825kg",
    "__v": 3,
    "nextService": new Date("2016-10-07T23:00:00.000Z"),
    "nextInspection": new Date("2017-08-05T23:00:00.000Z"),
    "nextVehicleCheck": new Date("2017-06-15T23:00:00.000Z"),
    "checkHistory": [{
      "checkDate": new Date("2017-06-09T13:33:55.960Z"),
      "checkedBy": "A Driver",
      "filename": null,
      "_id": "59413b439c5335e84e9a867f"
    }],
    "inspectionHistory": [{
      "inspectionDate": new Date("2017-06-04T13:33:55.936Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a8674"
    }],
    "serviceHistory": [{
      "serviceDate": new Date("2016-04-30T13:33:55.923Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a866b"
    }],
    "motHistory": [{
      "testDate": new Date("2016-10-18T00:00:00.000Z"),
      "expiryDate": new Date("2017-10-17T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 108726,
      "motTestNumber": "9766 7300 0123",
      "_id": "59413b439c5335e84e9a8662",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2016-10-17T00:00:00.000Z"),
      "testResult": "Fail",
      "odometerReading": 108722,
      "expiryDate": null,
      "motTestNumber": "5394 4656 1269",
      "_id": "59413b439c5335e84e9a8661",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2016-03-18T00:00:00.000Z"),
      "expiryDate": new Date("2017-03-20T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 93345,
      "motTestNumber": "7182 8451 8028",
      "_id": "59413b439c5335e84e9a8660",
      "advisoryNotices": ["Offside Front Tyre worn close to the legal limit (4.1.E.1)"]
    }, {
      "testDate": new Date("2016-03-09T00:00:00.000Z"),
      "testResult": "Fail",
      "odometerReading": 93208,
      "expiryDate": null,
      "motTestNumber": "1575 3406 5538",
      "_id": "59413b439c5335e84e9a865f",
      "advisoryNotices": ["Offside Front Tyre worn close to the legal limit (4.1.E.1)"]
    }],
    "type": "Grey",
    "category": "B"
  },
  "__v": 0,
  "updated": new Date("2017-06-14T14:29:17.774Z")
}, {
  "_id": "59413b8e5cbc02844b1c819b",
  "_companyId": "012345678901234567891000",
  "alertType": {
    "_id": "59413b7e65dd129c312e0445",
    "reportType": {
      "_id": "59413b7465dd129c312e0443",
      "name": "None Compliant Vehicles",
      "query": "getNoneCompliantVehicles",
      "category": "Vehicle",
      "__v": 0
    },
    "__v": 0,
    "priority": "High"
  },
  "created": new Date("2017-06-14T13:35:10.462Z"),
  "status": "Active",
  "driver": null,
  "vehicle": {
    "_id": new ObjectID("59413b439c5335e84e9a8634"),
    "compliance": {
      "compliant": false,
      "tax": true,
      "mot": true,
      "service": true,
      "inspection": true,
      "vehicleCheck": false,
      "_id": "59413b439c5335e84e9a864f"
    },
    "updatedAt": new Date("2017-06-14T13:33:55.234Z"),
    "createdAt": new Date("2017-06-14T13:33:55.141Z"),
    "_companyId": "012345678901234567891000",
    "make": "FORD",
    "model": "Unknown",
    "registrationNumber": "CX65 PUE",
    "annualMileage": 12000,
    "inspectionIntervalType": "A",
    "taxDueDate": new Date("2018-03-01T00:00:00.000Z"),
    "motDueDate": new Date("2018-08-31T23:00:00.000Z"),
    "dateOfFirstRegistration": new Date("2015-08-31T23:00:00.000Z"),
    "yearOfManufacture": 2015,
    "cylinderCapacityCc": "2198 cc",
    "co2Emissions": "212 g/km",
    "fuelType": "DIESEL",
    "exportMarker": "No",
    "vehicleStatus": "Tax not due",
    "vehicleColour": "WHITE",
    "vehicleTypeApproval": "N1",
    "wheelplan": "2 AXLE RIGID BODY",
    "revenueWeight": "3500kg",
    "__v": 3,
    "nextService": new Date("2018-04-07T23:00:00.000Z"),
    "nextInspection": new Date("2017-07-22T23:00:00.000Z"),
    "nextVehicleCheck": new Date("2017-06-08T23:00:00.000Z"),
    "checkHistory": [{
      "checkDate": new Date("2017-06-02T13:33:55.222Z"),
      "checkedBy": "A Driver",
      "filename": null,
      "_id": "59413b439c5335e84e9a864e"
    }],
    "inspectionHistory": [{
      "inspectionDate": new Date("2017-05-14T13:33:55.187Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a8644"
    }],
    "serviceHistory": [{
      "serviceDate": new Date("2017-04-08T13:33:55.154Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b439c5335e84e9a863c"
    }],
    "motHistory": [],
    "type": "Grey",
    "category": "B"
  },
  "__v": 0,
  "updated": new Date("2017-06-14T14:29:17.774Z")
}, {
  "_id": "59413b8e5cbc02844b1c81a9",
  "_companyId": "012345678901234567891000",
  "alertType": {
    "_id": "59413b7e65dd129c312e0445",
    "reportType": {
      "_id": "59413b7465dd129c312e0443",
      "name": "None Compliant Vehicles",
      "query": "getNoneCompliantVehicles",
      "category": "Vehicle",
      "__v": 0
    },
    "__v": 0,
    "priority": "High"
  },
  "created": new Date("2017-06-14T13:35:10.462Z"),
  "status": "Active",
  "driver": null,
  "vehicle": {
    "_id": "59413b449c5335e84e9a8690",
    "compliance": {
      "compliant": false,
      "tax": true,
      "mot": true,
      "service": true,
      "inspection": true,
      "vehicleCheck": false,
      "_id": "59413b449c5335e84e9a86bb"
    },
    "updatedAt": new Date("2017-06-14T13:33:56.700Z"),
    "createdAt": new Date("2017-06-14T13:33:56.617Z"),
    "_companyId": "012345678901234567891000",
    "make": "MERCEDES",
    "model": "A 200",
    "registrationNumber": "GX05 BZC",
    "annualMileage": 2915,
    "inspectionIntervalType": "A",
    "taxDueDate": new Date("2017-12-01T00:00:00.000Z"),
    "motDueDate": new Date("2017-12-02T00:00:00.000Z"),
    "dateOfFirstRegistration": new Date("2005-08-19T00:00:00.000Z"),
    "yearOfManufacture": 2005,
    "cylinderCapacityCc": "2035 cc",
    "co2Emissions": "172 g/km",
    "fuelType": "PETROL",
    "exportMarker": "No",
    "vehicleStatus": "Tax not due",
    "vehicleColour": "BLACK",
    "vehicleTypeApproval": "M1",
    "wheelplan": "2 AXLE RIGID BODY",
    "revenueWeight": "Not available",
    "__v": 3,
    "nextService": new Date("2018-03-19T00:00:00.000Z"),
    "nextInspection": new Date("2017-07-16T23:00:00.000Z"),
    "nextVehicleCheck": new Date("2017-06-08T23:00:00.000Z"),
    "checkHistory": [{
      "checkDate": new Date("2017-06-02T13:33:56.686Z"),
      "checkedBy": "A Driver",
      "filename": null,
      "_id": "59413b449c5335e84e9a86ba"
    }],
    "inspectionHistory": [{
      "inspectionDate": new Date("2017-05-08T13:33:56.653Z"),
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b449c5335e84e9a86ae"
    }],
    "serviceHistory": [{
      "serviceDate": new Date("2017-03-19T14:33:56.637Z"),
      "expiryDate": null,
      "supplierId": "012345678901234567892000",
      "filename": null,
      "_id": "59413b449c5335e84e9a86a4"
    }],
    "motHistory": [{
      "testDate": new Date("2016-12-03T00:00:00.000Z"),
      "expiryDate": new Date("2017-12-02T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 101109,
      "motTestNumber": "3401 5875 7448",
      "_id": "59413b449c5335e84e9a869a",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2015-07-29T00:00:00.000Z"),
      "expiryDate": new Date("2016-08-04T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 96980,
      "motTestNumber": "7755 2021 5273",
      "_id": "59413b449c5335e84e9a8699",
      "advisoryNotices": ["small cut on offside front tyre side wall"]
    }, {
      "testDate": new Date("2014-07-10T00:00:00.000Z"),
      "expiryDate": new Date("2015-08-04T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 88260,
      "motTestNumber": "1159 1109 4197",
      "_id": "59413b449c5335e84e9a8698",
      "advisoryNotices": ["Windscreen has damage to an area less than a 10mm circle within zone 'A' (8.3.1a)"]
    }, {
      "testDate": new Date("2013-08-05T00:00:00.000Z"),
      "expiryDate": new Date("2014-08-04T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 80185,
      "motTestNumber": "8951 9791 3299",
      "_id": "59413b449c5335e84e9a8697",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2013-07-26T00:00:00.000Z"),
      "expiryDate": null,
      "testResult": "Fail",
      "odometerReading": 80195,
      "motTestNumber": "9754 4720 3249",
      "_id": "59413b449c5335e84e9a8696",
      "advisoryNotices": ["Windscreen has damage to an area less than a 40mm circle outside zone 'A' (8.3.1d)"]
    }, {
      "testDate": new Date("2012-07-27T00:00:00.000Z"),
      "expiryDate": new Date("2013-07-26T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 68955,
      "motTestNumber": "4954 8910 2204",
      "_id": "59413b449c5335e84e9a8695",
      "advisoryNotices": ["Nearside Brake pipe slightly corroded (3.6.B.2c)", "Offside Brake pipe slightly corroded (3.6.B.2c)", "Under-trays fitted obscuring some underside components", "Vehicle's internal headlight adjuster altered to recheck lights"]
    }, {
      "testDate": new Date("2011-07-27T00:00:00.000Z"),
      "expiryDate": new Date("2012-07-26T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 42893,
      "motTestNumber": "6656 2850 1272",
      "_id": "59413b449c5335e84e9a8694",
      "advisoryNotices": []
    }, {
      "testDate": new Date("2010-06-16T00:00:00.000Z"),
      "expiryDate": new Date("2011-06-15T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 48502,
      "motTestNumber": "9415 2716 0122",
      "_id": "59413b449c5335e84e9a8693",
      "advisoryNotices": ["Nearside Rear Tyre worn close to the legal limit (4.1.E.1)"]
    }, {
      "testDate": new Date("2010-06-15T00:00:00.000Z"),
      "expiryDate": null,
      "testResult": "Fail",
      "odometerReading": 48502,
      "motTestNumber": "1125 4636 0143",
      "_id": "59413b449c5335e84e9a8692",
      "advisoryNotices": ["Offside Front Brake pad(s) wearing thin (3.5.1g)", "Offside Front Tyre worn close to the legal limit (4.1.E.1)", "ofside rear tyre has cuts within limits to the tread area"]
    }, {
      "testDate": new Date("2009-04-27T00:00:00.000Z"),
      "expiryDate": new Date("2010-04-26T00:00:00.000Z"),
      "testResult": "Pass",
      "odometerReading": 37722,
      "motTestNumber": "3441 2731 9136",
      "_id": "59413b449c5335e84e9a8691",
      "advisoryNotices": []
    }],
    "type": "Grey",
    "category": "B"
  },
  "__v": 0,
  "updated": new Date("2017-06-14T14:30:55.251Z")
}];


