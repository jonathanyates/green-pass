export interface IAudit {
  _id?: string,
  _companyId?: string,
  userId?: string,
  role?: string,
  action: string,
  target?: string,
  result?: any,
  error?: string,
  dateTime: Date
}