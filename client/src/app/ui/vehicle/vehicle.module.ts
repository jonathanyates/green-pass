import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleInputComponent } from './vehicle-input/vehicle-input.component';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { VehicleDateAlertComponent } from './vehicle-details/vehicle-date-alert.component';
import { VehicleDetailsTabListComponent } from './vehicle-details-tab-list/vehicle-details-tab-list.component';
import { VehicleMotHistoryComponent } from './vehicle-mot/vehicle-mot-history';
import { VehicleServiceListComponent } from './vehicle-service/vehicle-service-list/vehicle-service-list.component';
import { VehicleInspectionListComponent } from './vehicle-inspection/vehicle-inspection-list/vehicle-inspection-list.component';
import { VehicleRoutingModule } from './vehicle-routing.module';
import { VehicleServiceInputComponent } from './vehicle-service/vehicle-service-input/vehicle-service-input.component';
import { VehicleServiceComponent } from './vehicle-service/vehicle-service-detail/vehicle-service.component';
import { VehicleServiceInputMapper } from './vehicle-service/vehicle-service-input/vehicle-service-input.mapper';
import { VehicleInspectionInputComponent } from './vehicle-inspection/vehicle-inspection-input/vehicle-inspection-input.component';
import { VehicleInspectionComponent } from './vehicle-inspection/vehicle-inspection-detail/vehicle-service.component';
import { VehicleInspectionInputMapper } from './vehicle-inspection/vehicle-inspection-input/vehicle-inspection-input.mapper';
import { VehicleCheckListComponent } from './vehicle-check/vehicle-check-list/vehicle-check-list.component';
import { VehicleCheckInputMapper } from './vehicle-check/vehicle-check-input/vehicle-check-input.mapper';
import { VehicleCheckInputComponent } from './vehicle-check/vehicle-check-input/vehicle-check-input.component';
import { VehicleCheckComponent } from './vehicle-check/vehicle-check-detail/vehicle-check.component';
import { VehicleListContainerComponent } from './vehicle-list/vehicle-list-container.component';
import { VehicleDriverSelectComponent } from './vehicle-driver/vehicle-driver-select.component';
import { VehicleSearchComponent } from './vehicle-search/vehicle-search.component';
import { VehicleDvlaDetailsComponent } from './vehicle-dvla-details/vehicle-dvla-details.component';
import { VehicleSearchInputComponent } from './vehicle-input/vehicle-search-input.component';
import { VehicleAnnualTestHistoryComponent } from './vehicle-mot/vehicle-annual-test-history';

@NgModule({
  imports: [FormsModule, ReactiveFormsModule, CommonModule, SharedModule, VehicleRoutingModule],
  exports: [
    VehicleListComponent,
    VehicleListContainerComponent,
    VehicleInputComponent,
    VehicleSearchInputComponent,
    VehicleDetailsComponent,
    VehicleDvlaDetailsComponent,
    VehicleMotHistoryComponent,
    VehicleAnnualTestHistoryComponent,
    VehicleDateAlertComponent,
    VehicleDetailsTabListComponent,

    VehicleServiceListComponent,
    VehicleServiceInputComponent,
    VehicleServiceComponent,

    VehicleInspectionListComponent,
    VehicleInspectionInputComponent,
    VehicleInspectionComponent,

    VehicleCheckListComponent,
    VehicleCheckInputComponent,
    VehicleCheckComponent,

    VehicleDriverSelectComponent,
    VehicleSearchComponent,
  ],
  declarations: [
    VehicleListComponent,
    VehicleListContainerComponent,
    VehicleInputComponent,
    VehicleSearchInputComponent,
    VehicleDetailsComponent,
    VehicleDvlaDetailsComponent,
    VehicleMotHistoryComponent,
    VehicleAnnualTestHistoryComponent,
    VehicleDateAlertComponent,
    VehicleDetailsTabListComponent,

    VehicleServiceListComponent,
    VehicleServiceInputComponent,
    VehicleServiceComponent,

    VehicleInspectionListComponent,
    VehicleInspectionInputComponent,
    VehicleInspectionComponent,

    VehicleCheckListComponent,
    VehicleCheckInputComponent,
    VehicleCheckComponent,

    VehicleDriverSelectComponent,
    VehicleSearchComponent,
  ],
  providers: [VehicleServiceInputMapper, VehicleInspectionInputMapper, VehicleCheckInputMapper],
})
export class VehicleModule {}
