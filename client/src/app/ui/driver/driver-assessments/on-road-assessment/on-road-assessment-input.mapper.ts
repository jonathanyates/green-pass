import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";
import {FormInput, FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {FormatDateForInput, GetDate} from "../../../../../../../shared/utils/helpers";

@Injectable()
export class OnRoadAssessmentInputMapper {

  mapFrom(form:FormGroup, inputGroups: FormInputGroup[], onRoadAssessment: IOnRoadAssessment): IOnRoadAssessment {

    if (!onRoadAssessment._id) {
      return {
        inviteDate: GetDate(form.controls['inviteDate'].value),
        assessedBy: form.controls['assessedBy'].value,
        complete: false
      };
    }

    let input = <FormInput>inputGroups[0].inputs.find((input:FormInput) => input.id === 'file');
    let fileChanged = input.file && input.file !== onRoadAssessment.file;
    let assessmentDate = GetDate(form.controls['assessmentDate'].value);
    let result = form.controls['result'].value;

    return {
      inviteDate: onRoadAssessment.inviteDate,
      assessmentDate: GetDate(form.controls['assessmentDate'].value),
      assessedBy: form.controls['assessedBy'].value,
      result: form.controls['result'].value,
      complete: assessmentDate != null && result != null,
      file: fileChanged ? input.file : null,
      fileType: input.fileType
    };
  }

  mapTo(assessment: IOnRoadAssessment): FormInputGroup[] {

    if (!assessment._id) {
      return [
        {
          label: 'On Road Assessment',
          inputs: [
            { id: 'inviteDate', label: 'Invite Date', type:'date', required: true, autoFocus: true,
              value: FormatDateForInput(assessment.inviteDate) },
            { id: 'assessedBy', label: 'Assessor', type:'text', required: false, autoFocus: false, value: assessment.assessedBy },
          ]
        }
      ];
    }

    let reportInput = <FormInput>{ id: 'file', label: 'Report', type:'file', required: false, value: '' };

    if (assessment.file) {
      reportInput.file = assessment.file;
      reportInput.fileType = assessment.fileType;
    }

    reportInput.action = file => {
      reportInput.file = file;
      reportInput.fileType = file.fileType;
      return Observable.empty();
    };

    let resultTypes = [
      { key: 'Pass', value: 'Pass', selected: false },
      { key: 'Fail', value: 'Fail', selected: false }
    ];

    return [
      {
        label: 'On Road Assessment',
        inputs: [
          { id: 'assessedBy', label: 'Assessor', type:'text', required: true, autoFocus: false, value: assessment.assessedBy },
          { id: 'assessmentDate', label: 'Assessment Date', type:'date', required: true, autoFocus: false,
            value: FormatDateForInput(assessment.assessmentDate) },
          { id: 'result', label: 'Result', type:'dropdown', required: true, autoFocus: false, value: assessment.result,
            gridClass: 'col-md-6', options: resultTypes},
          reportInput
        ]
      }
    ];
  }

}
