import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {IContact} from "../../../../../../shared/interfaces/driver.interface";
import {SelectDriverAction, SelectDriverReferenceAction} from "../../../store/drivers/driver.actions";

@Injectable()
export class  DriverReferenceResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let id = route.params['id'];
    let driverId = route.params['driverId'];
    let referenceId = route.params['referenceId'];

    if (!id || !driverId) return false;

    this.store.dispatch(new SelectCompanyAction(id));
    this.store.dispatch(new SelectDriverAction(id, driverId));
    this.store.dispatch(new SelectDriverReferenceAction(referenceId));

    return true;
  }
}
