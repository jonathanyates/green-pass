export interface IDriverReminderSetting {
  sendEmails: boolean,
  cron: string
}

export interface IPointsCheckPeriod {
  noOfPoints: number,
  months: number
}

export interface ICompanySettings {
  vehicles: {
    grey: {
      exclusions: {
        service: boolean,
        inspection: boolean,
        checks: boolean
      }
    }
  },
  drivers: {
    allowInput: boolean,
    references: boolean,
    reminders: IDriverReminderSetting,
    licenceChecks?: {
      checkPeriods: IPointsCheckPeriod[]
    },
    includeFaw?: boolean,
    includeDbs?: boolean,
    includePNumber?: boolean
  },
}

/*
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */


