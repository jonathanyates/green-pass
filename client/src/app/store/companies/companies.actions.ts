import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {Company} from "../../ui/company/company.model";
import {LogoutAction} from "../authentication/authentication.actions";

export const ActionTypes = {
  LOAD:                 type('[Companies] Load'),
  LOAD_SUCCESS:         type('[Companies] Load Success')
};

/**
 * Load Companies Actions
 */
export class LoadCompaniesAction implements Action {
  type = ActionTypes.LOAD;
  constructor() { }
}

export class LoadCompaniesSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: Company[]) { }
}

export type Actions
  = LoadCompaniesSuccessAction
  | LogoutAction;
