import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {VehicleService} from "../../../services/vehicle.service";
import {DriverDetailsTabListComponent} from "../driver-details-tab-list/driver-details-tab-list.component";
import {SelectVehicle} from "../../../store/vehicles/vehicle.selectors";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {DriverService} from "../../../services/driver.service";

@Component({
  selector: 'gp-driver-vehicle',
  templateUrl: '../driver-details-tab-list/driver-details-tab-list.component.html'
})
export class VehicleDriverComponent extends DriverDetailsTabListComponent {

  vehicle: IVehicle;

  constructor(store: Store<AppState>,
              router: Router,
              route: ActivatedRoute,
              vehicleService: VehicleService,
              driverService: DriverService) {
    super(store, router, route, vehicleService, driverService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.subscriptions.push(
      this.store.let(SelectVehicle())
        .filter(driver => driver !== null)
        .subscribe(driver => {
          this.vehicle = driver;
        }));
  }
}
