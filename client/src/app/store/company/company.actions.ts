import {type} from "../../shared/util";
import {Supplier, Depot} from "../../ui/company/company.model";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {ICompanyAlertType} from "../../../../../shared/interfaces/alert.interface";
import {ICompany} from "../../../../../shared/interfaces/company.interface";

export const ActionTypes = {
  SELECT:             type('[Company] Select'),
  SELECT_SUCCESS:     type('[Company] Select Success'),
  LOAD:               type('[Company] Load'),
  LOAD_SUCCESS:       type('[Company] Load Success'),
  LOAD_FAIL:          type('[Company] Load Fail'),
  SAVE:               type('[Company] Save'),
  SAVE_SUCCESS:       type('[Company] Save Success'),
  CLEAR:              type('[Company] Clear'),
  SELECT_SELECTED_TAB:type('[Company] Select Selected Tab'),
  SAVE_DEPOT:         type('[Depot] Save Depot'),
  SAVE_SUPPLIER:      type('[Supplier] Save Supplier'),
  SAVE_ALERT_TYPES:   type('[Alert Types] Save Alert Types')
};

/**
 * Select Company Actions
 */
export class SelectCompanyAction implements Action {
  type = ActionTypes.SELECT;
  constructor(public payload: string) { }  // payload = company id
}

export class SelectCompanySuccessAction implements Action {
  type = ActionTypes.SELECT_SUCCESS;
  constructor(public payload: ICompany) { }
}

/**
 * Load Company Actions
 */
export class LoadCompanyAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload: string) { }  // payload = company id
}

export class LoadCompanySuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: ICompany) { }
}

export class LoadCompanyFailAction implements Action {
  type = ActionTypes.LOAD_FAIL;
  constructor(public payload: any) { }
}

/**
 * Save Company Actions
 */
export class SaveCompanyAction implements Action {
  type = ActionTypes.SAVE;
  constructor(public payload: ICompany) { }
}

export class SaveCompanySuccessAction implements Action {
  type = ActionTypes.SAVE_SUCCESS;
  constructor(public payload: ICompany) { }
}

/**
 * Clear Company Actions
 */
export class ClearCompanyAction implements Action {
  type= ActionTypes.CLEAR;
  payload = null;
}

/**
 * Depot Actions
 */
export class SaveDepotAction implements Action {
  type = ActionTypes.SAVE_DEPOT;
  constructor(public payload: Depot) { }
}

/**
 * Supplier Actions
 */
export class SaveSupplierAction implements Action {
  type = ActionTypes.SAVE_SUPPLIER;
  constructor(public payload: Supplier) { }
}

/**
 * Alert Type Actions
 */
export class SaveCompanyAlertTypesAction implements Action {
  type = ActionTypes.SAVE_ALERT_TYPES;
  constructor(public payload: ICompanyAlertType[]) { }
}

/**
 * Selected Tab Actions
 */

export class SelectCompanyDetailsTabAction implements Action {
  type = ActionTypes.SELECT_SELECTED_TAB;
  constructor(public payload: string) {}
}

export type Actions
  = LoadCompanySuccessAction
  | LoadCompanyFailAction
  | SelectCompanySuccessAction
  | SaveCompanyAction
  | SaveCompanySuccessAction
  | SaveCompanySuccessAction
  | ClearCompanyAction
  | SaveDepotAction
  | SaveSupplierAction
  | SaveCompanyAlertTypesAction
  | SelectCompanyDetailsTabAction
  | LogoutAction;
