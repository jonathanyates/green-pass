import { Express } from 'express';
import fs = require('fs');
import expressWinston = require('express-winston');
import winston = require('winston');
import serverConfig, { Environments } from './server.config';
require('winston-daily-rotate-file');
require('winston-loggly-bulk');

const logDir = 'log';
const env = process.env.NODE_ENV || Environments.development;
const tsFormat = () => new Date().toLocaleTimeString();
const level = 'debug'; // env !== Environments.production ? 'debug' : 'info';

export function configureLoggerMiddleware(app: Express) {
  // Create the log directory if it does not exist
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }

  if (serverConfig.loggly.enabled) {
    winston.add(winston.transports.Loggly, {
      token: serverConfig.loggly.token,
      subdomain: serverConfig.loggly.subdomain,
      tags: ['GreenPass-Api-Logs'],
      json: true,
    });
  }

  winston.add(winston.transports.DailyRotateFile, {
    filename: `${logDir}/log`,
    timestamp: tsFormat,
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: level,
  });

  const transports = [
    new winston.transports.Console({
      json: false,
      colorize: true,
    }),
    new winston.transports.DailyRotateFile({
      filename: `${logDir}/express.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd.',
      prepend: true,
      level: level,
    }),
  ];

  if (serverConfig.loggly.enabled) {
    transports.push(
      new winston.transports.Loggly({
        token: serverConfig.loggly.token,
        subdomain: serverConfig.loggly.subdomain,
        tags: ['GreenPass-Api-Express'],
        json: true,
      })
    );
  }

  app.use(
    expressWinston.logger({
      transports: transports,
    })
  );
}

export function configureErrorLoggerMiddleware(app: Express) {
  const transports = [
    new winston.transports.Console({
      json: true,
      colorize: true,
    }),
    new winston.transports.DailyRotateFile({
      filename: `${logDir}/express-errors.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd.',
      prepend: true,
      level: level,
    }),
  ];

  if (serverConfig.loggly.enabled) {
    transports.push(
      new winston.transports.Loggly({
        token: serverConfig.loggly.token,
        subdomain: serverConfig.loggly.subdomain,
        tags: ['GreenPass-Api-Express', 'GreenPass-Api-Express-Errors'],
        json: true,
      })
    );
  }

  app.use(
    expressWinston.errorLogger({
      transports: transports,
    })
  );
}
