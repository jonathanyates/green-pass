#!/usr/bin/env bash

mongod
mongo
use admin
db.createUser(
  {
    user: "useradmin",
    pwd: "everest99",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

mongod --auth
mongo -u "useradmin" -p "everest99" --authenticationDatabase "admin"

use green-pass
db.createUser(
  {
    user: "greenpass-admin",
    pwd: "everest99",
    roles: [ { role: "readWrite", db: "green-pass" } ]
  }
)

mongo -u "greenpass-admin" -p "everest99" --authenticationDatabase "green-pass"