import { connect } from 'amqplib/callback_api';

connect('amqp://localhost', function (err, conn) {
  conn.createChannel(function (err, ch) {
    let queue = 'hello';

    ch.assertQueue(queue, { durable: false });

    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue);
    ch.consume(
      queue,
      function (message) {
        let data = JSON.parse(message.content);
        console.log(' [x] Received %s', JSON.stringify(data));
      },
      { noAck: true }
    );
  });
});
