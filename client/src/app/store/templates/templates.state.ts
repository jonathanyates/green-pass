import {ITemplate} from "../../../../../shared/interfaces/document.interface";

export interface TemplatesState {
  list: ITemplate[];
  selected?: ITemplate
}
