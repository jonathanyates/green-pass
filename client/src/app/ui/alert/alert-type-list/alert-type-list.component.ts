import {Location} from '@angular/common';
import { Paths } from '../../../shared/constants';
import { AppState } from '../../../store/app.states';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {LoadAlertTypesAction} from "../../../store/alerts/alert.actions";
import {SelectAlertTypes} from "../../../store/alerts/alert.selectors";
import {IAlertType} from "../../../../../../shared/interfaces/alert.interface";

@Component({
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"></gp-dynamic-list>
`
})
export class AlertTypeListComponent implements OnInit, OnDestroy {

  list: IDynamicList;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private location: Location) { }

  ngOnInit() {
    this.store.dispatch(new LoadAlertTypesAction());

    this.list = {
      columns: [
        { header: 'Name', field: 'name' },
        { header: 'Category', field: 'category' },
        { header: 'Priority', field: 'priority'},
        { header: 'Query Name', field: 'query'},
      ]
    };

    this.subscription = this.store.let(SelectAlertTypes())
      .filter(alertTypes => alertTypes != null)
      .subscribe((alertTypes:IAlertType[]) => {
        this.list.items = alertTypes.map(alertType => ({
          _id: alertType._id,
          name: alertType.reportType.name,
          category: alertType.reportType.category,
          priority: alertType.priority,
          query: alertType.reportType.query
        }));
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  select(alertType:IAlertType) {
    return this.router.navigate(['./', alertType._id], { relativeTo: this.route });
  }

  add() {
    return this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
