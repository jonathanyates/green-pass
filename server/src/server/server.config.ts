import devServerConfig from './env/dev.env';
import prodServerConfig from './env/prod.env';

export const Environments = {
  development: 'development',
  uat: 'uat',
  production: 'production',
};

export interface IServerConfig {
  db: string;
  authentication: {
    secret: string;
  };
  dvla: {
    uri: string;
    apiKey: string;
  };
  dvsa: {
    mot: {
      uri: string;
      apiKey: string;
    };
    hgvTest: {
      uri: string;
      apiKey: string;
    };
  };
  legalesign: {
    apiKey: string;
    baseUrl: string;
    group: string;
    email: string;
    user: string;
  };
  roadMarque: {
    url: string;
    dllUrl: string;
    username: string;
    password: string;
    customerId: string;
    resellerId: string;
    dllPassword: string;
    driverGroup: string;
    updateAssessments: boolean;
  };
  client: {
    baseUrl: string;
  };
  server: {
    apiRoot: string;
    path: string;
  };
  email: {
    sendEmails: boolean;
  };
  loggly: {
    enabled: boolean;
    token: string;
    subdomain: string;
  };
  rabbitmq: {
    url: string;
  };
}

const serverConfig: IServerConfig =
  process.env.NODE_ENV === Environments.production ? prodServerConfig : devServerConfig;

export default serverConfig;
