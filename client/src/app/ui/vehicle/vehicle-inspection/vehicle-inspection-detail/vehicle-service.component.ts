import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../../shared/components/dynamic-list-group/dynamic-list-group";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectVehiclesState} from "../../../../store/vehicles/vehicle.selectors";
import {IVehicle, IVehicleInspection} from "../../../../../../../shared/interfaces/vehicle.interface";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {FormatDate} from "../../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-vehicle-inspection',
  template: `
    <gp-card [header]="'Vehicle inspection'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class VehicleInspectionComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private inspection: IVehicleInspection;
  private vehicle: IVehicle;
  private subscription: Subscription;

  url: any;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectVehiclesState())
      .filter(state => state.selectedInspection != null)
      .combineLatest(this.store.let(SelectCompany())
          .filter(company => company !== null)
          .map(company => company.suppliers),
        (state, suppliers) => ({
          vehicleState: state,
          supplier: suppliers.find(supplier => supplier._id === state.selectedInspection.supplierId)
        }))
      .subscribe(result => {
        this.vehicle = result.vehicleState.selected;
        this.inspection = result.vehicleState.selectedInspection;
        let supplier = result.supplier;
        if (this.inspection) {
          this.items = [
            {title: "Inspection Date", value: FormatDate(this.inspection.inspectionDate) }
          ];

          if (supplier) {
            this.items.push({title: 'Inspectiond By', value: supplier.name});
            this.items.push({title: 'Address', value: supplier.address});

            if (supplier.phone) {
              this.items.push({title: 'Phone', value: supplier.phone})
            }

            if (supplier.phone) {
              this.items.push({title: 'Mobile', value: supplier.mobile})
            }

            if (supplier.email) {
              this.items.push({title: 'Email', value: supplier.email})
            }
          }

          if (this.inspection.filename) {
            this.items.push({title: 'Inspection Report', value: this.inspection.file, type: this.inspection.fileType})
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
