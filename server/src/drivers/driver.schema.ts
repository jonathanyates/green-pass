import mongoose from 'mongoose';
import { IDriver, IDriverVehicleMap } from '../../../shared/interfaces/driver.interface';
import { AddressSchema } from '../address/address.schema';
import { AssessmentSchema } from '../assessments/assessment.schema';

export const DriverSchema = new mongoose.Schema(
  {
    _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true },
    _user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    address: { type: AddressSchema, required: false },
    dateOfBirth: { type: Date, required: false },
    gender: { type: String, required: false },
    licenceNumber: { type: String, required: false },
    niNumber: { type: String, required: false },
    phone: { type: String, required: false },
    mobile: { type: String, required: false },
    licence: {
      checks: [
        {
          number: { type: String, required: true },
          issueNumber: { type: Number, required: false },
          status: { type: String, required: false },
          validFrom: { type: Date, required: false },
          validTo: { type: Date, required: false },
          scan: {
            front: { type: String, required: false },
            back: { type: String, required: false },
          },
          endorsements: [
            {
              category: { type: String, required: true },
              points: { type: Number, required: true },
              description: { type: String, required: true },
              courtDetails: {
                code: { type: String, required: false },
                description: { type: String, required: false },
              },
              offenceDate: { type: Date, required: true },
              expiryDate: { type: Date, required: true },
              removalDate: { type: Date, required: true },
            },
          ],
          points: { type: Number, required: false },
          allowedVehicles: {
            vehicles: [
              {
                category: { type: String, required: true },
                startDate: { type: Date, required: false },
                endDate: { type: Date, required: false },
                description: { type: String, required: true },
                restrictions: [
                  {
                    code: { type: String, required: true },
                    description: { type: String, required: true },
                  },
                ],
              },
            ],
            provisional: [
              {
                category: { type: String, required: true },
                startDate: { type: Date, required: false },
                endDate: { type: Date, required: false },
                description: { type: String, required: true },
                restrictions: [
                  {
                    code: { type: String, required: true },
                    description: { type: String, required: true },
                  },
                ],
              },
            ],
          },
          tachographCard: {
            type: {
              status: { type: String, required: false },
              validFrom: { type: Date, required: false },
              validTo: { type: Date, required: false },
              number: { type: String, required: false },
            },
            required: false,
          },
          cpc: {
            type: {
              categories: [
                {
                  category: { type: String, required: false },
                  endDate: { type: Date, required: false },
                  description: { type: String, required: false },
                },
              ],
            },
            required: false,
          },
          checkCode: { type: String, required: false },
          checkDate: { type: Date, required: false },
        },
      ],
      nextCheckDate: { type: Date, required: false },
    },
    nextOfKin: {
      type: {
        title: { type: String, required: true },
        forename: { type: String, required: true },
        surname: { type: String, required: true },
        address: { type: AddressSchema, required: true },
        phone: { type: String, required: true },
        mobile: { type: String, required: false },
        email: { type: String, required: false },
      },
      required: false,
    },
    references: [
      {
        forename: { type: String, required: true },
        surname: { type: String, required: true },
        address: { type: AddressSchema, required: true },
        phone: { type: String, required: false },
        mobile: { type: String, required: false },
        email: { type: String, required: false },
      },
    ],
    insuranceHistory: [
      {
        validFrom: { type: Date, required: true },
        validTo: { type: Date, required: true },
        filename: { type: String, required: false },
        fileType: { type: String, required: false },
      },
    ],

    fawRequired: { type: Boolean, required: false },
    fawHistory: [
      {
        validFrom: { type: Date, required: true },
        validTo: { type: Date, required: true },
        filename: { type: String, required: false },
        fileType: { type: String, required: false },
      },
    ],
    dbsExpiry: { type: Date, required: false },

    assessments: [AssessmentSchema],
    nextAssessmentDate: { type: Date, required: false },
    lastAssessmentUpdatedDate: { type: Date, required: false },
    lastAssessmentLoginDate: { type: Date, required: false },
    documents: [
      {
        documentId: { type: String, required: true },
        templates: [{ type: String, required: false }],
        name: { type: String, required: true },
        folderName: { type: String, required: false },
        status: { type: String, required: false },
        statusDateTime: { type: Date, required: false },
        uri: { type: String, required: false },
        embeddingUri: { type: String, required: false },
        signed: { type: Boolean, required: false, default: false },
      },
    ],
    ownedVehicle: { type: Boolean, required: false },
    removed: { type: Boolean, default: false, required: false },

    // Sea Cadets
    pNumber: { type: String, required: false },
  },
  { timestamps: true }
);

export interface IDriverModel extends mongoose.Document, IDriver {
  _id: any;
}

export const DriverVehicleMapSchema = new mongoose.Schema({
  driverId: { type: mongoose.Schema.Types.ObjectId, ref: 'Driver', required: true },
  vehicleId: { type: mongoose.Schema.Types.ObjectId, ref: 'Vehicle', required: true },
});

export interface IDriverVehicleMapModel extends mongoose.Document, IDriverVehicleMap {}
