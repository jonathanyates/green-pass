import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {IUser} from "../../../../shared/interfaces/user.interface";
import {HttpClient, HttpHeaders} from "@angular/common/http";

interface IAuthenticationResponse {
  token: string;
}

@Injectable()
export class AuthorizationService extends HttpBaseService {

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  login(username: string, password: string): Observable<string> {
    let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');
    let credentials = "username=" + username + "&password=" + password;
    return this.httpClient.post<IAuthenticationResponse>(apiPath + '/auth/login', credentials, { headers: headers })
      .map(res => res.token)
      .catch(err => {
        return this.handleError(err);
      });
  }

  authenticate(token: string): Observable<boolean> {
    if (token == null) {
      throw new Error('Unauthorized');
    }

    let headers = new HttpHeaders().append('Authorization', token);

    return this.httpClient.post<boolean>(apiPath + '/auth/authorize', { headers: headers})
      .map(res => true)
      .catch(res => this.handleError(res));
  }

  forgotPassword(email: string): Observable<string> {
    let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');
    let body = "email=" + email;
    let url = apiPath + '/auth/forgot';
    return this.httpClient.post<string>(url, body, { headers: headers})
      .map(result => result)
      .catch(this.handleError);
  }

  verifyResetToken(token: string): Observable<boolean> {
    let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');
    let url = apiPath + '/auth/verify-reset/' + token;
    return this.httpClient.get<boolean>(url, { headers: headers})
      .map(result => result)
      .catch(this.handleError);
  }

  resetPassword(token: string, password: string): Observable<string> {
    let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');
    let body = "password=" + password;
    let url = apiPath + '/auth/reset/' + token;
    return this.httpClient.post<string>(url, body, { headers: headers})
      .map(result => result)
      .catch(this.handleError);
  }

  changePassword(user: IUser, password: string): Observable<string> {

    this.authorize();

    let options = this.getOptions();
    options.headers = options.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let body = "password=" + password;
    let url = apiPath + '/auth/change';
    return this.httpClient.post<IAuthenticationResponse>(url, body, options)
      .map(result => result.token)
      .catch(this.handleError);
  }
}
