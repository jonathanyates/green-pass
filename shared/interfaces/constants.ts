export const AssessmentType = {
  online: 'online',
  onlineModule: 'onlineModule',
  onRoad: 'onRoad'
};

export const AssessmentResult = {
  fail: 'fail',
  deferred: 'deferred',
  pass: 'pass'
};

export const AddressDefaults = {
  country: 'United Kingdom'
};

export const VehicleTypes = {
  fleet: 'Fleet',
  grey: 'Grey'
};