import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { ICompanyService } from './company.service.interface';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { TYPES } from '../container/container.types';
import { IApiController } from '../interfaces/api-controller.interface';
import codes from '../shared/error.codes';
import { AuditActions, AuditTargets, IAuditService } from '../audit/audit.service';
import { jsonDeserialiser } from '../../../shared/utils/json.deserialiser';

export interface ICompanyController extends IApiController {
  addInsurance(req, res: Response, next: NextFunction);
  updateInsurance(req: Request, res: Response, next: NextFunction);
}

@injectable()
export class CompanyController implements IApiController {
  constructor(
    @inject(TYPES.services.CompanyService) private companyService: ICompanyService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {
    this.companyService
      .getAll()
      .then((companies) => {
        if (!companies) {
          return res.status(codes.NOT_FOUND);
        }
        return res.status(codes.OK).json(companies);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.GetAll, AuditTargets.Company, err);
        next(err);
      });
  };

  get = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid company id.' });

    this.companyService
      .get(id)
      .then((company) => {
        if (!company) {
          return res.status(codes.NOT_FOUND).send({ message: 'Company not found.' });
        }

        return res.status(codes.OK).json(company);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Get, AuditTargets.Company, err);
        next(err);
      });
  };

  add = (req: Request, res: Response, next: NextFunction) => {
    const company = jsonDeserialiser.parseDates(req.body);

    this.companyService
      .add(company)
      .then((company) => {
        this.auditService.audit(req.user, AuditActions.Add, 'Company', company);
        return res.status(codes.CREATED).json(company);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Add, AuditTargets.Company, err);
        next(err);
      });
  };

  update = (req: Request, res: Response, next: NextFunction) => {
    const company = jsonDeserialiser.parseDates(req.body);

    this.companyService
      .update(company)
      .then((company) => {
        if (!company) {
          return res.status(codes.NOT_FOUND).send({ message: 'Company not found.' });
        }

        return res.status(codes.ACCEPTED).json(company);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Update, AuditTargets.Company, err);
        next(err);
      });
  };

  remove = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company id specified.' });
    }

    this.companyService
      .remove(companyId)
      .then((company) => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.Company, company);
        res.status(codes.ACCEPTED).json(company);
      })
      .catch((err) => {
        this.auditService.auditError(req.user, AuditActions.Remove, AuditTargets.Company, err);
        next(err);
      });
  };

  addInsurance = (req, res: Response, next: NextFunction) => {
    const companyId = req.params.id;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid From specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid To field specified.' });
    }

    const insurance = {
      validFrom: new Date(req.body.validFrom),
      validTo: new Date(req.body.validTo),
      file: req.file,
      fileType: req.body.fileType,
    };

    this.companyService
      .addInsurance(companyId, insurance)
      .then((insurance) => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.CompanyInsurance, insurance);
        res.status(codes.CREATED).json(insurance);
      })
      .catch((err) => next(err));
  };

  updateInsurance = (req: Request, res: Response, next: NextFunction) => {
    const companyId = req.params.id;
    const insuranceId = req.params.insuranceId;

    if (!companyId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No company Id specified.' });
    }

    if (!insuranceId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No check Id specified.' });
    }

    if (!req.body.validFrom) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid from specified.' });
    }

    if (!req.body.validTo) {
      return res.status(codes.BAD_REQUEST).send({ message: 'No valid to field specified.' });
    }

    const insurance = {
      validFrom: new Date(req.body.validFrom),
      validTo: new Date(req.body.validTo),
      file: req.file,
      fileType: req.body.fileType,
    };

    this.companyService
      .updateInsurance(companyId, insuranceId, insurance)
      .then((insurance) => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.CompanyInsurance, insurance);
        res.status(codes.CREATED).json(insurance);
      })
      .catch((err) => next(err));
  };
}
