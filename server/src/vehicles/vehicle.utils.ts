export const is3YearsOld = (dateOfFirstRegistration: Date) => {
  const date = new Date(
    dateOfFirstRegistration.getFullYear() + 3,
    dateOfFirstRegistration.getMonth(),
    dateOfFirstRegistration.getDate()
  );

  const now = new Date(Date.now());
  const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  const age = date.getTime();
  const todayTime = today.getTime();
  const ageFromToday = todayTime - age;

  return ageFromToday >= 0;
};
