import {IUser} from "./user.interface";
import {IAddress} from "./address.interface";
import {IDocument} from "./document.interface";
import {IAssessment} from "./assessment.interface";
import {IDriverCompliance} from "./compliance.interface";
import {IFawCertificate, IInsurance} from "./common.interface";
import {IVehicle} from "./vehicle.interface";

export interface IRestriction {
  code: string,
  description: string
}

export interface IAllowedVehicle {
  category: string,
  startDate: Date,
  endDate: Date,
  description: string,
  restrictions: IRestriction[],
  expanded : boolean
}

export interface IAllowedVehicles {
  vehicles: IAllowedVehicle[],
  provisional: IAllowedVehicle[],
}

export interface IEndorsement {
  category: string,
  points: number,
  description: string,
  courtDetails: {
    code: string,
    description: string
  },
  offenceDate: Date,
  expiryDate: Date,
  removalDate: Date
}

export interface ITachographCard {
  status: string,
  validFrom: Date,
  validTo: Date,
  number: string
}

export interface ICpcCategory {
  category: string,
  endDate: Date,
  description: string
}

export interface ICpc {
  categories: ICpcCategory[]
}

export interface IDrivingLicence {
  number: string,
  issueNumber?: number,
  status?: string,
  validFrom?: Date,
  validTo?: Date,
  endorsements?: IEndorsement[],
  points: number,
  allowedVehicles?: IAllowedVehicles,
  tachographCard?: ITachographCard,
  cpc?: ICpc,
  checkCode: string,
  checkDate: Date,
  scan?: {
    front?: string,
    back?: string
  },
}

export interface IContact {
  _id?: any,
  title?: string,
  forename: string,
  surname: string,
  address: IAddress,
  phone: string,
  mobile?: string,
  email?: string
}

export interface IDriverInsurance extends IInsurance {}

export interface IDriver {
  _id?: any,
  _companyId?: string,
  _user: IUser,
  fullName?: string,
  address: IAddress,
  dateOfBirth: Date,
  gender: string,
  licenceNumber?: string,
  niNumber: string,
  phone: string,
  mobile?: string,
  licence: {
    checks: IDrivingLicence[],
    nextCheckDate?: Date,
  },
  nextOfKin?: IContact,
  references?: IContact[],

  insuranceHistory?: IDriverInsurance[],

  fawRequired?: boolean;
  fawHistory?: IFawCertificate[],
  dbsExpiry?: Date,

  assessments?:  IAssessment[],
  nextAssessmentDate?: Date,
  lastAssessmentUpdatedDate?: Date,
  lastAssessmentLoginDate?: Date,

  documents?: IDocument[],

  vehicles?: Array<IVehicle>,
  ownedVehicle?: boolean,

  compliance?: IDriverCompliance,
  removed?: boolean

  // Sea Cadets
  pNumber?: string;
}

export interface IDriverVehicleMap {
  driverId: string|IDriver,
  vehicleId: string|IVehicle
}

export interface IDriverImport {
  forename: string,
  surname: string,
  email: string,
  ownedVehicle: boolean
}
