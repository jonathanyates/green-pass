import logger = require('winston');
import mongoose from 'mongoose';
import { injectable, inject } from 'inversify';
import { IApiService } from '../interfaces/api-service.interface';
import '../../../shared/extensions/date.extensions';
import {
  IVehicle,
  IVehicleCheck,
  IVehicleInspection,
  IVehicleService,
  IVehicleSummary,
} from '../../../shared/interfaces/vehicle.interface';
import { TYPES } from '../container/container.types';
import { IVehicleModel } from './vehicle.schema';
import { ICompanyModel } from '../companies/company.schema';
import { IDvlaService } from '../dvla/service/dvla.service.interface';
import { inspectionIntervals } from '../../../shared/models/inspection-intervals';
import { IFileService } from '../files/file.service';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';
import { IDbContext } from '../db/db.context.interface';
import { IVehicleRepository } from './vehicle.repository';
import { Vehicle } from '../../../client/src/app/ui/vehicle/vehicle.model';
import { VehicleTypes } from '../../../shared/interfaces/constants';
import { IDriverVehicleMapRepository } from '../drivers/driver-vehicle-map.repository';
import { IDriverRepository } from '../drivers/driver.repository';
import { ICompanyRepository } from '../companies/company.repository';
import { SequencePromise } from '../../../shared/extensions/promise.extensions';
import { mapMotEnquiryToVehicle } from '../dvla/service/motEnquiryMapper';
import { mapVehicleEnquiryToVehicle } from '../dvla/service/vehicleEnquiryMapper';
import { mapHgvTestEnquiryToVehicle } from '../dvla/service/hgvTestEnquiryMapper';

const delay = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

export interface IVehicleApiService extends IApiService<IVehicle> {
  findByPage(criteria: any, page: number, pageSize: number): Promise<{ total: number; items: IVehicle[] }>;

  remove(vehicleId: string): Promise<IVehicle>;

  addService(companyId: string, vehicleId: string, service: any): Promise<IVehicleService>;
  updateService(companyId: string, vehicleId: string, serviceId: string, service: any): Promise<IVehicleService>;

  addInspection(companyId: string, vehicleId: string, inspection: any): Promise<IVehicleInspection>;
  updateInspection(
    companyId: string,
    vehicleId: string,
    inspectionId: string,
    inspection: any
  ): Promise<IVehicleInspection>;

  addCheck(companyId: string, vehicleId: string, check: IVehicleCheck): Promise<IVehicleCheck>;
  updateCheck(companyId: string, vehicleId: string, checkId: string, check: IVehicleCheck): Promise<IVehicleCheck>;

  vehicleCheck(companyId: string, registrationNumber: string, vehicle?: IVehicle, checkExists?: boolean);
  vehicleChecks(): Promise<IVehicle[]>;
  getVehicleSummary(companyId: string): Promise<IVehicleSummary>;

  addDriver(driverId: string, vehicleId: string): Promise<IVehicle>;

  removeDriver(driverId: string, vehicleId: string): Promise<IVehicle>;
}

@injectable()
export class VehicleService implements IVehicleApiService {
  constructor(
    @inject(TYPES.repositories.VehicleRepository) private vehicleRepository: IVehicleRepository,
    @inject(TYPES.repositories.DriverRepository) private driverRepository: IDriverRepository,
    @inject(TYPES.repositories.DriverVehicleMapRepository)
    private driverVehicleMapRepository: IDriverVehicleMapRepository,
    @inject(TYPES.repositories.CompanyRepository) private companyRepository: ICompanyRepository,
    @inject(TYPES.services.DvlaService) private dvlaService: IDvlaService,
    @inject(TYPES.services.FileService) private fileService: IFileService,
    @inject(TYPES.db.DbContext) private dbContext: IDbContext
  ) {}

  addService(companyId: string, vehicleId: string, service: IVehicleService): Promise<IVehicleService> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle) => {
      const file = service.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const vehicleService = {
        serviceDate: new Date(service.serviceDate),
        supplierId: new mongoose.Types.ObjectId(service.supplierId),
        filename: filename,
        fileType: service.fileType,
      };

      if (!vehicle.serviceHistory) {
        vehicle.serviceHistory = [];
      }
      vehicle.serviceHistory.push(vehicleService);
      vehicle.nextService = this.getNextServiceDate(vehicle);

      service = vehicle.serviceHistory.find((service) => service.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          vehicleId: vehicle._id,
          serviceId: service._id,
        },
      };

      if (path) {
        return this.fileService
          .createFile(filename, path, options)
          .then((x) => vehicle.save().then((vehicle) => service));
      }

      return vehicle.save().then((vehicle) => service);
    });
  }

  updateService(companyId: string, vehicleId: string, serviceId: string, service: any): Promise<IVehicleService> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle: IVehicleModel) => {
      let vehicleService = vehicle.serviceHistory.find((item) => item._id.toString() == serviceId);

      if (!vehicleService) {
        throw ServerError(`Service ${serviceId} not found for vehicle ${vehicleId}.`, codes.NOT_FOUND);
      }

      const file = service.file;
      const filename = vehicleService.filename ? vehicleService.filename : file ? file.filename : null;

      vehicleService.filename = filename;
      vehicleService.serviceDate = new Date(service.serviceDate);
      vehicleService.supplierId = new mongoose.Types.ObjectId(service.supplierId);
      vehicleService.fileType = service.fileType;
      vehicle.nextService = this.getNextServiceDate(vehicle);

      return <Promise<IVehicleService>>vehicle.save().then((vehicle) => {
        vehicleService = vehicle.serviceHistory.find((item) => item._id.toString() == serviceId);

        if (file) {
          const path = file.path;

          const options = {
            filename: filename,
            metadata: {
              vehicleId: vehicle._id,
              serviceId: serviceId,
            },
          };

          return this.fileService.updateFile(filename, path, options).then((x) => vehicleService);
        }

        return vehicleService;
      });
    });
  }

  addInspection(companyId: string, vehicleId: string, inspection: IVehicleInspection): Promise<IVehicleInspection> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle) => {
      const file = inspection.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const vehicleInspection = {
        inspectionDate: new Date(inspection.inspectionDate),
        supplierId: new mongoose.Types.ObjectId(inspection.supplierId),
        filename: filename,
        fileType: inspection.fileType,
      };

      if (!vehicle.inspectionHistory) {
        vehicle.inspectionHistory = [];
      }
      vehicle.inspectionHistory.push(vehicleInspection);
      vehicle.nextInspection = this.getNextInspectionDate(vehicle);
      inspection = vehicle.inspectionHistory.find((inspection) => inspection.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          vehicleId: vehicle._id,
          inspectionId: inspection._id,
        },
      };

      if (path) {
        return this.fileService
          .createFile(filename, path, options)
          .then((x) => vehicle.save().then((vehicle) => inspection));
      }

      return vehicle.save().then((vehicle) => inspection);
    });
  }

  updateInspection(
    companyId: string,
    vehicleId: string,
    inspectionId: string,
    inspection: any
  ): Promise<IVehicleInspection> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle: IVehicleModel) => {
      let vehicleInspection = vehicle.inspectionHistory.find((item) => item._id.toString() == inspectionId);

      if (!vehicleInspection) {
        throw ServerError(`Inspection ${inspectionId} not found for vehicle ${vehicleId}.`, codes.NOT_FOUND);
      }

      const file = inspection.file;
      const filename = vehicleInspection.filename ? vehicleInspection.filename : file ? file.filename : null;

      vehicleInspection.filename = filename;
      vehicleInspection.inspectionDate = new Date(inspection.inspectionDate);
      vehicleInspection.supplierId = new mongoose.Types.ObjectId(inspection.supplierId);
      vehicleInspection.fileType = inspection.fileType;

      return <Promise<IVehicleInspection>>vehicle.save().then((vehicle) => {
        vehicleInspection = vehicle.inspectionHistory.find((item) => item._id.toString() == inspectionId);

        if (file) {
          const path = file.path;

          const options = {
            filename: filename,
            metadata: {
              vehicleId: vehicle._id,
              inspectionId: inspectionId,
            },
          };

          return this.fileService.updateFile(filename, path, options).then((x) => vehicleInspection);
        }

        return vehicleInspection;
      });
    });
  }

  addCheck(companyId: string, vehicleId: string, check: IVehicleCheck): Promise<IVehicleCheck> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle: IVehicleModel) => {
      const file = check.file;
      const filename = file ? file.filename : null;
      const path = file ? file.path : null;

      const vehicleCheck = {
        checkDate: new Date(check.checkDate),
        checkedBy: check.checkedBy,
        odometerReading: check.odometerReading,
        filename: filename,
        fileType: check.fileType,
      };

      if (!vehicle.checkHistory) {
        vehicle.checkHistory = [];
      }
      vehicle.checkHistory.push(vehicleCheck);
      vehicle.nextVehicleCheck = this.getNextCheckDate(vehicle);
      check = vehicle.checkHistory.find((check) => check.filename === filename);

      const options = {
        filename: filename,
        metadata: {
          vehicleId: vehicle._id,
          checkId: check._id,
        },
      };

      if (path) {
        this.fileService.createFile(filename, path, options).then((x) => vehicle.save().then((vehicle) => check));
      }

      return vehicle.save().then((vehicle) => check);
    });
  }

  updateCheck(companyId: string, vehicleId: string, checkId: string, check: IVehicleCheck): Promise<IVehicleCheck> {
    return this.dbContext.vehicle.findById(vehicleId).then((vehicle: IVehicleModel) => {
      let vehicleCheck = vehicle.checkHistory.find((item) => item._id.toString() == checkId);

      if (!vehicleCheck) {
        throw ServerError(`Check ${checkId} not found for vehicle ${vehicleId}.`, codes.NOT_FOUND);
      }

      const file = check.file;
      const filename = vehicleCheck.filename ? vehicleCheck.filename : file ? file.filename : null;

      vehicleCheck.filename = filename;
      vehicleCheck.checkDate = new Date(check.checkDate);
      vehicleCheck.checkedBy = check.checkedBy;
      vehicleCheck.odometerReading = check.odometerReading;
      vehicleCheck.fileType = check.fileType;

      vehicle.nextVehicleCheck = this.getNextCheckDate(vehicle);

      return <Promise<IVehicleCheck>>vehicle.save().then((vehicle) => {
        vehicleCheck = vehicle.checkHistory.find((item) => item._id.toString() == checkId);

        if (file) {
          const path = file.path;

          const options = {
            filename: filename,
            metadata: {
              vehicleId: vehicle._id,
              checkId: checkId,
            },
          };

          return this.fileService.updateFile(filename, path, options).then((x) => vehicleCheck);
        }

        return vehicleCheck;
      });
    });
  }

  getAll(companyId: string): Promise<IVehicle[]> {
    if (!mongoose.Types.ObjectId.isValid(companyId)) {
      return Promise.reject(ServerError('Invalid company id.', codes.BAD_REQUEST));
    }

    return this.vehicleRepository.find({ _companyId: companyId });
  }

  get(id: string): Promise<IVehicle> {
    return this.vehicleRepository.get(id);
  }

  find(criteria: any): Promise<IVehicle[]> {
    return this.vehicleRepository.find(criteria);
  }

  findByPage(criteria: any, page: number, pageSize: number): Promise<{ total: number; items: IVehicle[] }> {
    return this.vehicleRepository.find(criteria, page, pageSize).then((vehicles) => {
      return this.vehicleRepository.count(criteria).then((total) => {
        return {
          total: total,
          page: page,
          items: vehicles,
        };
      });
    });
  }

  findOne(criteria: any): Promise<IVehicle> {
    return this.vehicleRepository.findOne(criteria);
  }

  async vehicleCheck(
    companyId: string,
    registrationNumber: string,
    vehicle?: IVehicle,
    checkExists = true
  ): Promise<IVehicle> {
    if (!vehicle) {
      vehicle = new Vehicle();
    }

    // set nextService to now if there is no service history
    if (!vehicle.serviceHistory || vehicle.serviceHistory.length === 0) {
      vehicle.nextService = new Date();
    }

    // set inspectionHistory to now if there is no inspection History
    if (!vehicle.inspectionHistory || vehicle.inspectionHistory.length === 0) {
      vehicle.nextInspection = new Date();
    }

    // set checkHistory to now if there is no check History
    if (!vehicle.checkHistory || vehicle.checkHistory.length === 0) {
      vehicle.nextVehicleCheck = new Date();
    }

    registrationNumber = registrationNumber.replace(/"/g, '').toUpperCase();

    try {
      logger.info(`Performing Dvla vehicle check for ${registrationNumber}`);

      const vehicleEnquiry = await this.dvlaService.vehicleEnquiry(registrationNumber);

      if (!vehicleEnquiry || !vehicleEnquiry.registrationNumber) {
        logger.error('Vehicle not found.');
        throw ServerError('Vehicle not found.', codes.NOT_FOUND);
      }

      if (checkExists) {
        const exists = await this.vehicleRepository.findOne({
          _companyId: companyId,
          registrationNumber: vehicleEnquiry.registrationNumber,
        });

        if (exists) {
          const message = 'A vehicle with the same registration number already exists on our system.';
          logger.error(message);
          throw ServerError(message, codes.CONFLICT);
        }
      }

      logger.info('Setting vehicle enquiry details');
      vehicle = mapVehicleEnquiryToVehicle(vehicleEnquiry, vehicle);
    } catch (error) {
      logger.error(`Error performing vehicle check for vehicle ${vehicle.registrationNumber}. Error: ${error.message}`);
    }

    try {
      logger.info('Performing MOT Check for ' + vehicle.registrationNumber);
      const motEnquiry = await this.dvlaService.motEnquiry(vehicle.registrationNumber);

      if (!motEnquiry) {
        logger.error(
          `MOT Check failed. No MOT enquiry details returned for registration ${vehicle.registrationNumber}`
        );
        return vehicle;
      }

      if (motEnquiry.motTests || motEnquiry.motTestDueDate) {
        vehicle = mapMotEnquiryToVehicle(motEnquiry, vehicle);
        return vehicle;
      }
    } catch (error) {
      logger.error('MOT Check failed.');
      logger.error(error.message);
    }

    try {
      logger.info(
        `No MOT details found for vehicle ${vehicle.registrationNumber}. Checking HGV/PSV Enquiry Service instead...`
      );

      // Else if there are no motTests and no Due Date then check the HGV/PSV Enquiry as the vehicle may be HGV/PSV
      const hgvEnquiry = await this.dvlaService.hgvTestEnquiry(registrationNumber);

      if (!hgvEnquiry) {
        logger.error(`No HGV Test enquiry details returned for registration ${vehicle.registrationNumber}`);
        return vehicle;
      }

      if (hgvEnquiry.annualTests || hgvEnquiry.annualTestExpiryDate) {
        vehicle = mapHgvTestEnquiryToVehicle(hgvEnquiry, vehicle);
      } else {
        logger.error(
          `No Annual Test history or Expiry Date returned in HGV Test enquiry details returned for registration ${vehicle.registrationNumber}`
        );
      }
    } catch (error) {
      logger.error('HGV Test Check failed.');
      logger.error(error.errorMessage || error.message);
    }

    logger.info('Vehicle check completed successfully');
    return vehicle;
  }

  vehicleChecks(): Promise<IVehicle[]> {
    // perform checks for vehicles where the tax or mot date is due within the next 7 days.
    const checkDate = new Date().addDays(7);
    return this.vehicleRepository
      .find({ $or: [{ taxDueDate: { $lte: checkDate } }, { motDueDate: { $lte: checkDate } }, { motDueDate: null }] })
      .then((vehicles) => {
        return Promise.all([
          SequencePromise.sequence(vehicles, (vehicle) =>
            this.vehicleCheck(vehicle._companyId, vehicle.registrationNumber, vehicle, false)
              .then((vehicle) => {
                if (vehicle) {
                  return this.vehicleRepository.update(vehicle);
                }
                return null;
              })
              .then((vehicle) => {
                return delay(500).then((_) => vehicle);
              })
              .catch((error) => {
                logger.error(
                  `Error performing vehicle check for vehicle ${vehicle.registrationNumber}. Error: ${error.message}`
                );
                return null;
              })
          ),
        ]);
      });
  }

  remove(vehicleId: string): Promise<IVehicle> {
    return Promise.all([
      this.vehicleRepository.remove(vehicleId),
      this.driverVehicleMapRepository
        .getVehicleDrivers(vehicleId)
        .then((drivers) =>
          Promise.all(drivers.map((driver) => this.driverVehicleMapRepository.remove(driver._id, vehicleId)))
        ),
    ]).then((_) => this.get(vehicleId));
  }

  add(companyId: string, vehicle: IVehicle): Promise<IVehicle> {
    return this.companyRepository.get(companyId).then((company: ICompanyModel) => {
      if (!company) {
        throw ServerError('Company not found.', codes.NOT_FOUND);
      }

      vehicle._companyId = companyId;
      vehicle.registrationNumber = vehicle.registrationNumber.replace(/"/g, '');

      if (!vehicle.dvlaCheckDate) {
        return this.vehicleCheck(vehicle._companyId, vehicle.registrationNumber, vehicle).then((vehicle) =>
          this.vehicleRepository.add(vehicle)
        );
      }

      return this.vehicleRepository.add(vehicle);
    });
  }

  update(vehicle: IVehicle | IVehicleModel): Promise<IVehicle> {
    return this.vehicleRepository.update(vehicle);
  }

  getNextServiceDate(vehicle: IVehicle): Date {
    if (!vehicle.serviceHistory || vehicle.serviceHistory.length === 0) {
      const now = new Date();
      return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    }

    const lastService = vehicle.serviceHistory.sort((a, b) => b.serviceDate.getTime() - a.serviceDate.getTime())[0];

    // if mileage is > 12000 miles then calc how many weeks to next service
    if (vehicle.annualMileage > 12000) {
      const weeklyMileage = Math.round(vehicle.annualMileage / 52.1429);
      const weeksToNextService = Math.floor(12000 / weeklyMileage);

      const now = new Date();
      return new Date(
        lastService.serviceDate.getFullYear(),
        lastService.serviceDate.getMonth(),
        lastService.serviceDate.getDate()
      ).addDays(weeksToNextService * 7);
    }

    const nextAnnualServiceDate = new Date(
      lastService.serviceDate.getFullYear() + 1,
      lastService.serviceDate.getMonth(),
      lastService.serviceDate.getDate()
    );

    return nextAnnualServiceDate;
  }

  getNextInspectionDate(vehicle: IVehicle): Date {
    if (!vehicle.inspectionHistory || vehicle.inspectionHistory.length === 0) {
      const now = new Date();
      return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    }

    const lastInspection = vehicle.inspectionHistory.sort(
      (a, b) => b.inspectionDate.getTime() - a.inspectionDate.getTime()
    )[0];

    let inspectionFrequency = 4;
    if (vehicle.annualMileage) {
      const annualMileage = vehicle.annualMileage / 1000;
      const intervalType = vehicle.inspectionIntervalType ? vehicle.inspectionIntervalType : 'A';
      const curve = inspectionIntervals[intervalType];
      const points = curve.points.filter((point) => point.x < annualMileage + 20);
      if (points.length > 0) {
        inspectionFrequency = points.slice(-1)[0].y; // take last
      }
    }

    const nextInspectionDate = new Date(
      lastInspection.inspectionDate.getFullYear(),
      lastInspection.inspectionDate.getMonth(),
      lastInspection.inspectionDate.getDate() + inspectionFrequency * 7
    );

    return nextInspectionDate;
  }

  getNextCheckDate(vehicle: IVehicle): Date {
    if (!vehicle.checkHistory || vehicle.checkHistory.length === 0) {
      const now = new Date();
      return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    }

    const last = vehicle.checkHistory.sort((a, b) => b.checkDate.getTime() - a.checkDate.getTime())[0];

    // one week from last check
    const nextCheckDate = new Date(
      last.checkDate.getFullYear(),
      last.checkDate.getMonth(),
      last.checkDate.getDate() + 7
    );

    return nextCheckDate;
  }

  getVehicleSummary(companyId: string): Promise<IVehicleSummary> {
    return Promise.all([
      this.vehicleRepository.count({ _companyId: new mongoose.Types.ObjectId(companyId), type: VehicleTypes.grey }),
      this.vehicleRepository.count({ _companyId: new mongoose.Types.ObjectId(companyId), type: VehicleTypes.fleet }),
    ]).then((results) => {
      return <IVehicleSummary>{
        _companyId: companyId,
        grey: results[0],
        fleet: results[1],
      };
    });
  }

  // add/remove driver

  addDriver(driverId: string, vehicleId: string): Promise<IVehicle> {
    return this.driverVehicleMapRepository.get(driverId, vehicleId).then((map) => {
      if (map) {
        throw ServerError(
          `Can not add Driver ${driverId} to Vehicle ${vehicleId}. Driver already exists.`,
          codes.CONFLICT
        );
      }

      return this.driverVehicleMapRepository.add(driverId, vehicleId).then((map) => this.get(vehicleId));
    });
  }

  removeDriver(driverId: string, vehicleId: string): Promise<IVehicle> {
    return this.driverVehicleMapRepository.remove(driverId, vehicleId).then((_) => this.get(vehicleId));
  }
}
