import {Component, OnInit, OnDestroy} from '@angular/core';
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {Subject} from "rxjs/Subject";
import {SelectCompany} from "../../../store/company/company.selectors";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {SelectVehicle} from "../../../store/vehicles/vehicle.selectors";
import {SaveVehicleAction} from "../../../store/vehicles/vehicle.actions";

@Component({
  selector: 'gp-search-vehicle-input',
  templateUrl: 'vehicle-search-input.component.html'
})
export class VehicleSearchInputComponent implements OnInit, OnDestroy {

  searchTerm$: Subject<string> = new Subject<string>();
  error: string;
  company: ICompany;
  vehicle: IVehicle;
  private subscriptions = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectVehicle())
      .filter(vehicle => vehicle !== null)
      .subscribe(vehicle => this.vehicle = vehicle));

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  vehicleChanged(vehicle: IVehicle) {
    this.vehicle = vehicle;
  }

  searchTermChanged(term: string) {
    this.searchTerm$.next(term);
  }

  save(vehicle: IVehicle) {
    this.store.dispatch(new SaveVehicleAction(vehicle));
  }

  cancel() {
    return this.router.navigate(['../'], { relativeTo:  this.route});
  }
}
