import {Component, Input} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {IDocument} from "../../../../../../../shared/interfaces/document.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-documents-card',
  templateUrl: 'driver-documents-card.component.html',
  host: {'class': 'card'}
})
export class DriverDocumentsCardComponent {

  total: number;
  completed: number;
  percentage: number;
  isComplete: boolean;

  private _driver: IDriver;
  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.setDocumentStatus(driver);
    }
  }

  constructor(private router: Router) {}

  private setDocumentStatus(driver: IDriver) {
    if (driver.documents) {
      let documents: IDocument[] = driver.documents;
      this.total = driver.compliance.documents.total;
      this.completed = driver.compliance.documents.completed;
      this.percentage = driver.compliance.documents.percentage;
      this.isComplete = driver.compliance.documents.compliant;
    }
  }

  viewDocuments() {
    this.router.navigate([Paths.Driver, Paths.Documents]);
  }
}
