const motTestData = {
  registration: 'E18EBY',
  make: 'FORD',
  model: 'FIESTA',
  firstUsedDate: '2017.05.31',
  fuelType: 'Petrol',
  primaryColour: 'WHITE',
  vehicleId: '4Tq319nVKLz+25IRaUo79w==',
  registrationDate: '2017.05.31',
  manufactureDate: '2017.05.31',
  engineSize: '998',
  motTests: [
    {
      completedDate: '2022.05.30 09:33:08',
      testResult: 'PASSED',
      expiryDate: '2023.06.08',
      odometerValue: '24855',
      odometerUnit: 'mi',
      odometerResultType: 'READ',
      motTestNumber: '914655760009',
      rfrAndComments: [],
    },
    {
      completedDate: '2013.11.01 11:28:34',
      testResult: 'FAILED',
      odometerValue: '47118',
      odometerUnit: 'mi',
      odometerResultType: 'READ',
      motTestNumber: '841470560098',
      rfrAndComments: [
        {
          text: 'Front brake disc excessively pitted (3.5.1h)',
          type: 'FAIL',
          dangerous: true,
        },
        {
          text: 'Nearside Rear wheel bearing has slight play (2.6.2)',
          type: 'ADVISORY',
          dangerous: false,
        },
      ],
    },
    {
      completedDate: '2018.05.20 11:28:34',
      testResult: 'FAILED',
      odometerValue: '57318',
      odometerUnit: 'mi',
      odometerResultType: 'READ',
      motTestNumber: '741489560458',
      rfrAndComments: [
        {
          text: 'Nearside Parking brake efficiency below requirements (1.4.2 (a) (i))',
          type: 'MAJOR',
          dangerous: false,
        },
        {
          text: 'Front brake disc excessively pitted (3.5.1h)',
          type: 'DANGEROUS',
          dangerous: false,
        },
        {
          text: 'tyres wearing unevenly',
          type: 'USER ENTERED',
          dangerous: true,
        },
      ],
    },
  ],
};

export default motTestData;
