import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./authentication.actions";
import {AuthorizationService} from "../../services/authorization.service";
import {AuthenticationHelper} from "./authentication.helper";
import {Router, ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {AuthenticationState} from "./authentication.reducer";
import "../../shared/rxjs-operators.ts";
import {
  ChangePasswordSuccessAction,
  ForgotPasswordAction, ForgotPasswordSuccessAction, LoginAction,
  SetRedirectUrlAction
} from "./authentication.actions";
import {Paths} from "../../shared/constants";
import {BusyAction} from "../progress/progress.actions";
import {Location} from '@angular/common';
import {IUser} from "../../../../../shared/interfaces/user.interface";
import {SelectAuthenticationState} from "./authentication.selectors";
import {Roles} from "../../../../../shared/constants";

declare let $: any;

@Injectable()
export class AuthenticationEffects {

  private authenticationState: AuthenticationState;

  constructor(private authService: AuthorizationService,
              private actions$: Actions,
              private router: Router,
              private store: Store<AppState>,
              private route: ActivatedRoute, private location: Location) {

    this.store.let(SelectAuthenticationState())
      .subscribe((state: AuthenticationState) => this.authenticationState = state);

    route.url.subscribe(url => this.authenticate());
  }

  private authenticate() {
    let token = localStorage.getItem('auth_token');
    let tokenValid = token
      && typeof token === 'string'
      && AuthenticationHelper.isTokenValid(token);

    let user = tokenValid ? AuthenticationHelper.getUserFromToken(token) : null;

    if (tokenValid && user) {
      this.store.dispatch(new actions.LoginSuccessAction(token, false));
    }
  }

  @Effect() login$ = this.actions$
    .ofType(actions.ActionTypes.LOGIN)
    .map((action:LoginAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.authService.login(payload.username, payload.password)
      .map(token => {
        if (AuthenticationHelper.isTokenValid(token)) {
          localStorage.setItem('auth_token', token);
          return new actions.LoginSuccessAction(token, true);
        }
        else {
          throw new Error('Login failed');
        }
      })
      .catch((err: Error) => Observable.of(new actions.LoginFailedAction()))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) loginSuccess = this.actions$
    .ofType(actions.ActionTypes.LOGIN_SUCCESS)
    .do((action: actions.LoginSuccessAction) => {
      if (action.redirect) {
        this.navigateUser(action.payload.user);
      }
    });

  private navigateUser(user: IUser) {
    if (this.authenticationState && this.authenticationState.redirectUrl) {
      let result = this.router.navigate([this.authenticationState.redirectUrl]);
      this.store.dispatch(new SetRedirectUrlAction(null)); // reset redirect url
      return result;
    }

    // Navigate based on the users role
    switch (user.role) {
      case Roles.Company :
        return this.router.navigate([user.role, user._companyId]);
      case Roles.Driver :
        return this.router.navigate([user.role]);
      case Roles.Admin :
        return this.router.navigate([user.role]);
      default :
        return this.router.navigate([Paths.Unauthorized]);
    }
  }

  @Effect({dispatch: false}) logout = this.actions$
    .ofType(actions.ActionTypes.LOGOUT)
    .do(action => {
      localStorage.removeItem('auth_token');
      return this.router.navigate([Paths.Root]);
    });

  // forgot password

  @Effect() forgotPassword = this.actions$
    .ofType(actions.ActionTypes.FORGOT_PASSWORD)
    .map((action:ForgotPasswordAction) => action.payload)
    .do(payload => this.store.dispatch(new BusyAction()))
    .switchMap(payload => this.authService.forgotPassword(payload)
      .map(result => {
        this.store.dispatch(new BusyAction(false));
        return new ForgotPasswordSuccessAction(result)
      })
      .catch((err: Error) => Observable.of(new actions.LoginFailedAction()))
      .finally(() => this.store.dispatch(new BusyAction(false)))
    );

  @Effect({ dispatch: false }) changePasswordSuccess = this.actions$
    .ofType(actions.ActionTypes.CHANGE_PASSWORD_SUCCESS)
    .map((action:ChangePasswordSuccessAction) => action.payload)
    .do(user => this.navigateUser(user))
    .catch((err: Error) => Observable.of(new actions.LoginFailedAction()));
}
