import {Component, Input, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {ICompanySubscription} from "../../../../../../shared/interfaces/subscription.interface";
import {FormatDate} from "../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-subscription-details',
  template: `    
    <gp-dynamic-list-group [items]="items" (edit)="edit()"></gp-dynamic-list-group>
    
    <ng-container *ngIf="trialItems">
      <h3 class="p-2 mt-3 bg-primary text-white">Trial</h3>
      <gp-dynamic-list-group [items]="trialItems"></gp-dynamic-list-group>
    </ng-container>
  `
})
export class SubscriptionDetailsComponent implements OnInit {

  items: IListGroupItem[];
  trialItems: IListGroupItem[];
  private _company: ICompany;
  private _subscription: ICompanySubscription;

  @Input() set company(company) {
    this._company = company;
    this._subscription = company.subscription;
    this.setItems(this._subscription);
  }
  get company() {
    return this._company;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
  }

  private setItems(subscription: ICompanySubscription) {
    if (!this._subscription) return;

    this.items = [
      {title: "Name", value: this._subscription.name},
      {title: "Start Date", value: FormatDate(subscription.startDate)},
      {title: "Renewal Date", value: FormatDate(subscription.renewalDate)},
      {title: "Active", value: this.formatBoolean(subscription.active)},
      {title: "Mandates", value: this.formatBoolean(subscription.resources.mandates)},
      {title: "Licence Checks", value: this.formatBoolean(subscription.resources.licenceCheck)},
      {title: "On Line Assessments", value: this.formatBoolean(subscription.resources.onLineAssessments)},
      {title: "On Road Training", value: this.formatBoolean(subscription.resources.onRoadTraining)},
      {title: "Workshops", value: this.formatBoolean(subscription.resources.workshops)},
      {title: "Presentations", value: this.formatBoolean(subscription.resources.presentations)},
      {title: "Alerts", value: this.formatAlerts(subscription.resources.alerts)}
    ];

    if (subscription.trial) {
      let trial = subscription.trial;
      this.trialItems = [
        {title: "Trial", value: 'Yes'},
        {title: "End Date", value: FormatDate(trial.endDate)},
        {title: "Mandates", value: this.formatBoolean(trial.subscription.resources.mandates)},
        {title: "Licence Checks", value: this.formatBoolean(trial.subscription.resources.licenceCheck)},
        {title: "On Line Assessments", value: this.formatBoolean(trial.subscription.resources.onLineAssessments)},
        {title: "On Road Training", value: this.formatBoolean(trial.subscription.resources.onRoadTraining)},
        {title: "Workshops", value: this.formatBoolean(trial.subscription.resources.workshops)},
        {title: "Presentations", value: this.formatBoolean(trial.subscription.resources.presentations)},
        {title: "Alerts", value: this.formatAlerts(trial.subscription.resources.alerts)}
      ]
    }
  }

  edit() {
    return this.router.navigate(['../', Paths.Subscription, Paths.Edit], { relativeTo: this.route });
  }

  formatBoolean(resource: boolean) {
    return resource ? 'Yes' : 'No';
  }

  formatAlerts(alerts: number) {
    if (alerts === -1) {
      return 'Unlimited'
    } else if (alerts === 0) {
      return 'None'
    }
    return alerts;
  }

}
