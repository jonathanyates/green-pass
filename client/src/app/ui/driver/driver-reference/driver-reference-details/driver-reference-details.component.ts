import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../../shared/components/dynamic-list-group/dynamic-list-group";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {IContact, IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectDriversState} from "../../../../store/drivers/driver.selectors";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {SelectLoggedInUser} from "../../../../store/authentication/authentication.selectors";
import {SelectCompany} from "../../../../store/company/company.selectors";
import {Roles} from "../../../../../../../shared/constants";

@Component({
  selector: 'gp-driver-reference-details',
  template: `
    <gp-card [header]="'Driver reference'" (edit)="edit()" [allowEdit]="allowEdit">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class DriverReferenceDetailComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  allowEdit: boolean;
  private user: IUser;
  private reference: IContact;
  private driver: IDriver;
  private subscriptions = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriversState())
      .filter(state => state.selectedReference != null)
      .subscribe(state => {
        this.driver = state.selected;
        this.reference = state.selectedReference;
        if (this.reference) {
          this.items = [
            {title: "Name", value: this.reference.forename + ' ' + this.reference.surname},
            {title: "Address", value: this.reference.address},
            {title: "Phone", value: this.reference.phone},
            {title: "Mobile", value: this.reference.mobile},
            {title: "Email", value: this.reference.email}
          ];
        }
      }));


    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .combineLatest(this.store.let(SelectLoggedInUser()))
      .subscribe(results => {
        let company = results[0];
        this.user = results[1];
        let isDriver = this.user && this.user.role == Roles.Driver;
        this.allowEdit = isDriver === false ||
          (company.settings && company.settings.drivers && company.settings.drivers.allowInput);
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
      return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
