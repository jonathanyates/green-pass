import mongoose from 'mongoose';
import { IQueryableRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { Query } from 'mongoose';
import { IReportSchedule } from '../../../shared/interfaces/report.interface';
import { IReportScheduleModel } from './report-schedule.schema';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class ReportScheduleRepository implements IQueryableRepository<IReportScheduleModel> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  private populate(model) {
    return model.populate('reportType').populate('recipients');
  }

  getAll(): Promise<IReportScheduleModel[]> {
    return this.queryAll().exec();
  }

  get(id: string): Promise<IReportScheduleModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid ReportSchedule id.', codes.BAD_REQUEST));
    }

    return this.query(id).exec();
  }

  queryAll(): Query<IReportScheduleModel[], mongoose.Document> {
    return this.populate(this.dbContext.reportSchedule.find());
  }

  query(id: string): Query<IReportScheduleModel, mongoose.Document> {
    return this.populate(this.dbContext.reportSchedule.findById(id));
  }

  find(criteria: any): Promise<IReportScheduleModel[]> {
    return this.populate(this.dbContext.reportSchedule.find(criteria)).exec();
  }

  findOne(criteria: any): Promise<IReportScheduleModel> {
    return this.populate(this.dbContext.reportSchedule.findOne(criteria)).exec();
  }

  async add<T extends IReportSchedule>(ReportSchedule: IReportScheduleModel): Promise<IReportScheduleModel> {
    const reportScheduleModel = new this.dbContext.reportSchedule(ReportSchedule);
    // const populatedModel = await this.populate(reportScheduleModel);
    const savedModel = await reportScheduleModel.save();
    return savedModel;
  }

  update<T extends IReportSchedule>(ReportSchedule: IReportSchedule): Promise<IReportScheduleModel> {
    return this.populate(
      this.dbContext.reportSchedule.findByIdAndUpdate(ReportSchedule._id, ReportSchedule, { new: true })
    )
      .exec()
      .then((model) => model.toObject());
  }

  remove<T extends IReportSchedule>(reportScheduleId: string): Promise<IReportScheduleModel> {
    return this.populate(this.dbContext.reportSchedule.findByIdAndRemove(reportScheduleId)).exec();
  }
}
