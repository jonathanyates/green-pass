import {Component, OnDestroy, OnInit} from '@angular/core';
import {IDrivingLicence} from "../../../../../../shared/interfaces/driver.interface";
import {Subscription} from "rxjs";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectDriver, SelectDriverLicence} from "../../../store/drivers/driver.selectors";

@Component({
  selector: 'gp-driver-licence-container',
  templateUrl: 'driver-licence-container.component.html'
})
export class DriverLicenceContainerComponent implements OnInit, OnDestroy {

  licence: IDrivingLicence;
  nextCheckDate: Date;
  private subs: Subscription[] = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subs.push(this.store.let(SelectDriverLicence())
      .combineLatest(this.store.let(SelectDriver())
        .filter(driver => driver != null && driver.licence != null)
        .map(driver => driver.licence.nextCheckDate))
      .subscribe(results => {
        this.licence = results[0];
        this.nextCheckDate = results[1];
      }));
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
