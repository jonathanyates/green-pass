import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {PageState} from "./page.state";

export const SelectPage = () => {
  return (store:Store<AppState>) => {
    return store.select<PageState>(state => state.page)
      .map(state => state && state.page ? state.page : null);
  }
};
