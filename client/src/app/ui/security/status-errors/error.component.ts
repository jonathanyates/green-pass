import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.states';
import { ClearErrorAction } from '../../../store/error/error.actions';
import { ErrorState } from '../../../store/error/error.state';
import { Router } from '@angular/router';
import { Paths } from '../../../shared/constants';
import { SelectError } from '../../../store/error/error.selectors';

@Component({
  selector: 'gp-error',
  template: `
    <gp-heading [text]="'Error'"></gp-heading>
    <div *ngIf="error" class="alert alert-danger" role="alert">
      <p>
        <span>{{ message }}</span>
        <br />
        <span>Please contact your administrator for support and if possible send the following error details.</span>
        <br />
        <span><gp-dynamic-content [content]="error.message"></gp-dynamic-content></span>
      </p>
    </div>
  `,
})
export class ErrorComponent implements OnInit {
  message: string;
  error: ErrorState;

  constructor(private location: Location, private store: Store<AppState>, private router: Router) {}

  ngOnInit() {
    this.store.let(SelectError()).subscribe((error) => {
      if (error) {
        this.message = this.getMessage(error.code);
        this.error = error;
      } else {
        this.router.navigate([Paths.Root]);
      }
    });
  }

  private getMessage(code: number): string {
    switch (code) {
      case 404:
        return 'Unfortunately the resource you were looking for could not be found.';
      case 413:
        return 'Unfortunately the file you are trying upload is too large. Please try a smaller file.';
      case 500:
        return 'Unfortunately something went wrong on our server.';
      default:
        return 'Unfortunately an error has occured.';
    }
  }

  back() {
    this.store.dispatch(new ClearErrorAction());
    this.location.back();
  }
}
