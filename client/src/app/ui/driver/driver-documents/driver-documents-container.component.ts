import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Subscription} from "rxjs";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {IDocument} from "../../../../../../shared/interfaces/document.interface";
import {UpdateDriverStatusAction} from "../../../store/drivers/driver.actions";

@Component({
  selector: 'gp-driver-documents-container',
  template: `
    <gp-card [header]="'My Documents'">
        <gp-driver-documents [documents]="documents" [lightHeader]="true" (select)="onSelect($event)"></gp-driver-documents>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
`
})
export class DriverDocumentsContainerComponent implements OnInit {

  documents: IDocument[];
  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.subscriptions.push(
      this.store.let(SelectDriver())
        .filter(driver => driver !== null)
        .subscribe(driver => this.documents = driver.documents));

    this.subscriptions.push(
      this.store.let(SelectDriver())
        .filter(driver => driver !== null).take(1)
        .subscribe(driver => this.store.dispatch(new UpdateDriverStatusAction(driver))));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onSelect(document: IDocument) {
    return this.router.navigate(['./', document.documentId], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
