import {Store} from "@ngrx/store";
import {UsersState} from "./users.state";
import {AppState} from "../app.states";

export const SelectUsersState = () => {
  return (store:Store<AppState>) => {
    return store.select<UsersState>(state => state.users);
  }
};

export const SelectUsers = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectUsersState())
      .map(state => state.list);
  }
};

export const SelectSelectedUser = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectUsersState())
      .map(state => state.selected)
      .distinctUntilChanged();
  }
};
