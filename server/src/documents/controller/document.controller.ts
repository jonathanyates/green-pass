import mongoose from 'mongoose';
import { inject, injectable } from 'inversify';
import { IDocumentController } from './document.controller.interface';
import { NextFunction, Request, Response } from 'express-serve-static-core';
import { TYPES } from '../../container/container.types';
import { IDocumentService } from '../service/document.service.interface';
import { IDocumentSignatureService } from '../interfaces/signature.service.interface';
import codes from '../../shared/error.codes';

@injectable()
export class DocumentController implements IDocumentController {
  constructor(
    @inject(TYPES.services.DocumentService) private documentService: IDocumentService,
    @inject(TYPES.services.DocumentSignatureService) private signatureService: IDocumentSignatureService
  ) {}

  getDocuments = (req, res, next) => {
    const driverId = req.params.driverId;

    if (!mongoose.Types.ObjectId.isValid(driverId)) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid driver id.' });
    }

    this.documentService
      .getDocuments(driverId)
      .then((documents) => {
        return res.status(codes.ACCEPTED).json(documents);
      })
      .catch((error) => {
        if (error.code === 'ENOTFOUND') {
          error = new Error('Unable to connect to document service at this time.');
          error.statusCode = codes.SERVICE_UNAVAILABLE;
        }
        next(error);
      });
  };

  sendDriverDocuments = (req, res, next) => {
    const driverId = req.params.driverId;

    this.documentService
      .sendDocuments(driverId)
      .then((driver) => {
        return next(null, res.status(codes.ACCEPTED).json(driver));
      })
      .catch((error) => {
        next(error);
      });
  };

  requestRecipientView = (req, res, next) => {
    const driverId = req.params.driverId;
    const documentId = req.params.documentId;
    const returnUrl = req.query.returnUrl;

    if (!mongoose.Types.ObjectId.isValid(driverId)) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid driver id.' });
    }

    if (!documentId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'documentId is required.' });
    }

    return this.documentService
      .requestEmbeddedUrl(driverId, documentId, returnUrl)
      .then((viewUrl) => {
        return res.status(codes.ACCEPTED).json(viewUrl);
      })
      .catch((error) => {
        next(error);
      });
  };

  getDocumentPdf = (req: Request, res: Response, next: NextFunction) => {
    const documentId = req.params.documentId;

    if (!documentId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'documentId is required.' });
    }

    return this.signatureService
      .getDocumentPdf(documentId)
      .then((data) => {
        return res.status(codes.OK).send(data);
      })
      .catch((error) => {
        next(error);
      });
  };

  getNewEmbeddedUrl = (req: Request, res: Response, next: NextFunction) => {
    const driverId = req.params.driverId;

    if (!driverId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'Invalid driver id.' });
    }

    const documentId = req.params.documentId;

    if (!documentId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'documentId is required.' });
    }

    const signerId = req.params.signerId;

    if (!signerId) {
      return res.status(codes.BAD_REQUEST).send({ message: 'signerId is required.' });
    }

    return this.documentService
      .getNewEmbeddedUrl(driverId, documentId, signerId)
      .then((url) => {
        return res.status(codes.OK).send(url);
      })
      .catch((error) => {
        next(error);
      });
  };
}
