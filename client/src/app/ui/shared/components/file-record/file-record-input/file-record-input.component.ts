import {Component, EventEmitter, Output, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {FileRecordInputMapper} from "./file-record-input.mapper";
import {FormInputGroup} from "../../dynamic-form/dynamic-form.model";
import {IFileRecord} from "../../../../../../../../shared/interfaces/common.interface";

@Component({
  selector: 'gp-fileRecord-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="onSave($event)" (cancel)="onCancel()"></gp-dynamic-form>
  `
})
export class FileRecordInputComponent {

  inputGroups: FormInputGroup[];

  @Input() heading: string;
  @Input() fileHeading: string;

  private _fileRecord: IFileRecord;
  @Input() set fileRecord(fileRecord: IFileRecord) {

    if (!fileRecord) {
      this._fileRecord = <IFileRecord> {
        validFrom: null,
        validTo: null
      }
    } else {
      this._fileRecord = fileRecord;
    }

    this.inputGroups = this.fileRecordInputMapper.mapTo(this._fileRecord,
      this.heading, this.fileHeading);
  }

  @Output() submit: EventEmitter<IFileRecord> = new EventEmitter<IFileRecord>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fileRecordInputMapper: FileRecordInputMapper) { }

  onSave(form: FormGroup) {
    let fileRecord = Object.assign({}, this._fileRecord, this.fileRecordInputMapper.mapFrom(form, this.inputGroups, this._fileRecord));
    this.submit.emit(fileRecord);
  }

  onCancel() {
    this.cancel.emit();
  }
}
