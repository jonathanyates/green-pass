import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'gp-dynamic-content',
  template: `
    <div [innerHTML]="content | safeHtml"></div>
  `
})
export class DynamicContentComponent implements OnInit {

  @Input() content:string;

  constructor() { }

  ngOnInit() {
  }

}
