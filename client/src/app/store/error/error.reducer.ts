import * as actions from "./error.actions";
import {ErrorState} from "./error.state";
import {ActionTypes} from "../authentication/authentication.actions";
import {ServerError} from "../../shared/errors/server.error";

const initialState: ErrorState = null;

export function ErrorReducer(state: ErrorState = initialState, action: actions.Actions): ErrorState {
    switch (action.type) {
      case actions.ActionTypes.HANDLE_ERROR:
      case actions.ActionTypes.SET_ERROR:
          return Object.assign({}, <ServerError>action.payload);

      case actions.ActionTypes.CLEAR_ERROR:
        return null;

      case ActionTypes.LOGOUT:
        return null;

      default:
        return state;
    }
  };
