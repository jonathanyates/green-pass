import {Component, Input} from "@angular/core";

@Component({
  selector: 'gp-error-alert',
  template: `
    <ng-container *ngIf="error">
      <div class="row alert alert-danger m-1" role="alert">
        <p>
          <gp-dynamic-content [content]="error"></gp-dynamic-content>
        </p>
      </div>
    </ng-container>`
})
export class ErrorAlertComponent {
  @Input() error: string;
}
