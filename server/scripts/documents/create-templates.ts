import 'reflect-metadata';
import path = require('path');
import { TYPES } from '../../src/container/container.types';
import { IDocumentSignatureService } from '../../src/documents/interfaces/signature.service.interface';
import container from '../../src/container/container.config';

const filePath = path.normalize(__dirname + '/../../../../../docs/documents');

console.log(`Creating templates from ${filePath} ...`);

const signatureService = container.get<IDocumentSignatureService>(TYPES.services.DocumentSignatureService);

signatureService
  .createTemplates(filePath)
  .then((companies) => {
    console.log('Created templates successfully.');
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
