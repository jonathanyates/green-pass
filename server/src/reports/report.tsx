import React from 'react';
import { IDynamicList } from '../../../shared/models/dynamic-list.model';
import { CSSProperties } from 'react';
import { FormatDate } from '../../../shared/utils/helpers';

const colors = {
  primary: '#00A550',
  headerText: '#fff',
};

const styles = {
  badge: {
    padding: '0.5em 0.5em',
    margin: '0.25em 0.4em',
    fontSize: '75%',
    fontWeight: 'bold',
    lineHeight: '1',
    color: '#fff',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    borderRadius: '0.25rem',
  },
  'success-color': { backgroundColor: '#00C851' },
  'warning-color': { backgroundColor: '#ffbb33' },
  'danger-color': { backgroundColor: '#ff4444' },
  'green-text': { color: '#4CAF50' },
  'red-text': { color: '#F44336' },
};

export const Table = (props) => {
  const name: string = props.name;
  let list: IDynamicList = props.list;

  if (!list) {
    list = {
      columns: [],
      items: [],
    };
  }

  if (!list.items) {
    list.items = [];
  }

  if (!list.columns) {
    list.columns = [];
  }

  const tableStyle: CSSProperties = {
    padding: 0,
    borderSpacing: 0,
    fontFamily: 'Roboto, sans-serif',
    fontSize: '1rem',
    fontWeight: 300,
    color: '#292b2c',
  };
  const tdStyle = {
    borderTop: '1px solid #eceeef',
  };
  const cellStyle: CSSProperties = {
    padding: '1em',
    fontFamily: 'Roboto, sans-serif',
    fontSize: '1rem',
    fontWeight: 300,
    textAlign: 'center',
  };
  const date = FormatDate(new Date(), 'EEEE, Do MMMM yyyy');

  return (
    <html>
      <head>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
      </head>
      <body>
        <h1>{name} Report</h1>
        <p>Date: {date}</p>

        <table style={tableStyle}>
          <thead>
            <tr style={{ color: colors.headerText, background: colors.primary }}>
              {list.columns.map((column) => {
                return (
                  <th style={cellStyle} key={column.header}>
                    {column.header}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {list.items.map((item, i) => {
              return (
                <tr key={'row' + i}>
                  {list.columns.map((column) => {
                    let style: CSSProperties;

                    if (list.styles != null && list.styles[i] && list.styles[i][column.field]) {
                      const classes: string = list.styles[i][column.field].toString();
                      style = classes
                        .split(' ')
                        .reduce((acc, style) => Object.assign(acc, styles[style]), Object.assign({}, cellStyle));

                      if (classes.includes('fa')) {
                        style.fontWeight = 'normal';
                        item[column.field] = classes.includes('fa-check') ? 'Yes' : 'No';
                      }
                    } else {
                      style = cellStyle;
                    }
                    return (
                      <td style={tdStyle} key={column.field}>
                        <div style={style}>{item[column.field]}</div>
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </body>
    </html>
  );
};
