import {IInspectionIntervalCurve} from "../interfaces/vehicle.interface";

export const inspectionIntervals = {
  A: <IInspectionIntervalCurve>{
    description: 'Lightly loaded vehicles',
    points: [
      {x: 0, y: 13},
      {x: 5, y: 12},
      {x: 10, y: 11},
      {x: 15, y: 10},
      {x: 20, y: 10},
      {x: 25, y: 10},
      {x: 40, y: 9},
      {x: 60, y: 8},
      {x: 80, y: 7},
      {x: 100, y: 7},
      {x: 120, y: 6},
      {x: 140, y: 6},
      {x: 160, y: 6}
    ]
  },
  B: <IInspectionIntervalCurve>{
    description: 'General haulage',
    points: [
      {x: 0, y: 10},
      {x: 5, y: 9},
      {x: 10, y: 9},
      {x: 15, y: 8},
      {x: 20, y: 8},
      {x: 25, y: 8},
      {x: 40, y: 7},
      {x: 60, y: 6},
      {x: 80, y: 6},
      {x: 100, y: 5},
      {x: 120, y: 5},
      {x: 140, y: 5},
      {x: 160, y: 4}
    ]
  },
  C: <IInspectionIntervalCurve>{
    description: 'Arduous work',
    points: [
      {x: 0, y: 8},
      {x: 5, y: 7},
      {x: 10, y: 7},
      {x: 20, y: 6},
      {x: 25, y: 6},
      {x: 30, y: 6},
      {x: 35, y: 6},
      {x: 40, y: 5},
      {x: 60, y: 5},
      {x: 80, y: 4},
      {x: 100, y: 4},
      {x: 120, y: 4},
      {x: 140, y: 4},
      {x: 160, y: 4}
    ]
  }
};