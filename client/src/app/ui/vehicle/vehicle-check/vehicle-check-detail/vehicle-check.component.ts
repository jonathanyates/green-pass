import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../../shared/components/dynamic-list-group/dynamic-list-group";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {SelectVehiclesState} from "../../../../store/vehicles/vehicle.selectors";
import {IVehicle, IVehicleCheck} from "../../../../../../../shared/interfaces/vehicle.interface";
import {FormatDate} from "../../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-vehicle-check',
  template: `
    <gp-card [header]="'Vehicle check'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class VehicleCheckComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private check: IVehicleCheck;
  private vehicle: IVehicle;
  private subscription: Subscription;

  url: any;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectVehiclesState())
      .filter(state => state.selectedCheck != null)
      .subscribe(state => {
        this.vehicle = state.selected;
        this.check = state.selectedCheck;

        if (this.check) {
          this.items = [
            {title: "Check Date", value: FormatDate(this.check.checkDate) },
            {title: "Checked By", value: this.check.checkedBy },
            {title: "Odometer Reading", value: this.check.odometerReading }
          ];

          if (this.check.filename) {
            this.items.push({title: 'Vehicle Check Report', value: this.check.file, type: this.check.fileType})
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
