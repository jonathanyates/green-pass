import * as actions from "./company.actions";
import {CompanyState} from "./company.state";
import {Company} from "../../ui/company/company.model";
import {ActionTypes} from "../authentication/authentication.actions";
import {SaveCompanyInsuranceSuccessAction, SelectCompanyInsuranceSuccessAction} from "./company-insurance.actions";
import * as CompanyInsuranceActions from './company-insurance.actions';

const initialState: CompanyState = {
  company: null,
  selectedInsurance: null,
  selectedTab: null
};

export function CompanyReducer(state: CompanyState = initialState, action: actions.Actions): CompanyState {
  let fleetInsuranceHistory;
  let selectedTab = state.company && action.payload
    ? state.company._id == action.payload._id
      ? state.selectedTab : null
    : null;

  switch (action.type) {

    case actions.ActionTypes.LOAD_SUCCESS:
      return {
        company: action.payload,
        selectedInsurance: null,
        selectedTab: selectedTab
      };

    case actions.ActionTypes.SAVE_SUCCESS:
      return {
        company: action.payload,
        selectedTab: selectedTab
      };


    case actions.ActionTypes.CLEAR:
    case actions.ActionTypes.LOAD_FAIL:
      return {company: null};

    case actions.ActionTypes.SAVE_DEPOT:
      let depotList;
      const depots = state.company.depots;
      const depot = action.payload;

      if (depot._id) {
        depotList = depots.map(item => {
          return item._id === depot._id ? depot : item
        });
      } else {
        depotList = [...depots, action.payload];
      }

      return Object.assign({}, state, {
        company: Object.assign(new Company(), state.company, {
          depots: depotList
        })
      });

    case actions.ActionTypes.SAVE_SUPPLIER:
      let supplierList;
      const suppliers = state.company.suppliers;
      const supplier = action.payload;

      if (supplier._id) {
        supplierList = suppliers.map(item => {
          return item._id === supplier._id ? supplier : item
        });
      } else {
        supplierList = [...suppliers, action.payload];
      }

      return Object.assign({}, state, {
        company: Object.assign(new Company(), state.company, {
          suppliers: supplierList
        })
      });

    case actions.ActionTypes.SAVE_ALERT_TYPES:
      const alertTypes = action.payload;

      return Object.assign({}, state, {
        company: Object.assign(new Company(), state.company, {
          alertTypes: alertTypes
        })
      });

    //  CompanyInsuranceActions

    case CompanyInsuranceActions.ActionTypes.SAVE_COMPANY_INSURANCE_SUCCESS:
      const insurancePayload = (<SaveCompanyInsuranceSuccessAction>action).payload;
      const insuranceCompany = insurancePayload.company;
      const insurance = insurancePayload.insurance;

      let insuranceExists = insuranceCompany.fleetInsuranceHistory.some(item => item._id === insurance._id);

      if (insuranceExists) {
        fleetInsuranceHistory = insuranceCompany.fleetInsuranceHistory.map(item => {
          return item._id === insurance._id ? insurance : item
        });
      } else {
        fleetInsuranceHistory = [...insuranceCompany.fleetInsuranceHistory, insurance];
      }

      return Object.assign({}, state, {
        company: Object.assign({}, state.company, {
          fleetInsuranceHistory: fleetInsuranceHistory
        }),
        selectedInsurance: insurance
      });

    case CompanyInsuranceActions.ActionTypes.SELECT_COMPANY_INSURANCE_SUCCESS:
      let selectedInsurance = (<SelectCompanyInsuranceSuccessAction>action).payload;
      if (selectedInsurance && selectedInsurance.file) {
        fleetInsuranceHistory = state.company.fleetInsuranceHistory.map(item => {
          return item._id === selectedInsurance._id ? selectedInsurance : item
        });

        return Object.assign({}, state, {
          company: Object.assign({}, state.company, {
            fleetInsuranceHistory: fleetInsuranceHistory
          }),
          selectedInsurance: action.payload
        });
      } else {
        return Object.assign({}, state, {
          selectedInsurance: action.payload
        });
      }

    case CompanyInsuranceActions.ActionTypes.CLEAR_COMPANY_INSURANCE:
      return Object.assign({}, state, {
        selectedInsurance: null
      });

    case actions.ActionTypes.SELECT_SELECTED_TAB:
      return Object.assign({}, state, {
        selectedTab: action.payload
      });

    case ActionTypes.LOGOUT:
      return initialState;

    default:
      return state;
  }

};
