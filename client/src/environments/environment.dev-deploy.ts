// This code is to support multiple sub domains,
// so that we can deploy the same build to multiple sub domain deployments

let domain = window.location.hostname;
let subdomain = 'dev.';

if (domain.indexOf('.') > 0) {
  let part = domain.split('.')[0];
  if (part !== 'greenpasscompliance' && part !== 'localhost') {
    subdomain = `${part}.`;
  }
}

export const environment = {
  production: true,
  apiRoot: `https://${subdomain}app.greenpasscompliance.co.uk`,
  wwwRoot: `http://${subdomain}greenpasscompliance.co.uk`,
  legalesign: {
    username: '2ee1170d2f3a4b20a56d4d729b5187',
    group: 'green-pass-compliance',
    path: `https://${subdomain}app.greenpasscompliance.co.uk/driver/documents/`,
  },
};
