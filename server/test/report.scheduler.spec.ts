import 'reflect-metadata';
import { IReportScheduler, ReportScheduler } from '../src/reports/report.scheduler';
import { IReportQueryResult, IReportSchedule, IReportType } from '../../shared/interfaces/report.interface';
import { IReportQueryService } from '../src/reports/report-query.service';
import { IEmailService } from '../src/email/email.service';
import { IReportGeneratorService } from '../src/reports/report-generator.service';
import container from '../src/container/container.config';
import { TYPES } from '../src/container/container.types';
import { IUser } from '../../shared/interfaces/user.interface';
import { ICompanyService } from '../src/companies/company.service.interface';

describe.skip('Report Scheduler', function () {
  describe.skip('scheduleReport', function () {
    it('schedules a Report', () => {
      const reportQueryService: IReportQueryService = {
        queries: {
          test: (companyId: string, reportType: IReportType): Promise<IReportQueryResult> => {
            return Promise.resolve({
              reportType: reportType,
              result: [
                {
                  col1: 'foo1',
                  col2: 'bar1',
                },
                {
                  col1: 'foo2',
                  col2: 'bar2',
                },
              ],
            });
          },
        },
      };

      // let emailService: IEmailService = {
      //   send(recipients: string[], subject: string, content: string, attachments: any[]): Promise<boolean> {
      //     return Promise.resolve(true);
      //   }
      // };

      const companyService = container.get<ICompanyService>(TYPES.services.CompanyService);
      const emailService = container.get<IEmailService>(TYPES.services.EmailService);
      const reportGeneratorService = container.get<IReportGeneratorService>(TYPES.services.ReportGeneratorService);
      const reportScheduler = container.get<IReportScheduler>(TYPES.services.ReportScheduler);

      const reportSchedule: IReportSchedule = {
        recipients: [
          {
            username: 'jyates',
            password: 'xxx',
            forename: 'Jonathan',
            surname: 'Yates',
            email: 'jonathan.yates@hotmail.com',
            role: '',
          },
        ],
        cron: '*/5 * * * * *',
        reportType: {
          name: 'test',
          query: 'test',
          category: 'test',
        },
      };

      return reportScheduler.scheduleReport(reportSchedule).then((result) => {
        expect(result).toBeTruthy();
      });
    });
  });
});
