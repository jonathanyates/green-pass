import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {LogoutAction} from "../authentication/authentication.actions";
import {ITemplate} from "../../../../../shared/interfaces/document.interface";

export const ActionTypes = {
  LOAD:                     type('[Templates] Load'),
  LOAD_SUCCESS:             type('[Templates] Load Success'),
  SELECT_LIST:              type('[Templates] Select'),
  SELECT_LIST_SUCCESS:      type('[Templates] Select Success'),
  SELECT_TEMPLATE:          type('[Template] Select'),
  SELECT_TEMPLATE_SUCCESS:  type('[Template] Select Success'),
  SAVE_COMPANY_TEMPLATES:   type('[Templates] Save Company Templates'),
  SELECT_COMPANY_TEMPLATE:  type('[Company Template] Select'),
  SELECT_COMPANY_TEMPLATE_SUCCESS:  type('[Company Template] Select Success')
};

/**
 * Load Templates Actions
 */
export class LoadTemplatesAction implements Action {
  type = ActionTypes.LOAD;
  constructor(public payload?: string) { } //  payload company id
}

export class LoadTemplatesSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: ITemplate[]) { }
}

/**
 * Select Templates Actions
 */
export class SelectTemplatesAction implements Action {
  type = ActionTypes.SELECT_LIST;
  constructor(public payload?: string) { } //  payload company id
}

export class SelectTemplatesSuccessAction implements Action {
  type = ActionTypes.SELECT_LIST_SUCCESS;
  constructor(public payload: ITemplate[]) { }
}

/**
 * Select Template Actions
 */
export class SelectTemplateAction implements Action {
  type = ActionTypes.SELECT_TEMPLATE;
  constructor(public payload: string) {
  }
}

export class SelectTemplateSuccessAction implements Action {
  type = ActionTypes.SELECT_TEMPLATE_SUCCESS;
  constructor(public payload: ITemplate) { }
}

/**
 * Select Company Template Actions
 */
export class SelectCompanyTemplateAction implements Action {
  type = ActionTypes.SELECT_COMPANY_TEMPLATE;
  payload: { companyId:string, templateId:string };

  constructor(companyId:string, templateId:string) {
    this.payload = { companyId:companyId, templateId:templateId }
  }
}

export class SelectCompanyTemplateSuccessAction implements Action {
  type = ActionTypes.SELECT_COMPANY_TEMPLATE_SUCCESS;
  constructor(public payload: ITemplate) { }
}


/**
 * Save Templates Actions
 */
export class SaveCompanyTemplatesAction implements Action {
  type = ActionTypes.SAVE_COMPANY_TEMPLATES;
  payload: { companyId: string, templates: string[] };

  constructor(companyId: string, templates: string[]) {
    this.payload = { companyId: companyId, templates: templates }
  }
}

export type Actions
  = LoadTemplatesSuccessAction
  | SelectTemplatesSuccessAction
  | SelectTemplateSuccessAction
  | SelectCompanyTemplateSuccessAction
  | LogoutAction;
