import {Component, Input} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-vehicles-card',
  templateUrl: 'driver-vehicles-card.component.html',
  host: {'class': 'card'}
})
export class DriverVehiclesCardComponent {

  hasVehicles: boolean;
  private _driver: IDriver;

  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasVehicles = driver.compliance.vehicles;
    }
  }

  constructor(private router: Router) {}

  viewVehicles() {
    // navigate to add vehicle if the driver has no vehicles to view
    if (this._driver && (!this._driver.vehicles || this._driver.vehicles.length === 0)) {
      return this.router.navigate([Paths.Driver, Paths.Vehicles, Paths.Add]);
    }
    return this.router.navigate([Paths.Driver, Paths.Vehicles]);
  }
}
