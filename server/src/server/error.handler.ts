import {NextFunction, Response, Request} from "express-serve-static-core";
import codes from "../shared/error.codes";

export function ErrorHandler(err: any, req: Request, res: Response, next: NextFunction) {
  if (err) {
    if (err.name === "MongoError") {
      const error = JSON.stringify({
        errorType: err.name,
        code: err.code,
        message: err.message
      });
      res.status(codes.INTERNAL_SERVER_ERROR).send(error);
      return;
    }
    else {
      if (err.statusCode) {
        res.status(err.statusCode).send({message: err.message});
        return;
      }
      next(err);
    }
  } else {
    next()
  }
}