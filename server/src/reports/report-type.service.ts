import {injectable, inject} from "inversify";
import {IReportType} from "../../../shared/interfaces/report.interface";
import {TYPES} from "../container/container.types";
import {IRepository} from "../db/db.repository";

export interface IReportTypeService {
  getAll(): Promise<IReportType[]>;
  get(id: string): Promise<IReportType>;
  find(criteria: any): Promise<IReportType[]>;
  findOne(criteria: any): Promise<IReportType>;
  add(report: IReportType): Promise<IReportType>;
  update(report: IReportType): Promise<IReportType>;
}

@injectable()
export class ReportTypeService implements IReportTypeService {

  constructor(@inject(TYPES.repositories.ReportTypeRepository) private reportTypeRepository: IRepository<IReportType>) {
  }

  getAll = (): Promise<IReportType[]> => {
    return this.reportTypeRepository.getAll();
  };

  get = (id: string): Promise<IReportType> => {
    return this.reportTypeRepository.get(id);
  };

  find = (criteria: any): Promise<IReportType[]> => {
    return this.reportTypeRepository.find(criteria);
  };

  findOne = (criteria: any): Promise<IReportType> => {
    return this.reportTypeRepository.findOne(criteria);
  };

  add = (report: IReportType): Promise<IReportType> => {
    return this.reportTypeRepository.add(report);
  };

  update = (report: IReportType): Promise<IReportType> => {
    return this.reportTypeRepository.update(report);
  };

}