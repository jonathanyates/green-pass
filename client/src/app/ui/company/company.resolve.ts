import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Company} from "./company.model";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {LoadCompanyAction} from "../../store/company/company.actions";
import {SelectCompany} from "../../store/company/company.selectors";
import {SelectError} from "../../store/error/error.selectors";
import {SelectComplianceAction} from "../../store/compliance/compliance.actions";
import {LoadSelectedAlertSummaryAction, SelectAlertTypesAction} from "../../store/alerts/alert.actions";
import {SelectReportTypesAction} from "../../store/reports/report.actions";
import {ICompany} from "../../../../../shared/interfaces/company.interface";

@Injectable()
export class CompanyResolve {

  constructor(
    private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICompany|boolean> {

    let id = route.params['id'];

    if (!id) {
      return Observable.of(new Company());
    }

    this.store.dispatch(new LoadCompanyAction(id));
    this.store.dispatch(new SelectComplianceAction(id));
    this.store.dispatch(new LoadSelectedAlertSummaryAction(id));
    this.store.dispatch(new SelectAlertTypesAction());
    this.store.dispatch(new SelectReportTypesAction());

    return this.store.let(SelectCompany())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (company, error) => ({ company:company, error:error }))
      .filter(result => result.error != null || (result.company != null && result.company._id === id))
      .map(result => result.error ? false : result.company)
      .take(1);
  }
}
