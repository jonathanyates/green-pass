import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectError} from "../../../store/error/error.selectors";
import {SelectTemplates} from "../../../store/templates/template.selectors";
import {LoadTemplatesAction} from "../../../store/templates/template.actions";
import {SelectCompanyAction, ClearCompanyAction} from "../../../store/company/company.actions";
import {ITemplate} from "../../../../../../shared/interfaces/document.interface";

@Injectable()
export class TemplateListResolve {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITemplate[]|boolean> {

    let companyId = route.params['id'];
    if (companyId) {
      this.store.dispatch(new SelectCompanyAction(companyId));
      this.store.dispatch(new LoadTemplatesAction(companyId));
    } else {
      this.store.dispatch(new ClearCompanyAction());
      this.store.dispatch(new LoadTemplatesAction());
    }

    return this.store.let(SelectTemplates())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (templates, error) => ({templates: templates, error: error}))
      .filter(result => result.error != null || result.templates != null)
      .map(result => result.error ? false : result.templates)
      .take(1);
  }
}
