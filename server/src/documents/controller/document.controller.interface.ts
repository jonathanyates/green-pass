import {Request, Response, NextFunction} from "express-serve-static-core";

export interface IDocumentController {

  getDocuments(req:Request, res:Response, next: NextFunction);

  sendDriverDocuments(req:Request, res:Response, next: NextFunction);

  requestRecipientView(req:Request, res:Response, next: NextFunction);

  getDocumentPdf(req:Request, res:Response, next: NextFunction);

  getNewEmbeddedUrl(req:Request, res:Response, next: NextFunction);
}