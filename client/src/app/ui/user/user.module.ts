import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {UserListComponent} from "./user-list/user-list.component";
import {UserInputComponent} from "./user-input/user-input.component";
import { UserDetailsComponent } from './user-details/user-details.component';
import {UserRoutingModule} from "./user-routing.module";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    UserRoutingModule
  ],
  exports: [],
  declarations: [
    UserListComponent,
    UserInputComponent,
    UserDetailsComponent
  ],
  providers: [],
})
export class UserModule { }
