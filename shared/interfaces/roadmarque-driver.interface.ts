import {IAssessmentInvite, IAssessmentResult, ITrainingModule} from "./assessment.interface";

export interface IRoadMarqueDriver {
  basicDetails: {
    userId: number,
    firstname:  string,
    middlename:  string,
    surname:  string,
    surnameOnLicence:  string,
    refId:  string,
    emailAddress:  string,
    jobTitle:  string,
    department:  string,
    grade:  string,
    birthDate: Date,
    sex:  string,
    title:  string,
    doNotUseForResearch: boolean,
    pointsOnLicence:  number,
    trainingInitiatedInLastYear:  string,
    eLearningInvitedAny: boolean,
    eLearningCompletedAny: boolean,
    eLearningCompletedAll: boolean,
    eLearningInvitedTheory: boolean,
    eLearningCompletedTheory: boolean,
    eLearningInvitedCustomer: boolean,
    eLearningCompletedCustomer: boolean,
    eLearningInvitedStress: boolean,
    eLearningCompletedStress: boolean,
    classroomInvited: boolean,
    classroomCompleted: boolean,
    oneToOneInvited: boolean,
    oneToOneCompleted: boolean,
    supervisorInvited: boolean,
    supervisorCompleted: boolean,
    groupName: string
  },
  aptitudeAssessments: {
    assessmentResult?: IAssessmentResult[],
    overallAssessmentRiskLevel: string,
    assessmentInvite: {
      assessmentInviteDate: Date,
      assessmentStartDate: Date,
      testsRemaining: {
        testDescription: { value: string }[]
      }
    }
  },
  trainingModules: {
    intervention: ITrainingModule[]
  }
}