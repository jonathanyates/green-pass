import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {Observable} from "rxjs";
import {SelectUsers} from "../../../store/users/user.selectors";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {SelectUsersAction} from "../../../store/users/user.actions";
import {SelectError} from "../../../store/error/error.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";

@Injectable()
export class UserListResolve implements Resolve<IUser[]|boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUser[]|boolean> {

    let companyId = route.params['id'];
    if (!companyId) return Observable.of(false);

    this.store.dispatch(new SelectCompanyAction(companyId));
    this.store.dispatch(new SelectUsersAction(companyId));

    return this.store.let(SelectUsers())
      .combineLatest(this.store.let(SelectError()), // to get any errors
        (users, error) => ({ users:users, error:error }))
      .filter(result => result.error != null || result.users != null)
      .map(result => result.error ? false : result.users)
      .take(1);
  }
}
