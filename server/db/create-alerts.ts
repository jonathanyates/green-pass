import "reflect-metadata";
import {TYPES} from "../src/container/container.types";
import container from "../src/container/container.config";
import {IDbContext} from "../src/db/db.context.interface";
import {IAlertQueryService} from "../src/alerts/alert-query.service";

const alertQueryService: IAlertQueryService = container.get<IAlertQueryService>(TYPES.services.AlertQueryService);
const dbContext: IDbContext = container.get<IDbContext>(TYPES.db.DbContext);

console.log('Creating alerts...');

alertQueryService.updateAllCompanies()
.then(alertTypes => {
  console.log('Alerts created.');
  dbContext.disconnect();
  return process.exit(0);
})
.catch(error => {
  console.log(error);
  dbContext.disconnect();
  return process.exit(1);
});
