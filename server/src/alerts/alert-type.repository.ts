import { IQueryableRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import mongoose from 'mongoose';
import { IAlertType } from '../../../shared/interfaces/alert.interface';
import { IAlertTypeModel } from './alert-type.schema';
import { IMessageService } from '../messaging/message.service';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class AlertTypeRepository implements IQueryableRepository<IAlertTypeModel> {
  constructor(
    @inject(TYPES.db.DbContext) private dbContext: IDbContext,
    @inject(TYPES.services.MessageService) private messageService: IMessageService
  ) {}

  getAll(): Promise<IAlertTypeModel[]> {
    return this.queryAll().exec();
  }

  get(id: string): Promise<IAlertTypeModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid AlertType id.', codes.BAD_REQUEST));
    }

    return this.query(id).exec();
  }

  queryAll(): mongoose.Query<IAlertTypeModel[], mongoose.Document> {
    const result = this.dbContext.alertType.find().populate('reportType');
    return result;
  }

  query(id: string): mongoose.Query<IAlertTypeModel, mongoose.Document> {
    const result = this.dbContext.alertType.findById(id).populate('reportType');
    return result;
  }

  find(criteria: any): Promise<IAlertTypeModel[]> {
    return this.dbContext.alertType.find(criteria).populate('reportType').exec();
  }

  findOne(criteria: any): Promise<IAlertTypeModel> {
    return this.dbContext.alertType.findOne(criteria).populate('reportType').exec();
  }

  async add(AlertType: IAlertTypeModel): Promise<IAlertTypeModel> {
    const alertTypeModel = new this.dbContext.alertType(AlertType);
    const populatedModel = await alertTypeModel.populate('reportType');
    return populatedModel.save();
  }

  update<T extends IAlertType>(alertType: IAlertType): Promise<IAlertTypeModel> {
    return this.dbContext.alertType
      .findByIdAndUpdate(alertType._id, alertType, { new: true })
      .populate('reportType')
      .exec();
  }

  remove<T extends IAlertType>(alertTypeId: string): Promise<IAlertTypeModel> {
    return this.dbContext.alertType.findByIdAndRemove(alertTypeId).exec();
  }
}
