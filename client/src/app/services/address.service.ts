import {Injectable} from '@angular/core';
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {apiPath} from "../shared/constants";
import {IAddressList} from "../../../../shared/interfaces/address.interface";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AddressService extends HttpBaseService {

  private addressUrl = apiPath + '/address/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  search(postcode: string, house: string = null): Observable<IAddressList> {

    this.authorize();
    let url = this.addressUrl + postcode.replace(/\s/g,'').toUpperCase();
    if (house) {
      url += '/' + house;
    }

    return this.httpClient.get<IAddressList>(url, this.getOptions())
      .map(result => this.handleResult(result))
      .catch(this.handleError);
  }

}
