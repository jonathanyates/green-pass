import logger from 'winston';
import container from '../container/container.config';
import { IComplianceService } from '../compliance/compliance.service';
import { TYPES } from '../container/container.types';
import { IMessageService } from '../messaging/message.service';
import { queueNames } from '../messaging/messages.constants';
import schedule = require('node-schedule');
import { IAlertQueryService } from '../alerts/alert-query.service';
import { configureLogger } from './logger';

configureLogger('compliance', 'GreenPass-Compliance-Update-Service');

logger.info('Starting compliance update service...');

const messageService = container.get<IMessageService>(TYPES.services.MessageService);
const complianceService = container.get<IComplianceService>(TYPES.services.ComplianceService);
const alertQueryService = container.get<IAlertQueryService>(TYPES.services.AlertQueryService);

let updating = false;

messageService.receive<string>(queueNames.companyComplianceUpdate).subscribe({
  next: (companyId: string) => {
    if (!companyId) {
      return;
    }

    logger.info(`Updating Compliance for company id ${companyId}`);

    if (!updating) {
      doComplianceAndAlertUpdates(companyId);
    } else {
      wait(companyId);
    }
  },
  error: (error) => {
    logger.error(`Compliance Update Service message queue error. \nError: ${error.message}`);
    throw error;
  },
});

// ensure we do not try to update compliance concurrently
const wait = (companyId) => {
  setTimeout(() => {
    if (!updating) {
      doComplianceAndAlertUpdates(companyId);
    } else {
      wait(companyId);
    }
  }, 100);
};

const doComplianceAndAlertUpdates = (companyId) => {
  updating = true;
  complianceService
    .updateCompanyCompliance(companyId)
    .then((compliance) => {
      if (compliance) {
        logger.info(`Company compliance updated. ${compliance.compliant}% compliant.`);
        logger.info(`Updating Alerts for company id ${companyId}`);

        alertQueryService
          .updateAllAlerts(companyId)
          .then((alerts) => {
            updating = false;
            if (alerts) {
              logger.info(`Company Alerts updated successfully.`);
            }
          })
          .catch((error) => {
            updating = false;
            logger.error(`Failed to update company alerts for company ${companyId}. \nError: ${error.message}`);
          });
      }
    })
    .catch((error) => {
      updating = false;
      logger.error(`Failed to update company compliance for company ${companyId}. \nError: ${error.message}`);
    });
};

const cron = process.env.CRON ? process.env.CRON : '0 4 * * *'; // default run at 4am

logger.info('Scheduling Compliance updates to run at Cron interval ' + cron);

schedule.scheduleJob(cron, () => {
  logger.info('Updating Company Compliance for all companies...');

  complianceService
    .update()
    .then((compliances) => {
      logger.info('Successfully updated company compliance for all companies at ' + new Date().toString());
      logger.info(`Updating Alerts for for all companies.`);

      alertQueryService
        .updateAllCompanies()
        .then((alerts) => {
          logger.info('Successfully updated alerts compliance for all companies at ' + new Date().toString());
        })
        .catch((error) => {
          logger.error('Error updating company alerts at ' + new Date().toString());
          logger.error(error.message);
        });
    })
    .catch((error) => {
      logger.error('Error updating company compliance at ' + new Date().toString());
      logger.error(error.message);
    });
});

logger.info('Compliance Update Service is running.');

/* cron reference
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */
