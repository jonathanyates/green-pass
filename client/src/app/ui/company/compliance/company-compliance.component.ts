import {Component, OnInit} from '@angular/core';
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ICompanyDriverCompliance, ICompliance} from "../../../../../../shared/interfaces/compliance.interface";
import {BaseComponent} from "../../shared/components/base/base.component";
import {SelectSelectedCompliance} from "../../../store/compliance/compliance.selectors";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {IComplianceCardModel} from "../../shared/components/compliance-card/compliance-card.component";
import {Observable} from "rxjs/Observable";
import {SelectCompany} from "../../../store/company/company.selectors";

@Component({
  selector: 'gp-drivers-compliance',
  template: `
    <gp-compliance-cards [model]="model" (back)="back()"></gp-compliance-cards>
  `
})

export class CompanyComplianceComponent extends BaseComponent implements OnInit {

  model: IComplianceCardModel[];

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private router: Router) {
    super();
  }

  ngOnInit() {
    Observable.combineLatest(
      this.store.let(SelectSelectedCompliance())
        .filter(x => x != null),
      this.store.let(SelectCompany())
    )
      .subscribe(results => {
        let compliance = results[0];
        let company = results[1];

        this.model = [
          {id: 'compliant', header: 'Company compliance', chartText: 'Compliant', percentage: compliance.compliant},
          {id: 'drivers', header: 'Company drivers', chartText: 'Compliant', percentage: compliance.driver.compliant},
          {id: 'vehicles', header: 'Company vehicles', chartText: 'Compliant', percentage: compliance.vehicle.compliant}
        ];

        if (compliance.company.insurance != null) {
          this.model.push({id: 'insurance', header: 'Fleet insurance', chartText: 'Compliant',
            percentage: compliance.company.insurance ? 100 : 0
          })
        }
      })
  }

  back() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
