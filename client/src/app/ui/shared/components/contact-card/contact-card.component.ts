import {Component, OnInit, Input} from '@angular/core';
import {IContact} from "../../../../../../../shared/interfaces/driver.interface";

@Component({
  selector: 'gp-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.css'],
  host: {'class': 'card'}
})
export class ContactCardComponent implements OnInit {

  @Input() title: string;
  @Input() contact: IContact;

  constructor() { }

  ngOnInit() {
  }

}
