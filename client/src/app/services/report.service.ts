import {Injectable} from '@angular/core';
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {apiPath} from "../shared/constants";
import {Observable} from "rxjs";
import {IReportSchedule, IReportType} from "../../../../shared/interfaces/report.interface";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ReportService extends HttpBaseService {

  private reportTypesUrl = apiPath + '/reporttypes';
  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getReportType(id: string): Observable<IReportType> {

    this.authorize();

    return this.httpClient.get<IReportType>(this.reportTypesUrl, this.getOptions())
      .map(res => this.handleResult<IReportType>(res))
      .catch(this.handleError);
  }

  getReportTypes(): Observable<IReportType[]> {

    this.authorize();

    return this.httpClient.get<IReportType[]>(this.reportTypesUrl, this.getOptions())
      .map(res => this.handleResult<IReportType[]>(res))
      .catch(this.handleError);
  }

  saveReportType(reportType: IReportType): Observable<IReportType> {

    this.authorize();

    if (reportType._id) {
      return this.httpClient.put<IReportType>(this.reportTypesUrl + '/' + reportType._id, reportType, this.getOptions())
        .map(res => this.handleResult<IReportType>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IReportType>(this.reportTypesUrl, reportType, this.getOptions())
        .map(res => this.handleResult<IReportType>(res))
        .catch(this.handleError);
    }
  }

  getReport(companyId: string, reportTypeId: string): Observable<any> {

    this.authorize();

    return this.httpClient.get(`${this.companiesUrl}${companyId}/reports/${reportTypeId}`, this.getOptions())
      .map(res => this.handleResult<any>(res))
      .catch(this.handleError);
  }

  getReportSchedules(companyId: string): Observable<IReportSchedule[]> {
    this.authorize();

    return this.httpClient.get<IReportSchedule[]>(`${this.companiesUrl}${companyId}/schedules`, this.getOptions())
      .map(res => this.handleResult<IReportSchedule[]>(res))
      .catch(this.handleError);
  }

  getReportSchedule(companyId: string, reportTypeId: string): Observable<IReportSchedule> {
    this.authorize();

    return this.httpClient.get<IReportSchedule>(`${this.companiesUrl}${companyId}/schedules/${reportTypeId}`, this.getOptions())
      .map(res => this.handleResult<IReportSchedule>(res))
      .catch(this.handleError);
  }

  saveReportSchedule(companyId: string, reportSchedule: IReportSchedule): Observable<IReportSchedule> {

    this.authorize();

    if (reportSchedule._id) {
      return this.httpClient.put<IReportSchedule>(`${this.companiesUrl}${companyId}/schedules`, reportSchedule, this.getOptions())
        .map(res => this.handleResult<IReportSchedule>(res))
        .catch(this.handleError);
    } else {
      let url = `${this.companiesUrl}${companyId}/schedules`;
      return this.httpClient.post<IReportSchedule>(url, reportSchedule, this.getOptions())
        .map(res => this.handleResult<IReportSchedule>(res))
        .catch(this.handleError);
    }
  }

  removeReportSchedule(companyId: string, reportScheduleId: string): Observable<IReportSchedule> {
    this.authorize();

    return this.httpClient.delete<IReportSchedule>(`${this.companiesUrl}${companyId}/schedules/${reportScheduleId}`, this.getOptions())
      .map(res => this.handleResult<IReportSchedule>(res))
      .catch(this.handleError);
  }

}
