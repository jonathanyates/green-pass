import { Injectable } from '@angular/core';
import {Effect, Actions} from "@ngrx/effects";
import * as actions from "./page.actions";
import {Observable} from "rxjs";
import {SetErrorAction} from "../error/error.actions";
import {PageService} from "../../services/page.service";
import {Router} from "@angular/router";

@Injectable()
export class PageEffects {

  constructor(
    private actions$:Actions,
    private router: Router,
    private pageService:PageService) { }

  @Effect() loadPage$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .distinctUntilChanged()
    .switchMap((action: actions.LoadPageAction) => this.pageService.getHtml(action.payload)
      .map(page =>  new actions.LoadPageActionSuccess(page))
      .catch(error => {
        console.log(error.message);
        this.router.navigate(['home']);
        return Observable.of(new SetErrorAction(error));
      })
    );
}
