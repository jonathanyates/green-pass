import {IReportType} from "../../../../../shared/interfaces/report.interface";

export interface ReportState {
  reportTypes: IReportType[];
}
