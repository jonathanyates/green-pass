export interface ErrorState {
  message: string;
  code: number;
}
