import { Component, Input, OnInit } from '@angular/core';
import { IListGroupItem } from '../../shared/components/dynamic-list-group/dynamic-list-group';
import { ActivatedRoute, Router } from '@angular/router';
import { Paths } from '../../../shared/constants';
import { ICompany } from '../../../../../../shared/interfaces/company.interface';

@Component({
  selector: 'gp-company-details',
  template: `<gp-dynamic-list-group [items]="items" (edit)="edit()"></gp-dynamic-list-group>`,
})
export class CompanyDetailsComponent implements OnInit {
  items: IListGroupItem[];
  private _company: ICompany;

  @Input() set company(company) {
    this._company = company;
    if (company) {
      this.items = [
        { title: 'Name', value: company.name },
        { title: 'Address', value: company.address },
        { title: 'Core business', value: company.coreBusiness },
        { title: 'No of employees', value: company.noOfEmployees },
        { title: 'Controlling mind', value: `${company.controllingMind.name} (${company.controllingMind.position})` },
        { title: 'Phone', value: company.phone },
        { title: 'Mobile', value: company.mobile },
        { title: 'Email', value: company.email },
        // {title: "Subscription", value: company.subscription ? company.subscription.name : 'Not Set'}
      ];
    }
  }
  get company() {
    return this._company;
  }

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {}

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
