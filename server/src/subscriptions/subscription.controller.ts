import {injectable, inject} from "inversify";
import {TYPES} from "../container/container.types";
import {Request, Response, NextFunction} from "express-serve-static-core";
import {ISubscriptionService} from "./subscription.service";
import codes from "../shared/error.codes";

export interface ISubscriptionController {
  getAll(req:Request, res:Response, next: NextFunction);
}

@injectable()
export class SubscriptionController implements ISubscriptionController {

  constructor(
    @inject(TYPES.services.SubscriptionService) private subscriptionService: ISubscriptionService
  ) {}

  getAll = (req: Request, res: Response, next: NextFunction) => {

    this.subscriptionService.getAll()
      .then(subscriptions => res.status(codes.OK).json(subscriptions))
      .catch(err => next(err));
  }
}