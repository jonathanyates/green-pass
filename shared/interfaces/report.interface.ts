import {IUser} from "./user.interface";

export const ReportCategory = {
  Driver: 'Driver',
  Vehicle: 'Vehicle',
  System: 'System'
};

export interface IReportType {
  _id?: any,
  name: string,
  query: string,
  category: string
}

export interface IReportQueryResult {
  reportType: IReportType,
  result: any
}

export interface IReportSchedule {
  _id?: any,
  _companyId?: any,
  reportType: IReportType,
  cron: string,
  recipients: IUser[]
}

/*
 *     *     *   *    *        command to be executed
 -     -     -   -    -
 |     |     |   |    |
 |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
 |     |     |   +------- month (1 - 12)
 |     |     +--------- day of        month (1 - 31)
 |     +----------- hour (0 - 23)
 +------------- min (0 - 59)
 */