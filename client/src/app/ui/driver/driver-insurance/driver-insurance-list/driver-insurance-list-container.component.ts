import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {SelectDriverUserInfo} from "../../../../store/drivers/driver.selectors";
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {IUser} from "../../../../../../../shared/interfaces/user.interface";
import {IInsurance} from "../../../../../../../shared/interfaces/common.interface";
import {Paths} from "../../../../shared/constants";
import {LoadDriverAction} from '../../../../store/drivers/driver.actions';
import {HandleErrorAction} from '../../../../store/error/error.actions';
import {BusyAction} from '../../../../store/progress/progress.actions';
import {DriverService} from '../../../../services/driver.service';
import {IDriver} from '../../../../../../../shared/interfaces/driver.interface';

@Component({
  selector: 'gp-driver-insurance-list-container',
  template: `
    <gp-card [header]="'Business Insurance'">
      <gp-driver-insurance-list [insuranceHistory]="insuranceHistory" [lightHeader]="true" 
                                [user]="user" [allowInput]="allowInput"
                                (delete)="deleteInsurance($event)"
                                (add)="addInsurance()" (select)="selectInsurance($event)"></gp-driver-insurance-list>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>    
  `
})

export class DriverInsuranceListContainerComponent implements OnInit {

  insuranceHistory: IInsurance[];
  user: IUser;
  driver: IDriver;
  allowInput: boolean = false;
  protected subscriptions: Subscription[] = [];

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute,
    private driverService: DriverService) { }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        this.insuranceHistory = info.driver ? info.driver.insuranceHistory : null;
        this.user = info.user;
        this.driver = info.driver;
        this.allowInput = info.allowInput;
      }));
  }

  addInsurance() {
    return this.router.navigate(['./', Paths.New], { relativeTo: this.route });
  }

  selectInsurance(insurance: IInsurance) {
    return this.router.navigate(['./', insurance._id], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  deleteInsurance(insurance: IInsurance) {
    if (insurance) {
      this.store.dispatch(new BusyAction());
      this.driverService.deleteDriverInsurance(this.driver, insurance)
        .subscribe(driver => {
          this.store.dispatch(new LoadDriverAction(this.driver._companyId, this.driver._id));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
  }
}
