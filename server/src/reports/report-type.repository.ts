import mongoose from 'mongoose';
import { IQueryableRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { Query } from 'mongoose';
import { IReportTypeModel } from './report-type.schema';
import { IReportType } from '../../../shared/interfaces/report.interface';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class ReportTypeRepository implements IQueryableRepository<IReportTypeModel> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  getAll(): Promise<IReportTypeModel[]> {
    return this.dbContext.reportType.find().exec();
  }

  get(id: string): Promise<IReportTypeModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid Report id.', codes.BAD_REQUEST));
    }

    return this.dbContext.reportType.findById(id).exec();
  }

  queryAll(): Query<IReportTypeModel[], mongoose.Document> {
    const result = this.dbContext.reportType.find();
    return result;
  }

  query(id: string): Query<IReportTypeModel, mongoose.Document> {
    const result = this.dbContext.reportType.findById(id);
    return result;
  }

  find(criteria: any): Promise<IReportTypeModel[]> {
    return this.dbContext.reportType.find(criteria).exec();
  }

  findOne(criteria: any): Promise<IReportTypeModel> {
    return this.dbContext.reportType.findOne(criteria).exec();
  }

  add<T extends IReportType>(Report: IReportTypeModel): Promise<IReportTypeModel> {
    const ReportModel = new this.dbContext.reportType(Report);
    return ReportModel.save();
  }

  update<T extends IReportType>(Report: IReportType): Promise<IReportTypeModel> {
    return this.dbContext.reportType.findByIdAndUpdate(Report._id, Report, { new: true }).exec();
  }

  remove<T extends IReportType>(reportId: string): Promise<IReportTypeModel> {
    return this.dbContext.reportType.findByIdAndRemove(reportId).exec();
  }
}
