import mongoose from 'mongoose';
import { IReportType } from '../../../shared/interfaces/report.interface';

export const ReportTypeSchema = new mongoose.Schema({
  name: { type: String, required: true },
  query: { type: String, required: true },
  category: { type: String, required: true },
});

export interface IReportTypeModel extends mongoose.Document, IReportType {
  _id: any;
}
