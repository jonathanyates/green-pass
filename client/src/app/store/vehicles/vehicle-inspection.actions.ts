import {type} from "../../shared/util";
import {Action} from "@ngrx/store";
import {IVehicle, IVehicleInspection} from "../../../../../shared/interfaces/vehicle.interface";

export const ActionTypes = {
  SELECT_VEHICLE_INSPECTION: type('[Vehicle Inspection] Select'),
  SELECT_VEHICLE_INSPECTION_SUCCESS : type('[Vehicle Inspection] Select Success'),
  SAVE_VEHICLE_INSPECTION:   type('[Vehicle Inspection] Save'),
  SAVE_VEHICLE_INSPECTION_SUCCESS :   type('[Vehicle Inspection] Save Success'),
  CLEAR_VEHICLE_INSPECTION:  type('[Vehicle Inspection] Clear')
};

/**
 * Vehicle Inspection Actions
 */

export class SelectVehicleInspectionAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_INSPECTION;
  payload: { vehicleId: string, inspectionId: string };
  constructor(vehicleId: string, inspectionId: string) {
    this.payload = { vehicleId: vehicleId, inspectionId: inspectionId }
  }
}

export class SelectVehicleInspectionSuccessAction implements Action {
  type = ActionTypes.SELECT_VEHICLE_INSPECTION_SUCCESS;
  constructor(public payload: IVehicleInspection) { } // inspection id
}

export class SaveVehicleInspectionAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_INSPECTION;
  payload: { vehicle: IVehicle, inspection: IVehicleInspection };
  constructor(vehicle: IVehicle, inspection: IVehicleInspection) {
    this.payload = { vehicle: vehicle, inspection:inspection }
  }
}

export class SaveVehicleInspectionSuccessAction implements Action {
  type = ActionTypes.SAVE_VEHICLE_INSPECTION_SUCCESS;
  payload: { vehicle: IVehicle, inspection: IVehicleInspection };
  constructor(vehicle: IVehicle, inspection: IVehicleInspection) {
    this.payload = { vehicle: vehicle, inspection:inspection }
  }
}

export class ClearVehicleInspectionAction implements Action {
  type = ActionTypes.CLEAR_VEHICLE_INSPECTION;
  payload = null;
  constructor() { } // inspection id
}

export type Actions
  = SelectVehicleInspectionSuccessAction
  | SaveVehicleInspectionSuccessAction
  | ClearVehicleInspectionAction;
