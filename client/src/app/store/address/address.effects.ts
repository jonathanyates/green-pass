import * as actions from "./address.actions";
import {Observable} from "rxjs";
import {Injectable} from '@angular/core';
import {Effect, Actions} from "@ngrx/effects";
import {AddressService} from "../../services/address.service";
import {SetErrorAction} from "../error/error.actions";

declare let $: any;

@Injectable()
export class AddressEffects {

  constructor(private actions$: Actions,
              private addressService: AddressService) {
  }

  @Effect() loadAddressList$ = this.actions$
    .ofType(actions.ActionTypes.LOAD)
    .distinctUntilChanged()
    .switchMap((action: actions.LoadAddressListAction) => this.addressService.search(action.payload)
      .do(addressList => $('#searchModal').modal('show'))
      .map(addressList => new actions.LoadAddressListActionSuccess(addressList))
      .catch(error => {
        console.log(error.message);
        return Observable.of(new SetErrorAction(error));
      })
    );

  @Effect({ dispatch: false }) selectAddress$ = this.actions$
    .ofType(actions.ActionTypes.SELECT)
    .distinctUntilChanged()
    .do(addressList => $('#searchModal').modal('hide'))
}
