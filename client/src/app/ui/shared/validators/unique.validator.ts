import {Directive, Input} from "@angular/core";
import {NG_VALIDATORS, Validator, AbstractControl} from "@angular/forms";

@Directive({
  selector: '[gp-unique]',
  providers: [{provide: NG_VALIDATORS, useExisting: DuplicateValidatorDirective, multi: true}]
})
export class DuplicateValidatorDirective implements Validator {

  @Input('gp-unique') existingValues: Array<string>;

  validate(control: AbstractControl): {[key: string]: any} {

    let value = control.value;

    return this.existingValues.some(x => x === value)
      ? {'duplicate': {value}}
      : null;
  }

}
