import mongoose from 'mongoose';
import { IQueryableRepository } from '../db/db.repository';
import { injectable, inject } from 'inversify';
import { IDbContext } from '../db/db.context.interface';
import { TYPES } from '../container/container.types';
import { Query } from 'mongoose';
import { IAuditModel } from './audit.schema';
import { IAudit } from './audit.interface';
import { ServerError } from '../shared/errors';
import codes from '../shared/error.codes';

@injectable()
export class AuditRepository implements IQueryableRepository<IAuditModel> {
  constructor(@inject(TYPES.db.DbContext) private dbContext: IDbContext) {}

  getAll(): Promise<IAuditModel[]> {
    return this.dbContext.audit.find().exec();
  }

  get(id: string): Promise<IAuditModel> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return Promise.reject(ServerError('Invalid Audit id.', codes.BAD_REQUEST));
    }

    return this.dbContext.audit.findById(id).exec();
  }

  queryAll(): Query<IAuditModel[], mongoose.Document> {
    const result = this.dbContext.audit.find();
    return result;
  }

  query(id: string): Query<IAuditModel, mongoose.Document> {
    const result = this.dbContext.audit.findById(id);
    return result;
  }

  find(criteria: any): Promise<IAuditModel[]> {
    return this.dbContext.audit.find(criteria).exec();
  }

  findOne(criteria: any): Promise<IAuditModel> {
    return this.dbContext.audit.findOne(criteria).exec();
  }

  add<T extends IAudit>(Audit: IAuditModel): Promise<IAuditModel> {
    const AuditModel = new this.dbContext.audit(Audit);
    return AuditModel.save();
  }

  update<T extends IAudit>(Audit: IAudit): Promise<IAuditModel> {
    return this.dbContext.audit.findByIdAndUpdate(Audit._id, Audit, { new: true }).exec();
  }

  remove<T extends IAudit>(auditId: string): Promise<IAuditModel> {
    return this.dbContext.audit.findByIdAndRemove(auditId).exec();
  }
}
