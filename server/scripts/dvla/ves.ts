import 'reflect-metadata';
import { DvlaService } from '../../src/dvla/service/dvla.service';
import { vehicles } from '../../db/vehicles';

const dvlaService = new DvlaService();

Promise.all(
  vehicles.map((vehicle) => {
    vehicle.reg = vehicle.reg.toUpperCase();

    // console.log('Attempting to call vehicle enquiry for:');
    // console.log(`Reg: ${vehicle.reg}, Make: ${vehicle.make}`);
    return dvlaService.vehicleEnquiry(vehicle.reg).catch((error) => {
      console.log('Failed vehicle enquiry for:');
      console.log(`Error: ${error.message}`);
      console.log('');
      return {
        registrationNumber: vehicle.reg,
        error: error.message,
      };
    });
  })
)
  .then((result) => {
    //console.log(result);
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
