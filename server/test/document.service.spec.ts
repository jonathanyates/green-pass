describe('Document Service', function () {
  describe('getDocuments', function () {
    it('gets Documents', (done) => {
      done();
    });
  });

  describe('requestEmbeddedUrl', function () {
    it('requests Recipient View', (done) => {
      done();
    });
  });

  describe('sendDocuments', function () {
    it('sends Driver Documents', (done) => {
      done();
    });
  });

  describe('getDocuments', function () {
    it('gets Documents', (done) => {
      done();
    });
  });

  describe('sendDocumentsForAllCompanies', function () {
    it('sends Documents For All Companies', (done) => {
      done();
    });
  });

  describe('sendDocumentsForCompany', function () {
    it('sends Documents For Company', (done) => {
      done();
    });
  });

  describe('getCompanyTemplates', function () {
    it('gets Templates', (done) => {
      done();
    });
  });

  describe('saveCompanyTemplates', function () {
    it('saves Templates', (done) => {
      done();
    });
  });
});
