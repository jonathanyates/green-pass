export interface IPaginationInstance {
  id?: string;
  itemsPerPage: number;
  currentPage: number;
  totalItems?: number;
}
