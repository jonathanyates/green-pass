import {ContainerModule, interfaces} from "inversify";
import {IRepository} from "../db/db.repository";
import {TYPES} from "../container/container.types";
import {ISubscriptionService, SubscriptionService} from "./subscription.service";
import {SubscriptionRepository} from "./subscription.repository";
import {ISubscriptionModel} from "./subscription.schema";
import {ISubscription} from "../../../shared/interfaces/subscription.interface";
import {IApiRouter} from "../interfaces/api-router.interface";
import {SubscriptionRouter} from "./subscription.router";
import {ISubscriptionController, SubscriptionController} from "./subscription.controller";

export const subscriptionModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.SubscriptionRouter).to(SubscriptionRouter).inSingletonScope();
  bind<ISubscriptionController>(TYPES.controllers.SubscriptionController).to(SubscriptionController).inSingletonScope();
  bind<ISubscriptionService>(TYPES.services.SubscriptionService).to(SubscriptionService).inSingletonScope();
  bind<IRepository<ISubscription|ISubscriptionModel>>(TYPES.repositories.SubscriptionRepository).to(SubscriptionRepository).inSingletonScope();
});
