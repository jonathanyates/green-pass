import express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../container/container.types';
import { IApiRouter } from '../interfaces/api-router.interface';
import { IAuthorizationMiddleware } from '../security/authorization/authorization.middleware.interface';
import { IReportTypeController } from './report-type.controller';

@injectable()
export class ReportTypeRouter implements IApiRouter {
  routes = express.Router({ mergeParams: true });

  constructor(
    @inject(TYPES.controllers.ReportTypeController) private reportTypeController: IReportTypeController,
    @inject(TYPES.security.AuthorizationMiddleware) private authorizationMiddleware: IAuthorizationMiddleware
  ) {
    this.configure();
  }

  configure() {
    this.routes.use(this.authorizationMiddleware.user.middleware());

    this.routes
      .get(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportTypeController.getAll
      )
      .get(
        '/:reportTypeId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.companyAdmin),
        this.reportTypeController.get
      )
      .post(
        '/',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.reportTypeController.add
      )
      .put(
        '/:reportTypeId',
        this.authorizationMiddleware.user.can(this.authorizationMiddleware.roles.any),
        this.reportTypeController.update
      );
  }
}
