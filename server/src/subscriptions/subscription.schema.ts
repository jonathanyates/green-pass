import mongoose from 'mongoose';
import { ISubscription } from '../../../shared/interfaces/subscription.interface';

export const SubscriptionResourcesSchema = new mongoose.Schema({
  mandates: { type: Boolean, required: true },
  licenceCheck: { type: Boolean, required: true },
  onLineAssessments: { type: Boolean, required: true },
  onRoadTraining: { type: Boolean, required: true },
  alerts: { type: Number, required: true },
  presentations: { type: Boolean, required: true },
  workshops: { type: Boolean, required: true },
});

export const SubscriptionSchema = new mongoose.Schema({
  name: { type: String, required: true },
  resources: { type: SubscriptionResourcesSchema, required: true },
});

export interface ISubscriptionModel extends mongoose.Document, ISubscription {
  _id: any;
}
