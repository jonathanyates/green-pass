import { IVehicle } from '../../../../shared/interfaces/vehicle.interface';
import { IDvsaHgvTestEnquiry } from '../interfaces/dvsa-hgv-test-enquiry.interface';

export const mapHgvTestEnquiryToVehicle = (hgvTestEnquiry: IDvsaHgvTestEnquiry, vehicle: IVehicle) => {
  if (!hgvTestEnquiry) {
    return vehicle;
  }

  vehicle.model = hgvTestEnquiry.model;
  vehicle.dateOfFirstRegistration = hgvTestEnquiry.registrationDate ? new Date(hgvTestEnquiry.registrationDate) : null;
  vehicle.manufactureDate = hgvTestEnquiry.manufactureDate ? new Date(hgvTestEnquiry.manufactureDate) : null;
  vehicle.motDueDate = hgvTestEnquiry.annualTestExpiryDate ? new Date(hgvTestEnquiry.annualTestExpiryDate) : null;

  vehicle.vehicleType = hgvTestEnquiry.vehicleType;
  vehicle.class = hgvTestEnquiry.vehicleClass;

  if (hgvTestEnquiry.annualTests && hgvTestEnquiry.annualTests.length > 0) {
    vehicle.annualTestHistory = hgvTestEnquiry.annualTests.map((test) => {
      return {
        testDate: test.testDate && new Date(test.testDate),
        testType: test.testType,
        testResult: test.testResult,
        testCertificateNumber: test.testCertificateNumber,
        expiryDate: test.expiryDate && new Date(test.expiryDate),
        numberOfDefectsAtTest: test.numberOfDefectsAtTest,
        numberOfAdvisoryDefectsAtTest: test.numberOfAdvisoryDefectsAtTest,
        defects: test.defects,
        location: test.location,
      };
    });

    const latestMotTest = vehicle.annualTestHistory.find((item) => item.hasOwnProperty('expiryDate'));
    vehicle.motDueDate = latestMotTest && latestMotTest.expiryDate;
  }

  return vehicle;
};
