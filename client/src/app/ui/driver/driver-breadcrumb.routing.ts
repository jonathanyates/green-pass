import {Injectable} from "@angular/core";
import {BreadcrumbService} from "../../services/breadcrumb.service";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {User} from "../user/user.model";
import {DriversState} from "../../store/drivers/drivers.state";
import {SelectDriversState} from "../../store/drivers/driver.selectors";
import {IDocument} from "../../../../../shared/interfaces/document.interface";
import {SelectDocument} from "../../store/documents/document.selectors";
import {FormatDate} from "../../../../../shared/utils/helpers";
import {VehiclesState} from "../../store/vehicles/vehicles.state";
import {SelectVehiclesState} from "../../store/vehicles/vehicle.selectors";

@Injectable()
export class DriverBreadcrumbRouting {

  private driversState: DriversState;
  private vehicleState: VehiclesState;
  private document: IDocument;

  constructor(private breadcrumbService: BreadcrumbService,
              private store: Store<AppState>) {
  }

  excluded = ['details', 'documents', 'licence', 'insurance', 'faw', 'references', 'vehicles',
    'services', 'inspections', 'checks', 'new', 'add'];

  configure() {
    this.store.let(SelectDriversState())
      .subscribe(state => {
        this.driversState = state
      });

    this.store.let(SelectVehiclesState())
      .subscribe(state => this.vehicleState = state);

    this.store.let(SelectDocument())
      .subscribe(document => {
        this.document = document;
      });

    this.breadcrumbService.addFriendlyNameForRoute('/driver/licenceCheck', 'Licence Check');
    this.breadcrumbService.addFriendlyNameForRoute('/driver/licence', 'Driving Licence');
    this.breadcrumbService.addFriendlyNameForRoute('/driver/vehicles', 'Vehicles');
    this.breadcrumbService.addFriendlyNameForRoute('/driver/roadMarque', 'Roadmarque Assessments');
    this.breadcrumbService.addFriendlyNameForRoute('/driver/nextofkin', 'Next Of Kin');

    this.breadcrumbService.addCallbackForRouteRegex('^/driver$', this.getDriver);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/documents/.*$', this.getDocument);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/insurance/.*$', this.getDriverInsurance);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/faw/.*$', this.getDriverFaw);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/references/.*$', this.getDriverReference);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/vehicles/.*$', this.getVehicle);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/vehicles/.*/services/.*$', this.getVehicleService);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/vehicles/.*/inspections/.*$', this.getVehicleInspection);
    this.breadcrumbService.addCallbackForRouteRegex('^/driver/vehicles/.*/checks/.*$', this.getVehicleCheck);
  }

  getDocument = (id: string) => {
    if (!this.document || this.excluded.indexOf(id) > -1) {
      return id;
    }

    return this.document.name;
  };

  getDriverInsurance = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.driversState.selected) {
      let driverInsurance = this.driversState.selected.insuranceHistory.find(insurance => insurance._id === id);
      return driverInsurance ? FormatDate(driverInsurance.validTo) : '...';
    }
    return id;
  };

  getDriverFaw = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.driversState.selected) {
      let faw = this.driversState.selected.fawHistory.find(fawCert => fawCert._id === id);
      return faw ? FormatDate(faw.validTo) : '...';
    }
    return id;
  };

  getDriverReference = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.driversState.selected) {
      let reference = this.driversState.selected.references.find(reference => reference._id === id);
      return reference ? reference.forename + ' ' + reference.surname : '...';
    }
    return id;
  };

  getVehicle = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    let vehicle = this.vehicleState
      ? this.vehicleState.selected && this.vehicleState.selected._id === id
        ? this.vehicleState.selected
        : this.vehicleState.list
          ? this.vehicleState.list.find(vehicle => vehicle._id === id)
          : null
      : null;

    return vehicle ? vehicle.registrationNumber : '...';
  };

  getDriver = (id: string) => {
    if (!this.driversState || !this.driversState.selected || this.excluded.indexOf(id) > -1) {
      return id;
    }

    let driver = this.driversState.selected;
    return driver ? User.getFullName(driver._user) : 'driver';
  };

  getVehicleService = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleService = this.vehicleState.selected.serviceHistory.find(service => service._id === id);
      return vehicleService ? FormatDate(vehicleService.serviceDate) : '...';
    }
    return id;
  };

  getVehicleInspection = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleInspection = this.vehicleState.selected.inspectionHistory.find(inspection => inspection._id === id);
      return vehicleInspection ? FormatDate(vehicleInspection.inspectionDate) : '...';
    }
    return id;
  };

  getVehicleCheck = (id: string) => {
    if (this.excluded.indexOf(id) > -1) {
      return id;
    }

    if (this.vehicleState.selected) {
      let vehicleCheck = this.vehicleState.selected.checkHistory.find(check => check._id === id);
      return vehicleCheck ? FormatDate(vehicleCheck.checkDate) : '...';
    }
    return id;
  };
}
