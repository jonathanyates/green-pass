import '../../shared/extensions/date.extensions';
import { VehicleCompliance } from '../../shared/models/vehicle-compliance.model';
import { Vehicle } from '../../client/src/app/ui/vehicle/vehicle.model';
import { VehicleTypes } from '../../shared/interfaces/constants';
import { ICompanySettings } from '../../shared/interfaces/company-settings.interface';
import { defaultSettingsFactory } from '../../shared/interfaces/company-settings.factory';

describe('VehicleCompliance', () => {
    describe('construct', () => {
        it('expect vehicle to be none compliant', () => {
            const vehicle = new Vehicle();
            const settings: ICompanySettings = defaultSettingsFactory();

            const vehicleCompliance = new VehicleCompliance(vehicle, settings);
            expect(vehicleCompliance.compliant).toBeFalsy();
          });

        it('expect vehicle to have compliance insurance', () => {
            const vehicle = new Vehicle();
            vehicle.type = VehicleTypes.grey;
            const settings: ICompanySettings = defaultSettingsFactory();

            const vehicleCompliance = new VehicleCompliance(vehicle, settings);
            expect(vehicleCompliance.compliant).toBeFalsy();
          });
      });
  });
