import logger = require('winston');
import container from '../container/container.config';
import { IReportScheduler } from '../reports/report.scheduler';
import { TYPES } from '../container/container.types';
import { configureLogger } from './logger';
import { IDriverReminderScheduler } from '../drivers/driver-reminder.scheduler';

configureLogger('reports', 'GreenPass-Report-Scheduler-Service');

logger.info('Starting Report Scheduler...');

const reportScheduler = container.get<IReportScheduler>(TYPES.services.ReportScheduler);
reportScheduler.scheduleReports();
reportScheduler.listen();

logger.info('Report Scheduler running.');

logger.info('Starting Driver Reminder Scheduler...');

const driverReminderScheduler = container.get<IDriverReminderScheduler>(TYPES.services.DriverReminderScheduler);
driverReminderScheduler.scheduleAll();
driverReminderScheduler.listen();

logger.info('Report Driver Reminder running.');
