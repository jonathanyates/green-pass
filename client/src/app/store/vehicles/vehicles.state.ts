import {
  IVehicle, IVehicleCheck, IVehicleInspection, IVehicleInsurance,
  IVehicleService
} from "../../../../../shared/interfaces/vehicle.interface";

export interface VehiclesState {
  list: IVehicle[];
  total: number;
  page: number;
  selected?: IVehicle,
  selectedService?: IVehicleService,
  selectedInspection?: IVehicleInspection,
  selectedCheck?: IVehicleCheck,
  selectedTab?: string
}
