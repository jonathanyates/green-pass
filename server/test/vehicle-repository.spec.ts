import 'reflect-metadata';
import { TYPES } from '../src/container/container.types';
import container from '../src/container/container.config';
import mongoose from 'mongoose';
import { IVehicle } from '../../shared/interfaces/vehicle.interface';
import { IVehicleRepository } from '../src/vehicles/vehicle.repository';

describe.skip('Vehicle Repository', function () {
  describe.skip('get Vehicle', function () {
    this.timeout(20000);
    it('get vehicle with drivers', () => {
      const vehicleRepository = <IVehicleRepository>(
        container.get<IVehicleRepository>(TYPES.repositories.VehicleRepository)
      );

      return vehicleRepository.get('599de89b4dba0568ea7d3c85').then((vehicle: IVehicle) => {
        expect(vehicle).toBeDefined();
        expect(vehicle.drivers).toBeDefined();
        expect(vehicle.drivers?.length).toEqual(1);
        expect(vehicle.drivers![0].licenceNumber).toEqual('DICKI706226LH9HK');
      });
    });
  });

  describe.skip('findOne Vehicle', () => {
    this.timeout(20000);
    it('find one vehicle with drivers', async () => {
      const vehicleRepository = <IVehicleRepository>(
        container.get<IVehicleRepository>(TYPES.repositories.VehicleRepository)
      );

      const vehicle = await vehicleRepository.findOne({ _id: new mongoose.Types.ObjectId('599de89b4dba0568ea7d3c85') });
      expect(vehicle).toBeDefined();
      expect(vehicle.drivers).toBeDefined();
      expect(vehicle.drivers?.length).toEqual(1);
      expect(vehicle.drivers![0].licenceNumber).toEqual('DICKI706226LH9HK');
    });
  });

  describe.skip('find Vehicles', () => {
    this.timeout(20000);
    it('find drivers with drivers', async () => {
      const vehicleRepository = <IVehicleRepository>(
        container.get<IVehicleRepository>(TYPES.repositories.VehicleRepository)
      );

      const drivers = await vehicleRepository.find({ _id: new mongoose.Types.ObjectId('599de89b4dba0568ea7d3c85') });
      expect(drivers).toBeDefined();
      expect(drivers.length).toEqual(1);

      const vehicle = drivers[0];
      expect(vehicle).toBeDefined();
      expect(vehicle.drivers).toBeDefined();
      expect(vehicle.drivers?.length).toEqual(1);
      expect(vehicle.drivers![0].licenceNumber).toEqual('DICKI706226LH9HK');
    });
  });
});
