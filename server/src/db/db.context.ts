import mongoose from 'mongoose';
import logger = require('winston');
import Grid from 'gridfs-stream';
import serverConfig from '../server/server.config';
import { injectable } from 'inversify';
import { IDbContext } from './db.context.interface';
import { CompanySchema, ICompanyModel } from '../companies/company.schema';
import { DriverSchema, DriverVehicleMapSchema, IDriverModel, IDriverVehicleMapModel } from '../drivers/driver.schema';
import { UserSchema, IUserModel } from '../users/user.schema';
import { VehicleSchema, IVehicleModel } from '../vehicles/vehicle.schema';
import { AddressListSchema, IAddressModel } from '../address/address.schema';
import { ComplianceSchema, IComplianceModel } from '../compliance/compliance.schema';
import { AssessmentSchema, IAssessmentModel } from '../assessments/assessment.schema';
import { AuditSchema, IAuditModel } from '../audit/audit.schema';
import { AlertSchema, IAlertModel } from '../alerts/alert.schema';
import { AlertTypeSchema, IAlertTypeModel } from '../alerts/alert-type.schema';
import { ReportTypeSchema, IReportTypeModel } from '../reports/report-type.schema';
import { IReportScheduleModel, ReportScheduleSchema } from '../reports/report-schedule.schema';
import { ISubscriptionModel, SubscriptionSchema } from '../subscriptions/subscription.schema';
import { ITemplateModel, TemplateSchema } from '../templates/template.schema';
import { GridFSBucket } from 'mongodb';

mongoose.Promise = global.Promise;

@injectable()
export class DbContext implements IDbContext {
  constructor() {
    // delay connect to allow mongo instance to start
    // setTimeout(() => {
    //   this.connect();
    // }, 10000);
    this.connect();
  }

  gridFs: Grid.Grid;
  gridFSBucket: any;

  company: mongoose.Model<ICompanyModel> = mongoose.model<ICompanyModel>('Company', CompanySchema);
  user: mongoose.Model<IUserModel> = mongoose.model<IUserModel>('User', UserSchema);
  driver: mongoose.Model<IDriverModel> = mongoose.model<IDriverModel>('Driver', DriverSchema);
  vehicle: mongoose.Model<IVehicleModel> = mongoose.model<IVehicleModel>('Vehicle', VehicleSchema);
  driverVehicleMap: mongoose.Model<IDriverVehicleMapModel> = mongoose.model<IDriverVehicleMapModel>(
    'DriverVehicleMap',
    DriverVehicleMapSchema,
    'driverVehicleMap'
  );
  address: mongoose.Model<IAddressModel> = mongoose.model<IAddressModel>('Address', AddressListSchema);
  compliance: mongoose.Model<IComplianceModel> = mongoose.model<IComplianceModel>('Compliance', ComplianceSchema);
  assessment: mongoose.Model<IAssessmentModel> = mongoose.model<IAssessmentModel>('Assessment', AssessmentSchema);
  audit: mongoose.Model<IAuditModel> = mongoose.model<IAuditModel>('Audit', AuditSchema);

  template: mongoose.Model<ITemplateModel> = mongoose.model<ITemplateModel>('Template', TemplateSchema);

  alertType: mongoose.Model<IAlertTypeModel> = mongoose.model<IAlertTypeModel>(
    'AlertType',
    AlertTypeSchema,
    'alertTypes'
  );
  alert: mongoose.Model<IAlertModel> = mongoose.model<IAlertModel>('Alert', AlertSchema);

  reportType: mongoose.Model<IReportTypeModel> = mongoose.model<IReportTypeModel>(
    'ReportType',
    ReportTypeSchema,
    'reportTypes'
  );
  reportSchedule: mongoose.Model<IReportScheduleModel> = mongoose.model<IReportScheduleModel>(
    'ReportSchedule',
    ReportScheduleSchema,
    'reportSchedules'
  );

  subscription: mongoose.Model<ISubscriptionModel> = mongoose.model<ISubscriptionModel>(
    'Subscription',
    SubscriptionSchema
  );

  disconnect: () => void;

  async connect() {
    logger.info(`Connecting to database ${serverConfig.db} ...`);
    mongoose.set('strictQuery', false);

    mongoose.connection.once('open', () => {
      const gridFSBucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
        bucketName: 'uploads',
      });

      this.gridFSBucket = gridFSBucket;
      this.gridFs = Grid(mongoose.connection.db, mongoose.mongo);
      this.gridFs.collection('uploads');
    });

    mongoose.connection.on('connected', function () {
      logger.debug('Mongoose connection open.');
    });

    mongoose.connection.on('error', function (err) {
      logger.error('Mongoose connection error: ' + err);
    });

    mongoose.connection.on('disconnected', function () {
      logger.debug('Mongoose connection disconnected');
    });

    const db = await mongoose.connect(serverConfig.db);
    this.disconnect = () => mongoose.connection.close();

    return db;
  }
}
