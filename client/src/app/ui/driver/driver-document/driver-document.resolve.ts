import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SearchDriverAction} from "../../../store/drivers/driver.actions";
import {SelectLoggedInUser} from "../../../store/authentication/authentication.selectors";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {Roles} from "../../../../../../shared/constants";
import {SelectDocumentAction} from "../../../store/documents/document.actions";

@Injectable()
export class DriverDocumentResolve {

  private user: IUser;

  constructor(private store: Store<AppState>) {
    this.store.let(SelectLoggedInUser())
      .subscribe(user => this.user = user);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    //if (this.user.role !== Roles.Driver) return false;

    let companyId = this.user ? this.user._companyId : null;
    let userId = this.user ? this.user._id : null;
    if (!companyId || !userId) return false;

    let documentId = route.params['documentId'];

    this.store.dispatch(new SearchDriverAction(companyId, {_user: userId}));
    this.store.dispatch(new SelectDocumentAction(documentId));

    return true;
  }
}
