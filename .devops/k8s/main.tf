terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    # azurerm = {
    #   source  = "hashicorp/azurerm"
    #   version = "~> 3.0.0"
    # }
  }
}

resource "helm_release" "ingress-nginx" {
  provider         = helm.digitalocean
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress-nginx"
  create_namespace = true

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-health-probe-request-path"
    value = "/healthz"
  }

  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }
}

resource "helm_release" "cert-manager" {
  provider         = helm.digitalocean
  name             = "cert-manager"
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  create_namespace = true

  set {
    name  = "installCRDs"
    value = true
  }
}

resource "helm_release" "rabbitmq" {
  provider         = helm.digitalocean
  name             = "rabbitmq"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "rabbitmq"
  namespace        = "rabbitmq"
  create_namespace = true

  set {
    name  = "auth.username"
    value = local.rabbitmq_username
  }
  set {
    name  = "auth.password"
    value = local.rabbitmq_password
  }
}

resource "kubernetes_secret" "regcred" {
  provider = kubernetes.digitalocean
  metadata {
    name = "regcred"
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${local.docker_registry_server}" = {
          "username" = local.docker_registry_username
          "password" = local.docker_registry_password
          "email"    = local.docker_registry_email
          "auth"     = base64encode("${local.docker_registry_username}:${local.docker_registry_password}")
        }
      }
    })
  }
}

# https://opensource.com/article/21/8/terraform-deploy-helm
# resource "helm_release" "gp-kubernetes-dashboard" {
#   provider   = helm.azurerm
#   name       = "gp-kubernetes-dashboard"
#   repository = "https://kubernetes.github.io/dashboard/"
#   chart      = "kubernetes-dashboard"
#   namespace  = "default"

#   set {
#     name  = "service.type"
#     value = "LoadBalancer"
#   }

#   set {
#     name  = "protocolHttp"
#     value = "true"
#   }

#   set {
#     name  = "service.externalPort"
#     value = 80
#   }

#   set {
#     name  = "replicaCount"
#     value = 2
#   }

#   set {
#     name  = "rbac.clusterReadOnlyRole"
#     value = "true"
#   }
# }
