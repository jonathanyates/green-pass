import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {FormInputGroup} from "../../../shared/components/dynamic-form/dynamic-form.model";
import {IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {AppState} from "../../../../store/app.states";
import {OnRoadAssessmentInputMapper} from "./on-road-assessment-input.mapper";
import {SelectDriver} from "../../../../store/drivers/driver.selectors";
import {SaveOnRoadAssessmentAction} from "../../../../store/drivers/driver.actions";

@Component({
  selector: 'gp-check-input',
  template: `
    <gp-dynamic-form [inputGroups]="inputGroups" (submit)="save($event)" (cancel)="cancel()"></gp-dynamic-form>
  `
})
export class OnRoadAssessmentInputComponent implements OnInit, OnDestroy {

  inputGroups: FormInputGroup[];
  private driver: IDriver;
  private assessment: IOnRoadAssessment;
  private subscriptions = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private assessmentInputMapper: OnRoadAssessmentInputMapper) { }

  ngOnInit() {
    this.subscriptions.push(this.route.data
      .subscribe((data: { onRoadAssessment: IOnRoadAssessment }) => {
        this.assessment = data.onRoadAssessment;
        this.inputGroups = this.assessmentInputMapper.mapTo(this.assessment);
      }));

    this.subscriptions.push(this.store.let(SelectDriver())
      .subscribe(driver => this.driver = driver));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  save(form: FormGroup) {
    let assessment = {
      ...this.assessment,
      ...this.assessmentInputMapper.mapFrom(form, this.inputGroups, this.assessment)
  };

    this.store.dispatch(new SaveOnRoadAssessmentAction(this.driver, assessment));
  }

  cancel() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
