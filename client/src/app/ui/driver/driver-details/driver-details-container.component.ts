import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectDriverUserInfo} from "../../../store/drivers/driver.selectors";
import {AppState} from "../../../store/app.states";
import {Paths} from "../../../shared/constants";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {BaseComponent} from "../../shared/components/base/base.component";
import {Roles} from "../../../../../../shared/constants";
import {ICompanySettings} from '../../../../../../shared/interfaces/company-settings.interface';
import {SelectCompany} from '../../../store/company/company.selectors';

@Component({
  selector: 'gp-driver-container',
  template: `
    <gp-card [header]="'My Details'">
      <gp-driver-details [driver]="driver" [allowEdit]="allowInput" 
                         [isDriver]="isDriver" (edit)="edit()"
                         [companySettings]="companySettings"></gp-driver-details>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class DriverDetailsContainerComponent extends BaseComponent implements OnInit {

  driver: IDriver;
  companySettings: ICompanySettings;
  isDriver: boolean;
  allowInput: boolean = false;

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        this.driver = info.driver;
        this.allowInput = info.allowInput;
        let user = info.user;
        this.isDriver = user && user.role == Roles.Driver;
      }));

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => {
        this.companySettings = company.settings;
      }))
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
