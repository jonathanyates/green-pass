import {Component, Input} from "@angular/core";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {Router} from "@angular/router";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-driver-references-card',
  templateUrl: 'driver-references-card.component.html',
  host: {'class': 'card'}
})
export class DriverReferencesCardComponent {

  hasReferences: boolean;
  private _driver: IDriver;

  @Input() set driver(driver: IDriver) {
    this._driver = driver;
    if (driver) {
      this.hasReferences = driver.references && driver.references.length > 0;
    }
  }

  constructor(private router: Router) {}

  viewReferences() {
    // navigate to add vehicle if the driver has no vehicles to view
    if (this._driver && (!this._driver.references || this._driver.references.length === 0)) {
      return this.router.navigate([Paths.Driver, Paths.References, Paths.New]);
    }
    return this.router.navigate([Paths.Driver, Paths.References]);
  }
}
