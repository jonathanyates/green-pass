import { Component, Input } from '@angular/core';
import { IVehicle } from '../../../../../../shared/interfaces/vehicle.interface';
import { FormatDate } from '../../../../../../shared/utils/helpers';
import { IListGroupItem } from '../../shared/components/dynamic-list-group/dynamic-list-group';
import { Paths } from '../../../shared/constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'gp-vehicle-details',
  templateUrl: './vehicle-details.component.html',
})
export class VehicleDetailsComponent {
  details: IListGroupItem[];
  private _vehicle: IVehicle;

  constructor(private router: Router, private route: ActivatedRoute) {}

  @Input() allowInput: boolean = false;

  @Input()
  set vehicle(vehicle: IVehicle) {
    this._vehicle = vehicle;
    this.setDetails(vehicle);
  }
  get vehicle(): IVehicle {
    return this._vehicle;
  }

  setDetails(vehicle: IVehicle) {
    this.details = [
      { title: 'Registration Number', value: vehicle ? vehicle.registrationNumber : null },
      { title: 'Make', value: vehicle ? vehicle.make : null },
      { title: 'Model', value: vehicle ? vehicle.model : null },
      {
        title: 'Date of first registration',
        value: vehicle ? FormatDate(vehicle.dateOfFirstRegistration, 'dd MMMM yyyy') : null,
      },
      {
        title: 'Date of manufacture',
        value: vehicle ? FormatDate(vehicle.manufactureDate, 'dd MMMM yyyy') : null,
      },
      {
        title: 'Date first used',
        value: vehicle ? FormatDate(vehicle.firstUsedDate, 'dd MMMM yyyy') : null,
      },
      { title: 'Category', value: vehicle ? vehicle.category : null },
      { title: 'Fleet/Grey', value: vehicle ? vehicle.type : null },
      { title: 'Annual mileage', value: vehicle ? vehicle.annualMileage : null },
      { title: 'Cylinder capacity (cc)', value: vehicle ? vehicle.cylinderCapacityCc : null },
      { title: 'CO₂ Emissions', value: vehicle ? vehicle.co2Emissions : null },
      { title: 'Fuel type', value: vehicle ? vehicle.fuelType : null },
      { title: 'Euro status', value: vehicle ? vehicle.euroStatus : null },
      { title: 'Real Driving Emissions (RDE)', value: vehicle ? vehicle.realDrivingEmissions : null },
      { title: 'Export marker', value: vehicle ? vehicle.exportMarker : null },
      { title: 'Vehicle status', value: vehicle ? vehicle.vehicleStatus : null },
      { title: 'Colour', value: vehicle ? vehicle.vehicleColour : null },
      { title: 'Vehicle type approval', value: vehicle ? vehicle.vehicleTypeApproval : null },
      { title: 'Wheelplan', value: vehicle ? vehicle.wheelplan : null },
      { title: 'Revenue weight', value: vehicle ? vehicle.revenueWeight : null },
      {
        title: 'Date of last V5C (logbook) issued',
        value: vehicle ? FormatDate(vehicle.dateOfLastv5cIssued, 'dd MMMM yyyy') : null,
      },
    ];

    if (vehicle.vehicleType) {
      this.details.splice(6, 0, { title: 'Vehicle Type', value: vehicle.vehicleType });
      this.details.splice(7, 0, { title: 'Class', value: vehicle.class });
    }
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }
}
