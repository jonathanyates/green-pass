import { Component } from '@angular/core';

@Component({
    selector: 'gp-page-not-found',
    template: `
    <gp-heading [text]="'Page not found'"></gp-heading>
    <div class="alert alert-danger" role="alert">
      <p>Sorry this page does not exist.</p>
    </div>
`
})
export class PageNotFoundComponent {
}
