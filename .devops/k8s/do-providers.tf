provider "digitalocean" {
  token = local.do.token
}

resource "digitalocean_kubernetes_cluster" "gp-k8s-cluster" {
  name    = "${local.clustername}-${terraform.workspace}"
  region  = local.do.env[terraform.workspace].region
  version = local.do.env[terraform.workspace].k8s_version

  node_pool {
    name       = "gp-k8s-${terraform.workspace}-pool"
    size       = local.do.env[terraform.workspace].size
    node_count = local.do.env[terraform.workspace].node_count
  }
}

provider "kubernetes" {
  alias = "digitalocean"
  host  = digitalocean_kubernetes_cluster.gp-k8s-cluster.endpoint
  token = digitalocean_kubernetes_cluster.gp-k8s-cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.gp-k8s-cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
  alias = "digitalocean"
  kubernetes {
    host  = digitalocean_kubernetes_cluster.gp-k8s-cluster.endpoint
    token = digitalocean_kubernetes_cluster.gp-k8s-cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.gp-k8s-cluster.kube_config[0].cluster_ca_certificate
    )
  }
}
