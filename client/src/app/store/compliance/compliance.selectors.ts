import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {ComplianceState} from "./compliance.state";
import {CompanyCompliance} from "../../../../../shared/models/company-compliance.model";

export const SelectCompliance = () => {
  return (store:Store<AppState>) => {
    return store.select<ComplianceState>(state => state.compliance)
      .map(state => state.list);
  }
};

export const SelectSelectedCompliance = () => {
  return (store:Store<AppState>) => {
    return store.select<ComplianceState>(state => state.compliance)
      .map(state => {
        let compliance = state.selected;
        if (compliance) {
          return CompanyCompliance.create(compliance);
        }
        return compliance;
      });
  }
};

export const SelectTotalCompliance = () => {
  return (store:Store<AppState>) => {
    return store.let(SelectCompliance())
      .filter(items => items != null && items.length > 0)
      .map(items => CompanyCompliance.getTotalCompliance(items));
  }
};
