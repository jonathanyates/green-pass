import {
  Component, Input, Output, EventEmitter, AfterViewInit, OnInit,
  OnDestroy
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormInput, FormInputGroup} from './dynamic-form.model';
import {DynamicFormBuilder} from './dynamic-form.builder';
import {Messages} from '../../../../store/app.messages';
import {ServerError} from '../../../../shared/errors/server.error';
import {messenger} from '../../../../services/messenger';
import {Subscription} from 'rxjs/Subscription';

declare let $: any;

@Component({
  selector: 'gp-dynamic-form',
  templateUrl: 'dynamic-form.component.html'
})
export class DynamicFormComponent implements OnInit, AfterViewInit, OnDestroy {

  submitted = false;
  private _inputGroups: FormInputGroup[];
  private _error: string;
  private subscription: Subscription;

  @Input()
  set error(error: string) {
    this._error = error;
    if (error) {
      this.scrollToEnd();
    }
  }
  get error(): string {
    return this._error;
  }

  @Input()
  set inputGroups(inputGroups: FormInputGroup[]) {
    this._inputGroups = inputGroups;
    if (inputGroups) {
      this.form = DynamicFormBuilder.buildFromGroups(this._inputGroups);
      this.afterInit.emit(this.form);
    }
  }
  get inputGroups(): FormInputGroup[] {
    return this._inputGroups;
  }

  @Output() submit: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() cancel: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() afterInit: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  form: FormGroup;
  errors: string[];

  ngOnInit(): void {
    this.subscription = messenger.toObservable(Messages.SaveError)
      .subscribe((error: ServerError) => {
        this.error = error.message;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.error = null;
  }

  ngAfterViewInit(): void {
    // Material Select Initialization
    // Data Picker Initialization
    setTimeout(() => {
      $(document).ready(function() {
        $('.mdb-select').material_select();
        $('.datepicker').pickadate({
          selectYears: 225,
        });
      });
    }, 300);

  }

  onSubmit() {

    if (!this.form.valid) {
      this.errors = Object.keys(this.form.controls)
        .filter(key => this.form.controls[key].errors)
        .map(key => {
          let control = this.form.controls[key];
          let errors = Object.keys(control.errors)
            .map(error => this.toProperCase(error)).join(', ');
          let input: FormInput = (<any>control).input;
          let error = `${input.label} - ${errors}`;
          control.markAsTouched();
          control.markAsDirty();
          return error;
        });

      return;
    }

    this.errors = null;
    this.error = null;
    this.submitted = true;
    this.submit.emit(this.form);
  }

  toProperCase(str: string) {
    return str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
  };

  onCancel() {
    this.error = null;
    this.cancel.emit(this.form);
  }

  scrollToEnd(): void {
    setTimeout(() => {
      window.scrollTo(0, document.body.scrollHeight);
    }, 1);
  }

}
