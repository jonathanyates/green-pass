import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Paths} from '../../../shared/constants';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.states';
import {SelectIsGreenPassAdmin, SelectLoggedInUser} from '../../../store/authentication/authentication.selectors';
import {IUser} from '../../../../../../shared/interfaces/user.interface';
import {IListGroupItem} from '../../shared/components/dynamic-list-group/dynamic-list-group';
import {SelectSelectedCompliance} from '../../../store/compliance/compliance.selectors';
import {IReportType} from '../../../../../../shared/interfaces/report.interface';
import {SelectReportTypesForCompany} from '../../../store/reports/report.selectors';
import {AlertService} from '../../../services/alert.service';
import {SelectCompany} from '../../../store/company/company.selectors';
import {ICompany} from '../../../../../../shared/interfaces/company.interface';
import {ISubscription} from '../../../../../../shared/interfaces/subscription.interface';
import {SubscriptionHelper} from '../../../../../../shared/models/subscription.model';
import { Subscription } from 'rxjs/Subscription';
import {SelectSelectedAlertSummary} from "../../../store/alerts/alert.selectors";

@Component({
  selector: 'gp-company-dashboard',
  templateUrl: 'company-dashboard.component.html'
})
export class CompanyDashboardComponent implements OnInit, OnDestroy {

  company: ICompany;
  subscription: ISubscription;
  detailItems: IListGroupItem[];
  contactItems: IListGroupItem[];
  reportTypes: IReportType[];
  user: IUser;
  drivers: number = 0;
  vehicles: number = 0;
  compliance: any;
  isAdmin: boolean;

  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<AppState>,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectCompany())
      .filter(company => company != null)
      .combineLatest(this.store.let(SelectIsGreenPassAdmin()).take(1))
      .subscribe(results => {
        this.company = results[0];
        this.isAdmin = results[1];
        this.subscription = SubscriptionHelper.getSubscription(this.company);

        if (this.company) {
          this.detailItems = [
            {title: 'Core Business', value: this.company.coreBusiness},
            {
              title: 'Controlling Mind',
              value: `${this.company.controllingMind.name} - ${this.company.controllingMind.position}`
            }
          ];
          if (this.company.noOfEmployees > 0) {
            this.detailItems.push({title: 'Size', value: `${this.company.noOfEmployees} employees`});
          }
          this.contactItems = [
            {title: 'Phone', value: this.company.phone},
            {title: 'Mobile', value: this.company.mobile},
            {title: 'Email', value: this.company.email}
          ];

          this.subscriptions.push(
            this.store.let(SelectSelectedAlertSummary())
              .subscribe(summary => {
                this.drivers = summary ? summary.driverAlerts : 0;
                this.vehicles = summary ? summary.vehicleAlerts : 0;
              }));
        }
      }));

    this.subscriptions.push(this.store.let(SelectSelectedCompliance())
      .subscribe(compliance => {
        this.compliance = compliance;
      }));

    this.subscriptions.push(this.store.let(SelectLoggedInUser())
      .subscribe(user => this.user = user));

    this.subscriptions.push(this.store.let(SelectReportTypesForCompany())
      .subscribe(reportTypes => this.reportTypes = reportTypes));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  selectCompany() {
    this.router.navigate(['./', Paths.Details], { relativeTo: this.route })
  }

  selectDrivers() {
    this.router.navigate(['./', Paths.Drivers], { relativeTo: this.route });
  }

  selectVehicles() {
    this.router.navigate(['./', Paths.Vehicles], { relativeTo: this.route });
  }

  selectDocumentTemplates() {
    this.router.navigate(['./', Paths.Templates], { relativeTo: this.route });
  }

  selectUsers() {
    this.router.navigate(['./', Paths.Users], { relativeTo: this.route });
  }

  selectDepots() {
    this.router.navigate(['./', Paths.Depots], { relativeTo: this.route });
  }

  selectSuppliers() {
    this.router.navigate(['./', Paths.Suppliers], { relativeTo: this.route });
  }

  selectAlerts() {
    this.router.navigate(['./', Paths.Alerts], { relativeTo: this.route });
  }

  manageAlerts() {
    this.router.navigate(['./', Paths.AlertTypes], { relativeTo: this.route });
  }

  selectReports() {
    this.router.navigate(['./', Paths.Reports], { relativeTo: this.route });
  }

  selectCompanyCompliance() {
    this.router.navigate(['./', Paths.Compliance], { relativeTo: this.route });
  }

  selectDriverCompliance() {
    this.router.navigate(['./', Paths.Drivers, Paths.Compliance], { relativeTo: this.route });
  }

  selectVehicleCompliance() {
    this.router.navigate(['./', Paths.Vehicles, Paths.Compliance], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate([Paths.Companies]);
  }

}
