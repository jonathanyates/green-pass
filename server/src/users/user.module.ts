import {ContainerModule, interfaces} from "inversify";
import {IApiRouter} from "../interfaces/api-router.interface";
import {TYPES} from "../container/container.types";
import {IApiController} from "../interfaces/api-controller.interface";
import {IRepository} from "../db/db.repository";
import {UserRouter} from "./user.router";
import {UserController} from "./user.controller";
import {IUserService, UserService} from "./user.service";
import {UserRepository} from "./user.repository";
import {IUserModel} from "./user.schema";

export const userModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.UserRouter).to(UserRouter).inSingletonScope();
  bind<IApiController>(TYPES.controllers.UserController).to(UserController).inSingletonScope();
  bind<IUserService>(TYPES.services.UserService).to(UserService).inSingletonScope();
  bind<IRepository<IUserModel>>(TYPES.repositories.UserRepository).to(UserRepository).inSingletonScope();
});