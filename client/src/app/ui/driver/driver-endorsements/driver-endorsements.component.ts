import {Component, Input} from '@angular/core';
import {IEndorsement} from "../../../../../../shared/interfaces/driver.interface";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {FormatDate} from "../../../../../../shared/utils/helpers";

@Component({
  selector: 'gp-driver-endorsements',
  template: `<gp-dynamic-list [list]="list" [lightHeader]="true"></gp-dynamic-list>`,
  styleUrls: ['./driver-endorsements.component.css']
})
export class DriverEndorsementsComponent {

  private _endorsements: IEndorsement[];

  @Input()
  set endorsements(endorsements: IEndorsement[]) {
    this._endorsements = endorsements;
    this.createList(endorsements);
  }
  get endorsements() { return this._endorsements; }

  list: IDynamicList;

  createList(endorsements: IEndorsement[]) {
    this.list = {
      columns: [
        {header: 'Category', field: 'category'},
        {header: 'Description', field: 'description'},
        {header: 'Points', field: 'points'},
        {header: 'Offence Date', field: 'offenceDate'},
        {header: 'Expiry Date', field: 'expiryDate'},
        {header: 'Removal Date', field: 'removalDate'},
        {header: 'Court/Office', field: 'courtDetails'},
      ]
    };

    if (endorsements) {
      this.list.items = endorsements.map(endorsement => Object.assign({}, endorsement, {
        offenceDate: FormatDate(new Date(endorsement.offenceDate)),
        expiryDate: FormatDate(new Date(endorsement.expiryDate)),
        removalDate: FormatDate(new Date(endorsement.removalDate)),
        courtDetails: endorsement.courtDetails.description
      }))
    }
  }
}
