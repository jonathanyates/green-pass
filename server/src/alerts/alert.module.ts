import {ContainerModule, interfaces} from "inversify";
import {IQueryableRepository,} from "../db/db.repository";
import {IAlertModel} from "./alert.schema";
import {TYPES} from "../container/container.types";
import {AlertRepository, IAlertRepository} from "./alert.repository";
import {AlertService, IAlertService} from "./alert.service";
import {AlertRouter, AlertTypeRouter, IAlertRouter} from "./alert.router";
import {IApiRouter} from "../interfaces/api-router.interface";
import {AlertController, IAlertController} from "./alert.controller";
import {AlertTypeRepository} from "./alert-type.repository";
import {IAlertTypeModel} from "./alert-type.schema";
import {AlertQueryService, IAlertQueryService} from "./alert-query.service";

export const alertModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IAlertRouter>(TYPES.routers.AlertRouter).to(AlertRouter).inSingletonScope();
  bind<IApiRouter>(TYPES.routers.AlertTypeRouter).to(AlertTypeRouter).inSingletonScope();
  bind<IAlertController>(TYPES.controllers.AlertController).to(AlertController).inSingletonScope();
  bind<IAlertService>(TYPES.services.AlertService).to(AlertService).inSingletonScope();
  bind<IAlertQueryService>(TYPES.services.AlertQueryService).to(AlertQueryService).inSingletonScope();
  bind<IQueryableRepository<IAlertTypeModel>>(TYPES.repositories.AlertTypeRepository).to(AlertTypeRepository).inSingletonScope();
  bind<IAlertRepository>(TYPES.repositories.AlertRepository).to(AlertRepository).inSingletonScope();
});