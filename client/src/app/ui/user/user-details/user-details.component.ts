import {Component, OnDestroy, OnInit} from '@angular/core';
import {IListGroupItem} from "../../shared/components/dynamic-list-group/dynamic-list-group";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Paths} from "../../../shared/constants";
import {IUser} from "../../../../../../shared/interfaces/user.interface";
import {User} from "../user.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectSelectedUser} from "../../../store/users/user.selectors";

@Component({
  selector: 'gp-user-details',
  template: `
    <gp-card [header]="'User details'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private user: IUser;
  private subscription: Subscription;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<AppState>) {
  }

  ngOnInit() {
    this.subscription = this.store.let(SelectSelectedUser())
      .filter(x => x != null)
      .subscribe(user => {
        this.user = user;
        this.items = [
          {title: "Name", value: User.getFullName(user)},
          {title: "Username", value: user.username},
          {title: "Email", value: user.email}
        ]
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit() {
    this.router.navigate(['.', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
