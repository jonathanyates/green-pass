import {Injectable} from "@angular/core";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {apiPath} from "../shared/constants";
import {Observable} from "rxjs";
import {IPage} from "../ui/dynamic/page.model";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PageService extends HttpBaseService {

  private apiUrl = apiPath + '/pages/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getPage(pageId): Observable<IPage> {
    return this.httpClient.get<IPage>(this.apiUrl + pageId, this.getOptions())
      .map(result => result)
      .catch(this.handleError);
  }

  getHtml(pageId): Observable<IPage> {

    return this.httpClient.get('/assets/html' + pageId + '.html', { responseType: 'text' })
      .map(result => {
        let content = result;
          return {
            _id: null,
            pageId: pageId,
            content: content
          }
      }).catch(this.handleError);
  }

  savePage(thePage: IPage): Observable<IPage> {

    this.authorize();

    const url = this.apiUrl + thePage.pageId;

    if (thePage._id) {
      return this.httpClient.put<IPage>(url, thePage, this.getOptions())
        .map(res => res)
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IPage>(url, thePage, this.getOptions())
        .map(res => res)
        .catch(this.handleError);
    }
  }

}
