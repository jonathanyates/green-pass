import '../../../../../../shared/extensions/date.extensions';
import {Component, OnInit, ViewChild} from '@angular/core';
import {Company} from "../../company/company.model";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {pagination, Paths} from "../../../shared/constants";
import {Driver} from "../driver.model";
import {SelectCompany} from "../../../store/company/company.selectors";
import {SelectDriversPagination} from "../../../store/drivers/driver.selectors";
import {LoadDriversAction} from "../../../store/drivers/driver.actions";
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {ModalComponent} from "../../shared/components/modal/model.component";
import {DriverService} from "../../../services/driver.service";
import {BusyAction} from "../../../store/progress/progress.actions";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {SelectIsGreenPassAdmin} from "../../../store/authentication/authentication.selectors";

@Component({
  template: `
    <gp-drivers [company]="company" [drivers]="drivers" (back)="back()" [showSearch]="true"
                [total]="total" [page]="page" [itemsPerPage]="itemsPerPage" [serverMode]="true"
                (pageChange)="onPageChange($event)" (search)="onSearch($event)"
                (add)="onAdd()" (remove)="onRemove($event)"></gp-drivers>
    <button *ngIf="allowImport" class="btn btn-primary waves-effect" (click)="importDrivers()">
      <i class="fa fa-download mr-2" aria-hidden="true"></i>Import</button>

    <gp-model [title]="'Remove Driver'" (confirmed)="onRemoveConfirmed($event)">
      <div *ngIf="selectedDriver; else noDriver">
        Are you sure you want to remove driver '{{selectedDriver._user.forename}} {{selectedDriver._user.surname}}' ?
      </div>
      <ng-template #noDriver>No Driver selected</ng-template>
    </gp-model>
  `
})
export class DriverListContainerComponent implements OnInit {

  company: Company;
  drivers: Driver[];

  total: number;
  page: number;
  itemsPerPage: number;
  selectedDriver: IDriver;
  allowImport: boolean;
  private searchCriteria: any;
  private subscriptions = [];

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<AppState>,
              private driverService: DriverService) {
  }

  ngOnInit() {
    this.itemsPerPage = pagination.pageSize;
    //this.drivers = null;
    this.subscriptions.push(this.store.let(SelectDriversPagination())
      .filter(drivers => drivers !== null)
      .subscribe(results => {
        this.drivers = results.drivers;
        this.total = results.total;
        this.page = results.page;
      }));

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));

    this.subscriptions.push(this.store.let(SelectIsGreenPassAdmin())
      .subscribe(isAdmin => this.allowImport = isAdmin))
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onPageChange(page: number) {
    if (this.searchCriteria) {
      this.store.dispatch(new LoadDriversAction(this.company._id, page, this.itemsPerPage, this.searchCriteria));
      return;
    }

    this.store.dispatch(new LoadDriversAction(this.company._id, page, this.itemsPerPage))

  }

  onSearch(term: string) {
    if (term) {
      this.searchCriteria = {
        name: term
      };
      this.store.dispatch(new LoadDriversAction(this.company._id, 1, this.itemsPerPage, this.searchCriteria));
      return
    }

    this.store.dispatch(new LoadDriversAction(this.company._id, 1, this.itemsPerPage))
  }

  onAdd() {
    if (this.company && this.company._id) {
      return this.router.navigate(['./', Paths.New], { relativeTo: this.route });
    }
  }

  onRemove(driver:IDriver) {
    if (!driver) return;
    this.modal.open();
    this.selectedDriver = driver;
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedDriver) {
      this.store.dispatch(new BusyAction());
      this.driverService.removeDriver(this.selectedDriver._companyId, this.selectedDriver._id)
        .subscribe(driver => {
          this.store.dispatch(new LoadDriversAction(this.company._id, this.page, this.itemsPerPage));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
    this.selectedDriver = null;
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  importDrivers() {
    this.router.navigate(['./', Paths.Import], { relativeTo: this.route });
  }

}
