import * as actions from "./authentication.actions";
import {IUser} from "../../../../../shared/interfaces/user.interface";

export interface AuthenticationState {
  token?: string;
  user?: IUser;
  loggedIn: boolean;
  message?: string;
  redirectUrl?: string;
}

const initialState: AuthenticationState = {
  token: null,
  user: null,
  loggedIn: false
};

export function AuthenticationReducer(state: AuthenticationState = initialState, action: actions.Actions): AuthenticationState {
    switch (action.type) {
      case actions.ActionTypes.LOGIN_SUCCESS:
        let loginSuccessPayload = <AuthenticationState>action.payload;
        return Object.assign({}, state, {
          token: loginSuccessPayload.token,
          user: loginSuccessPayload.user,
          loggedIn: loginSuccessPayload.token !== null && loginSuccessPayload.user != null,
          message: null
        });

      case actions.ActionTypes.CHANGE_PASSWORD_SUCCESS:
        let user = <IUser>action.payload;
        return Object.assign({}, state, {
          user: user,
        });

      case actions.ActionTypes.LOGIN_FAILED:
        let loginFailedPayload = <AuthenticationState>action.payload;
        return Object.assign({}, state, {
          token: null,
          user: null,
          loggedIn: false,
          message: loginFailedPayload.message
        });

      case actions.ActionTypes.SET_REDIRECT_URL:
        return Object.assign({}, state, {
          redirectUrl: action.payload
        });

      case actions.ActionTypes.LOGOUT:
        return { token: null, user: null, loggedIn: false, message: '', redirectUrl: null };

      default:
        return state;
    }
}

