import {IAddress} from "./address.interface";

export function isAddress(address: IAddress): address is IAddress {
  return (<IAddress>address).postcode !== undefined;
}
