import {Component, OnInit, Input} from '@angular/core';
import {AddressInputMapper} from "./address-input.mapper";
import {IAddress} from "../../../../../../../shared/interfaces/address.interface";
import {FormInputGroup} from "../dynamic-form/dynamic-form.model";

@Component({
  selector: 'gp-address-input',
  template: '<gp-dynamic-form [inputGroups]="inputGroups"></gp-dynamic-form>',
  styleUrls: ['address-input.component.css']
})
export class AddressInputComponent implements OnInit {

  constructor(private addressInputMapper: AddressInputMapper) {}

  @Input() set address(address: IAddress) {
    if (address) {
      let inputGroup = this.addressInputMapper.mapTo(address);
      this.inputGroups = [inputGroup]
    }
  }

  inputGroups: FormInputGroup[];

  ngOnInit() {
  }

}
