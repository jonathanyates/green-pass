import {IAddress, IAddressList} from "../../../../../shared/interfaces/address.interface";

export interface AddressState {
  list: IAddressList,
  selected?: IAddress
}
