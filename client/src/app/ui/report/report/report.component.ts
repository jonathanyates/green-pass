import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Company} from "../../company/company.model";
import {ReportService} from "../../../services/report.service";
import {BusyAction} from "../../../store/progress/progress.actions";
import {IReportQueryResult} from "../../../../../../shared/interfaces/report.interface";
import {SelectReportTypesAction} from "../../../store/reports/report.actions";

@Component({
  template: `
    <ng-container *ngIf="reportResult">
      <div [ngSwitch]="reportResult.reportType.category">
        <div *ngSwitchCase="'Driver'">
          <gp-drivers [company]="company" [drivers]="reportResult.result" (back)="back()"
                      [itemsPerPage]="itemsPerPage"></gp-drivers>
        </div>
        <div *ngSwitchCase="'Vehicle'">
          <gp-vehicle-list [vehicles]="reportResult.result" (back)="back()"></gp-vehicle-list>
        </div>
        <div *ngSwitchDefault>Unknown report category</div>
      </div>
    </ng-container>
  `
})
export class ReportComponent implements OnInit, OnDestroy {

  error: string;
  company: Company;
  reportResult: IReportQueryResult;

  total: number;
  page: number;
  itemsPerPage: number;

  private subscriptions = [];

  constructor(
    private reportService: ReportService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.itemsPerPage = 10;
    this.store.dispatch(new SelectReportTypesAction());

    this.route.params.map((params: Params) => params['reportTypeId'])
      .combineLatest(
        this.store.let(SelectCompany()).filter(x => x != null)
      )
      .subscribe(results => {
        let reportTypeId = results[0];
        this.company = results[1];

        this.store.dispatch(new BusyAction());

        this.reportService.getReport(this.company._id, reportTypeId)
          .catch(error => this.error = error.message)
          .finally(() => this.store.dispatch(new BusyAction(false)))
          .subscribe((reportResult: IReportQueryResult) => {
            this.reportResult = reportResult;
          });
      });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
