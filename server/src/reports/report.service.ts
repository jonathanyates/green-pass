
import {IReportQueryService} from "./report-query.service";
import {TYPES} from "../container/container.types";
import {inject, injectable} from "inversify";
import {IReportTypeService} from "./report-type.service";
import codes from "../shared/error.codes";
import {IReportQueryResult, IReportType} from "../../../shared/interfaces/report.interface";

export interface IReportService {
  get(companyId: string, reportTypeId: string): Promise<any>;
}

@injectable()
export class ReportService {

  constructor(
    @inject(TYPES.services.ReportTypeService) private reportTypeService: IReportTypeService,
    @inject(TYPES.services.ReportQueryService) private reportQueryService: IReportQueryService
  ) {}

  get(companyId: string, reportTypeId: string): Promise<any> {
    return this.reportTypeService.get(reportTypeId)
      .then(reportType => {

        const query: (companyId: string, reportType: IReportType) => Promise<IReportQueryResult> =
          this.reportQueryService.queries[reportType.query];

        if (!query) {
          const error:any = new Error(`Query '${reportType.query}' not found for report type ${reportType.name}`);
          error.status = codes.NOT_FOUND;
          return error;
        }

        return query(companyId, reportType);
      })
  }
}