import * as actions from "./companies.actions";
import {CompaniesState} from "./companies.state";
import {ActionTypes} from "../authentication/authentication.actions";

export const initialState: CompaniesState = {
  list: []
};

export function CompaniesReducer(state: CompaniesState = initialState, action: actions.Actions): CompaniesState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_SUCCESS:
        return Object.assign({}, state, { list: action.payload });

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }

};
