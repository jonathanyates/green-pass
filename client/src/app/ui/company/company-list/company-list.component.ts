import { Paths } from '../../../shared/constants';
import { AppState } from '../../../store/app.states';
import { SelectCompanies } from '../../../store/companies/companies.selectors';
import { IDynamicList } from '../../../../../../shared/models/dynamic-list.model';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {ICompany} from "../../../../../../shared/interfaces/company.interface";
import {ModalComponent} from "../../shared/components/modal/model.component";
import {BusyAction} from "../../../store/progress/progress.actions";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {LoadCompaniesAction} from "../../../store/companies/companies.actions";
import {CompaniesService} from "../../../services/companies.service";
import {ColumnType} from "../../../../../../shared/models/column-type.model";

@Component({
  template: `
    <gp-dynamic-list [list]="list" (add)="add()" (itemSelected)="select($event)" (back)="back()"
                     (remove)="onRemove($event)"></gp-dynamic-list>

    <gp-model [title]="'Remove Company'" (confirmed)="onRemoveConfirmed($event)">
      <div *ngIf="selectedCompany; else noCompany">
        Are you sure you want to remove company '{{selectedCompany.name}}' ?
      </div>
      <ng-template #noCompany>No Company selected</ng-template>
    </gp-model>
`
})
export class CompanyListComponent implements OnInit, OnDestroy {

  companies: ICompany[];
  list: IDynamicList;
  selectedCompany: ICompany;
  private subscription: Subscription;

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private companyService: CompaniesService) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectCompanies())
      .subscribe(companies => {
        this.list = {
          columns: [
            { header: 'Name', field: 'name' },
            { header: 'Address', field: 'address' },
            {header: '', field: 'remove', columnType: ColumnType.Icon}
          ],
          items: companies,
          actions: companies.map(item => ({
            remove: {
              action: args => { if (args) this.onRemove(args.item) },
              icon: 'fa fa-trash-o green-text'
            }
          }))
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  select(company: ICompany) {
    if (company) {
      this.router.navigate([Paths.Company, company._id]);
    }
  }

  add() {
    this.router.navigate([Paths.Companies, Paths.New]);
  }

  back() {
    this.router.navigate([Paths.Admin]);
  }

  onRemove(company:ICompany) {
    if (!company) return;
    this.selectedCompany = company;
    this.modal.open();
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedCompany) {
      this.store.dispatch(new BusyAction());
      this.companyService.removeCompany(this.selectedCompany._id)
        .subscribe(company => {
          this.store.dispatch(new LoadCompaniesAction());
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
    this.selectedCompany = null;
  }

}
