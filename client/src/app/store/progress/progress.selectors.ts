import {Store} from "@ngrx/store";
import {AppState} from "../app.states";
import {ProgressState} from "./progress.state";

export const IsBusy = () => {
  return (store:Store<AppState>) => {
    return store.select<ProgressState>(state => state.progress)
      .map(state => state.busy)
      .distinctUntilChanged();
  }
};
