import {injectable, inject} from "inversify";
import {TYPES} from "../container/container.types";
import {IApiController} from "../interfaces/api-controller.interface";
import codes from "../shared/error.codes";
import {AuditActions, AuditTargets, IAuditService} from "../audit/audit.service";
import {IUserService} from "./user.service";
import {Request, Response, NextFunction} from "express-serve-static-core";
import {jsonDeserialiser} from "../../../shared/utils/json.deserialiser";

@injectable()
export class UserController implements IApiController {

  constructor(
    @inject(TYPES.services.UserService) private userService: IUserService,
    @inject(TYPES.services.AuditService) private auditService: IAuditService
  ) {}

  getAll = (req, res, next) => {

    const companyId = req.params.id;

    this.userService.getAll(companyId)
      .then(users => res.status(codes.OK).json(users))
      .catch(err => next(err));
  };

  get = (req, res, next) => {

    const id = req.params.userId;

    this.userService.get(id)
      .then(user => {
        if (!user) {
          return res.status(codes.NOT_FOUND).send({message: `User not found with id ${id}`});
        }
        return res.status(codes.OK).json(user);
      })
      .catch(err => next(err));
  };

  add = (req, res, next) => {

    const companyId = req.params.id;
    const user = jsonDeserialiser.parseDates(req.body);

    if (!user) {
      return res.status(codes.BAD_REQUEST).send({message: 'No user specified.'});
    }

    this.userService.add(companyId, user)
      .then(user => {
        this.auditService.audit(req.user, AuditActions.Add, AuditTargets.User, user);
        res.status(codes.CREATED).json(user)
      })
      .catch(err => next(err));
  };

  update = (req, res, next) => {

    const user = jsonDeserialiser.parseDates(req.body);

    if (!user) {
      return res.status(codes.BAD_REQUEST).send({message: 'No user specified.'});
    }

    this.userService.update(user)
      .then(user => {
        this.auditService.audit(req.user, AuditActions.Update, AuditTargets.User, user);
        res.status(codes.ACCEPTED).json(user)
      })
      .catch(err => next(err));
  }

  remove = (req: Request, res: Response, next: NextFunction) => {
    const userId = req.params.userId;

    if (!userId) {
      return res.status(codes.BAD_REQUEST).send({message: 'No user id specified.'});
    }

    this.userService.remove(userId)
      .then(user => {
        this.auditService.audit(req.user, AuditActions.Remove, AuditTargets.User, user);
        res.status(codes.ACCEPTED).json(user)
      })
      .catch(err => {
        this.auditService.auditError(req.user, AuditActions.Remove, AuditTargets.User, err);
        next(err)
      });
  };
}