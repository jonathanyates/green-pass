provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "gp-resource-group" {
  name     = "gp-resources-${terraform.workspace}"
  location = local.env[terraform.workspace].location
}

resource "azurerm_kubernetes_cluster" "gp-k8s-cluster" {
  name                = "${local.clustername}-${terraform.workspace}"
  location            = azurerm_resource_group.gp-resource-group.location
  resource_group_name = azurerm_resource_group.gp-resource-group.name
  dns_prefix          = "k8s${terraform.workspace}"

  default_node_pool {
    name       = "${terraform.workspace}pool"
    vm_size    = local.env[terraform.workspace].node_pool_size
    node_count = local.env[terraform.workspace].node_count
  }

  identity {
    type = "SystemAssigned"
  }
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value     = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config_raw
  sensitive = true
}

provider "kubernetes" {
  alias                  = "azurerm"
  host                   = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.host
  username               = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.username
  password               = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.password
  client_certificate     = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.cluster_ca_certificate)
}

provider "helm" {
  alias = "azurerm"
  kubernetes {
    host                   = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.host
    username               = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.username
    password               = azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.password
    client_certificate     = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.gp-k8s-cluster.kube_config.0.cluster_ca_certificate)
  }
}
