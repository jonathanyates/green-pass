import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.states";
import {Driver} from "./driver.model";
import {SelectCompanyAction} from "../../store/company/company.actions";
import {SelectDriverAction, SelectDriverSuccessAction} from "../../store/drivers/driver.actions";
import {Paths} from "../../shared/constants";

@Injectable()
export class DriverResolve {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let companyId = route.params['id'];
    if (!companyId) return false;

    this.store.dispatch(new SelectCompanyAction(companyId));

    let driverId = route.params['driverId'];

    // new driver
    if (!driverId && route.url.slice(-1)[0].path === Paths.New) {
      this.store.dispatch(new SelectDriverSuccessAction(new Driver(companyId)));
      return true;
    }

    if (driverId) {
      this.store.dispatch(new SelectDriverAction(companyId, driverId));
    }

    return true;
  }
}
