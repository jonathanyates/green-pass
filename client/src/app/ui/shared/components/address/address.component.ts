import {Component, Input} from '@angular/core';
import {IAddress} from "../../../../../../../shared/interfaces/address.interface";

@Component({
  selector: 'gp-address',
  templateUrl: 'address.component.html',
  styleUrls: ['address.component.css']
})
export class AddressComponent  {
  @Input() address: IAddress;
}
