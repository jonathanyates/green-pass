import "reflect-metadata";
import {TYPES} from "../src/container/container.types";
import container from "../src/container/container.config";
import {IReportTypeService} from "../src/reports/report-type.service";
import {reportInfo} from "./report-types";
import {IAlertService} from "../src/alerts/alert.service";

const reportService:IReportTypeService = container.get<IReportTypeService>(TYPES.services.ReportTypeService);
const alertService:IAlertService = container.get<IAlertService>(TYPES.services.AlertService);

Promise.all(reportInfo.map(info => {
  return reportService.add(info.reportType)
    .then(reportType => {
      info.alertType.reportType = reportType;
      return alertService.addAlertType(info.alertType)
    })
}))
.then(reports => {
  console.log((reports ? reports.length : 0) + ' reports created.');
  process.exit();
})
.catch(error =>{
  console.log(error);
  process.exit(1);
});