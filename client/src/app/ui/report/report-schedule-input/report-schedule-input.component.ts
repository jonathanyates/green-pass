import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {ReportService} from "../../../services/report.service";
import {LoadReportTypesAction} from "../../../store/reports/report.actions";
import {IReportSchedule, IReportType} from "../../../../../../shared/interfaces/report.interface";
import {Paths} from "../../../shared/constants";
import {SelectCompany} from "../../../store/company/company.selectors";
import {Company} from "../../company/company.model";
import {IDynamicList} from "../../../../../../shared/models/dynamic-list.model";
import {ReportScheduleRecipientsComponent} from "app/ui/report/report-schedule-input/report-schedule-recipients.component";
import {SelectReportTypes} from "../../../store/reports/report.selectors";
import {Cron} from "../../shared/components/cron/cron.model";

@Component({
  moduleId: module.id,
  selector: 'gp-report-schedule-input',
  templateUrl: 'report-schedule-input.component.html'
})
export class ReportScheduleInputComponent implements OnInit, OnDestroy {

  reportSchedule: IReportSchedule;
  private company: Company;
  private reportType: IReportType;
  private subscriptions = [];

  @ViewChild(ReportScheduleRecipientsComponent) recipientsInput: ReportScheduleRecipientsComponent;

  private _cron: Cron;
  set cron(cron: Cron) {
    this._cron = cron;
  }
  get cron() { return this._cron; }

  recipients: IDynamicList;
  error: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<AppState>,
              private reportService: ReportService,
              private location: Location) {
  }

  ngOnInit() {
    this.store.dispatch(new LoadReportTypesAction());

    this.subscriptions.push(
      this.store.let(SelectCompany())
        .subscribe(company => {
          this.company = company;
        }));

    this.subscriptions.push(
      this.route.params.map((params: Params) => params['reportTypeId'])
        .switchMap(reportTypeId => this.store.let(SelectReportTypes())
          .filter(reportTypes => reportTypes != null)
          .map(reportTypes => reportTypes.find(reportType => reportType._id === reportTypeId))
        ).take(1)
        .subscribe(reportType => {
          this.reportType = reportType;
        })
    );

    this.subscriptions.push(
      this.route.params.switchMap((params: Params) => {
        let companyId = params['id'];
        let reportTypeId = params['reportTypeId'];
        return this.reportService.getReportSchedule(companyId, reportTypeId)
          .map(reportSchedule => {
            return reportSchedule
              ? reportSchedule
              : {
                  _companyId: companyId,
                  reportType: this.reportType,
                  cron: '* * * * *',
                  recipients: []
              }
          })
      })
        .subscribe((reportSchedule: IReportSchedule) => {
          this.reportSchedule = reportSchedule;
          this.cron = reportSchedule && reportSchedule.cron
            ? Cron.fromString(reportSchedule.cron)
            : Cron.fromString('* * * * *')
        })
    );

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscriptions => subscriptions.unsubscribe());
  }

  save() {
    let reportSchedule = Object.assign({}, this.reportSchedule, {
      cron: this.cron.toString(),
      recipients: this.recipientsInput.selectedUsers
    });

    if (reportSchedule._id && (!reportSchedule.recipients || reportSchedule.recipients.length === 0)) {
      return this.reportService.removeReportSchedule(this.company._id, reportSchedule._id)
        .subscribe(
          result => this.back(),
          error => this.error = error
        );
    }

    if (!this.reportSchedule._id) {
      reportSchedule._companyId = this.company._id;
      reportSchedule.reportType = this.reportType
    }

    this.reportService.saveReportSchedule(this.company._id, reportSchedule)
      .subscribe(
        result => this.back(),
        error => this.error = error
      );
  }

  back() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

}
