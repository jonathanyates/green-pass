import {Component, Input, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {FormInput} from '../dynamic-form/dynamic-form.model';
import {FormGroup} from '@angular/forms';

declare let $: any;

@Component({
  selector: 'gp-select-input',
  templateUrl: 'select-input.component.html'
})
export class SelectInputComponent implements AfterViewInit {

  errors = '';
  hasSelected: boolean;
  @Input() form: FormGroup;
  @Input() input: FormInput;
  @ViewChild('select') selectElement: ElementRef;

  ngAfterViewInit(): void {

    if (this.input && this.input.options) {
      this.hasSelected = this.input.options.some(option => option.selected);
    }

    const nativeSelect = this.selectElement.nativeElement;
    nativeSelect.onchange = () => {
      const value = nativeSelect.value;
      this.input.control.setValue(value);
    };

    const index = this.input.options.findIndex(item => item.selected);
    if (index > -1) {
      let option = nativeSelect.children[index + 1];
      if (option) {
        option.selected = true;
        const value = nativeSelect.value;
        this.input.control.setValue(value);
      }
    } else {
      let option = nativeSelect.children[0];
      if (option) {
        option.selected = true;
        const value = nativeSelect.value;
        this.input.control.setValue(value);
      }
    }

    this.input.control.valueChanges
      .subscribe(value => this.validate());
  }

  validate() {
    const control = this.input.control;
    if (control && control.dirty && !control.valid && control.touched) {
      this.errors = '';
      for (const key in control.errors) {
        if (key === 'pattern') {
          this.errors = 'Invalid ' + this.input.label;
        }
      }
    } else {
      this.errors = '';
    }

    if (control && this.input.select) {
      this.input.select(control.value);
    }
  }

}
