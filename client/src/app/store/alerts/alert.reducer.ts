import * as actions from "./alert.actions";
import * as companyActions from "../company/company.actions";
import {AlertsState} from "./alert.state";
import {ActionTypes} from "../authentication/authentication.actions";

export const initialState: AlertsState = {
  alertTypes: null,
  alertSummary: null,
  selected: null
};

export function AlertsReducer(state: AlertsState = initialState, action: actions.Actions): AlertsState {
    switch (action.type) {

      case actions.ActionTypes.LOAD_ALERT_TYPES_SUCCESS:
        return Object.assign({}, state, {
          alertTypes: action.payload
        });

      case actions.ActionTypes.LOAD_SUMMARY_SUCCESS:
        return Object.assign({}, state, {
          alertSummary: action.payload,
          selected: null
        });

      case actions.ActionTypes.LOAD_SELECTED_SUMMARY_ALERT_SUCCESS:
        return Object.assign({}, state, {
          selected: action.payload
        });

      case companyActions.ActionTypes.LOAD:
        return Object.assign({}, state, {
          selected: null
        });

      case ActionTypes.LOGOUT:
        return initialState;

      default:
        return state;
    }

};
