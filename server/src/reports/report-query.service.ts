import {inject, injectable} from "inversify";
import {IDriverService} from "../drivers/driver.service";
import {TYPES} from "../container/container.types";
import {IReportQueryResult, IReportType} from "../../../shared/interfaces/report.interface";
import {IVehicleApiService} from "../vehicles/vehicle.service";

export interface IReportQueryService {
  queries: any;
}

export const reportNames = {
  getNoneCompliantDrivers: 'None Compliant Drivers',
  getNoneCompliantVehicles: 'None Compliant Vehicles',
  getLicenceCheckRequired: 'Licence Checks',
  getUnsignedMandates: 'Outstanding Documents',
  getOnRoadAssessments: 'On Road Assessments'
};

export interface IReportQueryResultFunction {
  (companyId: string, reportType: IReportType): Promise<IReportQueryResult>
}

@injectable()
export class ReportQueryService implements IReportQueryService {

  constructor(@inject(TYPES.services.DriverService) private driverService: IDriverService,
              @inject(TYPES.services.VehicleService) private vehicleService: IVehicleApiService) {
  }

  queries = {
    getNoneCompliantDrivers: (companyId: string, reportType: IReportType): Promise<IReportQueryResult> => {
      return this.driverService.getAll(companyId)
      .then(drivers => drivers
        ? drivers.filter(driver => !driver.compliance || driver.compliance.compliant === false)
        : drivers)
      .then(results => {
        return <IReportQueryResult>{
          reportType: reportType,
          result: results
        }
      })
    },

    getNoneCompliantVehicles: (companyId: string, reportType: IReportType): Promise<IReportQueryResult> => {
      return this.vehicleService.getAll(companyId)
        .then(vehicles => vehicles
          ? vehicles.filter(vehicle => !vehicle.compliance || vehicle.compliance.compliant === false)
          : vehicles)
        .then(results => {
          return <IReportQueryResult>{
            reportType: reportType,
            result: results
          }
        })
    },

    getUnsignedMandates: (companyId: string, reportType: IReportType) => {
      return this.driverService.find({
        $and: [
          {'_companyId': {$eq: companyId}},
          {
            $and: [
              {'documents.status': {$ne: 'completed'} },
              {'documents.status': {$ne: 'Signed'} },
            ]
          }
        ]
      })
      .then(results => {
        return <IReportQueryResult>{
          reportType: reportType,
          result: results
        }
      })
    },

    getLicenceCheckRequired: (companyId: string, reportType: IReportType) => {
      return this.driverService.find({
          $and: [
            {'_companyId': {$eq: companyId}},
            {
              $or: [
                {'licence.nextCheckDate': {$exists: false}},
                {'licence.nextCheckDate': {$lte: new Date()}}
              ]
            }
          ]
        }
      )
      .then(results => {
        return <IReportQueryResult>{
          reportType: reportType,
          result: results
        }
      })
    },

    getOnRoadAssessments: (companyId: string, reportType: IReportType) => {
      return this.driverService.find({
        $and: [
          {'_companyId': {$eq: companyId}},
          {'assessments.onRoad': {$exists: true}},
          {
            $or: [
              {'assessments.onRoad.complete': {$exists: false}},
              {'assessments.onRoad.complete': {$eq: false}}
            ]
          }
        ]
      })
        .then(results => {
          return <IReportQueryResult>{
            reportType: reportType,
            result: results
          }
        })
    }
  }

}