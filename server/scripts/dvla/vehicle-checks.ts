import "reflect-metadata";
import {IVehicleApiService} from '../../src/vehicles/vehicle.service';
import container from '../../src/container/container.config';
import {TYPES} from '../../src/container/container.types';

const vehicleService = <IVehicleApiService>container.get<IVehicleApiService>(TYPES.services.VehicleService);

vehicleService.vehicleChecks()
  .then(vehicles => {
    const count= vehicles && vehicles.length;
    console.log(count + ' Vehicle checks successfully completed');
    process.exit();
  })
  .catch(error => {
    console.log(error);
    process.exit(1);
  });