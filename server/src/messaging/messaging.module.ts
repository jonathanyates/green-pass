import {ContainerModule, interfaces} from "inversify";
import {TYPES} from "../container/container.types";
import {IMessageService, MessageService} from "./message.service";

export const messagingModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IMessageService>(TYPES.services.MessageService).to(MessageService).inSingletonScope();
});