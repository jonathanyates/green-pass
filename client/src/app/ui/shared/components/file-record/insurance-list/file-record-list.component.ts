import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {IFileRecord} from "../../../../../../../../shared/interfaces/common.interface";
import {IDynamicList} from "../../../../../../../../shared/models/dynamic-list.model";
import {FormatDate} from "../../../../../../../../shared/utils/helpers";
import {ColumnType} from '../../../../../../../../shared/models/column-type.model';
import {ModalComponent} from '../../modal/model.component';

@Component({
  selector: 'gp-fileRecord-list',
  template: `
    <gp-dynamic-list [list]="list" (add)="onAdd()" [allowAdd]="allowAdd"
                     (itemSelected)="select($event)" [lightHeader]="lightHeader"></gp-dynamic-list>
      
    <ng-container *ngIf="selectedFileRecord">
      <gp-model [title]="'Remove ' + fileRecordType" (confirmed)="onDeleteConfirmed($event)">
        <div *ngIf="selectedFileRecord; else noFileRecord">
          Are you sure you want to remove this fileRecord ?
        </div>
        <ng-template #noFileRecord>{{'No ' + fileRecordType + ' selected'}}</ng-template>
      </gp-model>
    </ng-container>
  `
})
export class FileRecordListComponent {

  list: IDynamicList;
  selectedFileRecord: IFileRecord;

  @ViewChild(ModalComponent) modal:ModalComponent;

  @Input() allowAdd: boolean;
  @Input() lightHeader: boolean = false;

  @Input() set fileRecordHistory(fileRecordHistory: IFileRecord[]) {
    this.setFileRecordHistory(fileRecordHistory);
  }

  @Input() fileRecordType: string;

  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<IFileRecord> = new EventEmitter<IFileRecord>();
  @Output() itemSelected: EventEmitter<any> = new EventEmitter<any>();
  @Output() back: EventEmitter<any> = new EventEmitter<any>();

  setFileRecordHistory(fileRecordHistory: IFileRecord[]): void {
    this.list = {
      columns: [
        {header: 'Valid From', field: 'validFrom'},
        {header: 'Valid To', field: 'validTo'},
        {header: 'Document Uploaded', field: 'hasFile'},
        {header: '', field: 'remove', columnType: ColumnType.Icon}
      ]
    };

    if (fileRecordHistory) {
      let items = fileRecordHistory
        .sort((a, b) => b.validFrom.getTime() - a.validFrom.getTime());

      this.list.items = items.map(fileRecord => {
          return Object.assign({}, fileRecord , {
            validFrom: FormatDate(fileRecord.validFrom),
            validTo: FormatDate(fileRecord.validTo)
          })
        });

      this.list.actions = fileRecordHistory.map(item => ({
        remove: {
          action: args => { if (args) this.onDelete(args.item) },
          icon: 'fa fa-trash-o green-text'
        }
      }));

      this.list.styles = items.map(fileRecord => {
        return Object.assign({}, {
          hasFile: fileRecord.filename
            ? 'fa fa-check green-text'
            : 'fa fa-times red-text'
        })
      })
    }
  }

  onAdd() {
    this.add.emit();
  }

  select(fileRecord: IFileRecord) {
    this.itemSelected.emit(fileRecord);
  }

  onDelete(fileRecord: IFileRecord) {
    if (!fileRecord) return;
    this.selectedFileRecord = fileRecord;
    setTimeout(() => this.modal.open(), 0);
  }

  onDeleteConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedFileRecord) {
      this.delete.emit(this.selectedFileRecord);
    }
    this.selectedFileRecord = null;
  }
}
