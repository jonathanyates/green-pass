import { Component } from '@angular/core';

@Component({
    selector: 'gp-unauthorized',
    template: `
    <gp-heading [text]="'Unauthorized'"></gp-heading>
    <div class="alert alert-danger" role="alert">
      <p>Sorry you are not authorized to view this page.</p>
    </div>
`
})
export class UnauthorizedComponent {
}
