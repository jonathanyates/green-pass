import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {pagination, Paths} from "../../../shared/constants";
import {IVehicle} from "../../../../../../shared/interfaces/vehicle.interface";
import {SelectCompany} from "../../../store/company/company.selectors";
import {SelectVehiclesPagination} from "../../../store/vehicles/vehicle.selectors";
import {LoadVehiclesAction} from "../../../store/vehicles/vehicle.actions";
import {ModalComponent} from "../../shared/components/modal/model.component";
import {VehicleService} from "../../../services/vehicle.service";
import {BusyAction} from "../../../store/progress/progress.actions";
import {HandleErrorAction} from "../../../store/error/error.actions";
import {ICompany} from "../../../../../../shared/interfaces/company.interface";

@Component({
  template: `
    <gp-vehicle-list [vehicles]="vehicles" (back)="back()"
                     [total]="total" [page]="page" [itemsPerPage]="itemsPerPage" [serverMode]="true"
                     (pageChange)="onPageChange($event)" (search)="onSearch($event)"
                     (add)="onAdd()" (remove)="onRemove($event)"></gp-vehicle-list>   
    
    <gp-model [title]="'Remove Vehicle'" (confirmed)="onRemoveConfirmed($event)">
      <div *ngIf="selectedVehicle; else noVehicle">
        Are you sure you want to remove vehicle '{{selectedVehicle.registrationNumber}}' ?
      </div>
      <ng-template #noVehicle>No Vehicle selected</ng-template>
    </gp-model>
  `
})
export class VehicleListContainerComponent implements OnInit {

  company: ICompany;
  vehicles: IVehicle[];

  total: number;
  page: number;
  itemsPerPage: number;
  selectedVehicle: IVehicle;

  private searchCriteria: any;
  private subscriptions = [];

  @ViewChild(ModalComponent) modal:ModalComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<AppState>,
              private vehicleService: VehicleService) {
  }

  ngOnInit() {
    this.itemsPerPage = pagination.pageSize;
    this.vehicles = null;
    this.subscriptions.push(this.store.let(SelectVehiclesPagination())
      .filter(vehicles => vehicles !== null)
      .subscribe(results => {
        this.vehicles = results.vehicles;
        this.total = results.total;
        this.page = results.page;
      }));

    this.subscriptions.push(this.store.let(SelectCompany())
      .subscribe(company => this.company = company));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onPageChange(page: number) {
    if (this.searchCriteria) {
      this.store.dispatch(new LoadVehiclesAction(this.company._id, page, this.itemsPerPage, this.searchCriteria));
      return;
    }

    this.store.dispatch(new LoadVehiclesAction(this.company._id, page, this.itemsPerPage))

  }

  onSearch(term: string) {
    if (term) {
      this.searchCriteria = {
        registrationNumber: term
      };
      this.store.dispatch(new LoadVehiclesAction(this.company._id, 1, this.itemsPerPage, this.searchCriteria));
      return
    }

    this.store.dispatch(new LoadVehiclesAction(this.company._id, 1, this.itemsPerPage))
  }

  onRemove(vehicle:IVehicle) {
    if (!vehicle) return;
    this.modal.open();
    this.selectedVehicle = vehicle;
  }

  onRemoveConfirmed(confirmed: boolean) {
    if (confirmed && this.selectedVehicle) {
      this.store.dispatch(new BusyAction());
      this.vehicleService.removeVehicle(this.selectedVehicle._companyId, this.selectedVehicle._id)
        .subscribe(vehicle => {
          this.store.dispatch(new LoadVehiclesAction(this.company._id, this.page, this.itemsPerPage));
          this.store.dispatch(new BusyAction(false));
        }, error => {
          this.store.dispatch(new HandleErrorAction(error));
          this.store.dispatch(new BusyAction(false));
        })
    }
    this.selectedVehicle = null;
  }

  onAdd() {
    if (this.company && this.company._id) {
      return this.router.navigate(['./', Paths.New], { relativeTo: this.route });
    }
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }

}
