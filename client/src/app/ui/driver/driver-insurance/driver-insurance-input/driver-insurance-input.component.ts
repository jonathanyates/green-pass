import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {Subscription} from "rxjs";
import { SelectDriversState} from "../../../../store/drivers/driver.selectors";
import {IDriver, IDriverInsurance} from "../../../../../../../shared/interfaces/driver.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {IInsurance} from "../../../../../../../shared/interfaces/common.interface";
import {SaveDriverInsuranceAction} from "../../../../store/drivers/driver-insurance.actions";

@Component({
  selector: 'gp-driver-insurance-input',
  template: `
    <gp-fileRecord-input [fileRecord]="insurance"
                         [heading]="'Insurance details'"
                         [fileHeading]="'Insurance Certificate'"
                         (submit)="save($event)" (cancel)="cancel()">
    </gp-fileRecord-input>
  `
})
export class DriverInsuranceInputComponent implements OnInit, OnDestroy {

  insurance: IDriverInsurance;
  private driver: IDriver;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.store.let(SelectDriversState())
      .subscribe(state => {
        this.driver = state.selected;
        this.insurance = state.selectedInsurance
          ? state.selectedInsurance
          : <IDriverInsurance>{ validFrom: null, validTo: null };
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save(insurance: IInsurance) {
    this.store.dispatch(new SaveDriverInsuranceAction(this.driver, insurance));
  }

  cancel() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
