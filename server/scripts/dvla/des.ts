import 'reflect-metadata';
import { DvlaService } from '../../src/dvla/service/dvla.service';
import { driverInfo } from '../../db/drivers';

// const drivingLicenceNumber = 'YATES607198JP9LK',
//   nationalInsuranceNumber = 'NR071159A',
//   postCode = 'PR31YF';

const dvlaService = new DvlaService();

const promiseSerial = (funcs) =>
  funcs.reduce(
    (promise, func) => promise.then((result) => func().then(Array.prototype.concat.bind(result))),
    Promise.resolve([])
  );

const funcs = driverInfo
  .slice(-1)
  .map((info) => () => dvlaService.drivingLicenceEnquiry(info.licenceNo, info.ni, info.postcode));

promiseSerial(funcs)
  .then((result) => {
    console.log(result);
    process.exit();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
