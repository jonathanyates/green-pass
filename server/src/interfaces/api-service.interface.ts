export interface IApiService<T> {

  getAll(companyId: string): Promise<T[]>;

  get(id: string): Promise<T>;

  find(criteria: any): Promise<T[]>;

  findOne(criteria: any): Promise<T>;

  add(companyId: string, entity: T): Promise<T>;

  update(entity: T): Promise<T>;
}