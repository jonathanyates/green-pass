import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {apiPath} from "../shared/constants";
import {HttpBaseService} from "./http-base.service";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.states";
import {User} from "../ui/user/user.model";
import {IDriver, IDriverInsurance} from "../../../../shared/interfaces/driver.interface";
import {ServerError} from "../shared/errors/server.error";
import HTTP_STATUS_CODES from "../shared/errors/status-codes.enum";
import {IFawCertificate, IPaginationResults} from "../../../../shared/interfaces/common.interface";
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable()
export class DriverService extends HttpBaseService {

  private companiesUrl = apiPath + '/companies/';

  constructor(httpClient: HttpClient, store: Store<AppState>) {
    super(httpClient, store);
  }

  getDrivers(companyId: string, page: number, pageSize: number, searchParameters?: any)
    : Observable<IPaginationResults<IDriver>> {

    this.authorize();

    let params = new HttpParams()
      .set('page', page ? page.toString() : '1')
      .set('pageSize', pageSize ? pageSize.toString() : '10');

    if (searchParameters) {
      for(let prop in searchParameters) {
        if (searchParameters.hasOwnProperty(prop)) {
          params = params.set(prop, searchParameters[prop]);
        }
      }
    }

    return this.httpClient.get<IPaginationResults<IDriver>>(this.companiesUrl + companyId + '/drivers', this.getOptions(params))
      .map(res =>  {
        let result = this.handleResult<IPaginationResults<IDriver>>(res);
        if (result.items) {
          result.items.forEach(driver => driver.fullName = User.getFullName(driver._user));
        }
        return result;
      })
      .catch(this.handleError);
  }

  searchDriver(companyId: string, searchParameters: any): Observable<IDriver> {
    this.authorize();

    let params = new HttpParams();
    for(let prop in searchParameters) {
      if (searchParameters.hasOwnProperty(prop)) {
        params = params.set(prop, searchParameters[prop]);
      }
    }

    return this.httpClient.get<IDriver[]>(this.companiesUrl + companyId + '/drivers', this.getOptions(params))
      .map(response =>  {
        let drivers = this.handleResult<IDriver[]>(response);
        if (drivers && drivers.length > 0) {
          let driver = drivers[0];
          driver.fullName = User.getFullName(driver._user);
          return driver;
        }
        return null;
      })
      .catch(this.handleError);
  }

  getDriver(companyId: string, driverId: string): Observable<IDriver> {

    this.authorize();

    return this.httpClient.get<IDriver>(`${this.companiesUrl}${companyId}/drivers/${driverId}`, this.getOptions())
      .map(response => this.extractDriver(response))
      .catch(this.handleError);
  }

  saveDriver(driver: IDriver): Observable<IDriver> {

    this.authorize();

    if (driver._id) {
      return this.httpClient.put<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}`, driver, this.getOptions())
        .map(response => this.extractDriver(response))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers`, driver, this.getOptions())
        .map(response => this.extractDriver(response))
        .catch(this.handleError);
    }
  }

  importDrivers(companyId: string, drivers: Array<any>): Observable<IDriver[]> {

    this.authorize();

    return this.httpClient.post<IDriver[]>(`${this.companiesUrl}${companyId}/drivers/import`, drivers, this.getOptions())
      .map(response =>  {
        let drivers = this.handleResult<IDriver[]>(response);
        if (drivers && drivers.length > 0) {
          let driver = drivers[0];
          driver.fullName = User.getFullName(driver._user);
          return driver;
        }
        return null;
      })
      .catch(error => {
        return this.handleError(error);
      });

  }

  removeDriver(companyId: string,driverId: string): Observable<IDriver> {
    this.authorize();

    return this.httpClient.delete<IDriver>(`${this.companiesUrl}${companyId}/drivers/${driverId}`, this.getOptions())
      .map(response => this.extractDriver(response))
      .catch(this.handleError);
  }

  updateDriverStatus(driver: IDriver): Observable<IDriver> {

    this.authorize();

    return this.httpClient.get<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/status`, this.getOptions())
      .map(response => this.extractDriver(response))
      .catch(this.handleError);
  }

  getDrivingLicenceCheckCode(driver: IDriver, licenceNumber: string = null, niNumber: string = null, postcode: string = null): Observable<string> {

    this.authorize();

    let params = new HttpParams()
      .set('licenceNumber', licenceNumber)
      .set('niNumber', niNumber)
      .set('postcode', postcode);

    let options = params ? this.getOptions(params) : this.getOptions();

    return this.httpClient.get<string>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/licence/checkcode`, options)
      .map(checkCode =>  {
        if (!checkCode) {
          throw new ServerError('No Check Code', HTTP_STATUS_CODES.NOT_FOUND);
        }
        checkCode = checkCode.replace(/"/g, '');
        return checkCode;
      })
      .catch(this.handleError);
  }

  drivingLicenceCheck(driver: IDriver, checkCode: string, licenceNumber: string = null): Observable<IDriver> {

    this.authorize();

    let params;
    if (licenceNumber) {
      params = new HttpParams().set('licenceNumber', licenceNumber);
    }

    let options = params ? this.getOptions(params) : this.getOptions();

    return this.httpClient.get<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/licence/check/${checkCode}`, options)
      .map(response =>  this.extractDriver(response))
      .catch(err => this.handleError(err));
  }

  saveDriverInsurance(driver: IDriver, driverInsurance: IDriverInsurance): Observable<IDriverInsurance> {

    this.authorize();

    let formData = new FormData();
    formData.append('validFrom', driverInsurance.validFrom.toString());
    formData.append('validTo', driverInsurance.validTo.toString());
    formData.append('fileType', driverInsurance.fileType);

    if (driverInsurance.file) {
      formData.append('file', driverInsurance.file);
    }

    if (driverInsurance._id) {
      return this.httpClient.put<IDriverInsurance>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/insurance/${driverInsurance._id}`, formData, this.getOptions())
        .map(res => this.handleResult<IDriverInsurance>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IDriverInsurance>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/insurance/`, formData, this.getOptions())
        .map(res => this.handleResult<IDriverInsurance>(res))
        .catch(this.handleError);
    }
  }

  deleteDriverInsurance(driver: IDriver, driverInsurance: IDriverInsurance): Observable<IDriver> {
    this.authorize();

    return this.httpClient.delete<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/insurance/${driverInsurance._id}`, this.getOptions())
      .map(response =>  this.extractDriver(response))
      .catch(this.handleError);
  }

  saveDriverFaw(driver: IDriver, fawCertificate: IFawCertificate): Observable<IFawCertificate> {

    this.authorize();

    let formData = new FormData();
    formData.append('validFrom', fawCertificate.validFrom.toString());
    formData.append('validTo', fawCertificate.validTo.toString());
    formData.append('fileType', fawCertificate.fileType);

    if (fawCertificate.file) {
      formData.append('file', fawCertificate.file);
    }

    if (fawCertificate._id) {
      return this.httpClient.put<IFawCertificate>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/faw/${fawCertificate._id}`, formData, this.getOptions())
        .map(res => this.handleResult<IFawCertificate>(res))
        .catch(this.handleError);
    } else {
      return this.httpClient.post<IFawCertificate>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/faw/`, formData, this.getOptions())
        .map(res => this.handleResult<IFawCertificate>(res))
        .catch(this.handleError);
    }
  }

  deleteDriverFaw(driver: IDriver, fawCertificate: IFawCertificate): Observable<IDriver> {
    this.authorize();

    return this.httpClient.delete<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/faw/${fawCertificate._id}`, this.getOptions())
      .map(response =>  this.extractDriver(response))
      .catch(this.handleError);
  }

  addVehicle(driver: IDriver, vehicleId: string): Observable<IDriver> {

    this.authorize();
    let map = { driverId: driver._id, vehicleId: vehicleId };

    return this.httpClient.post<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/vehicles/${vehicleId}`,
      map, this.getOptions())
      .map(response =>  this.extractDriver(response))
      .catch(this.handleError);
  }

  removeVehicle(driver: IDriver, vehicleId: string): Observable<IDriver> {

    this.authorize();

    return this.httpClient.delete<IDriver>(`${this.companiesUrl}${driver._companyId}/drivers/${driver._id}/vehicles/${vehicleId}`, this.getOptions())
      .map(response =>  this.extractDriver(response))
      .catch(this.handleError);
  }

  private extractDriver(response: IDriver):IDriver {
    let driver = this.handleResult<IDriver>(response);
    driver.fullName = User.getFullName(driver._user);
    return driver;
  }

}
