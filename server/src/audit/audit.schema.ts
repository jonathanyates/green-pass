import mongoose from 'mongoose';
import { IAudit } from './audit.interface';

export const AuditSchema = new mongoose.Schema({
  _companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
  role: { type: String, required: true },
  action: { type: String, required: true },
  target: { type: String, required: false },
  result: mongoose.Schema.Types.Mixed,
  error: { type: String, required: false },
  dateTime: { type: Date, required: true },
});

export interface IAuditModel extends mongoose.Document, IAudit {
  _id: any;
}
