import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../store/app.states";
import {IListGroupItem} from "../../../shared/components/dynamic-list-group/dynamic-list-group";
import {IOnRoadAssessment} from "../../../../../../../shared/interfaces/assessment.interface";
import {IDriver} from "../../../../../../../shared/interfaces/driver.interface";
import {SelectDriver} from "../../../../store/drivers/driver.selectors";
import {FormatDate} from "../../../../../../../shared/utils/helpers";
import {Paths} from "../../../../shared/constants";

@Component({
  selector: 'gp-on-road-assessment',
  template: `
    <gp-card [header]="'On Road Assessment'" (edit)="edit()">
      <gp-dynamic-list-group [items]="items"></gp-dynamic-list-group>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})
export class OnRoadAssessmentComponent implements OnInit, OnDestroy {

  items: IListGroupItem[];
  private assessment: IOnRoadAssessment;
  private driver: IDriver;
  private subscriptions = [];

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data
      .subscribe((data: { onRoadAssessment: IOnRoadAssessment }) => {
        this.assessment = data.onRoadAssessment;
        this.initialise();
      }));

    this.subscriptions.push(this.store.let(SelectDriver())
      .subscribe(driver => this.driver = driver));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  private initialise() {
    if (this.assessment) {
      this.items = [
        {title: "Invite Date", value: FormatDate(this.assessment.inviteDate)},
        {title: "Assessment Date", value: FormatDate(this.assessment.assessmentDate)},
        {title: "Assessed by", value: this.assessment.assessedBy},
        {title: "Result", value: this.assessment.result},
      ];

      if (this.assessment.filename) {
        this.items.push({title: 'Report', value: this.assessment.file, type: this.assessment.fileType})
      } else {
        this.items.push({title: 'Report', value: 'Not uploaded'})
      }
    }
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
