import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class ReplayMessenger {

  private subjects = {};

  public toObservable<T>(key:any):Observable<T> {
    if (!this.subjects[key]) {
      this.subjects[key] = new ReplaySubject(1);
    }
    return this.subjects[key].asObservable();
  }

  public publish<T>(key:any, value:T) {
    if (!this.subjects[key]) {
      this.subjects[key] = new ReplaySubject(1);
    }
    this.subjects[key].next(value);
  }
}

export const replayMessenger = new ReplayMessenger();

/* usage
 var messenger = new Messenger();

 messenger.toObservable("foo").subscribe(x => {
 console.log("subscribed to " + x);
 });

 messenger.publish("foo", { bar : 'bar'});
 */
