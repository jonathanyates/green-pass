import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectCompanyAction} from "../../../store/company/company.actions";
import {SelectDriverAction} from "../../../store/drivers/driver.actions";
import {ClearDriverInsuranceAction, SelectDriverInsuranceAction} from "../../../store/drivers/driver-insurance.actions";

@Injectable()
export class DriverInsuranceResolve implements Resolve<boolean> {

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let id = route.params['id'];
    let driverId = route.params['driverId'];
    let insuranceId = route.params['insuranceId'];

    if (!id || !driverId) return false;

    this.store.dispatch(new SelectCompanyAction(id));
    this.store.dispatch(new SelectDriverAction(id, driverId));

    if (insuranceId) {
      this.store.dispatch(new SelectDriverInsuranceAction(driverId, insuranceId));
    } else {
      if (route.url[route.url.length-1].path == 'new') {
        this.store.dispatch(new ClearDriverInsuranceAction());
      }
    }

    return true;
  }

}
