import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {FormInput} from "../../shared/components/dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {DynamicFormBuilder} from "../../shared/components/dynamic-form/dynamic-form.builder";
import {AuthorizationService} from "../../../services/authorization.service";
import {BusyAction} from "../../../store/progress/progress.actions";
import {Paths} from "../../../shared/constants";
import {RegExpPatterns} from "../../../../../../shared/constants";

@Component({
  selector: 'gp-login',
  templateUrl: 'forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit  {

  inputs:FormInput[];
  form:FormGroup;
  message:string;
  error:string;

  constructor(
    private authService: AuthorizationService,
    private store: Store<AppState>,
    public router: Router) {
  }

  ngOnInit(): void {

    this.inputs = [
      { id: 'email', label: 'Email address', type:'text', icon: 'fa-email',
        regex: RegExpPatterns.Email, required: true, autoFocus: true }
    ];

    this.form = DynamicFormBuilder.build(this.inputs);
  }

  submit() {
    let email = this.form.controls['email'].value.toString().trim().toLowerCase();

    this.store.dispatch(new BusyAction());

    this.authService.forgotPassword(email)
      .subscribe(message => {
        this.store.dispatch(new BusyAction(false));
        this.message = message;
      },(err: Error) => {
        this.store.dispatch(new BusyAction(false));
        this.error = err.message;
      })
  }

  cancel() {
    this.router.navigate([Paths.Root]);
  }

}
