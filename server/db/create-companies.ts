import "reflect-metadata";
import {TYPES} from "../src/container/container.types";
import {ICompanyService} from "../src/companies/company.service.interface";
import {companies} from "./companies";
import container from "../src/container/container.config";

const companyService = container.get<ICompanyService>(TYPES.services.CompanyService);

Promise.all(companies.map(company => companyService.add(company)))
  .then(companies => {
    console.log('Companies created successfully.');
    process.exit();
  })
  .catch(error =>{
    console.log(error);
    process.exit(1);
  });