import {Component, Input, AfterViewInit} from '@angular/core';
import {FormInput} from "../dynamic-form/dynamic-form.model";
import {FormGroup} from "@angular/forms";
import {Observable} from "rxjs";

declare let $: any;

@Component({
  selector: 'gp-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements AfterViewInit {

  @Input() form: FormGroup;
  @Input() input: FormInput;
  @Input() errors: string;

  results: Array<any> = null;
  showResults: boolean = false;

  ngAfterViewInit(): void {
    let control = $('#' + this.input.id);

    this.input.control.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .map(value => value ? value.replace(/\s/g,'') : null)
      .switchMap(value => {
        if (value && value.length === 6) {
          return this.input.action(value).startWith([]);
        }
        return Observable.of([]);
      }).subscribe(results => {
        if (control.is(':focus')) {
          this.results = results;
          this.showResults = results && results.length > 0;
        }
      });
  }

  onKeyDown = (evt: KeyboardEvent) => {
    let event: any = evt || window.event;
    if (event.keyCode == 27) {
      this.showResults = false;
    }
  };

  hideResults() {
    setTimeout(() => {
      this.showResults = false;
    }, 500);
  }

  select(item) {
    this.input.select(item);
    this.showResults = false;
  }

  search(value: string) {
    let term = value ? value.replace(/\s/g,'') : null;
    if (term && term.length === 6) {
      this.input.action(value).startWith([])
        .subscribe(results => {
          this.results = results;
          this.showResults = results && results.length > 0;
        });
    }
  }

}
