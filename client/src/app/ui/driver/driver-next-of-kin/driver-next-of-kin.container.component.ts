import {Component, OnInit} from '@angular/core';
import {IContact} from "../../../../../../shared/interfaces/driver.interface";
import {Paths} from "../../../shared/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectDriverUserInfo} from "../../../store/drivers/driver.selectors";
import {AppState} from "../../../store/app.states";
import {Store} from "@ngrx/store";
import {BaseComponent} from "../../shared/components/base/base.component";

@Component({
  selector: 'gp-driver-next-of-kin-component',
  template: `
    <gp-card [header]="'Next Of Kin'">
        <gp-driver-next-of-kin [nextOfKin]="nextOfKin" [allowInput]="allowInput" (edit)="edit()"></gp-driver-next-of-kin>
    </gp-card>
    <button class="btn btn-primary waves-effect" (click)="back()">Back</button>
  `
})

export class DriverNextOfKinContainerComponent extends BaseComponent implements OnInit {

  nextOfKin: IContact;
  allowInput: boolean = false;

  constructor(
    protected store: Store<AppState>,
    protected router: Router,
    private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriverUserInfo())
      .subscribe(info => {
        if (info.driver) {
          this.nextOfKin = info.driver.nextOfKin;
        }
        this.allowInput = info.allowInput;
      }));
  }

  edit() {
    return this.router.navigate(['./', Paths.Edit], { relativeTo: this.route });
  }

  back() {
    return this.router.navigate(['../'], { relativeTo: this.route });
  }
}
