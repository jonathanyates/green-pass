import {ContainerModule, interfaces} from "inversify";
import {IQueryableRepository,} from "../db/db.repository";
import {IAuditModel} from "./audit.schema";
import {TYPES} from "../container/container.types";
import {AuditRepository} from "./audit.repository";
import {AuditService, IAuditService} from "./audit.service";
import {AuditRouter} from "./audit.router";
import {IApiRouter} from "../interfaces/api-router.interface";
import {AuditController, IAuditController} from "./audit.controller";

export const auditModule = new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IApiRouter>(TYPES.routers.AuditRouter).to(AuditRouter).inSingletonScope();
  bind<IAuditController>(TYPES.controllers.AuditController).to(AuditController).inSingletonScope();
  bind<IAuditService>(TYPES.services.AuditService).to(AuditService).inSingletonScope();
  bind<IQueryableRepository<IAuditModel>>(TYPES.repositories.AuditRepository).to(AuditRepository).inSingletonScope();
});