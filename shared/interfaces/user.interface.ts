export interface IUser {
  _id?: any,
  _companyId?: string,
  title?: string,
  username: string,
  password: string,
  forename: string,
  surname: string,
  email?: string,
  role: string
  resetPasswordToken?: string,
  resetPasswordExpires?: Date,
  changePassword?: boolean,
  removed?: boolean
}