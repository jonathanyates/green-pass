import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IDriver} from "../../../../../../shared/interfaces/driver.interface";
import {Subscription} from "rxjs/Subscription";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.states";
import {SelectDriver} from "../../../store/drivers/driver.selectors";
import {BusyAction} from "../../../store/progress/progress.actions";
import {AssessmentLoginSuccessAction} from "../../../store/drivers/driver.actions";
import {Observable} from "rxjs/Observable";
import {AssessmentService} from "../../../services/assessment.service";

@Component({
  selector: 'gp-roadmarque',
  template: `
    <div *ngIf="url">
      <iframe id="frame" width="100%" style="height: 100vh; border: none" #frame 
              [src]="url | safe" (load)="onFrameLoad(frame)"
              [ngClass]="{'hidden-xs-up': busy}"></iframe>
    </div>
    <div *ngIf="error">
      <div class="alert alert-danger" role="alert">
        <p>Unfortunately there seems to be a problem connecting to Road Marque Assessments at the moment.</p>
        <p>Please try later.</p>
      </div>
    </div>
  `
})
export class RoadMarqueComponent implements OnInit, OnDestroy {

  driver: IDriver;
  url: string;
  error: string;
  showFrame: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(private store: Store<AppState>,
              private assessmentService: AssessmentService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.store.let(SelectDriver())
     .filter(driver => driver != null).take(1)
      .subscribe((driver:IDriver) => {
        this.driver = driver;
        this.store.dispatch(new BusyAction());
        this.assessmentService.roadMarqueLogin(driver)
          .timeout(10000)
          .subscribe(url => {
            this.url = url;
            this.store.dispatch(new AssessmentLoginSuccessAction());
            this.store.dispatch(new BusyAction(false));
          }, err => {
            this.store.dispatch(new BusyAction(false));
            this.error = err.message;
          });
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onFrameLoad(frame) {

    // if (frame.src === this.url) {
    //   frame.src = 'https://www.roadmarque.com/Roadmarque/RoadMarqueModule2/DriverAssessments.aspx';
    //
    //   Observable.timer(200).subscribe(_ => {
    //     this.showFrame = true;
    //     this.store.dispatch(new BusyAction(false));
    //   });
    //
    //   return;
    // }

    Observable.timer(200).subscribe(_ => {
      window.scrollTo(0,0)
    });
  }

}
